/* ScrutariLib_Data - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ThesaurusURI;


/**
 *
 * @author Vincent Calame
 */
public final class URIPrimitives {

    private URIPrimitives() {
    }

    public static void writeScrutariDataURI(ScrutariDataURI scrutariDataURI, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeShort(scrutariDataURI.getType());
        if (scrutariDataURI instanceof BaseURI) {
            write((BaseURI) scrutariDataURI, primitivesWriter);
        } else if (scrutariDataURI instanceof CorpusURI) {
            write((CorpusURI) scrutariDataURI, primitivesWriter);
        } else if (scrutariDataURI instanceof ThesaurusURI) {
            write((ThesaurusURI) scrutariDataURI, primitivesWriter);
        } else if (scrutariDataURI instanceof FicheURI) {
            write((FicheURI) scrutariDataURI, primitivesWriter);
        } else if (scrutariDataURI instanceof MotcleURI) {
            write((MotcleURI) scrutariDataURI, primitivesWriter);
        } else {
            throw new SwitchException("obj instanceof = " + scrutariDataURI.getClass().getName());
        }
    }

    private static void write(BaseURI baseURI, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeString(baseURI.getAuthority());
        primitivesWriter.writeString(baseURI.getBaseName());
    }

    private static void write(CorpusURI corpusURI, PrimitivesWriter primitivesWriter) throws IOException {
        write(corpusURI.getBaseURI(), primitivesWriter);
        primitivesWriter.writeString(corpusURI.getCorpusName());
    }

    private static void write(ThesaurusURI thesaurusURI, PrimitivesWriter primitivesWriter) throws IOException {
        write(thesaurusURI.getBaseURI(), primitivesWriter);
        primitivesWriter.writeString(thesaurusURI.getThesaurusName());
    }

    private static void write(FicheURI ficheURI, PrimitivesWriter primitivesWriter) throws IOException {
        write(ficheURI.getCorpusURI(), primitivesWriter);
        primitivesWriter.writeString(ficheURI.getFicheId());
    }

    private static void write(MotcleURI motcleURI, PrimitivesWriter primitivesWriter) throws IOException {
        write(motcleURI.getThesaurusURI(), primitivesWriter);
        primitivesWriter.writeString(motcleURI.getMotcleId());
    }

    public static ScrutariDataURI readScrutariDataURI(PrimitivesReader primitivesReader) throws IOException {
        short type = primitivesReader.readShort();
        switch (type) {
            case ScrutariDataURI.BASEURI_TYPE:
                return toBaseURI(primitivesReader);
            case ScrutariDataURI.CORPUSURI_TYPE:
                return toCorpusURI(primitivesReader);
            case ScrutariDataURI.THESAURUSURI_TYPE:
                return toThesaurusURI(primitivesReader);
            case ScrutariDataURI.FICHEURI_TYPE:
                return toFicheURI(primitivesReader);
            case ScrutariDataURI.MOTCLEURI_TYPE:
                return toMotcleURI(primitivesReader);
            default:
                throw new SwitchException("primitivesReader.readShort() = " + type);

        }
    }

    private static BaseURI toBaseURI(PrimitivesReader primitivesReader) throws IOException {
        String authority = primitivesReader.readString();
        String baseName = primitivesReader.readString();
        return new BaseURI(authority, baseName);
    }

    private static CorpusURI toCorpusURI(PrimitivesReader primitivesReader) throws IOException {
        BaseURI baseURI = toBaseURI(primitivesReader);
        String corpusName = primitivesReader.readString();
        return new CorpusURI(baseURI, corpusName);
    }

    private static ThesaurusURI toThesaurusURI(PrimitivesReader primitivesReader) throws IOException {
        BaseURI baseURI = toBaseURI(primitivesReader);
        String thesaurusName = primitivesReader.readString();
        return new ThesaurusURI(baseURI, thesaurusName);
    }

    private static FicheURI toFicheURI(PrimitivesReader primitivesReader) throws IOException {
        CorpusURI corpusURI = toCorpusURI(primitivesReader);
        String ficheId = primitivesReader.readString();
        return new FicheURI(corpusURI, ficheId);
    }

    private static MotcleURI toMotcleURI(PrimitivesReader primitivesReader) throws IOException {
        ThesaurusURI thesaurusURI = toThesaurusURI(primitivesReader);
        String motcleId = primitivesReader.readString();
        return new MotcleURI(thesaurusURI, motcleId);
    }

}
