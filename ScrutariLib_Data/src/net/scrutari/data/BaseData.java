/* ScrutariLib_Data - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesBuilder;
import net.scrutari.datauri.BaseURI;


/**
 *
 * @author Vincent Calame
 */
public final class BaseData implements Serializable {

    private static final long serialVersionUID = 2L;
    private final BaseURI baseURI;
    private final Integer baseCode;
    private final String scrutariSourceName;
    private final List<Lang> availableLangList;
    private final String baseIconPart1;
    private final String baseIconPart2;
    private final Phrases phrases;
    private final Attributes attributes;
    private final List<Integer> corpusCodeList;
    private final List<Integer> thesaurusCodeList;


    private BaseData(BaseURI baseURI, Integer baseCode, String scrutariSourceName, Lang[] availableLangArray, String baseIconPart1, String baseIconPart2, Phrases phrases, Attributes attributes, List<Integer> corpusCodeList, List<Integer> thesaurusCodeList) {
        this.baseURI = baseURI;
        this.baseCode = baseCode;
        this.scrutariSourceName = scrutariSourceName;
        this.availableLangList = LangsUtils.wrap(availableLangArray);
        this.baseIconPart1 = baseIconPart1;
        this.baseIconPart2 = baseIconPart2;
        this.phrases = phrases;
        this.attributes = attributes;
        this.corpusCodeList = corpusCodeList;
        this.thesaurusCodeList = thesaurusCodeList;
    }

    public String getScrutariSourceName() {
        return scrutariSourceName;
    }

    public BaseURI getBaseURI() {
        return baseURI;
    }

    public Integer getBaseCode() {
        return baseCode;
    }

    public List<Lang> getAvailableLangList() {
        return availableLangList;
    }

    public Lang checkAvailableLang(Lang lang) {
        int length = availableLangList.size();
        if (length == 0) {
            return lang;
        }
        for (int i = 0; i < length; i++) {
            if (lang.equals(availableLangList.get(i))) {
                return lang;
            }
        }
        if (!lang.isRootLang()) {
            Lang rootLang = lang.getRootLang();
            for (int i = 0; i < length; i++) {
                if (rootLang.equals(availableLangList.get(i))) {
                    return rootLang;
                }
            }
        }
        return availableLangList.get(0);
    }

    public String getBaseIcon(@Nullable Lang lang) {
        if (baseIconPart2 == null) {
            return baseIconPart1;
        }
        StringBuilder buf = new StringBuilder(baseIconPart1.length() + baseIconPart2.length() + 10);
        buf.append(baseIconPart1);
        if (lang != null) {
            buf.append(lang.toString());
        } else {
            buf.append("$LANG");
        }
        buf.append(baseIconPart2);
        return buf.toString();
    }

    public List<Integer> getCorpusCodeList() {
        return corpusCodeList;
    }

    public List<Integer> getThesaurusCodeList() {
        return thesaurusCodeList;
    }

    public Phrases getPhrases() {
        return phrases;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public static BaseData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        Integer baseCode = primitivesReader.readInt();
        BaseURI baseURI = (BaseURI) URIPrimitives.readScrutariDataURI(primitivesReader);
        String scrutariSourceName = primitivesReader.readString();
        int langsUILength = primitivesReader.readInt();
        Lang[] langsUI = new Lang[langsUILength];
        for (int i = 0; i < langsUILength; i++) {
            langsUI[i] = Lang.build(primitivesReader.readString());
        }
        String baseIconPart1 = primitivesReader.readString();
        String baseIconPart2 = primitivesReader.readString();
        Phrases phrases = PhrasesPrimitives.readPhrases(primitivesReader);
        Attributes attributes = AttributesPrimitives.readAttributes(primitivesReader);
        Integer[] corpusCodeArray = primitivesReader.readIntegerArray();
        Integer[] thesaurusCodeArray = primitivesReader.readIntegerArray();
        return new BaseData(baseURI, baseCode, scrutariSourceName, langsUI, baseIconPart1, baseIconPart2, phrases, attributes,
                PrimUtils.wrap(corpusCodeArray), PrimUtils.wrap(thesaurusCodeArray));
    }

    public static void toPrimitives(BaseData baseData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(baseData.baseCode);
        URIPrimitives.writeScrutariDataURI(baseData.baseURI, primitivesWriter);
        primitivesWriter.writeString(baseData.scrutariSourceName);
        int availableLangLength = baseData.availableLangList.size();
        primitivesWriter.writeInt(availableLangLength);
        for (int i = 0; i < availableLangLength; i++) {
            primitivesWriter.writeString(baseData.availableLangList.get(i).toString());
        }
        primitivesWriter.writeString(baseData.baseIconPart1);
        primitivesWriter.writeString(baseData.baseIconPart2);
        PhrasesPrimitives.writePhrases(baseData.phrases, primitivesWriter);
        AttributesPrimitives.writeAttributes(baseData.attributes, primitivesWriter);
        primitivesWriter.writeIntArray(baseData.corpusCodeList);
        primitivesWriter.writeIntArray(baseData.thesaurusCodeList);
    }


    public static class Builder implements LabelConsumer {

        private final BaseURI baseURI;
        private final List<Integer> corpusList = new ArrayList<Integer>();
        private final List<Integer> thesaurusList = new ArrayList<Integer>();
        private final Set<Lang> availableLangSet = new LinkedHashSet<Lang>();
        private final PhrasesBuilder phrasesBuilder = new PhrasesBuilder();
        private final AttributesBuilder attributesBuilder = new AttributesBuilder();
        private String baseIcon;

        public Builder(BaseURI baseURI) {
            this.baseURI = baseURI;
        }

        @Override
        public void addLabel(String name, Label label) {
            label = PhrasesPrimitives.checkLabel(label);
            if (label == null) {
                return;
            }
            phrasesBuilder.getPhraseBuilder(name).putLabel(label);
        }

        public void addAttribute(AttributeKey attributeKey, List<CleanedString> valueList) {
            AttributesPrimitives.addAttribute(attributesBuilder, attributeKey, valueList);
        }

        public void addAvailableLang(Lang lang) {
            availableLangSet.add(lang);
        }

        public BaseURI getBaseURI() {
            return baseURI;
        }

        public boolean hasBaseIcon() {
            return (this.baseIcon != null);
        }

        public boolean setBaseIcon(URI baseIconURI) {
            if (baseIconURI == null) {
                this.baseIcon = null;
                return true;
            }
            if (!baseIconURI.isAbsolute()) {
                return false;
            }
            if (baseIconURI.isOpaque()) {
                return false;
            }
            String scheme = baseIconURI.getScheme();
            if ((!scheme.equals("http")) && (!scheme.equals("https"))) {
                return false;
            }
            this.baseIcon = baseIconURI.toASCIIString();
            return true;
        }

        public void addCorpusCode(Integer corpusCode) {
            corpusList.add(corpusCode);
        }

        public void addThesaurusCode(Integer thesaurusCode) {
            thesaurusList.add(thesaurusCode);
        }

        public BaseData toBaseData(Integer baseCode, String scrutariSourceName) {
            List<Integer> finalCorpusList = PrimUtils.wrap(PrimUtils.toArray(corpusList));
            List<Integer> finalThesaurusList = PrimUtils.wrap(PrimUtils.toArray(thesaurusList));
            Lang[] availableLangArray = availableLangSet.toArray(new Lang[availableLangSet.size()]);
            String baseIconPart1 = null;
            String baseIconPart2 = null;
            if (baseIcon != null) {
                String param = "$LANGUI";
                int idx = baseIcon.indexOf(param);
                if (idx == -1) {
                    param = "$LANG";
                    idx = baseIcon.indexOf(param);
                }
                if (idx == -1) {
                    baseIconPart1 = baseIcon;
                    baseIconPart2 = null;
                } else {
                    baseIconPart1 = baseIcon.substring(0, idx);
                    baseIconPart2 = baseIcon.substring(idx + param.length());
                }
            }
            Phrases phrases = phrasesBuilder.toPhrases();
            Attributes attributes = attributesBuilder.toAttributes();
            return new BaseData(baseURI, baseCode, scrutariSourceName, availableLangArray, baseIconPart1, baseIconPart2, phrases, attributes, finalCorpusList, finalThesaurusList);
        }

    }

}
