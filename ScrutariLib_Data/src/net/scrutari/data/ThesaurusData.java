/* ScrutariLib_Data - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesBuilder;
import net.scrutari.datauri.ThesaurusURI;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusData implements Serializable {

    private static final long serialVersionUID = 2L;
    private final ThesaurusURI thesaurusURI;
    private final Integer thesaurusCode;
    private final Integer baseCode;
    private final Phrases phrases;
    private final Attributes attributes;
    private final List<Integer> motcleCodeList;

    private ThesaurusData(ThesaurusURI thesaurusURI, Integer thesaurusCode, Integer baseCode, Phrases phrases, Attributes attributes, List<Integer> motcleCodeList) {
        this.thesaurusURI = thesaurusURI;
        this.thesaurusCode = thesaurusCode;
        this.baseCode = baseCode;
        this.phrases = phrases;
        this.attributes = attributes;
        this.motcleCodeList = motcleCodeList;
    }

    public ThesaurusURI getThesaurusURI() {
        return thesaurusURI;
    }

    public Integer getThesaurusCode() {
        return thesaurusCode;
    }

    public Integer getBaseCode() {
        return baseCode;
    }

    public Phrases getPhrases() {
        return phrases;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public List<Integer> getMotcleCodeList() {
        return motcleCodeList;
    }

    public static ThesaurusData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        Integer thesaurusCode = primitivesReader.readInt();
        Integer baseCode = primitivesReader.readInt();
        ThesaurusURI thesaurusURI = (ThesaurusURI) URIPrimitives.readScrutariDataURI(primitivesReader);
        Phrases phrases = PhrasesPrimitives.readPhrases(primitivesReader);
        Attributes attributes = AttributesPrimitives.readAttributes(primitivesReader);
        Integer[] motcleCodeArray = primitivesReader.readIntegerArray();
        return new ThesaurusData(thesaurusURI, thesaurusCode, baseCode, phrases, attributes, PrimUtils.wrap(motcleCodeArray));
    }

    public static void toPrimitives(ThesaurusData thesaurusData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(thesaurusData.thesaurusCode);
        primitivesWriter.writeInt(thesaurusData.baseCode);
        URIPrimitives.writeScrutariDataURI(thesaurusData.thesaurusURI, primitivesWriter);
        PhrasesPrimitives.writePhrases(thesaurusData.phrases, primitivesWriter);
        AttributesPrimitives.writeAttributes(thesaurusData.attributes, primitivesWriter);
        primitivesWriter.writeIntArray(thesaurusData.motcleCodeList);
    }


    public static class Builder implements LabelConsumer {

        private final ThesaurusURI thesaurusURI;
        private final PhrasesBuilder phrasesBuilder = new PhrasesBuilder();
        private final AttributesBuilder attributesBuilder = new AttributesBuilder();
        private final List<Integer> motcleList = new ArrayList<Integer>();


        public Builder(ThesaurusURI thesaurusURI) {
            this.thesaurusURI = thesaurusURI;
        }

        public ThesaurusURI getThesaurusURI() {
            return thesaurusURI;
        }

        @Override
        public void addLabel(String name, Label label) {
            label = PhrasesPrimitives.checkLabel(label);
            if (label == null) {
                return;
            }
            phrasesBuilder.getPhraseBuilder(name).putLabel(label);
        }

        public void addAttribute(AttributeKey attributeKey, List<CleanedString> valueList) {
            AttributesPrimitives.addAttribute(attributesBuilder, attributeKey, valueList);
        }

        public void addMotcleCode(Integer motcleCode) {
            motcleList.add(motcleCode);
        }

        public ThesaurusData toThesaurusData(Integer thesaurusCode, Integer baseCode) {
            List<Integer> finalMotcleCodeList = PrimUtils.wrap(PrimUtils.toArray(motcleList));
            Phrases phrases = phrasesBuilder.toPhrases();
            Attributes attributes = attributesBuilder.toAttributes();
            return new ThesaurusData(thesaurusURI, thesaurusCode, baseCode, phrases, attributes, finalMotcleCodeList);
        }

    }

}
