/* ScrutariLib_Data - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesBuilder;
import net.scrutari.datauri.CorpusURI;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusData implements Serializable {

    private static final long serialVersionUID = 2L;
    private final CorpusURI corpusURI;
    private final Integer corpusCode;
    private final Integer baseCode;
    private final int complementCount;
    private final Phrases phrases;
    private final Attributes attributes;
    private final List<Integer> ficheCodeList;

    private CorpusData(CorpusURI corpusURI, Integer corpusCode, Integer baseCode, int complementCount, Phrases phrases, Attributes attributes, List<Integer> ficheCodeList) {
        this.corpusURI = corpusURI;
        this.corpusCode = corpusCode;
        this.baseCode = baseCode;
        this.complementCount = complementCount;
        this.phrases = phrases;
        this.attributes = attributes;
        this.ficheCodeList = ficheCodeList;
    }

    public CorpusURI getCorpusURI() {
        return corpusURI;
    }

    /**
     * Retourne le nombre maximum de champs complémentaires.
     */
    public int getComplementMaxNumber() {
        return complementCount;
    }

    /**
     * Retourne le code du corpus.
     */
    public Integer getCorpusCode() {
        return corpusCode;
    }

    /**
     * Retourne le code de la base.
     */
    public Integer getBaseCode() {
        return baseCode;
    }

    public Phrases getPhrases() {
        return phrases;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public List<Integer> getFicheCodeList() {
        return ficheCodeList;
    }


    public static CorpusData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        Integer corpusCode = primitivesReader.readInt();
        Integer baseCode = primitivesReader.readInt();
        CorpusURI corpusURI = (CorpusURI) URIPrimitives.readScrutariDataURI(primitivesReader);
        int complementCount = primitivesReader.readInt();
        Phrases phrases = PhrasesPrimitives.readPhrases(primitivesReader);
        Attributes attributes = AttributesPrimitives.readAttributes(primitivesReader);
        Integer[] ficheCodeArray = primitivesReader.readIntegerArray();
        return new CorpusData(corpusURI, corpusCode, baseCode, complementCount, phrases, attributes, PrimUtils.wrap(ficheCodeArray));
    }

    public static void toPrimitives(CorpusData corpusData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(corpusData.corpusCode);
        primitivesWriter.writeInt(corpusData.baseCode);
        URIPrimitives.writeScrutariDataURI(corpusData.corpusURI, primitivesWriter);
        int complementCount = corpusData.complementCount;
        primitivesWriter.writeInt(complementCount);
        PhrasesPrimitives.writePhrases(corpusData.phrases, primitivesWriter);
        AttributesPrimitives.writeAttributes(corpusData.attributes, primitivesWriter);
        primitivesWriter.writeIntArray(corpusData.ficheCodeList);
    }


    public static class Builder implements LabelConsumer {

        private final CorpusURI corpusURI;
        private final PhrasesBuilder phrasesBuilder = new PhrasesBuilder();
        private final AttributesBuilder attributesBuilder = new AttributesBuilder();
        private final List<Integer> ficheCodeList = new ArrayList<Integer>();
        private String hrefParent;
        private int complementCount = 0;

        public Builder(CorpusURI corpusURI) {
            this.corpusURI = corpusURI;
        }

        public CorpusURI getCorpusURI() {
            return corpusURI;
        }

        @Override
        public void addLabel(String name, Label label) {
            label = PhrasesPrimitives.checkLabel(label);
            if (label == null) {
                return;
            }
            phrasesBuilder.getPhraseBuilder(name).putLabel(label);
        }

        public void addAttribute(AttributeKey attributeKey, List<CleanedString> valueList) {
            AttributesPrimitives.addAttribute(attributesBuilder, attributeKey, valueList);
        }

        public int createNewComplement() {
            complementCount++;
            return complementCount;
        }

        public int getComplementMaxNumber() {
            return complementCount;
        }

        public void addFicheCode(Integer ficheCode) {
            ficheCodeList.add(ficheCode);
        }

        public CorpusData toCorpusData(Integer corpusCode, Integer baseCode) {
            List<Integer> finalFicheCodeList = PrimUtils.wrap(PrimUtils.toArray(ficheCodeList));
            Phrases phrases = phrasesBuilder.toPhrases();
            Attributes attributes = attributesBuilder.toAttributes();
            return new CorpusData(corpusURI, corpusCode, baseCode, complementCount, phrases, attributes, finalFicheCodeList);
        }

        public String getHrefParent() {
            return hrefParent;
        }

        public boolean setHrefParent(URI hrefParentURI) {
            if (hrefParentURI == null) {
                this.hrefParent = null;
                return true;
            }
            if (!hrefParentURI.isAbsolute()) {
                return false;
            }
            if (hrefParentURI.isOpaque()) {
                return false;
            }
            String scheme = hrefParentURI.getScheme();
            if ((!scheme.equals("http")) && (!scheme.equals("https"))) {
                return false;
            }
            this.hrefParent = hrefParentURI.toASCIIString();
            return true;
        }

    }

}
