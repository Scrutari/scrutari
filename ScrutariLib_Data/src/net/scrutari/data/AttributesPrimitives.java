/* ScrutariLib_Data - Copyright (c) 2020-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.text.ParseException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.html.jsoup.TrustedHtmlFactories;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public final class AttributesPrimitives {

    private AttributesPrimitives() {

    }

    public static void writeAttributes(Attributes attributes, PrimitivesWriter primitivesWriter) throws IOException {
        int attributeLength = attributes.size();
        primitivesWriter.writeInt(attributeLength);
        for (int i = 0; i < attributeLength; i++) {
            Attribute attribute = attributes.get(i);
            AttributeKey attributeKey = attribute.getAttributeKey();
            primitivesWriter.writeString(attributeKey.toString());
            int valueLength = attribute.size();
            primitivesWriter.writeInt(valueLength);
            for (int j = 0; j < valueLength; j++) {
                primitivesWriter.writeString(attribute.get(j));
            }
        }
    }

    public static Attributes readAttributes(PrimitivesReader primitivesReader) throws IOException {
        int attributeLength = primitivesReader.readInt();
        if (attributeLength == 0) {
            return AttributeUtils.EMPTY_ATTRIBUTES;
        }
        if (attributeLength == 1) {
            Attribute attribute = readAttribute(primitivesReader);
            return AttributeUtils.toSingletonAttributes(attribute);
        }
        Attribute[] array = new Attribute[attributeLength];
        for (int i = 0; i < attributeLength; i++) {
            array[i] = readAttribute(primitivesReader);
        }
        return new ArrayAttributes(array);
    }

    public static void readAttributes(PrimitivesReader primitivesReader, AttributesBuilder attributesBuilder) throws IOException {
        int attributeLength = primitivesReader.readInt();
        if (attributeLength == 0) {
            return;
        }
        for (int i = 0; i < attributeLength; i++) {
            attributesBuilder.appendValues(readAttribute(primitivesReader));
        }
    }

    static void addAttribute(AttributesBuilder attributesBuilder, AttributeKey attributeKey, List<CleanedString> valueList) {
        List<CleanedString> filteredList = new ArrayList<CleanedString>();
        for (CleanedString cleanedString : valueList) {
            String strg = cleanedString.toString();
            String checked = TrustedHtmlFactories.TEXT.check(strg);
            if (checked.equals(strg)) {
                filteredList.add(cleanedString);
            } else {
                cleanedString = CleanedString.newInstance(checked);
                if (cleanedString != null) {
                    filteredList.add(cleanedString);
                }
            }
        }
        attributesBuilder.appendValues(attributeKey, filteredList);
    }

    private static Attribute readAttribute(PrimitivesReader primitivesReader) throws IOException {
        String attributeKeyString = primitivesReader.readString();
        AttributeKey attributeKey;
        try {
            attributeKey = AttributeKey.parse(attributeKeyString);
        } catch (ParseException pe) {
            throw new IllegalStateException(pe);
        }
        int valueLength = primitivesReader.readInt();
        if (valueLength < 1) {
            throw new IllegalStateException("valCount < 1");
        }
        if (valueLength == 1) {
            return new SingletonAttribute(attributeKey, primitivesReader.readString());
        }
        String[] array = new String[valueLength];
        for (int i = 0; i < valueLength; i++) {
            array[i] = primitivesReader.readString();
        }
        return new ArrayAttribute(attributeKey, array);
    }


    private static class SingletonAttribute extends AbstractList<String> implements Attribute {

        private final AttributeKey key;
        private final String value;

        private SingletonAttribute(AttributeKey key, String value) {
            this.key = key;
            this.value = value;
        }

        @Override
        public AttributeKey getAttributeKey() {
            return key;
        }

        @Override
        public String getFirstValue() {
            return value;
        }

        @Override
        public int size() {
            return 1;
        }

        @Override
        public String get(int i) {
            if (i != 0) {
                throw new IndexOutOfBoundsException();
            }
            return value;
        }

    }


    private static class ArrayAttribute extends AbstractList<String> implements Attribute {

        private final AttributeKey key;
        private final String[] valueArray;

        private ArrayAttribute(AttributeKey key, String[] valueArray) {
            this.key = key;
            this.valueArray = valueArray;
        }

        @Override
        public AttributeKey getAttributeKey() {
            return key;
        }

        @Override
        public String getFirstValue() {
            return valueArray[0];
        }

        @Override
        public int size() {
            return valueArray.length;
        }

        @Override
        public String get(int i) {
            return valueArray[i];
        }

    }


    private static class ArrayAttributes extends AbstractList<Attribute> implements Attributes {

        private final Attribute[] attributeArray;

        private ArrayAttributes(Attribute[] attributeArray) {
            this.attributeArray = attributeArray;
        }

        @Override
        public Attribute getAttribute(AttributeKey key) {
            for (Attribute attribute : attributeArray) {
                if (attribute.getAttributeKey().equals(key)) {
                    return attribute;
                }
            }
            return null;
        }

        @Override
        public int size() {
            return attributeArray.length;
        }

        @Override
        public Attribute get(int i) {
            return attributeArray[i];
        }

    }

}
