/* ScrutariLib_Data - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;


/**
 *
 * @author Vincent Calame
 */
public final class IndexationData implements Serializable {

    private static final long serialVersionUID = 3L;
    private final Integer motcleCode;
    private final Integer ficheCode;
    private final int poids;

    public IndexationData(Integer ficheCode, Integer motcleCode, int poids) {
        this.ficheCode = ficheCode;
        this.motcleCode = motcleCode;
        this.poids = poids;
    }

    public Integer getMotcleCode() {
        return motcleCode;
    }

    public Integer getFicheCode() {
        return ficheCode;
    }

    public int getPoids() {
        return poids;
    }

    public static IndexationData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        int[] codes = primitivesReader.readInts(3);
        Integer ficheCode = codes[0];
        Integer motcleCode = codes[1];
        int poids = codes[2];
        return new IndexationData(ficheCode, motcleCode, poids);
    }

    public static void toPrimitives(IndexationData indexationData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(indexationData.ficheCode);
        primitivesWriter.writeInt(indexationData.motcleCode);
        primitivesWriter.writeInt(indexationData.poids);
    }

}
