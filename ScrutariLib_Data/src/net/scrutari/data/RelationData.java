/* ScrutariLib_Data - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public class RelationData implements Serializable {

    private static final long serialVersionUID = 1L;
    private final String type;
    private final List<Member> memberList;
    private final Attributes attributes;


    private RelationData(String type, List<Member> memberList, Attributes attributes) {
        this.type = type;
        this.memberList = memberList;
        this.attributes = attributes;
    }

    public String getType() {
        return type;
    }

    public List<Member> getMemberList() {
        return memberList;
    }

    public Attributes getAttributes() {
        return attributes;
    }


    public static class Member {

        private final Integer code;
        private final String role;

        private Member(Integer code, String role) {
            this.code = code;
            this.role = role;
        }

        public Integer getCode() {
            return code;
        }

        public String getRole() {
            return role;
        }

    }

    public static RelationData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        String type = primitivesReader.readString().intern();
        Attributes attributes = AttributesPrimitives.readAttributes(primitivesReader);
        int size = primitivesReader.readInt();
        Member[] array = new Member[size];
        for (int i = 0; i < size; i++) {
            Integer code = primitivesReader.readInt();
            String role = primitivesReader.readString().intern();
            array[i] = new Member(code, role);
        }
        return new RelationData(type, new InternalMemberList(array), attributes);
    }

    public static void toPrimitives(RelationData relationData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeString(relationData.getType());
        AttributesPrimitives.writeAttributes(relationData.attributes, primitivesWriter);
        int size = relationData.memberList.size();
        primitivesWriter.writeInt(size);
        for (int i = 0; i < size; i++) {
            Member member = relationData.memberList.get(i);
            primitivesWriter.writeInt(member.getCode());
            primitivesWriter.writeString(member.getRole());
        }
    }


    private static class InternalMemberList extends AbstractList<Member> implements RandomAccess {

        private final Member[] array;

        private InternalMemberList(Member[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Member get(int i) {
            return array[i];
        }

    }


    public static class Builder {

        private final String type;
        private final AttributesBuilder attributesBuilder = new AttributesBuilder();
        private final List<Member> memberList = new ArrayList<Member>();

        public Builder(String type) {
            this.type = type;
        }

        public void addAttribute(AttributeKey attributeKey, List<CleanedString> valueList) {
            AttributesPrimitives.addAttribute(attributesBuilder, attributeKey, valueList);
        }

        public void addMember(Integer code, String role) {
            if (role == null) {
                role = "";
            }
            memberList.add(new Member(code, role));
        }

        public RelationData toRelationData() {
            Attributes attributes = attributesBuilder.toAttributes();
            int size = memberList.size();
            List<Member> finalMemberList = new InternalMemberList(memberList.toArray(new Member[size]));
            return new RelationData(type, finalMemberList, attributes);
        }

    }

}
