/* ScrutariLib_Data - Copyright (c) 2005-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.html.jsoup.TrustedHtmlFactories;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.datauri.FicheURI;


/**
 *
 * @author Vincent Calame
 */
public final class FicheData implements Serializable {

    private static final long serialVersionUID = 2L;
    public final static Lang UNDETERMINED_LANG = Lang.build("und");
    private static final int DATE_NULL = 0x80000000;
    private final Integer ficheCode;
    private final Integer corpusCode;
    private final Integer baseCode;
    private final Lang lang;
    private final FuzzyDate date;
    private final String titre;
    private final String soustitre;
    private final String hrefPart1;
    private final String hrefPart2;
    private final String[] complements;
    private final FicheURI ficheURI;
    private final String ficheIcon;
    private final boolean withGeo;
    private final DegreDecimal latitude;
    private final DegreDecimal longitude;
    private final Attributes attributes;

    private FicheData(FicheURI ficheURI, Integer ficheCode, Integer corpusCode, Integer baseCode, Lang lang, FuzzyDate date, String titre, String soustitre, String hrefPart1, String hrefPart2, String[] complements, String ficheIcon, boolean withGeo, DegreDecimal latitude, DegreDecimal longitude, Attributes attributes) {
        this.corpusCode = corpusCode;
        this.ficheCode = ficheCode;
        this.baseCode = baseCode;
        this.lang = lang;
        this.date = date;
        this.titre = titre;
        this.soustitre = soustitre;
        this.hrefPart1 = hrefPart1;
        this.hrefPart2 = hrefPart2;
        this.complements = complements;
        this.ficheURI = ficheURI;
        this.ficheIcon = ficheIcon;
        this.withGeo = withGeo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.attributes = attributes;
    }

    public Integer getFicheCode() {
        return ficheCode;
    }

    public Integer getCorpusCode() {
        return corpusCode;
    }

    public Integer getBaseCode() {
        return baseCode;
    }

    /**
     * N'est jamais nul.
     */
    public Lang getLang() {
        return lang;
    }

    public int getComplementMaxNumber() {
        if (complements == null) {
            return 0;
        }
        return complements.length;
    }

    public FicheURI getFicheURI() {
        return ficheURI;
    }

    /**
     * Peut avoir la valeur nulle mais n'est pas de longueur nulle ou composé
     * d'espace blanc.
     */
    @Nullable
    public String getTitre() {
        return titre;
    }

    /**
     * Peut avoir la valeur nulle mais n'est pas de longueur nulle ou composé
     * d'espace blanc.
     */
    @Nullable
    public String getSoustitre() {
        return soustitre;
    }

    @Nullable
    public String getHref(@Nullable Lang workingLang) {
        if (hrefPart2 == null) {
            return hrefPart1;
        }
        StringBuilder buf = new StringBuilder(hrefPart1.length() + hrefPart2.length() + 10);
        buf.append(hrefPart1);
        if (workingLang != null) {
            buf.append(workingLang.toString());
        } else {
            buf.append("$LANG");
        }
        buf.append(hrefPart2);
        return buf.toString();
    }

    @Nullable
    public FuzzyDate getDate() {
        return date;
    }

    @Nullable
    public String getFicheIcon() {
        return ficheIcon;
    }

    public boolean isWithGeo() {
        return withGeo;
    }

    @Nullable
    public DegreDecimal getLatitude() {
        return latitude;
    }

    @Nullable
    public DegreDecimal getLongitude() {
        return longitude;
    }

    /**
     * Attention, les compléments sont numérotés à partir de 1.
     */
    @Nullable
    public String getComplementByNumber(int num) {
        if (complements == null) {
            throw new IndexOutOfBoundsException();
        }
        return complements[num - 1];
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public static FicheData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        Integer ficheCode = primitivesReader.readInt();
        Integer corpusCode = primitivesReader.readInt();
        Integer baseCode = primitivesReader.readInt();
        FuzzyDate date = null;
        int dateInt = primitivesReader.readInt();
        if (dateInt > 0) {
            date = FuzzyDate.fromInt(dateInt);
        }
        Lang lang = Lang.build(primitivesReader.readString());
        FicheURI ficheURI = (FicheURI) URIPrimitives.readScrutariDataURI(primitivesReader);
        String titre = primitivesReader.readString();
        String soustitre = primitivesReader.readString();
        String hrefPart1 = primitivesReader.readString();
        String hrefPart2 = primitivesReader.readString();
        String ficheIcon = primitivesReader.readString();
        boolean withGeoPoint = primitivesReader.readBoolean();
        DegreDecimal latitude = null;
        DegreDecimal longitude = null;
        if (withGeoPoint) {
            latitude = readDegreDecimal(primitivesReader);
            longitude = readDegreDecimal(primitivesReader);
        }
        String[] complements = null;
        int length = primitivesReader.readInt();
        if (length > 0) {
            complements = new String[length];
            for (int i = 0; i < length; i++) {
                complements[i] = primitivesReader.readString();
            }
        }
        Attributes attributes = AttributesPrimitives.readAttributes(primitivesReader);
        return new FicheData(ficheURI, ficheCode, corpusCode, baseCode, lang, date, titre, soustitre, hrefPart1, hrefPart2, complements, ficheIcon, withGeoPoint, latitude, longitude, attributes);
    }

    public static Lang getLangFromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        primitivesReader.skipInts(4);
        return Lang.build(primitivesReader.readString());
    }

    public static FuzzyDate getDateFromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        primitivesReader.skipInts(3);
        FuzzyDate date = null;
        int dateInt = primitivesReader.readInt();
        if (dateInt > 0) {
            date = FuzzyDate.fromInt(dateInt);
        }
        return date;
    }

    public static void toPrimitives(FicheData ficheData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(ficheData.ficheCode);
        primitivesWriter.writeInt(ficheData.corpusCode);
        primitivesWriter.writeInt(ficheData.baseCode);
        if (ficheData.date == null) {
            primitivesWriter.writeInt(DATE_NULL);
        } else {
            primitivesWriter.writeInt(FuzzyDate.toInt(ficheData.date));
        }
        primitivesWriter.writeString(ficheData.lang.toString());
        URIPrimitives.writeScrutariDataURI(ficheData.ficheURI, primitivesWriter);
        primitivesWriter.writeString(ficheData.titre);
        primitivesWriter.writeString(ficheData.soustitre);
        primitivesWriter.writeString(ficheData.hrefPart1);
        primitivesWriter.writeString(ficheData.hrefPart2);
        primitivesWriter.writeString(ficheData.ficheIcon);
        primitivesWriter.writeBoolean(ficheData.withGeo);
        if (ficheData.withGeo) {
            writeDegreDecimal(primitivesWriter, ficheData.latitude);
            writeDegreDecimal(primitivesWriter, ficheData.longitude);
        }
        if (ficheData.complements == null) {
            primitivesWriter.writeInt(-1);
        } else {
            int length = ficheData.complements.length;
            primitivesWriter.writeInt(length);
            for (int i = 0; i < length; i++) {
                primitivesWriter.writeString(ficheData.complements[i]);
            }
        }
        AttributesPrimitives.writeAttributes(ficheData.attributes, primitivesWriter);
    }

    private static DegreDecimal readDegreDecimal(PrimitivesReader primitivesReader) throws IOException {
        int partieEntiere = primitivesReader.readInt();
        byte zeroLength = primitivesReader.readByte();
        int partieDecimale = primitivesReader.readInt();
        return DegreDecimal.newInstance(partieEntiere, zeroLength, partieDecimale);
    }

    private static void writeDegreDecimal(PrimitivesWriter primitivesWriter, DegreDecimal decimal) throws IOException {
        primitivesWriter.writeInt(decimal.getPartieEntiere());
        primitivesWriter.writeByte(decimal.getZeroLength());
        primitivesWriter.writeInt(decimal.getPartieDecimale());
    }


    public static class Builder {

        private final FicheURI ficheURI;
        private final List<String> complementsList;
        private final AttributesBuilder attributesBuilder = new AttributesBuilder();
        private Lang lang = UNDETERMINED_LANG;
        private FuzzyDate date = null;
        private int complementMaxNumber = 0;
        private String titre;
        private String soustitre;
        private String href;
        private String ficheIcon;
        private boolean withGeo;
        private DegreDecimal latitude;
        private DegreDecimal longitude;

        /**
         * Construit une instance de Builder pour la fiche d'URI
         * <code>ficheURI</code> en précisant le nombre maximum de champs
         * complémentaires qu'est susceptible de contenir la fiche.
         */
        public Builder(FicheURI ficheURI, int complementMaxNumber) {
            this.ficheURI = ficheURI;
            this.complementMaxNumber = complementMaxNumber;
            if (complementMaxNumber > 0) {
                this.complementsList = new ArrayList<String>();
            } else {
                this.complementsList = null;
            }
        }

        /**
         * Modifie la langue de la fiche
         */
        public void setLang(Lang lang) {
            if (lang == null) {
                this.lang = UNDETERMINED_LANG;
            } else {
                this.lang = lang;
            }
        }

        /**
         * Modifie la date de la fiche.
         */
        public void setDate(FuzzyDate date) {
            this.date = date;
        }

        /**
         * Modifie le titre de la fiche.
         */
        public void setTitre(String titre) {
            this.titre = checkString(titre);
        }

        /**
         * Modifie le sous-titre de la fiche
         */
        public void setSoustitre(String soustitre) {
            this.soustitre = checkString(soustitre);
        }

        /**
         * Modifie l'hyperlien de la fiche.
         */
        public boolean setHref(URI hrefURI) {
            if (hrefURI == null) {
                this.href = null;
                return true;
            }
            if (!hrefURI.isAbsolute()) {
                this.href = hrefURI.toASCIIString();
                return true;
            }
            if (hrefURI.isOpaque()) {
                return false;
            }
            String scheme = hrefURI.getScheme();
            if ((!scheme.equals("http")) && (!scheme.equals("https"))) {
                return false;
            }
            this.href = hrefURI.toASCIIString();
            return true;
        }

        public boolean setFicheIcon(URI ficheIconURI) {
            if (ficheIconURI == null) {
                this.ficheIcon = null;
                return true;
            }
            if (!ficheIconURI.isAbsolute()) {
                return false;
            }
            if (ficheIconURI.isOpaque()) {
                return false;
            }
            String scheme = ficheIconURI.getScheme();
            if ((!scheme.equals("http")) && (!scheme.equals("https"))) {
                return false;
            }
            this.ficheIcon = ficheIconURI.toASCIIString();
            return true;
        }

        public boolean setGeoloc(String latitudeString, String longitudeString) {
            latitudeString = checkString(latitudeString);
            longitudeString = checkString(longitudeString);
            if ((latitudeString != null) && (longitudeString != null)) {
                try {
                    this.latitude = DegreDecimal.newInstance(StringUtils.parseDecimal(latitudeString));
                    this.longitude = DegreDecimal.newInstance(StringUtils.parseDecimal(longitudeString));
                    this.withGeo = true;
                    return true;
                } catch (NumberFormatException nfe) {
                    this.latitude = null;
                    this.longitude = null;
                    this.withGeo = false;
                    return false;
                }
            } else {
                return false;
            }
        }

        public Lang getLang() {
            return lang;
        }

        /**
         * Retourne l'URI de la fiche.
         */
        public FicheURI getFicheURI() {
            return ficheURI;
        }

        /**
         * Ajoute la valeur d'un champ complémentaire. L'ordre d'ajout des
         * champs détermine l'ordre final des champs dans l'instance de
         * FicheData issue de l'objet FicheInfoBuffer. Cette méthode retourne
         * false si le nombre maximum de champs est atteint.
         */
        public boolean addComplement(String s) {
            if (complementMaxNumber == 0) {
                return false;
            }
            if (complementsList.size() == complementMaxNumber) {
                return false;
            }
            complementsList.add(checkString(s));
            return true;
        }

        public void addAttribute(AttributeKey attributeKey, List<CleanedString> valueList) {
            AttributesPrimitives.addAttribute(attributesBuilder, attributeKey, valueList);
        }

        public void validate(DataValidator dataValidator) throws DataValidationException {
            if ((!withGeo) && (dataValidator.isMandatory(DataFieldKey.GEOLOC))) {
                throw new DataValidationException();
            }
            soustitre = dataValidator.checkLength(DataFieldKey.SOUSTITRE, soustitre);
        }

        /**
         * Construit une instance de FicheData à partir des données de l'objet
         * Builder.
         */
        public FicheData toFicheData(Integer ficheCode, String hrefParent, Integer corpusCode, Integer baseCode) throws DataValidationException {
            if ((href != null) && (hrefParent != null)) {
                if ((!(href.startsWith("http://"))) || (!(href.startsWith("https://")))) {
                    href = hrefParent + href;
                }
            }
            String hrefPart1 = null;
            String hrefPart2 = null;
            if (href != null) {
                String param = "$LANGUI";
                int idx = href.indexOf(param);
                if (idx == -1) {
                    param = "$LANG";
                    idx = href.indexOf(param);
                }
                if (idx == -1) {
                    hrefPart1 = href;
                    hrefPart2 = null;
                } else {
                    hrefPart1 = href.substring(0, idx);
                    hrefPart2 = href.substring(idx + param.length());
                }
            }
            String[] complements = null;
            if (complementMaxNumber > 0) {
                complements = new String[complementMaxNumber];
                for (int i = 0; i < complementsList.size(); i++) {
                    complements[i] = complementsList.get(i);
                }
            }
            Attributes attributes = attributesBuilder.toAttributes();
            return new FicheData(ficheURI, ficheCode, corpusCode, baseCode, lang, date, titre, soustitre, hrefPart1, hrefPart2, complements, ficheIcon, withGeo, latitude, longitude, attributes);
        }


        private String checkString(String s) {
            if (s == null) {
                return null;
            }
            s = TrustedHtmlFactories.TEXT.check(s);
            s = StringUtils.cleanString(s);
            if (s.length() == 0) {
                return null;
            }
            return s;
        }

    }

}
