/* ScrutariLib_Data - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import net.mapeadores.util.html.jsoup.TrustedHtmlFactories;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.Phrase;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class PhrasesPrimitives {

    private PhrasesPrimitives() {

    }

    public static void writePhrases(Phrases phrases, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(phrases.size());
        for (Phrase phrase : phrases) {
            primitivesWriter.writeString(phrase.getName());
            primitivesWriter.writeInt(phrase.size());
            for (Label label : phrase) {
                primitivesWriter.writeString(label.getLang().toString());
                primitivesWriter.writeString(label.getLabelString());
            }
        }
    }

    public static Phrases readPhrases(PrimitivesReader primitivesReader) throws IOException {
        int phraseCount = primitivesReader.readInt();
        if (phraseCount == 0) {
            return LabelUtils.EMPTY_PHRASES;
        }
        PhrasesBuilder phraseBuilder = new PhrasesBuilder();
        for (int i = 0; i < phraseCount; i++) {
            String phraseName = primitivesReader.readString();
            LabelChangeBuilder labelHolderBuilder = phraseBuilder.getPhraseBuilder(phraseName);
            int labelCount = primitivesReader.readInt();
            for (int j = 0; j < labelCount; j++) {
                Lang lang = Lang.build(primitivesReader.readString());
                labelHolderBuilder.putLabel(lang, primitivesReader.readString());
            }
        }
        return phraseBuilder.toPhrases();
    }

    static Label toLabel(Lang lang, String labelString) {
        return new DataLabel(lang, labelString);
    }

    static Label checkLabel(Label label) {
        if (label == null) {
            return null;
        }
        String labelString = label.getLabelString();
        String checkedString = TrustedHtmlFactories.TEXT.check(labelString);
        if (labelString.equals(checkedString)) {
            return label;
        } else {
            CleanedString cs = CleanedString.newInstance(checkedString);
            if (cs != null) {
                return new DataLabel(label.getLang(), cs.toString());
            } else {
                return null;
            }
        }
    }


    private static class DataLabel implements Label {

        private final String labelString;
        private final Lang lang;

        private DataLabel(Lang lang, String labelString) {
            this.lang = lang;
            this.labelString = labelString;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public String getLabelString() {
            return labelString;
        }

    }


}
