/* ScrutariLib_Data - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.text.StringChecker;
import net.mapeadores.util.text.StringSplitter;


/**
 *
 * @author Vincent Calame
 */
public interface DataValidator {

    public final static short MANDATORY_VALIDATION = 1;
    public final static short LENGTH_VALIDATION = 2;
    public final static short UNIQUE_VALIDATION = 3;
    public final static short FORMAT_VALIDATION = 4;
    public final static short SPLIT_VALIDATION = 5;

    public boolean isMandatory(DataFieldKey dataFieldKey);

    public String checkLength(DataFieldKey dataFieldKey, String value);

    public boolean isUnique(AttributeKey attributeKey);

    @Nullable
    public StringChecker getFormatChecker(AttributeKey attributeKey);

    @Nullable
    public StringSplitter getSplitter(AttributeKey attributeKey);

    public List<DataFieldKey> getDataFieldKeyList(short concernedValidation);

}
