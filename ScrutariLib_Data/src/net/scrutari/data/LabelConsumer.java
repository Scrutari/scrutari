/* ScrutariLib_Data - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
@FunctionalInterface
public interface LabelConsumer {

    public void addLabel(String name, Label label);

}
