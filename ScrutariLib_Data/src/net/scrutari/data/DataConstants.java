/* ScrutariLib_Data - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;


/**
 *
 * @author Vincent Calame
 */
public interface DataConstants {

    public final static String BASE_TITLE = "title";
    public final static String BASE_LONGTITLE = "longtitle";
    public final static String THESAURUS_TITLE = "title";
    public final static String CORPUS_TITLE = "title";
    public final static String CORPUS_FICHE = "fiche";
    public final static String CORPUS_COMPLEMENTPREFIX = "complement_";

}
