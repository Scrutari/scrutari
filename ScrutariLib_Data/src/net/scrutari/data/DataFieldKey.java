/* ScrutariLib_Data - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public final class DataFieldKey {

    public final static DataFieldKey SOUSTITRE = new DataFieldKey("soustitre");
    public final static DataFieldKey GEOLOC = new DataFieldKey("geoloc");
    private final String keyString;
    private final AttributeKey attributeKey;

    private DataFieldKey(String keyString) {
        this.keyString = keyString;
        this.attributeKey = null;
    }

    public DataFieldKey(AttributeKey attributeKey) {
        this.keyString = attributeKey.toString();
        this.attributeKey = attributeKey;
    }

    public AttributeKey getAttributeKey() {
        return attributeKey;
    }

    @Override
    public String toString() {
        return keyString;
    }

    @Override
    public int hashCode() {
        return keyString.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        DataFieldKey otherDataFieldKey = (DataFieldKey) other;
        return otherDataFieldKey.keyString.equals(this.keyString);
    }

}
