/* ScrutariLib_Data - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.IOException;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.scrutari.datauri.MotcleURI;


/**
 *
 * @author Vincent Calame
 */
public final class MotcleData implements Serializable {

    private static final long serialVersionUID = 2L;
    private final MotcleURI motcleURI;
    private final Integer motcleCode;
    private final Integer thesaurusCode;
    private final Integer baseCode;
    private final ArrayLabels arrayLabels;
    private final Attributes attributes;

    private MotcleData(MotcleURI motcleURI, Integer motcleCode, Integer thesaurusCode, Integer baseCode, ArrayLabels arrayLabels, Attributes attributes) {
        this.motcleURI = motcleURI;
        this.motcleCode = motcleCode;
        this.thesaurusCode = thesaurusCode;
        this.baseCode = baseCode;
        this.arrayLabels = arrayLabels;
        this.attributes = attributes;
    }

    public Integer getMotcleCode() {
        return motcleCode;
    }

    public MotcleURI getMotcleURI() {
        return motcleURI;
    }

    public Integer getThesaurusCode() {
        return thesaurusCode;
    }

    public Integer getBaseCode() {
        return baseCode;
    }

    public Labels getLabels() {
        return arrayLabels;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public static MotcleData fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        Integer motcleCode = primitivesReader.readInt();
        Integer thesaurusCode = primitivesReader.readInt();
        Integer baseCode = primitivesReader.readInt();
        MotcleURI motcleURI = (MotcleURI) URIPrimitives.readScrutariDataURI(primitivesReader);
        int labelLength = primitivesReader.readInt();
        Label[] array = new Label[labelLength];
        for (int i = 0; i < labelLength; i++) {
            Lang lang = Lang.build(primitivesReader.readString());
            String labelString = primitivesReader.readString();
            array[i] = PhrasesPrimitives.toLabel(lang, labelString);
        }
        Attributes attributes = AttributesPrimitives.readAttributes(primitivesReader);
        return new MotcleData(motcleURI, motcleCode, thesaurusCode, baseCode, new ArrayLabels(array), attributes);
    }

    public static void toPrimitives(MotcleData motcleData, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(motcleData.motcleCode);
        primitivesWriter.writeInt(motcleData.thesaurusCode);
        primitivesWriter.writeInt(motcleData.baseCode);
        URIPrimitives.writeScrutariDataURI(motcleData.getMotcleURI(), primitivesWriter);
        Label[] labelArray = motcleData.arrayLabels.labelArray;
        int length = labelArray.length;
        primitivesWriter.writeInt(length);
        for (int i = 0; i < length; i++) {
            Label label = labelArray[i];
            primitivesWriter.writeString(label.getLang().toString());
            primitivesWriter.writeString(label.getLabelString());
        }
        AttributesPrimitives.writeAttributes(motcleData.attributes, primitivesWriter);
    }


    public static class Builder {

        private final MotcleURI motcleURI;
        private final List<Label> labelList = new ArrayList<Label>();
        private final AttributesBuilder attributesBuilder = new AttributesBuilder();

        /**
         * Construit une instance de MotcleBuffer pour le mot-clé d'URI
         * <code>motcleURI</code>.
         */
        public Builder(MotcleURI motcleURI) {
            this.motcleURI = motcleURI;
        }

        /**
         * Retourne l'URI du mot-clé.
         */
        public MotcleURI getMotcleURI() {
            return motcleURI;
        }

        /**
         * Ajoute une instance de l'interface Label. S'il existe déjà un objet
         * Label pour la même que le libellé en argument, ce premier libellé est
         * remplacé.
         */
        public void addLabel(Label label) {
            label = PhrasesPrimitives.checkLabel(label);
            if (label == null) {
                return;
            }
            labelList.add(label);
        }

        public void addAttribute(AttributeKey attributeKey, List<CleanedString> valueList) {
            AttributesPrimitives.addAttribute(attributesBuilder, attributeKey, valueList);
        }

        /**
         * Construit une instance de MotcleData à partir des données de l'objet
         * MotcleInfoBuffer.
         */
        public MotcleData toMotcleData(Integer motcleCode, Integer thesaurusCode, Integer baseCode) {
            int size = labelList.size();
            Label[] labelArray = new Label[size];
            for (int i = 0; i < size; i++) {
                labelArray[i] = labelList.get(i);
            }
            Attributes attributes = attributesBuilder.toAttributes();
            return new MotcleData(motcleURI, motcleCode, thesaurusCode, baseCode, new ArrayLabels(labelArray), attributes);
        }

    }


    private static class ArrayLabels extends AbstractList<Label> implements Labels {

        private final Label[] labelArray;

        private ArrayLabels(Label[] labelArray) {
            this.labelArray = labelArray;
        }

        @Override
        public Label getLabel(Lang lang) {
            for (Label label : labelArray) {
                if (label.getLang().equals(lang)) {
                    return label;
                }
            }
            return null;
        }

        @Override
        public int size() {
            return labelArray.length;
        }

        @Override
        public Label get(int index) {
            return labelArray[index];
        }

    }

}
