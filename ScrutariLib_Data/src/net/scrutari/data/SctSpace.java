/* ScrutariLib_Data - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeDefBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;
import net.mapeadores.util.exceptions.InternalResourceException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelUtils;


/**
 *
 * @author Vincent Calame
 */
public final class SctSpace {

    public final static String INLINE_TYPE = "inline";
    public final static String LIST_TYPE = "list";
    public final static String BLOCK_TYPE = "block";
    public final static String FORMATTYPE_DEFOBJ = "format_type";
    public final static String GROUPNAME_DEFOBJ = "group_name";
    public final static CheckedNameSpace SCT_NAMESPACE = CheckedNameSpace.build("sct");
    public final static AttributeKey AUTHORS_KEY = AttributeKey.build(SCT_NAMESPACE, "authors");
    public final static AttributeKey TAGS_KEY = AttributeKey.build(SCT_NAMESPACE, "tags");
    public final static AttributeKey THUMBNAIL_KEY = AttributeKey.build(SCT_NAMESPACE, "thumbnail");
    public final static AttributeKey WEBSITE_KEY = AttributeKey.build(SCT_NAMESPACE, "website");
    public final static AttributeKey ALIAS_KEY = AttributeKey.build(SCT_NAMESPACE, "alias");
    private final static Map<AttributeKey, AttributeDef> defaultDefMap = new HashMap<AttributeKey, AttributeDef>();

    static {
        initAttributeDef(AUTHORS_KEY, INLINE_TYPE);
        initAttributeDef(TAGS_KEY, INLINE_TYPE);
        initAttributeDef(THUMBNAIL_KEY, LIST_TYPE);
        initAttributeDef(WEBSITE_KEY, INLINE_TYPE);
    }

    private SctSpace() {
    }

    public static boolean isValidFormatType(String formatType) {
        if (formatType.length() == 0) {
            return true;
        } else if (formatType.equals(INLINE_TYPE)) {
            return true;
        } else if (formatType.equals(LIST_TYPE)) {
            return true;
        } else if (formatType.equals(BLOCK_TYPE)) {
            return true;
        } else {
            return false;
        }
    }

    public static AttributeDef getDefaultAttributeDef(AttributeKey attributeKey) {
        return defaultDefMap.get(attributeKey);
    }

    public static String getFormatType(AttributeDef attributeDef) {
        Object obj = attributeDef.getDefObject(FORMATTYPE_DEFOBJ);
        if (obj == null) {
            return INLINE_TYPE;
        }
        String formatType = obj.toString();
        if (formatType.isEmpty()) {
            return INLINE_TYPE;
        }
        return formatType;
    }

    public static boolean isBlock(AttributeDef attributeDef) {
        if (attributeDef == null) {
            return false;
        }
        Object obj = attributeDef.getDefObject(FORMATTYPE_DEFOBJ);
        if (obj == null) {
            return false;
        }
        return (obj.equals(BLOCK_TYPE));
    }

    public static boolean ownsTo(AttributeDef attributeDef, String groupName) {
        if (attributeDef == null) {
            return false;
        }
        Object obj = attributeDef.getDefObject(GROUPNAME_DEFOBJ);
        if (obj == null) {
            return false;
        }
        return (obj.equals(groupName));
    }

    private static void initAttributeDef(AttributeKey attributeKey, String formatType) {
        AttributeDefBuilder builder = new AttributeDefBuilder(attributeKey);
        builder.putDefObject(FORMATTYPE_DEFOBJ, formatType);
        try {
            InputStream is = SctSpace.class.getResourceAsStream("l10n/" + attributeKey.toString() + ".csv");
            BufferedReader bufReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            String ligne;
            while ((ligne = bufReader.readLine()) != null) {
                if (ligne.length() > 0) {
                    int idx = ligne.indexOf(',');
                    if (idx != -1) {
                        Lang lang = Lang.parse(ligne.substring(0, idx));
                        CleanedString cs = CleanedString.newInstance(ligne.substring(idx + 1));
                        if (cs != null) {
                            builder.putLabel(LabelUtils.toLabel(lang, cs));
                        }
                    }
                }
            }
        } catch (IOException | ParseException e) {
            throw new InternalResourceException(e);
        }
        defaultDefMap.put(attributeKey, builder.toAttributeDef());
    }


}
