/* SctServer - Copyright (c) 2016-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import fr.exemole.sctserver.ResourceReference;
import fr.exemole.sctserver.api.EngineContext;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.docstream.ClassResourceDocStream;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreferenceBuilder;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.mimetype.MimeTypeUtils;
import net.mapeadores.util.text.RelativePath;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public final class EngineUtils {


    private EngineUtils() {

    }

    private static DocStream getDistDocStream(RelativePath relativePath) {
        ClassResourceDocStream classResourceDocStream = ClassResourceDocStream.newInstance(ResourceReference.class, "resources/" + relativePath.toString());
        if (classResourceDocStream != null) {
            String mimeType = MimeTypeUtils.getMimeType(MimeTypeUtils.DEFAULT_RESOLVER, relativePath.getLastName());
            classResourceDocStream.setMimeType(mimeType);
            return classResourceDocStream;
        } else {
            return null;
        }
    }

    public static Lang getDefaultLang(ScrutariSession scrutariSession) {
        return EngineUtils.getDefaultLang(scrutariSession.getEngine());
    }

    public static Lang getDefaultLang(SctEngine engine) {
        Lang lang = engine.getEngineMetadata().getDefaultLang();
        if (lang == null) {
            lang = engine.getEngineContext().getWebappDefaultLang();
        }
        return lang;
    }

    public static MessageLocalisation getMessageLocalisation(SctEngine engine, Lang lang) {
        LangPreferenceBuilder langPreferenceBuilder = new LangPreferenceBuilder();
        if (lang != null) {
            langPreferenceBuilder.addLang(lang);
        }
        Lang defaultLang = engine.getEngineMetadata().getDefaultLang();
        if (defaultLang != null) {
            langPreferenceBuilder.addLang(defaultLang);
        }
        langPreferenceBuilder.addLang(engine.getEngineContext().getWebappDefaultLang());
        return engine.getEngineContext().getMessageLocalisation(langPreferenceBuilder.toLangPreference());
    }

    public static MessageLocalisation getMessageLocalisation(EngineContext engineContext, Lang lang) {
        LangPreferenceBuilder langPreferenceBuilder = new LangPreferenceBuilder();
        if (lang != null) {
            langPreferenceBuilder.addLang(lang);
        }
        langPreferenceBuilder.addLang(engineContext.getWebappDefaultLang());
        return engineContext.getMessageLocalisation(langPreferenceBuilder.toLangPreference());
    }

    public static List<ScrutariDBName> wrap(ScrutariDBName[] array) {
        return new ScrutariDBNameList(array);
    }


    private static class ScrutariDBNameList extends AbstractList<ScrutariDBName> implements RandomAccess {

        private final ScrutariDBName[] array;

        private ScrutariDBNameList(ScrutariDBName[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ScrutariDBName get(int index) {
            return array[index];
        }

    }

}
