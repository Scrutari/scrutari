/* SctServer - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.DataConstants;
import net.scrutari.data.ThesaurusData;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusWrapper {

    private final ThesaurusData thesaurusData;
    private final String labelString;

    private ThesaurusWrapper(ThesaurusData thesaurusData, String labelString) {
        this.thesaurusData = thesaurusData;
        this.labelString = labelString;
    }

    public int getThesaurusCode() {
        return thesaurusData.getThesaurusCode();
    }

    public String getLabelString() {
        return labelString;
    }

    public static ThesaurusWrapper wrapThesaurus(ThesaurusData thesaurusData, Lang lang) {
        String labelString = LabelUtils.seekLabelString(thesaurusData.getPhrases(), DataConstants.THESAURUS_TITLE, lang, thesaurusData.getThesaurusURI().getThesaurusName());
        return new ThesaurusWrapper(thesaurusData, labelString);
    }

}
