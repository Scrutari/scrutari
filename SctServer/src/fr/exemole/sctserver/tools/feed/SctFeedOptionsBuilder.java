/* SctServer - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import fr.exemole.sctserver.api.feed.SctFeedOptions;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class SctFeedOptionsBuilder {

    private final short type;
    private final Lang lang;
    private final RequestMap requestMap;
    private final String selfUrl;
    private int optionMask = 0;

    public SctFeedOptionsBuilder(short type, Lang lang, RequestMap requestMap, String selfUrl) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        if (selfUrl == null) {
            throw new IllegalArgumentException("selfUrl is null");
        }
        this.type = type;
        this.lang = lang;
        this.requestMap = requestMap;
        this.selfUrl = selfUrl;
    }

    public void setOption(int option, boolean value) {
        if (value) {
            optionMask = optionMask | option;
        } else {
            optionMask = optionMask & ~option;
        }
    }

    public SctFeedOptions toSctFeedOptions() {
        return new InternalSctFeedOptions(type, lang, requestMap, optionMask, selfUrl);
    }


    private static class InternalSctFeedOptions implements SctFeedOptions {

        private final short type;
        private final Lang lang;
        private final RequestMap requestMap;
        private final int optionMask;
        private final String selfUrl;

        private InternalSctFeedOptions(short type, Lang lang, RequestMap requestMap, int optionMask, String selfUrl) {
            this.type = type;
            this.lang = lang;
            this.requestMap = requestMap;
            this.optionMask = optionMask;
            this.selfUrl = selfUrl;
        }

        @Override
        public short getSctFeedType() {
            return type;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public RequestMap getRequestMap() {
            return requestMap;
        }

        @Override
        public int getOptionMask() {
            return optionMask;
        }

        @Override
        public String getSelfUrl() {
            return selfUrl;
        }

    }

}
