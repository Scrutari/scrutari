/* SctEngine - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedInfo;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import fr.exemole.sctserver.request.ParameterErrorMessageKeys;
import fr.exemole.sctserver.request.Parameters;
import static fr.exemole.sctserver.request.Parameters.Q_ID;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.request.WarningHandler;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.searchengine.FicheSearchEngine;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.tools.EligibilityBuffer;


/**
 *
 * @author Vincent Calame
 */
public final class FicheFeedEngine {

    private final static long TWO_DAYS = 2 * 24 * 60 * 60 * 1000;
    private final static DateFormat ISO_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private FicheFeedEngine() {
    }

    public static FicheFeed build(ScrutariSession scrutariSession, SctFeedInfo sctFeedInfo, DataAccess dataAccess, SctFeedOptions feedOptions) throws ErrorMessageException {
        Lang lang = feedOptions.getLang();
        RequestMap requestMap = feedOptions.getRequestMap();
        if (requestMap == null) {
            return initDefault(scrutariSession, dataAccess, sctFeedInfo, feedOptions);
        }
        QId qId = getQId(requestMap);
        if ((qId != null) && (isAllList(requestMap))) {
            FicheSearchResult ficheSearchResult = scrutariSession.getSearch(qId);
            if (ficheSearchResult != null) {
                return initAllRecent(scrutariSession, dataAccess, sctFeedInfo, feedOptions, ficheSearchResult);
            } else {
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Q_ID, qId.toString());
            }
        }
        Map<Integer, FicheWrapper> ficheWrapperMap = initWrapperMap(scrutariSession, dataAccess, sctFeedInfo, feedOptions);
        SearchOptions searchOptions = RequestMapUtils.getSearchOptions(scrutariSession, requestMap, new WarningHandler(), true);
        if (qId != null) {
            FicheSearchResult ficheSearchResult = scrutariSession.getSearch(qId);
            if (ficheSearchResult == null) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Q_ID, qId.toString());
            }
            reduce(ficheWrapperMap, ficheSearchResult);
        } else if (requestMap.getParameter(Parameters.Q) != null) {
            SearchOperation searchOperation = RequestMapUtils.getSearchOperation(requestMap, scrutariSession.getScrutariDB().getFieldRankManager(), null);
            FicheSearchResult ficheSearchResult = FicheSearchEngine.search(scrutariSession.getScrutariDB(), searchOperation, searchOptions, scrutariSession.getGlobalSearchOptions(), null);
            reduce(ficheWrapperMap, ficheSearchResult);
        } else {
            reduce(ficheWrapperMap, dataAccess, searchOptions);
        }
        return new FicheFeed(feedOptions.getLang(), ficheWrapperMap.values());
    }


    private static FicheFeed initDefault(ScrutariSession scrutariSession, DataAccess dataAccess, SctFeedInfo sctFeedInfo, SctFeedOptions feedOptions) {
        return new FicheFeed(feedOptions.getLang(), initWrapperMap(scrutariSession, dataAccess, sctFeedInfo, feedOptions).values());
    }

    private static Map<Integer, FicheWrapper> initWrapperMap(ScrutariSession scrutariSession, DataAccess dataAccess, SctFeedInfo sctFeedInfo, SctFeedOptions feedOptions) {
        Lang lang = feedOptions.getLang();
        Map<Integer, FicheWrapper> ficheWrapperMap = new LinkedHashMap<Integer, FicheWrapper>();
        WrapperHolder wrapperHolder = new WrapperHolder(dataAccess, lang, scrutariSession.getGlobalSearchOptions());
        for (ScrutariDBName scrutariDBName : sctFeedInfo.getLastScrutariDBNameList()) {
            URITree uriTree = scrutariSession.getEngine().getEngineStorage().getAddedURITree(scrutariDBName, scrutariSession.getScrutariDataURIChecker());
            if (uriTree != null) {
                for (BaseNode baseNode : uriTree.getBaseNodeList()) {
                    BaseWrapper baseWrapper = wrapperHolder.getBaseWrapper(baseNode.getBaseURI());
                    if (baseWrapper == null) {
                        continue;
                    }
                    for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
                        CorpusWrapper corpusWrapper = wrapperHolder.getCorpusWrapper(corpusNode.getCorpusURI());
                        if (corpusWrapper == null) {
                            continue;
                        }
                        for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
                            FicheURI ficheURI = ficheNode.getFicheURI();
                            FicheData ficheData = (FicheData) dataAccess.getScrutariData(ficheURI);
                            if (ficheData == null) {
                                continue;
                            }
                            ficheWrapperMap.put(ficheData.getFicheCode(), FicheWrapper.wrapFiche(ficheData, corpusWrapper, baseWrapper, scrutariDBName));
                        }
                    }
                }
            }
        }
        return ficheWrapperMap;
    }

    private static FicheFeed initAllRecent(ScrutariSession scrutariSession, DataAccess dataAccess, SctFeedInfo sctFeedInfo, SctFeedOptions feedOptions, FicheSearchResult ficheSearchResult) {
        Lang lang = feedOptions.getLang();
        List<FicheWrapper> ficheWrapperList = new ArrayList<FicheWrapper>();
        WrapperHolder wrapperHolder = new WrapperHolder(dataAccess, lang, scrutariSession.getGlobalSearchOptions());
        for (FicheSearchResultGroup group : ficheSearchResult.getFicheSearchResultGroupList()) {
            for (FicheSearchResultInfo info : group.getFicheSearchResultInfoList()) {
                Integer ficheCode = info.getCode();
                FicheData ficheData = dataAccess.getFicheData(ficheCode);
                ScrutariDBName firstAdd = dataAccess.getFicheInfo(ficheCode).getFirstAdd();
                ficheWrapperList.add(FicheWrapper.wrapFiche(ficheData, wrapperHolder.getCorpusWrapper(ficheData.getCorpusCode()), wrapperHolder.getBaseWrapper(ficheData.getBaseCode()), firstAdd));
            }
        }
        return new FicheFeed(lang, ficheWrapperList);
    }

    private static boolean isAllList(RequestMap requestMap) {
        String allString = requestMap.getParameter(Parameters.ALL);
        if (allString == null) {
            return false;
        }
        if ((allString.equals("1")) || (allString.equals("always"))) {
            return true;
        }
        synchronized (ISO_FORMAT) {
            try {
                Date date = ISO_FORMAT.parse(allString);
                return isRecent(date);
            } catch (ParseException pe) {
                return false;
            }
        }
    }


    private static boolean isRecent(Date date) {
        long time = date.getTime();
        long current = System.currentTimeMillis();
        return (current - time < TWO_DAYS);
    }

    private static QId getQId(RequestMap requestMap) throws ErrorMessageException {
        String qIdString = requestMap.getParameter(Parameters.Q_ID);
        if (qIdString == null) {
            return null;
        }
        try {
            return QId.parse(qIdString);
        } catch (ParseException pe) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.WRONG_PARAMETER_VALUE, Q_ID, qIdString);
        }
    }

    private static void reduce(Map<Integer, FicheWrapper> ficheWrapperMap, FicheSearchResult ficheSearchResult) {
        Set<Integer> codeSet = new HashSet<Integer>();
        for (FicheSearchResultGroup ficheSearchResultGroup : ficheSearchResult.getFicheSearchResultGroupList()) {
            for (FicheSearchResultInfo info : ficheSearchResultGroup.getFicheSearchResultInfoList()) {
                codeSet.add(info.getCode());
            }
        }
        for (Iterator<FicheWrapper> it = ficheWrapperMap.values().iterator(); it.hasNext();) {
            FicheWrapper ficheWrapper = it.next();
            if (!codeSet.contains(ficheWrapper.getFicheData().getFicheCode())) {
                it.remove();
            }
        }
    }

    private static void reduce(Map<Integer, FicheWrapper> ficheWrapperMap, DataAccess dataAccess, SearchOptions searchOptions) {
        EligibilityBuffer eligibilityBuffer = new EligibilityBuffer(dataAccess, searchOptions.getFicheEligibility());
        Langs langs = searchOptions.getSearchOptionsDef().getFilterLangs();
        Set<Lang> langSet = null;
        if (!langs.isEmpty()) {
            langSet = new HashSet<Lang>(langs);
        }
        Set<Integer> corpusSet = null;
        if (searchOptions.isWithCorpusCodes()) {
            corpusSet = new HashSet<Integer>(searchOptions.getCorpusCodeList());
        }
        for (Iterator<FicheWrapper> it = ficheWrapperMap.values().iterator(); it.hasNext();) {
            FicheWrapper ficheWrapper = it.next();
            boolean remove = false;
            FicheData ficheData = ficheWrapper.getFicheData();
            if (langSet != null) {
                if ((!remove) && (!langSet.contains(ficheData.getLang()))) {
                    remove = true;
                }
            }
            if (corpusSet != null) {
                if ((!remove) && (!corpusSet.contains(ficheData.getCorpusCode()))) {
                    remove = true;
                }
            }
            if (eligibilityBuffer != null) {
                if ((!remove) && (!eligibilityBuffer.acceptFiche(ficheData.getFicheCode()))) {
                    remove = true;
                }
            }
            if (remove) {
                it.remove();
            }
        }
    }


    private static class WrapperHolder {

        private final Map<Integer, BaseWrapper> baseWrapperMap = new HashMap<Integer, BaseWrapper>();
        private final Map<Integer, CorpusWrapper> corpusWrapperMap = new HashMap<Integer, CorpusWrapper>();
        private final DataAccess dataAccess;
        private final Lang lang;
        private final GlobalSearchOptions globalSearchOptions;

        private WrapperHolder(DataAccess dataAccess, Lang lang, GlobalSearchOptions globalSearchOptions) {
            this.dataAccess = dataAccess;
            this.lang = lang;
            this.globalSearchOptions = globalSearchOptions;
        }

        private BaseWrapper getBaseWrapper(BaseURI baseURI) {
            BaseData baseData = (BaseData) dataAccess.getScrutariData(baseURI);
            if (baseData == null) {
                return null;
            }
            Integer baseCode = baseData.getBaseCode();
            BaseWrapper baseWrapper = baseWrapperMap.get(baseCode);
            if (baseWrapper == null) {
                baseWrapper = BaseWrapper.wrapBase(baseData, lang);
                baseWrapperMap.put(baseCode, baseWrapper);
            }
            return baseWrapper;
        }

        private BaseWrapper getBaseWrapper(Integer baseCode) {
            BaseWrapper baseWrapper = baseWrapperMap.get(baseCode);
            if (baseWrapper == null) {
                BaseData baseData = dataAccess.getBaseData(baseCode);
                baseWrapper = BaseWrapper.wrapBase(baseData, lang);
                baseWrapperMap.put(baseCode, baseWrapper);
            }
            return baseWrapper;
        }

        private CorpusWrapper getCorpusWrapper(CorpusURI corpusURI) {
            CorpusData corpusData = (CorpusData) dataAccess.getScrutariData(corpusURI);
            if (corpusData == null) {
                return null;
            }
            Integer corpusCode = corpusData.getCorpusCode();
            CorpusWrapper corpusWrapper = corpusWrapperMap.get(corpusCode);
            if (corpusWrapper == null) {
                corpusWrapper = CorpusWrapper.wrapCorpus(corpusData, lang, globalSearchOptions);
                corpusWrapperMap.put(corpusCode, corpusWrapper);
            }
            return corpusWrapper;
        }

        private CorpusWrapper getCorpusWrapper(Integer corpusCode) {
            CorpusWrapper corpusWrapper = corpusWrapperMap.get(corpusCode);
            if (corpusWrapper == null) {
                CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
                corpusWrapper = CorpusWrapper.wrapCorpus(corpusData, lang, globalSearchOptions);
                corpusWrapperMap.put(corpusCode, corpusWrapper);
            }
            return corpusWrapper;
        }

    }

}
