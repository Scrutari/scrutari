/* SctEngine - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.feed.SctFeedInfo;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.tools.EngineUtils;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class SctFeedInfoBuilder {

    private final static int DATE_MAX = 10;
    private final static DateFormat DAY_FORMAT = new SimpleDateFormat("yyyyMMdd");

    private SctFeedInfoBuilder() {
    }

    public static SctFeedInfo build(SctEngine engine, ScrutariDBName currentScrutariDBName) {
        EngineStorage engineStorage = engine.getEngineStorage();
        List<ScrutariDBName> feedScrutariDBNameList = new ArrayList<ScrutariDBName>();
        ScrutariDBName[] scrutariDBNameArray = engineStorage.getAddScrutariDBNameArray();
        int length = scrutariDBNameArray.length;
        if (length == 0) {
            feedScrutariDBNameList.add(currentScrutariDBName);
        } else {
            int step = 1;
            String previousDayString = null;
            for (int i = 0; i < length; i++) {
                ScrutariDBName date = scrutariDBNameArray[i];
                String dayString = DAY_FORMAT.format(date.getDate());
                if (i > 0) {
                    if (!dayString.equals(previousDayString)) {
                        step++;
                    }
                }
                previousDayString = dayString;
                feedScrutariDBNameList.add(date);
                if (step >= DATE_MAX) {
                    break;
                }
            }
        }
        List<ScrutariDBName> scrutariDBNameList = EngineUtils.wrap(feedScrutariDBNameList.toArray(new ScrutariDBName[feedScrutariDBNameList.size()]));
        return new InternalSctFeedInfo(scrutariDBNameList);
    }


    private static class InternalSctFeedInfo implements SctFeedInfo {

        private final List<ScrutariDBName> scrutariDBNameList;

        private InternalSctFeedInfo(List<ScrutariDBName> scrutariDBNameList) {
            this.scrutariDBNameList = scrutariDBNameList;
        }

        @Override
        public List<ScrutariDBName> getLastScrutariDBNameList() {
            return scrutariDBNameList;
        }

    }

}
