/* SctServer - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;


/**
 *
 * @author Vincent Calame
 */
public class CorpusWrapper {

    private final CorpusData corpusData;
    private final String corpusLabelString;
    private final String[] complementLabelStringArray;
    private final String categoryTerm;
    private final String categoryLabel;

    public CorpusWrapper(CorpusData corpusData, String corpusLabelString, String[] complementLabelStringArray, String categoryTerm, String categoryLabel) {
        this.corpusData = corpusData;
        this.corpusLabelString = corpusLabelString;
        this.complementLabelStringArray = complementLabelStringArray;
        this.categoryTerm = categoryTerm;
        this.categoryLabel = categoryLabel;
    }

    public String getCorpusLabelString() {
        return corpusLabelString;
    }

    public String getComplementLabel(int num) {
        return complementLabelStringArray[num - 1];
    }

    public CorpusData getCorpusData() {
        return corpusData;
    }

    public String getCategoryTerm() {
        return categoryTerm;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public static CorpusWrapper wrapCorpus(CorpusData corpusData, Lang lang, GlobalSearchOptions globalSearchOptions) {
        String categoryTerm = null;
        String categoryLabel = null;
        Category category = globalSearchOptions.getCategoryByCorpus(corpusData.getCorpusCode());
        if (category != null) {
            CategoryDef categoryDef = category.getCategoryDef();
            categoryTerm = categoryDef.getName();
            categoryLabel = categoryDef.getTitleLabels().seekLabelString(lang, null);
        }
        String corpusLabelString = LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_TITLE, lang, corpusData.getCorpusURI().getCorpusName());
        int complementCount = corpusData.getComplementMaxNumber();
        String[] complementLabelStringArray = new String[complementCount];
        for (int i = 0; i < complementCount; i++) {
            int num = i + 1;
            complementLabelStringArray[i] = LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_COMPLEMENTPREFIX + num, lang, "comp_" + num);
        }
        return new CorpusWrapper(corpusData, corpusLabelString, complementLabelStringArray, categoryTerm, categoryLabel);
    }

}
