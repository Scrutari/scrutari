/* SctServer - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import net.scrutari.data.FicheData;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class FicheWrapper {

    private final FicheData ficheData;
    private final BaseWrapper baseWrapper;
    private final CorpusWrapper corpusWrapper;
    private final ScrutariDBName scrutariDBName;

    private FicheWrapper(FicheData ficheData, CorpusWrapper corpusWrapper, BaseWrapper baseWrapper, ScrutariDBName scrutariDBName) {
        this.ficheData = ficheData;
        this.corpusWrapper = corpusWrapper;
        this.baseWrapper = baseWrapper;
        this.scrutariDBName = scrutariDBName;
    }

    public FicheData getFicheData() {
        return ficheData;
    }

    public CorpusWrapper getCorpusWrapper() {
        return corpusWrapper;
    }

    public BaseWrapper getBaseWrapper() {
        return baseWrapper;
    }

    public ScrutariDBName getScrutariDBName() {
        return scrutariDBName;
    }

    public static FicheWrapper wrapFiche(FicheData ficheData, CorpusWrapper corpusWrapper, BaseWrapper baseWrapper, ScrutariDBName scrutariDBName) {
        return new FicheWrapper(ficheData, corpusWrapper, baseWrapper, scrutariDBName);
    }

}
