/* SctServer - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class FicheFeed {

    private final Lang lang;
    private final List<FicheWrapper> ficheWrapperList;

    FicheFeed(Lang lang, Collection<FicheWrapper> ficheWrappers) {
        this.lang = lang;
        this.ficheWrapperList = new FicheWrapperList(ficheWrappers.toArray(new FicheWrapper[ficheWrappers.size()]));
    }

    public Lang getLang() {
        return lang;
    }

    public List<FicheWrapper> getFicheWrapperList() {
        return ficheWrapperList;
    }


    private static class FicheWrapperList extends AbstractList<FicheWrapper> implements RandomAccess {

        private final FicheWrapper[] array;

        private FicheWrapperList(FicheWrapper[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public FicheWrapper get(int i) {
            return array[i];
        }

    }

}
