/* SctServer - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.feed;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataConstants;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;


/**
 *
 * @author Vincent Calame
 */
public class BaseWrapper {

    private final BaseData baseData;
    private final String shortLabelString;
    private final String longLabelString;
    private final BaseCheck baseCheck;

    private BaseWrapper(BaseData baseData, BaseCheck baseCheck, String shortLabelString, String longLabelString) {
        this.baseData = baseData;
        this.shortLabelString = shortLabelString;
        this.longLabelString = longLabelString;
        this.baseCheck = baseCheck;
    }

    public Integer getBaseCode() {
        return baseData.getBaseCode();
    }

    public BaseCheck getBaseCheck() {
        return baseCheck;
    }

    public String getShortLabelString() {
        return shortLabelString;
    }

    public String getLongLabelString() {
        return longLabelString;
    }

    public static BaseWrapper wrapBase(BaseData baseData, Lang lang) {
        String shortLabelString = LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_TITLE, lang, baseData.getBaseURI().getBaseName());
        String longLabelString = LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_LONGTITLE, lang, baseData.getBaseURI().getBaseName());
        BaseCheck baseCheck = BaseChecker.check(baseData, lang);
        return new BaseWrapper(baseData, baseCheck, shortLabelString, longLabelString);
    }

}
