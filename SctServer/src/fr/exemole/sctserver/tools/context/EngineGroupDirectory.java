/* SctServer - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.context;

import fr.exemole.sctserver.api.context.EngineGroup;
import fr.exemole.sctserver.tools.ErrorUtils;
import fr.exemole.sctserver.tools.context.dom.EngineGroupDOMReader;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import javax.xml.parsers.DocumentBuilder;
import net.mapeadores.util.logging.MultiMessageHandler;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public final class EngineGroupDirectory {

    private EngineGroupDirectory() {

    }

    public static Map<String, EngineGroup> read(File groupDir, MultiMessageHandler multiMessageHandler) {
        Map<String, EngineGroup> result = new TreeMap<String, EngineGroup>();
        DocumentBuilder documentBuilder = DOMUtils.newDocumentBuilder();
        for (File f : groupDir.listFiles()) {
            if (f.isDirectory()) {
                continue;
            }
            String name = f.getName();
            if (!name.endsWith(".xml")) {
                continue;
            }
            name = name.substring(0, name.length() - 4);
            if (!StringUtils.isTechnicalName(name, true)) {
                continue;
            }
            multiMessageHandler.setCurrentSource(f.getPath());
            try {
                Document document = documentBuilder.parse(f);
                EngineGroupBuilder groupBuilder = new EngineGroupBuilder(name);
                EngineGroupDOMReader reader = new EngineGroupDOMReader(groupBuilder, multiMessageHandler);
                Element rootElement = document.getDocumentElement();
                reader.readEngineGroup(document.getDocumentElement(), "/" + rootElement.getTagName());
                result.put(name, groupBuilder.toEngineGroup());
            } catch (SAXException saxe) {
                DomMessages.saxException(multiMessageHandler, saxe);
            } catch (IOException ioe) {
                ErrorUtils.configCritical(multiMessageHandler, "_ error.exception.io", ioe.getMessage());
            }
        }
        return result;
    }

}
