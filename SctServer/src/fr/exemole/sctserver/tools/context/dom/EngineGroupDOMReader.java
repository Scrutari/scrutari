/* SctServer - Copyright (c) 2016-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.context.dom;

import fr.exemole.sctserver.tools.context.EngineGroupBuilder;
import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class EngineGroupDOMReader {

    private final EngineGroupBuilder engineGroupBuilder;
    private final MessageHandler messageHandler;

    public EngineGroupDOMReader(EngineGroupBuilder engineGroupBuilder, MessageHandler messageHandler) {
        this.engineGroupBuilder = engineGroupBuilder;
        this.messageHandler = messageHandler;
    }

    public void readEngineGroup(Element element, String xpath) {
        DOMUtils.readChildren(element, new RootConsumer(xpath));
    }


    private class RootConsumer implements Consumer<Element> {

        private final String rootXpath;

        private RootConsumer(String xpath) {
            this.rootXpath = xpath;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            String xpath = rootXpath + "/" + tagName;
            if (tagName.equals("engine")) {
                String engineName = element.getAttribute("name").trim();
                if (engineName.length() == 0) {
                    DomMessages.emptyAttribute(messageHandler, xpath, "name");
                } else if (StringUtils.isTechnicalName(engineName, true)) {
                    engineGroupBuilder.addEngine(engineName);
                } else {
                    DomMessages.wrongAttributeValue(messageHandler, xpath, "name", engineName);
                }
            } else if (tagName.equals("label")) {
                try {
                    LabelUtils.readLabel(element, engineGroupBuilder);
                } catch (ParseException ile) {
                    DomMessages.wrongLangAttribute(messageHandler, xpath, element.getAttribute("xml:lang"));
                }
            } else if (tagName.equals("attr")) {
                AttributeUtils.readAttrElement(engineGroupBuilder.getAttributesBuilder(), element);
            } else if (tagName.equals("phrase")) {
                LabelUtils.readPhraseElement(engineGroupBuilder.getPhrasesBuilder(), element, messageHandler, xpath);
            } else {
                DomMessages.unknownTagWarning(messageHandler, xpath);
            }
        }

    }

}
