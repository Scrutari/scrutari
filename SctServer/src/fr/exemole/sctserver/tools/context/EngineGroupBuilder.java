/* SctServer - Copyright (c) 2016-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.context;

import fr.exemole.sctserver.api.context.EngineGroup;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class EngineGroupBuilder extends DefBuilder {

    private final String name;
    private final Set<String> engineNameSet = new LinkedHashSet<String>();


    public EngineGroupBuilder(String name) {
        this.name = name;
    }

    public void addEngine(String engineName) {
        if (engineName == null) {
            throw new IllegalArgumentException("engineName is null");
        }
        engineNameSet.add(engineName);
    }


    public EngineGroup toEngineGroup() {
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        List<String> engineNameList = StringUtils.toList(engineNameSet);
        Phrases phrases = toPhrases();
        return new InternalEngineGroup(name, engineNameList, phrases, titleLabels, attributes);
    }


    private static class InternalEngineGroup implements EngineGroup {

        private final String name;
        private final List<String> engineNameList;
        private final Labels titleLabels;
        private final Phrases phrases;
        private final Attributes attributes;

        private InternalEngineGroup(String name, List<String> engineNameList, Phrases phrases,
                Labels titleLabels, Attributes attributes) {
            this.name = name;
            this.engineNameList = engineNameList;
            this.titleLabels = titleLabels;
            this.phrases = phrases;
            this.attributes = attributes;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<String> getEngineNameList() {
            return engineNameList;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Phrases getPhrases() {
            return phrases;
        }

    }

}
