/* SctServer - Copyright (c) 2012-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public final class ErrorUtils {

    private ErrorUtils() {
    }

    public static void configCritical(MessageHandler handler, String messageKey, Object... values) {
        handler.addMessage("critical.config", LocalisationUtils.toMessage(messageKey, values));
    }

}
