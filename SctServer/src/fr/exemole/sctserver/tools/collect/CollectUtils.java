/* SctServer - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.collect;

import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public final class CollectUtils {

    public final static List<ScrutariSource> EMPTY_SCRUTARISOURCELIST = Collections.emptyList();

    private CollectUtils() {
    }

    public static CollectResult toCollectResult(String state, FuzzyDate lastUpdate) {
        return new InternalCollectResult(state, lastUpdate);
    }

    public static String getStateMessageKey(String state) {
        switch (state) {
            case CollectResult.OK_STATE:
                return "_ state.source.ok";
            case CollectResult.UNKNOWN_STATE:
                return "_ state.source.unknown";
            case CollectResult.INFO_NOCONNECTION_STATE:
                return "_ state.source.info_noconnection";
            case CollectResult.INFO_BROKENURL_STATE:
                return "_ state.source.info_brokenurl";
            case CollectResult.INFO_DOWNLOADERROR_STATE:
                return "_ state.source.info_downloaderror";
            case CollectResult.INFO_NOTXML_STATE:
                return "_ state.source.info_notxml";
            case CollectResult.INFO_WRONGXML_STATE:
                return "_ state.source.info_wrongxml";
            case CollectResult.DATA_NOCONNECTION_STATE:
                return "_ state.source.data_noconnection";
            case CollectResult.DATA_BROKENURL_STATE:
                return "_ state.source.data_brokenurl";
            case CollectResult.DATA_DOWNLOADERROR_STATE:
                return "_ state.source.data_downloaderror";
            case CollectResult.DATA_TRANSFORMATION_STATE:
                return "_ state.source.data_transformation";
            case CollectResult.DATA_NOTXML_STATE:
                return "_ state.source.data_notxml";
            default:
                throw new SwitchException("state = " + state);
        }
    }

    public static List<ScrutariSource> wrap(ScrutariSource[] array) {
        return new ScrutariSourceList(array);

    }


    private static class InternalCollectResult implements CollectResult {

        private final String state;
        private final FuzzyDate lastUpdate;

        private InternalCollectResult(String state, FuzzyDate lastUpdate) {
            this.state = state;
            this.lastUpdate = lastUpdate;
        }

        @Override
        public String getState() {
            return state;
        }

        @Override
        public FuzzyDate getLastUpdate() {
            return lastUpdate;
        }

    }


    private static class ScrutariSourceList extends AbstractList<ScrutariSource> implements RandomAccess {

        private final ScrutariSource[] array;

        private ScrutariSourceList(ScrutariSource[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ScrutariSource get(int index) {
            return array[index];
        }

    }

}
