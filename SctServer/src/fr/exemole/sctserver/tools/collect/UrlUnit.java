/* SctServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.collect;

import java.net.URL;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class UrlUnit {

    private final URL url;
    private final String transformation;
    private final Map<String, String> tranformationParameters;

    public UrlUnit(URL url, String transformation, Map<String, String> tranformationParameters) {
        if (url == null) {
            throw new IllegalArgumentException("url is null");
        }
        if (transformation == null) {
            throw new IllegalArgumentException("transformation is null");
        }
        if (tranformationParameters == null) {
            throw new IllegalArgumentException("tranformationParameters is null");
        }
        this.url = url;
        this.transformation = transformation;
        this.tranformationParameters = tranformationParameters;
    }

    public URL getUrl() {
        return url;
    }

    public String getTransformation() {
        return transformation;
    }

    public Map<String, String> getTranformationParameters() {
        return tranformationParameters;
    }

}
