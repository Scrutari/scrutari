<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:param name="authority" select="'localhost'"/>
    <xsl:param name="base" select="'desmographies'"/>
    <xsl:param name="thesaurus" select="'dsmd'"/>
    
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>

    <xsl:template match="/">
        <base>
            <base-metadata>
                <authority><xsl:value-of select="$authority"/></authority>
		<base-name><xsl:value-of select="$base"/></base-name>
            </base-metadata>
            <thesaurus thesaurus-name="{$thesaurus}">
                <thesaurus-metadata>
                    <xsl:apply-templates select="dsmd" mode="_Metadata"/>
                </thesaurus-metadata>
                <xsl:apply-templates select="dsmd/atlas/structure" mode="_Motcle"/>
                <xsl:apply-templates select="dsmd/atlas/descripteurs/*/descripteur" mode="_Motcle"/>
            </thesaurus>
            <relations default-member-type="motcle" default-thesaurus="{$thesaurus}" default-role="inferior">
                <xsl:apply-templates select="dsmd/atlas/structure" mode="_SpecialRelations"/>
                <xsl:apply-templates select="dsmd/conf/param[@name='home.ventilationroot']" mode="_HomeRelation"/>
            </relations>
            <relations default-relation-type="family" default-member-type="motcle" default-thesaurus="{$thesaurus}" default-role="inferior">
                <xsl:apply-templates select="dsmd/atlas/descripteurs/infamille" mode="_Relation"/>
            </relations>
            <relations default-relation-type="hierarchy" default-member-type="motcle" default-thesaurus="{$thesaurus}" default-role="inferior">
                <xsl:apply-templates select="dsmd/atlas/liens/lhg"/>
            </relations>
        </base>
    </xsl:template>
    
    <xsl:template match="infamille" mode="_Relation">
        <relation>
            <member role="superior" uri="_family_{@idctxt}"/>
            <xsl:apply-templates select="descripteur" mode="_Inferior"/>
        </relation>
    </xsl:template>
    
    <xsl:template match="dsmd" mode="_Metadata">
        <xsl:apply-templates select="atlas/metadata/titre"/>
        <xsl:apply-templates select="atlas/metadata/titre/attr"/>
        <xsl:apply-templates select="atlas/metadata/langs"/>
        <xsl:apply-templates select="conf/param"/>
    </xsl:template>
    
    <xsl:template match="structure" mode="_Motcle">
        <xsl:apply-templates select="grille" mode="_Motcle"/>
        <xsl:apply-templates select="familles/contexte" mode="_Motcle">
            <xsl:with-param name="prefix" select="'_family'"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="structure" mode="_SpecialRelations">
        <relation type="grids">
            <member role="desmography" type="thesaurus" uri="{$thesaurus}"/>
            <xsl:apply-templates select="grille" mode="_Member"/>
        </relation>
        <relation type="rootfamilies">
            <member role="desmography" type="thesaurus" uri="{$thesaurus}"/>
            <xsl:apply-templates select="familles/contexte" mode="_FamilyMember"/>
        </relation>
        <xsl:apply-templates select="grille" mode="_Subsectors"/>
        <xsl:apply-templates select="familles/contexte[contexte]" mode="_Subfamilies"/>
        <xsl:apply-templates select="familles//contexte[attr[@ns='atlas' and @key='ventilationnaturelle']]" mode="_RosetteDefault"/>
    </xsl:template>
    
    <xsl:template match="grille" mode="_Motcle">
        <motcle motcle-id="{@name}">
            <xsl:apply-templates select="lib"/>
            <xsl:apply-templates select="attr"/>
        </motcle>
        <xsl:apply-templates select="contexte" mode="_Motcle">
            <xsl:with-param name="prefix" select="@name"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="grille" mode="_Val">
        <val><xsl:value-of select="@name"/></val>
    </xsl:template>
    
    <xsl:template match="grille" mode="_Member">
        <member uri="{@name}"/>
    </xsl:template>
    
    <xsl:template match="grille" mode="_Subsectors">
        <relation type="subsectors">
            <member role="superior" uri="{@name}"/>
            <xsl:apply-templates select="contexte" mode="_SectorMember">
                <xsl:with-param name="prefix" select="@name"/>
            </xsl:apply-templates>
        </relation>
        <xsl:apply-templates select="contexte[contexte]" mode="_Subsectors">
            <xsl:with-param name="prefix" select="@name"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="contexte" mode="_FamilyVal">
        <val>_family_<xsl:value-of select="@idctxt"/></val>
    </xsl:template>
    
    <xsl:template match="contexte" mode="_FamilyMember">
        <member uri="_family_{@idctxt}"/>
    </xsl:template>
    
    <xsl:template match="contexte" mode="_SectorMember">
        <xsl:param name="prefix"/>
        <member uri="{$prefix}_{@idctxt}"/>
    </xsl:template>
    
    <xsl:template match="contexte" mode="_Subsectors">
        <xsl:param name="prefix"/>
        <relation type="subsectors">
            <member role="superior" uri="{$prefix}_{@idctxt}"/>
            <xsl:apply-templates select="contexte" mode="_SectorMember">
                <xsl:with-param name="prefix" select="$prefix"/>
            </xsl:apply-templates>
        </relation>
        <xsl:apply-templates select="contexte[contexte]" mode="_Subsectors">
            <xsl:with-param name="prefix" select="$prefix"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="contexte" mode="_Subfamilies">
        <relation type="subfamilies">
            <member role="superior" uri="_family_{@idctxt}"/>
            <xsl:apply-templates select="contexte" mode="_FamilyMember"/>
        </relation>
        <xsl:apply-templates select="contexte[contexte]" mode="_Subfamilies"/>
    </xsl:template>         
    
    <xsl:template match="contexte" mode="_Motcle">
        <xsl:param name="prefix"/>
        <motcle motcle-id="{$prefix}_{@idctxt}">
            <xsl:apply-templates select="lib"/>
            <xsl:apply-templates select="attr"/>
        </motcle>
        <xsl:apply-templates select="contexte" mode="_Motcle">
            <xsl:with-param name="prefix" select="$prefix"/>
        </xsl:apply-templates>
    </xsl:template>
    
    <xsl:template match="contexte" mode="_RosetteDefault">
        <relation type="rosette.default">
            <member role="root" uri="_family_{@idctxt}"/>
            <xsl:apply-templates select="attr[@ns='atlas' and @key='ventilationnaturelle']/val[1]"  mode="_RosetteRelation"/>
        </relation>
    </xsl:template>
    
    <xsl:template match="descripteur" mode="_Motcle">
        <motcle motcle-id="{@iddesc}">
            <xsl:apply-templates select="lib"/>
            <xsl:apply-templates select="attr"/>
        </motcle>
    </xsl:template>
    
    <xsl:template match="descripteur" mode="_Inferior">
        <member uri="{@iddesc}"/>
    </xsl:template>
    
    <xsl:template match="lib">
        <lib xml:lang="{@xml:lang}"><xsl:value-of select="."/></lib>
    </xsl:template>
    
    <xsl:template match="lib" mode="_Label">
        <label xml:lang="{@xml:lang}"><xsl:value-of select="."/></label>
    </xsl:template>
    
    <xsl:template match="attr">
        <attr ns="{@ns}" key="{@key}">
            <xsl:copy-of select="val"/>
        </attr>
    </xsl:template>
    
    <xsl:template match="attr[@ns='atlas']">
        <attr ns="dsm" key="{@key}">
            <xsl:copy-of select="val"/>
        </attr>
    </xsl:template>
    
    <xsl:template match="attr[@ns='atlas' and @key='ventilationnaturelle']">
    </xsl:template>
    
    <xsl:template match="attr[@ns='atlas' and @key='ventilations']">
    </xsl:template>
    
    <xsl:template match="val" mode="_RosetteRelation">
        <xsl:variable name="text" select="text()"/>
        <xsl:choose>
            <xsl:when test="starts-with($text, 'ventilation:grille:')">
                <member role="grid" uri="{substring($text, 20)}"/>
            </xsl:when>
            <xsl:when test="starts-with($text, 'ventilation:famille:')">
                <member role="family" uri="_family_{substring($text, 21)}"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="val" mode="_VentilationConversion">
        <xsl:variable name="text" select="text()"/>
        <xsl:choose>
            <xsl:when test="starts-with($text, 'ventilation:grille:')">
                <val>grid:<xsl:value-of select="substring($text, 20)"/></val>
            </xsl:when>
            <xsl:when test="starts-with($text, 'ventilation:famille:')">
                <val>family:_family_<xsl:value-of select="substring($text, 21)"/></val>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template match="param">
    </xsl:template>
    
    <xsl:template match="param[@name='ignore.empty.secteur' and @value='true']">
        <attr ns="desmodojs" key="emptysectors">
            <val>ignore</val>
        </attr>
    </xsl:template>
    
    <xsl:template match="param[@name='home.ventilationroot']" mode="_HomeRelation">
        <xsl:variable name="text" select="@value"/>
        <relation type="home">
            <member role="desmography" type="thesaurus" uri="{$thesaurus}"/>
            <xsl:choose>
                <xsl:when test="starts-with($text, 'descripteur:')">
                    <member role="root" uri="{substring($text, 13)}"/>
                </xsl:when>
            </xsl:choose>
        </relation>
    </xsl:template>
    
    <xsl:template match="titre">
        <intitule-thesaurus>
            <xsl:apply-templates select="lib"/>
        </intitule-thesaurus>
        <phrase name="desmography.title">
            <xsl:apply-templates select="lib" mode="_Label"/>
        </phrase>
    </xsl:template>
    
    <xsl:template match="langs">
        <attr ns="dsm" key="langs">
            <xsl:apply-templates select="lang"/>
        </attr>
    </xsl:template>
    
    <xsl:template match="lang">
        <val><xsl:value-of select="."/></val>
    </xsl:template>
    
    <xsl:template match="lhg">
        <relation>
            <xsl:apply-templates select="desc-ctxt/desc" mode="_Superior"/>
            <xsl:apply-templates select="desc-ctxt/ctxt" mode="_Sector"/>
            <xsl:apply-templates select="desc" mode="_Inferior"/>
        </relation>
    </xsl:template>
    
    <xsl:template match="desc" mode="_Superior">
        <member role="superior" uri="{@iddesc}"/>
    </xsl:template>
    
    <xsl:template match="ctxt" mode="_Sector">
        <member role="sector" uri="{@grille}_{@idctxt}"/>
    </xsl:template>
    
    <xsl:template match="desc" mode="_Inferior">
        <member uri="{@iddesc}"/>
    </xsl:template>

</xsl:stylesheet>
