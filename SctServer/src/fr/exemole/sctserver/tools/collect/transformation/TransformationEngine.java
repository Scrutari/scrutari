/* SctServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.collect.transformation;

import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.StateErrorException;
import java.io.File;
import java.util.Map;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public final class TransformationEngine {

    private final MessageHandler messageHandler;

    private TransformationEngine(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    private void transform(File origin, File destination, String transformation, Map<String, String> transformationParameters) throws StateErrorException {
        try {
            Transformer transformer = getTransformer(transformation);
            for (Map.Entry<String, String> parameter : transformationParameters.entrySet()) {
                transformer.setParameter(parameter.getKey(), parameter.getValue());
            }
            transformer.transform(new StreamSource(origin), new StreamResult(destination));
        } catch (TransformerException e) {
            severe(e);
            throw new StateErrorException(CollectResult.DATA_TRANSFORMATION_STATE);
        }
    }

    public static void transform(File origin, File destination, String transformation, Map<String, String> transformationParameters, MessageHandler messageHandler) throws StateErrorException {
        TransformationEngine engine = new TransformationEngine(messageHandler);
        engine.transform(origin, destination, transformation, transformationParameters);
    }

    private Transformer getTransformer(String transformation) throws StateErrorException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        switch (transformation) {
            case "dsmd":
                try {
                    return transformerFactory.newTransformer(new StreamSource(getClass().getResourceAsStream("dsmd.xsl")));
                } catch (TransformerException e) {
                    messageHandler.addMessage("severe.transformation", "_ error.exception.transformation", "dsmd.xsl error " + e.getMessage());
                    throw new StateErrorException(CollectResult.DATA_TRANSFORMATION_STATE);
                }
            default:
                messageHandler.addMessage("severe.transformation", "_ error.exception.transformation", "Unknown transformation: " + transformation);
                throw new StateErrorException(CollectResult.DATA_TRANSFORMATION_STATE);
        }
    }

    private void severe(TransformerException transformerException) {
        StringBuilder buf = new StringBuilder();
        buf.append(transformerException.getMessage());
        SourceLocator sourceLocator = transformerException.getLocator();
        if (sourceLocator != null) {
            String sourceSystemId = sourceLocator.getSystemId();
            buf.append(" (");
            if (sourceSystemId != null) {
                buf.append(sourceSystemId);
            }
            buf.append(")");
        }
        messageHandler.addMessage("severe.transformation", "_ error.exception.transformation", buf.toString());
    }

}
