/* SctServer - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.collect;

import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ScrutariSourceStorage {

    private ScrutariSourceStorage() {
    }

    public static String toString(List<ScrutariSource> scrutariSourceList) {
        StringBuilder buf = new StringBuilder();
        for (ScrutariSource scrutariSource : scrutariSourceList) {
            ScrutariSourceDef scrutariSourceDef = scrutariSource.getScrutariSourceDef();
            buf.append(scrutariSourceDef.getName());
            buf.append("    ");
            buf.append(scrutariSource.getState());
            buf.append("    ");
            FuzzyDate lastUpdate = scrutariSource.getLastUpdate();
            if (lastUpdate == null) {
                buf.append("NULL");
            } else {
                buf.append(lastUpdate.toString());
            }
            buf.append('\n');
        }
        return buf.toString();
    }

    public static Map<String, Status> fromString(String s) {
        Map<String, Status> map = new HashMap<String, Status>();
        String[] tokens = StringUtils.getLineTokens(s, StringUtils.EMPTY_EXCLUDE);
        for (String line : tokens) {
            String[] subtokens = StringUtils.getTechnicalTokens(line, true);
            int subtokenLength = subtokens.length;
            if (subtokenLength < 2) {
                continue;
            }
            boolean error = false;
            String name = subtokens[0];
            String state = CollectResult.UNKNOWN_STATE;
            try {
                state = checkAlias(subtokens[1]);
            } catch (IllegalArgumentException iae) {
                error = true;
            }
            FuzzyDate lastUpdate = null;
            if (subtokenLength > 2) {
                if (!subtokens[2].equals("NULL")) {
                    try {
                        lastUpdate = FuzzyDate.parse(subtokens[2]);
                    } catch (ParseException pe) {
                        error = true;
                    }
                }
            }
            if (!error) {
                map.put(name, new Status(name, state, lastUpdate));
            }
        }
        return map;
    }

    private static String checkAlias(String stateString) {
        switch (stateString) {
            case "state_ok":
                return CollectResult.OK_STATE;
            case "state_unknown":
                return CollectResult.UNKNOWN_STATE;
            case "state_info_noconnection":
                return CollectResult.INFO_NOCONNECTION_STATE;
            case "state_info_brokenurl":
                return CollectResult.INFO_BROKENURL_STATE;
            case "state_info_downloaderror":
                return CollectResult.INFO_DOWNLOADERROR_STATE;
            case "state_info_notxml":
                return CollectResult.INFO_NOTXML_STATE;
            case "state_info_wrongxml":
                return CollectResult.INFO_WRONGXML_STATE;
            case "state_data_noconnection":
                return CollectResult.DATA_NOCONNECTION_STATE;
            case "state_data_brokenurl":
                return CollectResult.DATA_BROKENURL_STATE;
            case "state_data_downloaderror":
                return CollectResult.DATA_DOWNLOADERROR_STATE;
            case "state_data_notxml":
                return CollectResult.DATA_NOTXML_STATE;
            default:
                return stateString;
        }
    }


    public static class Status implements CollectResult {

        private final String name;
        private final String state;
        private final FuzzyDate lastUpdate;

        private Status(String name, String state, FuzzyDate lastUpdate) {
            this.name = name;
            this.state = state;
            this.lastUpdate = lastUpdate;
        }

        public String getName() {
            return name;
        }

        @Override
        public String getState() {
            return state;
        }

        @Override
        public FuzzyDate getLastUpdate() {
            return lastUpdate;
        }

    }

}
