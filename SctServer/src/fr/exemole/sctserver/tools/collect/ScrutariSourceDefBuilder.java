/* SctServer - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.collect;

import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import java.net.URL;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.datauri.BaseURI;
import net.scrutari.db.tools.codes.CodesUtils;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariSourceDefBuilder {

    private final static Map<String, String> EMPTY_MAP = Collections.emptyMap();
    private final String name;
    private BaseURI mainBaseURI;
    private final Set<BaseURI> aliasSet = new LinkedHashSet<BaseURI>();
    private final List<ScrutariSourceDef.UrlEntry> urlEntryList = new ArrayList<ScrutariSourceDef.UrlEntry>();

    public ScrutariSourceDefBuilder(String name) {
        if (!StringUtils.isTechnicalName(name, true)) {
            throw new IllegalArgumentException("Not a technical name: " + name);
        }
        this.name = name;
    }

    public boolean isEmpty() {
        return urlEntryList.isEmpty();
    }

    public ScrutariSourceDefBuilder addScrutariDataUrl(URL url, int frequency) {
        if (url == null) {
            throw new IllegalArgumentException("url is null");
        }
        if (frequency < 1) {
            frequency = 1;
        }
        urlEntryList.add(new InternalUrlEntry(ScrutariSourceDef.SCRUTARIDATA_TYPE, url, frequency));
        return this;
    }

    public ScrutariSourceDefBuilder addScrutariDataUrl(UrlUnit urlUnit, int frequency) {
        if (urlUnit == null) {
            throw new IllegalArgumentException("urlUnit is null");
        }
        if (frequency < 1) {
            frequency = 1;
        }
        urlEntryList.add(new InternalUrlEntry(ScrutariSourceDef.SCRUTARIDATA_TYPE, urlUnit, frequency));
        return this;
    }

    public ScrutariSourceDefBuilder addInfoUrl(URL url) {
        if (url == null) {
            throw new IllegalArgumentException("url is null");
        }
        urlEntryList.add(new InternalUrlEntry(ScrutariSourceDef.INFO_TYPE, url, 1));
        return this;
    }

    public ScrutariSourceDefBuilder addInfoUrl(UrlUnit urlUnit) {
        if (urlUnit == null) {
            throw new IllegalArgumentException("url is null");
        }
        urlEntryList.add(new InternalUrlEntry(ScrutariSourceDef.INFO_TYPE, urlUnit, 1));
        return this;
    }


    public ScrutariSourceDefBuilder setMainBaseURI(BaseURI mainBaseURI) {
        this.mainBaseURI = mainBaseURI;
        return this;
    }

    public ScrutariSourceDefBuilder addAliasBaseURI(BaseURI baseURI) {
        aliasSet.add(baseURI);
        return this;
    }

    public ScrutariSourceDef toScrutariSourceDef() {
        if (urlEntryList.isEmpty()) {
            throw new IllegalStateException("url is not initialized");
        }
        List<BaseURI> aliasList = CodesUtils.wrap(aliasSet.toArray(new BaseURI[aliasSet.size()]));
        List<ScrutariSourceDef.UrlEntry> finalUrlEntryList = new ScrutariSourceDefUrlEntryList(urlEntryList.toArray(new ScrutariSourceDef.UrlEntry[urlEntryList.size()]));
        return new InternalScrutariSourceDef(name, finalUrlEntryList, mainBaseURI, aliasList);
    }

    public static ScrutariSourceDefBuilder init(String name) {
        return new ScrutariSourceDefBuilder(name);
    }


    private static class InternalUrlEntry implements ScrutariSourceDef.UrlEntry {

        private final short type;
        private final URL url;
        private final int frequency;
        private final String transformation;
        private final Map<String, String> transformationParameters;

        private InternalUrlEntry(short type, URL url, int frequency) {
            this.type = type;
            this.url = url;
            this.frequency = frequency;
            this.transformation = "";
            this.transformationParameters = EMPTY_MAP;
        }

        private InternalUrlEntry(short type, UrlUnit urlUnit, int frequency) {
            this.type = type;
            this.url = urlUnit.getUrl();
            this.frequency = frequency;
            this.transformation = urlUnit.getTransformation();
            Map<String, String> map = urlUnit.getTranformationParameters();
            if ((this.transformation.isEmpty()) || (map.isEmpty())) {
                this.transformationParameters = EMPTY_MAP;
            } else {
                this.transformationParameters = Collections.unmodifiableMap(new LinkedHashMap<String, String>(map));
            }
        }

        @Override
        public URL getUrl() {
            return url;
        }

        @Override
        public short getType() {
            return type;
        }

        @Override
        public int getFrequency() {
            return frequency;
        }

        @Override
        public String getTransformation() {
            return transformation;
        }

        @Override
        public Map<String, String> getTransformationParameters() {
            return transformationParameters;
        }

    }


    private static class InternalScrutariSourceDef implements ScrutariSourceDef {

        private final String name;
        private final List<ScrutariSourceDef.UrlEntry> urlEntryList;
        private final BaseURI mainBaseURI;
        private final List<BaseURI> aliasList;

        private InternalScrutariSourceDef(String name, List<ScrutariSourceDef.UrlEntry> urlEntryList, BaseURI mainBaseURI, List<BaseURI> aliasList) {
            this.name = name;
            this.urlEntryList = urlEntryList;
            this.mainBaseURI = mainBaseURI;
            this.aliasList = aliasList;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<UrlEntry> getUrlEntryList() {
            return urlEntryList;
        }

        @Override
        public BaseURI getMainBaseURI() {
            return mainBaseURI;
        }

        @Override
        public List<BaseURI> getAliasList() {
            return aliasList;
        }

    }


    private static class ScrutariSourceDefUrlEntryList extends AbstractList<ScrutariSourceDef.UrlEntry> implements RandomAccess {

        private final ScrutariSourceDef.UrlEntry[] array;

        private ScrutariSourceDefUrlEntryList(ScrutariSourceDef.UrlEntry[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ScrutariSourceDef.UrlEntry get(int index) {
            return array[index];
        }

    }

}
