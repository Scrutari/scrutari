/* SctServer - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.collect;

import fr.exemole.sctserver.api.ScrutariSourceManager;
import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.ScrutariInfoContent;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.api.collect.StateErrorException;
import fr.exemole.sctserver.api.log.SctLogConstants;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.api.storage.SctStorageException;
import fr.exemole.sctserver.tools.collect.transformation.TransformationEngine;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.exceptions.NestedLibraryException;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.logging.MultiMessageHandler;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public final class CollectEngine {

    private final ScrutariSource scrutariSource;
    private final String scrutariSourceName;
    private final EngineStorage engineStorage;
    private final MultiMessageHandler messageHandler;
    private final SourceLog sourceLog;
    private final ScrutariSourceDef scrutariSourceDef;
    private final int level;
    private final List<EngineStorage.MatchedData> matchedDataList = new ArrayList<EngineStorage.MatchedData>();
    private FuzzyDate lastUpdateDate = null;
    private String userAgent = "Scrutari";

    private CollectEngine(ScrutariSource scrutariSource, EngineStorage engineStorage, MultiMessageHandler messageHandler, int level) {
        this.engineStorage = engineStorage;
        this.scrutariSource = scrutariSource;
        this.scrutariSourceName = scrutariSource.getScrutariSourceDef().getName();
        this.messageHandler = messageHandler;
        this.level = level;
        this.scrutariSourceDef = scrutariSource.getScrutariSourceDef();
        this.sourceLog = new SourceLog(engineStorage.getLogStorage().getPrintWriter(SctLogConstants.COLLECT_DIR, scrutariSourceName + ".txt"));
    }

    private CollectResult run() {
        String state = CollectResult.OK_STATE;
        messageHandler.setCurrentSource(scrutariSourceName);
        boolean error = false;
        sourceLog.log("Source", scrutariSourceName);
        sourceLog.log("CollectLevel", getLevelString(level));
        List<ScrutariSourceDef.UrlEntry> entryList = scrutariSourceDef.getUrlEntryList();
        sourceLog.log("EntryLength", entryList.size());
        for (ScrutariSourceDef.UrlEntry urlEntry : entryList) {
            short type = urlEntry.getType();
            sourceLog.chapter("Entry");
            sourceLog.log("Url", urlEntry.getUrl().toString());
            sourceLog.log("Type", getTypeString(type));
            try {
                switch (type) {
                    case ScrutariSourceDef.INFO_TYPE:
                        fromInfoType(urlEntry);
                        break;
                    case ScrutariSourceDef.SCRUTARIDATA_TYPE:
                        fromDataType(urlEntry);
                        break;
                    default:
                        throw new SwitchException("Unknown type: " + type);
                }
            } catch (StateErrorException stateErrorException) {
                state = stateErrorException.getErrorState();
                lastUpdateDate = null;
                error = true;
                sourceLog.log("ErrorState", state);
            }
            if (error) {
                break;
            }
        }
        if (!error) {
            engineStorage.saveScrutariData(scrutariSource.getScrutariSourceDef().getName(), matchedDataList);
        }
        for (EngineStorage.MatchedData data : matchedDataList) {
            ((InternalMatchedData) data).delete();
        }
        sourceLog.close();
        return CollectUtils.toCollectResult(state, lastUpdateDate);
    }

    public static CollectResult collectData(ScrutariSource scrutariSource, EngineStorage engineStorage, MultiMessageHandler messageHandler, int level) {
        messageHandler.setCurrentSource(scrutariSource.getScrutariSourceDef().getName());
        CollectEngine engine = new CollectEngine(scrutariSource, engineStorage, messageHandler, level);
        return engine.run();
    }

    private void fromDataType(ScrutariSourceDef.UrlEntry urlEntry) throws StateErrorException {
        URL dataUrl = urlEntry.getUrl();
        if (level == ScrutariSourceManager.LOCAL) {
            if (engineStorage.hasData(scrutariSourceName, dataUrl)) {
                addNoChange(dataUrl);
                return;
            }
        } else if (level == ScrutariSourceManager.CHECK) {
            int frequency = urlEntry.getFrequency();
            FuzzyDate currentDate = FuzzyDate.current();
            if ((engineStorage.hasData(scrutariSourceName, dataUrl)) && (FuzzyDate.compare(scrutariSource.getLastUpdate(), currentDate.roll(-frequency)) > 0)) {
                lastUpdateDate = FuzzyDate.max(lastUpdateDate, scrutariSource.getLastUpdate());
                addNoChange(dataUrl);
                return;
            }
        }
        File tempFile = createTempFile(scrutariSourceName);
        try (OutputStream os = new FileOutputStream(tempFile)) {
            downloadData(os, dataUrl);
        } catch (IOException ioe) {
            tempFile.delete();
            throw new SctStorageException(ioe);
        }
        addChange(urlEntry, tempFile);
        lastUpdateDate = FuzzyDate.current();
    }

    private void fromInfoType(ScrutariSourceDef.UrlEntry urlEntry) throws StateErrorException {
        URL infoUrl = urlEntry.getUrl();
        if (level == ScrutariSourceManager.LOCAL) {
            if (engineStorage.hasData(scrutariSourceName, infoUrl)) {
                addNoChange(infoUrl);
                return;
            }
        }
        ScrutariInfoContent scrutariInfoContent = getInfoContent(infoUrl);
        FuzzyDate infoDate = scrutariInfoContent.getDate();
        if (level == ScrutariSourceManager.CHECK) {
            if ((engineStorage.hasData(scrutariSourceName, infoUrl)) && (FuzzyDate.compare(infoDate, scrutariSource.getLastUpdate()) < 0)) {
                lastUpdateDate = FuzzyDate.max(lastUpdateDate, scrutariSource.getLastUpdate());
                addNoChange(infoUrl);
                return;
            }
        }
        File tempFile = createTempFile(scrutariSourceName);
        try (OutputStream os = new FileOutputStream(tempFile)) {
            List<Object> dataFragmentList = scrutariInfoContent.getDataFragmentList();
            sourceLog.log("DataFragmentLength", dataFragmentList.size());
            for (Object fragment : dataFragmentList) {
                sourceLog.chapter("DataFragment");
                if (fragment instanceof URL) {
                    URL url = (URL) fragment;
                    sourceLog.log("Type", "url");
                    sourceLog.log("URL", url.toString());
                    downloadData(os, url);
                } else if (fragment instanceof String) {
                    sourceLog.log("Type", "string");
                    os.write(((String) fragment).getBytes("UTF-8"));
                }
            }
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        addChange(urlEntry, tempFile);
        lastUpdateDate = FuzzyDate.current();
    }

    private void addChange(ScrutariSourceDef.UrlEntry entry, File tempFile) throws StateErrorException {
        sourceLog.chapter("/Entry");
        sourceLog.log("DataChange", "yes");
        File firstTempFile;
        String transformation = entry.getTransformation();
        if (!transformation.isEmpty()) {
            sourceLog.log("DataTransformation", transformation);
            firstTempFile = tempFile;
            tempFile = createTempFile(scrutariSourceName);
            TransformationEngine.transform(firstTempFile, tempFile, transformation, entry.getTransformationParameters(), messageHandler);
        } else {
            firstTempFile = null;
        }
        matchedDataList.add(new InternalMatchedData(entry.getUrl(), tempFile, firstTempFile));
    }

    private void addNoChange(URL url) {
        sourceLog.log("DataChange", "no");
        matchedDataList.add(new InternalMatchedData(url));
    }

    private static File createTempFile(String name) {
        try {
            File tempFile = File.createTempFile(name, ".tmp");
            tempFile.deleteOnExit();
            return tempFile;
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    private void downloadData(OutputStream outputStream, URL dataUrl) throws IOException, StateErrorException {
        try {
            Thread.sleep(100);
        } catch (InterruptedException ie) {

        }
        long startTime = System.currentTimeMillis();
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = (HttpURLConnection) dataUrl.openConnection();
            checkConnection(urlConnection);
            urlConnection.connect();
            int code = urlConnection.getResponseCode();
            sourceLog.log("ResponseCode", code);
            sourceLog.log("ResponseMessage", urlConnection.getResponseMessage());
            if (code != 200) {
                severeDownload(dataUrl, urlConnection.getResponseMessage());
                throw new StateErrorException(CollectResult.DATA_BROKENURL_STATE);
            }
        } catch (IOException ioe) {
            sourceLog.log("ConnectionIOException", ioe.getMessage());
            severeDownload(dataUrl, ioe.getMessage());
            throw new StateErrorException(CollectResult.DATA_NOCONNECTION_STATE);
        }
        try (InputStream is = urlConnection.getInputStream()) {
            byte[] buffer = new byte[1024];
            int size;
            while ((size = is.read(buffer, 0, 1024)) > 0) {
                outputStream.write(buffer, 0, size);
            }
        } catch (IOException ioe) {
            sourceLog.log("DownloadIOException", ioe.getMessage());
            severeDownload(dataUrl, ioe.getMessage());
            throw new StateErrorException(CollectResult.DATA_DOWNLOADERROR_STATE);
        }
        sourceLog.printIntervalle("DownloadTime", System.currentTimeMillis(), startTime);
    }

    private ScrutariInfoContent getInfoContent(URL url) throws StateErrorException {
        HttpURLConnection urlConnection;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            checkConnection(urlConnection);
            urlConnection.connect();
            int code = urlConnection.getResponseCode();
            sourceLog.log("ResponseCode", code);
            sourceLog.log("ResponseMessage", urlConnection.getResponseMessage());
            if (code != 200) {
                severeDownload(url, urlConnection.getResponseMessage());
                throw new StateErrorException(CollectResult.INFO_BROKENURL_STATE);
            }
        } catch (IOException ioe) {
            sourceLog.log("ConnectionIOException", ioe.getMessage());
            severeDownload(url, ioe.getMessage());
            throw new StateErrorException(CollectResult.INFO_NOCONNECTION_STATE);
        }
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new NestedLibraryException(e);
        }
        Document doc = null;
        long startTime = System.currentTimeMillis();
        try (InputStream is = urlConnection.getInputStream()) {
            doc = db.parse(is);
        } catch (IOException ioe) {
            sourceLog.log("DownloadIOException", ioe.getMessage());
            severeDownload(url, ioe.getMessage());
            throw new StateErrorException(CollectResult.INFO_DOWNLOADERROR_STATE);
        } catch (SAXException saxe) {
            sourceLog.log("XMLError", saxe.getMessage());
            severeDownload(url, saxe.getMessage());
            throw new StateErrorException(CollectResult.INFO_NOTXML_STATE);
        }
        sourceLog.printIntervalle("DownloadTime", System.currentTimeMillis(), startTime);
        String infoUrlString = url.toString();
        int idx = infoUrlString.lastIndexOf('/');
        String urlRoot = infoUrlString.substring(0, idx + 1);
        return readScrutariInfoDOM(doc.getDocumentElement(), urlRoot, messageHandler);
    }

    private void checkConnection(HttpURLConnection urlConnection) {
        urlConnection.setInstanceFollowRedirects(true);
        urlConnection.setRequestProperty("User-Agent", userAgent);
    }

    private static ScrutariInfoContent readScrutariInfoDOM(Element element_xml, String urlRoot, MultiMessageHandler messageHandler) throws StateErrorException {
        String dateString = null;
        List<Object> fragmentList = new ArrayList<Object>();
        NodeList nodeList = element_xml.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) node;
                String tagname = el.getTagName();
                if (tagname.equals("date")) {
                    dateString = XMLUtils.getData(el);
                } else if ((tagname.equals("path")) || (tagname.equals("scrutaridata-url"))) {
                    String pathString = XMLUtils.getData(el);
                    if (pathString.length() > 0) {
                        try {
                            URL dataUrl = buildDataUrl(pathString, urlRoot);
                            fragmentList.add(dataUrl);
                        } catch (MalformedURLException mue) {
                            DomMessages.wrongElementValue(messageHandler, "scrutaridata-url", pathString);
                            throw new StateErrorException(CollectResult.INFO_WRONGXML_STATE);
                        }
                    }
                } else if (tagname.equals("xml-fragment")) {
                    String xmlFragment = XMLUtils.getData(el);
                    if (xmlFragment.length() > 0) {
                        fragmentList.add(xmlFragment);
                    }
                }
            }
        }
        boolean wrongXml = false;
        if (dateString == null) {
            DomMessages.missingChildTag(messageHandler, element_xml.getTagName(), "date");
            wrongXml = true;
        }
        if (fragmentList.isEmpty()) {
            DomMessages.missingChildTag(messageHandler, element_xml.getTagName(), "scrutaridata-url");
            wrongXml = true;
        }
        if (wrongXml) {
            throw new StateErrorException(CollectResult.INFO_WRONGXML_STATE);
        }
        FuzzyDate date;
        try {
            date = FuzzyDate.parse(dateString);
        } catch (ParseException pe) {
            DomMessages.wrongElementValue(messageHandler, "date", dateString);
            throw new StateErrorException(CollectResult.INFO_WRONGXML_STATE);
        }
        return new InternalScrutariInfoContent(date, Collections.unmodifiableList(fragmentList));
    }

    private static URL buildDataUrl(String pathString, String urlRoot) throws MalformedURLException {
        try {
            URL url = new URL(pathString);
            return url;
        } catch (MalformedURLException mue) {
            URL url = new URL(urlRoot + pathString);
            return url;
        }
    }

    private void severeDownload(URL url, String message) {
        messageHandler.addMessage("severe.download", "_ error.exception.download", url.toString(), message);
    }

    private static String getLevelString(int level) {
        switch (level) {
            case ScrutariSourceManager.LOCAL:
                return "local";
            case ScrutariSourceManager.CHECK:
                return "check";
            case ScrutariSourceManager.FORCE:
                return "force";
            default:
                return "unknown";
        }
    }

    private static String getTypeString(short type) {
        switch (type) {
            case ScrutariSourceDef.INFO_TYPE:
                return "info";
            case ScrutariSourceDef.SCRUTARIDATA_TYPE:
                return "scrutaridata";
            default:
                return "unknown";
        }
    }


    private static class InternalScrutariInfoContent implements ScrutariInfoContent {

        private final FuzzyDate date;
        private final List<Object> fragmentList;

        private InternalScrutariInfoContent(FuzzyDate date, List<Object> fragmentList) {
            this.date = date;
            this.fragmentList = fragmentList;
        }

        @Override
        public FuzzyDate getDate() {
            return date;
        }

        @Override
        public List<Object> getDataFragmentList() {
            return fragmentList;
        }

    }


    private static class InternalMatchedData implements EngineStorage.MatchedData {

        private final File tempFile;
        private final File firstTempFile;
        private final URL url;

        private InternalMatchedData(URL url, File tempFile, File firstTempFile) {
            this.url = url;
            this.tempFile = tempFile;
            this.firstTempFile = firstTempFile;
        }

        private InternalMatchedData(URL url) {
            this.url = url;
            this.tempFile = null;
            this.firstTempFile = null;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            if (tempFile == null) {
                return null;
            }
            return new FileInputStream(tempFile);
        }

        @Override
        public URL getMatchingURL() {
            return url;
        }

        @Override
        public boolean isNotChanged() {
            return (tempFile == null);
        }

        private void delete() {
            if (tempFile != null) {
                tempFile.delete();
            }
            if (firstTempFile != null) {
                tempFile.delete();
            }
        }

    }


    private static class SourceLog {

        private final PrintWriter pw;

        private SourceLog(PrintWriter pw) {
            this.pw = pw;
        }

        public void chapter(String name) {
            pw.println();
            pw.print("[");
            pw.print(name);
            pw.println("]");
            pw.flush();
        }


        public void log(String key, String value) {
            pw.print(key);
            pw.print(": ");
            pw.println(value);
            pw.flush();
        }

        public void log(String key, int value) {
            pw.print(key);
            pw.print(": ");
            pw.println(value);
            pw.flush();
        }

        public void close() {
            pw.close();
        }

        private void printIntervalle(String key, long currentTime, long oldTime) {
            pw.print(key);
            pw.print(": ");
            int diff = (int) (currentTime - oldTime);
            if (diff == 0) {
                pw.print("00:00");
            } else {
                double ms = ((double) diff) / 1000;
                double s = Math.floor(ms);
                ms = (ms - s) * 1000;
                if (s > 0) {
                    double mn = Math.floor(s / 60);
                    s = s - (mn * 60);
                    if (mn < 10) {
                        pw.print("0");
                    }
                    pw.print(String.valueOf((int) mn));
                } else {
                    pw.print("00");
                }
                pw.print(":");
                if (s < 10) {
                    pw.print("0");
                }
                pw.print(String.valueOf((int) s));
                if (ms > 0) {
                    pw.print(",");
                    if (ms < 10) {
                        pw.print("0");
                    }
                    if (ms < 100) {
                        pw.print("0");
                    }
                    pw.print(String.valueOf((int) ms));
                }
            }
            pw.println();
            pw.flush();
        }

    }

}
