/* SctServer - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import fr.exemole.sctserver.tools.feed.BaseWrapper;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.BaseData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;


/**
 *
 * @author Vincent Calame
 */
public class ExportCache {

    private final DataAccess dataAccess;
    private final Lang lang;
    private final Map<Integer, BaseWrapper> baseWrapperMap = new HashMap<Integer, BaseWrapper>();
    private final Map<Integer, String> motcleLabelMap = new HashMap<Integer, String>();

    public ExportCache(DataAccess dataAccess, Lang lang) {
        this.dataAccess = dataAccess;
        this.lang = lang;
    }

    public BaseWrapper getBaseWrapper(Integer baseCode) {
        BaseWrapper baseWrapper = baseWrapperMap.get(baseCode);
        if (baseWrapper != null) {
            return baseWrapper;
        }
        BaseData baseData = dataAccess.getBaseData(baseCode);
        baseWrapper = BaseWrapper.wrapBase(baseData, lang);
        baseWrapperMap.put(baseCode, baseWrapper);
        return baseWrapper;
    }

    public String getIndexationString(Integer ficheCode) {
        StringBuilder buf = new StringBuilder();
        FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            List<Integer> indexationMotcleCodeList = ficheInfo.getIndexationCodeList(thesaurusData.getThesaurusCode());
            for (Integer motcleCode : indexationMotcleCodeList) {
                String libelleString = getMotcleLabel(motcleCode);
                if (libelleString.length() > 0) {
                    if (buf.length() > 0) {
                        buf.append("; ");
                    }
                    buf.append(libelleString);
                }
            }
        }
        return buf.toString();
    }

    private String getMotcleLabel(Integer motcleCode) {
        String labelString = motcleLabelMap.get(motcleCode);
        if (labelString != null) {
            return labelString;
        }
        MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
        motcleLabelMap.put(motcleCode, motcleData.getLabels().seekLabelString(lang, ""));
        return labelString;
    }

}
