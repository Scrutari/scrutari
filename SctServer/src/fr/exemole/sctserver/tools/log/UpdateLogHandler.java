/* SctServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.log;

import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.MessageLog;
import net.mapeadores.util.logging.MessageLogBuilder;
import net.mapeadores.util.logging.MultiMessageHandler;
import net.mapeadores.util.xml.DomMessages;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class UpdateLogHandler implements MultiMessageHandler {

    private final static int LOG_LIMIT = 25;
    private final MessageLogBuilder builder = new MessageLogBuilder();
    private final Set<String> exceptionOnlySet = new HashSet<String>();
    private String currentURI;
    private int keyOccurrence = 0;
    private boolean logExceptionOnly = false;

    public UpdateLogHandler() {
    }

    public void setCurrentScrutariSource(String name) {
        setCurrentSource(name);
    }

    public void exception(Exception e) {
        flush();
        this.logExceptionOnly = true;
        exceptionOnlySet.add(currentURI);
        if (e instanceof SAXException) {
            DomMessages.saxException(builder, (SAXException) e);
        } else {
            DomMessages.malformed(builder, "_ error.exception.io_withmessage", e.getClass().getName(), e.getMessage());
        }
    }

    @Override
    public void addMessage(String category, Message message) {
        if (!logExceptionOnly) {
            if (keyOccurrence < LOG_LIMIT) {
                builder.addMessage(category, message);
                keyOccurrence++;
            }
        }
    }

    @Override
    public void setCurrentSource(String uri) {
        flush();
        this.currentURI = uri;
        if (exceptionOnlySet.contains(uri)) {
            this.logExceptionOnly = true;
        } else {
            this.logExceptionOnly = false;
        }
        builder.setCurrentSource(uri);
    }

    public MessageLog toMessageLog() {
        flush();
        return builder.toMessageLog();
    }

    private void flush() {
        if (keyOccurrence >= LOG_LIMIT) {
            builder.addMessage("severe.more", "_ error.list.moreerrors", (keyOccurrence - 10));
        }
        this.keyOccurrence = 0;
    }

}
