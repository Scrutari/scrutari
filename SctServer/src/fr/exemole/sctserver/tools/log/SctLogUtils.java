/* SctEngine - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.log;

import fr.exemole.sctserver.api.log.QLog;
import fr.exemole.sctserver.api.log.SctLogConstants;
import fr.exemole.sctserver.api.storage.LogStorage;
import net.scrutari.searchengine.api.result.FicheSearchResult;


/**
 *
 * @author Vincent Calame
 */
public final class SctLogUtils {

    private SctLogUtils() {
    }

    public static boolean isValidLogDir(String dir) {
        switch (dir) {
            case SctLogConstants.CONF_DIR:
            case SctLogConstants.LOAD_DIR:
            case SctLogConstants.EXCEPTION_DIR:
            case SctLogConstants.DATA_DIR:
            case SctLogConstants.COLLECT_DIR:
                return true;
            default:
                return false;
        }
    }

    public static void cleanDir(LogStorage logStorage, String dir) {
        for (String name : logStorage.getAvailableLogs(dir)) {
            logStorage.log(dir, name, null);
        }
    }

    public static QLog toQLog(FicheSearchResult ficheSearchResult, String origin) {
        return new InternalQLog(ficheSearchResult.getFicheSearchSource().getCanonicalQ().toString(), origin, ficheSearchResult.getFicheCount(), ficheSearchResult.getFicheMaximum());
    }


    private static class InternalQLog implements QLog {

        private final String q;
        private final String origin;
        private final int ficheCount;
        private final int ficheMaximum;

        private InternalQLog(String q, String origin, int ficheCount, int ficheMaximum) {
            this.q = q;
            this.origin = origin;
            this.ficheCount = ficheCount;
            this.ficheMaximum = ficheMaximum;
        }

        @Override
        public String getQ() {
            return q;
        }

        @Override
        public String getOrigin() {
            return origin;
        }

        @Override
        public int getFicheCount() {
            return ficheCount;
        }

        @Override
        public int getFicheMaximum() {
            return ficheMaximum;
        }

    }

}
