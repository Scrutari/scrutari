/* ScrutariLib_SearchEngine - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;
import net.mapeadores.util.text.CleanedString;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.tools.options.SearchOptionsBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class BuildingUtils {

    public final static CheckedNameSpace CATEGORY_NAMESPACE = CheckedNameSpace.build("category");
    public final static AttributeKey CATEGORY_LIST_KEY = AttributeKey.build(CATEGORY_NAMESPACE, "list");
    public final static AttributeKey CATEGORY_MODE_KEY = AttributeKey.build(CATEGORY_NAMESPACE, "mode");
    public final static CleanedString EXCLUDE_MODE = CleanedString.newInstance("mode");
    private final static Set<Integer> EMPTY_SET = Collections.emptySet();

    private BuildingUtils() {

    }

    public static void computeReduction(SearchOptionsBuilder searchOptionsBuilder, DataAccess dataAccess, GlobalSearchOptions globalSearchOptions) {
        SearchOptionsDef searchOptionsDef = searchOptionsBuilder.getSearchOptionsDef();
        Collection<Integer> corpusCodes = computeCorpusCodeList(dataAccess, searchOptionsDef);
        if (corpusCodes != null) {
            searchOptionsBuilder.reduceCorpusCodes(corpusCodes);
        }
        Collection<Integer> corpusCategoryCodes = computeCategory(dataAccess, searchOptionsDef, globalSearchOptions);
        if (corpusCategoryCodes != null) {
            searchOptionsBuilder.reduceCorpusCodes(corpusCategoryCodes);
        }
        ListReduction thesaurusListReduction = searchOptionsDef.getListReduction(ScrutariDataURI.THESAURUSURI_TYPE);
        if (thesaurusListReduction != null) {
            Set<Integer> thesaurusSet = toSet(dataAccess, thesaurusListReduction);
            Collection<Integer> thesaurusCodes;
            if (thesaurusListReduction.isExclude()) {
                thesaurusCodes = excludeThesaurus(dataAccess, thesaurusSet);
            } else {
                thesaurusCodes = thesaurusSet;
            }
            searchOptionsBuilder.reduceThesaurusCodes(thesaurusCodes);
        }
    }

    private static Collection<Integer> computeCategory(DataAccess dataAccess, SearchOptionsDef searchOptionsDef, GlobalSearchOptions globalSearchOptions) {
        Attribute attribute = searchOptionsDef.getAttributes().getAttribute(CATEGORY_LIST_KEY);
        if (attribute == null) {
            return null;
        }
        boolean exclude = false;
        Attribute modeAttribute = searchOptionsDef.getAttributes().getAttribute(CATEGORY_MODE_KEY);
        if (modeAttribute != null) {
            exclude = modeAttribute.getFirstValue().equals(EXCLUDE_MODE.toString());
        }
        int categoryLength = attribute.size();
        List<Integer> selectionList = new ArrayList<Integer>();
        if (exclude) {
            Set<Integer> excludeSet = new HashSet<Integer>();
            for (int i = 0; i < categoryLength; i++) {
                String categoryName = attribute.get(i);
                Category category = globalSearchOptions.getCategoryByName(categoryName);
                if (category != null) {
                    for (Integer corpusCode : category.getCorpusCodeList()) {
                        excludeSet.add(corpusCode);
                    }
                }
            }
            for (CorpusData corpusData : dataAccess.getCorpusDataList()) {
                if (!excludeSet.contains(corpusData.getCorpusCode())) {
                    selectionList.add(corpusData.getCorpusCode());
                }
            }
        } else {
            for (int i = 0; i < categoryLength; i++) {
                String categoryName = attribute.get(i);
                Category category = globalSearchOptions.getCategoryByName(categoryName);
                if (category != null) {
                    for (Integer corpusCode : category.getCorpusCodeList()) {
                        selectionList.add(corpusCode);
                    }
                }
            }
        }
        return selectionList;
    }

    private static Collection<Integer> computeCorpusCodeList(DataAccess dataAccess, SearchOptionsDef searchOptionsDef) {
        ListReduction baseListReduction = searchOptionsDef.getListReduction(ScrutariDataURI.BASEURI_TYPE);
        ListReduction corpusListReduction = searchOptionsDef.getListReduction(ScrutariDataURI.CORPUSURI_TYPE);
        boolean withBaseReduction = (baseListReduction != null);
        boolean withCorpusReduction = (corpusListReduction != null);
        if (!withBaseReduction && !withCorpusReduction) {
            return null;
        }
        if ((!withBaseReduction) && (!corpusListReduction.isExclude())) {
            return toSet(dataAccess, corpusListReduction);
        } else {
            Filter filter;
            if ((withBaseReduction) && (withCorpusReduction)) {
                filter = new BothFilter(toSet(dataAccess, baseListReduction), baseListReduction.isExclude(), toSet(dataAccess, corpusListReduction), corpusListReduction.isExclude());
            } else if (withBaseReduction) {
                filter = new BaseFilter(toSet(dataAccess, baseListReduction), baseListReduction.isExclude());
            } else {
                filter = new CorpusFilter(toSet(dataAccess, corpusListReduction), corpusListReduction.isExclude());
            }
            List<Integer> resultList = new ArrayList<Integer>();
            for (CorpusData corpusData : dataAccess.getCorpusDataList()) {
                if (filter.accept(corpusData)) {
                    resultList.add(corpusData.getCorpusCode());
                }
            }
            return resultList;
        }
    }


    private static Collection<Integer> excludeThesaurus(DataAccess dataAccess, Set<Integer> excludeThesaurusSet) {
        List<Integer> resultList = new ArrayList<Integer>();
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            Integer thesaurusCode = thesaurusData.getThesaurusCode();
            if (!excludeThesaurusSet.contains(thesaurusCode)) {
                resultList.add(thesaurusCode);
            }
        }
        return resultList;
    }

    private static Set<Integer> toSet(DataAccess dataAccess, ListReduction listReduction) {
        List<ScrutariDataURI> uriList = listReduction.getScrutariDataURIList();
        if (uriList.isEmpty()) {
            return EMPTY_SET;
        }
        Set<Integer> set = new HashSet<Integer>();
        for (ScrutariDataURI uri : uriList) {
            Integer code = dataAccess.getCode(uri);
            if (code != null) {
                set.add(code);
            }
        }
        return set;
    }


    private static abstract class Filter {

        protected abstract boolean accept(CorpusData corpusData);

    }


    private static class CorpusFilter extends Filter {

        private final Set<Integer> corpusSet;
        private final boolean corpusExclude;

        private CorpusFilter(Set<Integer> corpusSet, boolean corpusExclude) {
            this.corpusSet = corpusSet;
            this.corpusExclude = corpusExclude;
        }

        @Override
        protected boolean accept(CorpusData corpusData) {
            boolean here = corpusSet.contains(corpusData.getCorpusCode());
            if (corpusExclude) {
                return !here;
            } else {
                return here;
            }
        }

    }


    private static class BaseFilter extends Filter {

        private final Set<Integer> baseSet;
        private final boolean baseExclude;

        private BaseFilter(Set<Integer> baseSet, boolean baseExclude) {
            this.baseSet = baseSet;
            this.baseExclude = baseExclude;
        }

        @Override
        protected boolean accept(CorpusData corpusData) {
            boolean here = baseSet.contains(corpusData.getBaseCode());
            if (baseExclude) {
                return !here;
            } else {
                return here;
            }
        }

    }


    private static class BothFilter extends Filter {

        private final Set<Integer> baseSet;
        private final Set<Integer> corpusSet;
        private final boolean baseExclude;
        private final boolean corpusExclude;

        private BothFilter(Set<Integer> baseSet, boolean baseExclude, Set<Integer> corpusSet, boolean corpusExclude) {
            this.baseSet = baseSet;
            this.baseExclude = baseExclude;
            this.corpusSet = corpusSet;
            this.corpusExclude = false;
        }

        @Override
        protected boolean accept(CorpusData corpusData) {
            boolean baseHere = baseSet.contains(corpusData.getBaseCode());
            boolean corpusHere = corpusSet.contains(corpusData.getCorpusCode());
            if (baseExclude) {
                if (corpusExclude) {
                    if (baseHere) {
                        return false;
                    } else if (corpusHere) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    if (corpusHere) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                if (corpusExclude) {
                    if (corpusHere) {
                        return false;
                    } else if (baseHere) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    if (corpusHere) {
                        return true;
                    } else if (baseHere) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }

    }

}
