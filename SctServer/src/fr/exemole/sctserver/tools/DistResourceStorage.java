/* SctServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import fr.exemole.sctserver.ResourceReference;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.ParseException;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.ResourceFolder;
import net.mapeadores.util.io.ResourceFolderBuilder;
import net.mapeadores.util.io.ResourceStorage;
import net.mapeadores.util.io.docstream.ClassResourceDocStream;
import net.mapeadores.util.mimetype.MimeTypeResolver;
import net.mapeadores.util.mimetype.MimeTypeUtils;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class DistResourceStorage implements ResourceStorage {

    private final ResourceFolder root;

    private DistResourceStorage(ResourceFolder root) {
        this.root = root;
    }

    public static ResourceStorage newInstance() {
        RootBuilder rootBuilder = new RootBuilder();
        rootBuilder.addList(ResourceReference.class, "");
        return new DistResourceStorage(rootBuilder.toResourceFolder());
    }

    @Override
    public String getName() {
        return "dist";
    }

    @Override
    public boolean containsResource(RelativePath path) {
        URL url = ResourceReference.class.getResource("resources/" + path.getPath());
        return (url != null);
    }

    @Override
    public DocStream getResourceDocStream(RelativePath path, MimeTypeResolver mimeTypeResolver) {
        ClassResourceDocStream classResourceDocStream = ClassResourceDocStream.newInstance(ResourceReference.class, "resources/" + path.getPath());
        if (classResourceDocStream == null) {
            return null;
        }
        String mimeType = MimeTypeUtils.getMimeType(mimeTypeResolver, path.getLastName());
        classResourceDocStream.setMimeType(mimeType);
        return classResourceDocStream;
    }

    @Override
    public ResourceFolder getRoot() {
        return root;
    }


    private static class RootBuilder {

        private final ResourceFolderBuilder rootResourceFolderBuilder = new ResourceFolderBuilder("");

        private RootBuilder() {

        }

        private void addList(Class referenceClass, String startPath) {
            if (referenceClass.getResource("resources/list.txt") != null) {
                try (BufferedReader reader = new BufferedReader(new InputStreamReader(referenceClass.getResourceAsStream("resources/list.txt"), "UTF-8"))) {
                    String ligne;
                    while ((ligne = reader.readLine()) != null) {
                        ligne = ligne.trim();
                        if (ligne.length() > 0) {
                            try {
                                addPath(RelativePath.parse(startPath + ligne));
                            } catch (ParseException pe) {

                            }
                        }
                    }
                } catch (IOException ioe) {
                }
            }
        }

        private void addPath(RelativePath relativePath) {
            String[] array = relativePath.toArray();
            int length = array.length;
            if (length > 1) {
                int lastIndex = length - 1;
                ResourceFolderBuilder subfolderBuilder = rootResourceFolderBuilder.getSubfolderBuilder(array[0]);
                for (int i = 1; i < lastIndex; i++) {
                    subfolderBuilder = subfolderBuilder.getSubfolderBuilder(array[i]);
                }
                subfolderBuilder.addResource(array[lastIndex]);
            }
        }


        private ResourceFolder toResourceFolder() {
            return rootResourceFolderBuilder.toRessourceFolder();
        }

    }

}
