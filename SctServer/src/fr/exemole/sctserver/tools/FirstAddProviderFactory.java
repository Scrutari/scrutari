/* SctServer - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import fr.exemole.sctserver.api.storage.EngineStorage;
import java.util.HashMap;
import java.util.Map;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.MotcleNode;
import net.scrutari.datauri.tree.ThesaurusNode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.tools.FirstAddProvider;


/**
 *
 * @author Vincent Calame
 */
public final class FirstAddProviderFactory {

    private FirstAddProviderFactory() {

    }

    public static FirstAddProvider build(EngineStorage engineStorage, ScrutariDataURIChecker checker, ScrutariDBName defaultDBName) {
        Map<ScrutariDataURI, ScrutariDBName> result = new HashMap<ScrutariDataURI, ScrutariDBName>();
        ScrutariDBName[] array = engineStorage.getAddScrutariDBNameArray();
        int length = array.length;
        for (int i = (length - 1); i >= 0; i--) {
            ScrutariDBName scrutariDBName = array[i];
            URITree uriTree = engineStorage.getAddedURITree(scrutariDBName, checker);
            populate(uriTree, scrutariDBName, result);
        }
        return new FirstAddProvider(result, defaultDBName);
    }

    private static void populate(URITree uriTree, ScrutariDBName scrutariDBName, Map<ScrutariDataURI, ScrutariDBName> result) {
        for (BaseNode baseNode : uriTree.getBaseNodeList()) {
            populate(baseNode.getBaseURI(), scrutariDBName, result);
            for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
                populate(corpusNode.getCorpusURI(), scrutariDBName, result);
                for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
                    populate(ficheNode.getFicheURI(), scrutariDBName, result);
                }
            }
            for (ThesaurusNode thesaurusNode : baseNode.getThesaurusNodeList()) {
                populate(thesaurusNode.getThesaurusURI(), scrutariDBName, result);
                for (MotcleNode motcleNode : thesaurusNode.getMotcleNodeList()) {
                    populate(motcleNode.getMotcleURI(), scrutariDBName, result);
                }
            }
        }
    }

    private static void populate(ScrutariDataURI scrutariDataURI, ScrutariDBName scrutariDBName, Map<ScrutariDataURI, ScrutariDBName> result) {
        if (!result.containsKey(scrutariDataURI)) {
            result.put(scrutariDataURI, scrutariDBName);
        }
    }

}
