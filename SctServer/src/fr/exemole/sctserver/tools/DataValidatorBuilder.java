/* SctServer - Copyright (c) 2017-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.text.StringChecker;
import net.mapeadores.util.text.StringCheckerUtils;
import net.mapeadores.util.text.StringSplitter;
import net.scrutari.data.DataFieldKey;
import net.scrutari.data.DataValidator;


/**
 *
 * @author Vincent Calame
 */
public class DataValidatorBuilder {

    private final static List<DataFieldKey> EMPTY_LIST = Collections.emptyList();
    private final static List<AttributeKey> EMPTY_ATTRIBUTEKEYLIST = Collections.emptyList();
    public final static DataValidator NEUTRAL_DATAVALIDATOR = new NeutralDataValidator();
    private final Set<AttributeKey> uniqueSet = new LinkedHashSet<AttributeKey>();
    private final Set<DataFieldKey> mandatorySet = new LinkedHashSet<DataFieldKey>();
    private final Map<DataFieldKey, StringChecker> lengthCheckerMap = new LinkedHashMap<DataFieldKey, StringChecker>();
    private final Map<AttributeKey, StringChecker> formatCheckerMap = new LinkedHashMap<AttributeKey, StringChecker>();
    private final Map<AttributeKey, StringSplitter> splitterMap = new LinkedHashMap<AttributeKey, StringSplitter>();

    public DataValidatorBuilder() {

    }

    public void addMandatory(DataFieldKey dataFieldKey) {
        if (dataFieldKey == null) {
            throw new IllegalArgumentException("dataFieldKey is null");
        }
        mandatorySet.add(dataFieldKey);
    }

    public void putLength(DataFieldKey dataFieldKey, int min, int max) {
        if (dataFieldKey == null) {
            throw new IllegalArgumentException("dataFieldKey is null");
        }
        if (min < 0) {
            min = 0;
        }
        if (max < min) {
            max = min;
        }
        lengthCheckerMap.put(dataFieldKey, StringCheckerUtils.newLengthChecker(min, max));
    }

    public void putFormatChecker(DataFieldKey dataFieldKey, StringChecker formatChecker) {
        AttributeKey attributeKey = dataFieldKey.getAttributeKey();
        if (attributeKey == null) {
            return;
        }
        if (formatChecker == null) {
            formatCheckerMap.remove(attributeKey);
        } else {
            formatCheckerMap.put(attributeKey, formatChecker);
        }
    }

    public void putSplitter(DataFieldKey dataFieldKey, StringSplitter stringSplitter) {
        AttributeKey attributeKey = dataFieldKey.getAttributeKey();
        if (attributeKey == null) {
            return;
        }
        if (stringSplitter == null) {
            splitterMap.remove(attributeKey);
        } else {
            splitterMap.put(attributeKey, stringSplitter);
        }

    }

    public void addUnique(DataFieldKey dataFieldKey) {
        if (dataFieldKey == null) {
            throw new IllegalArgumentException("dataFieldKey is null");
        }
        AttributeKey attributeKey = dataFieldKey.getAttributeKey();
        if (attributeKey != null) {
            addUnique(attributeKey);
        }
    }

    public void addUnique(AttributeKey attributeKey) {
        if (attributeKey == null) {
            throw new IllegalArgumentException("attributeKey is null");
        }
        uniqueSet.add(attributeKey);
    }

    public DataValidator toDataValidator() {
        Map<Short, List<DataFieldKey>> dataFieldKeyMap = new HashMap<Short, List<DataFieldKey>>();
        dataFieldKeyMap.put(DataValidator.MANDATORY_VALIDATION, toDataFieldKeyList(mandatorySet));
        dataFieldKeyMap.put(DataValidator.LENGTH_VALIDATION, toDataFieldKeyList(lengthCheckerMap.keySet()));
        dataFieldKeyMap.put(DataValidator.UNIQUE_VALIDATION, convertToDataFieldKeyList(uniqueSet));
        dataFieldKeyMap.put(DataValidator.FORMAT_VALIDATION, convertToDataFieldKeyList(formatCheckerMap.keySet()));
        dataFieldKeyMap.put(DataValidator.SPLIT_VALIDATION, convertToDataFieldKeyList(splitterMap.keySet()));
        return new InternalDataValidator(new HashSet<DataFieldKey>(mandatorySet), new HashMap<DataFieldKey, StringChecker>(lengthCheckerMap), new HashSet<AttributeKey>(uniqueSet), new HashMap<AttributeKey, StringChecker>(formatCheckerMap), new HashMap<AttributeKey, StringSplitter>(splitterMap), dataFieldKeyMap);
    }

    private List<DataFieldKey> toDataFieldKeyList(Collection<DataFieldKey> dataFieldKeys) {
        int size = dataFieldKeys.size();
        if (size == 0) {
            return EMPTY_LIST;
        }
        return new DataFieldKeyList(dataFieldKeys.toArray(new DataFieldKey[size]));
    }

    private List<DataFieldKey> convertToDataFieldKeyList(Collection<AttributeKey> attributeKeys) {
        int size = attributeKeys.size();
        if (size == 0) {
            return EMPTY_LIST;
        }
        DataFieldKey[] array = new DataFieldKey[size];
        int p = 0;
        for (AttributeKey attributeKey : attributeKeys) {
            array[p] = new DataFieldKey(attributeKey);
            p++;
        }
        return new DataFieldKeyList(array);
    }


    private static class NeutralDataValidator implements DataValidator {

        private NeutralDataValidator() {

        }

        @Override
        public boolean isMandatory(DataFieldKey dataFieldKey) {
            return false;
        }

        @Override
        public String checkLength(DataFieldKey dataFieldKey, String value) {
            return value;
        }

        @Override
        public boolean isUnique(AttributeKey attributeKey) {
            return false;
        }


        @Override
        public StringChecker getFormatChecker(AttributeKey attributeKey) {
            return null;
        }

        @Override
        public StringSplitter getSplitter(AttributeKey attributeKey) {
            return null;
        }

        @Override
        public List<DataFieldKey> getDataFieldKeyList(short concernedValidation) {
            return EMPTY_LIST;
        }

    }


    private static class InternalDataValidator implements DataValidator {

        private final Set<DataFieldKey> mandatorySet;
        private final Map<DataFieldKey, StringChecker> lengthCheckerMap;
        private final Set<AttributeKey> uniqueSet;
        private final Map<AttributeKey, StringChecker> formatCheckerMap;
        private final Map<AttributeKey, StringSplitter> splitterMap;
        private final Map<Short, List<DataFieldKey>> dataFieldKeyMap;

        private InternalDataValidator(Set<DataFieldKey> mandatorySet, Map<DataFieldKey, StringChecker> lengthCheckerMap, Set<AttributeKey> uniqueSet, Map<AttributeKey, StringChecker> formatCheckerMap, Map<AttributeKey, StringSplitter> splitterMap, Map<Short, List<DataFieldKey>> dataFieldKeyMap) {
            this.mandatorySet = mandatorySet;
            this.lengthCheckerMap = lengthCheckerMap;
            this.uniqueSet = uniqueSet;
            this.formatCheckerMap = formatCheckerMap;
            this.dataFieldKeyMap = dataFieldKeyMap;
            this.splitterMap = splitterMap;
        }

        @Override
        public boolean isMandatory(DataFieldKey dataFieldKey) {
            return mandatorySet.contains(dataFieldKey);
        }

        @Override
        public String checkLength(DataFieldKey dataFieldKey, String value) {
            if (value == null) {
                return null;
            }
            StringChecker checker = lengthCheckerMap.get(dataFieldKey);
            if (checker == null) {
                return value;
            }
            return checker.check(value);
        }

        @Override
        public boolean isUnique(AttributeKey attributeKey) {
            return uniqueSet.contains(attributeKey);
        }

        @Override
        public StringChecker getFormatChecker(AttributeKey attributeKey) {
            return formatCheckerMap.get(attributeKey);
        }

        @Override
        public StringSplitter getSplitter(AttributeKey attributeKey) {
            return splitterMap.get(attributeKey);
        }

        @Override
        public List<DataFieldKey> getDataFieldKeyList(short concernedValidation) {
            List<DataFieldKey> list = dataFieldKeyMap.get(concernedValidation);
            if (list == null) {
                return EMPTY_LIST;
            } else {
                return list;
            }
        }

    }


    private static class DataFieldKeyList extends AbstractList<DataFieldKey> implements RandomAccess {

        private final DataFieldKey[] array;

        private DataFieldKeyList(DataFieldKey[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public DataFieldKey get(int index) {
            return array[index];
        }

    }

}
