/* SctServer - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import fr.exemole.sctserver.ResourceReference;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.ini.IniParser;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisationFactory;


/**
 *
 * @author Vincent Calame
 */
public class SctMessageLocalisationFactory extends MessageLocalisationFactory {

    private SctMessageLocalisationFactory() {

    }

    private void add(Lang lang, String iniPath) {
        try (Reader reader = new InputStreamReader(ResourceReference.class.getResourceAsStream("resources/" + iniPath), "UTF-8")) {
            Map<String, String> stringMap = new HashMap<String, String>();
            IniParser.parseIni(reader, stringMap);
            for (Map.Entry<String, String> entry : stringMap.entrySet()) {
                addEntry(entry.getKey(), lang, entry.getValue());
            }
        } catch (IOException ioe) {
            throw new NestedIOException(ioe);
        }
    }

    public static SctMessageLocalisationFactory build() {
        SctMessageLocalisationFactory sctMessageLocalisationFactory = new SctMessageLocalisationFactory();
        InputStream inputStream = ResourceReference.class.getResourceAsStream("resources/list.txt");
        if (inputStream != null) {
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                String ligne;
                while ((ligne = reader.readLine()) != null) {
                    ligne = ligne.trim();
                    if ((ligne.startsWith("l10n/")) && (ligne.endsWith(".ini"))) {
                        String path = ligne.substring(5);
                        int idx = path.indexOf('/');
                        if (idx > 0) {
                            try {
                                Lang lang = Lang.parse(path.substring(0, idx));
                                sctMessageLocalisationFactory.add(lang, ligne);
                            } catch (ParseException pe) {

                            }
                        }
                    }
                }
            } catch (IOException ioe) {
            }
        }
        return sctMessageLocalisationFactory;
    }

}
