/* SctServer - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.tools.conf.dom.UriCodesDOMReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.CodeMatch;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
final class UriCodesConf {

    private CodeMatch[] codeMatchArray;
    private MessageSource messageSource;

    private UriCodesConf() {
    }

    CodeMatch[] getCodeMatchArray() {
        return codeMatchArray;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static UriCodesConf load(File f) throws IOException, ParserConfigurationException {
        UriCodesConf conf = new UriCodesConf();
        conf.init(f);
        return conf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        UriCodesBuilder builder = new UriCodesBuilder();
        UriCodesDOMReader domReader = new UriCodesDOMReader(builder, messageHandler);
        domReader.readUriCodes(document.getDocumentElement(), "");
        codeMatchArray = builder.toScrutariDataURICodeList();
        messageSource = messageHandler.toMessageSource();
    }


    private static class UriCodesBuilder implements UriCodesDOMReader.Builder {

        private final List<CodeMatch> codeMatchList = new ArrayList<CodeMatch>();
        private final Set<Integer> codeSet = new HashSet<Integer>();
        private final Set<ScrutariDataURI> uriSet = new HashSet<ScrutariDataURI>();


        public UriCodesBuilder() {

        }

        @Override
        public boolean containsCode(int code) {
            return codeSet.contains(code);
        }

        @Override
        public boolean containsURI(ScrutariDataURI scrutariDataURI) {
            return uriSet.contains(scrutariDataURI);
        }

        @Override
        public void addScrutariDataURI(int code, ScrutariDataURI scrutariDataURI) {
            codeSet.add(code);
            uriSet.add(scrutariDataURI);
            codeMatchList.add(new CodeMatch(code, scrutariDataURI));
        }

        public CodeMatch[] toScrutariDataURICodeList() {
            if (codeMatchList.isEmpty()) {
                return null;
            } else {
                return codeMatchList.toArray(new CodeMatch[codeMatchList.size()]);
            }
        }

    }


}
