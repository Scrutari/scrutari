/* SctServer - Copyright (c) 2009-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.tools.conf.dom.SourcesDOMReader;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
class SourcesConf {

    private ScrutariSourceDef[] scrutariSourceDefArray = null;
    private MessageSource messageSource;

    private SourcesConf() {
    }

    ScrutariSourceDef[] getScrutariSourceDefArray() {
        return scrutariSourceDefArray;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static SourcesConf load(File f) throws IOException, ParserConfigurationException {
        SourcesConf conf = new SourcesConf();
        conf.init(f);
        return conf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        scrutariSourceDefArray = null;
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        SourcesDOMReader domReader = new SourcesDOMReader(messageHandler);
        scrutariSourceDefArray = domReader.readSources(document.getDocumentElement(), "");
        messageSource = messageHandler.toMessageSource();
    }

}
