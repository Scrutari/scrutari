/* SctServer - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.tools.DataValidatorBuilder;
import fr.exemole.sctserver.tools.conf.dom.ValidationDOMReader;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.data.DataValidator;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
class ValidationConf {

    private DataValidator dataValidator = DataValidatorBuilder.NEUTRAL_DATAVALIDATOR;
    private MessageSource messageSource;

    private ValidationConf() {

    }

    DataValidator getDataValidator() {
        return dataValidator;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static ValidationConf load(File f) throws IOException, ParserConfigurationException {
        ValidationConf validationConf = new ValidationConf();
        validationConf.init(f);
        return validationConf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        DataValidatorBuilder dataValidatorBuilder = new DataValidatorBuilder();
        ValidationDOMReader validationDOMReader = new ValidationDOMReader(dataValidatorBuilder, messageHandler);
        validationDOMReader.readValidation(document.getDocumentElement(), "");
        dataValidator = dataValidatorBuilder.toDataValidator();
        messageSource = messageHandler.toMessageSource();
    }

}
