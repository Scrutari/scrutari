/* SctServer - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.tools.conf.dom.FieldsDOMReader;
import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
class FieldsConf {

    private Map<String, FieldVariant> map;
    private MessageSource messageSource;

    private FieldsConf() {

    }

    Map<String, FieldVariant> getFieldVariantMap() {
        return map;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static FieldsConf load(File f) throws IOException, ParserConfigurationException {
        FieldsConf fieldsConf = new FieldsConf();
        fieldsConf.init(f);
        return fieldsConf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        map = new LinkedHashMap<String, FieldVariant>();
        FieldsDOMReader domReader = new FieldsDOMReader(map, messageHandler);
        domReader.readFields(document.getDocumentElement(), "");
        messageSource = messageHandler.toMessageSource();
    }

}
