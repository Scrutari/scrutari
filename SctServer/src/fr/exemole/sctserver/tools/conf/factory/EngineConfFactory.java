/* SctServer - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.api.conf.ConfConstants;
import fr.exemole.sctserver.api.conf.ConfFileInfo;
import fr.exemole.sctserver.api.conf.ConfIOException;
import fr.exemole.sctserver.api.conf.EngineConf;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.logging.MessageSource;
import net.scrutari.data.DataValidator;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.db.api.CodeMatch;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.PertinencePonderation;


/**
 *
 * @author Vincent Calame
 */
public class EngineConfFactory {


    private EngineConfFactory() {
    }

    public static EngineConf fromDirectory(File confDir) {
        return new InternalEngineConf(confDir);
    }


    private static class InternalEngineConf implements EngineConf {

        private final File confDir;
        private final InternalConfFileInfo sourcesFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo metadataFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo categoriesFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo searchLogFilterFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo uriCodesFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo attributesFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo optionsFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo validationFileInfo = new InternalConfFileInfo();
        private final InternalConfFileInfo fieldsFileInfo = new InternalConfFileInfo();
        private ScrutariSourceDef[] scrutariSourceDefArray = null;
        private EngineMetadata engineMetadata = null;
        private CategoryDef[] categoryDefArray = null;
        private PertinencePonderation globalPertinencePonderation = null;
        private String[] pertinenceOrderArray = null;
        private CodeMatch[] initialCodeMatchArray = null;
        private Map<String, AttributeDef[]> attributeDefArrayMap = null;
        private DataValidator dataValidator = null;
        private Map<String, FieldVariant> fieldVariantMap = null;

        private InternalEngineConf(File confDir) {
            this.confDir = confDir;
        }

        @Override
        public ScrutariSourceDef[] getScrutariSourceDefArray() {
            return scrutariSourceDefArray;
        }

        @Override
        public CategoryDef[] getCategoryDefArray() {
            return categoryDefArray;
        }

        @Override
        public PertinencePonderation getGlobalPertinencePonderation() {
            return globalPertinencePonderation;
        }

        @Override
        public String[] getPertinenceOrderArray() {
            return pertinenceOrderArray;
        }

        @Override
        public CodeMatch[] getInitialCodeMatchArray(ScrutariDataURIChecker checker) {
            if (initialCodeMatchArray == null) {
                return null;
            }
            int length = initialCodeMatchArray.length;
            if (length == 0) {
                return null;
            }
            CodeMatch[] result = new CodeMatch[length];
            if (checker == null) {
                System.arraycopy(initialCodeMatchArray, 0, result, 0, length);
            } else {
                for (int i = 0; i < length; i++) {
                    CodeMatch codeMatch = initialCodeMatchArray[i];
                    ScrutariDataURI uri = checker.checkURI(codeMatch.getScrutariDataURI());
                    result[i] = new CodeMatch(codeMatch.getCode(), uri);
                }
            }
            return result;
        }

        @Override
        public EngineMetadata getEngineMetadata() {
            return engineMetadata;
        }

        @Override
        public Map<String, AttributeDef[]> getAttributeDefArrayMap() {
            return attributeDefArrayMap;
        }

        @Override
        public DataValidator getDataValidator() {
            return dataValidator;
        }

        @Override
        public Map<String, FieldVariant> getFieldVariantMap() {
            return fieldVariantMap;
        }

        @Override
        public void reload() throws ConfIOException {
            clear();
            if (!confDir.exists()) {
                return;
            }
            try {
                if (!confDir.isDirectory()) {
                    throw new ConfIOException("confDirectory is not a directory : " + confDir.getPath());
                }
                for (File f : confDir.listFiles()) {
                    if (!f.isDirectory()) {
                        String name = f.getName();
                        if ((name.length() > 4) && (name.endsWith(".xml"))) {
                            String baseName = name.substring(0, name.length() - 4);
                            switch (baseName) {
                                case ConfConstants.SOURCES_NAME:
                                    initSources(f);
                                    break;
                                case ConfConstants.METADATA_NAME:
                                    initMetadata(f);
                                    break;
                                case ConfConstants.CATEGORIES_NAME:
                                    initCategories(f);
                                    break;
                                case ConfConstants.URI_CODES_NAME:
                                    initUriCodes(f);
                                    break;
                                case ConfConstants.ATTRIBUTES_NAME:
                                    initAttributes(f);
                                    break;
                                case ConfConstants.OPTIONS_NAME:
                                    initOptions(f);
                                    break;
                                case ConfConstants.VALIDATION_NAME:
                                    initValidation(f);
                                    break;
                                case ConfConstants.FIELDS_NAME:
                                    initFields(f);
                                    break;
                            }
                        }
                    }
                }
            } catch (SecurityException | IOException | ParserConfigurationException e) {
                throw new ConfIOException(e);
            }
        }

        private void clear() {
            sourcesFileInfo.clear();
            metadataFileInfo.clear();
            categoriesFileInfo.clear();
            searchLogFilterFileInfo.clear();
            uriCodesFileInfo.clear();
            validationFileInfo.clear();
            scrutariSourceDefArray = null;
            categoryDefArray = null;
            initialCodeMatchArray = null;
            attributeDefArrayMap = null;
            dataValidator = null;
            fieldVariantMap = null;
        }

        private void initSources(File f) throws IOException, ParserConfigurationException {
            sourcesFileInfo.setHere(true);
            SourcesConf sourcesConf = SourcesConf.load(f);
            sourcesFileInfo.setMessageSource(sourcesConf.getMessageSource());
            scrutariSourceDefArray = sourcesConf.getScrutariSourceDefArray();
        }

        private void initMetadata(File f) throws IOException, ParserConfigurationException {
            metadataFileInfo.setHere(true);
            MetadataConf metadataConf = MetadataConf.load(f);
            metadataFileInfo.setMessageSource(metadataConf.getMessageSource());
            engineMetadata = metadataConf.getEngineMetadata();
        }

        private void initCategories(File f) throws IOException, ParserConfigurationException {
            categoriesFileInfo.setHere(true);
            CategoriesConf categoriesConf = CategoriesConf.load(f);
            categoriesFileInfo.setMessageSource(categoriesConf.getMessageSource());
            categoryDefArray = categoriesConf.getCategoryDefArray();
        }

        private void initUriCodes(File f) throws IOException, ParserConfigurationException {
            uriCodesFileInfo.setHere(true);
            UriCodesConf uriCodesConf = UriCodesConf.load(f);
            uriCodesFileInfo.setMessageSource(uriCodesConf.getMessageSource());
            initialCodeMatchArray = uriCodesConf.getCodeMatchArray();
        }

        private void initAttributes(File f) throws IOException, ParserConfigurationException {
            attributesFileInfo.setHere(true);
            AttributesConf attributesConf = AttributesConf.load(f);
            attributesFileInfo.setMessageSource(attributesConf.getMessageSource());
            attributeDefArrayMap = attributesConf.getAttributeDefArrayMap();
        }

        private void initOptions(File f) throws IOException, ParserConfigurationException {
            optionsFileInfo.setHere(true);
            OptionsConf optionsConf = OptionsConf.load(f);
            optionsFileInfo.setMessageSource(optionsConf.getMessageSource());
            globalPertinencePonderation = optionsConf.getGlobalPertinencePonderation();
            pertinenceOrderArray = optionsConf.getPertinenceOrderArray();
        }

        private void initValidation(File f) throws IOException, ParserConfigurationException {
            validationFileInfo.setHere(true);
            ValidationConf validationConf = ValidationConf.load(f);
            validationFileInfo.setMessageSource(validationConf.getMessageSource());
            dataValidator = validationConf.getDataValidator();
        }

        private void initFields(File f) throws IOException, ParserConfigurationException {
            fieldsFileInfo.setHere(true);
            FieldsConf fieldsConf = FieldsConf.load(f);
            fieldsFileInfo.setMessageSource(fieldsConf.getMessageSource());
            fieldVariantMap = fieldsConf.getFieldVariantMap();
        }

        @Override
        public ConfFileInfo getConfFileInfo(String name) {
            switch (name) {
                case ConfConstants.SOURCES_NAME:
                    return sourcesFileInfo;
                case ConfConstants.METADATA_NAME:
                    return metadataFileInfo;
                case ConfConstants.CATEGORIES_NAME:
                    return categoriesFileInfo;
                case ConfConstants.URI_CODES_NAME:
                    return uriCodesFileInfo;
                case ConfConstants.ATTRIBUTES_NAME:
                    return attributesFileInfo;
                case ConfConstants.OPTIONS_NAME:
                    return optionsFileInfo;
                case ConfConstants.VALIDATION_NAME:
                    return validationFileInfo;
                case ConfConstants.FIELDS_NAME:
                    return fieldsFileInfo;
                default:
                    throw new IllegalArgumentException("Unknown name : " + name);
            }
        }

    }


    private static class InternalConfFileInfo implements ConfFileInfo {

        private boolean here = false;
        private MessageSource originMessageSource = null;

        private InternalConfFileInfo() {
        }

        private void clear() {
            this.here = false;
            this.originMessageSource = null;
        }

        @Override
        public boolean isHere() {
            return here;
        }

        @Override
        public String getName() {
            return originMessageSource.getName();
        }

        @Override
        public List<MessageSource.Entry> getEntryList() {
            return originMessageSource.getEntryList();
        }

        private void setHere(boolean here) {
            this.here = here;
        }

        private void setMessageSource(MessageSource messageSource) {
            this.originMessageSource = messageSource;
        }

    }


}
