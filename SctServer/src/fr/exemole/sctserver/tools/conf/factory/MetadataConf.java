/* SctServer - Copyright (c) 2016-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.tools.conf.dom.MetadataDOMReader;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class MetadataConf {

    private EngineMetadata engineMetadata;
    private MessageSource messageSource;

    private MetadataConf() {

    }

    EngineMetadata getEngineMetadata() {
        return engineMetadata;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static MetadataConf load(File f) throws IOException, ParserConfigurationException {
        MetadataConf conf = new MetadataConf();
        conf.init(f);
        return conf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        MetadataDOMReader metadataDOMReader = new MetadataDOMReader(messageHandler);
        engineMetadata = metadataDOMReader.readMetadata(document.getDocumentElement(), "");
        messageSource = messageHandler.toMessageSource();
    }

}
