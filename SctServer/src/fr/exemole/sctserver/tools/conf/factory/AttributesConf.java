/* SctServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.tools.conf.dom.AttributesDOMReader;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.db.api.ScrutariConstants;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
final class AttributesConf {

    private Map<String, AttributeDef[]> attributeDefListMap;
    private MessageSource messageSource;

    private AttributesConf() {
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    Map<String, AttributeDef[]> getAttributeDefArrayMap() {
        return attributeDefListMap;
    }

    static AttributesConf load(File f) throws IOException, ParserConfigurationException {
        AttributesConf conf = new AttributesConf();
        conf.init(f);
        return conf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        AttributesBuilder builder = new AttributesBuilder();
        AttributesDOMReader domReader = new AttributesDOMReader(builder, messageHandler);
        domReader.readAttributes(document.getDocumentElement(), "");
        attributeDefListMap = builder.toMap();
        messageSource = messageHandler.toMessageSource();
    }


    private static class AttributesBuilder implements AttributesDOMReader.Builder {

        private final Set<AttributeKey> keySet = new HashSet<AttributeKey>();
        private final Map<String, List<AttributeDef>> listMap = new HashMap<String, List<AttributeDef>>();
        private List<AttributeDef> currentList;

        private AttributesBuilder() {

        }

        @Override
        public boolean startGroup(String name) {
            if (ScrutariConstants.isValidGroupName(name)) {
                List<AttributeDef> list = listMap.get(name);
                if (list == null) {
                    list = new ArrayList<AttributeDef>();
                    listMap.put(name, list);
                }
                currentList = list;
                return true;
            } else {
                return false;
            }
        }

        @Override
        public boolean addAttributeDef(AttributeDef attributeDef) {
            AttributeKey attributeKey = attributeDef.getAttributeKey();
            if (keySet.contains(attributeKey)) {
                return false;
            }
            if (currentList == null) {
                throw new IllegalStateException("startGroup must be successfully invoked first");
            }
            currentList.add(attributeDef);
            keySet.add(attributeKey);
            return true;
        }

        private Map<String, AttributeDef[]> toMap() {
            Map<String, AttributeDef[]> map = new HashMap<String, AttributeDef[]>();
            for (Map.Entry<String, List<AttributeDef>> entry : listMap.entrySet()) {
                List<AttributeDef> list = entry.getValue();
                map.put(entry.getKey(), list.toArray(new AttributeDef[list.size()]));
            }
            return map;
        }

    }

}
