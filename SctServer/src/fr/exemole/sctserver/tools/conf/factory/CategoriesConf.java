/* SctServer - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.tools.conf.dom.CategoriesDOMReader;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.tools.options.CategoryDefBuilder;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
class CategoriesConf {

    private CategoryDef[] categoryDefArray;
    private MessageSource messageSource;


    private CategoriesConf() {
    }

    CategoryDef[] getCategoryDefArray() {
        return categoryDefArray;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static CategoriesConf load(File f) throws IOException, ParserConfigurationException {
        CategoriesConf conf = new CategoriesConf();
        conf.init(f);
        return conf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        categoryDefArray = null;
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        CategoryBuilder builder = new CategoryBuilder();
        CategoriesDOMReader domReader = new CategoriesDOMReader(builder, messageHandler);
        domReader.readCategories(document.getDocumentElement(), "");
        categoryDefArray = builder.flush();
        messageSource = messageHandler.toMessageSource();
    }


    private static class CategoryBuilder implements CategoriesDOMReader.Builder {

        private final Map<String, CategoryDefBuilder> map = new LinkedHashMap<String, CategoryDefBuilder>();
        private final Set<CorpusURI> uriSet = new HashSet<CorpusURI>();


        private CategoryBuilder() {

        }

        @Override
        public CategoryDefBuilder addCategory(String name) {
            CategoryDefBuilder category = map.get(name);
            if (!map.containsKey(name)) {
                category = new CategoryDefBuilder(name);
                map.put(name, category);
            }
            return category;
        }

        @Override
        public CategoryDefBuilder getCategory(String name) {
            return map.get(name);
        }


        @Override
        public boolean addCorpusURI(CategoryDefBuilder builder, CorpusURI corpusURI) {
            if (uriSet.contains(corpusURI)) {
                return false;
            }
            uriSet.add(corpusURI);
            builder.addCorpusURI(corpusURI);
            return true;
        }

        private CategoryDef[] flush() {
            if (!map.containsKey(CategoryDef.DEFAULT_NAME)) {
                map.put(CategoryDef.DEFAULT_NAME, new CategoryDefBuilder(CategoryDef.DEFAULT_NAME));
            }
            int count = map.size();
            CategoryDef[] categoryDefArray = new CategoryDef[count];
            int value = count;
            int p = 0;
            for (CategoryDefBuilder categoryDefBuilder : map.values()) {
                int rank = value * 10;
                value = value - 1;
                categoryDefBuilder.setRank(rank);
                categoryDefArray[p] = categoryDefBuilder.toCategoryDef();
                p++;
            }
            return categoryDefArray;
        }

    }


}
