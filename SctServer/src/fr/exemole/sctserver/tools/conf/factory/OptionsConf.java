/* SctServer - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.factory;

import fr.exemole.sctserver.tools.conf.dom.OptionsDOMReader;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.MessageSourceBuilder;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
class OptionsConf {

    private PertinencePonderation globalPertinencePonderation;
    private String[] pertinenceOrderArray;
    private MessageSource messageSource;

    private OptionsConf() {

    }

    PertinencePonderation getGlobalPertinencePonderation() {
        return globalPertinencePonderation;
    }

    String[] getPertinenceOrderArray() {
        return pertinenceOrderArray;
    }

    MessageSource getMessageSource() {
        return messageSource;
    }

    static OptionsConf load(File f) throws IOException, ParserConfigurationException {
        OptionsConf conf = new OptionsConf();
        conf.init(f);
        return conf;
    }

    private void init(File f) throws IOException, ParserConfigurationException {
        MessageSourceBuilder messageHandler = new MessageSourceBuilder(f.getPath());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document document;
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            document = db.parse(f);
        } catch (SAXException saxe) {
            DomMessages.saxException(messageHandler, saxe);
            messageSource = messageHandler.toMessageSource();
            return;
        }
        OptionsBuilder builder = new OptionsBuilder();
        OptionsDOMReader optionsDOMReader = new OptionsDOMReader(builder, messageHandler);
        optionsDOMReader.readOptions(document.getDocumentElement(), "");
        globalPertinencePonderation = builder.globalPertinencePonderation;
        pertinenceOrderArray = builder.pertinenceOrderArray;
        messageSource = messageHandler.toMessageSource();
    }


    private static class OptionsBuilder implements OptionsDOMReader.Builder {

        PertinencePonderation globalPertinencePonderation;
        String[] pertinenceOrderArray;

        private OptionsBuilder() {

        }

        @Override
        public void setGlobalPertinencePonderation(PertinencePonderation pertinencePonderation) {
            this.globalPertinencePonderation = pertinencePonderation;
        }

        @Override
        public void setPertinenceOrderArray(String[] pertinenceOrderArray) {
            this.pertinenceOrderArray = pertinenceOrderArray;
        }

    }

}
