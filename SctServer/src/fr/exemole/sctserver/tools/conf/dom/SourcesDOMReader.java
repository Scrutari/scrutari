/* SctServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.tools.collect.ScrutariSourceDefBuilder;
import fr.exemole.sctserver.tools.collect.UrlUnit;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class SourcesDOMReader {

    private final MessageHandler messageHandler;
    private final Map<String, ScrutariSourceDef> scrutariSourceDefMap = new LinkedHashMap<String, ScrutariSourceDef>();
    private final Set<BaseURI> baseURISet = new HashSet<BaseURI>();

    public SourcesDOMReader(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public ScrutariSourceDef[] readSources(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "source":
                    addSource(child, childXpath);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
        return scrutariSourceDefMap.values().toArray(new ScrutariSourceDef[scrutariSourceDefMap.size()]);
    }

    private void addSource(Element element, String xpath) {
        String name = element.getAttribute("name");
        if (name.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "name");
            return;
        }
        xpath = xpath + "[name='" + name + "']";
        if (!StringUtils.isTechnicalName(name, true)) {
            DomMessages.wrongAttributeValue(messageHandler, xpath, "name", name);
            return;
        }
        if (scrutariSourceDefMap.containsKey(name)) {
            DomMessages.invalid(messageHandler, "_ error.existing.xml.attributevalue", xpath, "name", name);
            return;
        }
        ScrutariSourceDefBuilder builder = new ScrutariSourceDefBuilder(name);
        DOMUtils.readChildren(element, new SourceConsumer(builder, xpath));
        if (builder.isEmpty()) {
            DomMessages.missingChildTag(messageHandler, xpath, "info-url|scrutaridata-url");
        } else {
            scrutariSourceDefMap.put(name, builder.toScrutariSourceDef());
        }
    }

    private BaseURI toBaseURI(Element element, String xpath) {
        String elString = XMLUtils.getData(element);
        String uriString = elString;
        if (!uriString.startsWith("base:")) {
            uriString = "base:" + uriString;
        }
        try {
            BaseURI baseURI = (BaseURI) URIParser.parse(uriString);
            if (baseURISet.contains(baseURI)) {
                DomMessages.invalid(messageHandler, "_ error.existing.xml.tagvalue", xpath, elString);
                return null;
            } else {
                baseURISet.add(baseURI);
                return baseURI;
            }
        } catch (URIParseException upe) {
            DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_uri", xpath, elString, upe.getMessage());
            return null;
        }
    }


    private class SourceConsumer implements Consumer<Element> {

        private final ScrutariSourceDefBuilder builder;
        private final String xpath;

        private SourceConsumer(ScrutariSourceDefBuilder builder, String xpath) {
            this.xpath = xpath;
            this.builder = builder;
        }

        @Override
        public void accept(Element child) {
            String tagName = child.getTagName();
            String childXpath = xpath + "/" + tagName;
            switch (tagName) {
                case "info-url": {
                    String urlString = XMLUtils.getData(child);
                    if (urlString.length() == 0) {
                        DomMessages.emptyElement(messageHandler, childXpath);
                    } else {
                        try {
                            URL url = new URL(urlString);
                            builder.addInfoUrl(url);
                        } catch (MalformedURLException mue) {
                            DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_url", childXpath, urlString, mue.getMessage());
                        }
                    }
                    break;
                }
                case "info": {
                    String transformation = child.getAttribute("transform").trim();
                    if (isValidTransformation(transformation)) {
                        UrlUnitConsumer unitConsumer = new UrlUnitConsumer(transformation, childXpath);
                        DOMUtils.readChildren(child, unitConsumer);
                        UrlUnit urlUnit = unitConsumer.toUrlUnit();
                        if (urlUnit != null) {
                            builder.addInfoUrl(urlUnit);
                        }
                    } else {
                        DomMessages.wrongAttributeValue(messageHandler, childXpath, "transform", transformation);
                    }
                    break;
                }
                case "scrutaridata-url": {
                    String urlString = XMLUtils.getData(child);
                    if (urlString.length() == 0) {
                        DomMessages.emptyElement(messageHandler, childXpath);
                    } else {
                        try {
                            URL url = new URL(urlString);
                            builder.addScrutariDataUrl(url, getFrequency(child));
                        } catch (MalformedURLException mue) {
                            DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_url", childXpath, urlString, mue.getMessage());
                        }
                    }
                    break;
                }
                case "scrutaridata": {
                    String transformation = child.getAttribute("transform").trim();
                    if (isValidTransformation(transformation)) {
                        UrlUnitConsumer unitConsumer = new UrlUnitConsumer(transformation, childXpath);
                        DOMUtils.readChildren(child, unitConsumer);
                        UrlUnit urlUnit = unitConsumer.toUrlUnit();
                        if (urlUnit != null) {
                            builder.addScrutariDataUrl(urlUnit, getFrequency(child));
                        }
                    } else {
                        DomMessages.wrongAttributeValue(messageHandler, childXpath, "transform", transformation);
                    }
                    break;
                }
                case "uri": {
                    BaseURI baseURI = toBaseURI(child, childXpath);
                    if (baseURI != null) {
                        builder.setMainBaseURI(baseURI);
                    }
                    break;
                }
                case "alias": {
                    BaseURI baseURI = toBaseURI(child, childXpath);
                    if (baseURI != null) {
                        builder.addAliasBaseURI(baseURI);
                    }
                    break;
                }
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }

        }

        private int getFrequency(Element element) {
            int frequency = 1;
            String freqString = element.getAttribute("freq");
            if (freqString.length() > 0) {
                try {
                    frequency = Integer.parseInt(freqString);
                } catch (NumberFormatException nfe) {
                    DomMessages.invalidWarning(messageHandler, "_ error.wrong.xml.attributevalue_integer", xpath + "/" + element.getTagName(), "freq", freqString);
                }
            }
            return frequency;
        }

    }


    private class UrlUnitConsumer implements Consumer<Element> {

        private final String transformation;
        private final String xpath;
        private URL url;
        private final Map<String, String> paramMap = new LinkedHashMap<String, String>();

        UrlUnitConsumer(String transformation, String xpath) {
            this.transformation = transformation;
            this.xpath = xpath;
        }

        @Override
        public void accept(Element child) {
            String tagName = child.getTagName();
            String childXpath = xpath + "/" + tagName;
            switch (tagName) {
                case "url": {
                    if (url == null) {
                        String urlString = XMLUtils.getData(child);
                        if (urlString.length() == 0) {
                            DomMessages.emptyElement(messageHandler, childXpath);
                        } else {
                            try {
                                url = new URL(urlString);
                            } catch (MalformedURLException mue) {
                                DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_url", childXpath, urlString, mue.getMessage());
                            }
                        }
                    }
                    break;
                }
                case "param": {
                    String name = child.getAttribute("name");
                    if (name.isEmpty()) {
                        DomMessages.emptyAttribute(messageHandler, childXpath, "name");
                    } else {
                        paramMap.put(name, child.getAttribute("value"));
                    }
                    break;
                }
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        }

        UrlUnit toUrlUnit() {
            if (url == null) {
                return null;
            }
            return new UrlUnit(url, transformation, paramMap);
        }

    }

    private static boolean isValidTransformation(String name) {
        switch (name) {
            case "":
            case "dsmd":
                return true;
            default:
                return false;
        }
    }

}
