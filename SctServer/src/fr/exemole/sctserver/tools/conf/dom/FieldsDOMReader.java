/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.tools.fieldvariant.AliasBuilder;
import fr.exemole.sctserver.tools.fieldvariant.FieldVariantBuilder;
import java.util.Map;
import java.util.function.Consumer;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class FieldsDOMReader {

    private final Map<String, FieldVariant> map;
    private final MessageHandler messageHandler;

    public FieldsDOMReader(Map<String, FieldVariant> map, MessageHandler messageHandler) {
        this.map = map;
        this.messageHandler = messageHandler;
    }

    public void readFields(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "variant":
                    String name = child.getAttribute("name");
                    if (name.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, childXpath, "name");
                    } else if (!StringUtils.isTechnicalName(name, true)) {
                        DomMessages.wrongAttributeValue(messageHandler, childXpath, "name", name);
                    } else if (map.containsKey(name)) {
                        DomMessages.invalid(messageHandler, "_ error.existing.xml.attributevalue", childXpath, "name", name);
                    } else {
                        FieldVariantBuilder fieldVariantBuilder = new FieldVariantBuilder(name);
                        DOMUtils.readChildren(child, new FieldVariantConsumer(fieldVariantBuilder, childXpath + "[@name='" + name + "']"));
                        map.put(name, fieldVariantBuilder.toFieldVariant());
                    }
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
    }


    private class FieldVariantConsumer implements Consumer<Element> {

        private final FieldVariantBuilder fieldVariantBuilder;
        private final String variantXPath;

        private FieldVariantConsumer(FieldVariantBuilder fieldVariantBuilder, String variantXPath) {
            this.fieldVariantBuilder = fieldVariantBuilder;
            this.variantXPath = variantXPath;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            String xpath = variantXPath + "/" + tagName;
            switch (tagName) {
                case "fiche":
                    String ficheFields = element.getAttribute("fields");
                    if (ficheFields.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, xpath, "fields");
                    } else {
                        fieldVariantBuilder.parseFicheFields(ficheFields);
                    }
                    break;
                case "motcle":
                    String motcleFields = element.getAttribute("fields");
                    if (motcleFields.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, xpath, "fields");
                    } else {
                        fieldVariantBuilder.parseMotcleFields(motcleFields);
                    }
                    break;
                case "alias":
                    String aliasName = element.getAttribute("name");
                    String fields = element.getAttribute("fields");
                    boolean ok = true;
                    if (aliasName.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, xpath, "name");
                        ok = false;
                    }
                    if (fields.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, xpath, "fields");
                        ok = false;
                    }
                    if (ok) {
                        AliasBuilder aliasBuilder = fieldVariantBuilder.getAliasBuilder(aliasName);
                        aliasBuilder.parseFields(fields);
                        Attr attr = (Attr) element.getAttributes().getNamedItem("separator");
                        if (attr != null) {
                            String separator = attr.getValue();
                            separator = separator.replace("\\n", "\n");
                            aliasBuilder.setSeparator(separator);
                        }
                    }
                    break;
            }
        }

    }

}
