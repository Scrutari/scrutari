/* SctServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeDefBuilder;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.data.SctSpace;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public class AttributesDOMReader {

    private final Builder builder;
    private final MessageHandler messageHandler;

    public AttributesDOMReader(Builder builder, MessageHandler messageHandler) {
        this.builder = builder;
        this.messageHandler = messageHandler;
    }

    public void readAttributes(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "group":
                    readGroup(child, childXpath);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
    }

    private void readGroup(Element element_xml, String xpath) {
        String groupName = element_xml.getAttribute("name");
        if (groupName.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "name");
            return;
        }
        xpath = xpath + "[@name='" + groupName + "']";
        boolean start = builder.startGroup(groupName);
        if (!start) {
            DomMessages.invalidWarning(messageHandler, "_ error.unknown.xml.attributevalue", xpath, "name", groupName);
            return;
        }
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                String tagName = element.getTagName();
                if (tagName.equals("attr-def")) {
                    String localKey = element.getAttribute("key").trim();
                    if (localKey.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, xpath + "/" + tagName, "key");
                        continue;
                    }
                    String nameSpace = element.getAttribute("ns").trim();
                    if (nameSpace.length() == 0) {
                        DomMessages.emptyAttribute(messageHandler, xpath + "/" + tagName, "ns");
                        continue;
                    }
                    String xpath2 = xpath + "/" + tagName + "[@ns='" + nameSpace + "' and @key='" + localKey + "']";
                    AttributeKey attributeKey;
                    try {
                        attributeKey = AttributeKey.parse(nameSpace + ":" + localKey);
                    } catch (ParseException pe) {
                        DomMessages.wrongAttributeValue(messageHandler, xpath2, "key|ns", nameSpace + ":" + localKey);
                        continue;
                    }
                    AttributeDefConsumer attributeDefConsumer = new AttributeDefConsumer(attributeKey, xpath2, groupName);
                    DOMUtils.readChildren(element, attributeDefConsumer);
                    boolean done = builder.addAttributeDef(attributeDefConsumer.toAttributeDef());
                    if (!done) {
                        DomMessages.invalidWarning(messageHandler, "_ error.existing.xml.attributevalue", xpath2, "key|ns", attributeKey.toString());
                    }
                } else {
                    DomMessages.unknownTagWarning(messageHandler, xpath + "/" + tagName);
                }
            }
        }
    }


    private class AttributeDefConsumer implements Consumer<Element> {

        private final AttributeKey attributeKey;
        private final String xpath;
        private final AttributeDefBuilder builder;

        private AttributeDefConsumer(AttributeKey attributeKey, String xpath, String groupName) {
            this.attributeKey = attributeKey;
            this.xpath = xpath;
            this.builder = new AttributeDefBuilder(attributeKey);
            builder.putDefObject(SctSpace.GROUPNAME_DEFOBJ, groupName);
            AttributeDef defaultAttributeDef = SctSpace.getDefaultAttributeDef(attributeKey);
            if (defaultAttributeDef != null) {
                builder.putDefObject(SctSpace.FORMATTYPE_DEFOBJ, defaultAttributeDef.getDefObject(SctSpace.FORMATTYPE_DEFOBJ));
                for (Label label : defaultAttributeDef.getTitleLabels()) {
                    builder.putLabel(label);
                }
            }
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if ((tagName.equals("label")) || (tagName.equals("lib"))) {
                try {
                    Label label = LabelUtils.readLabel(element);
                    if (label != null) {
                        builder.putLabel(label);
                    }
                } catch (ParseException pe) {
                    DomMessages.wrongLangAttribute(messageHandler, xpath + "/" + tagName, element.getAttribute("xml:lang"));
                }
            } else if (tagName.equals("format")) {
                String type = element.getAttribute("type");
                if (SctSpace.isValidFormatType(type)) {
                    builder.putDefObject(SctSpace.FORMATTYPE_DEFOBJ, type);
                } else {
                    DomMessages.invalidWarning(messageHandler, "_ error.unknown.xml.attributevalue", xpath + "/" + tagName, "type", type);
                }
            }
        }

        public AttributeDef toAttributeDef() {
            return builder.toAttributeDef();
        }

    }


    public static interface Builder {

        public boolean startGroup(String name);

        public boolean addAttributeDef(AttributeDef attributeDef);

    }

}
