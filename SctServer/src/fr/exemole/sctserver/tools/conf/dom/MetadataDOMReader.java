/* SctServer - Copyright (c) 2016-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import fr.exemole.sctserver.api.EngineMetadata;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;
import net.mapeadores.util.text.PhrasesBuilder;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class MetadataDOMReader {

    private final LabelChangeBuilder titleLabelsBuilder = new LabelChangeBuilder();
    private final PhrasesBuilder phrasesBuilder = new PhrasesBuilder();
    private final AttributesBuilder attributesBuilder = new AttributesBuilder();
    private final MessageHandler messageHandler;
    private URL iconUrl;
    private URL websiteUrl;
    private Lang defaultLang;

    public MetadataDOMReader(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public EngineMetadata readMetadata(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "label": {
                    try {
                        Label label = LabelUtils.readLabel(child);
                        if (label != null) {
                            titleLabelsBuilder.putLabel(label);
                        }
                    } catch (ParseException pe) {
                        DomMessages.wrongLangAttribute(messageHandler, childXpath, child.getAttribute("xml:lang"));
                    }
                    break;
                }
                case "title": {
                    DOMUtils.readChildren(child, new LabelsConsumer(titleLabelsBuilder, childXpath));
                    break;
                }
                case "phrase": {
                    LabelUtils.readPhraseElement(phrasesBuilder, child, messageHandler, childXpath);
                    break;
                }
                case "description": {
                    LabelUtils.readPhraseElement(phrasesBuilder, child, messageHandler, childXpath, "description");
                    break;
                }
                case "icon": {
                    String data = XMLUtils.getData(child);
                    if (data.length() > 0) {
                        iconUrl = getURL(data, childXpath);
                    }
                    break;
                }
                case "website": {
                    String data = XMLUtils.getData(child);
                    if (data.length() > 0) {
                        websiteUrl = getURL(data, childXpath);
                    }
                    break;
                }
                case "default-lang":
                case "default-lang-ui": {
                    String data = XMLUtils.getData(child);
                    if (data.length() > 0) {
                        try {
                            defaultLang = Lang.parse(data);
                        } catch (ParseException pe) {
                            DomMessages.wrongElementValue(messageHandler, childXpath, data);
                        }
                    }
                    break;
                }
                case "attr": {
                    AttributeUtils.readAttrElement(attributesBuilder, child);
                    break;
                }
            }
        });
        return new InternalEngineMetadata(titleLabelsBuilder.toLabels(), phrasesBuilder.toPhrases(), iconUrl, websiteUrl, defaultLang, attributesBuilder.toAttributes());
    }

    private URL getURL(String data, String urlXPath) {
        try {
            URL url = new URL(data);
            return url;
        } catch (MalformedURLException mue) {
            DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_url", urlXPath, data, mue.getMessage());
            return null;
        }
    }


    private class LabelsConsumer implements Consumer<Element> {

        private final LabelChangeBuilder labelChangeBuilder;
        private final String xpath;


        private LabelsConsumer(LabelChangeBuilder changeLabelBuilder, String xpath) {
            this.labelChangeBuilder = changeLabelBuilder;
            this.xpath = xpath;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            switch (tagName) {
                case "label":
                case "lib":
                    try {
                    Label label = LabelUtils.readLabel(element);
                    if (label != null) {
                        labelChangeBuilder.putLabel(label);
                    }
                } catch (ParseException pe) {
                    DomMessages.wrongLangAttribute(messageHandler, xpath + "/" + tagName, element.getAttribute("xml:lang"));
                }
            }
        }

    }


    private static class InternalEngineMetadata implements EngineMetadata {

        private final Labels titleLabels;
        private final Phrases phrases;
        private final URL iconUrl;
        private final URL websiteUrl;
        private final Lang defaultLang;
        private final Attributes attributes;

        private InternalEngineMetadata(Labels titleLabels, Phrases phrases, URL iconUrl, URL websiteUrl, Lang defaultLang, Attributes attributes) {
            this.titleLabels = titleLabels;
            this.phrases = phrases;
            this.iconUrl = iconUrl;
            this.websiteUrl = websiteUrl;
            this.defaultLang = defaultLang;
            this.attributes = attributes;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Phrases getPhrases() {
            return phrases;
        }

        @Override
        public URL getIcon() {
            return iconUrl;
        }

        @Override
        public URL getWebsite() {
            return websiteUrl;
        }

        @Override
        public Lang getDefaultLang() {
            return defaultLang;
        }

    }

}
