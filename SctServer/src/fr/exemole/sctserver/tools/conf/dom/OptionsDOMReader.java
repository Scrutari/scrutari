/* SctServer - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.tools.options.PertinencePonderationBuilder;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class OptionsDOMReader {

    private final Builder builder;
    private final MessageHandler messageHandler;

    public OptionsDOMReader(Builder builder, MessageHandler messageHandler) {
        this.builder = builder;
        this.messageHandler = messageHandler;
    }

    public void readOptions(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "ponderation":
                    setPonderation(child);
                    break;
                case "pertinence":
                    setPertinence(child);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, tagName);
            }
        });
    }

    private void setPonderation(Element element) {
        int occurrence = getInt(element, "occurrence");
        int date = getInt(element, "date");
        int origin = getInt(element, "origin");
        int lang = getInt(element, "lang");
        builder.setGlobalPertinencePonderation(PertinencePonderationBuilder.toPertinencePonderation(occurrence, date, origin, lang));
    }

    private void setPertinence(Element element) {
        String order = element.getAttribute("order");
        String[] tokens = StringUtils.getTechnicalTokens(order, true);
        if (tokens.length > 0) {
            builder.setPertinenceOrderArray(tokens);
        }
    }

    private int getInt(Element element, String attribute) {
        String vale = element.getAttribute(attribute);
        if (vale.length() == 0) {
            return 0;
        }
        try {
            int itg = Integer.parseInt(vale);
            return itg;
        } catch (NumberFormatException nfe) {
            return 0;
        }
    }


    public static interface Builder {

        public void setGlobalPertinencePonderation(PertinencePonderation pertinencePonderation);

        public void setPertinenceOrderArray(String[] pertinenceOrderArray);

    }

}
