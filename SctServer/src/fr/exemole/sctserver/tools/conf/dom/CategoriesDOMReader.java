/* SctServer - Copyright (c) 2006-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import java.text.ParseException;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.tools.options.CategoryDefBuilder;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public class CategoriesDOMReader {

    private final Builder builder;
    private final MessageHandler messageHandler;

    public CategoriesDOMReader(Builder builder, MessageHandler messageHandler) {
        this.builder = builder;
        this.messageHandler = messageHandler;
    }

    public void readCategories(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "category":
                    addCategory(child, childXpath);
                    break;
            }
        });
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "category":
                    break;
                case "corpus-list":
                    addCorpusList(child, childXpath);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
    }

    private void addCategory(Element element_xml, String xpath) {
        String name = element_xml.getAttribute("name");
        if (name.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "name");
            return;
        }
        xpath = xpath + "[@name='" + name + "']";
        CategoryDefBuilder categoryDefBuilder = builder.addCategory(name);
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                String tagName = element.getTagName();
                if ((tagName.equals("label")) || (tagName.equals("lib"))) {
                    try {
                        LabelUtils.readLabel(element, categoryDefBuilder);
                    } catch (ParseException lie) {
                        DomMessages.invalidWarning(messageHandler, "_ error.wrong.xml.attributevalue", xpath + "/" + tagName, "xml:lang");
                    }
                } else if (tagName.equals("attr")) {
                    AttributeUtils.readAttrElement(categoryDefBuilder.getAttributesBuilder(), element);
                } else if (tagName.equals("phrase")) {
                    LabelUtils.readPhraseElement(categoryDefBuilder.getPhrasesBuilder(), element, messageHandler, xpath + "/" + tagName);
                } else {
                    DomMessages.unknownTagWarning(messageHandler, xpath + "/" + tagName);
                }
            }
        }
    }

    private void addCorpusList(Element element_xml, String xpath) {
        String name = element_xml.getAttribute("category-name");
        if (name.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "category-name");
            return;
        }
        xpath = xpath + "[@category-name='" + name + "']";
        if (name.equals(CategoryDef.DEFAULT_NAME)) {
            DomMessages.invalidWarning(messageHandler, "_ error.wrong.xml.attributevalue_withreason", xpath, "category-name", name, CategoryDef.DEFAULT_NAME + " cannot be use to define corpus list");
            return;
        }
        CategoryDefBuilder categoryDefBuilder = builder.getCategory(name);
        if (categoryDefBuilder == null) {
            DomMessages.invalid(messageHandler, "_ error.unknown.xml.attributevalue", xpath, "category-name", name);
            return;
        }
        NodeList liste = element_xml.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) liste.item(i);
                String tagName = element.getTagName();
                if (tagName.equals("uri")) {
                    String uriString = XMLUtils.getData(element);
                    if (uriString.length() == 0) {
                        DomMessages.emptyElement(messageHandler, xpath + "/" + tagName);
                        continue;
                    }
                    try {
                        ScrutariDataURI scrutariDataURI = URIParser.parse(uriString);
                        if (scrutariDataURI instanceof CorpusURI) {
                            boolean done = builder.addCorpusURI(categoryDefBuilder, (CorpusURI) scrutariDataURI);
                            if (!done) {
                                DomMessages.invalidWarning(messageHandler, "_ error.existing.xml.tagvalue", xpath + "/" + tagName, uriString);
                            }
                        }
                    } catch (URIParseException uripe) {
                        DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_uri", xpath + "/" + tagName, uriString, uripe.getMessage());
                    }
                } else {
                    DomMessages.unknownTagWarning(messageHandler, xpath + "/" + tagName);
                }
            }
        }
    }


    public static interface Builder {

        public CategoryDefBuilder addCategory(String name);

        public CategoryDefBuilder getCategory(String name);

        public boolean addCorpusURI(CategoryDefBuilder builder, CorpusURI corpusURI);

    }

}
