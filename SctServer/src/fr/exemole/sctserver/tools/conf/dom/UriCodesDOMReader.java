/* SctServer - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class UriCodesDOMReader {

    private final MessageHandler messageHandler;
    private final Builder builder;

    public UriCodesDOMReader(Builder builder, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.builder = builder;
    }

    public void readUriCodes(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "uri":
                    addUri(child, childXpath);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
    }

    private void addUri(Element element, String xpath) {
        String codeString = element.getAttribute("code");
        if (codeString.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "code");
            return;
        }
        xpath = xpath + "[@code='" + codeString + "']";
        int code = 0;
        try {
            code = Integer.parseInt(codeString);
            if (code < 1) {
                DomMessages.invalid(messageHandler, "_ error.wrong.xml.attributevalue_withreason", xpath, "code", codeString, "code < 1");
            }
        } catch (NumberFormatException nfe) {
            DomMessages.wrongIntegerAttributeValue(messageHandler, xpath, "code", codeString);
        }
        if (builder.containsCode(code)) {
            DomMessages.invalid(messageHandler, "_ error.existing.xml.attributevalue", xpath, "code", String.valueOf(code));
        } else {
            String uriString = XMLUtils.getData(element);
            if (uriString.length() == 0) {
                DomMessages.emptyElement(messageHandler, xpath);
            } else {
                try {
                    ScrutariDataURI scrutariDataURI = URIParser.parse(uriString);
                    if (builder.containsURI(scrutariDataURI)) {
                        DomMessages.invalid(messageHandler, "_ error.existing.xml.tagvalue", xpath, uriString);
                    } else {
                        builder.addScrutariDataURI(code, scrutariDataURI);
                    }
                } catch (URIParseException uripe) {
                    DomMessages.invalid(messageHandler, "_ error.wrong.xml.tagvalue_uri", xpath, uriString, uripe.getMessage());
                }
            }
        }
    }


    public static interface Builder {

        public boolean containsCode(int code);

        public boolean containsURI(ScrutariDataURI scrutariDataURI);

        public void addScrutariDataURI(int code, ScrutariDataURI scrutariDataURI);

    }

}
