/* SctServer - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.LabelChangeBuilder;
import net.mapeadores.util.text.PhrasesBuilder;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public final class ConfDOMUtils {

    private ConfDOMUtils() {

    }

    public static void readPhraseElement(PhrasesBuilder phrasesBuilder, Element element, MessageHandler messageHandler, String xpath) {
        String name = element.getAttribute("name");
        if (name.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "name");
            return;
        }
        xpath = xpath + "[@name='" + name + "']";
        readPhraseElement(phrasesBuilder, element, messageHandler, xpath, name);
    }

    public static void readPhraseElement(PhrasesBuilder phrasesBuilder, Element element, MessageHandler messageHandler, String xpath, String name) {
        LabelChangeBuilder phraseBuilder = phrasesBuilder.getPhraseBuilder(name);
        DOMUtils.readChildren(element, new PhraseConsumer(phraseBuilder, messageHandler, xpath));
    }


    private static class PhraseConsumer implements Consumer<Element> {

        private final LabelChangeBuilder phraseBuilder;
        private final MessageHandler messageHandler;
        private final String xpath;

        private PhraseConsumer(LabelChangeBuilder phraseBuilder, MessageHandler messageHandler, String xpath) {
            this.phraseBuilder = phraseBuilder;
            this.messageHandler = messageHandler;
            this.xpath = xpath;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            String tagPath = xpath + "/" + tagName;
            if (tagName.equals("label")) {
                String langString = element.getAttribute("xml:lang");
                if (langString.isEmpty()) {
                    DomMessages.emptyAttribute(messageHandler, tagPath, "xml:lang");
                } else {
                    tagPath = tagPath + "[@xml:lang='" + langString + "']";
                    try {
                        Lang lang = Lang.parse(element.getAttribute("xml:lang"));
                        CleanedString labelString = DOMUtils.contentToCleanedString(element);
                        if (labelString != null) {
                            phraseBuilder.putLabel(lang, labelString);
                        } else {
                            DomMessages.emptyElement(messageHandler, tagPath);
                        }
                    } catch (ParseException pe) {
                        DomMessages.wrongLangAttribute(messageHandler, tagPath, langString);
                    }
                }
            } else {
                DomMessages.unknownTagWarning(messageHandler, tagPath);
            }
        }

    }

}
