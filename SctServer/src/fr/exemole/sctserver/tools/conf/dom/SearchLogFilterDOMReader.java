/* SctServer - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import net.mapeadores.util.conditions.ConditionsUtils;
import net.mapeadores.util.conditions.TextTest;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class SearchLogFilterDOMReader {

    private final MessageHandler messageHandler;
    private final Builder builder;

    public SearchLogFilterDOMReader(Builder builder, MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.builder = builder;
    }

    public void readSearchLogFilter(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            switch (tagName) {
                case "deny":
                    addDeny(child, childXpath);
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
    }

    private void addDeny(Element element, String xpath) {
        TextTest ipCondition = ConditionsUtils.parseTextTest(element.getAttribute("ip"));
        TextTest siteCondition = ConditionsUtils.parseTextTest(element.getAttribute("site"));
        TextTest refCondition = ConditionsUtils.parseTextTest(element.getAttribute("ref"));
        if ((ipCondition == null) && (refCondition == null) && (siteCondition == null)) {
            DomMessages.emptyAttribute(messageHandler, xpath, "deny");
            return;
        }
        builder.addDeny(ipCondition, siteCondition, refCondition);
    }


    public static interface Builder {

        public void addDeny(TextTest ipCondition, TextTest siteCondition, TextTest refCondition);

    }

}
