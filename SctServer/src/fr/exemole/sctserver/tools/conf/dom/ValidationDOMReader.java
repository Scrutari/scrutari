/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.conf.dom;

import fr.exemole.sctserver.tools.DataValidatorBuilder;
import java.text.ParseException;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.NearStringSplitter;
import net.mapeadores.util.text.StringChecker;
import net.mapeadores.util.text.StringCheckerUtils;
import net.mapeadores.util.text.StringSplitter;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.data.DataFieldKey;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ValidationDOMReader {

    private final DataValidatorBuilder dataValidatorBuilder;
    private final MessageHandler messageHandler;

    public ValidationDOMReader(DataValidatorBuilder dataValidatorBuilder, MessageHandler messageHandler) {
        this.dataValidatorBuilder = dataValidatorBuilder;
        this.messageHandler = messageHandler;
    }

    public void readValidation(Element element, String xpath) {
        String elementXpath = xpath + "/" + element.getTagName();
        DOMUtils.readChildren(element, (child) -> {
            String tagName = child.getTagName();
            String childXpath = elementXpath + "/" + tagName;
            DataFieldKey dataFieldKey = getField(child, childXpath);
            if (dataFieldKey == null) {
                return;
            }
            switch (tagName) {
                case "mandatory":
                    dataValidatorBuilder.addMandatory(dataFieldKey);
                    break;
                case "length":
                case "check":
                    int min = getInt(child, childXpath, "min");
                    int max = getInt(child, childXpath, "max");
                    if ((min >= 0) && (max >= 0)) {
                        dataValidatorBuilder.putLength(dataFieldKey, min, max);
                    }
                    break;
                case "unique":
                    dataValidatorBuilder.addUnique(dataFieldKey);
                    break;
                case "format":
                    StringChecker formatChecker = getFormatChecker(child, childXpath);
                    if (formatChecker != null) {
                        dataValidatorBuilder.putFormatChecker(dataFieldKey, formatChecker);
                    }
                    break;
                case "split":
                    StringSplitter stringSplitter = getStringSplitter(child, childXpath);
                    if (stringSplitter != null) {
                        dataValidatorBuilder.putSplitter(dataFieldKey, stringSplitter);
                    }
                    break;
                default:
                    DomMessages.unknownTagWarning(messageHandler, childXpath);
            }
        });
    }


    private DataFieldKey getField(Element element, String xpath) {
        String field = element.getAttribute("field").trim();
        if (field.length() > 0) {
            switch (field) {
                case "geoloc":
                    return DataFieldKey.GEOLOC;
                case "soustitre":
                    return DataFieldKey.SOUSTITRE;
                default: {
                    try {
                        AttributeKey attributeKey = AttributeKey.parse(field);
                        return new DataFieldKey(attributeKey);
                    } catch (ParseException pe) {
                        DomMessages.invalidWarning(messageHandler, "_ error.unknown.xml.attributevalue", xpath, "field", field);
                        return null;
                    }
                }
            }
        } else {
            String nameSpace = element.getAttribute("ns").trim();
            String localKey = element.getAttribute("key").trim();
            if (nameSpace.length() == 0) {
                if (localKey.length() == 0) {
                    DomMessages.emptyAttribute(messageHandler, xpath, "field");
                } else {
                    DomMessages.emptyAttribute(messageHandler, xpath, "ns");
                }
                return null;
            } else if (localKey.length() == 0) {
                DomMessages.emptyAttribute(messageHandler, xpath, "key");
                return null;
            }
            try {
                AttributeKey attributeKey = AttributeKey.parse(nameSpace + ":" + localKey);
                return new DataFieldKey(attributeKey);
            } catch (ParseException pe) {
                DomMessages.wrongAttributeValue(messageHandler, xpath + "[@ns='" + nameSpace + "' and @key='" + localKey + "']", "key|ns", nameSpace + ":" + localKey);
                return null;
            }
        }
    }

    private int getInt(Element element, String xpath, String attributeName) {
        String intValue = element.getAttribute(attributeName);
        if (intValue.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, attributeName);
            return -1;
        }
        try {
            int value = Integer.parseInt(intValue);
            if (value < 0) {
                value = 0;
            }
            return value;
        } catch (NumberFormatException nfe) {
            DomMessages.wrongIntegerAttributeValue(messageHandler, xpath, attributeName, intValue);
            return -1;
        }
    }

    private StringChecker getFormatChecker(Element element, String xpath) {
        String type = element.getAttribute("type").trim();
        if (type.length() == 0) {
            DomMessages.emptyAttribute(messageHandler, xpath, "type");
            return null;
        }
        switch (type) {
            case "url":
                return StringCheckerUtils.URL;
            default:
                DomMessages.invalidWarning(messageHandler, "_ error.unknown.xml.attributevalue", xpath, "type", type);
                return null;
        }
    }

    private StringSplitter getStringSplitter(Element element, String xpath) {
        int near = 350;
        int threshold = 500;
        String nearString = element.getAttribute("near").trim();
        if (nearString.length() > 0) {
            try {
                near = Integer.parseInt(nearString);
            } catch (NumberFormatException nfe) {
                DomMessages.invalidWarning(messageHandler, "_ error.wrong.xml.attributevalue", xpath, "near", nearString);
            }
            if (near < 50) {
                near = 350;
            }
        }
        String thresholdString = element.getAttribute("threshold").trim();
        if (thresholdString.length() > 0) {
            try {
                threshold = Integer.parseInt(thresholdString);
            } catch (NumberFormatException nfe) {
                DomMessages.invalidWarning(messageHandler, "_ error.wrong.xml.attributevalue", xpath, "threshold", thresholdString);
            }
            if (threshold < 100) {
                threshold = 500;
            }
        }
        return new NearStringSplitter(near, threshold);
    }

}
