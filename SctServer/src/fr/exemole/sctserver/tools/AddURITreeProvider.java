/* SctServer - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools;

import fr.exemole.sctserver.api.storage.EngineStorage;
import java.util.Collections;
import java.util.List;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.URITreeProvider;


/**
 *
 * @author Vincent Calame
 */
public class AddURITreeProvider implements URITreeProvider {

    private final static URITree EMPTY_URITREE = new EmptyURITree();
    private boolean init = false;
    private final EngineStorage engineStorage;
    private ScrutariDBName[] scrutariDBNameArray;
    private URITree[] uriTreeArray;

    public AddURITreeProvider(EngineStorage engineStorage) {
        this.engineStorage = engineStorage;
    }

    private void testInit() {
        if (init) {
            return;
        }
        scrutariDBNameArray = engineStorage.getAddScrutariDBNameArray();
        uriTreeArray = new URITree[scrutariDBNameArray.length];
        init = true;
    }

    @Override
    public int getAddURITreeDateCount() {
        testInit();
        return scrutariDBNameArray.length;
    }

    @Override
    public ScrutariDBName getScrutariDBName(int i) {
        testInit();
        return scrutariDBNameArray[i];
    }

    @Override
    public URITree getAddURITree(int i, ScrutariDataURIChecker scrutariDataURIChecker) {
        testInit();
        URITree uriTree = uriTreeArray[i];
        if (uriTree == null) {
            uriTree = engineStorage.getAddedURITree(scrutariDBNameArray[i], scrutariDataURIChecker);
            if (uriTree == null) {
                uriTree = EMPTY_URITREE;
            }
            uriTreeArray[i] = uriTree;
        }
        return uriTree;
    }


    private static class EmptyURITree implements URITree {

        private final List<BaseNode> EMPTY_LIST = Collections.emptyList();

        private EmptyURITree() {
        }

        @Override
        public List<BaseNode> getBaseNodeList() {
            return EMPTY_LIST;
        }

    }

}
