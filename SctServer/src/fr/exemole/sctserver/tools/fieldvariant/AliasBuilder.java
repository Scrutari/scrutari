/* SctServer - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.fieldvariant;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.text.ParseException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.RandomAccess;
import java.util.Set;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class AliasBuilder {

    private final static String COMPLEMENT_PREFIX = "complement_";
    private final static String DEFAULT_SEPARATOR = ", ";
    private final String name;
    private final Set<String> existingSet = new HashSet<String>();
    private final List<FieldVariant.FieldReference> fieldReferenceList = new ArrayList<FieldVariant.FieldReference>();
    private String separator = DEFAULT_SEPARATOR;

    public AliasBuilder(String name) {
        if ((name == null) || (name.length() == 0)) {
            throw new IllegalArgumentException("aliasName is null or empty");
        }
        this.name = name;
    }

    public AliasBuilder setSeparator(String separator) {
        if (separator == null) {
            separator = DEFAULT_SEPARATOR;
        }
        this.separator = separator;
        return this;
    }

    public AliasBuilder addCoreField(int field) {
        addFieldReference(new InternalCoreFieldReference(field));
        return this;
    }

    public AliasBuilder addComplementField(int complementNumber) {
        if (complementNumber > 0) {
            addFieldReference(new InternalComplementFieldReference(complementNumber));
        }
        return this;
    }

    public AliasBuilder addAttributeField(AttributeKey attributeKey) {
        addFieldReference(new InternalAttributeFieldReference(attributeKey));
        return this;
    }

    public AliasBuilder parseFields(String fields) {
        String[] tokens = StringUtils.getTechnicalTokens(fields, true);
        for (String token : tokens) {
            String constantToken = token.toLowerCase();
            int coreField = FieldVariant.checkCoreFicheFieldString(constantToken);
            if (coreField != -1) {
                addCoreField(coreField);
            } else {
                if (constantToken.startsWith(COMPLEMENT_PREFIX)) {
                    try {
                        int complementNumber = Integer.parseInt(constantToken.substring(COMPLEMENT_PREFIX.length()));
                        addComplementField(complementNumber);
                    } catch (NumberFormatException nfe) {

                    }
                } else {
                    try {
                        AttributeKey attributeKey = AttributeKey.parse(token);
                        addAttributeField(attributeKey);
                    } catch (ParseException pe) {

                    }
                }
            }
        }
        return this;
    }


    private void addFieldReference(FieldVariant.FieldReference fieldReference) {
        String key = fieldReference.toString();
        if (!existingSet.contains(key)) {
            existingSet.add(key);
            fieldReferenceList.add(fieldReference);
        }
    }

    public FieldVariant.Alias toAlias() {
        FieldVariant.FieldReference[] array = fieldReferenceList.toArray(new FieldVariant.FieldReference[fieldReferenceList.size()]);
        return new InternalAlias(name, separator, new FieldReferenceList(array));
    }


    private static class InternalAlias implements FieldVariant.Alias {

        private final String name;
        private final String separator;
        private final List<FieldVariant.FieldReference> fieldReferenceList;
        private final boolean isArray;

        private InternalAlias(String name, String separator, List<FieldVariant.FieldReference> fieldReferenceList) {
            this.name = name;
            this.separator = separator;
            this.fieldReferenceList = fieldReferenceList;
            this.isArray = (separator.equals("ARRAY"));
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public String getSeparator() {
            return separator;
        }

        @Override
        public List<FieldVariant.FieldReference> getFieldReferenceList() {
            return fieldReferenceList;
        }

        @Override
        public boolean isArray() {
            return isArray;
        }

    }


    private static class InternalCoreFieldReference implements FieldVariant.CoreFieldReference {

        private final int field;

        public InternalCoreFieldReference(int field) {
            this.field = field;
        }

        @Override
        public int getField() {
            return field;
        }

        @Override
        public String toString() {
            return "core_" + field;
        }

    }


    private static class InternalComplementFieldReference implements FieldVariant.ComplementFieldReference {

        private final int complementNumber;

        public InternalComplementFieldReference(int complementNumber) {
            this.complementNumber = complementNumber;
        }

        @Override
        public int getComplementNumber() {
            return complementNumber;
        }

        @Override
        public String toString() {
            return "complement_" + complementNumber;
        }

    }


    private static class InternalAttributeFieldReference implements FieldVariant.AttributeFieldReference {

        private final AttributeKey attributeKey;

        private InternalAttributeFieldReference(AttributeKey attributeKey) {
            this.attributeKey = attributeKey;
        }

        @Override
        public AttributeKey getAttributeKey() {
            return attributeKey;
        }

        @Override
        public String toString() {
            return attributeKey.toString();
        }

    }


    private static class FieldReferenceList extends AbstractList<FieldVariant.FieldReference> implements RandomAccess {

        private final FieldVariant.FieldReference[] array;

        private FieldReferenceList(FieldVariant.FieldReference[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public FieldVariant.FieldReference get(int index) {
            return array[index];
        }

    }

}
