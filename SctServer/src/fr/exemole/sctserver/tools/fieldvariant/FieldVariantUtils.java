/* SctServer - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.fieldvariant;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public final class FieldVariantUtils {

    public final static List<FieldVariant> EMPTY_FIELDWARIANTLIST = Collections.emptyList();
    public final static List<FieldVariant.Alias> EMPTY_ALIASLIST = Collections.emptyList();

    private FieldVariantUtils() {

    }

    public static List<FieldVariant> wrap(FieldVariant[] array) {
        return new FieldVariantList(array);
    }

    public static List<FieldVariant.Alias> wrap(FieldVariant.Alias[] array) {
        return new AliasList(array);
    }

    public static String ficheFieldsToString(FieldVariant fieldVariant) {
        FicheStringEngine engine = new FicheStringEngine(fieldVariant);
        return engine.run();
    }

    public static String motcleFieldsToString(FieldVariant fieldVariant) {
        MotcleStringEngine engine = new MotcleStringEngine(fieldVariant);
        return engine.run();
    }

    public static String aliasFieldsToString(FieldVariant.Alias alias) {
        AliasEngine engine = new AliasEngine(alias);
        return engine.run();
    }


    private static class FieldVariantList extends AbstractList<FieldVariant> implements RandomAccess {

        private final FieldVariant[] array;

        private FieldVariantList(FieldVariant[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public FieldVariant get(int index) {
            return array[index];
        }

    }


    private static class AliasList extends AbstractList<FieldVariant.Alias> implements RandomAccess {

        private final FieldVariant.Alias[] array;

        private AliasList(FieldVariant.Alias[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public FieldVariant.Alias get(int index) {
            return array[index];
        }

    }


    private static abstract class StringEngine {

        private final StringBuffer buf = new StringBuffer();


        protected abstract String run();

        protected void append(String text) {
            buf.append(text);
        }

        protected String flush() {
            return buf.toString();
        }

        protected void separator() {
            if (buf.length() > 0) {
                buf.append(",");
            }
        }

    }


    private static abstract class VariantEngine extends StringEngine {

        protected final FieldVariant fieldVariant;

        protected VariantEngine(FieldVariant fieldVariant) {
            this.fieldVariant = fieldVariant;
        }

        protected void addWith(boolean isWith, String fieldName) {
            if (isWith) {
                separator();
                append(fieldName);
            }
        }

        protected void addMulti(short type, String fieldName) {
            switch (type) {
                case FieldVariant.CONCERNED_TYPE:
                    separator();
                    append(fieldName);
                    break;
                case FieldVariant.ALL_TYPE:
                    separator();
                    append(fieldName);
                    append("_all");
                    break;
            }
        }

        protected void addAttr(short type, String fieldName) {
            switch (type) {
                case FieldVariant.CONCERNED_TYPE:
                    separator();
                    append(fieldName);
                    break;
                case FieldVariant.ALL_TYPE:
                    separator();
                    append(fieldName);
                    append("_all");
                    break;
                case FieldVariant.PRIMARY_TYPE:
                    separator();
                    append(fieldName);
                    append("_primary");
                    break;
            }
        }


    }


    private static class FicheStringEngine extends VariantEngine {

        private FicheStringEngine(FieldVariant fieldVariant) {
            super(fieldVariant);
        }

        @Override
        protected String run() {
            if (!fieldVariant.isFicheWith(FieldVariant.FICHE_CODEFICHE)) {
                append("-codefiche");
            }
            simple(FieldVariant.FICHE_CODECORPUS);
            simple(FieldVariant.FICHE_CODEBASE);
            simple(FieldVariant.FICHE_AUTHORITY);
            simple(FieldVariant.FICHE_BASENAME);
            simple(FieldVariant.FICHE_CORPUSNAME);
            simple(FieldVariant.FICHE_FICHEID);
            simple(FieldVariant.FICHE_TITRE);
            simple(FieldVariant.FICHE_SOUSTITRE);
            simple(FieldVariant.FICHE_HREF);
            simple(FieldVariant.FICHE_LANG);
            simple(FieldVariant.FICHE_YEAR);
            simple(FieldVariant.FICHE_DATE);
            simple(FieldVariant.FICHE_DATEISO);
            simple(FieldVariant.FICHE_FICHEICON);
            simple(FieldVariant.FICHE_ICON);
            simple(FieldVariant.FICHE_LATITUDE);
            simple(FieldVariant.FICHE_LONGITUDE);
            simple(FieldVariant.FICHE_COMPLEMENTS, "complements");
            attr(FieldVariant.FICHE_ATTRIBUTES, "attrs");
            simple(FieldVariant.FICHE_MTITRE, "mtitre");
            simple(FieldVariant.FICHE_MSOUSTITRE, "msoustitre");
            multi(FieldVariant.FICHE_MCOMPLEMENTS, "mcomplements");
            attr(FieldVariant.FICHE_MATTRIBUTES, "attrs");
            simple(FieldVariant.FICHE_SCORE, "score");
            if (fieldVariant.getFicheWithType(FieldVariant.FICHE_CODEMOTCLEARRAY) == FieldVariant.IGNORE_TYPE) {
                separator();
                append("-codemotclearray");
            } else {
                multi(FieldVariant.FICHE_CODEMOTCLEARRAY, "codemotclearray");
            }
            multi(FieldVariant.FICHE_BYTHESAURUSMAP, "bythesaurusmap");
            return flush();
        }

        private void simple(int field) {
            addWith(fieldVariant.isFicheWith(field), FieldVariant.coreFicheFieldString(field));
        }

        private void simple(int field, String fieldName) {
            addWith(fieldVariant.isFicheWith(field), fieldName);
        }

        protected void attr(int field, String fieldName) {
            addAttr(fieldVariant.getFicheWithType(field), fieldName);
        }

        protected void multi(int field, String fieldName) {
            addMulti(fieldVariant.getFicheWithType(field), fieldName);
        }

    }


    private static class MotcleStringEngine extends VariantEngine {

        private MotcleStringEngine(FieldVariant fieldVariant) {
            super(fieldVariant);
        }

        @Override
        protected String run() {
            if (!fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODEMOTCLE)) {
                append("-codemotcle");
            }
            simple(FieldVariant.MOTCLE_CODETHESAURUS, "codethesaurus");
            simple(FieldVariant.MOTCLE_CODEBASE, "codebase");
            simple(FieldVariant.MOTCLE_BASENAME, "basename");
            simple(FieldVariant.MOTCLE_THESAURUSNAME, "thesaurusname");
            simple(FieldVariant.MOTCLE_MOTCLEID, "motcleid");
            simple(FieldVariant.MOTCLE_LABELS, "labels");
            attr(FieldVariant.MOTCLE_ATTRIBUTES, "attrs");
            multi(FieldVariant.MOTCLE_MLABELS, "mlabels");
            return flush();
        }

        private void simple(int field, String fieldName) {
            addWith(fieldVariant.isMotcleWith(field), fieldName);
        }

        private void attr(int field, String fieldName) {
            addAttr(fieldVariant.getMotcleWithType(field), fieldName);
        }

        private void multi(int field, String fieldName) {
            addMulti(fieldVariant.getMotcleWithType(field), fieldName);
        }

    }


    private static class AliasEngine extends StringEngine {

        private final FieldVariant.Alias alias;

        private AliasEngine(FieldVariant.Alias alias) {
            this.alias = alias;
        }

        @Override
        protected String run() {
            for (FieldVariant.FieldReference fieldReference : alias.getFieldReferenceList()) {
                String value = "";
                if (fieldReference instanceof FieldVariant.CoreFieldReference) {
                    value = FieldVariant.coreFicheFieldString(((FieldVariant.CoreFieldReference) fieldReference).getField());
                } else if (fieldReference instanceof FieldVariant.ComplementFieldReference) {
                    value = getValue((FieldVariant.ComplementFieldReference) fieldReference);
                } else if (fieldReference instanceof FieldVariant.AttributeFieldReference) {
                    value = getValue((FieldVariant.AttributeFieldReference) fieldReference);
                }
                separator();
                append(value);
            }
            return flush();
        }

        private String getValue(FieldVariant.ComplementFieldReference fieldReference) {
            return "complement_" + fieldReference.getComplementNumber();
        }

        private String getValue(FieldVariant.AttributeFieldReference fieldReference) {
            return fieldReference.getAttributeKey().toString();
        }

    }

}
