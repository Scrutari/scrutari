/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.tools.fieldvariant;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.text.ParseException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class FieldVariantBuilder {

    private final String name;
    private final boolean[] ficheWithArray = new boolean[27];
    private final short[] ficheWithTypeArray = new short[27];
    private final boolean[] motcleWithArray = new boolean[11];
    private final short[] motcleWithTypeArray = new short[11];
    private final Map<String, AliasBuilder> ficheAliasBuilderMap = new LinkedHashMap<String, AliasBuilder>();

    public FieldVariantBuilder(String name) {
        if (!name.isEmpty()) {
            if (!StringUtils.isTechnicalName(name, true)) {
                throw new IllegalArgumentException("wrong name: " + name);
            }
        }
        this.name = name;
        ficheWithArray[FieldVariant.FICHE_CODEFICHE] = true;
        ficheWithTypeArray[FieldVariant.FICHE_CODEFICHE] = FieldVariant.CONCERNED_TYPE;
        motcleWithArray[FieldVariant.MOTCLE_CODEMOTCLE] = true;
        motcleWithTypeArray[FieldVariant.MOTCLE_CODEMOTCLE] = FieldVariant.CONCERNED_TYPE;
    }

    public FieldVariantBuilder setFicheWith(int field, boolean value) {
        if ((field < 0) || (field >= ficheWithArray.length)) {
            return this;
        }
        if (value) {
            ficheWithArray[field] = true;
            ficheWithTypeArray[field] = FieldVariant.CONCERNED_TYPE;
        } else {
            ficheWithArray[field] = false;
            ficheWithTypeArray[field] = FieldVariant.IGNORE_TYPE;
        }
        return this;
    }

    public FieldVariantBuilder setFicheWithType(int field, short value) {
        if ((field < 0) || (field >= ficheWithTypeArray.length)) {
            return this;
        }
        ficheWithTypeArray[field] = value;
        ficheWithArray[field] = isWith(value);
        return this;
    }

    public FieldVariantBuilder setMotcleWith(int field, boolean value) {
        if ((field < 0) || (field >= motcleWithArray.length)) {
            return this;
        }
        if (value) {
            motcleWithArray[field] = true;
            motcleWithTypeArray[field] = FieldVariant.CONCERNED_TYPE;
        } else {
            motcleWithArray[field] = false;
            motcleWithTypeArray[field] = FieldVariant.IGNORE_TYPE;
        }
        return this;
    }

    public FieldVariantBuilder setMotcleWithType(int field, short value) {
        if ((field < 0) || (field >= motcleWithTypeArray.length)) {
            return this;
        }
        motcleWithTypeArray[field] = value;
        motcleWithArray[field] = isWith(value);
        return this;
    }

    public FieldVariantBuilder parseFicheFields(String ficheFields) {
        String[] tokens = StringUtils.getTechnicalTokens(ficheFields, true);
        for (String token : tokens) {
            String constantToken = token.toLowerCase();
            int coreField = FieldVariant.checkCoreFicheFieldString(constantToken);
            if (coreField != -1) {
                setFicheWith(coreField, true);
            } else {
                switch (constantToken) {
                    case "-codefiche":
                        setFicheWith(FieldVariant.FICHE_CODEFICHE, false);
                        break;
                    case "geo":
                        setFicheWith(FieldVariant.FICHE_LATITUDE, true);
                        setFicheWith(FieldVariant.FICHE_LONGITUDE, true);
                        break;
                    case "complements":
                        setFicheWith(FieldVariant.FICHE_COMPLEMENTS, true);
                        break;
                    case "attrs":
                        setFicheWithType(FieldVariant.FICHE_ATTRIBUTES, FieldVariant.CONCERNED_TYPE);
                        break;
                    case "attrs_primary":
                        setFicheWithType(FieldVariant.FICHE_ATTRIBUTES, FieldVariant.PRIMARY_TYPE);
                        break;
                    case "attrs_all":
                        setFicheWithType(FieldVariant.FICHE_ATTRIBUTES, FieldVariant.ALL_TYPE);
                        break;
                    case "mtitre":
                    case "mtitre_def":
                        setFicheWith(FieldVariant.FICHE_MTITRE, true);
                        break;
                    case "msoustitre":
                    case "msoustitre_def":
                        setFicheWith(FieldVariant.FICHE_MSOUSTITRE, true);
                        break;
                    case "mcomplements":
                        setFicheWithType(FieldVariant.FICHE_MCOMPLEMENTS, FieldVariant.CONCERNED_TYPE);
                        break;
                    case "mcomplements_all":
                        setFicheWithType(FieldVariant.FICHE_MCOMPLEMENTS, FieldVariant.ALL_TYPE);
                        break;
                    case "mattrs":
                        setFicheWithType(FieldVariant.FICHE_MATTRIBUTES, FieldVariant.CONCERNED_TYPE);
                        break;
                    case "mattrs_primary":
                        setFicheWithType(FieldVariant.FICHE_MATTRIBUTES, FieldVariant.PRIMARY_TYPE);
                        break;
                    case "mattrs_all":
                        setFicheWithType(FieldVariant.FICHE_MATTRIBUTES, FieldVariant.ALL_TYPE);
                        break;
                    case "score":
                        setFicheWith(FieldVariant.FICHE_SCORE, true);
                        break;
                    case "-codemotclearray":
                        setFicheWithType(FieldVariant.FICHE_CODEMOTCLEARRAY, FieldVariant.IGNORE_TYPE);
                        break;
                    case "codemotclearray":
                        setFicheWithType(FieldVariant.FICHE_CODEMOTCLEARRAY, FieldVariant.CONCERNED_TYPE);
                        break;
                    case "codemotclearray_all":
                        setFicheWithType(FieldVariant.FICHE_CODEMOTCLEARRAY, FieldVariant.ALL_TYPE);
                        break;
                    case "bythesaurusmap":
                        setFicheWithType(FieldVariant.FICHE_BYTHESAURUSMAP, FieldVariant.CONCERNED_TYPE);
                        break;
                    case "bythesaurusmap_all":
                        setFicheWithType(FieldVariant.FICHE_BYTHESAURUSMAP, FieldVariant.ALL_TYPE);
                        break;
                    default: {
                        try {
                            AttributeKey attributeKey = AttributeKey.parse(token);
                            if (!ficheAliasBuilderMap.containsKey(token)) {
                                AliasBuilder builder = new AliasBuilder(token);
                                builder.addAttributeField(attributeKey);
                                ficheAliasBuilderMap.put(token, builder);
                            }
                        } catch (ParseException pe) {

                        }
                    }
                }
            }
        }
        return this;
    }

    public FieldVariantBuilder parseMotcleFields(String motcleFields) {
        String[] tokens = StringUtils.getTechnicalTokens(motcleFields, true);
        for (String token : tokens) {
            String constantToken = token.toLowerCase();
            switch (constantToken) {
                case "-codemotcle":
                    setMotcleWith(FieldVariant.MOTCLE_CODEMOTCLE, false);
                    break;
                case "codethesaurus":
                    setMotcleWith(FieldVariant.MOTCLE_CODETHESAURUS, true);
                    break;
                case "codebase":
                    setMotcleWith(FieldVariant.MOTCLE_CODEBASE, true);
                    break;
                case "basename":
                    setMotcleWith(FieldVariant.MOTCLE_BASENAME, true);
                    break;
                case "thesaurusname":
                    setMotcleWith(FieldVariant.MOTCLE_THESAURUSNAME, true);
                    break;
                case "motcleid":
                    setMotcleWith(FieldVariant.MOTCLE_MOTCLEID, true);
                    break;
                case "labels":
                case "libelles":
                    setMotcleWith(FieldVariant.MOTCLE_LABELS, true);
                    break;
                case "attrs":
                    setMotcleWith(FieldVariant.MOTCLE_ATTRIBUTES, true);
                    break;
                case "mlabels":
                case "mlibelles":
                    setMotcleWithType(FieldVariant.MOTCLE_MLABELS, FieldVariant.CONCERNED_TYPE);
                    break;
                case "mlabels_all":
                case "mlabels_def":
                case "mlibelles_def":
                    setMotcleWithType(FieldVariant.MOTCLE_MLABELS, FieldVariant.ALL_TYPE);
                    break;
                case "score":
                    setMotcleWith(FieldVariant.MOTCLE_SCORE, true);
                    break;
                case "fichecount":
                    setMotcleWith(FieldVariant.MOTCLE_FICHECOUNT, true);
                    break;
            }
        }
        return this;
    }

    public FieldVariantBuilder addCoreAlias(String aliasName, int coreField) {
        getAliasBuilder(aliasName).addCoreField(coreField);
        return this;
    }

    public AliasBuilder getAliasBuilder(String aliasName) {
        if ((aliasName == null) || (aliasName.length() == 0)) {
            throw new IllegalArgumentException("aliasName is null or empty");
        }
        AliasBuilder builder = ficheAliasBuilderMap.get(aliasName);
        if (builder == null) {
            builder = new AliasBuilder(aliasName);
            ficheAliasBuilderMap.put(aliasName, builder);
        }
        return builder;
    }

    public FieldVariant toFieldVariant() {
        int ficheLength = ficheWithArray.length;
        boolean[] finalFicheWithArray = new boolean[ficheLength];
        System.arraycopy(ficheWithArray, 0, finalFicheWithArray, 0, ficheLength);
        short[] finalFicheWithTypeArray = new short[ficheLength];
        System.arraycopy(ficheWithTypeArray, 0, finalFicheWithTypeArray, 0, ficheLength);
        int motcleLength = motcleWithArray.length;
        boolean[] finalMotcleWithArray = new boolean[motcleLength];
        System.arraycopy(motcleWithArray, 0, finalMotcleWithArray, 0, motcleLength);
        short[] finalMotcleWithTypeArray = new short[motcleLength];
        System.arraycopy(motcleWithTypeArray, 0, finalMotcleWithTypeArray, 0, motcleLength);
        List<FieldVariant.Alias> ficheAliasList = toList(ficheAliasBuilderMap);
        return new InternalFieldVariant(name, finalFicheWithArray, finalFicheWithTypeArray, finalMotcleWithArray, finalMotcleWithTypeArray, ficheAliasList);
    }

    private List<FieldVariant.Alias> toList(Map<String, AliasBuilder> map) {
        int size = map.size();
        if (size == 0) {
            return FieldVariantUtils.EMPTY_ALIASLIST;
        }
        FieldVariant.Alias[] array = new FieldVariant.Alias[size];
        int p = 0;
        for (AliasBuilder builder : map.values()) {
            array[p] = builder.toAlias();
            p++;
        }
        return FieldVariantUtils.wrap(array);
    }

    public static FieldVariantBuilder init(String name) {
        return new FieldVariantBuilder(name);
    }

    private static boolean isFicheIdOnly(boolean[] ficheWithArray, List<FieldVariant.Alias> ficheAliasList) {
        if (!ficheAliasList.isEmpty()) {
            return false;
        }
        int length = ficheWithArray.length;
        for (int i = 0; i < length; i++) {
            if (ficheWithArray[i]) {
                switch (i) {
                    case FieldVariant.FICHE_CODEFICHE:
                    case FieldVariant.FICHE_CODECORPUS:
                    case FieldVariant.FICHE_CODEBASE:
                    case FieldVariant.FICHE_BASENAME:
                    case FieldVariant.FICHE_CORPUSNAME:
                    case FieldVariant.FICHE_CODEMOTCLEARRAY:
                        break;
                    default:
                        return false;
                }
            }
        }
        return true;
    }


    private static boolean isWith(short type) {
        switch (type) {
            case FieldVariant.IGNORE_TYPE:
            case FieldVariant.DEFAULT_TYPE:
                return false;
            default:
                return true;
        }
    }


    private static class InternalFieldVariant implements FieldVariant {

        private final String name;
        private final boolean[] ficheWithArray;
        private final short[] ficheWithTypeArray;
        private final boolean[] motcleWithArray;
        private final short[] motcleWithTypeArray;
        private final List<FieldVariant.Alias> ficheAliasList;
        private final boolean ficheIdsOnly;

        private InternalFieldVariant(String name, boolean[] ficheWithArray, short[] ficheWithTypeArray, boolean[] motcleWithArray, short[] motcleWithTypeArray, List<FieldVariant.Alias> ficheAliasList) {
            this.name = name;
            this.ficheWithArray = ficheWithArray;
            this.ficheWithTypeArray = ficheWithTypeArray;
            this.motcleWithArray = motcleWithArray;
            this.motcleWithTypeArray = motcleWithTypeArray;
            this.ficheAliasList = ficheAliasList;
            this.ficheIdsOnly = isFicheIdOnly(ficheWithArray, ficheAliasList);
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public boolean isFicheWith(int field) {
            try {
                return ficheWithArray[field];
            } catch (IndexOutOfBoundsException e) {
                return false;
            }
        }

        @Override
        public short getFicheWithType(int field) {
            try {
                return ficheWithTypeArray[field];
            } catch (IndexOutOfBoundsException e) {
                return FieldVariant.DEFAULT_TYPE;
            }
        }

        @Override
        public boolean isMotcleWith(int field) {
            try {
                return motcleWithArray[field];
            } catch (IndexOutOfBoundsException e) {
                return false;
            }
        }

        @Override
        public short getMotcleWithType(int field) {
            try {
                return motcleWithTypeArray[field];
            } catch (IndexOutOfBoundsException e) {
                return FieldVariant.DEFAULT_TYPE;
            }
        }

        @Override
        public List<Alias> getFicheAliasList() {
            return ficheAliasList;
        }

        @Override
        public boolean isFicheIdsOnly() {
            return ficheIdsOnly;
        }

    }


}
