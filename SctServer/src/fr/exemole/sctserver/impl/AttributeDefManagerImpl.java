/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.AttributeDefManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributeUtils;
import net.scrutari.db.api.ScrutariConstants;


/**
 *
 * @author Vincent Calame
 */
class AttributeDefManagerImpl implements AttributeDefManager {

    private Map<String, List<AttributeDef>> attributeDefListMap = Collections.emptyMap();
    private Map<AttributeKey, AttributeDef> attributeDefMap = Collections.emptyMap();
    private Set<String> groupNameSet = Collections.emptySet();

    AttributeDefManagerImpl() {

    }

    @Override
    public Set<String> getGroupNameSet() {
        return groupNameSet;
    }

    @Override
    public List<AttributeDef> getAttributeDefList(String name) {
        List<AttributeDef> list = attributeDefListMap.get(name);
        if (list == null) {
            return AttributeUtils.EMPTY_ATTRIBUTEDEFLIST;
        }
        return list;
    }

    @Override
    public AttributeDef getAttributeDef(AttributeKey attributeKey) {
        return attributeDefMap.get(attributeKey);
    }

    void reload(Map<String, AttributeDef[]> defArrayMap) {
        Map<String, List<AttributeDef>> newListMap = new HashMap<String, List<AttributeDef>>();
        Map<AttributeKey, AttributeDef> newDefMap = new HashMap<AttributeKey, AttributeDef>();
        if (defArrayMap != null) {
            add(defArrayMap, newListMap, newDefMap, ScrutariConstants.PRIMARY_GROUP);
            add(defArrayMap, newListMap, newDefMap, ScrutariConstants.SECONDARY_GROUP);
            add(defArrayMap, newListMap, newDefMap, ScrutariConstants.TECHNICAL_GROUP);
        }
        this.attributeDefListMap = newListMap;
        this.attributeDefMap = newDefMap;
        this.groupNameSet = Collections.unmodifiableSet(newListMap.keySet());
    }

    private static void add(Map<String, AttributeDef[]> defArrayMap, Map<String, List<AttributeDef>> newListMap, Map<AttributeKey, AttributeDef> newDefMap, String groupName) {
        AttributeDef[] array = defArrayMap.get(groupName);
        if (array == null) {
            return;
        }
        List<AttributeDef> finalList = new ArrayList<AttributeDef>();
        for (AttributeDef attributeDef : array) {
            AttributeKey attributeKey = attributeDef.getAttributeKey();
            if (!newDefMap.containsKey(attributeKey)) {
                newDefMap.put(attributeKey, attributeDef);
                finalList.add(attributeDef);
            }
        }
        newListMap.put(groupName, AttributeUtils.toImmutableList(finalList));
    }

}
