/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.FieldVariantManager;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.tools.fieldvariant.FieldVariantBuilder;
import fr.exemole.sctserver.tools.fieldvariant.FieldVariantUtils;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
class FieldVariantManagerImpl implements FieldVariantManager {

    private final static FieldVariant EMPTY_VARIANT_INSTANCE = FieldVariantBuilder.init(EMPTY_VARIANT).toFieldVariant();
    private final static FieldVariant QUERY_VARIANT_INSTANCE = FieldVariantBuilder.init(QUERY_VARIANT)
            .parseFicheFields("codecorpus,mtitre,msoustitre,mattrs_all,attrs,mcomplements,year,href,icon")
            .parseMotcleFields("mlabels")
            .toFieldVariant();
    private final static FieldVariant DATA_VARIANT_INSTANCE = FieldVariantBuilder.init(DATA_VARIANT)
            .parseFicheFields("codecorpus,titre,soustitre,complements,year,href,icon")
            .parseMotcleFields("labels")
            .toFieldVariant();
    private final static FieldVariant GEO_VARIANT_INSTANCE = FieldVariantBuilder.init(GEO_VARIANT)
            .parseFicheFields("-codefiche")
            .addCoreAlias("name", FieldVariant.FICHE_TITRE)
            .addCoreAlias("url", FieldVariant.FICHE_HREF)
            .parseMotcleFields("labels")
            .toFieldVariant();
    private Map<String, FieldVariant> fieldVariantMap = new LinkedHashMap<String, FieldVariant>();
    private List<FieldVariant> fieldVariantList = FieldVariantUtils.EMPTY_FIELDWARIANTLIST;


    FieldVariantManagerImpl() {
        testDefault(fieldVariantMap);
    }

    @Override
    public FieldVariant getFieldVariant(String name) {
        return fieldVariantMap.get(name);
    }

    @Override
    public List<FieldVariant> getFieldVariantList(boolean withDefault) {
        if (withDefault) {
            return new ArrayList<FieldVariant>(fieldVariantMap.values());
        } else {
            return fieldVariantList;
        }
    }

    void reload(Map<String, FieldVariant> map) {
        if (map == null) {
            Map<String, FieldVariant> newMap = new LinkedHashMap<String, FieldVariant>();
            testDefault(newMap);
            this.fieldVariantMap = newMap;
            this.fieldVariantList = FieldVariantUtils.EMPTY_FIELDWARIANTLIST;
        } else {
            Map<String, FieldVariant> newMap = new LinkedHashMap<String, FieldVariant>(map);
            testDefault(newMap);
            this.fieldVariantMap = newMap;
            int size = map.size();
            if (size == 0) {
                this.fieldVariantList = FieldVariantUtils.EMPTY_FIELDWARIANTLIST;
            } else {
                this.fieldVariantList = FieldVariantUtils.wrap(map.values().toArray(new FieldVariant[size]));
            }
        }
    }

    private static void testDefault(Map<String, FieldVariant> fieldVariantMap) {
        testDefault(fieldVariantMap, EMPTY_VARIANT, EMPTY_VARIANT_INSTANCE);
        testDefault(fieldVariantMap, QUERY_VARIANT, QUERY_VARIANT_INSTANCE);
        testDefault(fieldVariantMap, DATA_VARIANT, DATA_VARIANT_INSTANCE);
        testDefault(fieldVariantMap, GEO_VARIANT, GEO_VARIANT_INSTANCE);
    }

    private static void testDefault(Map<String, FieldVariant> fieldVariantMap, String name, FieldVariant defaultVariant) {
        if (!fieldVariantMap.containsKey(name)) {
            fieldVariantMap.put(name, defaultVariant);
        }
    }

}
