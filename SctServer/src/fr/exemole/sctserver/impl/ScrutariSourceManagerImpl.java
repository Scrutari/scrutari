/* SctServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.ScrutariSourceManager;
import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.api.conf.EngineConf;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.tools.collect.CollectEngine;
import fr.exemole.sctserver.tools.collect.CollectUtils;
import fr.exemole.sctserver.tools.collect.ScrutariSourceStorage;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MultiMessageHandler;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
class ScrutariSourceManagerImpl implements ScrutariSourceManager {

    private final Map<String, InternalScrutariSource> scrutariSourceMap = new LinkedHashMap<String, InternalScrutariSource>();
    private List<ScrutariSource> scrutariSourceList = CollectUtils.EMPTY_SCRUTARISOURCELIST;
    private EngineStorage engineStorage;

    ScrutariSourceManagerImpl(EngineStorage engineStorage) {
        this.engineStorage = engineStorage;
    }

    @Override
    public ScrutariSource getScrutariSouce(String scrutariSourceName) {
        return scrutariSourceMap.get(scrutariSourceName);
    }

    @Override
    public List<ScrutariSource> getScrutariSourceList() {
        return scrutariSourceList;
    }

    void reloadConf() {
        EngineConf engineConf = engineStorage.getEngineConf();
        ScrutariSourceDef[] scrutariSourceDefArray = engineConf.getScrutariSourceDefArray();
        if (scrutariSourceDefArray == null) {
            scrutariSourceMap.clear();
        } else if (scrutariSourceMap.isEmpty()) {
            Map<String, ScrutariSourceStorage.Status> statusMap = ScrutariSourceStorage.fromString(engineStorage.getContent(EngineStorage.STATUS_NAME));
            int length = scrutariSourceDefArray.length;
            for (int i = 0; i < length; i++) {
                ScrutariSourceDef scrutariSourceDef = scrutariSourceDefArray[i];
                String name = scrutariSourceDef.getName();
                InternalScrutariSource scrutariSource = new InternalScrutariSource(scrutariSourceDef);
                ScrutariSourceStorage.Status status = statusMap.get(name);
                if (status != null) {
                    scrutariSource.update(status);
                }
                scrutariSourceMap.put(name, scrutariSource);
            }
        } else {
            Map<String, InternalScrutariSource> copyMap = new HashMap<String, InternalScrutariSource>();
            copyMap.putAll(scrutariSourceMap);
            scrutariSourceMap.clear();
            int length = scrutariSourceDefArray.length;
            for (int i = 0; i < length; i++) {
                ScrutariSourceDef scrutariSourceDef = scrutariSourceDefArray[i];
                String name = scrutariSourceDef.getName();
                InternalScrutariSource scrutariSource = copyMap.get(name);
                if (scrutariSource != null) {
                    scrutariSource.setScrutariSourceDef(scrutariSourceDef);
                } else {
                    scrutariSource = new InternalScrutariSource(scrutariSourceDef);
                }
                scrutariSourceMap.put(name, scrutariSource);
            }
        }
        if (scrutariSourceMap.isEmpty()) {
            scrutariSourceList = CollectUtils.EMPTY_SCRUTARISOURCELIST;
        } else {
            scrutariSourceList = CollectUtils.wrap(scrutariSourceMap.values().toArray(new ScrutariSource[scrutariSourceMap.size()]));
        }
        store();
    }

    @Override
    public boolean collect(MultiMessageHandler messageHandler, int level) {
        if (scrutariSourceMap.isEmpty()) {
            return false;
        }
        boolean dataChanged = false;
        for (InternalScrutariSource scrutariSource : scrutariSourceMap.values()) {
            CollectResult collectResult = CollectEngine.collectData(scrutariSource, engineStorage, messageHandler, level);
            CollectState collectState = checkCollectState(scrutariSource, collectResult);
            if (collectState.isUpdated()) {
                scrutariSource.update(collectResult);
            }
            if (collectState.isDataChanged()) {
                dataChanged = true;
            }
        }
        store();
        if ((level == FORCE)) {
            return true;
        } else {
            return dataChanged;
        }
    }

    @Override
    public void collect(String sourceName, boolean force) {
        InternalScrutariSource scrutariSource = scrutariSourceMap.get(sourceName);
        if (scrutariSource == null) {
            return;
        }
        int level = (force) ? ScrutariSourceManager.FORCE : ScrutariSourceManager.CHECK;
        CollectResult collectResult = CollectEngine.collectData(scrutariSource, engineStorage, LogUtils.NULL_MULTIMESSAGEHANDLER, level);
        CollectState collectState = checkCollectState(scrutariSource, collectResult);
        if (collectState.isUpdated()) {
            scrutariSource.update(collectResult);
            store();
        }
    }

    boolean setState(ScrutariSource scrutariSource, String state) {
        boolean done = ((InternalScrutariSource) scrutariSource).setState(state);
        if (done) {
            store();
        }
        return done;
    }

    private void store() {
        engineStorage.saveContent(EngineStorage.STATUS_NAME, ScrutariSourceStorage.toString(scrutariSourceList));
    }

    private CollectState checkCollectState(InternalScrutariSource scrutariSource, CollectResult collectResult) {
        boolean updated = false;
        boolean dataChanged = false;
        String currentState = scrutariSource.getState();
        String newState = collectResult.getState();
        if (!currentState.equals(newState)) {
            updated = true;
        }
        if (newState.equals(CollectResult.OK_STATE)) {
            if ((!currentState.equals(CollectResult.OK_STATE)) || (FuzzyDate.compare(collectResult.getLastUpdate(), scrutariSource.getLastUpdate()) != 0)) {
                dataChanged = true;
                updated = true;
            }
        }
        return new CollectState(updated, dataChanged);
    }


    private class InternalScrutariSource implements ScrutariSource {

        private ScrutariSourceDef scrutariSourceDef;
        private String state = CollectResult.UNKNOWN_STATE;
        private FuzzyDate lastUpdate = null;

        private InternalScrutariSource(ScrutariSourceDef scrutariSourceDef) {
            this.scrutariSourceDef = scrutariSourceDef;
        }

        private InternalScrutariSource(ScrutariSource scrutariSource) {
            this.scrutariSourceDef = scrutariSource.getScrutariSourceDef();
            this.lastUpdate = scrutariSource.getLastUpdate();
            this.state = scrutariSource.getState();
        }

        @Override
        public ScrutariSourceDef getScrutariSourceDef() {
            return scrutariSourceDef;
        }

        @Override
        public String getState() {
            return state;
        }

        @Override
        public FuzzyDate getLastUpdate() {
            return lastUpdate;
        }

        private void setScrutariSourceDef(ScrutariSourceDef scrutariSourceDef) {
            if (!areUrlEqual(this.scrutariSourceDef, scrutariSourceDef)) {
                state = CollectResult.UNKNOWN_STATE;
                lastUpdate = null;
            }
            this.scrutariSourceDef = scrutariSourceDef;
        }

        private boolean setState(String state) {
            if (state.equals(this.state)) {
                return false;
            }
            this.state = state;
            return true;
        }

        private void update(CollectResult collectResult) {
            this.state = collectResult.getState();
            this.lastUpdate = collectResult.getLastUpdate();
        }

    }

    private static boolean areUrlEqual(ScrutariSourceDef ssd1, ScrutariSourceDef ssd2) {
        List<ScrutariSourceDef.UrlEntry> entryList1 = ssd1.getUrlEntryList();
        List<ScrutariSourceDef.UrlEntry> entryList2 = ssd2.getUrlEntryList();
        int size1 = entryList1.size();
        int size2 = entryList2.size();
        if (size1 != size2) {
            return false;
        }
        for (int i = 0; i < size1; i++) {
            if (!entryList1.get(i).getUrl().equals(entryList2.get(i).getUrl())) {
                return false;
            }
        }
        return true;
    }


    private static class CollectState {

        private final boolean updated;
        private final boolean dataChanged;

        private CollectState(boolean updated, boolean dataChanged) {
            this.updated = updated;
            this.dataChanged = dataChanged;
        }

        public boolean isUpdated() {
            return updated;
        }

        public boolean isDataChanged() {
            return dataChanged;
        }

    }

}
