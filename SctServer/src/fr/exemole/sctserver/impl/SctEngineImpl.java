/* SctEngine - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.ConfState;
import fr.exemole.sctserver.api.EngineContext;
import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.api.FieldVariantManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.ScrutariSourceManager;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.api.conf.ConfConstants;
import fr.exemole.sctserver.api.conf.ConfFileInfo;
import fr.exemole.sctserver.api.conf.ConfIOException;
import fr.exemole.sctserver.api.conf.EngineConf;
import fr.exemole.sctserver.api.log.SctLogConstants;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.tools.DataValidatorBuilder;
import fr.exemole.sctserver.tools.FirstAddProviderFactory;
import fr.exemole.sctserver.tools.SctJsAnalyser;
import fr.exemole.sctserver.tools.log.UpdateLogHandler;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.io.ResourceStorage;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.io.ResourceUtils;
import net.mapeadores.util.jslib.JsAnalyser;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logging.MessageLog;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.logging.TimeLog;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataValidator;
import net.scrutari.datauri.BaseURI;
import net.scrutari.db.api.AliasManager;
import net.scrutari.db.api.DataCoder;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.caches.DataCacheReaderFactory;
import net.scrutari.db.api.caches.DataCacheWriter;
import net.scrutari.db.tools.AliasManagerBuilder;
import net.scrutari.db.tools.DataCoderUtils;
import net.scrutari.db.tools.FieldRankManagerBuilder;
import net.scrutari.db.tools.FirstAddProvider;
import net.scrutari.db.tools.ScrutariDBBuilder;
import net.scrutari.db.tools.URIHistory;
import net.scrutari.xml.BaseURIXmlEngine;
import net.scrutari.xml.ConfMetadataEngine;
import net.scrutari.xml.FirstPassEngine;
import net.scrutari.xml.Parameters;
import net.scrutari.xml.SctXmlUtils;
import net.scrutari.xml.SecondPassEngine;
import net.scrutari.xml.XmlChecker;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public final class SctEngineImpl implements SctEngine {

    private final String name;
    private final EngineContext engineContext;
    private final EngineStorage engineStorage;
    private final EngineConf engineConf;
    private final ScrutariSourceManagerImpl scrutariSourceManager;
    private final EngineMetadataImpl engineMetadata;
    private final FieldVariantManagerImpl fieldVariantManager;
    private final AttributeDefManagerImpl attributeDefManager;
    private final JsAnalyser jsAnalyser;
    private final ResourceStorages resourceStorages;
    private ScrutariSessionImpl scrutariSession;
    private final ConfStateImpl confState = new ConfStateImpl();
    private boolean onReload = false;

    public SctEngineImpl(String name, EngineStorage engineStorage, EngineContext engineContext) {
        this.name = name;
        this.engineContext = engineContext;
        this.engineStorage = engineStorage;
        this.engineConf = engineStorage.getEngineConf();
        this.engineMetadata = new EngineMetadataImpl();
        this.scrutariSourceManager = new ScrutariSourceManagerImpl(engineStorage);
        this.fieldVariantManager = new FieldVariantManagerImpl();
        this.attributeDefManager = new AttributeDefManagerImpl();
        this.jsAnalyser = new SctJsAnalyser(this);
        this.resourceStorages = merge(engineStorage, engineContext);
        init();
    }

    private void init() {
        boolean done = false;
        TimeLog timeLog = new TimeLog("cache@" + name);
        LogStorage logStorage = engineStorage.getLogStorage();
        logStorage.removeDir(SctLogConstants.LOAD_DIR);
        logStorage.removeDir(SctLogConstants.EXCEPTION_DIR);
        try (PrintWriter pw = logStorage.getPrintWriter(SctLogConstants.LOAD_DIR, "_running.txt")) {
            timeLog.setPrintWriter(pw);
            String exceptionLog = null;
            try {
                timeLog.logAction("cacheCheck");
                EngineStorage.CacheCheck cacheCheck = engineStorage.checkLastCache(null);
                if (cacheCheck != null) {
                    timeLog.logAction("aliasManager");
                    AliasManager aliasManager = (AliasManager) engineStorage.getCachedObject(cacheCheck.getScrutariDBName(), AliasManager.class);
                    FieldRankManager fieldRankManager = (FieldRankManager) engineStorage.getCachedObject(cacheCheck.getScrutariDBName(), FieldRankManager.class);
                    if ((aliasManager != null) && (fieldRankManager != null)) {
                        timeLog.logAction("confDirectory");
                        reloadConfDirectory();
                        timeLog.logAction("scrutariSourceManager");
                        scrutariSourceManager.reloadConf();
                        timeLog.logAction("firstAddProvider");
                        FirstAddProvider firstAddProvider = FirstAddProviderFactory.build(engineStorage, aliasManager, cacheCheck.getScrutariDBName());
                        timeLog.logAction("scrutariDB");
                        ScrutariDB newScrutariDB = ScrutariDBBuilder.build(cacheCheck.getScrutariDBName(), fieldRankManager, cacheCheck.getDataCacheReaderFactory(), cacheCheck.getLexieCacheReaderFactory(), aliasManager, firstAddProvider, timeLog);
                        scrutariSession = ScrutariSessionImpl.newInstance(this, newScrutariDB, aliasManager, engineConf, timeLog);
                        done = true;
                    }
                }
            } catch (Exception e) {
                timeLog.log(e);
                exceptionLog = TimeLog.toExceptionLogString(name, e);
            }
            timeLog.setPrintWriter(null);
            logStorage.log(SctLogConstants.LOAD_DIR, "_running.txt", null);
            if (done) {
                logStorage.log(SctLogConstants.LOAD_DIR, "init.txt", timeLog.logStep("end").toString());
            }
            logStorage.log(SctLogConstants.EXCEPTION_DIR, "cache.txt", exceptionLog);
        }
        if (!done) {
            onReload = true;
            reloadConfDirectory();
            scrutariSourceManager.reloadConf();
            reinit("init", "init.txt", ScrutariSourceManager.LOCAL);
            onReload = false;
        }
    }

    @Override
    public boolean reloadConf() {
        if (onReload) {
            return false;
        }
        onReload = true;
        reloadConfDirectory();
        scrutariSourceManager.reloadConf();
        scrutariSession.reloadConf(engineConf);
        onReload = false;
        return true;
    }

    @Override
    public boolean update() {
        return reload("update", false);
    }

    @Override
    public boolean reload(boolean force) {
        return reload("reload", force);
    }

    @Override
    public String getEngineName() {
        return name;
    }

    @Override
    public EngineContext getEngineContext() {
        return engineContext;
    }

    @Override
    public EngineMetadata getEngineMetadata() {
        return engineMetadata;
    }

    @Override
    public synchronized ScrutariSession getScrutariSession() {
        return scrutariSession;
    }

    @Override
    public ScrutariSourceManager getScrutariSourceManager() {
        return scrutariSourceManager;
    }

    @Override
    public FieldVariantManager getFieldVariantManager() {
        return fieldVariantManager;
    }

    @Override
    public AttributeDefManager getAttributeDefManager() {
        return attributeDefManager;
    }

    @Override
    public EngineStorage getEngineStorage() {
        return engineStorage;
    }

    @Override
    public ConfState getConfState() {
        return confState;
    }

    @Override
    public JsAnalyser getJsAnalyser() {
        return jsAnalyser;
    }

    @Override
    public ResourceStorages getResourceStorages() {
        return resourceStorages;
    }

    private boolean reload(String reloadType, boolean forceReload) {
        if (onReload) {
            return false;
        }
        if (scrutariSession != null) {
            if (ScrutariDBName.isCurrentName(scrutariSession.getScrutariDB().getScrutariDBName())) {
                return false;
            }
        }
        onReload = true;
        reloadConfDirectory();
        scrutariSourceManager.reloadConf();
        reinit(reloadType, "update.txt", (forceReload) ? ScrutariSourceManager.FORCE : ScrutariSourceManager.CHECK);
        onReload = false;
        return true;
    }

    private void reinit(String reloadType, String logFile, int level) {
        UpdateLogHandler updateLogHandler = new UpdateLogHandler();
        TimeLog timeLog = new TimeLog(reloadType + "@" + name);
        LogStorage logStorage = engineStorage.getLogStorage();
        String exceptionLog = null;
        try (PrintWriter pw = logStorage.getPrintWriter(SctLogConstants.LOAD_DIR, "_running.txt")) {
            timeLog.setPrintWriter(pw);
            timeLog.logAction("Collect");
            boolean done = scrutariSourceManager.collect(updateLogHandler, level);
            if ((!reloadType.equals("update")) || (done)) {
                newSession(updateLogHandler, timeLog);
                MessageLog messageLog = updateLogHandler.toMessageLog();
                String dataLogContent = null;
                if (!messageLog.isEmpty()) {
                    dataLogContent = LogUtils.toXmlString(messageLog);
                }
                logStorage.log(SctLogConstants.DATA_DIR, "data.xml", dataLogContent);
            }
        } catch (Exception e) {
            timeLog.log(e);
            exceptionLog = TimeLog.toExceptionLogString(name, e);
        }
        timeLog.setPrintWriter(null);
        logStorage.log(SctLogConstants.LOAD_DIR, "_running.txt", null);
        if (!timeLog.isEmpty()) {
            logStorage.log(SctLogConstants.LOAD_DIR, logFile, timeLog.logStep("end").toString());
            logStorage.log(SctLogConstants.EXCEPTION_DIR, "load.txt", exceptionLog);
        }
    }

    private void newSession(UpdateLogHandler updateLogHandler, TimeLog timeLog) {
        cleanCache();
        ScrutariDBName scrutariDbName = ScrutariDBName.newName();
        timeLog.logAction("aliasManager");
        AliasManager aliasManager = checkBaseURI(updateLogHandler, timeLog);
        engineStorage.cacheObject(scrutariDbName, aliasManager);
        timeLog.logAction("firstAddProvider");
        FirstAddProvider firstAddProvider = FirstAddProviderFactory.build(engineStorage, aliasManager, scrutariDbName);
        timeLog.logAction("dataCoder");
        URIHistory uriHistory = URIHistory.build(engineConf.getInitialCodeMatchArray(aliasManager), engineStorage.getWholeURITree(aliasManager), firstAddProvider);
        DataCoder encapsulatingDataCoder = DataCoderUtils.encapsulate(uriHistory);
        timeLog.logAction("fieldRankManager");
        FieldRankManager fieldRankManager = FieldRankManagerBuilder.build(engineConf.getAttributeDefArrayMap(), engineConf.getPertinenceOrderArray());
        timeLog.logAction("dataCacheWriter");
        DataCacheWriter dataCacheWriter = engineStorage.getDataCacheWriter(scrutariDbName);
        parseXml(dataCacheWriter, encapsulatingDataCoder, aliasManager, updateLogHandler, timeLog);
        DataCacheReaderFactory dataCacheReaderFactory = dataCacheWriter.endCacheWrite();
        ScrutariDB newScrutariDB = ScrutariDBBuilder.build(scrutariDbName, fieldRankManager, dataCacheReaderFactory, engineStorage.getLexieCacheWriter(scrutariDbName), aliasManager, firstAddProvider, timeLog);
        engineStorage.cacheObject(scrutariDbName, fieldRankManager);
        timeLog.logAction("uriTree");
        engineStorage.saveWholeURITree(uriHistory.toURITree());
        if (uriHistory.hasNewURI()) {
            engineStorage.saveAddedURITree(uriHistory.getNewlyAddedURITree(), scrutariDbName);
        }
        timeLog.logAction("scrutariSession");
        ScrutariSessionImpl newScrutariSession = ScrutariSessionImpl.newInstance(this, newScrutariDB, aliasManager, engineConf, timeLog);
        synchronized (this) {
            scrutariSession = newScrutariSession;
        }
    }

    private void cleanCache() {
        Set<ScrutariDBName> remainingSet = new HashSet<ScrutariDBName>();
        if (scrutariSession != null) {
            remainingSet.add(scrutariSession.getScrutariDB().getScrutariDBName());
        }
        engineStorage.cleanCache(remainingSet);
    }

    private AliasManager checkBaseURI(UpdateLogHandler updateLogHandler, TimeLog timeLog) {
        List<ScrutariSource> scrutariSourceList = scrutariSourceManager.getScrutariSourceList();
        AliasManagerBuilder aliasManagerBuilder = buildFromSource(scrutariSourceList, updateLogHandler);
        BaseURIXmlEngine engine = new BaseURIXmlEngine(updateLogHandler);
        for (ScrutariSource scrutariSource : scrutariSourceList) {
            String scrutariSourceName = scrutariSource.getScrutariSourceDef().getName();
            updateLogHandler.setCurrentScrutariSource(scrutariSourceName);
            List<EngineStorage.StoredData> dataList = engineStorage.getDataList(scrutariSourceName);
            if (dataList.isEmpty()) {
                continue;
            }
            boolean ok = checkXml(scrutariSource, dataList, updateLogHandler);
            if (ok) {
                for (EngineStorage.StoredData data : dataList) {
                    try (InputStream is = data.getInputStream()) {
                        BaseURI baseURI = engine.read(is);
                        if (baseURI != null) {
                            String existingSourceName = aliasManagerBuilder.getSourceName(baseURI);
                            if ((existingSourceName != null) && (!existingSourceName.equals(scrutariSourceName))) {
                                SctXmlUtils.reference(updateLogHandler, "_ error.existing.baseuri", baseURI);
                            } else {
                                BaseURI currentMain = aliasManagerBuilder.getMainBaseURI(scrutariSourceName);
                                if (currentMain == null) {
                                    aliasManagerBuilder.putMainBaseURI(scrutariSourceName, baseURI);
                                } else {
                                    aliasManagerBuilder.putAliasBaseURI(scrutariSourceName, baseURI);
                                }
                            }
                        }
                    } catch (SAXException | IOException e) {
                        scrutariSourceManager.setState(scrutariSource, CollectResult.DATA_NOTXML_STATE);
                        updateLogHandler.exception(e);
                    }
                }
            }
        }
        return aliasManagerBuilder.toAliasManager();
    }

    private boolean checkXml(ScrutariSource scrutariSource, List<EngineStorage.StoredData> dataList, UpdateLogHandler updateLogHandler) {
        XmlChecker xmlChecker = new XmlChecker();
        boolean ok = true;
        for (EngineStorage.StoredData data : dataList) {
            boolean done = false;
            try (InputStream is = data.getInputStream()) {
                xmlChecker.check(is);
                done = true;
            } catch (SAXException | IOException e) {
                scrutariSourceManager.setState(scrutariSource, CollectResult.DATA_NOTXML_STATE);
                updateLogHandler.exception(e);
            }
            if ((!done) && (data.hasBackup())) {
                boolean restored = false;
                try {
                    restored = data.restoreBackup();
                } catch (IOException ioe) {
                    updateLogHandler.exception(ioe);
                }
                if (restored) {
                    try (InputStream is = data.getInputStream()) {
                        xmlChecker.check(is);
                        done = true;
                    } catch (SAXException | IOException e) {
                        updateLogHandler.exception(e);
                    }
                }
            }
            if (!done) {
                ok = false;
            }
        }
        return ok;
    }

    private void parseXml(DataCacheWriter dataCacheWriter, DataCoder dataCoder, AliasManager aliasManager, UpdateLogHandler updateLogHandler, TimeLog timeLog) {
        FirstPassEngine firstPassEngine = new FirstPassEngine();
        ConfMetadataEngine confMetadataEngine = new ConfMetadataEngine();
        List<ScrutariSource> scrutariSourceList = scrutariSourceManager.getScrutariSourceList();
        List<List<EngineStorage.StoredData>> list = new ArrayList<List<EngineStorage.StoredData>>();
        DataValidator dataValidator = engineConf.getDataValidator();
        if (dataValidator == null) {
            dataValidator = DataValidatorBuilder.NEUTRAL_DATAVALIDATOR;
        }
        for (ScrutariSource scrutariSource : scrutariSourceManager.getScrutariSourceList()) {
            String scrutariSourceName = scrutariSource.getScrutariSourceDef().getName();
            BaseURI baseURI = aliasManager.getMainBaseURI(scrutariSourceName);
            if (baseURI == null) {
                list.add(null);
                continue;
            }
            timeLog.logAction("baseXmlEngine@" + scrutariSourceName);
            List<EngineStorage.StoredData> dataList = engineStorage.getDataList(scrutariSourceName);
            list.add(dataList);
            updateLogHandler.setCurrentScrutariSource(scrutariSourceName);
            BaseData.Builder baseDataBuilder = new BaseData.Builder(baseURI);
            Integer baseCode = dataCoder.getCode(baseURI, true);
            Parameters parameters = new Parameters(dataCacheWriter, dataCoder, dataValidator, updateLogHandler, baseURI, baseCode);
            boolean first = true;
            for (EngineStorage.StoredData data : dataList) {
                try (InputStream is = data.getInputStream()) {
                    firstPassEngine.run(baseDataBuilder, parameters, is, first);
                } catch (SAXException | IOException e) {
                    scrutariSourceManager.setState(scrutariSource, CollectResult.DATA_NOTXML_STATE);
                    updateLogHandler.exception(e);
                }
                first = false;
            }
            if (engineStorage.hasConfBaseMetadata(scrutariSourceName)) {
                try (InputStream is = engineStorage.getConfBaseMetadataInputstream(scrutariSourceName)) {
                    confMetadataEngine.run(baseDataBuilder, is, updateLogHandler, dataValidator);
                } catch (SAXException | IOException e) {
                    updateLogHandler.exception(e);
                }
            }
            BaseData baseData = baseDataBuilder.toBaseData(parameters.getBaseCode(), scrutariSourceName);
            dataCacheWriter.cacheBaseData(baseData);
        }
        SecondPassEngine secondPassEngine = new SecondPassEngine(dataCacheWriter, dataCoder, updateLogHandler, dataValidator);
        int dataListSize = list.size();
        for (int i = 0; i < dataListSize; i++) {
            List<EngineStorage.StoredData> dataList = list.get(i);
            if (dataList == null) {
                continue;
            }
            ScrutariSource scrutariSource = scrutariSourceList.get(i);
            String scrutariSourceName = scrutariSource.getScrutariSourceDef().getName();
            timeLog.logAction("indexationXmlEngine@" + scrutariSourceName);
            BaseURI baseURI = aliasManager.getMainBaseURI(scrutariSourceName);
            updateLogHandler.setCurrentScrutariSource(scrutariSourceName);
            for (EngineStorage.StoredData data : dataList) {
                try (InputStream is = data.getInputStream()) {
                    secondPassEngine.run(is, baseURI);
                } catch (SAXException | IOException e) {
                    scrutariSourceManager.setState(scrutariSource, CollectResult.DATA_NOTXML_STATE);
                    updateLogHandler.exception(e);
                }
            }
        }
        for (int i = 0; i < dataListSize; i++) {
            List<EngineStorage.StoredData> dataList = list.get(i);
            if (dataList == null) {
                continue;
            }
            for (EngineStorage.StoredData data : dataList) {
                try {
                    data.backup();
                } catch (IOException ioe) {
                    updateLogHandler.exception(ioe);
                }
            }
        }
    }

    private void reloadConfDirectory() {
        try {
            engineConf.reload();
            checkConfLog(ConfConstants.SOURCES_NAME);
            checkConfLog(ConfConstants.METADATA_NAME);
            checkConfLog(ConfConstants.ATTRIBUTES_NAME);
            checkConfLog(ConfConstants.CATEGORIES_NAME);
            checkConfLog(ConfConstants.URI_CODES_NAME);
            checkConfLog(ConfConstants.OPTIONS_NAME);
            checkConfLog(ConfConstants.VALIDATION_NAME);
            checkConfLog(ConfConstants.FIELDS_NAME);
            confState.setOkState();
            engineMetadata.reload(engineConf.getEngineMetadata());
            fieldVariantManager.reload(engineConf.getFieldVariantMap());
            attributeDefManager.reload(engineConf.getAttributeDefArrayMap());
        } catch (ConfIOException cioe) {
            confState.setGlobalConfigException(cioe);
        }
    }

    private void checkConfLog(String name) {
        ConfFileInfo confFileInfo = engineConf.getConfFileInfo(name);
        if (!confFileInfo.isHere()) {
            confState.setFileState(name, ConfConstants.FILE_MISSING);
        } else {
            if (!confFileInfo.isEmpty()) {
                int errorType = 0;
                for (MessageSource.Entry entry : confFileInfo.getEntryList()) {
                    String category = entry.getCategory();
                    if (category.equals(DomMessages.SEVERE_MALFORMED)) {
                        errorType = 2;
                        break;
                    } else if (category.startsWith("severe.")) {
                        errorType = 1;
                    }
                }
                switch (errorType) {
                    case 2:
                        confState.setFileState(name, ConfConstants.FILE_XML_MALFORMED);
                        break;
                    case 1:
                        confState.setFileState(name, ConfConstants.FILE_XML_ERROR);
                        break;
                    default:
                        confState.setFileState(name, ConfConstants.FILE_XML_WARNING);
                        break;
                }
            } else {
                confState.setFileState(name, ConfConstants.FILE_OK);
            }
            String logContent = null;
            if (!confFileInfo.isEmpty()) {
                logContent = LogUtils.toXmlString(confFileInfo);
            }
            engineStorage.getLogStorage().log(SctLogConstants.CONF_DIR, name + ".xml", logContent);
        }

    }

    public static AliasManagerBuilder buildFromSource(List<ScrutariSource> scrutariSourceList, MessageHandler messageHandler) {
        AliasManagerBuilder aliasManagerBuilder = new AliasManagerBuilder();
        for (ScrutariSource scrutariSource : scrutariSourceList) {
            ScrutariSourceDef scrutariSourceDef = scrutariSource.getScrutariSourceDef();
            String scrutariSourceName = scrutariSourceDef.getName();
            BaseURI mainBaseURI = scrutariSourceDef.getMainBaseURI();
            if (mainBaseURI != null) {
                if (aliasManagerBuilder.containsBaseURI(mainBaseURI)) {
                    SctXmlUtils.reference(messageHandler, "_ error.existing.baseuri", mainBaseURI);
                } else {
                    aliasManagerBuilder.putMainBaseURI(scrutariSourceName, mainBaseURI);
                }
            }
            for (BaseURI baseURI : scrutariSourceDef.getAliasList()) {
                if (aliasManagerBuilder.containsBaseURI(baseURI)) {
                    SctXmlUtils.reference(messageHandler, "_ error.existing.baseuri", mainBaseURI);
                } else {
                    aliasManagerBuilder.putAliasBaseURI(scrutariSourceName, baseURI);
                }
            }
        }
        return aliasManagerBuilder;
    }

    private static ResourceStorages merge(EngineStorage engineStorage, EngineContext engineContext) {
        ResourceStorages contextResourceStorages = engineContext.getResourceStorages();
        int length = contextResourceStorages.size();
        ResourceStorage[] array = new ResourceStorage[length + 1];
        array[0] = engineStorage.getEngineResourceStorage();
        for (int i = 0; i < length; i++) {
            array[i + 1] = contextResourceStorages.get(i);
        }
        return ResourceUtils.toResourceStorages(array);
    }


}
