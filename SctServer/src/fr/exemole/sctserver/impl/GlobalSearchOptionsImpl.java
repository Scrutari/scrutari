/* SctServer - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.conf.EngineConf;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.primitives.PrimUtils;
import net.scrutari.data.CorpusData;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.stats.CountStats;
import net.scrutari.db.api.stats.ScrutariDBStats;
import net.scrutari.db.tools.stats.CountStatsBuilder;
import net.scrutari.db.tools.stats.LangStatsBuilder;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.tools.options.OptionsUtils;
import net.scrutari.searchengine.tools.options.PertinencePonderationBuilder;
import net.scrutari.db.api.stats.LangStats;


/**
 *
 * @author Vincent Calame
 */
class GlobalSearchOptionsImpl implements GlobalSearchOptions {

    private final PertinencePonderation pertinencePonderation;
    private final Map<Integer, Category> byRankMap;
    private final Map<Integer, Category> byCorpusMap;
    private final List<Category> categoryList;

    GlobalSearchOptionsImpl(PertinencePonderation pertinencePonderation, List<Category> categoryArray, Map<Integer, Category> byRankMap, Map<Integer, Category> byCorpusMap) {
        this.pertinencePonderation = pertinencePonderation;
        this.categoryList = categoryArray;
        this.byRankMap = byRankMap;
        this.byCorpusMap = byCorpusMap;
    }

    @Override
    public PertinencePonderation getPertinencePonderation() {
        return pertinencePonderation;
    }

    @Override
    public List<Category> getCategoryList() {
        return categoryList;
    }

    @Override
    public Category getCategoryByRank(int rank) {
        if (byRankMap == null) {
            return null;
        }
        return byRankMap.get(rank);
    }

    @Override
    public Category getCategoryByName(String categoryName) {
        if (categoryList.isEmpty()) {
            return null;
        }
        for (Category category : categoryList) {
            if (category.getCategoryDef().getName().equals(categoryName)) {
                return category;
            }
        }
        return null;
    }

    @Override
    public Category getCategoryByCorpus(Integer corpusCode) {
        if (byCorpusMap == null) {
            return null;
        }
        return byCorpusMap.get(corpusCode);
    }

    static GlobalSearchOptionsImpl newInstance(ScrutariDB scrutariDB, EngineConf engineConf) {
        PertinencePonderation globalPertinencePonderation = engineConf.getGlobalPertinencePonderation();
        if (globalPertinencePonderation == null) {
            globalPertinencePonderation = PertinencePonderationBuilder.DEFAULT_PONDERATION;
        }
        CategoryDef[] categoryDefArray = engineConf.getCategoryDefArray();
        Map<Integer, Category> byRankMap = null;
        Map<Integer, Category> byCorpusMap = null;
        Category[] categoryArray = null;
        if (categoryDefArray != null) {
            int length = categoryDefArray.length;
            CategoryBuilder[] builderArray = new CategoryBuilder[length];
            CategoryBuilder defaultCategoryBuilder = null;
            Set<Integer> insertedSet = new HashSet<Integer>();
            try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
                for (int i = 0; i < length; i++) {
                    CategoryDef categoryDef = categoryDefArray[i];
                    CategoryBuilder builder = new CategoryBuilder(categoryDef);
                    builderArray[i] = builder;
                    String name = categoryDef.getName();
                    if (name.equals(CategoryDef.DEFAULT_NAME)) {
                        defaultCategoryBuilder = builder;
                    } else {
                        for (CorpusURI corpusURI : categoryDef.getCorpusURIList()) {
                            Integer corpusCode = dataAccess.getCode(corpusURI);
                            if ((corpusCode != null) && (!insertedSet.contains(corpusCode))) {
                                insertedSet.add(corpusCode);
                                builder.addCorpus(corpusCode);
                            }
                        }
                    }
                }
                for (CorpusData corpusData : dataAccess.getCorpusDataList()) {
                    Integer corpusCode = corpusData.getCorpusCode();
                    if (!insertedSet.contains(corpusCode)) {
                        defaultCategoryBuilder.addCorpus(corpusCode);
                    }
                }
            }
            categoryArray = new Category[length];
            byRankMap = new HashMap<Integer, Category>();
            byCorpusMap = new HashMap<Integer, Category>();
            ScrutariDBStats scrutariDBStats = scrutariDB.getStats();
            for (int i = 0; i < length; i++) {
                Category category = builderArray[i].toCategory(scrutariDBStats);
                categoryArray[i] = category;
                byRankMap.put(category.getCategoryDef().getRank(), category);
                populate(byCorpusMap, category);
            }
        }
        List<Category> categoryList;
        if (categoryArray == null) {
            categoryList = OptionsUtils.EMPTY_CATEGORYLIST;
        } else {
            categoryList = OptionsUtils.wrap(categoryArray);
        }
        return new GlobalSearchOptionsImpl(globalPertinencePonderation, categoryList, byRankMap, byCorpusMap);
    }

    private static void populate(Map<Integer, Category> byCorpusMap, Category category) {
        for (Integer corpusCode : category.getCorpusCodeList()) {
            byCorpusMap.put(corpusCode, category);
        }
    }


    private static class CategoryBuilder {

        private final CategoryDef categoryDef;
        private final CountStatsBuilder countStatsBuilder;
        private final LangStatsBuilder ficheStatsBuilder;
        private final List<Integer> corpusList = new ArrayList<Integer>();

        private CategoryBuilder(CategoryDef categoryDef) {
            this.categoryDef = categoryDef;
            this.countStatsBuilder = CountStatsBuilder.newCategoryInstance();
            this.ficheStatsBuilder = new LangStatsBuilder();
        }

        private void addCorpus(Integer corpusCode) {
            corpusList.add(corpusCode);
        }

        private Category toCategory(ScrutariDBStats scrutariDBStats) {
            int count = corpusList.size();
            Integer[] array = new Integer[count];
            for (int i = 0; i < count; i++) {
                Integer corpusCode = corpusList.get(i);
                CountStats countStats = scrutariDBStats.getCountStats(corpusCode);
                countStatsBuilder.increaseCorpusCount(1);
                countStatsBuilder.increaseFicheCount(countStats.getFicheCount());
                ficheStatsBuilder.addLangStats(scrutariDBStats.getLangStats(corpusCode));
                array[i] = corpusCode;
            }
            return new InternalCategory(categoryDef, countStatsBuilder.toCountStats(), ficheStatsBuilder.toLangStats(), PrimUtils.wrap(array));
        }

    }


    private static class InternalCategory implements Category {

        private final CategoryDef categoryDef;
        private final CountStats countStats;
        private final LangStats ficheStats;
        private final List<Integer> corpusCodeList;

        private InternalCategory(CategoryDef categoryDef, CountStats countStats, LangStats ficheStats, List<Integer> corpusCodeList) {
            this.categoryDef = categoryDef;
            this.countStats = countStats;
            this.ficheStats = ficheStats;
            this.corpusCodeList = corpusCodeList;
        }

        @Override
        public CategoryDef getCategoryDef() {
            return categoryDef;
        }

        @Override
        public CountStats getCountStats() {
            return countStats;
        }

        @Override
        public LangStats getFicheStats() {
            return ficheStats;
        }

        @Override
        public List<Integer> getCorpusCodeList() {
            return corpusCodeList;
        }

    }

}
