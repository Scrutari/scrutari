/* SctServer - Copyright (c) 2014-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.ConfState;
import fr.exemole.sctserver.api.conf.ConfConstants;
import fr.exemole.sctserver.api.conf.ConfIOException;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
class ConfStateImpl implements ConfState {

    private final Map<String, Short> stateMap = new HashMap<String, Short>();
    private short state = ConfConstants.GLOBAL_UNDEFINED;
    private String confIOMessage = null;

    ConfStateImpl() {
    }

    @Override
    public String getConfIOMessage() {
        return confIOMessage;
    }

    @Override
    public short getGlobalState() {
        return state;
    }

    @Override
    public short getFileState(String name) {
        Short sh = stateMap.get(name);
        if (sh == null) {
            return ConfConstants.FILE_STATE_UNKNOWN;
        }
        return sh;
    }

    void setGlobalConfigException(ConfIOException ioe) {
        state = ConfConstants.GLOBAL_CONFIO_ERROR;
        confIOMessage = ioe.getFullMessage();
    }

    void setOkState() {
        this.state = ConfConstants.GLOBAL_OK;
    }

    void setFileState(String name, short fileState) {
        stateMap.put(name, fileState);
    }

}
