/* SctServer - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.EngineMetadata;
import java.net.URL;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;


/**
 *
 * @author Vincent Calame
 */
class EngineMetadataImpl implements EngineMetadata {

    private EngineMetadata source;

    EngineMetadataImpl() {

    }

    void reload(EngineMetadata source) {
        this.source = source;
    }

    @Override
    public Labels getTitleLabels() {
        if (source != null) {
            return source.getTitleLabels();
        } else {
            return LabelUtils.EMPTY_LABELS;
        }
    }

    @Override
    public Attributes getAttributes() {
        if (source != null) {
            return source.getAttributes();
        } else {
            return AttributeUtils.EMPTY_ATTRIBUTES;
        }
    }

    @Override
    public Phrases getPhrases() {
        if (source != null) {
            return source.getPhrases();
        } else {
            return LabelUtils.EMPTY_PHRASES;
        }
    }

    @Override
    public URL getIcon() {
        if (source != null) {
            return source.getIcon();
        } else {
            return null;
        }
    }

    @Override
    public URL getWebsite() {
        if (source != null) {
            return source.getWebsite();
        } else {
            return null;
        }
    }

    @Override
    public Lang getDefaultLang() {
        if (source != null) {
            return source.getDefaultLang();
        } else {
            return null;
        }
    }

}
