/* SctEngine - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.impl;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.SearchStoragePolicy;
import fr.exemole.sctserver.api.conf.EngineConf;
import fr.exemole.sctserver.api.feed.SctFeedCache;
import fr.exemole.sctserver.api.feed.SctFeedInfo;
import fr.exemole.sctserver.api.log.QLog;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.tools.AddURITreeProvider;
import fr.exemole.sctserver.tools.BuildingUtils;
import fr.exemole.sctserver.tools.feed.SctFeedInfoBuilder;
import fr.exemole.sctserver.tools.log.SctLogUtils;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.TimeLog;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.searchengine.FicheSearchEngine;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchSource;
import net.scrutari.searchengine.tools.operands.search.LogicalSearchOperationEngine;
import net.scrutari.searchengine.tools.options.SearchOptionsBuilder;
import net.scrutari.supermotcle.api.SupermotcleProvider;
import net.scrutari.supermotcle.tools.SupermotcleProviderBuilder;


/**
 *
 * @author Vincent Calame
 */
class ScrutariSessionImpl implements ScrutariSession {

    private final SctEngine engine;
    private final ScrutariDB scrutariDB;
    private final ScrutariDataURIChecker scrutariDataURIChecker;
    private final SupermotcleProvider supermotcleProvider;
    private final SctFeedInfo sctFeedInfo;
    private final SctFeedCache sctFeedCache;
    private GlobalSearchOptionsImpl globalSearchOptions;
    private int subid;

    private ScrutariSessionImpl(SctEngine engine, ScrutariDB scrutariDB, ScrutariDataURIChecker scrutariDataURIChecker, SupermotcleProvider supermotcleProvider) {
        this.engine = engine;
        this.scrutariDB = scrutariDB;
        this.scrutariDataURIChecker = scrutariDataURIChecker;
        this.supermotcleProvider = supermotcleProvider;
        ScrutariDBName scrutariDBName = scrutariDB.getScrutariDBName();
        this.sctFeedInfo = SctFeedInfoBuilder.build(engine, scrutariDBName);
        EngineStorage engineStorage = engine.getEngineStorage();
        this.sctFeedCache = engineStorage.getSctFeedCache(scrutariDBName);
        this.subid = engineStorage.getQIdStorage().getMaxSubId(scrutariDBName) + 1;
    }

    @Override
    public String getEngineName() {
        return engine.getEngineName();
    }

    @Override
    public SctEngine getEngine() {
        return engine;
    }

    @Override
    public ScrutariDB getScrutariDB() {
        return scrutariDB;
    }

    @Override
    public ScrutariDataURIChecker getScrutariDataURIChecker() {
        return scrutariDataURIChecker;
    }

    @Override
    public SupermotcleProvider getSupermotcleProvider() {
        return supermotcleProvider;
    }

    @Override
    public SctFeedInfo getSctFeedInfo() {
        return sctFeedInfo;
    }

    @Override
    public SctFeedCache getSctFeedCache() {
        return sctFeedCache;
    }

    @Override
    public GlobalSearchOptions getGlobalSearchOptions() {
        return globalSearchOptions;
    }

    @Override
    public FicheSearchResult search(SearchOperation searchOperation, SearchOptions searchOptions, SearchStoragePolicy searchStoragePolicy) {
        EngineStorage engineStorage = engine.getEngineStorage();
        boolean store = searchStoragePolicy.store();
        QId qId = null;
        ScrutariDBName scrutariDBName = scrutariDB.getScrutariDBName();
        if (store) {
            qId = engineStorage.getQIdStorage().containsQId(searchOperation.getCanonicalQ(), searchOptions.getSearchOptionsDef());
            if (qId != null) {
                FicheSearchResult existingFicheSearchResult = getSearch(qId);
                if (existingFicheSearchResult != null) {
                    if (searchStoragePolicy.alwaysLog()) {
                        logSearch(existingFicheSearchResult, searchStoragePolicy);
                    }
                    return existingFicheSearchResult;
                }
            } else {
                synchronized (this) {
                    qId = new QId(scrutariDBName, subid);
                    subid++;
                }
            }
        }
        FicheSearchResult ficheSearchResult = FicheSearchEngine.search(scrutariDB, searchOperation, searchOptions, getGlobalSearchOptions(), qId);
        if (store) {
            logSearch(ficheSearchResult, searchStoragePolicy);
            engineStorage.getFicheSearchCache(scrutariDBName).saveFicheSearchResult(ficheSearchResult);
            engineStorage.getQIdStorage().saveFicheSearchSource(ficheSearchResult.getFicheSearchSource());
        }
        return ficheSearchResult;
    }

    @Override
    public FicheSearchResult getSearch(QId qId) {
        EngineStorage engineStorage = engine.getEngineStorage();
        FicheSearchResult ficheSearchResult;
        try (LexieAccess lexieAccess = scrutariDB.openLexieAccess()) {
            ficheSearchResult = engineStorage.getFicheSearchCache(scrutariDB.getScrutariDBName()).getFicheSearchResult(qId, lexieAccess);
        }
        if (ficheSearchResult != null) {
            return ficheSearchResult;
        }
        FicheSearchSource ficheSearchSource = engineStorage.getQIdStorage().getFicheSearchSource(qId);
        if (ficheSearchSource == null) {
            return null;
        }
        SearchOptionsDef searchOptionsDef = ficheSearchSource.getSearchOptionsDef();
        SearchOptions searchOptions;
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            if (!searchOptionsDef.isEmpty()) {
                SearchOptionsBuilder searchOptionsBuilder = new SearchOptionsBuilder(searchOptionsDef);
                BuildingUtils.computeReduction(searchOptionsBuilder, dataAccess, globalSearchOptions);
                searchOptionsBuilder.checkEligibility(dataAccess, this, LogUtils.NULL_MULTIMESSAGEHANDLER, new AddURITreeProvider(engineStorage));
                searchOptions = searchOptionsBuilder.toSearchOptions();
            } else {
                searchOptions = SearchOptionsBuilder.EMPTY_SEARCHOPTIONS;
            }
            CanonicalQ canonicalQ = ficheSearchSource.getCanonicalQ();
            SearchOperation searchOperation = LogicalSearchOperationEngine.parse(canonicalQ.toString(), LogUtils.NULL_MULTIMESSAGEHANDLER, scrutariDB.getFieldRankManager());
            if (searchOperation == null) {
                return null;
            }
            ficheSearchResult = FicheSearchEngine.search(scrutariDB, searchOperation, searchOptions, getGlobalSearchOptions(), qId);
            engineStorage.getFicheSearchCache(scrutariDB.getScrutariDBName()).saveFicheSearchResult(ficheSearchResult);
        }
        return ficheSearchResult;
    }

    private void logSearch(FicheSearchResult ficheSearchResult, SearchStoragePolicy searchStoragePolicy) {
        QLog qLog = SctLogUtils.toQLog(ficheSearchResult, searchStoragePolicy.getOrigin());
        engine.getEngineStorage().getLogStorage().log(this, qLog);
    }

    void reloadConf(EngineConf engineConf) {
        globalSearchOptions = GlobalSearchOptionsImpl.newInstance(scrutariDB, engineConf);
        engine.getEngineStorage().cleanTransientCache(scrutariDB.getScrutariDBName());
    }

    static ScrutariSessionImpl newInstance(SctEngine engine, ScrutariDB scrutariDB, ScrutariDataURIChecker scrutariDataURIChecker, EngineConf engineConf, TimeLog timeLog) {
        timeLog.logAction("supermotcleProvider");
        SupermotcleProvider supermotcleProvider = SupermotcleProviderBuilder.build(scrutariDB);
        timeLog.logAction("globalSearchOptions");
        ScrutariSessionImpl scrutariSession = new ScrutariSessionImpl(engine, scrutariDB, scrutariDataURIChecker, supermotcleProvider);
        scrutariSession.reloadConf(engineConf);
        return scrutariSession;
    }

}
