/* SctServer - Copyright (c) 2017-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.enginemetadata;

import fr.exemole.sctserver.api.EngineMetadata;
import java.io.IOException;
import java.net.URL;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Phrase;


/**
 *
 * @author Vincent Calame
 */
public final class EngineMetadataJson_A {

    private EngineMetadataJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, String engineName, EngineMetadata engineMetadata, Lang lang, boolean withLabelMap) throws IOException {
        CommonJson.defaultLabel(jsonWriter, engineMetadata.getTitleLabels(), lang, "title", engineName);
        if (withLabelMap) {
            jsonWriter.key("titleLabelMap");
            CommonJson.object(jsonWriter, engineMetadata.getTitleLabels());
        }
        Phrase description = engineMetadata.getPhrases().getPhrase("description");
        if (description != null) {
            CommonJson.defaultLabel(jsonWriter, description, lang, "description", null);
            if (withLabelMap) {
                jsonWriter.key("descriptionLabelMap");
                CommonJson.object(jsonWriter, description);
            }
        }
        printUrl(jsonWriter, "icon", engineMetadata.getIcon());
        printUrl(jsonWriter, "website", engineMetadata.getWebsite());
        jsonWriter.key("attrMap");
        CommonJson.object(jsonWriter, engineMetadata.getAttributes());
    }

    private static void printUrl(JSONWriter jsonWriter, String key, URL url) throws IOException {
        if (url == null) {
            return;
        }
        jsonWriter.key(key);
        jsonWriter.value(url.toString());
    }

}
