/* SctServer - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.category;

import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;


/**
 *
 * @author Vincent Calame
 */
public final class CategoryJson_A {

    private CategoryJson_A() {

    }

    public static void object(JSONWriter jsonWriter, Category category, Lang lang, boolean withStats) throws IOException {
        CategoryDef categoryDef = category.getCategoryDef();
        jsonWriter.object();
        jsonWriter.key("rank");
        jsonWriter.value(categoryDef.getRank());
        jsonWriter.key("name");
        jsonWriter.value(categoryDef.getName());
        jsonWriter.key("title");
        jsonWriter.value(categoryDef.getTitle(lang));
        if (withStats) {
            jsonWriter.key("stats");
            jsonWriter.object();
            JsonUtils.addCountStats(jsonWriter, category.getCountStats());
            JsonUtils.addLangStats(jsonWriter, category.getFicheStats());
            jsonWriter.endObject();
        }
        List<Integer> corpusCodeList = category.getCorpusCodeList();
        int corpusCount = corpusCodeList.size();
        jsonWriter.key("codecorpusArray");
        jsonWriter.array();
        for (int i = 0; i < corpusCount; i++) {
            jsonWriter.value(corpusCodeList.get(i));
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
    }

}
