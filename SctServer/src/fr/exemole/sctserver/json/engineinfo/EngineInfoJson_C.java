/* SctServer - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.engineinfo;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.json.JsonUtils;
import fr.exemole.sctserver.json.attributedef.AttributeDefJson_B;
import fr.exemole.sctserver.json.basedata.BaseDataJson_B;
import fr.exemole.sctserver.json.category.CategoryJson_B;
import fr.exemole.sctserver.json.corpusdata.CorpusDataJson_B;
import fr.exemole.sctserver.json.enginemetadata.EngineMetadataJson_B;
import fr.exemole.sctserver.json.thesaurusdata.ThesaurusDataJson_B;
import fr.exemole.sctserver.tools.EngineUtils;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.stats.LangStat;
import net.scrutari.db.api.stats.ScrutariDBStats;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.db.api.stats.LangStats;


/**
 *
 * @author Vincent Calame
 */
public final class EngineInfoJson_C {

    private EngineInfoJson_C() {

    }

    public static void properties(JSONWriter jsonWriter, DataAccess dataAccess, ScrutariSession scrutariSession, Lang lang, InfoOptions infoOptions) throws IOException {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        SctEngine engine = scrutariSession.getEngine();
        GlobalSearchOptions globalSearchOptions = scrutariSession.getGlobalSearchOptions();
        MessageLocalisation messageLocalisation = EngineUtils.getMessageLocalisation(scrutariSession.getEngine(), lang);
        LangStats ficheEngineStats = scrutariDB.getStats().getFicheEngineLangStats();
        ScrutariDBStats stats;
        if (infoOptions.isWith(InfoOptions.STATS)) {
            stats = scrutariDB.getStats();
        } else {
            stats = null;
        }
        jsonWriter.key("engineInfo");
        jsonWriter.object();
        if (infoOptions.isWith(InfoOptions.BASE)) {
            jsonWriter.key("baseMap");
            jsonWriter.object();
            for (BaseData baseData : dataAccess.getBaseDataList()) {
                jsonWriter.key("code_" + baseData.getBaseCode());
                BaseDataJson_B.object(jsonWriter, baseData, lang, stats);
            }
            jsonWriter.endObject();
        }
        if (infoOptions.isWith(InfoOptions.CORPUS)) {
            jsonWriter.key("corpusMap");
            jsonWriter.object();
            for (CorpusData corpusData : dataAccess.getCorpusDataList()) {
                jsonWriter.key("code_" + corpusData.getCorpusCode());
                CorpusDataJson_B.object(jsonWriter, corpusData, lang, stats);
            }
            jsonWriter.endObject();
        }
        if (infoOptions.isWith(InfoOptions.THESAURUS)) {
            jsonWriter.key("thesaurusMap");
            jsonWriter.object();
            for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                jsonWriter.key("code_" + thesaurusData.getThesaurusCode());
                ThesaurusDataJson_B.object(jsonWriter, thesaurusData, lang, stats);
            }
            jsonWriter.endObject();
        }
        if (infoOptions.isWith(InfoOptions.CATEGORY)) {
            List<Category> categoryList = globalSearchOptions.getCategoryList();
            if (!categoryList.isEmpty()) {
                jsonWriter.key("categoryMap");
                jsonWriter.object();
                for (Category category : categoryList) {
                    if (!category.getCorpusCodeList().isEmpty()) {
                        jsonWriter.key(category.getCategoryDef().getName());
                        CategoryJson_B.object(jsonWriter, category, lang, infoOptions.isWith(InfoOptions.STATS));
                    }
                }
                jsonWriter.endObject();
            }
        }
        if (infoOptions.isWith(InfoOptions.ATTRIBUTES)) {
            AttributeDefJson_B.properties(jsonWriter, engine.getAttributeDefManager(), lang);
        }
        if (infoOptions.isWith(InfoOptions.METADATA)) {
            jsonWriter.key("metadata");
            jsonWriter.object();
            EngineMetadataJson_B.properties(jsonWriter, engine.getEngineName(), engine.getEngineMetadata(), lang, false);
            jsonWriter.endObject();
        }
        if (infoOptions.isWith(InfoOptions.LANG)) {
            jsonWriter.key("langMap");
            jsonWriter.object();
            for (LangStat langStat : ficheEngineStats) {
                String code = langStat.getLang().toString();
                jsonWriter.key(code);
                String value = messageLocalisation.toString(code);
                if (value != null) {
                    value = StringUtils.getFirstPart(value);
                } else {
                    value = code;
                }
                jsonWriter.value(value);
            }
            jsonWriter.endObject();
        }
        if (infoOptions.isWith(InfoOptions.STATS)) {
            jsonWriter.key("stats");
            jsonWriter.object();
            JsonUtils.addCountStats(jsonWriter, stats.getEngineCountStats());
            JsonUtils.addLangStats(jsonWriter, ficheEngineStats);
            jsonWriter.endObject();
        }
        jsonWriter.endObject();
    }

}
