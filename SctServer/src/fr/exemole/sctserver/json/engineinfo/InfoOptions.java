/* SctServer - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.engineinfo;

import fr.exemole.sctserver.request.Parameters;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class InfoOptions {

    public final static int BASE = 0;
    public final static int CORPUS = 1;
    public final static int THESAURUS = 2;
    public final static int CATEGORY = 3;
    public final static int ATTRIBUTES = 4;
    public final static int METADATA = 5;
    public final static int LANG = 6;
    public final static int STATS = 7;
    private final boolean[] withArray = new boolean[8];

    public InfoOptions() {

    }

    public boolean isWith(int info) {
        try {
            return withArray[info];
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public void setWith(int info, boolean value) {
        if ((info < 0) || (info >= withArray.length)) {
            return;
        }
        withArray[info] = value;
    }

    public void setAll(boolean value) {
        int length = withArray.length;
        for (int i = 0; i < length; i++) {
            withArray[i] = value;
        }
    }

    public void parseOptions(String s) {
        String[] tokens = StringUtils.getTechnicalTokens(s, true);
        for (String token : tokens) {
            String constantToken = token.toLowerCase();
            switch (constantToken) {
                case "base":
                    setWith(BASE, true);
                    break;
                case "corpus":
                    setWith(CORPUS, true);
                    break;
                case "thesaurus":
                    setWith(THESAURUS, true);
                    break;
                case "category":
                    setWith(CATEGORY, true);
                    break;
                case "attributes":
                    setWith(ATTRIBUTES, true);
                    break;
                case "metadata":
                    setWith(METADATA, true);
                    break;
                case "lang":
                    setWith(LANG, true);
                    break;
                case "stats":
                    setWith(STATS, true);
                    break;
                case Parameters.ALL:
                    setAll(true);
                    break;
            }
        }

    }

}
