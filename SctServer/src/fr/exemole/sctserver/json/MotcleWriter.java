/* SctServer- Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.motcledata.MotcleDataJson_A;
import fr.exemole.sctserver.json.motcledata.MotcleDataJson_B;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.MotcleData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public abstract class MotcleWriter {

    protected final JSONWriter jsonWriter;
    protected final DataAccess dataAccess;
    protected final FieldVariant fieldVariant;
    protected final Lang lang;

    public MotcleWriter(JSONWriter jsonWriter, DataAccess dataAccess, FieldVariant fieldVariant, Lang lang) {
        this.jsonWriter = jsonWriter;
        this.dataAccess = dataAccess;
        this.fieldVariant = fieldVariant;
        this.lang = lang;
    }

    public MotcleData object(Integer code) throws IOException {
        return object(code, null);
    }

    public MotcleData object(MotcleSearchResultInfo motcleSearchResultInfo) throws IOException {
        return object(motcleSearchResultInfo.getCode(), motcleSearchResultInfo);
    }

    private MotcleData object(Integer code, MotcleSearchResultInfo motcleSearchResultInfo) throws IOException {
        MotcleData motcleData = dataAccess.getMotcleData(code);
        MotcleInfo motcleInfo = (MotcleInfo) dataAccess.getMotcleInfo(code);
        jsonWriter.object();
        properties(motcleData, motcleInfo, motcleSearchResultInfo);
        jsonWriter.endObject();
        return motcleData;
    }

    public abstract void properties(MotcleData motcleData, MotcleInfo motcleInfo, MotcleSearchResultInfo motcleSearchResultInfo) throws IOException;

    public static MotcleWriter getAVersion(JSONWriter jsonWriter, DataAccess dataAccess, FieldVariant fieldVariant, Lang lang) {
        return new AVersion(jsonWriter, dataAccess, fieldVariant, lang);
    }

    public static MotcleWriter getBVersion(JSONWriter jsonWriter, DataAccess dataAccess, FieldVariant fieldVariant, Lang lang) {
        return new BVersion(jsonWriter, dataAccess, fieldVariant, lang);
    }


    private static class AVersion extends MotcleWriter {

        private AVersion(JSONWriter jsonWriter, DataAccess dataAccess, FieldVariant fieldVariant, Lang lang) {
            super(jsonWriter, dataAccess, fieldVariant, lang);
        }

        @Override
        public void properties(MotcleData motcleData, MotcleInfo motcleInfo, MotcleSearchResultInfo motcleSearchResultInfo) throws IOException {
            MotcleDataJson_A.properties(jsonWriter, motcleData, fieldVariant, motcleSearchResultInfo, lang);
        }

    }


    private static class BVersion extends MotcleWriter {

        private BVersion(JSONWriter jsonWriter, DataAccess dataAccess, FieldVariant fieldVariant, Lang lang) {
            super(jsonWriter, dataAccess, fieldVariant, lang);
        }

        @Override
        public void properties(MotcleData motcleData, MotcleInfo motcleInfo, MotcleSearchResultInfo motcleSearchResultInfo) throws IOException {
            MotcleDataJson_B.properties(jsonWriter, motcleData, motcleInfo, fieldVariant, motcleSearchResultInfo, lang);
        }

    }


}
