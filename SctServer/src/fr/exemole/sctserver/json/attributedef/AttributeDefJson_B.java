/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.attributedef;

import fr.exemole.sctserver.api.AttributeDefManager;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.SctSpace;
import net.scrutari.db.api.ScrutariConstants;


/**
 *
 * @author Vincent Calame
 */
public final class AttributeDefJson_B {

    private AttributeDefJson_B() {

    }

    public static void properties(JSONWriter jsonWriter, AttributeDefManager attributeDefManager, Lang lang) throws IOException {
        jsonWriter.key("attributes");
        jsonWriter.object();
        addAttributeDefList(jsonWriter, attributeDefManager, ScrutariConstants.PRIMARY_GROUP, lang);
        addAttributeDefList(jsonWriter, attributeDefManager, ScrutariConstants.SECONDARY_GROUP, lang);
        addAttributeDefList(jsonWriter, attributeDefManager, ScrutariConstants.TECHNICAL_GROUP, lang);
        jsonWriter.endObject();
    }

    private static void addAttributeDefList(JSONWriter jsonWriter, AttributeDefManager attributeDefManager, String name, Lang lang) throws IOException {
        List<AttributeDef> attributeDefList = attributeDefManager.getAttributeDefList(name);
        jsonWriter.key(name);
        jsonWriter.array();
        for (AttributeDef attributeDef : attributeDefList) {
            jsonWriter.object();
            AttributeKey attributeKey = attributeDef.getAttributeKey();
            jsonWriter.key("name");
            jsonWriter.value(attributeKey.toString());
            jsonWriter.key("ns");
            jsonWriter.value(attributeKey.getNameSpace());
            jsonWriter.key("key");
            jsonWriter.value(attributeKey.getLocalKey());
            String type = SctSpace.getFormatType(attributeDef);
            jsonWriter.key("type");
            jsonWriter.value(type);
            jsonWriter.key("title");
            jsonWriter.value(attributeDef.getTitle(lang));
            jsonWriter.key("phraseMap");
            CommonJson.object(jsonWriter, attributeDef.getPhrases(), lang);
            jsonWriter.key("attrMap");
            CommonJson.object(jsonWriter, attributeDef.getAttributes());
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
    }

}
