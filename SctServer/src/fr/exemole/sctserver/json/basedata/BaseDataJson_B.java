/* SctServer - Copyright (c) 2017-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.basedata;

import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataConstants;
import net.scrutari.datauri.BaseURI;
import net.scrutari.db.api.stats.ScrutariDBStats;


/**
 *
 * @author Vincent Calame
 */
public class BaseDataJson_B {

    private BaseDataJson_B() {

    }

    public static void object(JSONWriter jsonWriter, BaseData baseData, Lang lang, ScrutariDBStats scrutariDBStats) throws IOException {
        int code = baseData.getBaseCode();
        jsonWriter.object();
        jsonWriter.key("codebase");
        jsonWriter.value(code);
        jsonWriter.key("sourcename");
        jsonWriter.value(baseData.getScrutariSourceName());
        BaseURI baseURI = baseData.getBaseURI();
        jsonWriter.key("authority");
        jsonWriter.value(baseURI.getAuthority());
        String baseName = baseURI.getBaseName();
        jsonWriter.key("basename");
        jsonWriter.value(baseName);
        jsonWriter.key("title");
        jsonWriter.value(LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_TITLE, lang, baseName));
        Lang checkedLang = baseData.checkAvailableLang(lang);
        String baseIcon = baseData.getBaseIcon(checkedLang);
        if ((baseIcon != null) && (baseIcon.length() > 0)) {
            jsonWriter.key("baseicon");
            jsonWriter.value(baseIcon);
        }
        List<Integer> corpusCodeList = baseData.getCorpusCodeList();
        int corpusLength = corpusCodeList.size();
        jsonWriter.key("codecorpusArray");
        jsonWriter.array();
        for (int i = 0; i < corpusLength; i++) {
            jsonWriter.value(corpusCodeList.get(i));
        }
        jsonWriter.endArray();
        List<Integer> thesaurusCodeList = baseData.getThesaurusCodeList();
        int thesaurusLength = thesaurusCodeList.size();
        jsonWriter.key("codethesaurusArray");
        jsonWriter.array();
        for (int i = 0; i < thesaurusLength; i++) {
            jsonWriter.value(thesaurusCodeList.get(i));
        }
        jsonWriter.endArray();
        jsonWriter.key("phraseMap");
        CommonJson.object(jsonWriter, baseData.getPhrases(), lang);
        jsonWriter.key("attrMap");
        CommonJson.object(jsonWriter, baseData.getAttributes());
        if (scrutariDBStats != null) {
            jsonWriter.key("stats");
            jsonWriter.object();
            JsonUtils.addCountStats(jsonWriter, scrutariDBStats.getCountStats(code));
            JsonUtils.addLangStats(jsonWriter, scrutariDBStats.getLangStats(code));
            jsonWriter.endObject();
        }
        jsonWriter.endObject();
    }

}
