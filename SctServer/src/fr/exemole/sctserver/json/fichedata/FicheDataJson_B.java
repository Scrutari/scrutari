/* SctServer - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichedata;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.FicheData;
import net.scrutari.data.SctSpace;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.ScrutariConstants;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FieldRank;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;


/**
 *
 * @author Vincent Calame
 */
public final class FicheDataJson_B {

    private FicheDataJson_B() {

    }

    public static void object(JSONWriter jsonWriter, FicheData ficheData, FieldVariant fieldVariant, BaseCheck baseCheck, FicheSearchResultInfo info, AttributeDefManager attributeDefManager, FieldRankManager fieldRankManager) throws IOException {
        jsonWriter.object();
        properties(jsonWriter, ficheData, fieldVariant, baseCheck, info, attributeDefManager, fieldRankManager);
        jsonWriter.endObject();
    }

    public static void properties(JSONWriter jsonWriter, FicheData ficheData, FieldVariant fieldVariant, BaseCheck baseCheck, FicheSearchResultInfo info, AttributeDefManager attributeDefManager, FieldRankManager fieldRankManager) throws IOException {
        FieldResolver resolver = new FieldResolver(jsonWriter, ficheData, fieldVariant, baseCheck);
        resolver.property(FieldVariant.FICHE_CODEFICHE, "codefiche")
                .property(FieldVariant.FICHE_CODECORPUS, "codecorpus")
                .property(FieldVariant.FICHE_CODEBASE, "codebase")
                .property(FieldVariant.FICHE_AUTHORITY, "authority")
                .property(FieldVariant.FICHE_BASENAME, "basename")
                .property(FieldVariant.FICHE_CORPUSNAME, "corpusname")
                .property(FieldVariant.FICHE_FICHEID, "ficheid")
                .property(FieldVariant.FICHE_TITRE, "titre")
                .property(FieldVariant.FICHE_SOUSTITRE, "soustitre")
                .property(FieldVariant.FICHE_HREF, "href")
                .property(FieldVariant.FICHE_LANG, "lang")
                .property(FieldVariant.FICHE_YEAR, "year")
                .property(FieldVariant.FICHE_DATE, "date")
                .property(FieldVariant.FICHE_DATEISO, "dateiso")
                .property(FieldVariant.FICHE_FICHEICON, "ficheicon")
                .property(FieldVariant.FICHE_ICON, "icon")
                .property(FieldVariant.FICHE_LATITUDE, "lat")
                .property(FieldVariant.FICHE_LONGITUDE, "lon");
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_COMPLEMENTS)) {
            int maxNumber = ficheData.getComplementMaxNumber();
            if (maxNumber > 0) {
                boolean inclus = false;
                for (int num = 1; num <= maxNumber; num++) {
                    String comp = ficheData.getComplementByNumber(num);
                    if (comp == null) {
                        continue;
                    }
                    if (!inclus) {
                        jsonWriter.key("complementArray");
                        jsonWriter.array();
                        inclus = true;
                    }
                    jsonWriter.object();
                    jsonWriter.key("number");
                    jsonWriter.value(num);
                    jsonWriter.key("value");
                    jsonWriter.value(comp);
                    jsonWriter.endObject();
                }
                if (inclus) {
                    jsonWriter.endArray();
                }
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_ATTRIBUTES)) {
            short attrType = fieldVariant.getFicheWithType(FieldVariant.FICHE_ATTRIBUTES);
            jsonWriter.key("attrMap");
            if (attrType == FieldVariant.ALL_TYPE) {
                CommonJson.object(jsonWriter, ficheData.getAttributes());
            } else {
                jsonWriter.object();
                Attributes attributes = ficheData.getAttributes();
                int attributeLength = attributes.size();
                for (int i = 0; i < attributeLength; i++) {
                    Attribute attribute = attributes.get(i);
                    AttributeKey attributeKey = attribute.getAttributeKey();
                    FieldRank fieldRank = fieldRankManager.getAttributeFieldRank(attributeKey);
                    int fieldType = fieldRank.getType();
                    boolean include = false;
                    if (fieldType == FieldRank.TECHNICAL_TYPE) {
                        include = true;
                    } else if ((attrType == FieldVariant.PRIMARY_TYPE) && (fieldType == FieldRank.PRIMARY_TYPE)) {
                        include = true;
                    }
                    if (include) {
                        jsonWriter.key(attributeKey.toString());
                        int valueLength = attribute.size();
                        jsonWriter.array();
                        for (int j = 0; j < valueLength; j++) {
                            jsonWriter.value(attribute.get(j));
                        }
                        jsonWriter.endArray();
                    }
                }
                jsonWriter.endObject();
            }
        }
        List<FieldVariant.Alias> aliasList = fieldVariant.getFicheAliasList();
        for (FieldVariant.Alias alias : aliasList) {
            if (alias.isArray()) {
                List<String> valueList = JsonUtils.getAliasValueList(alias, ficheData, baseCheck);
                if (!valueList.isEmpty()) {
                    jsonWriter.key(alias.getName());
                    jsonWriter.array();
                    for (String value : valueList) {
                        jsonWriter.value(value);
                    }
                    jsonWriter.endArray();
                }
            } else {
                String aliasValue = JsonUtils.getAliasValue(alias, ficheData, baseCheck);
                if (aliasValue.length() > 0) {
                    jsonWriter.key(alias.getName())
                            .value(aliasValue);
                }
            }
        }
        if (info == null) {
            return;
        }
        Map<AlineaRank, InAlineaOccurrences> map = new HashMap<AlineaRank, InAlineaOccurrences>();
        Set<FieldRank> atttributeRankSet = null;
        InAlineaOccurrences titreOccurrences = null;
        InAlineaOccurrences soustitreOccurrences = null;
        for (InAlineaOccurrences occurrences : info.getInAlineaOccurrencesList()) {
            AlineaRank alineaRank = occurrences.getAlineaRank();
            switch (alineaRank.getType()) {
                case FieldRank.TITRE_TYPE:
                    titreOccurrences = occurrences;
                    break;
                case FieldRank.SOUSTITRE_TYPE:
                    soustitreOccurrences = occurrences;
                    break;
                case FieldRank.PRIMARY_TYPE:
                case FieldRank.SECONDARY_TYPE:
                    FieldRank fieldRank = alineaRank.getFieldRank();
                    if (atttributeRankSet == null) {
                        atttributeRankSet = new HashSet<FieldRank>();
                    }
                    atttributeRankSet.add(fieldRank);
                    map.put(alineaRank, occurrences);
                    break;
                default:
                    map.put(alineaRank, occurrences);
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MTITRE)) {
            String titre = ficheData.getTitre();
            if (titre != null) {
                jsonWriter.key("mtitre");
                JsonUtils.writeMValue(jsonWriter, titre, JsonUtils.getList(titreOccurrences));
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MSOUSTITRE)) {
            String soustitre = ficheData.getSoustitre();
            if (soustitre != null) {
                jsonWriter.key("msoustitre");
                JsonUtils.writeMValue(jsonWriter, soustitre, JsonUtils.getList(soustitreOccurrences));
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MCOMPLEMENTS)) {
            boolean all = (fieldVariant.getFicheWithType(FieldVariant.FICHE_MCOMPLEMENTS) == FieldVariant.ALL_TYPE);
            int maxNumber = ficheData.getComplementMaxNumber();
            if (maxNumber > 0) {
                boolean started = false;
                for (int num = 1; num <= maxNumber; num++) {
                    String comp = ficheData.getComplementByNumber(num);
                    if (comp == null) {
                        continue;
                    }
                    List<SearchTokenOccurrence> searchTokenOccurrenceList = JsonUtils.getList(map, fieldRankManager.getComplementAlineaRank(num));
                    if ((searchTokenOccurrenceList == null) && (!all)) {
                        continue;
                    }
                    if (!started) {
                        jsonWriter.key("mcomplementArray");
                        jsonWriter.array();
                        started = true;
                    }
                    jsonWriter.object();
                    jsonWriter.key("number");
                    jsonWriter.value(num);
                    jsonWriter.key("mvalue");
                    JsonUtils.writeMValue(jsonWriter, comp, searchTokenOccurrenceList);
                    jsonWriter.endObject();
                }
                if (started) {
                    jsonWriter.endArray();
                }
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MATTRIBUTES)) {
            short type = fieldVariant.getFicheWithType(FieldVariant.FICHE_MATTRIBUTES);
            boolean onlyConcerned = (type == FieldVariant.CONCERNED_TYPE);
            boolean onlyPrimary = (type == FieldVariant.PRIMARY_TYPE);
            Attributes attributes = ficheData.getAttributes();
            int attributeLength = attributes.size();
            boolean started = false;
            for (int i = 0; i < attributeLength; i++) {
                Attribute attribute = attributes.get(i);
                AttributeKey attributeKey = attribute.getAttributeKey();
                FieldRank attributeRank = fieldRankManager.getAttributeFieldRank(attributeKey);
                if (!attributeRank.isTextField()) {
                    continue;
                }
                AttributeDef attributeDef = attributeDefManager.getAttributeDef(attributeKey);
                boolean isNotConcerned = ((atttributeRankSet == null) || (!atttributeRankSet.contains(attributeRank)));
                boolean isBlock = SctSpace.isBlock(attributeDef);
                if ((isBlock) && (isNotConcerned)) {
                    continue;
                }
                boolean stop = false;
                if (onlyConcerned) {
                    if (isNotConcerned) {
                        stop = true;
                    }
                } else if (onlyPrimary) {
                    if (!SctSpace.ownsTo(attributeDef, ScrutariConstants.PRIMARY_GROUP)) {
                        if (isNotConcerned) {
                            stop = true;
                        }
                    }
                }
                if (stop) {
                    continue;
                }
                if (!started) {
                    jsonWriter.key("mattrMap");
                    jsonWriter.object();
                    started = true;
                }
                jsonWriter.key(attributeKey.toString());
                int valueLength = attribute.size();
                jsonWriter.array();
                for (int j = 0; j < valueLength; j++) {
                    String value = attribute.get(j);
                    AlineaRank alineaRank = AlineaRank.build(attributeRank, j + 1);
                    List<SearchTokenOccurrence> searchTokenOccurrenceList = JsonUtils.getList(map, alineaRank);
                    if ((searchTokenOccurrenceList != null) || (!isBlock)) {
                        JsonUtils.writeMValue(jsonWriter, value, searchTokenOccurrenceList);
                    }
                }
                jsonWriter.endArray();
            }
            if (started) {
                jsonWriter.endObject();
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_SCORE)) {
            jsonWriter.key("score");
            jsonWriter.array();
            for (int score : info.getScoreArray()) {
                jsonWriter.value(score);
            }
            jsonWriter.endArray();
        }
    }


    private static class FieldResolver {

        private final FieldVariant fieldVariant;
        private final FicheData ficheData;
        private final BaseCheck baseCheck;
        private final JSONWriter jw;

        private FieldResolver(JSONWriter jw, FicheData ficheData, FieldVariant fieldVariant, BaseCheck baseCheck) {
            this.fieldVariant = fieldVariant;
            this.ficheData = ficheData;
            this.baseCheck = baseCheck;
            this.jw = jw;
        }

        private FieldResolver property(int field, String propertyKey) throws IOException {
            if (fieldVariant.isFicheWith(field)) {
                switch (field) {
                    case FieldVariant.FICHE_CODEFICHE:
                        jw.key(propertyKey)
                                .value(ficheData.getFicheCode());
                        return this;
                    case FieldVariant.FICHE_CODECORPUS:
                        jw.key(propertyKey)
                                .value(ficheData.getCorpusCode());
                        return this;
                    case FieldVariant.FICHE_CODEBASE:
                        jw.key(propertyKey)
                                .value(ficheData.getBaseCode());
                        return this;
                    case FieldVariant.FICHE_YEAR:
                        FuzzyDate date = ficheData.getDate();
                        if (date != null) {
                            jw.key(propertyKey)
                                    .value(date.getYear());
                        }
                        return this;
                    default:
                        String value = JsonUtils.getCoreValue(ficheData, field, baseCheck);
                        if (value != null) {
                            jw.key(propertyKey)
                                    .value(value);
                        }
                        return this;
                }
            } else {
                return this;
            }
        }

    }

}
