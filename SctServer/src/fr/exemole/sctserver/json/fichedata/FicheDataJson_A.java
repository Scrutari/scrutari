/* SctServer - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichedata;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.SctSpace;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.ScrutariConstants;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FieldRank;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;


/**
 *
 * @author Vincent Calame
 */
public final class FicheDataJson_A {


    private FicheDataJson_A() {

    }

    public static void object(JSONWriter jsonWriter, FicheData ficheData, FieldVariant fieldVariant, BaseCheck baseCheck, FicheSearchResultInfo info, AttributeDefManager attributeDefManager, FieldRankManager fieldRankManager) throws IOException {
        jsonWriter.object();
        properties(jsonWriter, ficheData, fieldVariant, baseCheck, info, attributeDefManager, fieldRankManager);
        jsonWriter.endObject();
    }

    public static void properties(JSONWriter jsonWriter, Integer ficheCode, CorpusData corpusData, FieldVariant fieldVariant) throws IOException {
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CODEFICHE)) {
            jsonWriter.key("codefiche");
            jsonWriter.value(ficheCode);
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CODECORPUS)) {
            jsonWriter.key("codecorpus");
            jsonWriter.value(corpusData.getCorpusCode());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CODEBASE)) {
            jsonWriter.key("codebase");
            jsonWriter.value(corpusData.getBaseCode());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_BASENAME)) {
            BaseURI baseURI = corpusData.getCorpusURI().getBaseURI();
            jsonWriter.key("authority");
            jsonWriter.value(baseURI.getAuthority());
            jsonWriter.key("basename");
            jsonWriter.value(baseURI.getBaseName());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CORPUSNAME)) {
            jsonWriter.key("corpusname");
            jsonWriter.value(corpusData.getCorpusURI().getCorpusName());
        }
    }

    public static void properties(JSONWriter jsonWriter, FicheData ficheData, FieldVariant fieldVariant, BaseCheck baseCheck, FicheSearchResultInfo info, AttributeDefManager attributeDefManager, FieldRankManager fieldRankManager) throws IOException {
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CODEFICHE)) {
            jsonWriter.key("codefiche");
            jsonWriter.value(ficheData.getFicheCode());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CODECORPUS)) {
            jsonWriter.key("codecorpus");
            jsonWriter.value(ficheData.getCorpusCode());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CODEBASE)) {
            jsonWriter.key("codebase");
            jsonWriter.value(ficheData.getBaseCode());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_BASENAME)) {
            BaseURI baseURI = ficheData.getFicheURI().getCorpusURI().getBaseURI();
            jsonWriter.key("authority");
            jsonWriter.value(baseURI.getAuthority());
            jsonWriter.key("basename");
            jsonWriter.value(baseURI.getBaseName());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_CORPUSNAME)) {
            FicheURI ficheURI = ficheData.getFicheURI();
            jsonWriter.key("corpusname");
            jsonWriter.value(ficheURI.getCorpusURI().getCorpusName());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_FICHEID)) {
            FicheURI ficheURI = ficheData.getFicheURI();
            jsonWriter.key("ficheid");
            jsonWriter.value(ficheURI.getFicheId());
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_TITRE)) {
            String titre = ficheData.getTitre();
            if (titre != null) {
                jsonWriter.key("titre");
                jsonWriter.value(ficheData.getTitre());
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_SOUSTITRE)) {
            String soustitre = ficheData.getSoustitre();
            if (soustitre != null) {
                jsonWriter.key("soustitre");
                jsonWriter.value(soustitre);
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_HREF)) {
            String href = ficheData.getHref(baseCheck.getCheckedLang());
            if (href != null) {
                jsonWriter.key("href");
                jsonWriter.value(href);
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_LANG)) {
            Lang lang = ficheData.getLang();
            if (!lang.equals(FicheData.UNDETERMINED_LANG)) {
                jsonWriter.key("lang");
                jsonWriter.value(lang.toString());
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_YEAR)) {
            FuzzyDate date = ficheData.getDate();
            if (date != null) {
                jsonWriter.key("annee");
                jsonWriter.value(date.getYear());
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_DATE)) {
            FuzzyDate date = ficheData.getDate();
            if (date != null) {
                jsonWriter.key("date");
                jsonWriter.value(date.toString());
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_DATEISO)) {
            FuzzyDate date = ficheData.getDate();
            if (date != null) {
                jsonWriter.key("dateiso");
                jsonWriter.value(date.toISOString());
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_FICHEICON)) {
            String ficheIcon = ficheData.getFicheIcon();
            if (ficheIcon != null) {
                jsonWriter.key("ficheicon");
                jsonWriter.value(ficheIcon);
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_ICON)) {
            String icon = ficheData.getFicheIcon();
            if (icon == null) {
                icon = baseCheck.getBaseIcon();
            }
            if (icon != null) {
                jsonWriter.key("icon");
                jsonWriter.value(icon);
            }
        }
        if (ficheData.isWithGeo()) {
            if (fieldVariant.isFicheWith(FieldVariant.FICHE_LATITUDE)) {
                jsonWriter.key("lat");
                jsonWriter.value(ficheData.getLongitude().toString());
            }
            if (fieldVariant.isFicheWith(FieldVariant.FICHE_LONGITUDE)) {
                jsonWriter.key("lon");
                jsonWriter.value(ficheData.getLongitude().toString());
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_COMPLEMENTS)) {
            int maxNumber = ficheData.getComplementMaxNumber();
            if (maxNumber > 0) {
                boolean inclus = false;
                for (int num = 1; num <= maxNumber; num++) {
                    String comp = ficheData.getComplementByNumber(num);
                    if (comp == null) {
                        continue;
                    }
                    if (!inclus) {
                        jsonWriter.key("complementArray");
                        jsonWriter.array();
                        inclus = true;
                    }
                    jsonWriter.object();
                    jsonWriter.key("num");
                    jsonWriter.value(num);
                    jsonWriter.key("comp");
                    jsonWriter.value(comp);
                    jsonWriter.endObject();
                }
                if (inclus) {
                    jsonWriter.endArray();
                }
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_ATTRIBUTES)) {
            short attrType = fieldVariant.getFicheWithType(FieldVariant.FICHE_ATTRIBUTES);
            jsonWriter.key("attrMap");
            if (attrType == FieldVariant.ALL_TYPE) {
                CommonJson.object(jsonWriter, ficheData.getAttributes());
            } else {
                jsonWriter.object();
                Attributes attributes = ficheData.getAttributes();
                int attributeLength = attributes.size();
                for (int i = 0; i < attributeLength; i++) {
                    Attribute attribute = attributes.get(i);
                    AttributeKey attributeKey = attribute.getAttributeKey();
                    FieldRank fieldRank = fieldRankManager.getAttributeFieldRank(attributeKey);
                    int fieldType = fieldRank.getType();
                    boolean include = false;
                    if (fieldType == FieldRank.TECHNICAL_TYPE) {
                        include = true;
                    } else if ((attrType == FieldVariant.PRIMARY_TYPE) && (fieldType == FieldRank.PRIMARY_TYPE)) {
                        include = true;
                    }
                    if (include) {
                        jsonWriter.key(attributeKey.toString());
                        int valueLength = attribute.size();
                        jsonWriter.array();
                        for (int j = 0; j < valueLength; j++) {
                            jsonWriter.value(attribute.get(j));
                        }
                        jsonWriter.endArray();
                    }
                }
                jsonWriter.endObject();
            }
        }
        List<FieldVariant.Alias> aliasList = fieldVariant.getFicheAliasList();
        for (FieldVariant.Alias alias : aliasList) {
            String aliasValue = JsonUtils.getAliasValue(alias, ficheData, baseCheck);
            if (aliasValue.length() > 0) {
                jsonWriter.key(alias.getName());
                jsonWriter.value(aliasValue);
            }
        }
        if (info == null) {
            return;
        }
        Map<AlineaRank, InAlineaOccurrences> map = new HashMap<AlineaRank, InAlineaOccurrences>();
        Set<FieldRank> atttributeRankSet = null;
        InAlineaOccurrences titreOccurrences = null;
        InAlineaOccurrences soustitreOccurrences = null;
        for (InAlineaOccurrences occurrences : info.getInAlineaOccurrencesList()) {
            AlineaRank alineaRank = occurrences.getAlineaRank();
            switch (alineaRank.getType()) {
                case FieldRank.TITRE_TYPE:
                    titreOccurrences = occurrences;
                    break;
                case FieldRank.SOUSTITRE_TYPE:
                    soustitreOccurrences = occurrences;
                    break;
                case FieldRank.PRIMARY_TYPE:
                case FieldRank.SECONDARY_TYPE:
                    FieldRank fieldRank = alineaRank.getFieldRank();
                    if (atttributeRankSet == null) {
                        atttributeRankSet = new HashSet<FieldRank>();
                    }
                    atttributeRankSet.add(fieldRank);
                    map.put(alineaRank, occurrences);
                    break;
                default:
                    map.put(alineaRank, occurrences);
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MTITRE)) {
            String titre = ficheData.getTitre();
            if (titre != null) {
                jsonWriter.key("mtitre");
                JsonUtils.writeMValue(jsonWriter, titre, JsonUtils.getList(titreOccurrences));
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MSOUSTITRE)) {
            String soustitre = ficheData.getSoustitre();
            if (soustitre != null) {
                jsonWriter.key("msoustitre");
                JsonUtils.writeMValue(jsonWriter, soustitre, JsonUtils.getList(soustitreOccurrences));
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MCOMPLEMENTS)) {
            boolean all = (fieldVariant.getFicheWithType(FieldVariant.FICHE_MCOMPLEMENTS) == FieldVariant.ALL_TYPE);
            int maxNumber = ficheData.getComplementMaxNumber();
            if (maxNumber > 0) {
                boolean started = false;
                for (int num = 1; num <= maxNumber; num++) {
                    String comp = ficheData.getComplementByNumber(num);
                    if (comp == null) {
                        continue;
                    }
                    List<SearchTokenOccurrence> searchTokenOccurrenceList = JsonUtils.getList(map, fieldRankManager.getComplementAlineaRank(num));
                    if ((searchTokenOccurrenceList == null) && (!all)) {
                        continue;
                    }
                    if (!started) {
                        jsonWriter.key("mcomplementArray");
                        jsonWriter.array();
                        started = true;
                    }
                    jsonWriter.object();
                    jsonWriter.key("num");
                    jsonWriter.value(num);
                    jsonWriter.key("mcomp");
                    JsonUtils.writeMValue(jsonWriter, comp, searchTokenOccurrenceList);
                    jsonWriter.endObject();
                }
                if (started) {
                    jsonWriter.endArray();
                }
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MATTRIBUTES)) {
            short type = fieldVariant.getFicheWithType(FieldVariant.FICHE_MATTRIBUTES);
            boolean onlyConcerned = (type == FieldVariant.CONCERNED_TYPE);
            boolean onlyPrimary = (type == FieldVariant.PRIMARY_TYPE);
            Attributes attributes = ficheData.getAttributes();
            int attributeLength = attributes.size();
            boolean started = false;
            for (int i = 0; i < attributeLength; i++) {
                Attribute attribute = attributes.get(i);
                AttributeKey attributeKey = attribute.getAttributeKey();
                FieldRank attributeRank = fieldRankManager.getAttributeFieldRank(attributeKey);
                if (!attributeRank.isTextField()) {
                    continue;
                }
                AttributeDef attributeDef = attributeDefManager.getAttributeDef(attributeKey);
                boolean isNotConcerned = ((atttributeRankSet == null) || (!atttributeRankSet.contains(attributeRank)));
                boolean isBlock = SctSpace.isBlock(attributeDef);
                if ((isBlock) && (isNotConcerned)) {
                    continue;
                }
                boolean stop = false;
                if (onlyConcerned) {
                    if (isNotConcerned) {
                        stop = true;
                    }
                } else if (onlyPrimary) {
                    if (!SctSpace.ownsTo(attributeDef, ScrutariConstants.PRIMARY_GROUP)) {
                        if (isNotConcerned) {
                            stop = true;
                        }
                    }
                }
                if (stop) {
                    continue;
                }
                if (!started) {
                    jsonWriter.key("mattrMap");
                    jsonWriter.object();
                    started = true;
                }
                jsonWriter.key(attributeKey.toString());
                int valueLength = attribute.size();
                jsonWriter.array();
                for (int j = 0; j < valueLength; j++) {
                    String value = attribute.get(j);
                    AlineaRank alineaRank = AlineaRank.build(attributeRank, j + 1);
                    List<SearchTokenOccurrence> searchTokenOccurrenceList = JsonUtils.getList(map, alineaRank);
                    if ((searchTokenOccurrenceList != null) || (!isBlock)) {
                        JsonUtils.writeMValue(jsonWriter, value, searchTokenOccurrenceList);
                    }
                }
                jsonWriter.endArray();
            }
            if (started) {
                jsonWriter.endObject();
            }
        }
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_SCORE)) {
            jsonWriter.key("score");
            jsonWriter.array();
            for (int score : info.getScoreArray()) {
                jsonWriter.value(score);
            }
            jsonWriter.endArray();
        }
    }

}
