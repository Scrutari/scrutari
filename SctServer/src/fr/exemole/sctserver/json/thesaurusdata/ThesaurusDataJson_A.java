/* SctServer - Copyright (c) 2015-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.thesaurusdata;

import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.DataConstants;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.db.api.stats.ScrutariDBStats;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusDataJson_A {

    private ThesaurusDataJson_A() {

    }

    public static void object(JSONWriter jsonWriter, ThesaurusData thesaurusData, Lang lang, ScrutariDBStats scrutariDBStats) throws IOException {
        Integer code = thesaurusData.getThesaurusCode();
        jsonWriter.object();
        jsonWriter.key("codethesaurus");
        jsonWriter.value(code);
        jsonWriter.key("codebase");
        jsonWriter.value(thesaurusData.getBaseCode());
        ThesaurusURI thesaurusURI = thesaurusData.getThesaurusURI();
        String thesaurusName = thesaurusURI.getThesaurusName();
        jsonWriter.key("thesaurusname");
        jsonWriter.value(thesaurusName);
        jsonWriter.key("intitules");
        jsonWriter.object();
        jsonWriter.key("thesaurus");
        jsonWriter.value(LabelUtils.seekLabelString(thesaurusData.getPhrases(), DataConstants.THESAURUS_TITLE, lang, thesaurusName));
        jsonWriter.endObject();
        if (scrutariDBStats != null) {
            jsonWriter.key("stats");
            jsonWriter.object();
            JsonUtils.addCountStats(jsonWriter, scrutariDBStats.getCountStats(code));
            jsonWriter.endObject();
        }
        jsonWriter.endObject();
    }

}
