/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;


/**
 *
 * @author Vincent Calame
 */
public final class ResultGroupJson_A {

    private ResultGroupJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, FicheSearchResultGroup ficheSearchResultGroup, GlobalSearchOptions globalSearchOptions, Lang lang) throws IOException {
        if (globalSearchOptions.isWithCategory()) {
            jsonWriter.key("corpusClasse");
            jsonWriter.object();
            int categoryRank = ficheSearchResultGroup.getCategoryRank();
            jsonWriter.key("rank");
            jsonWriter.value(categoryRank);
            Category category = globalSearchOptions.getCategoryByRank(categoryRank);
            if (category != null) {
                CategoryDef categoryDef = category.getCategoryDef();
                jsonWriter.key("name");
                jsonWriter.value(categoryDef.getName());
                jsonWriter.key("intitule");
                jsonWriter.value(categoryDef.getTitle(lang));
            } else {
                jsonWriter.key("name");
                jsonWriter.value("unknown_rank_" + categoryRank);
                jsonWriter.key("intitule");
                jsonWriter.value("## Unknown Rank : " + categoryRank + " ##");
            }
            jsonWriter.endObject();
        }
        jsonWriter.key("ficheCount");
        jsonWriter.value(ficheSearchResultGroup.getFicheSearchResultInfoList().size());
    }

}
