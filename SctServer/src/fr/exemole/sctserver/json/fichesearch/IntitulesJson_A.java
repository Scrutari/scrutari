/* SctServer - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.attributedef.AttributeDefJson_A;
import java.io.IOException;
import java.util.Set;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.db.api.DataAccess;


/**
 *
 * @author Vincent Calame
 */
public final class IntitulesJson_A {

    private IntitulesJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, FieldVariant fieldVariant, DataAccess dataAccess, Lang lang, Set<Integer> corpusCodeSet, AttributeDefManager attributeDefManager) throws IOException {
        jsonWriter.key("corpusIntituleArray");
        jsonWriter.array();
        for (Integer corpusCode : corpusCodeSet) {
            CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
            CorpusURI corpusURI = corpusData.getCorpusURI();
            String corpusName = corpusURI.getCorpusName();
            jsonWriter.object();
            jsonWriter.key("codecorpus");
            jsonWriter.value(corpusCode);
            jsonWriter.key("corpus");
            jsonWriter.value(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_TITLE, lang, corpusName));
            jsonWriter.key("fiche");
            jsonWriter.value(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_FICHE, lang, corpusName));
            int compMaxNumber = corpusData.getComplementMaxNumber();
            for (int num = 1; num <= compMaxNumber; num++) {
                jsonWriter.key("complement_" + num);
                jsonWriter.value(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_COMPLEMENTPREFIX + num, lang, "complement_" + num));
            }
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
        if (fieldVariant.isFicheWith(FieldVariant.FICHE_MATTRIBUTES)) {
            AttributeDefJson_A.properties(jsonWriter, attributeDefManager, lang);
        }
    }

}
