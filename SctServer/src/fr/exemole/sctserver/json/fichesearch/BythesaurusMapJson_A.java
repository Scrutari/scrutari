/* SctServer - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.json.JSONWriter;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public final class BythesaurusMapJson_A {

    private BythesaurusMapJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess) throws IOException {
        List<Integer> motcleCodes = ficheSearchResultInfo.getMotcleCodeList();
        if (motcleCodes.isEmpty()) {
            return;
        }
        Map<Integer, BythesaurusBuffer> bufferMap = new LinkedHashMap<Integer, BythesaurusBuffer>();
        for (Integer motcleCode : motcleCodes) {
            Integer thesaurusCode = dataAccess.getMotcleInfo(motcleCode).getThesaurusCode();
            BythesaurusBuffer buffer = bufferMap.get(thesaurusCode);
            if (buffer == null) {
                buffer = new BythesaurusBuffer(thesaurusCode);
                bufferMap.put(thesaurusCode, buffer);
            }
            buffer.add(motcleCode);
        }
        jsonWriter.key("bythesaurusMap");
        jsonWriter.object();
        for (BythesaurusBuffer buffer : bufferMap.values()) {
            buffer.properties(jsonWriter);
        }
        jsonWriter.endObject();
    }

    public static void properties(JSONWriter jsonWriter, Integer ficheCode, DataAccess dataAccess, @Nullable Set<Integer> motcleCodeSet) throws IOException {
        boolean done = false;
        FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            Integer thesaurusCode = thesaurusData.getThesaurusCode();
            List<Integer> indexationMotcleCodeList = ficheInfo.getIndexationCodeList(thesaurusCode);
            if (!indexationMotcleCodeList.isEmpty()) {
                if (!done) {
                    jsonWriter.key("bythesaurusMap");
                    jsonWriter.object();
                    done = true;
                }
                jsonWriter.key("code_" + thesaurusCode);
                jsonWriter.array();
                for (Integer motcleCode : indexationMotcleCodeList) {
                    jsonWriter.value(motcleCode);
                    if (motcleCodeSet != null) {
                        motcleCodeSet.add(motcleCode);
                    }
                }
                jsonWriter.endArray();
            }

        }
        if (done) {
            jsonWriter.endObject();
        }
    }


    private static class BythesaurusBuffer {

        private final Integer thesaurusCode;
        private final List<Integer> motcleList = new ArrayList<Integer>();

        private BythesaurusBuffer(Integer thesaurusCode) {
            this.thesaurusCode = thesaurusCode;
        }

        private void add(Integer motcleCode) {
            motcleList.add(motcleCode);
        }

        private void properties(JSONWriter jsonWriter) throws IOException {
            jsonWriter.key("code_" + thesaurusCode);
            jsonWriter.array();
            for (Integer motcleCode : motcleList) {
                jsonWriter.value(motcleCode);
            }
            jsonWriter.endArray();
        }

    }

}
