/* SctServer - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;


/**
 *
 * @author Vincent Calame
 */
public final class GroupTypeJson_B {

    private GroupTypeJson_B() {

    }

    public static void properties(JSONWriter jsonWriter, GlobalSearchOptions globalSearchOptions) throws IOException {
        jsonWriter.key("ficheGroupType");
        if (globalSearchOptions.isWithCategory()) {
            jsonWriter.value("category");
        } else {
            jsonWriter.value("none");
        }
    }

}
