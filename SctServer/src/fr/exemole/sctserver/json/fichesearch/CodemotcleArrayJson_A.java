/* SctServer - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.json.JSONWriter;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public final class CodemotcleArrayJson_A {

    private CodemotcleArrayJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, FicheSearchResultInfo ficheSearchResultInfo) throws IOException {
        List<Integer> motcleCodeList = ficheSearchResultInfo.getMotcleCodeList();
        int ficheMotcleLength = motcleCodeList.size();
        if (ficheMotcleLength > 0) {
            jsonWriter.key("codemotcleArray");
            jsonWriter.array();
            for (int k = 0; k < ficheMotcleLength; k++) {
                jsonWriter.value(motcleCodeList.get(k));
            }
            jsonWriter.endArray();
        }
    }

    public static void properties(JSONWriter jsonWriter, Integer ficheCode, DataAccess dataAccess, @Nullable Set<Integer> motcleCodeSet) throws IOException {
        boolean done = false;
        FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            List<Integer> indexationMotcleCodeList = ficheInfo.getIndexationCodeList(thesaurusData.getThesaurusCode());
            for (Integer motcleCode : indexationMotcleCodeList) {
                if (!done) {
                    jsonWriter.key("codemotcleArray");
                    jsonWriter.array();
                    done = true;
                }
                jsonWriter.value(motcleCode);
                if (motcleCodeSet != null) {
                    motcleCodeSet.add(motcleCode);
                }
            }
        }
        if (done) {
            jsonWriter.endArray();
        }
    }

}
