/* SctServer - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class InsertOptions {

    public final static int SEARCHMETA = 0;
    public final static int MOTCLEARRAY = 1;
    public final static int ENGINEINFO = 2;
    private final boolean[] withArray = new boolean[3];

    public InsertOptions(boolean searchMetaDefault, boolean motclearrayDefault) {
        withArray[SEARCHMETA] = searchMetaDefault;
        withArray[MOTCLEARRAY] = motclearrayDefault;
    }

    public boolean isWith(int insert) {
        try {
            return withArray[insert];
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    public InsertOptions setWith(int insert, boolean value) {
        if ((insert < 0) || (insert >= withArray.length)) {
            return this;
        }
        withArray[insert] = value;
        return this;
    }

    public InsertOptions parseOptions(String options) {
        String[] tokens = StringUtils.getTechnicalTokens(options, true);
        for (String token : tokens) {
            String constantToken = token.toLowerCase();
            switch (constantToken) {
                case "-searchmeta":
                    setWith(SEARCHMETA, false);
                    break;
                case "-motclearray":
                    setWith(MOTCLEARRAY, false);
                    break;
                case "motclearray":
                    setWith(MOTCLEARRAY, true);
                    break;
                case "engineinfo":
                    setWith(ENGINEINFO, true);
                    break;
            }
        }
        return this;
    }

    public static InsertOptions init(boolean searchMetaDefault, boolean motclearrayDefault) {
        return new InsertOptions(searchMetaDefault, motclearrayDefault);
    }


}
