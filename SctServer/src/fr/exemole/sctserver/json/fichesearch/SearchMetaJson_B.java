/* SctServer - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import fr.exemole.sctserver.tools.BuildingUtils;
import java.io.IOException;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchSource;


/**
 *
 * @author Vincent Calame
 */
public final class SearchMetaJson_B {

    private SearchMetaJson_B() {

    }

    public static void properties(JSONWriter jsonWriter, DataAccess dataAccess, FicheSearchResult searchResult, Lang lang) throws IOException {
        FicheSearchSource ficheSearchSource = searchResult.getFicheSearchSource();
        CanonicalQ canonicalQ = ficheSearchSource.getCanonicalQ();
        jsonWriter.key("q");
        jsonWriter.value(canonicalQ.toString());
        QId qId = ficheSearchSource.getQId();
        if (qId != null) {
            jsonWriter.key("qId");
            jsonWriter.value(qId.toString());
        }
        jsonWriter.key("lang");
        jsonWriter.value(lang.toString());
        jsonWriter.key("ficheCount");
        jsonWriter.value(searchResult.getFicheCount());
        jsonWriter.key("ficheMaximum");
        jsonWriter.value(searchResult.getFicheMaximum());
        jsonWriter.key("motcleCount");
        jsonWriter.value(searchResult.getMotcleSearchResultInfoList().size());
        SearchOptionsDef searchOptionsDef = ficheSearchSource.getSearchOptionsDef();
        if (!searchOptionsDef.isEmpty()) {
            properties(jsonWriter, dataAccess, searchOptionsDef, lang);
        }
    }

    private static void properties(JSONWriter jsonWriter, DataAccess dataAccess, SearchOptionsDef searchOptionsDef, Lang lang) throws IOException {
        jsonWriter.key("options");
        jsonWriter.object();
        Langs filterLangs = searchOptionsDef.getFilterLangs();
        if (!filterLangs.isEmpty()) {
            jsonWriter.key("langlist");
            jsonWriter.object();
            jsonWriter.key("array");
            jsonWriter.array();
            for (Lang filterLang : filterLangs) {
                jsonWriter.value(filterLang.toString());
            }
            jsonWriter.endArray();
            jsonWriter.endObject();
        }
        reductionProperties(jsonWriter, dataAccess, searchOptionsDef, ScrutariDataURI.BASEURI_TYPE, "baselist");
        reductionProperties(jsonWriter, dataAccess, searchOptionsDef, ScrutariDataURI.CORPUSURI_TYPE, "corpuslist");
        reductionProperties(jsonWriter, dataAccess, searchOptionsDef, ScrutariDataURI.THESAURUSURI_TYPE, "thesauruslist");
        categoryProperties(jsonWriter, searchOptionsDef);
        jsonWriter.endObject();
    }

    private static void reductionProperties(JSONWriter jsonWriter, DataAccess dataAccess, SearchOptionsDef searchOptionsDef, short type, String key) throws IOException {
        ListReduction listReduction = searchOptionsDef.getListReduction(type);
        if (listReduction != null) {
            jsonWriter.key(key);
            jsonWriter.object();
            jsonWriter.key("exclude");
            jsonWriter.value(listReduction.isExclude());
            jsonWriter.key("array");
            jsonWriter.array();
            for (ScrutariDataURI uri : listReduction.getScrutariDataURIList()) {
                jsonWriter.value(dataAccess.getCode(uri));
            }
            jsonWriter.endArray();
            jsonWriter.endObject();
        }
    }

    private static void categoryProperties(JSONWriter jsonWriter, SearchOptionsDef searchOptionsDef) throws IOException {
        Attribute listAttribute = searchOptionsDef.getAttributes().getAttribute(BuildingUtils.CATEGORY_LIST_KEY);
        if (listAttribute == null) {
            return;
        }
        boolean exclude = false;
        Attribute modeAttribute = searchOptionsDef.getAttributes().getAttribute(BuildingUtils.CATEGORY_MODE_KEY);
        if (modeAttribute != null) {
            exclude = modeAttribute.getFirstValue().equals(BuildingUtils.EXCLUDE_MODE.toString());
        }
        jsonWriter.key("categorylist");
        jsonWriter.object();
        jsonWriter.key("exclude");
        jsonWriter.value(exclude);
        jsonWriter.key("array");
        jsonWriter.array();
        int categoryLength = listAttribute.size();
        for (int i = 0; i < categoryLength; i++) {
            jsonWriter.value(listAttribute.get(i));
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
    }

}
