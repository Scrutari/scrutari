/* SctServer - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.fichesearch;

import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchSource;


/**
 *
 * @author Vincent Calame
 */
public final class SearchMetaJson_A {

    private SearchMetaJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, FicheSearchResult searchResult, Lang lang) throws IOException {
        FicheSearchSource ficheSearchSource = searchResult.getFicheSearchSource();
        CanonicalQ canonicalQ = ficheSearchSource.getCanonicalQ();
        jsonWriter.key("q");
        jsonWriter.value(canonicalQ.toString());
        QId qId = ficheSearchSource.getQId();
        if (qId != null) {
            jsonWriter.key("qId");
            jsonWriter.value(qId.toString());
        }
        jsonWriter.key("qSource");
        jsonWriter.value(canonicalQ.toString());
        jsonWriter.key("lang");
        jsonWriter.value(lang.toString());
        jsonWriter.key("ficheCount");
        jsonWriter.value(searchResult.getFicheCount());
        jsonWriter.key("ficheMaximum");
        jsonWriter.value(searchResult.getFicheMaximum());
        jsonWriter.key("motcleCount");
        jsonWriter.value(searchResult.getMotcleSearchResultInfoList().size());
    }

}
