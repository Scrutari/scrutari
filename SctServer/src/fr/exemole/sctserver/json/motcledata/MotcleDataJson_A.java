/* SctServer - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.motcledata;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.Set;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.scrutari.data.MotcleData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public final class MotcleDataJson_A {

    private MotcleDataJson_A() {

    }

    public static void properties(JSONWriter jsonWriter, MotcleData motcleData, FieldVariant fieldVariant, MotcleSearchResultInfo motcleSearchResultInfo, Lang lang) throws IOException {
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODEMOTCLE)) {
            jsonWriter.key("codemotcle");
            jsonWriter.value(motcleData.getMotcleCode());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODETHESAURUS)) {
            jsonWriter.key("codethesaurus");
            jsonWriter.value(motcleData.getThesaurusCode());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODEBASE)) {
            jsonWriter.key("codebase");
            jsonWriter.value(motcleData.getBaseCode());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_BASENAME)) {
            BaseURI baseURI = motcleData.getMotcleURI().getThesaurusURI().getBaseURI();
            jsonWriter.key("authority");
            jsonWriter.value(baseURI.getAuthority());
            jsonWriter.key("basename");
            jsonWriter.value(baseURI.getBaseName());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_THESAURUSNAME)) {
            MotcleURI motcleURI = motcleData.getMotcleURI();
            jsonWriter.key("thesaurusname");
            jsonWriter.value(motcleURI.getThesaurusURI().getThesaurusName());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_MOTCLEID)) {
            MotcleURI motcleURI = motcleData.getMotcleURI();
            jsonWriter.key("motcleid");
            jsonWriter.value(motcleURI.getMotcleId());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_LABELS)) {
            Labels labels = motcleData.getLabels();
            int labelLength = labels.size();
            if (labelLength > 0) {
                jsonWriter.key("libelleArray");
                jsonWriter.array();
                for (int i = 0; i < labelLength; i++) {
                    jsonWriter.object();
                    Label label = labels.get(i);
                    jsonWriter.key("lang");
                    jsonWriter.value(label.getLang().toString());
                    jsonWriter.key("lib");
                    jsonWriter.value(label.getLabelString());
                    jsonWriter.endObject();
                }
                jsonWriter.endArray();
            }
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_MLABELS)) {
            if (motcleSearchResultInfo == null) {
                addDefaultMLibellesArray(jsonWriter, motcleData, lang, (fieldVariant.getMotcleWithType(FieldVariant.MOTCLE_MLABELS) == FieldVariant.ALL_TYPE));
            } else if (fieldVariant.getMotcleWithType(FieldVariant.MOTCLE_MLABELS) == FieldVariant.ALL_TYPE) {
                addDefaultMLibelles(jsonWriter, motcleData, motcleSearchResultInfo, true);
            } else {
                addMLibelles(jsonWriter, motcleData, motcleSearchResultInfo, lang);
            }
        }
    }

    private static void addDefaultMLibellesArray(JSONWriter jsonWriter, MotcleData motcleData, Lang lang, boolean all) throws IOException {
        Labels labels = motcleData.getLabels();
        int labelLength = labels.size();
        if (labelLength == 0) {
            return;
        }
        if (!all) {
            Label label = labels.getLabel(lang);
            if (label != null) {
                jsonWriter.key("mlibelleArray");
                jsonWriter.array();
                jsonWriter.object();
                jsonWriter.key("lang");
                jsonWriter.value(lang.toString());
                jsonWriter.key("mlib");
                JsonUtils.writeMValue(jsonWriter, label.getLabelString(), null);
                jsonWriter.endObject();
                jsonWriter.endArray();
                return;
            }
        }
        jsonWriter.key("mlibelleArray");
        jsonWriter.array();
        for (int i = 0; i < labelLength; i++) {
            Label label = labels.get(i);
            Lang labelLang = label.getLang();
            jsonWriter.object();
            jsonWriter.key("lang");
            jsonWriter.value(labelLang.toString());
            jsonWriter.key("mlib");
            JsonUtils.writeMValue(jsonWriter, label.getLabelString(), null);
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
    }

    private static void addDefaultMLibelles(JSONWriter jsonWriter, MotcleData motcleData, MotcleSearchResultInfo motcleSearchResultInfo, boolean all) throws IOException {
        Labels labels = motcleData.getLabels();
        int labelLength = labels.size();
        boolean inclus = false;
        for (int i = 0; i < labelLength; i++) {
            Label label = labels.get(i);
            Lang labelLang = label.getLang();
            InLangOccurrences occurrences = motcleSearchResultInfo.getInLangOccurrencesByLang(labelLang);
            if ((!all) && (occurrences == null)) {
                continue;
            }
            if (!inclus) {
                jsonWriter.key("mlibelleArray");
                jsonWriter.array();
                inclus = true;
            }
            jsonWriter.object();
            jsonWriter.key("lang");
            jsonWriter.value(labelLang.toString());
            jsonWriter.key("mlib");
            JsonUtils.writeMValue(jsonWriter, label.getLabelString(), occurrences.getSearchTokenOccurrenceList());
            jsonWriter.endObject();
        }
        if (inclus) {
            jsonWriter.endArray();
        }
    }

    private static void addMLibelles(JSONWriter jsonWriter, MotcleData motcleData, MotcleSearchResultInfo motcleSearchResultInfo, Lang lang) throws IOException {
        InLangOccurrences uiOccurrences = motcleSearchResultInfo.getInLangOccurrencesByLang(lang);
        if (uiOccurrences == null) {
            addDefaultMLibelles(jsonWriter, motcleData, motcleSearchResultInfo, false);
            return;
        }
        Labels labels = motcleData.getLabels();
        jsonWriter.key("mlibelleArray");
        jsonWriter.array();
        Label uiLabel = labels.getLabel(lang);
        jsonWriter.object();
        jsonWriter.key("lang");
        jsonWriter.value(lang.toString());
        jsonWriter.key("mlib");
        JsonUtils.writeMValue(jsonWriter, uiLabel.getLabelString(), uiOccurrences.getSearchTokenOccurrenceList());
        jsonWriter.endObject();
        int labelCount = labels.size();
        if (labelCount > 1) {
            Set<Integer> operandeSet = JsonUtils.getOperandSet(uiOccurrences);
            for (int i = 0; i < labelCount; i++) {
                Label label = labels.get(i);
                Lang labelLang = label.getLang();
                if (labelLang.equals(lang)) {
                    continue;
                }
                InLangOccurrences occurrences = motcleSearchResultInfo.getInLangOccurrencesByLang(labelLang);
                if (occurrences == null) {
                    continue;
                }
                if (JsonUtils.isCovered(occurrences, operandeSet)) {
                    continue;
                }
                jsonWriter.object();
                jsonWriter.key("lang");
                jsonWriter.value(labelLang.toString());
                jsonWriter.key("mlib");
                JsonUtils.writeMValue(jsonWriter, label.getLabelString(), occurrences.getSearchTokenOccurrenceList());
                jsonWriter.endObject();
            }
        }
        jsonWriter.endArray();
    }

}
