/* SctServer - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.motcledata;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.scrutari.data.MotcleData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;


/**
 *
 * @author Vincent Calame
 */
public class MotcleDataJson_B {

    private MotcleDataJson_B() {

    }

    public static void properties(JSONWriter jsonWriter, MotcleData motcleData, MotcleInfo motcleInfo, FieldVariant fieldVariant, MotcleSearchResultInfo motcleSearchResultInfo, Lang lang) throws IOException {
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODEMOTCLE)) {
            jsonWriter.key("codemotcle");
            jsonWriter.value(motcleData.getMotcleCode());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODETHESAURUS)) {
            jsonWriter.key("codethesaurus");
            jsonWriter.value(motcleData.getThesaurusCode());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_CODEBASE)) {
            jsonWriter.key("codebase");
            jsonWriter.value(motcleData.getBaseCode());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_BASENAME)) {
            BaseURI baseURI = motcleData.getMotcleURI().getThesaurusURI().getBaseURI();
            jsonWriter.key("authority");
            jsonWriter.value(baseURI.getAuthority());
            jsonWriter.key("basename");
            jsonWriter.value(baseURI.getBaseName());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_THESAURUSNAME)) {
            MotcleURI motcleURI = motcleData.getMotcleURI();
            jsonWriter.key("thesaurusname");
            jsonWriter.value(motcleURI.getThesaurusURI().getThesaurusName());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_MOTCLEID)) {
            MotcleURI motcleURI = motcleData.getMotcleURI();
            jsonWriter.key("motcleid");
            jsonWriter.value(motcleURI.getMotcleId());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_LABELS)) {
            jsonWriter.key("labelMap");
            CommonJson.object(jsonWriter, motcleData.getLabels());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_MLABELS)) {
            boolean includeAll = (fieldVariant.getMotcleWithType(FieldVariant.MOTCLE_MLABELS) == FieldVariant.ALL_TYPE);
            if (motcleSearchResultInfo == null) {
                addDefaultMLabelArray(jsonWriter, motcleData, lang, includeAll);
            } else {
                if (includeAll) {
                    addDefaultMLabelArray(jsonWriter, motcleData, motcleSearchResultInfo, true);
                } else {
                    addMLabelArray(jsonWriter, motcleData, motcleSearchResultInfo, lang);
                }
            }
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_SCORE)) {
            jsonWriter.key("score");
            if (motcleSearchResultInfo != null) {
                jsonWriter.value(motcleSearchResultInfo.getScore());
            } else {
                jsonWriter.value(0);
            }
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_FICHECOUNT)) {
            jsonWriter.key("fichecount");
            jsonWriter.value(motcleInfo.getIndexationCount());
        }
        if (fieldVariant.isMotcleWith(FieldVariant.MOTCLE_ATTRIBUTES)) {
            jsonWriter.key("attrMap");
            CommonJson.object(jsonWriter, motcleData.getAttributes());
        }
    }

    private static void addDefaultMLabelArray(JSONWriter jsonWriter, MotcleData motcleData, Lang lang, boolean all) throws IOException {
        Labels labels = motcleData.getLabels();
        int labelLength = labels.size();
        if (labelLength == 0) {
            return;
        }
        if (!all) {
            Label label = labels.getLabel(lang);
            if (label != null) {
                jsonWriter.key("mlabelArray");
                jsonWriter.array();
                jsonWriter.object();
                jsonWriter.key("lang");
                jsonWriter.value(lang.toString());
                jsonWriter.key("mvalue");
                JsonUtils.writeMValue(jsonWriter, label.getLabelString(), null);
                jsonWriter.endObject();
                jsonWriter.endArray();
                return;
            }
        }
        jsonWriter.key("mlabelArray");
        jsonWriter.array();
        for (int i = 0; i < labelLength; i++) {
            Label label = labels.get(i);
            Lang labelLang = label.getLang();
            jsonWriter.object();
            jsonWriter.key("lang");
            jsonWriter.value(labelLang.toString());
            jsonWriter.key("mvalue");
            JsonUtils.writeMValue(jsonWriter, label.getLabelString(), null);
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
    }


    private static void addDefaultMLabelArray(JSONWriter jsonWriter, MotcleData motcleData, MotcleSearchResultInfo motcleSearchResultInfo, boolean all) throws IOException {
        Labels labels = motcleData.getLabels();
        int labelLength = labels.size();
        boolean inclus = false;
        for (int i = 0; i < labelLength; i++) {
            Label label = labels.get(i);
            Lang labelLang = label.getLang();
            InLangOccurrences occurrences = motcleSearchResultInfo.getInLangOccurrencesByLang(labelLang);
            if ((!all) && (occurrences == null)) {
                continue;
            }
            if (!inclus) {
                jsonWriter.key("mlabelArray");
                jsonWriter.array();
                inclus = true;
            }
            List<SearchTokenOccurrence> searchTokenOccurrenceList;
            if (occurrences != null) {
                searchTokenOccurrenceList = occurrences.getSearchTokenOccurrenceList();
            } else {
                searchTokenOccurrenceList = null;
            }
            jsonWriter.object();
            jsonWriter.key("lang");
            jsonWriter.value(labelLang.toString());
            jsonWriter.key("mvalue");
            JsonUtils.writeMValue(jsonWriter, label.getLabelString(), searchTokenOccurrenceList);
            jsonWriter.endObject();
        }
        if (inclus) {
            jsonWriter.endArray();
        }
    }

    private static void addMLabelArray(JSONWriter jsonWriter, MotcleData motcleData, MotcleSearchResultInfo motcleSearchResultInfo, Lang lang) throws IOException {
        InLangOccurrences inLangOccurrences = motcleSearchResultInfo.getInLangOccurrencesByLang(lang);
        if (inLangOccurrences == null) {
            addDefaultMLabelArray(jsonWriter, motcleData, motcleSearchResultInfo, false);
            return;
        }
        Labels labels = motcleData.getLabels();
        jsonWriter.key("mlabelArray");
        jsonWriter.array();
        Label langLabel = labels.getLabel(lang);
        jsonWriter.object();
        jsonWriter.key("lang");
        jsonWriter.value(lang.toString());
        jsonWriter.key("mvalue");
        JsonUtils.writeMValue(jsonWriter, langLabel.getLabelString(), inLangOccurrences.getSearchTokenOccurrenceList());
        jsonWriter.endObject();
        int labelLength = labels.size();
        if (labelLength > 1) {
            Set<Integer> operandeSet = JsonUtils.getOperandSet(inLangOccurrences);
            for (int i = 0; i < labelLength; i++) {
                Label label = labels.get(i);
                Lang labelLang = label.getLang();
                if (labelLang.equals(lang)) {
                    continue;
                }
                InLangOccurrences occurrences = motcleSearchResultInfo.getInLangOccurrencesByLang(labelLang);
                if (occurrences == null) {
                    continue;
                }
                if (JsonUtils.isCovered(occurrences, operandeSet)) {
                    continue;
                }
                jsonWriter.object();
                jsonWriter.key("lang");
                jsonWriter.value(labelLang.toString());
                jsonWriter.key("mvalue");
                JsonUtils.writeMValue(jsonWriter, label.getLabelString(), occurrences.getSearchTokenOccurrenceList());
                jsonWriter.endObject();
            }
        }
        jsonWriter.endArray();
    }

}
