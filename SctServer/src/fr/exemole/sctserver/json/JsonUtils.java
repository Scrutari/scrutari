/* SctEngine - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.stats.CountStats;
import net.scrutari.db.api.stats.LangStat;
import net.scrutari.db.api.stats.LangStats;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;


/**
 *
 * @author Vincent Calame
 */
public final class JsonUtils {

    private JsonUtils() {

    }

    public static void writeMValue(JSONWriter jsonWriter, String value, List<SearchTokenOccurrence> searchTokenOccurrenceList) throws IOException {
        if ((searchTokenOccurrenceList == null) || (searchTokenOccurrenceList.isEmpty())) {
            jsonWriter.array();
            jsonWriter.value(value);
            jsonWriter.endArray();
            return;
        }
        MarkEngine engine = new MarkEngine(jsonWriter, value, searchTokenOccurrenceList);
        engine.buildArray();
    }

    public static void addLangStats(JSONWriter jsonWriter, LangStats langStats) throws IOException {
        jsonWriter.key("langArray");
        jsonWriter.array();
        for (LangStat langStat : langStats) {
            jsonWriter.object();
            jsonWriter.key("lang");
            jsonWriter.value(langStat.getLang().toString());
            jsonWriter.key("fiche");
            jsonWriter.value(langStat.getCount());
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
    }

    public static void addCountStats(JSONWriter jsonWriter, CountStats stats) throws IOException {
        int baseCount = stats.getBaseCount();
        if (baseCount > - 1) {
            jsonWriter.key("base");
            jsonWriter.value(baseCount);
        }
        int corpusCount = stats.getCorpusCount();
        if (corpusCount > - 1) {
            jsonWriter.key("corpus");
            jsonWriter.value(corpusCount);
        }
        int thesaurusCount = stats.getThesaurusCount();
        if (thesaurusCount > -1) {
            jsonWriter.key("thesaurus");
            jsonWriter.value(thesaurusCount);
        }
        int ficheCount = stats.getFicheCount();
        if (ficheCount > -1) {
            jsonWriter.key("fiche");
            jsonWriter.value(ficheCount);
        }
        int motcleCount = stats.getMotcleCount();
        if (motcleCount > -1) {
            jsonWriter.key("motcle");
            jsonWriter.value(motcleCount);
        }
        int indexationCount = stats.getIndexationCount();
        if (indexationCount > -1) {
            jsonWriter.key("indexation");
            jsonWriter.value(indexationCount);
        }
        int lexieCount = stats.getLexieCount();
        if (lexieCount > -1) {
            jsonWriter.key("lexie");
            jsonWriter.value(lexieCount);
        }
    }


    public static Set<Integer> getOperandSet(InLangOccurrences occurrences) {
        Set<Integer> set = new HashSet<Integer>();
        for (SearchTokenOccurrence searchTokenOccurrence : occurrences.getSearchTokenOccurrenceList()) {
            set.add(searchTokenOccurrence.getOperandNumber());
        }
        return set;
    }

    public static boolean isCovered(InLangOccurrences occurrences, Set<Integer> set) {
        for (SearchTokenOccurrence searchTokenOccurrence : occurrences.getSearchTokenOccurrenceList()) {
            if (!set.contains(searchTokenOccurrence.getOperandNumber())) {
                return false;
            }
        }
        return true;
    }

    public static List<SearchTokenOccurrence> getList(Map<AlineaRank, InAlineaOccurrences> map, AlineaRank alineaRank) {
        return getList(map.get(alineaRank));
    }

    public static List<SearchTokenOccurrence> getList(InAlineaOccurrences occurrences) {
        if (occurrences == null) {
            return null;
        }
        return occurrences.getSearchTokenOccurrenceList();
    }

    public static String getAliasValue(FieldVariant.Alias alias, FicheData ficheData, BaseCheck baseCheck) {
        StringBuilder buf = new StringBuilder();
        String separator = alias.getSeparator();
        for (FieldVariant.FieldReference fieldReference : alias.getFieldReferenceList()) {
            String value = null;
            if (fieldReference instanceof FieldVariant.CoreFieldReference) {
                value = getCoreValue(ficheData, ((FieldVariant.CoreFieldReference) fieldReference).getField(), baseCheck);
            } else if (fieldReference instanceof FieldVariant.ComplementFieldReference) {
                value = getComplementValue(ficheData, ((FieldVariant.ComplementFieldReference) fieldReference).getComplementNumber());
            } else if (fieldReference instanceof FieldVariant.AttributeFieldReference) {
                appendAttribute(ficheData, ((FieldVariant.AttributeFieldReference) fieldReference).getAttributeKey(), buf, separator);
            }
            if ((value != null) && (value.length() > 0)) {
                if (buf.length() > 0) {
                    buf.append(separator);
                }
                buf.append(value);
            }
        }
        return buf.toString();
    }

    public static List<String> getAliasValueList(FieldVariant.Alias alias, FicheData ficheData, BaseCheck baseCheck) {
        List<String> values = new ArrayList<String>();
        for (FieldVariant.FieldReference fieldReference : alias.getFieldReferenceList()) {
            String value = null;
            if (fieldReference instanceof FieldVariant.CoreFieldReference) {
                value = getCoreValue(ficheData, ((FieldVariant.CoreFieldReference) fieldReference).getField(), baseCheck);
            } else if (fieldReference instanceof FieldVariant.ComplementFieldReference) {
                value = getComplementValue(ficheData, ((FieldVariant.ComplementFieldReference) fieldReference).getComplementNumber());
            } else if (fieldReference instanceof FieldVariant.AttributeFieldReference) {
                Attribute attribute = ficheData.getAttributes().getAttribute(((FieldVariant.AttributeFieldReference) fieldReference).getAttributeKey());
                if (attribute != null) {
                    values.addAll(attribute);
                }
            }
            if ((value != null) && (value.length() > 0)) {
                values.add(value);
            }
        }
        return values;
    }

    public static String getCoreValue(FicheData ficheData, int field, BaseCheck baseCheck) {
        switch (field) {
            case FieldVariant.FICHE_CODEFICHE:
                return String.valueOf(ficheData.getFicheCode());
            case FieldVariant.FICHE_CODECORPUS:
                return String.valueOf(ficheData.getCorpusCode());
            case FieldVariant.FICHE_CODEBASE:
                return String.valueOf(ficheData.getBaseCode());
            case FieldVariant.FICHE_AUTHORITY:
                return ficheData.getFicheURI().getCorpusURI().getBaseURI().getAuthority();
            case FieldVariant.FICHE_BASENAME:
                return ficheData.getFicheURI().getCorpusURI().getBaseURI().getBaseName();
            case FieldVariant.FICHE_CORPUSNAME:
                return ficheData.getFicheURI().getCorpusURI().getCorpusName();
            case FieldVariant.FICHE_FICHEID:
                return ficheData.getFicheURI().getFicheId();
            case FieldVariant.FICHE_TITRE:
                return ficheData.getTitre();
            case FieldVariant.FICHE_SOUSTITRE:
                return ficheData.getSoustitre();
            case FieldVariant.FICHE_HREF:
                return ficheData.getHref(baseCheck.getCheckedLang());
            case FieldVariant.FICHE_LANG:
                Lang lang = ficheData.getLang();
                if (!lang.equals(FicheData.UNDETERMINED_LANG)) {
                    return lang.toString();
                } else {
                    return null;
                }
            case FieldVariant.FICHE_YEAR:
                FuzzyDate yearDate = ficheData.getDate();
                if (yearDate != null) {
                    return String.valueOf(yearDate.getYear());
                } else {
                    return null;
                }
            case FieldVariant.FICHE_DATE:
                FuzzyDate date = ficheData.getDate();
                if (date != null) {
                    return date.toString();
                } else {
                    return null;
                }
            case FieldVariant.FICHE_DATEISO:
                FuzzyDate isoDate = ficheData.getDate();
                if (isoDate != null) {
                    return isoDate.toISOString();
                } else {
                    return null;
                }
            case FieldVariant.FICHE_FICHEICON:
                return ficheData.getFicheIcon();
            case FieldVariant.FICHE_ICON:
                String icon = ficheData.getFicheIcon();
                if (icon == null) {
                    icon = baseCheck.getBaseIcon();
                }
                return icon;
            case FieldVariant.FICHE_LATITUDE:
                if (ficheData.isWithGeo()) {
                    return ficheData.getLatitude().toString();
                } else {
                    return null;
                }
            case FieldVariant.FICHE_LONGITUDE:
                if (ficheData.isWithGeo()) {
                    return ficheData.getLongitude().toString();
                } else {
                    return null;
                }
            default:
                return null;
        }

    }

    private static String getComplementValue(FicheData ficheData, int complementNumber) {
        try {
            return ficheData.getComplementByNumber(complementNumber);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    private static void appendAttribute(FicheData ficheData, AttributeKey attributeKey, StringBuilder buf, String separator) {
        Attribute attribute = ficheData.getAttributes().getAttribute(attributeKey);
        if (attribute == null) {
            return;
        }
        if (buf.length() > 0) {
            buf.append(separator);
        }
        int valueLength = attribute.size();
        for (int i = 0; i < valueLength; i++) {
            if (i > 0) {
                buf.append(separator);
            }
            buf.append(attribute.get(i));
        }
    }


    private static class MarkEngine {

        private final JSONWriter jsonWriter;
        List<SearchTokenOccurrence> searchTokenOccurrenceList;
        StringBuilder buf;
        String s;
        int openIndex;
        int closeIndex;
        int currentPosition;

        /*
         * Constructeur unique.
         */
        private MarkEngine(JSONWriter jsonWriter, String s, List<SearchTokenOccurrence> searchTokenOccurrenceList) {
            this.jsonWriter = jsonWriter;
            this.searchTokenOccurrenceList = searchTokenOccurrenceList;
            this.s = s;
            currentPosition = 0;
            SearchTokenOccurrence firstTokenOccurrence = searchTokenOccurrenceList.get(0);
            openIndex = firstTokenOccurrence.getBeginIndex();
            closeIndex = openIndex + firstTokenOccurrence.getLength() - 1;
            buf = new StringBuilder();
        }

        /*
         * Lancement de l'écriture.
         */
        private void buildArray() throws IOException {
            jsonWriter.array();
            int length = s.length();
            boolean onObject = false;
            for (int i = 0; i < length; i++) {
                if (i == openIndex) {
                    flushBuffer(false);
                    jsonWriter.object();
                    jsonWriter.key("i");
                    jsonWriter.value(searchTokenOccurrenceList.get(currentPosition).getOperandNumber());
                    jsonWriter.key("s");
                    onObject = true;
                }
                char carac = s.charAt(i);
                buf.append(carac);
                if (i == closeIndex) {
                    flushBuffer(true);
                    jsonWriter.endObject();
                    increment();
                    onObject = false;
                }
            }
            flushBuffer(false);
            if (onObject) {
                jsonWriter.endObject();
            }
            jsonWriter.endArray();
        }

        void flushBuffer(boolean withEmpty) throws IOException {
            String bufString = buf.toString();
            if ((!withEmpty) && (bufString.length() == 0)) {
                return;
            }
            jsonWriter.value(bufString);
            buf = new StringBuilder();
        }

        /*
         * gère les éventuels recouvrements de position. Cela peut aller jusqu'à
         * générer un élément s vide (<s token-index=""/>) si une position est
         * complètement incluse dans une autre.
         */
        void increment() throws IOException {
            if (currentPosition == (searchTokenOccurrenceList.size() - 1)) {
                openIndex = -1;
                closeIndex = -1;
                return;
            }
            currentPosition++;
            SearchTokenOccurrence tokenOccurrence = searchTokenOccurrenceList.get(currentPosition);
            int beginIndex = tokenOccurrence.getBeginIndex();
            int endIndex = beginIndex + tokenOccurrence.getLength() - 1;
            if (beginIndex > closeIndex) {
                openIndex = beginIndex;
                closeIndex = endIndex;
                return;
            }
            openIndex = -1;
            jsonWriter.object();
            jsonWriter.key("i");
            jsonWriter.value(tokenOccurrence.getOperandNumber());
            if (endIndex <= closeIndex) {
                jsonWriter.endObject();
                increment();
            } else {
                jsonWriter.key("s");
                closeIndex = endIndex;
            }
        }

    }

}
