/* SctServer - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.json.corpusdata;

import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.db.api.stats.ScrutariDBStats;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusDataJson_A {

    private CorpusDataJson_A() {

    }

    public static void object(JSONWriter jsonWriter, CorpusData corpusData, Lang lang, ScrutariDBStats scrutariDBStats) throws IOException {
        int code = corpusData.getCorpusCode();
        jsonWriter.object();
        jsonWriter.key("codecorpus");
        jsonWriter.value(code);
        jsonWriter.key("codebase");
        jsonWriter.value(corpusData.getBaseCode());
        CorpusURI corpusURI = corpusData.getCorpusURI();
        String corpusName = corpusURI.getCorpusName();
        jsonWriter.key("corpusname");
        jsonWriter.value(corpusName);
        int complementCount = corpusData.getComplementMaxNumber();
        jsonWriter.key("complementCount");
        jsonWriter.value(complementCount);
        jsonWriter.key("intitules");
        jsonWriter.object();
        jsonWriter.key("corpus");
        jsonWriter.value(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_TITLE, lang, corpusName));
        jsonWriter.key("fiche");
        jsonWriter.value(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_FICHE, lang, corpusName));
        for (int j = 1; j <= complementCount; j++) {
            jsonWriter.key("complement_" + j);
            jsonWriter.value(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_COMPLEMENTPREFIX + j, lang, "complement_" + j));
        }
        jsonWriter.endObject();
        if (scrutariDBStats != null) {
            jsonWriter.key("stats");
            jsonWriter.object();
            JsonUtils.addCountStats(jsonWriter, scrutariDBStats.getCountStats(code));
            JsonUtils.addLangStats(jsonWriter, scrutariDBStats.getLangStats(code));
            jsonWriter.endObject();
        }
        jsonWriter.endObject();
    }

}
