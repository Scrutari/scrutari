/* SctServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import java.io.IOException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.xml.XMLWriter;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.db.tools.util.BaseCheck;


/**
 *
 * @author Vincent Calame
 */
public final class FeedXmlUtils {

    private FeedXmlUtils() {
    }

    public static void addFicheData(XMLWriter xmlWriter, FicheData ficheData, SctFeedOptions feedOptions, BaseCheck baseCheck) throws IOException {
        int optionMask = feedOptions.getOptionMask();
        xmlWriter.openTag("li");
        xmlWriter.startOpenTag("p");
        xmlWriter.addAttribute("class", "scrutari-fiche-Titre");
        xmlWriter.endOpenTag();
        if ((optionMask & SctFeedConstants.ICON_OPTION) != 0) {
            String icon = ficheData.getFicheIcon();
            if (icon == null) {
                icon = baseCheck.getBaseIcon();
            }
            if (icon != null) {
                xmlWriter.startOpenTag("img", false);
                xmlWriter.addAttribute("src", icon);
                xmlWriter.addAttribute("class", "scrutari-fiche-Icon");
                xmlWriter.closeEmptyTag();
                xmlWriter.addText(" ");
            }
        }
        String href = ficheData.getHref(baseCheck.getCheckedLang());
        if (href != null) {
            xmlWriter.startOpenTag("a", false);
            xmlWriter.addAttribute("href", href);
            xmlWriter.endOpenTag();
        }
        String titre = ficheData.getTitre();
        if (titre != null) {
            xmlWriter.addText(titre);
        } else {
            xmlWriter.addText(ficheData.getFicheURI().getFicheId());
        }
        if (href != null) {
            xmlWriter.closeTag("a", false);
        }
        xmlWriter.closeTag("p", false);
        if ((optionMask & SctFeedConstants.SOUSTITRE_OPTION) != 0) {
            String soustitre = ficheData.getSoustitre();
            if (soustitre != null) {
                xmlWriter.startOpenTag("p");
                xmlWriter.addAttribute("class", "scrutari-fiche-Soustitre");
                xmlWriter.endOpenTag();
                xmlWriter.addText(soustitre);
                xmlWriter.closeTag("p", false);
            }
        }
        xmlWriter.closeTag("li");
    }

    public static void addMotcleData(XMLWriter xmlWriter, MotcleData motcleData, SctFeedOptions feedOptions) throws IOException {
        Labels labels = motcleData.getLabels();
        xmlWriter.openTag("li");
        xmlWriter.startOpenTag("p");
        xmlWriter.addAttribute("class", "scrutari-Motcle");
        xmlWriter.endOpenTag();
        int labelCount = labels.size();
        for (int l = 0; l < labelCount; l++) {
            if (l > 0) {
                xmlWriter.addText(" / ");
            }
            Label label = labels.get(l);
            xmlWriter.addText(label.getLabelString());
        }
        xmlWriter.closeTag("p", false);
        xmlWriter.closeTag("li");
    }

    public static String getDefaultLabel(EngineMetadata engineMetadata, String phraseName, Lang lang, String engineName) {
        Labels labels = engineMetadata.getPhrases().getPhrase(phraseName);
        if (labels == null) {
            labels = engineMetadata.getTitleLabels();
        }
        return labels.seekLabelString(lang, "Scrutari – " + engineName);
    }

}
