/* SctEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import net.mapeadores.util.xml.ns.NameSpace;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;


/**
 *
 * @author Vincent Calame
 */
class CategoriesEntriesXMLPart extends AbstractEntriesXMLPart {

    CategoriesEntriesXMLPart(AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession, SctFeedOptions sctFeedOptions) {
        super(xmlWriter, scrutariSession, sctFeedOptions);
    }

    @Override
    public void appendEntries(DataAccess dataAccess) throws IOException {
        for (ScrutariDBName scrutariDBName : sctFeedInfo.getLastScrutariDBNameList()) {
            appendFeedEntry(scrutariDBName, dataAccess, sctFeedOptions);
        }
    }

    private void appendFeedEntry(ScrutariDBName scrutariDBName, DataAccess dataAccess, SctFeedOptions feedOptions) throws IOException {
        URITree uriTree = scrutariSession.getEngine().getEngineStorage().getAddedURITree(scrutariDBName, dataAccess.getScrutariDataURIChecker());
        if (uriTree == null) {
            return;
        }
        EntryContent entryContent = new EntryContent(feedOptions, scrutariSession.getGlobalSearchOptions());
        entryContent.checkUriTree(dataAccess, uriTree);
        int ficheAddCount = entryContent.getFicheAddCount();
        if (ficheAddCount == 0) {
            return;
        }
        openTag("entry");
        addSimpleElement("title", getEntryTitle(scrutariDBName, ficheAddCount, feedOptions));
        addSimpleElement("updated", scrutariDBName.toGmtIsoFormat());
        addSimpleElement("id", "session:" + scrutariDBName.toString());
        startOpenTag("content");
        addAttribute("type", "xhtml");
        endOpenTag();
        startOpenTag("div");
        XMLUtils.appendNameSpaceAttribute(this, NameSpace.XHTML_NAMESPACE);
        endOpenTag();
        entryContent.addContent(this);
        closeTag("div");
        closeTag("content");
        closeTag("entry");
    }

    private String getEntryTitle(ScrutariDBName scrutariDBName, int ficheAddCount, SctFeedOptions feedOptions) {
        StringBuilder buf = new StringBuilder();
        if ((feedOptions.getOptionMask() & SctFeedConstants.TECHNICALTITLE_OPTION) != 0) {
            buf.append("s=");
            buf.append(scrutariSession.getEngineName());
            buf.append(";d=");
            buf.append(scrutariDBName.toGmtIsoFormat());
            buf.append(";f=");
            buf.append(ficheAddCount);
            return buf.toString();
        } else {
            buf.append(scrutariSession.getEngineName());
            buf.append(" + ");
            buf.append(ficheAddCount);
        }
        return buf.toString();
    }


    private static class EntryContent {

        private final SctFeedOptions feedOptions;
        private final GlobalSearchOptions globalSearchOptions;
        private int ficheAddCount = 0;
        private SortedMap<Integer, FicheGroup> ficheGroupByCategoryMap;
        private FicheGroup uniqueFicheGroup;

        private EntryContent(SctFeedOptions feedOptions, GlobalSearchOptions globalSearchOptions) {
            this.feedOptions = feedOptions;
            this.globalSearchOptions = globalSearchOptions;
            List<Category> categoryList = globalSearchOptions.getCategoryList();
            if (categoryList.isEmpty()) {
                uniqueFicheGroup = new FicheGroup(null);
            } else {
                ficheGroupByCategoryMap = new TreeMap<Integer, FicheGroup>(Comparator.reverseOrder());
                for (Category category : categoryList) {
                    ficheGroupByCategoryMap.put(category.getCategoryDef().getRank(), new FicheGroup(category));
                }
            }
        }

        private int getFicheAddCount() {
            return ficheAddCount;
        }

        private void addContent(XMLWriter xmlWriter) throws IOException {
            if (uniqueFicheGroup != null) {
                uniqueFicheGroup.add(xmlWriter, feedOptions);
            } else {
                for (FicheGroup ficheGroup : ficheGroupByCategoryMap.values()) {
                    ficheGroup.add(xmlWriter, feedOptions);
                }
            }
        }

        void checkUriTree(DataAccess dataAccess, URITree uriTree) {
            Lang lang = feedOptions.getLang();
            int ct = 0;
            for (BaseNode baseNode : uriTree.getBaseNodeList()) {
                BaseURI baseURI = baseNode.getBaseURI();
                BaseData baseData = (BaseData) dataAccess.getScrutariData(baseURI);
                if (baseData == null) {
                    continue;
                }
                BaseCheck baseCheck = BaseChecker.check(baseData, lang);
                for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
                    CorpusURI corpusURI = corpusNode.getCorpusURI();
                    CorpusData corpusData = (CorpusData) dataAccess.getScrutariData(corpusURI);
                    if (corpusData == null) {
                        continue;
                    }
                    FicheGroup ficheGroup = getFicheGroup(corpusData.getCorpusCode());
                    for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
                        FicheURI ficheURI = ficheNode.getFicheURI();
                        FicheData ficheData = (FicheData) dataAccess.getScrutariData(ficheURI);
                        if (ficheData == null) {
                            continue;
                        }
                        ficheGroup.addFicheData(ficheData, baseCheck);
                        ct++;
                    }
                }
            }
            this.ficheAddCount = ct;
        }

        private FicheGroup getFicheGroup(Integer corpusCode) {
            if (uniqueFicheGroup != null) {
                return uniqueFicheGroup;
            }
            Category category = globalSearchOptions.getCategoryByCorpus(corpusCode);
            return ficheGroupByCategoryMap.get(category.getCategoryDef().getRank());
        }

    }


    private static class FicheGroup {

        private final Category category;
        private final SortedSet<FicheDataWrapper> ficheDataWrapperSet;

        private FicheGroup(Category category) {
            this.category = category;
            ficheDataWrapperSet = new TreeSet<FicheDataWrapper>(new FicheDataWrapperComparator());
        }

        private void addFicheData(FicheData ficheData, BaseCheck baseCheck) {
            ficheDataWrapperSet.add(new FicheDataWrapper(ficheData, baseCheck));
        }

        public void add(XMLWriter xmlWriter, SctFeedOptions feedOptions) throws IOException {
            if (ficheDataWrapperSet.isEmpty()) {
                return;
            }
            if (category != null) {
                CategoryDef categoryDef = category.getCategoryDef();
                xmlWriter.startOpenTag("p");
                xmlWriter.addAttribute("class", "scrutari-FicheGroup");
                xmlWriter.endOpenTag();
                xmlWriter.addText(categoryDef.getTitle(feedOptions.getLang()));
                xmlWriter.closeTag("p", false);
            }
            xmlWriter.startOpenTag("ul");
            xmlWriter.addAttribute("class", "scrutari-FicheList");
            xmlWriter.endOpenTag();
            int count = 0;
            int max = 100;
            int size = ficheDataWrapperSet.size();
            if (size < (max + 50)) {
                max = size + 1;
            }
            for (FicheDataWrapper ficheDataWrapper : ficheDataWrapperSet) {
                ficheDataWrapper.add(xmlWriter, feedOptions);
                count++;
                if (count == max) {
                    int remaining = size - count;
                    xmlWriter.openTag("li");
                    xmlWriter.addText("+ " + remaining);
                    xmlWriter.closeTag("li");
                    break;
                }
            }
            xmlWriter.closeTag("ul");
        }

    }


    private static class FicheDataWrapper {

        private final FicheData ficheData;
        private final BaseCheck baseCheck;

        private FicheDataWrapper(FicheData ficheData, BaseCheck baseCheck) {
            this.ficheData = ficheData;
            this.baseCheck = baseCheck;

        }

        private FicheData getFicheData() {
            return ficheData;
        }

        private void add(XMLWriter xmlWriter, SctFeedOptions feedOptions) throws IOException {
            FeedXmlUtils.addFicheData(xmlWriter, ficheData, feedOptions, baseCheck);
        }

    }


    private static class FicheDataWrapperComparator implements Comparator<FicheDataWrapper> {

        private FicheDataWrapperComparator() {
        }

        @Override
        public int compare(FicheDataWrapper ficheDataWrapper1, FicheDataWrapper ficheDataWrapper2) {
            FicheData ficheData1 = ficheDataWrapper1.getFicheData();
            FicheData ficheData2 = ficheDataWrapper2.getFicheData();
            String titre1 = ficheData1.getTitre();
            String titre2 = ficheData2.getTitre();
            if (titre1 == null) {
                if (titre2 != null) {
                    return 1;
                }
                return compareCode(ficheData1, ficheData2);
            } else {
                if (titre2 == null) {
                    return -1;
                }
                int comp = titre1.compareToIgnoreCase(titre2);
                if (comp != 0) {
                    return comp;
                } else {
                    return compareCode(ficheData1, ficheData2);
                }
            }
        }

        private int compareCode(FicheData ficheData1, FicheData ficheData2) {
            int code1 = ficheData1.getFicheCode();
            int code2 = ficheData2.getFicheCode();
            if (code1 < code2) {
                return -1;
            }
            if (code1 > code2) {
                return 1;
            }
            return 0;
        }

    }

}
