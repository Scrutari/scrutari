/* SctServer - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import fr.exemole.sctserver.api.log.SctLogConstants;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.tools.EngineUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MessageLog;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.ns.NameSpace;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class ErrorsEntriesXMLPart extends AbstractEntriesXMLPart {

    private final MessageLocalisation messageLocalisation;

    ErrorsEntriesXMLPart(AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession, SctFeedOptions sctFeedOptions) {
        super(xmlWriter, scrutariSession, sctFeedOptions);
        messageLocalisation = EngineUtils.getMessageLocalisation(scrutariSession.getEngine(), sctFeedOptions.getLang());
    }

    @Override
    public void appendEntries(DataAccess dataAccess) throws IOException {
        SctEngine engine = scrutariSession.getEngine();
        LogStorage logStorage = engine.getEngineStorage().getLogStorage();
        ScrutariDBName scrutariDBName = scrutariSession.getScrutariDB().getScrutariDBName();
        String dataLog = logStorage.getLog(SctLogConstants.DATA_DIR, "data.xml");
        if (dataLog == null) {
            return;
        }
        MessageLog messageLog = LogUtils.readMessageLog(dataLog);
        int errorCount = getErrorCount(messageLog);
        if (errorCount == 0) {
            return;
        }
        openTag("entry");
        addSimpleElement("title", getEntryTitle(errorCount, messageLocalisation, sctFeedOptions.getLang()));
        addSimpleElement("updated", scrutariDBName.toGmtIsoFormat());
        addSimpleElement("id", "session:" + scrutariDBName.toString());
        startOpenTag("content");
        addAttribute("type", "xhtml");
        endOpenTag();
        startOpenTag("div");
        XMLUtils.appendNameSpaceAttribute(this, NameSpace.XHTML_NAMESPACE);
        endOpenTag();
        writeLog(messageLog, messageLocalisation);
        closeTag("div");
        closeTag("content");
        closeTag("entry");
    }

    private String getEntryTitle(int errorCount, MessageLocalisation messageLocalisation, Lang lang) {
        StringBuilder buf = new StringBuilder();
        buf.append("[");
        buf.append(scrutariSession.getEngineName());
        buf.append("] ");
        if (errorCount == 1) {
            buf.append(messageLocalisation.toString("_ info.admin.error_one"));
        } else {
            buf.append(messageLocalisation.toString("_ info.admin.error_many", StringUtils.toString(errorCount, lang.toLocale())));
        }
        return buf.toString();

    }

    private int getErrorCount(MessageLog messageLog) {
        int count = 0;
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            count = count + messageSource.getEntryList().size();
        }
        return count;
    }

    private void writeLog(MessageLog messageLog, MessageLocalisation messageLocalisation) throws IOException {
        openTag("ul");
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            openTag("li");
            openTag("p");
            openTag("code");
            addText("[");
            addText(messageSource.getName());
            addText("]");
            closeTag("code");
            closeTag("p");
            List<MessageSource.Entry> messageList = messageSource.getEntryList();
            openTag("ul");
            if (messageList.size() < 6) {
                for (MessageSource.Entry entry : messageList) {
                    printMessageEntry(entry, messageLocalisation);
                }
            } else {
                int remaining = 0;
                Map<String, Increment> incrementMap = new HashMap<String, Increment>();
                for (MessageSource.Entry entry : messageList) {
                    String category = entry.getCategory();
                    Increment increment = incrementMap.get(category);
                    if (increment == null) {
                        increment = new Increment(3);
                        incrementMap.put(category, increment);
                    }
                    if (increment.isReached()) {
                        remaining++;
                    } else {
                        printMessageEntry(entry, messageLocalisation);
                        increment.next();
                    }
                }
                if (remaining > 0) {
                    openTag("li");
                    openTag("p");
                    addText(messageLocalisation.toString("_ info.admin.moreerrors", remaining));
                    closeTag("p");
                    closeTag("li");
                }
            }
            closeTag("ul");
            closeTag("li");
        }
        closeTag("ul");
    }

    private boolean printMessageEntry(MessageSource.Entry entry, MessageLocalisation messageLocalisation) throws IOException {
        String category = entry.getCategory();
        openTag("li");
        openTag("p");
        if (category.length() > 0) {
            openTag("code");
            addText("[");
            addText(category);
            addText("]");
            closeTag("code");
            addText(" ");
        }
        addText(messageLocalisation.toString(entry.getMessage()));
        closeTag("p");
        closeTag("li");
        return true;
    }


    private static class Increment {

        private final int max;
        private int value = 0;

        private Increment(int max) {
            this.max = max;
        }

        public boolean isReached() {
            return (value >= max);
        }

        public void next() {
            value++;
        }

    }

}
