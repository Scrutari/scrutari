/* SctServer - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import fr.exemole.sctserver.tools.feed.BaseWrapper;
import fr.exemole.sctserver.tools.feed.CorpusWrapper;
import fr.exemole.sctserver.tools.feed.FicheFeed;
import fr.exemole.sctserver.tools.feed.FicheFeedEngine;
import fr.exemole.sctserver.tools.feed.FicheWrapper;
import fr.exemole.sctserver.tools.feed.ThesaurusWrapper;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.ns.NameSpace;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.SctSpace;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.ScrutariConstants;


/**
 *
 * @author Vincent Calame
 */
class FichesEntriesXMLPart extends AbstractEntriesXMLPart {

    private final AttributeDef[] primaryAttributeDefArray;
    private final AttributeDef[] secondaryAttributeDefArray;

    FichesEntriesXMLPart(AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession, SctFeedOptions sctFeedOptions) {
        super(xmlWriter, scrutariSession, sctFeedOptions);
        AttributeDefManager attributeDefManager = scrutariSession.getEngine().getAttributeDefManager();
        this.primaryAttributeDefArray = toArray(attributeDefManager, ScrutariConstants.PRIMARY_GROUP);
        this.secondaryAttributeDefArray = toArray(attributeDefManager, ScrutariConstants.SECONDARY_GROUP);
    }

    @Override
    public void appendEntries(DataAccess dataAccess) throws IOException {
        Lang lang = sctFeedOptions.getLang();
        List<ThesaurusData> thesaurusDataList = dataAccess.getThesaurusDataList();
        int thesaurusLength = thesaurusDataList.size();
        ThesaurusWrapper[] thesaurusWrapperArray = new ThesaurusWrapper[thesaurusLength];
        for (int i = 0; i < thesaurusLength; i++) {
            thesaurusWrapperArray[i] = ThesaurusWrapper.wrapThesaurus(thesaurusDataList.get(i), lang);
        }
        try {
            FicheFeed ficheFeed = FicheFeedEngine.build(scrutariSession, sctFeedInfo, dataAccess, sctFeedOptions);
            if (ficheFeed != null) {
                appendFicheFeed(ficheFeed, dataAccess, thesaurusWrapperArray);
            }
        } catch (ErrorMessageException errorMessageException) {
            CommandMessage errorMessage = errorMessageException.getErrorMessage();
            StringBuilder labelBuf = new StringBuilder();
            Object[] values = errorMessage.getMessageValues();
            int length = values.length;
            for (int i = 0; i < length; i++) {
                if (i > 0) {
                    labelBuf.append(", ");
                }
                labelBuf.append(values[i].toString());
            }
            appendDebugCategory("PARAMETER_ERROR/" + errorMessage.getMessageKey(), labelBuf.toString());
        }
    }

    private AttributeDef[] toArray(AttributeDefManager attributeDefManager, String name) {
        List<AttributeDef> list = attributeDefManager.getAttributeDefList(name);
        return list.toArray(new AttributeDef[list.size()]);
    }

    private void appendDebugCategory(String term, String label) throws IOException {
        startOpenTag("category");
        addAttribute("term", term);
        addAttribute("label", label);
        closeEmptyTag();
    }

    private void appendFicheFeed(FicheFeed ficheFeed, DataAccess dataAccess, ThesaurusWrapper[] thesaurusWrapperArray) throws IOException {
        Lang lang = ficheFeed.getLang();
        for (FicheWrapper ficheWrapper : ficheFeed.getFicheWrapperList()) {
            FicheData ficheData = ficheWrapper.getFicheData();
            CorpusWrapper corpusWrapper = ficheWrapper.getCorpusWrapper();
            BaseWrapper baseWrapper = ficheWrapper.getBaseWrapper();
            openTag("entry");
            String title = ficheData.getTitre();
            if (title == null) {
                title = "?";
            }
            addSimpleElement("title", title);
            addSimpleElement("updated", ficheWrapper.getScrutariDBName().toGmtIsoFormat());
            addSimpleElement("id", ficheData.getFicheURI().toString());
            openTag("author");
            addSimpleElement("name", baseWrapper.getShortLabelString());
            closeTag("author");
            startOpenTag("link");
            addAttribute("href", ficheData.getHref(baseWrapper.getBaseCheck().getCheckedLang()));
            addAttribute("rel", "alternate");
            closeEmptyTag();
            String categoryTerm = corpusWrapper.getCategoryTerm();
            if (categoryTerm != null) {
                startOpenTag("category");
                addAttribute("term", categoryTerm);
                addAttribute("label", corpusWrapper.getCategoryLabel());
                closeEmptyTag();
            }
            startOpenTag("content");
            addAttribute("type", "xhtml");
            endOpenTag();
            startOpenTag("div");
            XMLUtils.appendNameSpaceAttribute(this, NameSpace.XHTML_NAMESPACE);
            endOpenTag();
            appendContent(ficheWrapper, dataAccess, lang, thesaurusWrapperArray);
            closeTag("div");
            closeTag("content");
            closeTag("entry");
        }
    }

    private void appendContent(FicheWrapper ficheWrapper, DataAccess dataAccess, Lang lang, ThesaurusWrapper[] thesaurusWrapperArray) throws IOException {
        FicheData ficheData = ficheWrapper.getFicheData();
        CorpusWrapper corpusWrapper = ficheWrapper.getCorpusWrapper();
        String soustitre = ficheData.getSoustitre();
        if (soustitre != null) {
            openTag("p");
            openTag("em", false);
            addText(soustitre);
            closeTag("em", false);
            closeTag("p", false);
        }
        Attributes attributes = ficheData.getAttributes();
        for (AttributeDef attributeDef : primaryAttributeDefArray) {
            Attribute attribute = attributes.getAttribute(attributeDef.getAttributeKey());
            if (attribute != null) {
                appendAttribute(attribute, attributeDef, lang);
            }
        }
        int maxNumber = ficheData.getComplementMaxNumber();
        if (maxNumber > 0) {
            for (int num = 1; num <= maxNumber; num++) {
                String comp = ficheData.getComplementByNumber(num);
                if (comp == null) {
                    continue;
                }
                openTag("p");
                openTag("strong", false);
                addText(corpusWrapper.getComplementLabel(num));
                addText(" : ");
                closeTag("strong", false);
                addText(comp);
                closeTag("p", false);
            }
        }
        for (AttributeDef attributeDef : secondaryAttributeDefArray) {
            Attribute attribute = attributes.getAttribute(attributeDef.getAttributeKey());
            if (attribute != null) {
                appendAttribute(attribute, attributeDef, lang);
            }
        }
        FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheData.getFicheCode());
        for (ThesaurusWrapper thesaurusWrapper : thesaurusWrapperArray) {
            List<Integer> indexationMotcleCodeList = ficheInfo.getIndexationCodeList(thesaurusWrapper.getThesaurusCode());
            if (!indexationMotcleCodeList.isEmpty()) {
                openTag("p");
                openTag("strong", false);
                addText(thesaurusWrapper.getLabelString());
                addText(" : ");
                closeTag("strong", false);
                boolean next = false;
                for (Integer codeMotcle : indexationMotcleCodeList) {
                    MotcleData motcleData = dataAccess.getMotcleData(codeMotcle);
                    if (motcleData != null) {
                        if (next) {
                            addText(", ");
                        } else {
                            next = true;
                        }
                        addText(motcleData.getLabels().seekLabelString(lang, ""));
                    }
                }
                closeTag("p", false);
            }
        }
        openTag("p");
        openTag("strong", false);
        openTag("em", false);
        addText(ficheWrapper.getBaseWrapper().getLongLabelString());
        addText(" – ");
        addText(corpusWrapper.getCorpusLabelString());
        closeTag("em", false);
        closeTag("strong", false);
        closeTag("p", false);
    }

    private void appendAttribute(Attribute attribute, AttributeDef attributeDef, Lang lang) throws IOException {
        String title = attributeDef.getTitle(lang);
        String formatType = SctSpace.getFormatType(attributeDef);
        if (formatType.equals(SctSpace.BLOCK_TYPE)) {
            openTag("p");
            openTag("strong", false);
            addText(title);
            addText(" : ");
            closeTag("strong", false);
            closeTag("p", false);
            int valueLength = attribute.size();
            for (int i = 0; i < valueLength; i++) {
                addSimpleElement("p", attribute.get(i));
            }
        } else if (formatType.equals(SctSpace.LIST_TYPE)) {
            openTag("p");
            openTag("strong", false);
            addText(title);
            addText(" : ");
            closeTag("strong", false);
            closeTag("p", false);
            openTag("ul");
            int valueLength = attribute.size();
            for (int i = 0; i < valueLength; i++) {
                addSimpleElement("li", attribute.get(i));
            }
            closeTag("ul");
        } else {
            openTag("p");
            openTag("strong", false);
            addText(title);
            addText(" : ");
            closeTag("strong", false);
            int valueLength = attribute.size();
            for (int i = 0; i < valueLength; i++) {
                if (i > 0) {
                    addText(", ");
                }
                addText(attribute.get(i));
            }
            closeTag("p", false);
        }
    }

}
