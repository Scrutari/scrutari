/* SctEngine - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import java.io.IOException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.AppendableXMLPart;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.ns.NameSpace;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class AtomFeedXMLPart extends AppendableXMLPart {

    public final static String FEED_TITLE = "feed-title";
    public final static String FEED_AUTHOR = "feed-author";
    private final ScrutariSession scrutariSession;
    private final SctFeedOptions sctFeedOptions;

    public AtomFeedXMLPart(AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession, SctFeedOptions feedOptions) {
        super(xmlWriter);
        this.scrutariSession = scrutariSession;
        this.sctFeedOptions = feedOptions;
    }

    public void appendFeed() throws IOException {
        EngineMetadata engineMetadata = scrutariSession.getEngine().getEngineMetadata();
        String engineName = scrutariSession.getEngineName();
        Lang lang = sctFeedOptions.getLang();
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            startOpenTag("feed");
            XMLUtils.appendNameSpaceAttribute(this, NameSpace.ATOM_NAMESPACE);
            endOpenTag();
            addSimpleElement("title", FeedXmlUtils.getDefaultLabel(engineMetadata, FEED_TITLE, lang, engineName));
            addSimpleElement("updated", getUpdateScrutariDBName().toGmtIsoFormat());
            openTag("author");
            addSimpleElement("name", FeedXmlUtils.getDefaultLabel(engineMetadata, FEED_AUTHOR, lang, engineName));
            closeTag("author");
            String selfUrl = sctFeedOptions.getSelfUrl();
            addSimpleElement("id", selfUrl);
            startOpenTag("link");
            addAttribute("rel", "self");
            addAttribute("href", selfUrl);
            closeEmptyTag();
            AbstractEntriesXMLPart entriesXMLPart = AbstractEntriesXMLPart.newInstance(sctFeedOptions, this, scrutariSession);
            entriesXMLPart.appendEntries(dataAccess);
            closeTag("feed");
        }
    }

    private ScrutariDBName getUpdateScrutariDBName() {
        switch (sctFeedOptions.getSctFeedType()) {
            case SctFeedConstants.ERRORS_SCTFEED_TYPE:
                return scrutariSession.getScrutariDB().getScrutariDBName();
            default:
                return scrutariSession.getSctFeedInfo().getLastScrutariDBNameList().get(0);
        }
    }

}
