/* SctServer - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import net.mapeadores.util.xml.ns.NameSpace;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.MotcleNode;
import net.scrutari.datauri.tree.ThesaurusNode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;


/**
 *
 * @author Vincent Calame
 */
class TreeEntriesXMLPart extends AbstractEntriesXMLPart {

    TreeEntriesXMLPart(AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession, SctFeedOptions sctFeedOptions) {
        super(xmlWriter, scrutariSession, sctFeedOptions);
    }

    @Override
    public void appendEntries(DataAccess dataAccess) throws IOException {
        for (ScrutariDBName scrutariDBName : sctFeedInfo.getLastScrutariDBNameList()) {
            appendFeedEntry(scrutariDBName, dataAccess, sctFeedOptions);
        }
    }

    private void appendFeedEntry(ScrutariDBName scrutariDBName, DataAccess dataAccess, SctFeedOptions feedOptions) throws IOException {
        URITree uriTree = scrutariSession.getEngine().getEngineStorage().getAddedURITree(scrutariDBName, dataAccess.getScrutariDataURIChecker());
        if (uriTree == null) {
            return;
        }
        EntryContent entryContent = new EntryContent(feedOptions);
        entryContent.checkUriTree(dataAccess, uriTree);
        int ficheAddCount = entryContent.getFicheAddCount();
        int motcleAddCount = entryContent.getMotcleAddCount();
        if ((ficheAddCount == 0) && (motcleAddCount == 0)) {
            return;
        }
        openTag("entry");
        addSimpleElement("title", getEntryTitle(scrutariDBName, ficheAddCount, motcleAddCount, feedOptions));
        addSimpleElement("updated", scrutariDBName.toGmtIsoFormat());
        addSimpleElement("id", "session:" + scrutariDBName.toString());
        startOpenTag("content");
        addAttribute("type", "xhtml");
        endOpenTag();
        startOpenTag("div");
        XMLUtils.appendNameSpaceAttribute(this, NameSpace.XHTML_NAMESPACE);
        endOpenTag();
        entryContent.addContent(this);
        closeTag("div");
        closeTag("content");
        closeTag("entry");
    }

    private String getEntryTitle(ScrutariDBName scrutariDBName, int ficheAddCount, int motcleAddCount, SctFeedOptions feedOptions) {
        int optionMask = feedOptions.getOptionMask();
        StringBuilder buf = new StringBuilder();
        if ((optionMask & SctFeedConstants.TECHNICALTITLE_OPTION) != 0) {
            buf.append("s=");
            buf.append(scrutariSession.getEngineName());
            buf.append(";d=");
            buf.append(scrutariDBName.toGmtIsoFormat());
            buf.append(";f=");
            buf.append(ficheAddCount);
            if ((optionMask & SctFeedConstants.FICHEONLY_OPTION) == 0) {
                buf.append(";m=");
                buf.append(motcleAddCount);
            }
            return buf.toString();
        } else {
            buf.append(scrutariSession.getEngineName());
            buf.append(" + ");
            buf.append(ficheAddCount);
            if (motcleAddCount > 0) {
                buf.append(" / +");
                buf.append(motcleAddCount);
            }
        }
        return buf.toString();
    }


    private static class EntryContent {

        private final List<BaseBuffer> baseBufferList = new ArrayList<BaseBuffer>();
        private final SctFeedOptions feedOptions;
        private int motcleAddCount = 0;
        private int ficheAddCount = 0;

        private EntryContent(SctFeedOptions feedOptions) {
            this.feedOptions = feedOptions;
        }

        private int getFicheAddCount() {
            return ficheAddCount;
        }

        private int getMotcleAddCount() {
            return motcleAddCount;
        }

        public void addContent(XMLWriter xmlWriter) throws IOException {
            for (BaseBuffer baseBuffer : baseBufferList) {
                baseBuffer.addContent(xmlWriter, feedOptions);
            }
        }

        void checkUriTree(DataAccess dataAccess, URITree uriTree) {
            boolean withMotscles = ((feedOptions.getOptionMask() & SctFeedConstants.FICHEONLY_OPTION) == 0);
            for (BaseNode baseNode : uriTree.getBaseNodeList()) {
                BaseURI baseURI = baseNode.getBaseURI();
                BaseData baseData = (BaseData) dataAccess.getScrutariData(baseURI);
                if (baseData == null) {
                    continue;
                }
                BaseBuffer baseBuffer = new BaseBuffer(baseNode, baseData);
                baseBuffer.checkBase(dataAccess, withMotscles);
                int baseFicheAddCount = baseBuffer.getFicheAddCount();
                int baseMotcleAddCount = baseBuffer.getMotcleAddCount();
                if ((baseFicheAddCount == 0) && (baseMotcleAddCount == 0)) {
                    continue;
                }
                ficheAddCount = ficheAddCount + baseFicheAddCount;
                motcleAddCount = motcleAddCount + baseMotcleAddCount;
                baseBufferList.add(baseBuffer);
            }
        }


        private static class BaseBuffer {

            private final List<CorpusBuffer> corpusBufferList = new ArrayList<CorpusBuffer>();
            private final List<ThesaurusBuffer> thesaurusBufferList = new ArrayList<ThesaurusBuffer>();
            private final BaseNode baseNode;
            private final BaseData baseData;
            private int motcleAddCount;
            private int ficheAddCount;

            private BaseBuffer(BaseNode baseNode, BaseData baseData) {
                this.baseNode = baseNode;
                this.baseData = baseData;
            }

            private int getFicheAddCount() {
                return ficheAddCount;
            }

            private int getMotcleAddCount() {
                return motcleAddCount;
            }

            private void checkBase(DataAccess dataAccess, boolean withMotscles) {
                motcleAddCount = 0;
                ficheAddCount = 0;
                for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
                    CorpusURI corpusURI = corpusNode.getCorpusURI();
                    CorpusData corpusData = (CorpusData) dataAccess.getScrutariData(corpusURI);
                    if (corpusData == null) {
                        continue;
                    }
                    CorpusBuffer corpusBuffer = new CorpusBuffer(corpusNode, corpusData);
                    corpusBuffer.checkFiches(dataAccess);
                    int ficheCount = corpusBuffer.getAddCount();
                    if (ficheCount == 0) {
                        continue;
                    }
                    ficheAddCount = ficheAddCount + ficheCount;
                    corpusBufferList.add(corpusBuffer);
                }
                if (withMotscles) {
                    for (ThesaurusNode thesaurusNode : baseNode.getThesaurusNodeList()) {
                        ThesaurusURI thesaurusURI = thesaurusNode.getThesaurusURI();
                        ThesaurusData thesaurusData = (ThesaurusData) dataAccess.getScrutariData(thesaurusURI);
                        if (thesaurusData == null) {
                            continue;
                        }
                        ThesaurusBuffer thesaurusBuffer = new ThesaurusBuffer(thesaurusNode, thesaurusData);
                        thesaurusBuffer.checkMotscles(dataAccess);
                        int motcleCount = thesaurusBuffer.getAddCount();
                        if (motcleCount == 0) {
                            continue;
                        }
                        motcleAddCount = motcleAddCount + motcleCount;
                        thesaurusBufferList.add(thesaurusBuffer);
                    }
                }
            }

            private void addContent(XMLWriter xmlWriter, SctFeedOptions feedOptions) throws IOException {
                Lang lang = feedOptions.getLang();
                BaseCheck baseCheck = BaseChecker.check(baseData, lang);
                boolean active = baseNode.isActive();
                xmlWriter.startOpenTag("p");
                xmlWriter.addAttribute("class", "scrutari-Base");
                xmlWriter.endOpenTag();
                insertOpenTag(xmlWriter, active);
                xmlWriter.addText(LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_LONGTITLE, lang, baseData.getBaseURI().getBaseName()));
                insertCloseTag(xmlWriter, active);
                xmlWriter.closeTag("p", false);
                if (!corpusBufferList.isEmpty()) {
                    xmlWriter.startOpenTag("ul");
                    xmlWriter.addAttribute("class", "scrutari-CorpusList");
                    xmlWriter.endOpenTag();
                    for (CorpusBuffer corpusBuffer : corpusBufferList) {
                        corpusBuffer.addContent(xmlWriter, feedOptions, baseCheck);
                    }
                    xmlWriter.closeTag("ul");
                }
                if (!thesaurusBufferList.isEmpty()) {
                    xmlWriter.startOpenTag("ul");
                    xmlWriter.addAttribute("class", "scrutari-ThesaurusList");
                    xmlWriter.endOpenTag();
                    for (ThesaurusBuffer thesaurusBuffer : thesaurusBufferList) {
                        thesaurusBuffer.addContent(xmlWriter, feedOptions);
                    }
                    xmlWriter.closeTag("ul");
                }
            }

        }

    }


    private static class CorpusBuffer {

        private final List<FicheData> ficheDataList = new ArrayList<FicheData>();
        private final CorpusNode corpusNode;
        private final CorpusData corpusData;

        private CorpusBuffer(CorpusNode corpusNode, CorpusData corpusData) {
            this.corpusNode = corpusNode;
            this.corpusData = corpusData;
        }

        private int getAddCount() {
            return ficheDataList.size();
        }

        private void checkFiches(DataAccess dataAccess) {
            for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
                FicheURI ficheURI = ficheNode.getFicheURI();
                FicheData ficheData = (FicheData) dataAccess.getScrutariData(ficheURI);
                if (ficheData == null) {
                    continue;
                }
                ficheDataList.add(ficheData);
            }
        }

        private void addContent(XMLWriter xmlWriter, SctFeedOptions feedOptions, BaseCheck baseCheck) throws IOException {
            boolean corpusActive = corpusNode.isActive();
            xmlWriter.openTag("li");
            xmlWriter.startOpenTag("p");
            xmlWriter.addAttribute("class", "scrutari-Corpus");
            xmlWriter.endOpenTag();
            insertOpenTag(xmlWriter, corpusActive);
            xmlWriter.addText(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_TITLE, feedOptions.getLang(), corpusData.getCorpusURI().getCorpusName()));
            insertCloseTag(xmlWriter, corpusActive);
            xmlWriter.closeTag("p", false);
            xmlWriter.startOpenTag("ul");
            xmlWriter.addAttribute("class", "scrutari-FicheList");
            xmlWriter.endOpenTag();
            int count = 0;
            int max = 100;
            int size = ficheDataList.size();
            if (size < (max + 50)) {
                max = size + 1;
            }
            for (FicheData ficheData : ficheDataList) {
                FeedXmlUtils.addFicheData(xmlWriter, ficheData, feedOptions, baseCheck);
                count++;
                if (count == max) {
                    int remaining = size - count;
                    xmlWriter.openTag("li");
                    xmlWriter.addText("+ " + remaining);
                    xmlWriter.closeTag("li");
                    break;
                }
            }
            xmlWriter.closeTag("ul");
            xmlWriter.closeTag("li");
        }

    }


    private static class ThesaurusBuffer {

        private final List<MotcleData> motcleDataList = new ArrayList<MotcleData>();
        private final ThesaurusNode thesaurusNode;
        private final ThesaurusData thesaurusData;

        private ThesaurusBuffer(ThesaurusNode thesaurusNode, ThesaurusData thesaurusData) {
            this.thesaurusNode = thesaurusNode;
            this.thesaurusData = thesaurusData;
        }

        private int getAddCount() {
            return motcleDataList.size();
        }

        private void checkMotscles(DataAccess dataAccess) {
            for (MotcleNode motcleNode : thesaurusNode.getMotcleNodeList()) {
                MotcleURI motcleURI = motcleNode.getMotcleURI();
                MotcleData motcleData = (MotcleData) dataAccess.getScrutariData(motcleURI);
                if (motcleData == null) {
                    continue;
                }
                motcleDataList.add(motcleData);
            }
        }

        private void addContent(XMLWriter xmlWriter, SctFeedOptions feedOptions) throws IOException {
            boolean thesaurusActive = thesaurusNode.isActive();
            xmlWriter.openTag("li");
            xmlWriter.startOpenTag("p");
            xmlWriter.addAttribute("class", "scrutari-Thesaurus");
            xmlWriter.endOpenTag();
            insertOpenTag(xmlWriter, thesaurusActive);
            xmlWriter.addText(LabelUtils.seekLabelString(thesaurusData.getPhrases(), DataConstants.THESAURUS_TITLE, feedOptions.getLang(), thesaurusData.getThesaurusURI().getThesaurusName()));
            insertCloseTag(xmlWriter, thesaurusActive);
            xmlWriter.closeTag("p", false);
            xmlWriter.startOpenTag("ul");
            xmlWriter.addAttribute("class", "scrutari-MotcleList");
            xmlWriter.endOpenTag();
            int count = 0;
            int max = 100;
            int size = motcleDataList.size();
            if (size < (max + 50)) {
                max = size + 1;
            }
            for (MotcleData motcleData : motcleDataList) {
                FeedXmlUtils.addMotcleData(xmlWriter, motcleData, feedOptions);
                count++;
                if (count == max) {
                    int remaining = size - count;
                    xmlWriter.openTag("li");
                    xmlWriter.addText("+ " + remaining);
                    xmlWriter.closeTag("li");
                    break;
                }
            }
            xmlWriter.closeTag("ul");
            xmlWriter.closeTag("li");
        }

    }

    private static void insertOpenTag(XMLWriter xmlWriter, boolean active) throws IOException {
        if (active) {
            xmlWriter.openTag("strong", false);
        } else {
            xmlWriter.openTag("em", false);
        }
    }

    private static void insertCloseTag(XMLWriter xmlWriter, boolean active) throws IOException {
        if (active) {
            xmlWriter.closeTag("strong", false);
        } else {
            xmlWriter.closeTag("em", false);
        }
    }

}
