/* SctServer - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.xml.feed;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedInfo;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import java.io.IOException;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.xml.AppendableXMLPart;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.scrutari.db.api.DataAccess;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractEntriesXMLPart extends AppendableXMLPart {

    protected final ScrutariSession scrutariSession;
    protected final SctFeedInfo sctFeedInfo;
    protected final SctFeedOptions sctFeedOptions;

    public AbstractEntriesXMLPart(AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession, SctFeedOptions sctFeedOptions) {
        super(xmlWriter);
        this.scrutariSession = scrutariSession;
        this.sctFeedInfo = scrutariSession.getSctFeedInfo();
        this.sctFeedOptions = sctFeedOptions;
    }

    public abstract void appendEntries(DataAccess dataAccess) throws IOException;

    static AbstractEntriesXMLPart newInstance(SctFeedOptions sctFeedOptions, AppendableXMLWriter xmlWriter, ScrutariSession scrutariSession) {
        switch (sctFeedOptions.getSctFeedType()) {
            case SctFeedConstants.TREE_SCTFEED_TYPE:
                return new TreeEntriesXMLPart(xmlWriter, scrutariSession, sctFeedOptions);
            case SctFeedConstants.CATEGORIES_SCTFEED_TYPE:
                return new CategoriesEntriesXMLPart(xmlWriter, scrutariSession, sctFeedOptions);
            case SctFeedConstants.FICHES_SCTFEED_TYPE:
                return new FichesEntriesXMLPart(xmlWriter, scrutariSession, sctFeedOptions);
            case SctFeedConstants.ERRORS_SCTFEED_TYPE:
                return new ErrorsEntriesXMLPart(xmlWriter, scrutariSession, sctFeedOptions);
            default:
                throw new SwitchException("type=" + sctFeedOptions.getSctFeedType());
        }
    }

}
