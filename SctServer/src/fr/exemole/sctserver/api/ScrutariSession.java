/* SctEngine - Copyright (c) 2013-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import fr.exemole.sctserver.api.feed.SctFeedCache;
import fr.exemole.sctserver.api.feed.SctFeedInfo;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultHolder;
import net.scrutari.supermotcle.api.SupermotcleProvider;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariSession extends FicheSearchResultHolder {

    public String getEngineName();

    public SctEngine getEngine();

    public ScrutariDB getScrutariDB();

    public ScrutariDataURIChecker getScrutariDataURIChecker();

    public SupermotcleProvider getSupermotcleProvider();

    public SctFeedInfo getSctFeedInfo();

    public SctFeedCache getSctFeedCache();

    public GlobalSearchOptions getGlobalSearchOptions();

    public FicheSearchResult search(SearchOperation searchOperation, SearchOptions searchOptions, SearchStoragePolicy storagePolicy);

}
