/* SctServer_API - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public interface AttributeDefManager {

    public Set<String> getGroupNameSet();

    public List<AttributeDef> getAttributeDefList(String groupName);

    public AttributeDef getAttributeDef(AttributeKey attributeKey);

}
