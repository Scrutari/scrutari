/* SctServer - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.fieldvariant;

import java.util.List;
import net.mapeadores.util.attr.AttributeKey;


/**
 *
 * @author Vincent Calame
 */
public interface FieldVariant {

    public final static int FICHE_CODEFICHE = 0;
    public final static int FICHE_CODECORPUS = 1;
    public final static int FICHE_CODEBASE = 2;
    public final static int FICHE_BASENAME = 3;
    public final static int FICHE_CORPUSNAME = 4;
    public final static int FICHE_FICHEID = 5;
    public final static int FICHE_TITRE = 6;
    public final static int FICHE_SOUSTITRE = 7;
    public final static int FICHE_HREF = 8;
    public final static int FICHE_LANG = 9;
    public final static int FICHE_YEAR = 10;
    public final static int FICHE_DATE = 11;
    public final static int FICHE_FICHEICON = 12;
    public final static int FICHE_ICON = 13;
    public final static int FICHE_LATITUDE = 14;
    public final static int FICHE_COMPLEMENTS = 15;
    public final static int FICHE_ATTRIBUTES = 16;
    public final static int FICHE_MTITRE = 17;
    public final static int FICHE_MSOUSTITRE = 18;
    public final static int FICHE_MCOMPLEMENTS = 19;
    public final static int FICHE_MATTRIBUTES = 20;
    public final static int FICHE_SCORE = 21;
    public final static int FICHE_CODEMOTCLEARRAY = 22;
    public final static int FICHE_BYTHESAURUSMAP = 23;
    public final static int FICHE_DATEISO = 24;
    public final static int FICHE_AUTHORITY = 25;
    public final static int FICHE_LONGITUDE = 26;
    public final static short IGNORE_TYPE = -1;
    public final static short DEFAULT_TYPE = 0;
    public final static short CONCERNED_TYPE = 1;
    public final static short PRIMARY_TYPE = 2;
    public final static short ALL_TYPE = 3;
    public final static int MOTCLE_CODEMOTCLE = 0;
    public final static int MOTCLE_CODETHESAURUS = 1;
    public final static int MOTCLE_CODEBASE = 2;
    public final static int MOTCLE_BASENAME = 3;
    public final static int MOTCLE_THESAURUSNAME = 4;
    public final static int MOTCLE_MOTCLEID = 5;
    public final static int MOTCLE_LABELS = 6;
    public final static int MOTCLE_ATTRIBUTES = 7;
    public final static int MOTCLE_MLABELS = 8;
    public final static int MOTCLE_SCORE = 9;
    public final static int MOTCLE_FICHECOUNT = 10;

    public String getName();

    public boolean isFicheIdsOnly();

    public short getFicheWithType(int field);

    public short getMotcleWithType(int field);

    public List<Alias> getFicheAliasList();

    public default boolean isFicheWith(int field) {
        return isFicheWith(field, false);
    }

    public default boolean isFicheWith(int field, boolean defaultValue) {
        short type = getFicheWithType(field);
        switch (type) {
            case DEFAULT_TYPE:
                return defaultValue;
            case IGNORE_TYPE:
                return false;
            default:
                return true;
        }
    }

    public default boolean isMotcleWith(int field) {
        return isMotcleWith(field, false);
    }

    public default boolean isMotcleWith(int field, boolean defaultValue) {
        short type = getMotcleWithType(field);
        switch (type) {
            case DEFAULT_TYPE:
                return defaultValue;
            case IGNORE_TYPE:
                return false;
            default:
                return true;
        }
    }

    public static String coreFicheFieldString(int field) {
        switch (field) {
            case FieldVariant.FICHE_CODEFICHE:
                return "codefiche";
            case FieldVariant.FICHE_CODECORPUS:
                return "codecorpus";
            case FieldVariant.FICHE_CODEBASE:
                return "codebase";
            case FieldVariant.FICHE_AUTHORITY:
                return "authority";
            case FieldVariant.FICHE_BASENAME:
                return "basename";
            case FieldVariant.FICHE_CORPUSNAME:
                return "corpusname";
            case FieldVariant.FICHE_FICHEID:
                return "ficheid";
            case FieldVariant.FICHE_TITRE:
                return "titre";
            case FieldVariant.FICHE_SOUSTITRE:
                return "soustitre";
            case FieldVariant.FICHE_HREF:
                return "href";
            case FieldVariant.FICHE_LANG:
                return "lang";
            case FieldVariant.FICHE_YEAR:
                return "year";
            case FieldVariant.FICHE_DATE:
                return "date";
            case FieldVariant.FICHE_DATEISO:
                return "dateiso";
            case FieldVariant.FICHE_FICHEICON:
                return "ficheicon";
            case FieldVariant.FICHE_ICON:
                return "icon";
            case FieldVariant.FICHE_LATITUDE:
                return "latitude";
            case FieldVariant.FICHE_LONGITUDE:
                return "longitude";
            default:
                return "";
        }
    }

    public static int checkCoreFicheFieldString(String name) {
        switch (name) {
            case "codefiche":
                return FieldVariant.FICHE_CODEFICHE;
            case "codecorpus":
                return FieldVariant.FICHE_CODECORPUS;
            case "codebase":
                return FieldVariant.FICHE_CODEBASE;
            case "authority":
                return FieldVariant.FICHE_AUTHORITY;
            case "basename":
                return FieldVariant.FICHE_BASENAME;
            case "corpusname":
                return FieldVariant.FICHE_CORPUSNAME;
            case "ficheid":
                return FieldVariant.FICHE_FICHEID;
            case "titre":
                return FieldVariant.FICHE_TITRE;
            case "soustitre":
                return FieldVariant.FICHE_SOUSTITRE;
            case "href":
                return FieldVariant.FICHE_HREF;
            case "lang":
                return FieldVariant.FICHE_LANG;
            case "year":
            case "annee":
                return FieldVariant.FICHE_YEAR;
            case "date":
                return FieldVariant.FICHE_DATE;
            case "dateiso":
                return FieldVariant.FICHE_DATEISO;
            case "ficheicon":
                return FieldVariant.FICHE_FICHEICON;
            case "icon":
                return FieldVariant.FICHE_ICON;
            case "latitude":
            case "lat":
                return FieldVariant.FICHE_LATITUDE;
            case "longitude":
            case "lon":
                return FieldVariant.FICHE_LONGITUDE;
            default:
                return -1;
        }
    }


    public static interface Alias {

        public String getName();

        public String getSeparator();

        public List<FieldReference> getFieldReferenceList();

        public boolean isArray();

    }


    public static interface FieldReference {

    }


    public static interface CoreFieldReference extends FieldReference {

        public int getField();

    }


    public static interface ComplementFieldReference extends FieldReference {

        public int getComplementNumber();

    }


    public static interface AttributeFieldReference extends FieldReference {

        public AttributeKey getAttributeKey();

    }

}
