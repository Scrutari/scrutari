/* SctServer_API - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import fr.exemole.sctserver.api.context.EngineGroup;
import net.mapeadores.util.buildinfo.BuildInfo;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface EngineContext {

    public BuildInfo getBuildInfo();

    public SctEngine getEngine(String engineName);

    /**
     * Chaine vide si non défini.
     *
     * @return
     */
    public String getWebappCanonicalUrl();

    public Lang getWebappDefaultLang();

    public EngineGroup getEngineGroup(String groupName);

    public MessageLocalisation getMessageLocalisation(LangPreference langPreference);

    public DocStream getResourceDocStream(RelativePath path, SctEngine sctEngine);

    public ResourceStorages getResourceStorages();

}
