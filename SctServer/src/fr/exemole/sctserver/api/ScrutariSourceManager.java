/* SctServer_API - Copyright (c) 2008-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import fr.exemole.sctserver.api.collect.ScrutariSource;
import java.util.List;
import net.mapeadores.util.logging.MultiMessageHandler;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariSourceManager {

    public final static int LOCAL = 1;
    public final static int CHECK = 2;
    public final static int FORCE = 3;

    public ScrutariSource getScrutariSouce(String scrutariSourceName);

    public List<ScrutariSource> getScrutariSourceList();

    public boolean collect(MultiMessageHandler messageHandler, int level);

    public void collect(String sourceName, boolean force);

}
