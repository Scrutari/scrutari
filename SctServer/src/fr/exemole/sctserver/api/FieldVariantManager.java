/* SctServer_API - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface FieldVariantManager {

    public final static String EMPTY_VARIANT = "empty";
    public final static String QUERY_VARIANT = "query";
    public final static String DATA_VARIANT = "data";
    public final static String GEO_VARIANT = "geo";

    public FieldVariant getFieldVariant(String name);

    public List<FieldVariant> getFieldVariantList(boolean withDefault);

}
