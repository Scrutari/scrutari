/* SctServer_API - Copyright (c) 2011-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.feed;


/**
 *
 * @author Vincent Calame
 */
public interface SctFeedConstants {

    public final static short ATOM_FEED = 1;
    public final static short TREE_SCTFEED_TYPE = 1;
    public final static short CATEGORIES_SCTFEED_TYPE = 2;
    public final static short FICHES_SCTFEED_TYPE = 3;
    public final static short ERRORS_SCTFEED_TYPE = 4;
    public final static int INDENT_OPTION = 1;
    public final static int TECHNICALTITLE_OPTION = 2;
    public final static int ICON_OPTION = 4;
    public final static int SOUSTITRE_OPTION = 8;
    public final static int FICHEONLY_OPTION = 16;
}
