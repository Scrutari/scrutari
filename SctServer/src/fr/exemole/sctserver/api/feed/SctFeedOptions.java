/* SctServer_API - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.feed;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public interface SctFeedOptions {

    public short getSctFeedType();

    public Lang getLang();

    public int getOptionMask();

    public RequestMap getRequestMap();

    public String getSelfUrl();

}
