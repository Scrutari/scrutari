/* SctServer_API - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.conf;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import java.util.Map;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.AttributeDef;
import net.scrutari.data.DataValidator;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.db.api.CodeMatch;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.PertinencePonderation;


/**
 *
 * @author Vincent Calame
 */
public interface EngineConf {

    public ConfFileInfo getConfFileInfo(String name);

    public void reload() throws ConfIOException;

    @Nullable
    public ScrutariSourceDef[] getScrutariSourceDefArray();

    @Nullable
    public EngineMetadata getEngineMetadata();

    /**
     * Doit être nul ou contenir au moins un élément portant le nom _default.
     */
    @Nullable
    public CategoryDef[] getCategoryDefArray();

    @Nullable
    public PertinencePonderation getGlobalPertinencePonderation();

    @Nullable
    public String[] getPertinenceOrderArray();

    @Nullable
    public CodeMatch[] getInitialCodeMatchArray(ScrutariDataURIChecker checker);

    @Nullable
    public Map<String, AttributeDef[]> getAttributeDefArrayMap();

    @Nullable
    public DataValidator getDataValidator();

    @Nullable
    public Map<String, FieldVariant> getFieldVariantMap();

}
