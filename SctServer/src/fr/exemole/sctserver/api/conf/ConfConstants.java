/* SctServer_API - Copyright (c) 2009-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.conf;


/**
 *
 * @author Vincent Calame
 */
public interface ConfConstants {

    public final static short GLOBAL_CONFIO_ERROR = -1;
    public final static short GLOBAL_UNDEFINED = 0;
    public final static short GLOBAL_OK = 1;
    public final static short FILE_STATE_UNKNOWN = 0;
    public final static short FILE_MISSING = 1;
    public final static short FILE_XML_MALFORMED = 2;
    public final static short FILE_XML_ERROR = 3;
    public final static short FILE_XML_WARNING = 4;
    public final static short FILE_OK = 5;
    public static String SOURCES_NAME = "sources";
    public static String METADATA_NAME = "metadata";
    public static String CATEGORIES_NAME = "categories";
    public static String URI_CODES_NAME = "uri-codes";
    public static String ATTRIBUTES_NAME = "attributes";
    public static String OPTIONS_NAME = "options";
    public static String VALIDATION_NAME = "validation";
    public static String FIELDS_NAME = "fields";
}
