/* SctServer_API - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.conf;

import net.mapeadores.util.logging.MessageSource;


/**
 *
 * @author Vincent Calame
 */
public interface ConfFileInfo extends MessageSource {

    public boolean isHere();

}
