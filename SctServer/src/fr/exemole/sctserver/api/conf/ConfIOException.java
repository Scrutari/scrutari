/* SctServer_API - Copyright (c) 2009 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.conf;


/**
 *
 * @author Vincent Calame
 */
public class ConfIOException extends Exception {

    private String cause = "";

    public ConfIOException(String message) {
        super(message);
    }

    public ConfIOException(Exception exception) {
        super(exception);
        cause = exception.getClass().getSimpleName();
    }

    public String getFullMessage() {
        if (cause.length() > 0) {
            return cause + " - " + getMessage();
        }
        return getMessage();
    }

}
