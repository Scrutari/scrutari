/* SctServer_API - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.log;


/**
 *
 * @author Vincent Calame
 */
public interface QLog {

    public String getQ();

    public String getOrigin();

    public int getFicheCount();

    /**
     * Retourne -1 si il n'y a pas eu de filtre sur les fiches
     */
    public int getFicheMaximum();

}
