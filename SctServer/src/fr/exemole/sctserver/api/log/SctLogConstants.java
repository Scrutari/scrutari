/* SctServer_API - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.log;


/**
 *
 * @author Vincent Calame
 */
public interface SctLogConstants {

    public final static String CONF_DIR = "conf";
    public final static String LOAD_DIR = "load";
    public final static String EXCEPTION_DIR = "exception";
    public final static String DATA_DIR = "data";
    public final static String COLLECT_DIR = "collect";
}
