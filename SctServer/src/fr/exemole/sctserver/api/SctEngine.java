/* SctServer_API - Copyright (c) 2005-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import fr.exemole.sctserver.api.storage.EngineStorage;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.jslib.JsAnalyser;


/**
 *
 * @author Vincent Calame
 */
public interface SctEngine {

    public String getEngineName();

    public EngineMetadata getEngineMetadata();

    public EngineContext getEngineContext();

    public boolean reloadConf();

    public boolean reload(boolean force);

    public boolean update();

    public ScrutariSession getScrutariSession();

    public ScrutariSourceManager getScrutariSourceManager();

    public FieldVariantManager getFieldVariantManager();

    public AttributeDefManager getAttributeDefManager();

    public EngineStorage getEngineStorage();

    public ConfState getConfState();

    public JsAnalyser getJsAnalyser();

    public ResourceStorages getResourceStorages();

    public default String getCanonicalUrl() {
        return getEngineContext().getWebappCanonicalUrl() + getEngineName() + "/";
    }

}
