/* SctServer_API - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.collect;

import java.util.List;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariInfoContent {

    public FuzzyDate getDate();

    /**
     * Instance de URL ou de String
     *
     * @return
     */
    public List<Object> getDataFragmentList();

}
