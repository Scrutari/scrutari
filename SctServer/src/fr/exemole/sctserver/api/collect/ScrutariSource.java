/* SctServer_API - Copyright (c) 2008-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.collect;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariSource extends CollectResult {

    public ScrutariSourceDef getScrutariSourceDef();

}
