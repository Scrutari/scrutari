/* SctServer_API - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.collect;

import java.net.URL;
import java.util.List;
import java.util.Map;
import net.scrutari.datauri.BaseURI;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariSourceDef {

    public final static short INFO_TYPE = 1;
    public final static short SCRUTARIDATA_TYPE = 2;

    public String getName();

    public List<UrlEntry> getUrlEntryList();

    /**
     * Peut être nul.
     *
     * @return
     */
    public BaseURI getMainBaseURI();

    public List<BaseURI> getAliasList();


    public interface UrlEntry {

        public URL getUrl();

        public short getType();

        public int getFrequency();

        public String getTransformation();

        public Map<String, String> getTransformationParameters();

    }

}
