/* SctServer_API - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.collect;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface CollectResult {

    public static final String OK_STATE = "ok";
    public static final String UNKNOWN_STATE = "unknown";
    public static final String INFO_NOCONNECTION_STATE = "info_noconnection";
    public static final String INFO_BROKENURL_STATE = "info_brokenurl";
    public static final String INFO_DOWNLOADERROR_STATE = "info_downloaderror";
    public static final String INFO_NOTXML_STATE = "info_notxml";
    public static final String INFO_WRONGXML_STATE = "info_wrongxml";
    public static final String DATA_NOCONNECTION_STATE = "data_noconnection";
    public static final String DATA_BROKENURL_STATE = "data_brokenurl";
    public static final String DATA_DOWNLOADERROR_STATE = "data_downloaderror";
    public static final String DATA_TRANSFORMATION_STATE = "data_transformation";
    public static final String DATA_NOTXML_STATE = "data_notxml";

    public String getState();

    @Nullable
    public FuzzyDate getLastUpdate();

}
