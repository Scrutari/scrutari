/* SctServer_API - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.storage;

import net.scrutari.db.api.LexieAccess;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.result.FicheSearchResult;


/**
 *
 * @author Vincent Calame
 */
public interface FicheSearchCache {

    public FicheSearchResult getFicheSearchResult(QId qId, LexieAccess lexieAccess);

    public void saveFicheSearchResult(FicheSearchResult ficheSearchResult);

}
