/* SctServer_API - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.storage;

import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchSource;


/**
 *
 * @author Vincent Calame
 */
public interface QIdStorage {

    public QId containsQId(CanonicalQ canonicalQ, SearchOptionsDef searchOptionsDef);

    public FicheSearchSource getFicheSearchSource(QId qId);

    public void saveFicheSearchSource(FicheSearchSource ficheSearchSource);

    public int getMaxSubId(ScrutariDBName scrutariDBName);

}
