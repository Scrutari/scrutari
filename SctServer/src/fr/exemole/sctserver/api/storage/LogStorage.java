/* SctServer_API - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.storage;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.log.QLog;
import java.io.PrintWriter;
import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface LogStorage {

    public void log(ScrutariSession scrutariSession, QLog qLog);

    public void log(String logDir, String name, @Nullable String content);

    public void removeDir(String logDir);

    public PrintWriter getPrintWriter(String logDir, String name);

    public FuzzyDate[] getDateArray(short dateType, FuzzyDate dateFilter);

    public String getQLog(FuzzyDate date);

    public String getLog(String logDir, String name);

    public List<String> getAvailableLogs(String logDir);

}
