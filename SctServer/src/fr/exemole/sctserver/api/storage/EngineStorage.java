/* SctServer - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.storage;

import fr.exemole.sctserver.api.conf.EngineConf;
import fr.exemole.sctserver.api.feed.SctFeedCache;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.io.ResourceStorage;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.caches.DataCacheReaderFactory;
import net.scrutari.db.api.caches.DataCacheWriter;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheWriter;


/**
 *
 * @author Vincent Calame
 */
public interface EngineStorage {

    public final static String INFOS_NAME = "infos";
    public final static String STATUS_NAME = "status";


    public List<StoredData> getDataList(String scrutariSourceName);

    public boolean hasData(String scrutariSourceName, URL url);

    public void saveScrutariData(String scrutariSourceName, List<MatchedData> matchedDataList);

    public boolean hasConfBaseMetadata(String scrutariSourceName);

    public InputStream getConfBaseMetadataInputstream(String scrutariSourceName);

    public String getContent(String contentName);

    public void saveContent(String contentName, String content);

    public EngineConf getEngineConf();

    public void saveWholeURITree(URITree uriTree);

    public void saveAddedURITree(URITree uriTree, ScrutariDBName scrutariDBName);

    public URITree getWholeURITree(ScrutariDataURIChecker checker);

    public URITree getAddedURITree(ScrutariDBName scrutariDBName, ScrutariDataURIChecker checker);

    /**
     * Dates de la plus récente à la plus ancienne
     */
    public ScrutariDBName[] getAddScrutariDBNameArray();

    public QIdStorage getQIdStorage();

    public LogStorage getLogStorage();

    public DataCacheWriter getDataCacheWriter(ScrutariDBName scrutariDbName);

    public LexieCacheWriter getLexieCacheWriter(ScrutariDBName scrutariDbName);

    public SctFeedCache getSctFeedCache(ScrutariDBName scrutariDbName);

    public FicheSearchCache getFicheSearchCache(ScrutariDBName scrutariDbName);

    public void cacheObject(ScrutariDBName scrutariDbName, Object obj);

    public Object getCachedObject(ScrutariDBName scrutariDbName, Class objectClass);

    public void cleanCache(Set<ScrutariDBName> remainingSet);

    public void cleanTransientCache(ScrutariDBName scrutariDbName);

    public ResourceStorage getEngineResourceStorage();

    /**
     *
     * @param currentVersion peut être nul
     */
    public CacheCheck checkLastCache(ScrutariDBName currentName);


    public interface CacheCheck {

        public ScrutariDBName getScrutariDBName();

        public DataCacheReaderFactory getDataCacheReaderFactory();

        public LexieCacheReaderFactory getLexieCacheReaderFactory();

    }


    public interface MatchedData {

        public InputStream getInputStream() throws IOException;

        public URL getMatchingURL();

        public boolean isNotChanged();

    }


    public interface StoredData {

        public InputStream getInputStream() throws IOException;

        public boolean hasBackup();

        public boolean restoreBackup() throws IOException;

        public void backup() throws IOException;

    }

}
