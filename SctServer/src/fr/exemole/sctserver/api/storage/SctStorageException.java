/* SctServer - Copyright (c) 2008 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api.storage;

import java.io.File;


/**
 *
 * @author Vincent Calame
 */
public class SctStorageException extends RuntimeException {

    public SctStorageException() {
    }

    public SctStorageException(String message) {
        super(message);
    }

    public SctStorageException(Throwable throwable) {
        super(throwable);
    }

    public SctStorageException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public SctStorageException(File f, Throwable throwable) {
        super("file:" + f.getAbsolutePath(), throwable);
    }

}
