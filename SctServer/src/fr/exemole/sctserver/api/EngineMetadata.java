/* SctServer_API - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.api;

import java.net.URL;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;


/**
 *
 * @author Vincent Calame
 */
public interface EngineMetadata {

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public Phrases getPhrases();

    public URL getIcon();

    public URL getWebsite();

    public Lang getDefaultLang();

}
