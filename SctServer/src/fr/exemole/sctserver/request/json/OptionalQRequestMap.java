/* SctServer - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.request.Parameters;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import net.mapeadores.util.request.FileValue;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
class OptionalQRequestMap implements RequestMap {

    private final RequestMap requestMap;
    private final Set<String> parameterNameSet = new HashSet<String>();
    private final boolean optionalQ;

    OptionalQRequestMap(RequestMap requestMap) {
        this.requestMap = requestMap;
        parameterNameSet.addAll(requestMap.getParameterNameSet());
        boolean missingQ = false;
        if (!parameterNameSet.contains(Parameters.Q_ID)) {
            if (!parameterNameSet.contains(Parameters.Q)) {
                boolean noPrefix = true;
                for (String param : parameterNameSet) {
                    if (param.startsWith(Parameters.Q_PREFIX)) {
                        noPrefix = false;
                        break;
                    }
                }
                if (noPrefix) {
                    missingQ = true;
                }
            }
        }
        optionalQ = missingQ;
        if (missingQ) {
            parameterNameSet.add(Parameters.Q);
            parameterNameSet.add(Parameters.Q_MODE);
        }
    }

    @Override
    public FileValue getFileValue(String name) {
        return requestMap.getFileValue(name);
    }

    @Override
    public FileValue[] getFileValues(String name) {
        return requestMap.getFileValues(name);
    }

    @Override
    public String getParameter(String name) {
        if (name.equals(Parameters.Q)) {
            if (optionalQ) {
                return "*";
            } else {
                return requestMap.getParameter(name);
            }
        } else if (name.equals(Parameters.Q_MODE)) {
            if (optionalQ) {
                return "operation";
            } else {
                return requestMap.getParameter(name);
            }
        } else {
            return requestMap.getParameter(name);
        }
    }

    @Override
    public String[] getParameterValues(String name) {
        return requestMap.getParameterValues(name);
    }

    @Override
    public Set<String> getParameterNameSet() {
        return parameterNameSet;
    }

    @Override
    public Locale[] getAcceptableLocaleArray() {
        return requestMap.getAcceptableLocaleArray();
    }

    @Override
    public Object getSourceObject() {
        return requestMap.getSourceObject();
    }

}
