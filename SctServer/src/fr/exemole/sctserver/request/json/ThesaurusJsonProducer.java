/* SctServer  - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.json.thesaurusdata.ThesaurusDataJson_A;
import fr.exemole.sctserver.json.thesaurusdata.ThesaurusDataJson_B;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final ScrutariDB scrutariDB;
    private final Lang lang;

    public ThesaurusJsonProducer(int version, ScrutariDB scrutariDB, Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.version = version;
        this.scrutariDB = scrutariDB;
        this.lang = lang;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            if (version >= 3) {
                writeVersion3(jsonWriter, dataAccess);
            } else if (version == 2) {
                writeVersion2(jsonWriter, dataAccess);
            } else {
                writeVersion1(jsonWriter, dataAccess);
            }
            writeWarnings(jsonWriter);
            jsonWriter.endObject();
        }
    }

    private void writeVersion3(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        jsonWriter.key("thesaurusArray");
        jsonWriter.array();
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            ThesaurusDataJson_B.object(jsonWriter, thesaurusData, lang, scrutariDB.getStats());
        }
        jsonWriter.endArray();
    }

    private void writeVersion2(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        jsonWriter.key("thesaurusArray");
        jsonWriter.array();
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            ThesaurusDataJson_A.object(jsonWriter, thesaurusData, lang, scrutariDB.getStats());
        }
        jsonWriter.endArray();
    }

    private void writeVersion1(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        List<ThesaurusData> thesaurusDataList = dataAccess.getThesaurusDataList();
        jsonWriter.key("thesaurusData");
        jsonWriter.object();
        jsonWriter.key("thesaurusCount");
        jsonWriter.value(thesaurusDataList.size());
        if (!thesaurusDataList.isEmpty()) {
            jsonWriter.key("thesaurusArray");
            jsonWriter.array();
            for (ThesaurusData thesaurusData : thesaurusDataList) {
                ThesaurusDataJson_A.object(jsonWriter, thesaurusData, lang, scrutariDB.getStats());
            }
            jsonWriter.endArray();
        }
        jsonWriter.endObject();
    }


}
