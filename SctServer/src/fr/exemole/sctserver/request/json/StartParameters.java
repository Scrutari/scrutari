/* SctServer - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public class StartParameters {

    public final static short NONE_TYPE = 0;
    public final static short GLOBAL_TYPE = 1;
    public final static short IN_ALL_TYPE = 2;
    public final static short IN_SELECTION_TYPE = 3;
    public final static StartParameters NONE = new StartParameters(-1, -1, NONE_TYPE, null);
    private final int start;
    private final int limit;
    private final short type;
    private final Set<Integer> rankSet;

    private StartParameters(int start, int limit, short type, Set<Integer> rankSet) {
        this.start = start;
        this.limit = limit;
        this.type = type;
        this.rankSet = rankSet;
    }

    public int getStart() {
        return start;
    }

    public int getLimit() {
        return limit;
    }

    public short getType() {
        return type;
    }

    public boolean containsCategory(int rank) {
        if (rankSet == null) {
            throw new IllegalStateException();
        }
        return rankSet.contains(rank);
    }

    public static StartParameters newGlobalInstance(int start, int limit) {
        if (start < 1) {
            throw new IllegalArgumentException("start < 1");
        }
        if (limit < 1) {
            limit = -1;
        }
        return new StartParameters(start, limit, GLOBAL_TYPE, null);
    }

    public static StartParameters newInAllInstance(int start, int limit) {
        if (start < 1) {
            throw new IllegalArgumentException("start < 1");
        }
        if (limit < 1) {
            limit = -1;
        }
        return new StartParameters(start, limit, IN_ALL_TYPE, null);
    }

    public static StartParameters newInSelectionInstance(int start, int limit, Set<Integer> rankSet) {
        if (start < 1) {
            throw new IllegalArgumentException("start < 1");
        }
        if (limit < 1) {
            limit = -1;
        }
        return new StartParameters(start, limit, IN_SELECTION_TYPE, rankSet);
    }

}
