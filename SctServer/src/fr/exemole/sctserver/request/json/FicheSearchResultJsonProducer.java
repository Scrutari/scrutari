/* SctServer - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.MotcleWriter;
import fr.exemole.sctserver.json.engineinfo.EngineInfoJson_C;
import fr.exemole.sctserver.json.engineinfo.InfoOptions;
import fr.exemole.sctserver.json.fichedata.FicheDataJson_A;
import fr.exemole.sctserver.json.fichedata.FicheDataJson_B;
import fr.exemole.sctserver.json.fichesearch.BythesaurusMapJson_A;
import fr.exemole.sctserver.json.fichesearch.CodemotcleArrayJson_A;
import fr.exemole.sctserver.json.fichesearch.GroupTypeJson_A;
import fr.exemole.sctserver.json.fichesearch.GroupTypeJson_B;
import fr.exemole.sctserver.json.fichesearch.InsertOptions;
import fr.exemole.sctserver.json.fichesearch.IntitulesJson_A;
import fr.exemole.sctserver.json.fichesearch.IntitulesJson_B;
import fr.exemole.sctserver.json.fichesearch.ResultGroupJson_A;
import fr.exemole.sctserver.json.fichesearch.ResultGroupJson_B;
import fr.exemole.sctserver.json.fichesearch.SearchMetaJson_A;
import fr.exemole.sctserver.json.fichesearch.SearchMetaJson_B;
import fr.exemole.sctserver.json.fichesearch.SearchMetaJson_C;
import fr.exemole.sctserver.tools.EngineUtils;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.db.api.FieldRankManager;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchResultJsonProducer extends AbstractJsonProducer {

    private final static InfoOptions INFO_OPTIONS;
    private final int version;
    private final ScrutariSession scrutariSession;
    private final AttributeDefManager attributeDefManager;
    private final FieldRankManager fieldRankManager;
    private final FicheSearchResult searchResult;
    private final FieldVariant fieldVariant;
    private final InsertOptions insertOptions;
    private final Lang lang;
    private final StartParameters startParameters;
    private final short codemotcleType;
    private final short bythesaurusType;
    private final BaseChecker baseChecker;

    static {
        INFO_OPTIONS = new InfoOptions();
        INFO_OPTIONS.setAll(true);
        INFO_OPTIONS.setWith(InfoOptions.STATS, false);
    }

    public FicheSearchResultJsonProducer(int version, FicheSearchResult searchResult, ScrutariSession scrutariSession, FieldVariant fieldVariant, InsertOptions insertOptions, StartParameters startParameters) {
        Lang searchLang = searchResult.getFicheSearchSource().getSearchOptionsDef().getLang();
        if (searchLang == null) {
            this.lang = EngineUtils.getDefaultLang(scrutariSession);
        } else {
            this.lang = searchLang;
        }
        this.version = version;
        this.searchResult = searchResult;
        this.fieldVariant = fieldVariant;
        this.insertOptions = insertOptions;
        this.startParameters = startParameters;
        this.scrutariSession = scrutariSession;
        this.attributeDefManager = scrutariSession.getEngine().getAttributeDefManager();
        this.fieldRankManager = scrutariSession.getScrutariDB().getFieldRankManager();
        this.codemotcleType = fieldVariant.getFicheWithType(FieldVariant.FICHE_CODEMOTCLEARRAY);
        this.bythesaurusType = fieldVariant.getFicheWithType(FieldVariant.FICHE_BYTHESAURUSMAP);
        this.baseChecker = new BaseChecker(this.lang);
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            Set<Integer> corpusCodeSet = new LinkedHashSet<Integer>();
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            jsonWriter.key("ficheSearchResult");
            jsonWriter.object();
            if (insertOptions.isWith(InsertOptions.SEARCHMETA)) {
                if (version >= 3) {
                    jsonWriter.key("searchMeta");
                    jsonWriter.object();
                    SearchMetaJson_C.properties(jsonWriter, dataAccess, searchResult, lang);
                    jsonWriter.endObject();
                } else if (version == 2) {
                    SearchMetaJson_B.properties(jsonWriter, dataAccess, searchResult, lang);
                } else {
                    SearchMetaJson_A.properties(jsonWriter, searchResult, lang);
                }
            }
            if (version >= 1) {
                GroupTypeJson_B.properties(jsonWriter, scrutariSession.getGlobalSearchOptions());
            } else {
                GroupTypeJson_A.properties(jsonWriter, scrutariSession.getGlobalSearchOptions());
            }
            jsonWriter.key("ficheGroupArray");
            jsonWriter.array();
            switch (startParameters.getType()) {
                case StartParameters.NONE_TYPE:
                    writeNoneStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
                case StartParameters.GLOBAL_TYPE:
                    writeGlobalStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
                case StartParameters.IN_ALL_TYPE:
                    writeInAllStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
                case StartParameters.IN_SELECTION_TYPE:
                    writeInSelectionStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
            }
            jsonWriter.endArray();
            if (insertOptions.isWith(InsertOptions.MOTCLEARRAY)) {
                if (version >= 3) {
                    writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getBVersion(jsonWriter, dataAccess, fieldVariant, lang));
                } else {
                    writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getAVersion(jsonWriter, dataAccess, fieldVariant, lang));
                }
            }
            if (insertOptions.isWith(InsertOptions.ENGINEINFO)) {
                if (version >= 3) {
                    EngineInfoJson_C.properties(jsonWriter, dataAccess, scrutariSession, lang, INFO_OPTIONS);
                } else if (version >= 1) {
                    IntitulesJson_B.properties(jsonWriter, fieldVariant, dataAccess, lang, corpusCodeSet, scrutariSession.getEngine().getAttributeDefManager());
                } else {
                    IntitulesJson_A.properties(jsonWriter, fieldVariant, dataAccess, lang, corpusCodeSet, scrutariSession.getEngine().getAttributeDefManager());
                }
            }
            jsonWriter.endObject();
            writeWarnings(jsonWriter);
            jsonWriter.endObject();
        }
    }


    private void writeNoneStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            startGroup(jsonWriter, ficheSearchResultGroup);
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupCount = infoList.size();
            for (int j = 0; j < ficheByGroupCount; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
            }
            endGroup(jsonWriter);
        }
    }

    private void writeGlobalStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        int globalStartIndex = startParameters.getStart() - 1;
        int limit = startParameters.getLimit();
        boolean withLimit = (limit > 0);
        boolean limitStop = false;
        int ficheIncrement = 0;
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupCount = infoList.size();
            int startIndex = 0;
            if (ficheIncrement == 0) {
                if (ficheByGroupCount < globalStartIndex) {
                    globalStartIndex = globalStartIndex - ficheByGroupCount;
                    continue;
                } else {
                    startIndex = globalStartIndex;
                }
            }
            startGroup(jsonWriter, ficheSearchResultGroup);
            for (int j = startIndex; j < ficheByGroupCount; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
                if (withLimit) {
                    ficheIncrement++;
                    if (ficheIncrement == limit) {
                        limitStop = true;
                        break;
                    }
                }
            }
            endGroup(jsonWriter);
            if (limitStop) {
                break;
            }
        }
    }

    private void writeInAllStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        int startIndex = startParameters.getStart() - 1;
        int limit = startParameters.getLimit();
        boolean withLimit = (limit > 0);
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupCount = infoList.size();
            if (ficheByGroupCount < startIndex) {
                continue;
            }
            startGroup(jsonWriter, ficheSearchResultGroup);
            int min = ficheByGroupCount;
            if (withLimit) {
                min = Math.min(ficheByGroupCount, startIndex + limit);
            }
            for (int j = startIndex; j < min; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
            }
            endGroup(jsonWriter);
        }
    }

    private void writeInSelectionStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        int startIndex = startParameters.getStart() - 1;
        int limit = startParameters.getLimit();
        boolean withLimit = (limit > 0);
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            if (!startParameters.containsCategory(ficheSearchResultGroup.getCategoryRank())) {
                continue;
            }
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupCount = infoList.size();
            if (ficheByGroupCount < startIndex) {
                continue;
            }
            startGroup(jsonWriter, ficheSearchResultGroup);
            int min = ficheByGroupCount;
            if (withLimit) {
                min = Math.min(ficheByGroupCount, startIndex + limit);
            }
            for (int j = startIndex; j < min; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
            }
            endGroup(jsonWriter);
        }
    }

    private void startGroup(JSONWriter jsonWriter, FicheSearchResultGroup ficheSearchResultGroup) throws IOException {
        jsonWriter.object();
        if (version >= 1) {
            ResultGroupJson_B.properties(jsonWriter, ficheSearchResultGroup, scrutariSession.getGlobalSearchOptions(), lang);
        } else {
            ResultGroupJson_A.properties(jsonWriter, ficheSearchResultGroup, scrutariSession.getGlobalSearchOptions(), lang);
        }
        jsonWriter.key("ficheArray");
        jsonWriter.array();
    }

    private void endGroup(JSONWriter jsonWriter) throws IOException {
        jsonWriter.endArray();
        jsonWriter.endObject();
    }

    private void writeFicheSearchResultInfo(JSONWriter jsonWriter, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        Integer ficheCode = ficheSearchResultInfo.getCode();
        if ((fieldVariant.isFicheIdsOnly()) && (version >= 3)) {
            CorpusData corpusData = dataAccess.getFicheInfo(ficheCode).getCorpusData();
            corpusCodeSet.add(corpusData.getCorpusCode());
            jsonWriter.object();
            FicheDataJson_A.properties(jsonWriter, ficheCode, corpusData, fieldVariant);
            writeCodemotcleArray(jsonWriter, ficheCode, ficheSearchResultInfo, dataAccess);
            writeBythesaurusMap(jsonWriter, ficheCode, ficheSearchResultInfo, dataAccess);
            jsonWriter.endObject();
        } else {
            FicheData ficheData = dataAccess.getFicheData(ficheCode);
            corpusCodeSet.add(ficheData.getCorpusCode());
            BaseCheck baseCheck = baseChecker.getBaseCheck(dataAccess, ficheData);
            jsonWriter.object();
            if (version >= 3) {
                writeFicheVersion3(jsonWriter, ficheData, ficheSearchResultInfo, baseCheck);
            } else {
                writeFicheVersion1(jsonWriter, ficheData, ficheSearchResultInfo, baseCheck);
            }
            writeCodemotcleArray(jsonWriter, ficheCode, ficheSearchResultInfo, dataAccess);
            writeBythesaurusMap(jsonWriter, ficheCode, ficheSearchResultInfo, dataAccess);
            jsonWriter.endObject();
        }
    }

    private void writeFicheVersion3(JSONWriter jsonWriter, FicheData ficheData, FicheSearchResultInfo ficheSearchResultInfo, BaseCheck baseCheck) throws IOException {
        FicheDataJson_B.properties(jsonWriter, ficheData, fieldVariant, baseCheck, ficheSearchResultInfo, attributeDefManager, fieldRankManager);
    }

    private void writeFicheVersion1(JSONWriter jsonWriter, FicheData ficheData, FicheSearchResultInfo ficheSearchResultInfo, BaseCheck baseCheck) throws IOException {
        FicheDataJson_A.properties(jsonWriter, ficheData, fieldVariant, baseCheck, ficheSearchResultInfo, attributeDefManager, fieldRankManager);
    }

    private void writeCodemotcleArray(JSONWriter jsonWriter, Integer ficheCode, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess) throws IOException {
        switch (codemotcleType) {
            case FieldVariant.ALL_TYPE:
                CodemotcleArrayJson_A.properties(jsonWriter, ficheCode, dataAccess, null);
                break;
            case FieldVariant.IGNORE_TYPE:
                break;
            default:
                CodemotcleArrayJson_A.properties(jsonWriter, ficheSearchResultInfo);
        }
    }

    private void writeBythesaurusMap(JSONWriter jsonWriter, Integer ficheCode, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess) throws IOException {
        switch (bythesaurusType) {
            case FieldVariant.ALL_TYPE:
                BythesaurusMapJson_A.properties(jsonWriter, ficheCode, dataAccess, null);
                break;
            case FieldVariant.CONCERNED_TYPE:
                BythesaurusMapJson_A.properties(jsonWriter, ficheSearchResultInfo, dataAccess);
                break;
        }
    }

    private void writeMotcleArray(JSONWriter jsonWriter, DataAccess dataAccess, MotcleWriter motcleWriter) throws IOException {
        Set<Integer> allMotcleSet = getAllMotcleSet(dataAccess);
        jsonWriter.key("motcleArray");
        jsonWriter.array();
        for (MotcleSearchResultInfo motcleSearchResultInfo : searchResult.getMotcleSearchResultInfoList()) {
            motcleWriter.object(motcleSearchResultInfo);
            if (allMotcleSet != null) {
                allMotcleSet.remove(motcleSearchResultInfo.getCode());
            }
        }
        if ((allMotcleSet != null) && (allMotcleSet.size() > 0)) {
            for (Integer motcleCode : allMotcleSet) {
                motcleWriter.object(motcleCode);
            }
        }
        jsonWriter.endArray();
    }

    private Set<Integer> getAllMotcleSet(DataAccess dataAccess) {
        if ((codemotcleType != FieldVariant.ALL_TYPE) && (bythesaurusType != FieldVariant.ALL_TYPE)) {
            return null;
        }
        int motcleCount = scrutariSession.getScrutariDB().getStats().getEngineCountStats().getMotcleCount();
        Set<Integer> allMotcleSet = new HashSet<Integer>();
        boolean full = false;
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            for (FicheSearchResultInfo ficheResultInfo : ficheSearchResultGroup.getFicheSearchResultInfoList()) {
                FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheResultInfo.getCode());
                for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                    allMotcleSet.addAll(ficheInfo.getIndexationCodeList(thesaurusData.getThesaurusCode()));
                }
                if (allMotcleSet.size() == motcleCount) {
                    full = true;
                    break;
                }
            }
            if (full) {
                break;
            }
        }
        return allMotcleSet;
    }

}
