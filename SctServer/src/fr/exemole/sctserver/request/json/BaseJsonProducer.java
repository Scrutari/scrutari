/* SctServer - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.json.basedata.BaseDataJson_A;
import fr.exemole.sctserver.json.basedata.BaseDataJson_B;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.BaseData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;


/**
 *
 * @author Vincent Calame
 */
public class BaseJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final ScrutariDB scrutariDB;
    private final Lang lang;

    public BaseJsonProducer(int version, ScrutariDB scrutariDB, Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.version = version;
        this.scrutariDB = scrutariDB;
        this.lang = lang;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            if (version >= 3) {
                writeVersion3(jsonWriter, dataAccess);
            } else if (version == 2) {
                writeVersion2(jsonWriter, dataAccess);
            } else {
                writeVersion1(jsonWriter, dataAccess);
            }
            writeWarnings(jsonWriter);
            jsonWriter.endObject();
        }
    }

    private void writeVersion3(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        jsonWriter.key("baseArray");
        jsonWriter.array();
        for (BaseData baseData : dataAccess.getBaseDataList()) {
            BaseDataJson_B.object(jsonWriter, baseData, lang, scrutariDB.getStats());
        }
        jsonWriter.endArray();
    }

    private void writeVersion2(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        jsonWriter.key("baseArray");
        jsonWriter.array();
        for (BaseData baseData : dataAccess.getBaseDataList()) {
            BaseDataJson_A.object(jsonWriter, baseData, lang, scrutariDB.getStats());
        }
        jsonWriter.endArray();
    }

    private void writeVersion1(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        List<BaseData> baseDataList = dataAccess.getBaseDataList();
        jsonWriter.key("baseData");
        jsonWriter.object();
        jsonWriter.key("baseCount");
        jsonWriter.value(baseDataList.size());
        if (!baseDataList.isEmpty()) {
            jsonWriter.key("baseArray");
            jsonWriter.array();
            for (BaseData baseData : baseDataList) {
                BaseDataJson_A.object(jsonWriter, baseData, lang, scrutariDB.getStats());
            }
            jsonWriter.endArray();
        }
        jsonWriter.endObject();
    }

}
