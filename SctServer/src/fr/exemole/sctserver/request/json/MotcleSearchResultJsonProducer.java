/* SctServer - Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.scrutari.data.MotcleData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.MotcleSearchResult;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public class MotcleSearchResultJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final ScrutariDB scrutariDB;
    private final MotcleSearchResult motcleSearchResult;
    private final Lang lang;
    private final int limit;

    public MotcleSearchResultJsonProducer(int version, MotcleSearchResult motcleSearchResult, ScrutariDB scrutariDB, Lang lang, int limit) {
        this.version = version;
        this.scrutariDB = scrutariDB;
        this.motcleSearchResult = motcleSearchResult;
        this.lang = lang;
        this.limit = limit;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            jsonWriter.key("motcleSearchResult");
            jsonWriter.object();
            List<MotcleSearchResultInfo> infoList = motcleSearchResult.getMotcleSearchResultInfoList();
            int length = infoList.size();
            if (limit != -1) {
                length = Math.max(length, limit);
            }
            jsonWriter.key("count");
            jsonWriter.value(length);
            if (length > 0) {
                jsonWriter.key("infoArray");
                jsonWriter.array();
                for (int i = 0; i < length; i++) {
                    MotcleSearchResultInfo info = infoList.get(i);
                    Integer code = info.getCode();
                    MotcleData motcleData = dataAccess.getMotcleData(code);
                    InLangOccurrences occurrences = info.getInLangOccurrencesByLang(lang);
                    Label label = motcleData.getLabels().getLabel(lang);
                    jsonWriter.object();
                    jsonWriter.key("cdmtc");
                    jsonWriter.value(code);
                    jsonWriter.key("cdths");
                    jsonWriter.value(motcleData.getThesaurusCode());
                    if ((label != null) && (occurrences != null)) {
                        String lib = label.getLabelString();
                        jsonWriter.key("lib");
                        jsonWriter.value(lib);
                        jsonWriter.key("mlib");
                        JsonUtils.writeMValue(jsonWriter, lib, occurrences.getSearchTokenOccurrenceList());
                    }
                    jsonWriter.endObject();
                }
                jsonWriter.endArray();
            }
            jsonWriter.endObject();
            writeWarnings(jsonWriter);
            jsonWriter.endObject();
        }
    }

}
