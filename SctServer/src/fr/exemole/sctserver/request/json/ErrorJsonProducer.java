/* SctServer - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LineMessage;


/**
 *
 * @author Vincent Calame
 */
public class ErrorJsonProducer extends AbstractJsonProducer {

    private final CommandMessage commandMessage;
    private final MessageLocalisation messageLocalisation;

    public ErrorJsonProducer(CommandMessage commandMessage, MessageLocalisation messageLocalisation) {
        this.commandMessage = commandMessage;
        this.messageLocalisation = messageLocalisation;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        String errorKey = commandMessage.getMessageKey();
        Object[] values = commandMessage.getMessageValues();
        String parameter;
        String value = null;
        int length = values.length;
        if (length == 0) {
            parameter = "";
        } else {
            parameter = values[0].toString();
            if (length > 1) {
                value = values[1].toString();
            }
        }
        JSONWriter jw = new JSONWriter(appendable);
        jw.object();
        {
            jw.key("error");
            jw.object();
            {
                jw.key("key")
                        .value(errorKey);
                jw.key("parameter")
                        .value(parameter);
                if (value != null) {
                    jw.key("value")
                            .value(value);
                }
                if (messageLocalisation != null) {
                    jw.key("text")
                            .value(messageLocalisation.toString(commandMessage));
                }
                if (commandMessage.hasMultiError()) {
                    addErrorList(jw, commandMessage.getMultiErrorList(), messageLocalisation);
                }
            }
            jw.endObject();
            writeWarnings(jw);
        }
        jw.endObject();
    }

    private static void addErrorList(JSONWriter jw, List<Message> errorList, MessageLocalisation messageLocalisation) throws IOException {
        jw.key("array");
        jw.array();
        for (Message errorMessage : errorList) {
            jw.object();
            {
                jw.key("key")
                        .value(errorMessage.getMessageKey());
                Object[] values = errorMessage.getMessageValues();
                if (values.length > 0) {
                    jw.key("value")
                            .value(values[0].toString());
                }
                if (errorMessage instanceof LineMessage) {
                    jw.key("line")
                            .value(((LineMessage) errorMessage).getLineNumber());
                }
                if (messageLocalisation != null) {
                    jw.key("text")
                            .value(messageLocalisation.toString(errorMessage));
                }
            }
            jw.endObject();
        }
        jw.endArray();
    }

}
