/* SctServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.MotcleWriter;
import java.io.IOException;
import java.util.Collection;
import net.mapeadores.util.json.JSONWriter;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;


/**
 *
 * @author Vincent Calame
 */
public class MotcleJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final ScrutariDB scrutariDB;
    private final Collection<Integer> motcleCodes;
    private final FieldVariant fieldVariant;

    public MotcleJsonProducer(int version, ScrutariDB scrutariDB, Collection<Integer> motcleCodes, FieldVariant fieldVariant) {
        this.version = version;
        this.scrutariDB = scrutariDB;
        this.motcleCodes = motcleCodes;
        this.fieldVariant = fieldVariant;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            if (version >= 3) {
                writeArrayVersion(jsonWriter, MotcleWriter.getBVersion(jsonWriter, dataAccess, fieldVariant, null));
            } else if (version == 2) {
                writeArrayVersion(jsonWriter, MotcleWriter.getAVersion(jsonWriter, dataAccess, fieldVariant, null));
            } else {
                writeVersion1(jsonWriter, dataAccess);
            }
            writeWarnings(jsonWriter);
            jsonWriter.endObject();
        }
    }

    private void writeArrayVersion(JSONWriter jsonWriter, MotcleWriter motcleWriter) throws IOException {
        jsonWriter.key("motcleArray");
        jsonWriter.array();
        for (Integer motcleCode : motcleCodes) {
            motcleWriter.object(motcleCode);
        }
        jsonWriter.endArray();
    }

    private void writeVersion1(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        jsonWriter.key("motcleData");
        jsonWriter.object();
        int motcleLength = motcleCodes.size();
        jsonWriter.key("motcleCount");
        jsonWriter.value(motcleLength);
        if (motcleLength > 0) {
            MotcleWriter motcleWriter = MotcleWriter.getAVersion(jsonWriter, dataAccess, fieldVariant, null);
            jsonWriter.key("motcleArray");
            jsonWriter.array();
            for (Integer motcleCode : motcleCodes) {
                motcleWriter.object(motcleCode);
            }
            jsonWriter.endArray();
        }
        jsonWriter.endObject();
    }

}
