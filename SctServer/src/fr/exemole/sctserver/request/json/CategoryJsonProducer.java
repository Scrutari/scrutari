/* SctServer - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.json.category.CategoryJson_A;
import fr.exemole.sctserver.json.category.CategoryJson_B;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;


/**
 *
 * @author Vincent Calame
 */
public class CategoryJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final Lang lang;
    private final GlobalSearchOptions globalSearchOptions;

    public CategoryJsonProducer(int version, GlobalSearchOptions globalSearchOptions, Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.version = version;
        this.globalSearchOptions = globalSearchOptions;
        this.lang = lang;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jsonWriter = new JSONWriter(appendable);
        jsonWriter.object();
        if (version >= 3) {
            writeVersion3(jsonWriter);
        } else if (version == 2) {
            writeVersion2(jsonWriter);
        } else {
            writeVersion1(jsonWriter);
        }
        writeWarnings(jsonWriter);
        jsonWriter.endObject();
    }

    private void writeVersion3(JSONWriter jsonWriter) throws IOException {
        jsonWriter.key("categoryArray");
        jsonWriter.array();
        for (Category category : globalSearchOptions.getCategoryList()) {
            CategoryJson_B.object(jsonWriter, category, lang, true);
        }
        jsonWriter.endArray();
    }

    private void writeVersion2(JSONWriter jsonWriter) throws IOException {
        jsonWriter.key("categoryArray");
        jsonWriter.array();
        for (Category category : globalSearchOptions.getCategoryList()) {
            CategoryJson_A.object(jsonWriter, category, lang, true);
        }
        jsonWriter.endArray();
    }

    private void writeVersion1(JSONWriter jsonWriter) throws IOException {
        List<Category> categoryList = globalSearchOptions.getCategoryList();
        jsonWriter.key("categoryData");
        jsonWriter.object();
        jsonWriter.key("categoryCount");
        jsonWriter.value(categoryList.size());
        if (!categoryList.isEmpty()) {
            jsonWriter.key("categoryArray");
            jsonWriter.array();
            for (Category category : categoryList) {
                CategoryJson_A.object(jsonWriter, category, lang, true);
            }
            jsonWriter.endArray();
        }
        jsonWriter.endObject();
    }

}
