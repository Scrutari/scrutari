/* SctServer - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.EngineContext;
import fr.exemole.sctserver.api.FieldVariantManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.context.EngineGroup;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.engineinfo.InfoOptions;
import fr.exemole.sctserver.json.fichesearch.InsertOptions;
import fr.exemole.sctserver.request.ParameterErrorMessageKeys;
import fr.exemole.sctserver.request.Parameters;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.request.WarningHandler;
import fr.exemole.sctserver.request.WarningKeys;
import fr.exemole.sctserver.tools.EngineUtils;
import java.text.ParseException;
import java.util.Collection;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.request.RequestMap;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.tools.codes.CodesUtils;
import net.scrutari.db.tools.util.RelUtils;
import net.scrutari.searchengine.MotcleSearchEngine;
import net.scrutari.searchengine.SupermotcleSearchEngine;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.MotcleSearchResult;
import net.scrutari.searchengine.api.result.SupermotcleSearchResult;
import net.scrutari.searchengine.tools.options.OptionsUtils;
import net.scrutari.searchengine.tools.options.SearchOptionsBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class JsonProducerFactory {

    private JsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap) {
        WarningHandler warningHandler = new WarningHandler();
        AbstractJsonProducer abstractJsonProducer;
        try {
            abstractJsonProducer = getJsonProducer(scrutariSession, requestMap, warningHandler);
        } catch (ErrorMessageException eme) {
            Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
            abstractJsonProducer = new ErrorJsonProducer(eme.getErrorMessage(), EngineUtils.getMessageLocalisation(scrutariSession.getEngine(), lang));
        }
        if ((!warningHandler.isEmpty()) && (requestMap.isTrue(Parameters.WARNINGS))) {
            abstractJsonProducer.setWarningHandler(warningHandler);
        }
        return abstractJsonProducer;
    }

    private static AbstractJsonProducer getJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, WarningHandler warningHandler) throws ErrorMessageException {
        String type = RequestMapUtils.getMandatoryParam(Parameters.TYPE, requestMap);
        String versionString = requestMap.getParameter(Parameters.VERSION);
        int version = 0;
        if (versionString != null) {
            version = -1;
            try {
                version = Integer.parseInt(versionString);
            } catch (NumberFormatException nfe) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.WRONG_PARAMETER_VALUE, Parameters.VERSION, versionString);
            }
            if (version < 0) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.VERSION, versionString);
            }
        }
        if (type.equals("motcle")) {
            return getMotcleJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("q-motcle")) {
            return getMotcleSearchResultJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("q-supermotcle")) {
            return getSupermotcleSearchResultJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("base")) {
            return getBaseJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("fiche")) {
            return getFicheJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("q-fiche")) {
            return getFicheSearchResultJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("geojson")) {
            return getGeoJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("corpus")) {
            return getCorpusJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("thesaurus")) {
            return getThesaurusJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("category")) {
            return getCategoryJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("lang")) {
            return getLangJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("engine")) {
            return getEngineInfoJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else if (type.equals("enginegroup")) {
            return getEngineGroupJsonProducer(scrutariSession, requestMap, version, warningHandler);
        } else {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.TYPE, type);
        }
    }

    private static AbstractJsonProducer getSupermotcleSearchResultJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) throws ErrorMessageException {
        SearchOperation searchOperation = RequestMapUtils.getSearchOperation(requestMap, scrutariSession.getScrutariDB().getFieldRankManager(), warningHandler);
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        SearchOptionsDefBuilder searchOptionsDefBuilder = RequestMapUtils.buildSearchOptionsDefBuilder(requestMap, warningHandler);
        searchOptionsDefBuilder.addFilterLang(lang);
        int limit = RequestMapUtils.getLimit(requestMap, warningHandler);
        SupermotcleSearchResult searchResult = SupermotcleSearchEngine.search(scrutariSession.getSupermotcleProvider(), searchOperation, searchOptionsDefBuilder.toSearchOptionsDef());
        return new SupermotcleSearchResultJsonProducer(version, searchResult, lang, limit);
    }

    private static AbstractJsonProducer getMotcleSearchResultJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) throws ErrorMessageException {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        SearchOperation searchOperation = RequestMapUtils.getSearchOperation(requestMap, scrutariDB.getFieldRankManager(), warningHandler);
        SearchOptionsDefBuilder searchOptionsDefBuilder = RequestMapUtils.buildSearchOptionsDefBuilder(requestMap, warningHandler);
        if (!searchOptionsDefBuilder.hasFilterLang()) {
            //@todo message d'erreur
            return null;
        }
        Lang lang = searchOptionsDefBuilder.reduceToUniqueLang();
        int limit = RequestMapUtils.getLimit(requestMap, warningHandler);
        SearchOptionsBuilder searchOptionsBuilder = new SearchOptionsBuilder(searchOptionsDefBuilder.toSearchOptionsDef());
        MotcleSearchResult searchResult = MotcleSearchEngine.search(scrutariDB, searchOperation, searchOptionsBuilder.toSearchOptions());
        return new MotcleSearchResultJsonProducer(version, searchResult, scrutariDB, lang, limit);

    }

    private static AbstractJsonProducer getFicheSearchResultJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) throws ErrorMessageException {
        FicheSearchResult searchResult = buildFicheSearchResult(scrutariSession, requestMap, warningHandler);
        FieldVariant fieldVariant = RequestMapUtils.getFieldVariant(scrutariSession.getEngine().getFieldVariantManager(), requestMap, FieldVariantManager.QUERY_VARIANT);
        InsertOptions insertOptions = RequestMapUtils.checkInsertOptions(requestMap, InsertOptions.init(true, true));
        StartParameters startParameters = RequestMapUtils.getStartParameters(requestMap, scrutariSession.getGlobalSearchOptions(), warningHandler);
        return new FicheSearchResultJsonProducer(version, searchResult, scrutariSession, fieldVariant, insertOptions, startParameters);
    }

    private static AbstractJsonProducer getGeoJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) throws ErrorMessageException {
        OptionalQRequestMap optionalQRequestMap = new OptionalQRequestMap(requestMap);
        FicheSearchResult searchResult = buildFicheSearchResult(scrutariSession, optionalQRequestMap, warningHandler);
        FieldVariant fieldVariant = RequestMapUtils.getFieldVariant(scrutariSession.getEngine().getFieldVariantManager(), optionalQRequestMap, FieldVariantManager.GEO_VARIANT);
        InsertOptions insertOptions = RequestMapUtils.checkInsertOptions(requestMap, InsertOptions.init(true, true));
        StartParameters startParameters = RequestMapUtils.getStartParameters(optionalQRequestMap, scrutariSession.getGlobalSearchOptions(), warningHandler);
        Lang lang = searchResult.getFicheSearchSource().getSearchOptionsDef().getLang();
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        return new GeoJsonProducer(version, searchResult, scrutariSession, fieldVariant, insertOptions, startParameters, lang);
    }

    private static FicheSearchResult buildFicheSearchResult(ScrutariSession scrutariSession, RequestMap requestMap, WarningHandler warningHandler) throws ErrorMessageException {
        String qIdString = RequestMapUtils.getQIdString(requestMap);
        FicheSearchResult searchResult;
        if (qIdString != null) {
            if (qIdString.isEmpty()) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, Parameters.Q_ID);
            }
            try {
                QId qId = QId.parse(qIdString);
                searchResult = scrutariSession.getSearch(qId);
            } catch (ParseException pe) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.WRONG_PARAMETER_VALUE, Parameters.Q_ID, qIdString);
            }
            if (searchResult == null) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.Q_ID, qIdString);
            }
        } else {
            SearchOperation searchOperation = RequestMapUtils.getSearchOperation(requestMap, scrutariSession.getScrutariDB().getFieldRankManager(), warningHandler);
            SearchOptions searchOptions = RequestMapUtils.getSearchOptions(scrutariSession, requestMap, warningHandler, true);
            searchResult = scrutariSession.search(searchOperation, searchOptions, RequestMapUtils.getSearchStoragePolicy(requestMap));
        }
        return searchResult;
    }

    private static AbstractJsonProducer getCategoryJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        return new CategoryJsonProducer(version, scrutariSession.getGlobalSearchOptions(), lang);
    }

    private static AbstractJsonProducer getBaseJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        return new BaseJsonProducer(version, scrutariSession.getScrutariDB(), lang);
    }

    private static AbstractJsonProducer getCorpusJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        return new CorpusJsonProducer(version, scrutariSession.getScrutariDB(), lang);
    }

    private static AbstractJsonProducer getThesaurusJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        return new ThesaurusJsonProducer(version, scrutariSession.getScrutariDB(), lang);
    }

    private static AbstractJsonProducer getLangJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        Lang[] langArray = RequestMapUtils.getLangArray(requestMap);
        return new LangJsonProducer(scrutariSession, version, lang, langArray);
    }

    private static AbstractJsonProducer getEngineInfoJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        InfoOptions infoOptions = RequestMapUtils.getInfoOptions(requestMap);
        return new EngineInfoJsonProducer(version, scrutariSession, lang, infoOptions);
    }

    private static AbstractJsonProducer getFicheJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        Collection<Integer> ficheCodes;
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            String ficheListParam = requestMap.getParameter(Parameters.LIST_FICHE);
            if (ficheListParam != null) {
                warningHandler.setCurrentParameter(Parameters.LIST_FICHE);
                ficheCodes = RequestMapUtils.toCodes(dataAccess, ficheListParam, warningHandler, ScrutariDataURI.FICHEURI_TYPE);
                if (ficheCodes == null) {
                    ficheCodes = PrimUtils.EMPTY_LIST;
                }
            } else {
                SearchOptions searchOptions = RequestMapUtils.getSearchOptions(scrutariSession, requestMap, warningHandler, true);
                ficheCodes = OptionsUtils.buildFicheCodeList(dataAccess, searchOptions);
            }
            if (RequestMapUtils.ignoreAlternate(requestMap)) {
                ficheCodes = RelUtils.filterAlternate(dataAccess, lang, ficheCodes);
            }
        }
        String randomString = requestMap.getParameter(Parameters.RANDOM);
        if (randomString != null) {
            warningHandler.setCurrentParameter(Parameters.RANDOM);
            try {
                int randomNumber = Integer.parseInt(randomString);
                if ((randomNumber == -1) || (randomNumber > 0)) {
                    ficheCodes = CodesUtils.shuffle(ficheCodes, randomNumber);
                } else {
                    warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, randomString);
                }
            } catch (NumberFormatException nfe) {
                warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, randomString);
            }
        }
        InsertOptions insertOptions = RequestMapUtils.checkInsertOptions(requestMap, InsertOptions.init(false, false));
        FieldVariant fieldVariant = RequestMapUtils.getFieldVariant(scrutariSession.getEngine().getFieldVariantManager(), requestMap, FieldVariantManager.DATA_VARIANT);
        return new FicheJsonProducer(version, scrutariSession, ficheCodes, fieldVariant, lang, insertOptions);
    }

    private static AbstractJsonProducer getMotcleJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        Collection<Integer> motcleCodes;
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            String motcleListParam = requestMap.getParameter(Parameters.LIST_MOTCLE);
            if (motcleListParam != null) {
                warningHandler.setCurrentParameter(Parameters.LIST_MOTCLE);
                motcleCodes = RequestMapUtils.toCodes(dataAccess, motcleListParam, warningHandler, ScrutariDataURI.MOTCLEURI_TYPE);
                if (motcleCodes == null) {
                    motcleCodes = PrimUtils.EMPTY_LIST;
                }
            } else {
                SearchOptions searchOptions = RequestMapUtils.getSearchOptions(scrutariSession, requestMap, warningHandler, false);
                motcleCodes = OptionsUtils.buildMotcleCodeList(dataAccess, searchOptions);
            }
        }
        FieldVariant fieldVariant = RequestMapUtils.getFieldVariant(scrutariSession.getEngine().getFieldVariantManager(), requestMap, FieldVariantManager.DATA_VARIANT);
        return new MotcleJsonProducer(version, scrutariDB, motcleCodes, fieldVariant);
    }

    private static AbstractJsonProducer getEngineGroupJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, int version, WarningHandler warningHandler) throws ErrorMessageException {
        Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (lang == null) {
            lang = EngineUtils.getDefaultLang(scrutariSession);
        }
        return getEngineGroupJsonProducer(scrutariSession.getEngine().getEngineContext(), requestMap, version, lang, warningHandler);
    }

    public static EngineGroupJsonProducer getEngineGroupJsonProducer(EngineContext engineContext, RequestMap requestMap, int version, Lang lang, WarningHandler warningHandler) throws ErrorMessageException {
        String name = RequestMapUtils.getMandatoryParam(Parameters.NAME, requestMap);
        EngineGroup engineGroup = engineContext.getEngineGroup(name);
        if (engineGroup == null) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.NAME, name);
        }
        return new EngineGroupJsonProducer(version, engineContext, engineGroup, lang, false);
    }

}
