/* Scrutari - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.EngineContext;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.context.EngineGroup;
import fr.exemole.sctserver.json.enginemetadata.EngineMetadataJson_A;
import fr.exemole.sctserver.json.enginemetadata.EngineMetadataJson_B;
import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class EngineGroupJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final EngineContext engineContext;
    private final EngineGroup engineGroup;
    private final Lang lang;
    private final boolean withLabelMap;


    public EngineGroupJsonProducer(int version, EngineContext engineContext, EngineGroup engineGroup, Lang lang, boolean withLabelMap) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.version = version;
        this.engineContext = engineContext;
        this.engineGroup = engineGroup;
        this.lang = lang;
        this.withLabelMap = withLabelMap;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jsonWriter = new JSONWriter(appendable);
        jsonWriter.object();
        if (version >= 2) {
            writeVersion2(jsonWriter);
        } else {
            writeVersion1(jsonWriter);
        }
        writeWarnings(jsonWriter);
        jsonWriter.endObject();
    }

    private void writeVersion2(JSONWriter jsonWriter) throws IOException {
        String url = engineContext.getWebappCanonicalUrl();
        jsonWriter.key("engineGroup");
        jsonWriter.object();
        jsonWriter.key("name");
        jsonWriter.value(engineGroup.getName());
        CommonJson.defaultLabel(jsonWriter, engineGroup.getTitleLabels(), lang, "title", engineGroup.getName());
        if (withLabelMap) {
            jsonWriter.key("labelMap");
            CommonJson.object(jsonWriter, engineGroup.getTitleLabels());
        }
        jsonWriter.key("phraseMap");
        CommonJson.object(jsonWriter, engineGroup.getPhrases(), lang);
        jsonWriter.key("attrMap");
        CommonJson.object(jsonWriter, engineGroup.getAttributes());
        jsonWriter.key("engineArray");
        jsonWriter.array();
        for (String engineName : engineGroup.getEngineNameList()) {
            jsonWriter.object();
            jsonWriter.key("name");
            jsonWriter.value(engineName);
            jsonWriter.key("url");
            jsonWriter.value(url + engineName + "/");
            SctEngine engine = engineContext.getEngine(engineName);
            if (engine != null) {
                EngineMetadataJson_B.properties(jsonWriter, engineName, engine.getEngineMetadata(), lang, withLabelMap);
            } else {
                EngineMetadataJson_B.emptyProperties(jsonWriter, engineName, withLabelMap);
            }
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
    }

    private void writeVersion1(JSONWriter jsonWriter) throws IOException {
        String url = engineContext.getWebappCanonicalUrl();
        jsonWriter.key("engineGroup");
        jsonWriter.object();
        jsonWriter.key("name");
        jsonWriter.value(engineGroup.getName());
        CommonJson.defaultLabel(jsonWriter, engineGroup.getTitleLabels(), lang, "title", engineGroup.getName());
        if (withLabelMap) {
            jsonWriter.key("labelMap");
            CommonJson.object(jsonWriter, engineGroup.getTitleLabels());
        }
        jsonWriter.key("attrMap");
        CommonJson.object(jsonWriter, engineGroup.getAttributes());
        jsonWriter.key("engineArray");
        jsonWriter.array();
        for (String engineName : engineGroup.getEngineNameList()) {
            jsonWriter.object();
            jsonWriter.key("name");
            jsonWriter.value(engineName);
            jsonWriter.key("url");
            jsonWriter.value(url + engineName + "/");
            SctEngine engine = engineContext.getEngine(engineName);
            if (engine != null) {
                EngineMetadataJson_A.properties(jsonWriter, engineName, engine.getEngineMetadata(), lang, withLabelMap);
            }
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
        jsonWriter.endObject();
    }

}
