/* SctEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.tools.EngineUtils;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class LangJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final int version;
    private final Lang lang;
    private final Lang[] langArray;

    public LangJsonProducer(ScrutariSession scrutariSession, int version, Lang lang, Lang[] langArray) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.scrutariSession = scrutariSession;
        this.version = version;
        this.lang = lang;
        this.langArray = langArray;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        MessageLocalisation messageLocalisation = EngineUtils.getMessageLocalisation(scrutariSession.getEngine(), lang);
        JSONWriter jsonWriter = new JSONWriter(appendable);
        jsonWriter.object();
        jsonWriter.key("langMap");
        jsonWriter.object();
        if (langArray != null) {
            for (Lang currentLang : langArray) {
                String code = currentLang.toString();
                jsonWriter.key(code);
                String value = messageLocalisation.toString(code);
                if (value != null) {
                    value = StringUtils.getFirstPart(value);
                } else {
                    value = code;
                }
                jsonWriter.value(value);
            }
        }
        jsonWriter.endObject();
        writeWarnings(jsonWriter);
        jsonWriter.endObject();
    }

}
