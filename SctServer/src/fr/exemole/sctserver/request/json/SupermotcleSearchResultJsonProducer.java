/* SctServer - Copyright (c) 2010-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.collation.CollationUnit;
import net.scrutari.searchengine.api.result.SupermotcleSearchResult;
import net.scrutari.searchengine.api.result.SupermotcleSearchResultInfo;
import net.scrutari.supermotcle.api.Supermotcle;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleSearchResultJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final SupermotcleSearchResult supermotcleSearchResult;
    private final Lang lang;
    private final int limit;

    public SupermotcleSearchResultJsonProducer(int version, SupermotcleSearchResult supermotcleSearchResult, Lang lang, int limit) {
        this.version = version;
        this.supermotcleSearchResult = supermotcleSearchResult;
        this.lang = lang;
        this.limit = limit;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jsonWriter = new JSONWriter(appendable);
        jsonWriter.object();
        jsonWriter.key("supermotcleSearchResult");
        jsonWriter.object();
        List<SupermotcleSearchResultInfo> infoList = supermotcleSearchResult.getSupermotcleSearchResultInfoList();
        int infoLength = infoList.size();
        if (limit != -1) {
            infoLength = Math.min(infoLength, limit);
        }
        jsonWriter.key("supermotcleCount");
        jsonWriter.value(infoLength);
        if (infoLength > 0) {
            jsonWriter.key("supermotcleArray");
            jsonWriter.array();
            for (int i = 0; i < infoLength; i++) {
                SupermotcleSearchResultInfo info = infoList.get(i);
                Supermotcle supermotcle = info.getSupermotcle();
                CollationUnit collationUnit = supermotcle.getCollationUnit();
                String lib = collationUnit.getSourceString();
                jsonWriter.object();
                jsonWriter.key("codesupermotcle");
                jsonWriter.value(supermotcle.getCode());
                jsonWriter.key("lib");
                jsonWriter.value(lib);
                jsonWriter.key("mlib");
                JsonUtils.writeMValue(jsonWriter, lib, info.getSearchTokenOccurrenceList());
                jsonWriter.key("indexationCount");
                jsonWriter.value(supermotcle.getIndexationCount());
                jsonWriter.key("baseArray");
                jsonWriter.array();
                for (Supermotcle.IndexationByBase indexationByBase : supermotcle.getIndexationByBaseList()) {
                    jsonWriter.object();
                    jsonWriter.key("codebase");
                    jsonWriter.value(indexationByBase.getBaseCode());
                    jsonWriter.key("indexationCount");
                    jsonWriter.value(indexationByBase.getIndexationCount());
                    jsonWriter.endObject();
                }
                jsonWriter.endArray();
                jsonWriter.endObject();
            }
            jsonWriter.endArray();
        }
        jsonWriter.endObject();
        writeWarnings(jsonWriter);
        jsonWriter.endObject();
    }

}
