/* SctServer - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.MotcleWriter;
import fr.exemole.sctserver.json.fichedata.FicheDataJson_A;
import fr.exemole.sctserver.json.fichedata.FicheDataJson_B;
import fr.exemole.sctserver.json.fichesearch.BythesaurusMapJson_A;
import fr.exemole.sctserver.json.fichesearch.CodemotcleArrayJson_A;
import fr.exemole.sctserver.json.fichesearch.InsertOptions;
import java.io.IOException;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;
import net.scrutari.db.api.FieldRankManager;


/**
 *
 * @author Vincent Calame
 */
public class FicheJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final ScrutariSession scrutariSession;
    private final Collection<Integer> ficheCodes;
    private final FieldVariant fieldVariant;
    private final Lang lang;
    private final boolean withCodemotcleArray;
    private final boolean withBythesaurusMap;
    private final Set<Integer> motcleCodeSet;

    public FicheJsonProducer(int version, ScrutariSession scrutariSession, Collection<Integer> ficheCodes, FieldVariant fieldVariant, Lang lang, InsertOptions insertOptions) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.version = version;
        this.scrutariSession = scrutariSession;
        this.ficheCodes = ficheCodes;
        this.fieldVariant = fieldVariant;
        this.lang = lang;
        this.withCodemotcleArray = fieldVariant.isFicheWith(FieldVariant.FICHE_CODEMOTCLEARRAY, false);
        this.withBythesaurusMap = fieldVariant.isFicheWith(FieldVariant.FICHE_BYTHESAURUSMAP, false);
        if (insertOptions.isWith(InsertOptions.MOTCLEARRAY)) {
            this.motcleCodeSet = new TreeSet<Integer>();
        } else {
            this.motcleCodeSet = null;
        }
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            if (version >= 3) {
                writeVersion3(jsonWriter, dataAccess);
            } else if (version == 2) {
                writeVersion2(jsonWriter, dataAccess);
            } else {
                writeVersion1(jsonWriter, dataAccess);
            }
            writeWarnings(jsonWriter);
            jsonWriter.endObject();
        }
    }

    private void writeVersion3(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        AttributeDefManager attributeDefManager = scrutariSession.getEngine().getAttributeDefManager();
        FieldRankManager fieldRankManager = scrutariSession.getScrutariDB().getFieldRankManager();
        BaseChecker baseChecker = new BaseChecker(lang);
        jsonWriter.key("ficheArray");
        jsonWriter.array();
        for (Integer ficheCode : ficheCodes) {
            FicheData ficheData = dataAccess.getFicheData(ficheCode);
            BaseCheck baseCheck = baseChecker.getBaseCheck(dataAccess, ficheData);
            jsonWriter.object();
            FicheDataJson_B.properties(jsonWriter, ficheData, fieldVariant, baseCheck, null, attributeDefManager, fieldRankManager);
            if (withCodemotcleArray) {
                CodemotcleArrayJson_A.properties(jsonWriter, ficheCode, dataAccess, motcleCodeSet);
            }
            if (withBythesaurusMap) {
                BythesaurusMapJson_A.properties(jsonWriter, ficheCode, dataAccess, motcleCodeSet);
            }
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
        writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getBVersion(jsonWriter, dataAccess, fieldVariant, lang));
    }

    private void writeVersion2(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        AttributeDefManager attributeDefManager = scrutariSession.getEngine().getAttributeDefManager();
        FieldRankManager fieldRankManager = scrutariSession.getScrutariDB().getFieldRankManager();
        BaseChecker baseChecker = new BaseChecker(lang);
        jsonWriter.key("ficheArray");
        jsonWriter.array();
        for (Integer ficheCode : ficheCodes) {
            FicheData ficheData = dataAccess.getFicheData(ficheCode);
            BaseCheck baseCheck = baseChecker.getBaseCheck(dataAccess, ficheData);
            jsonWriter.object();
            FicheDataJson_A.properties(jsonWriter, ficheData, fieldVariant, baseCheck, null, attributeDefManager, fieldRankManager);
            if (withCodemotcleArray) {
                CodemotcleArrayJson_A.properties(jsonWriter, ficheCode, dataAccess, motcleCodeSet);
            }
            if (withBythesaurusMap) {
                BythesaurusMapJson_A.properties(jsonWriter, ficheCode, dataAccess, motcleCodeSet);
            }
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
        writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getAVersion(jsonWriter, dataAccess, fieldVariant, lang));
    }

    private void writeVersion1(JSONWriter jsonWriter, DataAccess dataAccess) throws IOException {
        AttributeDefManager attributeDefManager = scrutariSession.getEngine().getAttributeDefManager();
        FieldRankManager fieldRankManager = scrutariSession.getScrutariDB().getFieldRankManager();
        BaseChecker baseChecker = new BaseChecker(lang);
        jsonWriter.key("ficheData");
        jsonWriter.object();
        int ficheLength = ficheCodes.size();
        jsonWriter.key("ficheCount");
        jsonWriter.value(ficheLength);
        if (ficheLength > 0) {
            jsonWriter.key("ficheArray");
            jsonWriter.array();
            for (Integer ficheCode : ficheCodes) {
                FicheData ficheData = dataAccess.getFicheData(ficheCode);
                BaseCheck baseCheck = baseChecker.getBaseCheck(dataAccess, ficheData);
                jsonWriter.object();
                FicheDataJson_A.properties(jsonWriter, ficheData, fieldVariant, baseCheck, null, attributeDefManager, fieldRankManager);
                jsonWriter.endObject();
            }
            jsonWriter.endArray();
        }
        jsonWriter.endObject();
        writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getAVersion(jsonWriter, dataAccess, fieldVariant, lang));
    }

    private void writeMotcleArray(JSONWriter jsonWriter, DataAccess dataAccess, MotcleWriter motcleWriter) throws IOException {
        if (motcleCodeSet == null) {
            return;
        }
        jsonWriter.key("motcleArray");
        jsonWriter.array();
        for (Integer motcleCode : motcleCodeSet) {
            motcleWriter.object(motcleCode);
        }
        jsonWriter.endArray();
    }

}
