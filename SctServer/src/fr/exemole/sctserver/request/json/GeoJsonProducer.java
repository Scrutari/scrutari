/* SctServer - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.MotcleWriter;
import fr.exemole.sctserver.json.engineinfo.EngineInfoJson_C;
import fr.exemole.sctserver.json.engineinfo.InfoOptions;
import fr.exemole.sctserver.json.fichedata.FicheDataJson_A;
import fr.exemole.sctserver.json.fichedata.FicheDataJson_B;
import fr.exemole.sctserver.json.fichesearch.BythesaurusMapJson_A;
import fr.exemole.sctserver.json.fichesearch.CodemotcleArrayJson_A;
import fr.exemole.sctserver.json.fichesearch.InsertOptions;
import fr.exemole.sctserver.json.fichesearch.SearchMetaJson_B;
import fr.exemole.sctserver.json.fichesearch.SearchMetaJson_C;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.FicheData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.db.api.FieldRankManager;


/**
 *
 * @author Vincent Calame
 */
public class GeoJsonProducer extends AbstractJsonProducer {

    private final static InfoOptions INFO_OPTIONS;
    private final int version;
    private final ScrutariSession scrutariSession;
    private final AttributeDefManager attributeDefManager;
    private final FieldRankManager fieldRankManager;
    private final FicheSearchResult searchResult;
    private final FieldVariant fieldVariant;
    private final InsertOptions insertOptions;
    private final Lang lang;
    private final StartParameters startParameters;
    private final short codemotcleType;
    private final short bythesaurusType;
    private final BaseChecker baseChecker;

    static {
        INFO_OPTIONS = new InfoOptions();
        INFO_OPTIONS.setAll(true);
        INFO_OPTIONS.setWith(InfoOptions.STATS, false);
    }

    public GeoJsonProducer(int version, FicheSearchResult searchResult, ScrutariSession scrutariSession, FieldVariant fieldVariant, InsertOptions insertOptions, StartParameters startParameters, Lang lang) {
        this.version = version;
        this.searchResult = searchResult;
        this.fieldVariant = fieldVariant;
        this.insertOptions = insertOptions;
        this.startParameters = startParameters;
        this.scrutariSession = scrutariSession;
        this.lang = lang;
        this.attributeDefManager = scrutariSession.getEngine().getAttributeDefManager();
        this.fieldRankManager = scrutariSession.getScrutariDB().getFieldRankManager();
        this.codemotcleType = fieldVariant.getFicheWithType(FieldVariant.FICHE_CODEMOTCLEARRAY);
        this.bythesaurusType = fieldVariant.getFicheWithType(FieldVariant.FICHE_BYTHESAURUSMAP);
        this.baseChecker = new BaseChecker(lang);
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            Set<Integer> corpusCodeSet = new LinkedHashSet<Integer>();
            JSONWriter jsonWriter = new JSONWriter(appendable);
            jsonWriter.object();
            jsonWriter.key("type");
            jsonWriter.value("FeatureCollection");
            jsonWriter.key("features");
            jsonWriter.array();
            switch (startParameters.getType()) {
                case StartParameters.NONE_TYPE:
                    writeNoneStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
                case StartParameters.GLOBAL_TYPE:
                    writeGlobalStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
                case StartParameters.IN_ALL_TYPE:
                    writeInAllStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
                case StartParameters.IN_SELECTION_TYPE:
                    writeInSelectionStart(jsonWriter, dataAccess, corpusCodeSet);
                    break;
            }
            jsonWriter.endArray();
            if (version >= 3) {
                if (insertOptions.isWith(InsertOptions.SEARCHMETA)) {
                    jsonWriter.key("searchMeta");
                    jsonWriter.object();
                    SearchMetaJson_C.properties(jsonWriter, dataAccess, searchResult, lang);
                    jsonWriter.endObject();
                }
                if (insertOptions.isWith(InsertOptions.MOTCLEARRAY)) {
                    writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getBVersion(jsonWriter, dataAccess, fieldVariant, lang));
                }
                if (insertOptions.isWith(InsertOptions.ENGINEINFO)) {
                    EngineInfoJson_C.properties(jsonWriter, dataAccess, scrutariSession, lang, INFO_OPTIONS);
                }
            } else {
                jsonWriter.key("searchMetadata");
                jsonWriter.object();
                SearchMetaJson_B.properties(jsonWriter, dataAccess, searchResult, lang);
                writeMotcleArray(jsonWriter, dataAccess, MotcleWriter.getAVersion(jsonWriter, dataAccess, fieldVariant, lang));
                jsonWriter.endObject();
            }
            jsonWriter.endObject();
        }
    }

    private void writeNoneStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupCount = infoList.size();
            for (int j = 0; j < ficheByGroupCount; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
            }
        }
    }

    private void writeGlobalStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        int globalStartIndex = startParameters.getStart() - 1;
        int limit = startParameters.getLimit();
        boolean withLimit = (limit > 0);
        boolean limitStop = false;
        int ficheIncrement = 0;
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupLength = infoList.size();
            int startIndex = 0;
            if (ficheIncrement == 0) {
                if (ficheByGroupLength < globalStartIndex) {
                    globalStartIndex = globalStartIndex - ficheByGroupLength;
                    continue;
                } else {
                    startIndex = globalStartIndex;
                }
            }
            for (int j = startIndex; j < ficheByGroupLength; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
                if (withLimit) {
                    ficheIncrement++;
                    if (ficheIncrement == limit) {
                        limitStop = true;
                        break;
                    }
                }
            }
            if (limitStop) {
                break;
            }
        }
    }

    private void writeInAllStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        int startIndex = startParameters.getStart() - 1;
        int limit = startParameters.getLimit();
        boolean withLimit = (limit > 0);
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupLength = infoList.size();
            if (ficheByGroupLength < startIndex) {
                continue;
            }
            int min = ficheByGroupLength;
            if (withLimit) {
                min = Math.min(ficheByGroupLength, startIndex + limit);
            }
            for (int j = startIndex; j < min; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
            }
        }
    }

    private void writeInSelectionStart(JSONWriter jsonWriter, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        int startIndex = startParameters.getStart() - 1;
        int limit = startParameters.getLimit();
        boolean withLimit = (limit > 0);
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            if (!startParameters.containsCategory(ficheSearchResultGroup.getCategoryRank())) {
                continue;
            }
            List<FicheSearchResultInfo> infoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheByGroupLength = infoList.size();
            if (ficheByGroupLength < startIndex) {
                continue;
            }
            int min = ficheByGroupLength;
            if (withLimit) {
                min = Math.min(ficheByGroupLength, startIndex + limit);
            }
            for (int j = startIndex; j < min; j++) {
                FicheSearchResultInfo ficheSearchResultInfo = infoList.get(j);
                writeFicheSearchResultInfo(jsonWriter, ficheSearchResultInfo, dataAccess, corpusCodeSet);
            }
        }
    }

    private void writeFicheSearchResultInfo(JSONWriter jsonWriter, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess, Set<Integer> corpusCodeSet) throws IOException {
        Integer ficheCode = ficheSearchResultInfo.getCode();
        FicheData ficheData = dataAccess.getFicheData(ficheCode);
        if (!ficheData.isWithGeo()) {
            return;
        }
        corpusCodeSet.add(ficheData.getCorpusCode());
        BaseCheck baseCheck = baseChecker.getBaseCheck(dataAccess, ficheData);
        jsonWriter.object();
        jsonWriter.key("type");
        jsonWriter.value("Feature");
        jsonWriter.key("id");
        jsonWriter.value(ficheCode);
        jsonWriter.key("geometry");
        jsonWriter.object();
        jsonWriter.key("type");
        jsonWriter.value("Point");
        jsonWriter.key("coordinates");
        jsonWriter.array();
        jsonWriter.value(ficheData.getLongitude().toString());
        jsonWriter.value(ficheData.getLatitude().toString());
        jsonWriter.endArray();
        jsonWriter.endObject();
        jsonWriter.key("properties");
        jsonWriter.object();
        if (version >= 3) {
            writePropertiesVersion3(jsonWriter, ficheData, ficheSearchResultInfo, baseCheck);
        } else {
            writePropertiesVersion1(jsonWriter, ficheData, ficheSearchResultInfo, baseCheck);
        }
        writeCodemotcleArray(jsonWriter, ficheCode, ficheSearchResultInfo, dataAccess);
        writeBythesaurusMap(jsonWriter, ficheCode, ficheSearchResultInfo, dataAccess);
        jsonWriter.endObject();
        jsonWriter.endObject();
    }

    private void writePropertiesVersion3(JSONWriter jsonWriter, FicheData ficheData, FicheSearchResultInfo ficheSearchResultInfo, BaseCheck baseCheck) throws IOException {
        FicheDataJson_B.properties(jsonWriter, ficheData, fieldVariant, baseCheck, ficheSearchResultInfo, attributeDefManager, fieldRankManager);
    }

    private void writePropertiesVersion1(JSONWriter jsonWriter, FicheData ficheData, FicheSearchResultInfo ficheSearchResultInfo, BaseCheck baseCheck) throws IOException {
        FicheDataJson_A.properties(jsonWriter, ficheData, fieldVariant, baseCheck, ficheSearchResultInfo, attributeDefManager, fieldRankManager);
    }

    private void writeCodemotcleArray(JSONWriter jsonWriter, Integer ficheCode, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess) throws IOException {
        switch (codemotcleType) {
            case FieldVariant.ALL_TYPE:
                CodemotcleArrayJson_A.properties(jsonWriter, ficheCode, dataAccess, null);
                break;
            case FieldVariant.IGNORE_TYPE:
                break;
            default:
                CodemotcleArrayJson_A.properties(jsonWriter, ficheSearchResultInfo);
        }
    }

    private void writeBythesaurusMap(JSONWriter jsonWriter, Integer ficheCode, FicheSearchResultInfo ficheSearchResultInfo, DataAccess dataAccess) throws IOException {
        switch (bythesaurusType) {
            case FieldVariant.ALL_TYPE:
                BythesaurusMapJson_A.properties(jsonWriter, ficheCode, dataAccess, null);
                break;
            case FieldVariant.CONCERNED_TYPE:
                BythesaurusMapJson_A.properties(jsonWriter, ficheSearchResultInfo, dataAccess);
                break;
        }
    }

    private void writeMotcleArray(JSONWriter jsonWriter, DataAccess dataAccess, MotcleWriter motcleWriter) throws IOException {
        Set<Integer> allMotcleSet = getAllMotcleSet(dataAccess);
        jsonWriter.key("motcleArray");
        jsonWriter.array();
        for (MotcleSearchResultInfo motcleSearchResultInfo : searchResult.getMotcleSearchResultInfoList()) {
            motcleWriter.object(motcleSearchResultInfo);
            if (allMotcleSet != null) {
                allMotcleSet.remove(motcleSearchResultInfo.getCode());
            }
        }
        if ((allMotcleSet != null) && (allMotcleSet.size() > 0)) {
            for (Integer motcleCode : allMotcleSet) {
                motcleWriter.object(motcleCode);
            }
        }
        jsonWriter.endArray();
    }

    private Set<Integer> getAllMotcleSet(DataAccess dataAccess) {
        if (codemotcleType != FieldVariant.ALL_TYPE) {
            return null;
        }
        int motcleCount = scrutariSession.getScrutariDB().getStats().getEngineCountStats().getMotcleCount();
        Set<Integer> allMotcleSet = new HashSet<Integer>();
        boolean full = false;
        for (FicheSearchResultGroup ficheSearchResultGroup : searchResult.getFicheSearchResultGroupList()) {
            for (FicheSearchResultInfo ficheResultInfo : ficheSearchResultGroup.getFicheSearchResultInfoList()) {
                FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheResultInfo.getCode());
                for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                    allMotcleSet.addAll(ficheInfo.getIndexationCodeList(thesaurusData.getThesaurusCode()));
                }
                if (allMotcleSet.size() == motcleCount) {
                    full = true;
                    break;
                }
            }
            if (full) {
                break;
            }
        }
        return allMotcleSet;
    }

}
