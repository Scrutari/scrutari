/* SctEngine - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.json.engineinfo.EngineInfoJson_A;
import fr.exemole.sctserver.json.engineinfo.EngineInfoJson_B;
import fr.exemole.sctserver.json.engineinfo.EngineInfoJson_C;
import fr.exemole.sctserver.json.engineinfo.InfoOptions;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.db.api.DataAccess;


/**
 *
 * @author Vincent Calame
 */
public class EngineInfoJsonProducer extends AbstractJsonProducer {

    private final int version;
    private final ScrutariSession scrutariSession;
    private final Lang lang;
    private final InfoOptions infoOptions;

    public EngineInfoJsonProducer(int version, ScrutariSession scrutariSession, Lang lang, InfoOptions infoOptions) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        this.version = version;
        this.scrutariSession = scrutariSession;
        this.lang = lang;
        this.infoOptions = infoOptions;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        JSONWriter jsonWriter = new JSONWriter(appendable);
        jsonWriter.object();
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            if (version >= 3) {
                EngineInfoJson_C.properties(jsonWriter, dataAccess, scrutariSession, lang, infoOptions);
            } else if (version == 2) {
                EngineInfoJson_B.properties(jsonWriter, dataAccess, scrutariSession, lang, infoOptions);
            } else {
                EngineInfoJson_A.properties(jsonWriter, dataAccess, scrutariSession, lang, infoOptions);
            }
        }
        writeWarnings(jsonWriter);
        jsonWriter.endObject();
    }

}
