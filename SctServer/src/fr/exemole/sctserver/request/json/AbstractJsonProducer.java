/* SctServer - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request.json;

import fr.exemole.sctserver.request.WarningHandler;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.json.JsonProducer;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractJsonProducer implements JsonProducer {

    private WarningHandler warningHandler;

    public WarningHandler getWarningHandler() {
        return warningHandler;
    }

    public void setWarningHandler(WarningHandler warningHandler) {
        this.warningHandler = warningHandler;
    }

    public void writeWarnings(JSONWriter jsonWriter) throws IOException {
        if (warningHandler == null) {
            return;
        }
        List<WarningHandler.Warning> warningList = warningHandler.getWarningList();
        if (warningList.isEmpty()) {
            return;
        }
        jsonWriter.key("warnings");
        jsonWriter.array();
        for (WarningHandler.Warning warning : warningList) {
            jsonWriter.object();
            jsonWriter.key("key");
            jsonWriter.value(warning.getWarningKey());
            jsonWriter.key("parameter");
            jsonWriter.value(warning.getWarningParameter());
            Object[] values = warning.getWarningValues();
            jsonWriter.key("values");
            jsonWriter.array();
            for (Object value : values) {
                jsonWriter.value(value);
            }
            jsonWriter.endArray();
            jsonWriter.endObject();
        }
        jsonWriter.endArray();
    }

}
