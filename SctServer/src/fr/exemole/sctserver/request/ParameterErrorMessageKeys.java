/* SctServer - Copyright (c) 2011-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request;


/**
 *
 * @author Vincent Calame
 */
public interface ParameterErrorMessageKeys {

    public final static String UNSUPPORTED_MISSING_PARAMETER = "_ error.unsupported.missingparameter";
    public final static String EMPTY_PARAMETER = "_ error.empty.parametervalue";
    public final static String WRONG_PARAMETER_VALUE = "_ error.wrong.parametervalue";
    public final static String UNKNOWN_PARAMETER_VALUE = "_ error.unknown.parametervalue";
    public final static String WRONG_OPERATION_SYNTAX = "_ error.wrong.operationsyntax";
}
