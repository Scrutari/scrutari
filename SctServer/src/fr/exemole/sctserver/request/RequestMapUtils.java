/* SctEngine - Copyright (c) 2010-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request;

import fr.exemole.sctserver.api.FieldVariantManager;
import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SearchStoragePolicy;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.engineinfo.InfoOptions;
import fr.exemole.sctserver.json.fichesearch.InsertOptions;
import fr.exemole.sctserver.request.json.StartParameters;
import fr.exemole.sctserver.tools.AddURITreeProvider;
import fr.exemole.sctserver.tools.BuildingUtils;
import fr.exemole.sctserver.tools.fieldvariant.AliasBuilder;
import fr.exemole.sctserver.tools.fieldvariant.FieldVariantBuilder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.CommandMessageBuilder;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logicaloperation.LogicalOperationConstants;
import net.mapeadores.util.logicaloperation.OperandException;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.TextConstants;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.tools.codes.CodeParser;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityOperandParser;
import net.scrutari.searchengine.tools.operands.search.LogicalSearchOperationEngine;
import net.scrutari.searchengine.tools.operands.search.RawOperation;
import net.scrutari.searchengine.tools.operands.search.RawSearchOperationEngine;
import net.scrutari.searchengine.tools.options.ListReductionBuilder;
import net.scrutari.searchengine.tools.options.OptionsUtils;
import net.scrutari.searchengine.tools.options.PertinencePonderationBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;
import net.scrutari.supermotcle.api.Supermotcle;
import net.scrutari.supermotcle.api.SupermotcleHolder;


/**
 *
 * @author Vincent Calame
 */
public final class RequestMapUtils implements Parameters {

    private RequestMapUtils() {
    }

    public static String getMandatoryParam(String paramName, RequestMap requestMap) throws ErrorMessageException {
        String paramValue = requestMap.getParameter(paramName);
        if (paramValue == null) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNSUPPORTED_MISSING_PARAMETER, paramName);
        }
        if (paramValue.isEmpty()) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, paramName);
        }
        return paramValue;
    }

    public static StartParameters getStartParameters(RequestMap requestMap, GlobalSearchOptions globalSearchOptions, WarningHandler warningHandler) {
        String startParam = requestMap.getParameter(START);
        if (startParam == null) {
            return StartParameters.NONE;
        }
        int start;
        try {
            start = Integer.parseInt(startParam);
        } catch (NumberFormatException nfe) {
            warningHandler.setCurrentParameter(START);
            warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, startParam);
            return StartParameters.NONE;
        }
        if (start < 1) {
            return StartParameters.NONE;
        }
        String limitParam = requestMap.getParameter(LIMIT);
        int limit = -1;
        if (limitParam != null) {
            try {
                limit = Integer.parseInt(limitParam);
                if (limit < 1) {
                    limit = -1;
                }
            } catch (NumberFormatException nfe) {
                warningHandler.setCurrentParameter(LIMIT);
                warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, limitParam);
            }
        }
        if (!globalSearchOptions.isWithCategory()) {
            return StartParameters.newGlobalInstance(start, limit);
        }
        warningHandler.setCurrentParameter(STARTTYPE);
        String typeParam = requestMap.getParameter(STARTTYPE);
        if ((typeParam == null) || (typeParam.equals(STARTTYPE_VALUE_GLOBAL))) {
            return StartParameters.newGlobalInstance(start, limit);
        }
        if (typeParam.equals(STARTTYPE_VALUE_IN_ALL)) {
            return StartParameters.newInAllInstance(start, limit);
        }
        if (typeParam.startsWith(STARTTYPE_VALUE_IN_PREFIX)) {
            String[] tokens = StringUtils.getTechnicalTokens(typeParam.substring(STARTTYPE_VALUE_IN_PREFIX.length()), true);
            Set<Integer> rankSet = new HashSet<Integer>();
            int length = tokens.length;
            for (int i = 0; i < length; i++) {
                String token = tokens[i];
                Category category = globalSearchOptions.getCategoryByName(token);
                if (category == null) {
                    warningHandler.addWarning(WarningKeys.UNKNOWN_PARAMETER_VALUE, token);
                } else {
                    rankSet.add(category.getCategoryDef().getRank());
                }
            }
            return StartParameters.newInSelectionInstance(start, limit, rankSet);
        }
        warningHandler.addWarning(WarningKeys.UNKNOWN_PARAMETER_VALUE, typeParam);
        return StartParameters.newGlobalInstance(start, limit);
    }

    public static String getLangListParameterValue(RequestMap requestMap) {
        String param_langs = requestMap.getParameter(LIST_LANG);
        if (param_langs == null) {
            param_langs = requestMap.getParameter("langs");
        }
        return param_langs;
    }

    public static Lang[] getLangArray(RequestMap requestMap) {
        String param_langs = requestMap.getParameter(LIST_LANG);
        if (param_langs == null) {
            param_langs = requestMap.getParameter("langs");
        }
        if (param_langs == null) {
            return null;
        }
        Lang[] langArray = LangsUtils.toCleanLangArray(param_langs);
        if (langArray.length == 0) {
            return null;
        }
        return langArray;
    }

    public static Lang getLang(RequestMap requestMap, WarningHandler warningHandler) {
        String langString = requestMap.getParameter(LANG);
        if (langString == null) {
            langString = requestMap.getParameter("langui");
        }
        if (langString == null) {
            return null;
        }
        try {
            Lang lang = Lang.parse(langString);
            return lang;
        } catch (ParseException lie) {
            warningHandler.setCurrentParameter(LANG);
            warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, langString);
            return null;
        }
    }

    public static int getLimit(RequestMap requestMap, WarningHandler warningHandler) {
        String limit = requestMap.getParameter(LIMIT);
        if (limit == null) {
            return -1;
        }
        try {
            int limitInteger = Integer.parseInt(limit);
            if (limitInteger > 0) {
                return limitInteger;
            } else {
                return -1;
            }
        } catch (NumberFormatException nfe) {
            warningHandler.setCurrentParameter(LIMIT);
            warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, limit);
            return -1;
        }
    }

    public static int getStart(RequestMap requestMap, WarningHandler warningHandler) {
        String start = requestMap.getParameter(START);
        if (start == null) {
            return - 1;
        }
        try {
            int globalStartInteger = Integer.parseInt(start);
            if (globalStartInteger > 0) {
                return globalStartInteger;
            } else {
                return -1;
            }
        } catch (NumberFormatException nfe) {
            warningHandler.setCurrentParameter(START);
            warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, start);
            return -1;
        }
    }

    public static String getQIdString(RequestMap requestMap) {
        String qIdString = requestMap.getParameter(Q_ID);
        if (qIdString == null) {
            qIdString = requestMap.getParameter("idrecherche");
        }
        return qIdString;
    }

    public static InfoOptions getInfoOptions(RequestMap requestMap) {
        InfoOptions infoOptions = new InfoOptions();
        String infoString = requestMap.getParameter(INFO);
        if (infoString == null) {
            infoString = requestMap.getParameter("data");
        }
        if (infoString != null) {
            infoOptions.parseOptions(infoString);
        }
        return infoOptions;
    }

    public static SearchStoragePolicy getSearchStoragePolicy(RequestMap requestMap) {
        boolean store;
        boolean alwaysLog;
        switch (requestMap.getTrimedParameter(LOG)) {
            case "always":
            case "all":
                alwaysLog = true;
                break;
            default:
                alwaysLog = false;
        }
        switch (requestMap.getTrimedParameter(STORE)) {
            case "no":
                store = false;
                break;
            default:
                store = true;
        }
        String origin = requestMap.getTrimedParameter(ORIGIN);
        if (origin.isEmpty()) {
            origin = requestMap.getTrimedParameter("site");
        }
        return new InternalSearchStoragePolicy(store, alwaysLog, origin);
    }

    public static SearchOperation getSearchOperation(RequestMap requestMap, FieldRankManager fieldRankManager, WarningHandler warningHandler) throws ErrorMessageException {
        String modeString = requestMap.getParameter(Q_MODE);
        if (modeString == null) {
            modeString = Q_MODE_VALUE_INTERSECTION;
        }
        if (modeString.equals(Q_MODE_VALUE_OPERATION)) {
            return fromLogicalOperation(requestMap, fieldRankManager, warningHandler);
        }
        short mainOperator;
        if (modeString.equals(Q_MODE_VALUE_UNION)) {
            mainOperator = LogicalOperationConstants.UNION_OPERATOR;
        } else {
            mainOperator = LogicalOperationConstants.INTERSECTION_OPERATOR;
        }
        int defaultSearchType = getDefaultSearchType(requestMap, warningHandler);

        String q = requestMap.getParameter(Q);
        if (q == null) {
            q = requestMap.getParameter("recherche");
            if (q != null) {
                warningHandler.addWarning(WarningKeys.OBSOLETE_PARAMETER_NAME, "recherche");
            }
        }
        List<RawOperation> rawOperationList = checkRawOperationList(requestMap, mainOperator);
        if (q == null) {
            if (rawOperationList == null) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNSUPPORTED_MISSING_PARAMETER, Q);
            } else if (rawOperationList.isEmpty()) {
                throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, Q);
            }
        } else {
            q = q.trim();
            if (q.length() == 0) {
                if ((rawOperationList == null) || (rawOperationList.isEmpty())) {
                    throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, Q);
                }
            } else {
                RawOperation rawOperation = new RawOperation(q, "", mainOperator);
                if (rawOperationList == null) {
                    rawOperationList = Collections.singletonList(rawOperation);
                } else {
                    rawOperationList.add(0, rawOperation);
                }
            }
        }
        SearchMessageHandler handler = new SearchMessageHandler(Q, "");
        SearchOperation searchOperation = RawSearchOperationEngine.parse(rawOperationList, handler, fieldRankManager, defaultSearchType, mainOperator);
        if (handler.hasError()) {
            throw new ErrorMessageException(handler.toErrorMessage());
        } else if (searchOperation == null) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, Q, q);
        } else {
            return searchOperation;
        }
    }

    private static List<RawOperation> checkRawOperationList(RequestMap requestMap, short mainOperator) {
        List<RawOperation> rawOperationList = null;
        for (String paramName : requestMap.getParameterNameSet()) {
            if (paramName.startsWith(Q_PREFIX)) {
                String qName = paramName.substring(Q_PREFIX.length());
                if (qName.length() > 0) {
                    if (rawOperationList == null) {
                        rawOperationList = new ArrayList<RawOperation>();
                    }
                    String q = requestMap.getParameter(paramName).trim();
                    if (q.length() > 0) {
                        String scope = requestMap.getParameter(Q_SCOPE_PREFIX + qName);
                        String mode = requestMap.getParameter(Q_MODE_PREFIX + qName);
                        short operator = mainOperator;
                        if (mode != null) {
                            if (mode.equals(Q_MODE_VALUE_UNION)) {
                                operator = LogicalOperationConstants.UNION_OPERATOR;
                            } else if (mode.equals(Q_MODE_VALUE_INTERSECTION)) {
                                operator = LogicalOperationConstants.INTERSECTION_OPERATOR;
                            }
                        }
                        rawOperationList.add(new RawOperation(q, scope, operator));
                    }
                }
            }
        }
        return rawOperationList;
    }

    private static SearchOperation fromLogicalOperation(RequestMap requestMap, FieldRankManager fieldRankManager, WarningHandler warningHandler) throws ErrorMessageException {
        String q = requestMap.getParameter(Q);
        if (q == null) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNSUPPORTED_MISSING_PARAMETER, Q);
        }
        q = q.trim();
        if (q.length() == 0) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, Q);
        }
        SearchMessageHandler handler = new SearchMessageHandler(Q, q);
        SearchOperation searchOperation = LogicalSearchOperationEngine.parse(q, handler, fieldRankManager);
        if (handler.hasError()) {
            throw new ErrorMessageException(handler.toErrorMessage());
        } else if (searchOperation == null) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.EMPTY_PARAMETER, Q, q);
        } else {
            return searchOperation;
        }
    }

    public static int getDefaultSearchType(RequestMap requestMap, WarningHandler warningHandler) {
        String wildchar = requestMap.getParameter(WILDCHAR);
        if ((wildchar == null) || (wildchar.length() == 0)) {
            return TextConstants.STARTSWITH;
        } else if (wildchar.equals(WILDCHAR_VALUE_END)) {
            return TextConstants.STARTSWITH;
        } else if (wildchar.equals(WILDCHAR_VALUE_BOTH)) {
            return TextConstants.CONTAINS;
        } else if (wildchar.equals(WILDCHAR_VALUE_START)) {
            return TextConstants.ENDSWITH;
        } else if (wildchar.equals(WILDCHAR_VALUE_NONE)) {
            return TextConstants.MATCHES;
        } else {
            if (warningHandler != null) {
                warningHandler.setCurrentParameter(WILDCHAR);
                warningHandler.addWarning(WarningKeys.UNKNOWN_PARAMETER_VALUE, wildchar);
            }
            return TextConstants.STARTSWITH;
        }
    }

    public static SearchOptionsDefBuilder buildSearchOptionsDefBuilder(RequestMap requestMap, WarningHandler warningHandler) {
        SearchOptionsDefBuilder searchOptionsDefBuilder = SearchOptionsDefBuilder.init()
                .setLang(getLang(requestMap, warningHandler))
                .setPertinencePonderation(getPertinencePonderation(requestMap, warningHandler));
        String langListParam = requestMap.getParameter(LIST_LANG);
        if (langListParam == null) {
            langListParam = requestMap.getParameter("langs");
        }
        if (langListParam != null) {
            String[] tokens = StringUtils.getTechnicalTokens(langListParam, true);
            for (String token : tokens) {
                try {
                    Lang lang = Lang.parse(token);
                    searchOptionsDefBuilder.addFilterLang(lang);
                } catch (ParseException mce) {
                }
            }
        }
        return searchOptionsDefBuilder;
    }

    private static PertinencePonderation getPertinencePonderation(RequestMap requestMap, WarningHandler warningHandler) {
        String ponderationString = requestMap.getParameter(PONDERATION);
        if (ponderationString == null) {
            return null;
        }
        warningHandler.setCurrentParameter(PONDERATION);
        String[] ponderationTokenArray = StringUtils.getTechnicalTokens(ponderationString, true);
        try {
            int occurrence = getPonderationValue(ponderationTokenArray, 0);
            int date = getPonderationValue(ponderationTokenArray, 1);
            int origin = getPonderationValue(ponderationTokenArray, 2);
            int lang = getPonderationValue(ponderationTokenArray, 3);
            return PertinencePonderationBuilder.toPertinencePonderation(occurrence, date, origin, lang);
        } catch (NumberFormatException nfe) {
            warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, ponderationString);
            return null;
        }
    }

    private static int getPonderationValue(String[] tokenArray, int index) {
        if (index >= tokenArray.length) {
            return 0;
        }
        return Integer.parseInt(tokenArray[index]);
    }

    public static Supermotcle getSupermotcle(ScrutariSession scrutariSession, RequestMap requestMap) {
        String supermotcleString = requestMap.getParameter("supermotcle");
        if (supermotcleString == null) {
            return null;
        }
        int idx = supermotcleString.indexOf("/");
        if (idx == -1) {
            return null;
        }
        Lang lang;
        int code;
        try {
            lang = Lang.parse(supermotcleString.substring(0, idx));
        } catch (ParseException pe) {
            return null;
        }
        try {
            code = Integer.parseInt(supermotcleString.substring(idx + 1));
        } catch (NumberFormatException nfe) {
            return null;
        }
        SupermotcleHolder supermotcleHolder = scrutariSession.getSupermotcleProvider().getSupermotcleHolder(lang);
        if (supermotcleHolder == null) {
            return null;
        }
        Supermotcle supermotcle = supermotcleHolder.getSupermotcleByCode(code);
        if (supermotcle == null) {
            return null;
        }
        return supermotcle;
    }

    public static String getOrigin(RequestMap requestMap) {
        String origin = requestMap.getParameter(ORIGIN);
        if (origin == null) {
            origin = requestMap.getParameter("site");
        }
        if (origin == null) {
            return "";
        } else {
            return origin;
        }
    }

    public static SearchOptions getSearchOptions(ScrutariSession scrutariSession, RequestMap requestMap, WarningHandler warningHandler, boolean reduceThesaurus) {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            SearchOptionsDefBuilder searchOptionsDefBuilder = RequestMapUtils.buildSearchOptionsDefBuilder(requestMap, warningHandler);
            initBaseReductionList(searchOptionsDefBuilder, dataAccess, requestMap, warningHandler);
            initThesaurusReductionList(searchOptionsDefBuilder, dataAccess, requestMap, warningHandler);
            initCorpusReductionList(searchOptionsDefBuilder, dataAccess, requestMap, warningHandler);
            checkCategoryList(searchOptionsDefBuilder, requestMap, scrutariSession.getGlobalSearchOptions(), warningHandler);
            checkFilters(searchOptionsDefBuilder, requestMap, warningHandler);
            SearchOptionsBuilder searchOptionsBuilder = new SearchOptionsBuilder(searchOptionsDefBuilder.toSearchOptionsDef());
            BuildingUtils.computeReduction(searchOptionsBuilder, dataAccess, scrutariSession.getGlobalSearchOptions());
            searchOptionsBuilder.checkEligibility(dataAccess, scrutariSession, warningHandler.getCodeMessageHandler(), new AddURITreeProvider(scrutariSession.getEngine().getEngineStorage()));
            Supermotcle supermotcle = RequestMapUtils.getSupermotcle(scrutariSession, requestMap);
            if (supermotcle != null) {
                //@todo :
            }
            if (reduceThesaurus) {
                Integer[] currentCodeCorpusArray = searchOptionsBuilder.getCurrentCorpusCodeArray();
                if (currentCodeCorpusArray != null) {
                    Collection<Integer> selectionThesaurusCodes = OptionsUtils.getSelectionThesaurusCodes(dataAccess, currentCodeCorpusArray);
                    searchOptionsBuilder.reduceThesaurusCodes(selectionThesaurusCodes);
                }
            }
            SearchOptions searchOptions = searchOptionsBuilder.toSearchOptions();
            return searchOptions;
        }
    }


    private static void checkFilters(SearchOptionsDefBuilder searchOptionsDefBuilder, RequestMap requestMap, WarningHandler warningHandler) {
        for (String paramName : requestMap.getParameterNameSet()) {
            boolean isObsoleteParamName = false;
            String checkedParamName = paramName;
            if ((paramName.equals("indexation")) || (paramName.equals("flt-indexation"))) {
                checkedParamName = "flt-motcle";
                warningHandler.setCurrentParameter(paramName);
                warningHandler.addWarning(WarningKeys.OBSOLETE_PARAMETER_NAME, paramName);
                isObsoleteParamName = true;
            }
            if (checkedParamName.startsWith(FILTER)) {
                if (!isObsoleteParamName) {
                    warningHandler.setCurrentParameter(paramName);
                }
                EligibilityOperandParser.CheckedScope defaultScope;
                if (checkedParamName.equals(FILTER)) {
                    defaultScope = null;
                } else if (checkedParamName.startsWith(FILTER_PREFIX)) {
                    defaultScope = EligibilityOperandParser.checkDefaultScope(checkedParamName.substring(FILTER_PREFIX.length()));
                    if (defaultScope == null) {
                        warningHandler.addWarning(WarningKeys.UNKNOW_OPERAND_TYPE, paramName);
                        continue;
                    }
                } else {
                    continue;
                }
                String[] values = requestMap.getParameterValues(paramName);
                for (String value : values) {
                    value = value.trim();
                    if (value.length() == 0) {
                        continue;
                    }
                    try {
                        EligibilityOperand eligibilityOperand = EligibilityOperandParser.parse(value, defaultScope, warningHandler.getCodeMessageHandler());
                        searchOptionsDefBuilder.addFicheEligibilityOperand(eligibilityOperand);
                    } catch (OperandException pe) {
                        warningHandler.addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, value + "(" + pe.getErrorKey() + " / " + pe.getCharIndex() + ")");
                    }
                }
            }
        }
    }

    private static void checkCategoryList(SearchOptionsDefBuilder searchOptionsDefBuilder, RequestMap requestMap, GlobalSearchOptions globalSearchOptions, WarningHandler warningHandler) {
        String values = requestMap.getParameter(LIST_CATEGORY);
        if (values == null) {
            return;
        }
        values = values.trim();
        if (values.length() == 0) {
            return;
        }
        boolean exclude = false;
        if (values.charAt(0) == '!') {
            exclude = true;
            values = values.substring(1).trim();
        }
        warningHandler.setCurrentParameter(LIST_CATEGORY);
        String[] tokens = StringUtils.getTechnicalTokens(values, false);
        int length = tokens.length;
        if (length == 0) {
            warningHandler.addWarning(WarningKeys.ONLYSEPARATORS_PARAMETER_VALUE, values);
        }
        List<CleanedString> list = new ArrayList<CleanedString>();
        for (int i = 0; i < length; i++) {
            CleanedString cs = CleanedString.newInstance(tokens[i]);
            if (cs != null) {
                list.add(cs);
                if (globalSearchOptions.getCategoryByName(cs.toString()) == null) {
                    warningHandler.addWarning(WarningKeys.UNKNOWN_PARAMETER_VALUE, cs.toString());
                }
            }
        }
        if (!list.isEmpty()) {
            AttributesBuilder attributesBuilder = searchOptionsDefBuilder.getAttributesBuilder();
            attributesBuilder.appendValues(BuildingUtils.CATEGORY_LIST_KEY, list);
            if (exclude) {
                attributesBuilder.appendValue(BuildingUtils.CATEGORY_MODE_KEY, BuildingUtils.EXCLUDE_MODE);
            }
        }
    }

    private static void initBaseReductionList(SearchOptionsDefBuilder searchOptionsDefBuilder, DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler) {
        String param = requestMap.getParameter(LIST_BASE);
        if (param == null) {
            param = requestMap.getParameter("codesbases");
            if (param == null) {
                return;
            } else {
                warningHandler.setCurrentParameter("codesbases");
            }
        } else {
            warningHandler.setCurrentParameter(LIST_BASE);
        }
        param = param.trim();
        if (param.length() == 0) {
            return;
        }
        boolean exclude = false;
        if (param.charAt(0) == '!') {
            exclude = true;
            param = param.substring(1).trim();
        }
        Collection<Integer> codes = toCodes(dataAccess, param, warningHandler, ScrutariDataURI.BASEURI_TYPE);
        if (codes != null) {
            searchOptionsDefBuilder.putListReduction(ListReductionBuilder.build(dataAccess, ScrutariDataURI.BASEURI_TYPE, exclude, codes));
        }
    }

    private static void initThesaurusReductionList(SearchOptionsDefBuilder searchOptionsDefBuilder, DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler) {
        String param = requestMap.getParameter(LIST_THESAURUS);
        if (param == null) {
            return;
        } else {
            warningHandler.setCurrentParameter(LIST_THESAURUS);
        }
        param = param.trim();
        if (param.length() == 0) {
            return;
        }
        boolean exclude = false;
        if (param.charAt(0) == '!') {
            exclude = true;
            param = param.substring(1).trim();
        }
        Collection<Integer> codes = toCodes(dataAccess, param, warningHandler, ScrutariDataURI.THESAURUSURI_TYPE);
        if (codes != null) {
            searchOptionsDefBuilder.putListReduction(ListReductionBuilder.build(dataAccess, ScrutariDataURI.THESAURUSURI_TYPE, exclude, codes));
        }
    }

    private static void initCorpusReductionList(SearchOptionsDefBuilder searchOptionsDefBuilder, DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler) {
        String param = requestMap.getParameter(LIST_CORPUS);
        if (param == null) {
            param = requestMap.getParameter("codescorpus");
            if (param == null) {
                return;
            } else {
                warningHandler.setCurrentParameter("codescorpus");
            }
        } else {
            warningHandler.setCurrentParameter(LIST_CORPUS);
        }
        param = param.trim();
        if (param.length() == 0) {
            return;
        }
        boolean exclude = false;
        if (param.charAt(0) == '!') {
            exclude = true;
            param = param.substring(1).trim();
        }
        Collection<Integer> codes = toCodes(dataAccess, param, warningHandler, ScrutariDataURI.CORPUSURI_TYPE);
        if (codes != null) {
            searchOptionsDefBuilder.putListReduction(ListReductionBuilder.build(dataAccess, ScrutariDataURI.CORPUSURI_TYPE, exclude, codes));
        }
    }

    /**
     * Retourne null si la chaine est nulle ou blanche, si la chaine ne comprend
     * aucun caractère correct, retourne une liste vide
     */
    public static Collection<Integer> toCodes(DataAccess dataAccess, String values, WarningHandler warningHandler, short uriType) {
        if (values == null) {
            return null;
        }
        values = values.trim();
        if (values.length() == 0) {
            return null;
        }
        String[] tokens = StringUtils.getTechnicalTokens(values, false);
        int length = tokens.length;
        if (length == 0) {
            warningHandler.addWarning(WarningKeys.ONLYSEPARATORS_PARAMETER_VALUE, values);
            return PrimUtils.EMPTY_LIST;
        }
        Set<Integer> codeSet = new LinkedHashSet<Integer>();
        for (int i = 0; i < length; i++) {
            Integer code = CodeParser.toCode(dataAccess, tokens[i], warningHandler.getCodeMessageHandler(), uriType);
            if (code != null) {
                codeSet.add(code);
            }
        }
        return codeSet;
    }

    public static InsertOptions checkInsertOptions(RequestMap requestMap, InsertOptions insertOptions) {
        String optionsString = requestMap.getParameter(INSERT);
        if (optionsString != null) {
            insertOptions.parseOptions(optionsString);
        }
        String oldIntitules = requestMap.getParameter("intitules");
        if ((oldIntitules != null) && (oldIntitules.length() > 0)) {
            insertOptions.setWith(InsertOptions.ENGINEINFO, true);
        }
        return insertOptions;
    }

    public static FieldVariant getFieldVariant(FieldVariantManager fieldVariantManager, RequestMap requestMap, String defaultVariantName) {
        String ficheFields = requestMap.getParameter(FIELDS_FICHE);
        if (ficheFields == null) {
            ficheFields = requestMap.getParameter("prop");
        }
        String motcleFields = requestMap.getParameter(FIELDS_MOTCLE);
        if ((ficheFields != null) || (motcleFields != null)) {
            FieldVariantBuilder builder = new FieldVariantBuilder("");
            if (ficheFields != null) {
                builder.parseFicheFields(ficheFields);
                for (String paramName : requestMap.getParameterNameSet()) {
                    if (paramName.startsWith(FIELD_PREFIX)) {
                        String field = paramName.substring(FIELD_PREFIX.length());
                        String value = requestMap.getParameter(paramName);
                        AliasBuilder aliasBuilder = builder.getAliasBuilder(value);
                        int coreField = FieldVariant.checkCoreFicheFieldString(field);
                        if (coreField != -1) {
                            aliasBuilder.addCoreField(coreField);
                            builder.setFicheWith(coreField, false);
                        } else {
                            try {
                                AttributeKey attributeKey = AttributeKey.parse(field);
                                aliasBuilder.addAttributeField(attributeKey);
                            } catch (ParseException pe) {

                            }
                        }
                    }
                }
            }
            if (motcleFields != null) {
                builder.parseMotcleFields(motcleFields);
            }
            return builder.toFieldVariant();
        }
        String fieldVariantName = requestMap.getParameter(FIELDVARIANT);
        if (fieldVariantName != null) {
            FieldVariant fieldVariant = fieldVariantManager.getFieldVariant(fieldVariantName);
            if (fieldVariant != null) {
                return fieldVariant;
            }
        }
        FieldVariant fieldVariant = fieldVariantManager.getFieldVariant(defaultVariantName);
        if (fieldVariant != null) {
            return fieldVariant;
        } else {
            return fieldVariantManager.getFieldVariant(FieldVariantManager.EMPTY_VARIANT);
        }
    }

    public static boolean ignoreAlternate(RequestMap requestMap) {
        String ignoreValue = requestMap.getParameter(Parameters.IGNORE);
        if (ignoreValue == null) {
            return false;
        }
        return ignoreValue.equals(Parameters.IGNORE_VALUE_ALTERNATE);
    }


    private static class SearchMessageHandler implements MessageHandler {

        private final String sourceString;
        private final String parameter;
        private final CommandMessageBuilder commandMessageBuilder = new CommandMessageBuilder();

        private SearchMessageHandler(String parameter, String sourceString) {
            this.parameter = parameter;
            this.sourceString = sourceString;
        }

        @Override
        public void addMessage(String category, Message message) {
            commandMessageBuilder.addMultiError(message);
        }

        private boolean hasError() {
            return commandMessageBuilder.hasMultiError();
        }

        private CommandMessage toErrorMessage() {
            commandMessageBuilder.setError(ParameterErrorMessageKeys.WRONG_OPERATION_SYNTAX, parameter, sourceString);
            return commandMessageBuilder.toCommandMessage();
        }

    }


    private static class InternalSearchStoragePolicy implements SearchStoragePolicy {

        private final boolean store;
        private final boolean alwaysLog;
        private final String origin;


        private InternalSearchStoragePolicy(boolean store, boolean alwaysLog, String origin) {
            this.store = store;
            this.alwaysLog = alwaysLog;
            this.origin = origin;
        }

        @Override
        public boolean store() {
            return store;
        }

        @Override
        public boolean alwaysLog() {
            return alwaysLog;
        }

        @Override
        public String getOrigin() {
            return origin;
        }

    }

}
