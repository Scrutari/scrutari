/* SctServer - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request;


/**
 *
 * @author Vincent Calame
 */
public interface Parameters {

    public final static String ALL = "all";
    public final static String FIELD_PREFIX = "field-";
    public final static String FIELDS_FICHE = "fichefields";
    public final static String FIELDS_MOTCLE = "motclefields";
    public final static String FIELDVARIANT = "fieldvariant";
    public final static String FILTER = "flt";
    public final static String FILTER_ADD = "flt-add";
    public final static String FILTER_ATTR = "flt-attr";
    public final static String FILTER_BASE = "flt-base";
    public final static String FILTER_BBOX = "flt-bbox";
    public final static String FILTER_CIRCLE = "flt-circle";
    public final static String FILTER_CORPUS = "flt-corpus";
    public final static String FILTER_DATE = "flt-date";
    public final static String FILTER_LANG = "flt-lang";
    public final static String FILTER_MOTCLE = "flt-motcle";
    public final static String FILTER_PREFIX = "flt-";
    public final static String FILTER_QID = "flt-qid";
    public final static String FILTER_THESAURUS = "flt-thesaurus";
    public final static String IGNORE = "ignore";
    public final static String IGNORE_VALUE_ALTERNATE = "alternate";
    public final static String INFO = "info";
    public final static String INSERT = "insert";
    public final static String LANG = "lang";
    public final static String LIMIT = "limit";
    public final static String LIST_BASE = "baselist";
    public final static String LIST_CATEGORY = "categorylist";
    public final static String LIST_CORPUS = "corpuslist";
    public final static String LIST_FICHE = "fichelist";
    public final static String LIST_LANG = "langlist";
    public final static String LIST_MOTCLE = "motclelist";
    public final static String LIST_THESAURUS = "thesauruslist";
    public final static String LOG = "log";
    public final static String MODE = "mode";
    public final static String NAME = "name";
    public final static String ORIGIN = "origin";
    public final static String PONDERATION = "ponderation";
    public final static String Q = "q";
    public final static String Q_ID = "qid";
    public final static String Q_MODE = "q-mode";
    public final static String Q_MODE_PREFIX = "q-mode_";
    public final static String Q_MODE_VALUE_UNION = "union";
    public final static String Q_MODE_VALUE_INTERSECTION = "intersection";
    public final static String Q_MODE_VALUE_OPERATION = "operation";
    public final static String Q_PREFIX = "q_";
    public final static String Q_SCOPE_PREFIX = "q-scope_";
    public final static String RANDOM = "random";
    public final static String START = "start";
    public final static String STARTTYPE = "starttype";
    public final static String STARTTYPE_VALUE_GLOBAL = "global";
    public final static String STARTTYPE_VALUE_IN_ALL = "in_all";
    public final static String STARTTYPE_VALUE_IN_PREFIX = "in:";
    public final static String STORE = "store";
    public final static String TYPE = "type";
    public final static String VERSION = "version";
    public final static String WARNINGS = "warnings";
    public final static String WILDCHAR = "wildchar";
    public final static String WILDCHAR_VALUE_BOTH = "both";
    public final static String WILDCHAR_VALUE_END = "end";
    public final static String WILDCHAR_VALUE_NONE = "none";
    public final static String WILDCHAR_VALUE_START = "start";

}
