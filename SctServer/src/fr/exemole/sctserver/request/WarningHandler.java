/* SctServer - Copyright (c) 2011-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.MessageHandler;


/**
 *
 * @author Vincent Calame
 */
public class WarningHandler {

    private final List<Warning> warningList = new ArrayList<Warning>();
    private final CodeMessageHandler codeMessageHandler = new CodeMessageHandler();
    private String currentParameter;

    public WarningHandler() {
    }

    public boolean isEmpty() {
        return (warningList.isEmpty());
    }

    public void setCurrentParameter(String parameter) {
        this.currentParameter = parameter;
    }

    public void addWarning(String warningKey, Object... values) {
        warningList.add(new Warning(currentParameter, warningKey, values));
    }

    public List<Warning> getWarningList() {
        return warningList;
    }

    public MessageHandler getCodeMessageHandler() {
        return codeMessageHandler;
    }


    public static class Warning {

        private final String warningParameter;
        private final String warningKey;
        private final Object[] warningValues;

        private Warning(String warningParameter, String warningKey, Object[] warningValues) {
            this.warningParameter = warningParameter;
            this.warningKey = warningKey;
            this.warningValues = warningValues;
        }

        public String getWarningParameter() {
            return warningParameter;
        }

        public String getWarningKey() {
            return warningKey;
        }

        public Object[] getWarningValues() {
            return warningValues;
        }

    }


    private class CodeMessageHandler implements MessageHandler {

        private CodeMessageHandler() {
        }

        @Override
        public void addMessage(String category, Message message) {
            String key = message.getMessageKey();
            Object[] values = message.getMessageValues();
            if (key.equals("_ error.code.wrong.uritype")) {
                addWarning(WarningKeys.WRONG_SCRUTARIDATATYPE_PARAMETER_VALUE, values);
            } else if (key.contains(".unknown.")) {
                addWarning(WarningKeys.UNKNOWN_PARAMETER_VALUE, values);
            } else if (key.contains(".malformed.")) {
                addWarning(WarningKeys.MALFORMED_PARAMETER_VALUE, values);
            }
        }

    }

}
