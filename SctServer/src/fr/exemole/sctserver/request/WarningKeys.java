/* SctServer - Copyright (c) 2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.request;


/**
 *
 * @author Vincent Calame
 */
public interface WarningKeys {

    public final static String MALFORMED_PARAMETER_VALUE = "malformedParameterValue";
    public final static String UNKNOWN_PARAMETER_VALUE = "unknownParameterValue";
    public final static String WRONG_SCRUTARIDATATYPE_PARAMETER_VALUE = "wrongDataTypeParameterValue";
    /**
     * Cas d'un paramètre avec des séparateurs et rien entre les deux (exemple :
     * ,, ,,)
     */
    public final static String ONLYSEPARATORS_PARAMETER_VALUE = "onlySeparatorsParameterValue";
    public final static String OBSOLETE_PARAMETER_NAME = "obsoleteParameterName";
    public final static String UNKNOW_OPERAND_TYPE = "unknownOperandType";
}
