/* ScrutariLib_SearchEngine - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LocalisationLexieFilters;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.mapeadores.util.text.TextConstants;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.mapeadores.util.text.collation.map.ValueFilter;
import net.mapeadores.util.text.search.SearchToken;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchOperationReduction;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.SupermotcleSearchResult;
import net.scrutari.searchengine.api.result.SupermotcleSearchResultInfo;
import net.scrutari.searchengine.tools.operands.search.SearchOperationReductionBuilder;
import net.scrutari.searchengine.tools.result.ResultUtils;
import net.scrutari.searchengine.tools.result.SupermotcleSearchResultBuilder;
import net.scrutari.searchengine.tools.result.SupermotcleSearchResultInfoBuilder;
import net.scrutari.searchengine.tools.result.SupermotcleSearchResultInfoComparator;
import net.scrutari.supermotcle.api.Supermotcle;
import net.scrutari.supermotcle.api.SupermotcleHolder;
import net.scrutari.supermotcle.api.SupermotcleProvider;


/**
 *
 * @author Vincent Calame
 */
public final class SupermotcleSearchEngine {

    private SupermotcleSearchEngine() {
    }

    public static SupermotcleSearchResult search(SupermotcleProvider supermotcleProvider, SearchOperation searchOperation, SearchOptionsDef searchOptionsDef) {
        Langs langs = searchOptionsDef.getFilterLangs();
        if (langs.isEmpty()) {
            throw new IllegalArgumentException("searchOptionsDef.getFilterLangs() cannot be empty");
        }
        Lang lang = langs.get(0);
        SupermotcleHolder supermotcleHolder = supermotcleProvider.getSupermotcleHolder(lang);
        if (supermotcleHolder == null) {
            return ResultUtils.EMPTY_SUPERMOTCLE_SEARCHRESULT;
        }
        SearchOperationReduction reduction = SearchOperationReductionBuilder.lexieReduction(searchOperation, LocalisationLexieFilters.getLexieFilter(lang));
        Operand operand = reduction.getRootOperand();
        if (operand == null) {
            return ResultUtils.EMPTY_SUPERMOTCLE_SEARCHRESULT;
        }
        SearchSequenceEngine searchSequenceEngine = new SearchSequenceEngine(supermotcleHolder, searchOperation.getSearchTokenOperandList().size());
        seekOperand(searchSequenceEngine, operand);
        return searchSequenceEngine.getResult();
    }

    private static boolean seekOperand(SearchSequenceEngine searchOperationEngine, Operand operand) {
        if (operand instanceof SubOperand) {
            return seekSubOperand(searchOperationEngine, (SubOperand) operand);
        } else {
            SearchTokenOperand searchOperand = (SearchTokenOperand) operand;
            int operandNumber = searchOperand.getOperandNumber();
            SearchToken token = searchOperand.getSearchToken();
            if (token instanceof SimpleSearchToken) {
                SimpleSearchToken simpleToken = (SimpleSearchToken) token;
                boolean suite = searchOperationEngine.seekToken(simpleToken, operandNumber);
                if (!suite) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        }
    }

    private static boolean seekSubOperand(SearchSequenceEngine searchOperationEngine, SubOperand subOperand) {
        int count = subOperand.size();
        for (int i = 0; i < count; i++) {
            boolean next = seekOperand(searchOperationEngine, subOperand.get(i));
            if (!next) {
                return false;
            }
        }
        return true;
    }


    private static class SearchSequenceEngine {

        private final SupermotcleHolder supermotcleHolder;
        private final InternalValueFilter valueFilter = new InternalValueFilter();
        private final int operandMaxNumber;
        private Map<Integer, SupermotcleSearchResultInfoBuilder> previousMap;
        private boolean empty = false;


        private SearchSequenceEngine(SupermotcleHolder supermotcleHolder, int operandMaxNumber) {
            this.supermotcleHolder = supermotcleHolder;
            this.operandMaxNumber = operandMaxNumber;
        }

        private boolean seekToken(SimpleSearchToken token, int operandNumber) {
            List<SearchResultUnit<Supermotcle>> list = supermotcleHolder.search(token.getTokenString(), TextConstants.CONTAINS, valueFilter);
            if (list.isEmpty()) {
                empty = true;
                return false;
            }
            Map<Integer, SupermotcleSearchResultInfoBuilder> currentMap = new HashMap<Integer, SupermotcleSearchResultInfoBuilder>();
            int unitLength = list.size();
            for (int k = 0; k < unitLength; k++) {
                SearchResultUnit<Supermotcle> searchResultUnit = list.get(k);
                Supermotcle supermotcle = searchResultUnit.getValue();
                Integer code = supermotcle.getCode();
                SupermotcleSearchResultInfoBuilder currentBuilder;
                if (previousMap == null) {
                    currentBuilder = new SupermotcleSearchResultInfoBuilder(supermotcle, operandMaxNumber);
                } else {
                    currentBuilder = previousMap.get(code); //existe du fait de ValueFilter

                }
                currentBuilder.addSearchResultUnit(operandNumber, searchResultUnit);
                currentMap.put(code, currentBuilder);
            }
            if (currentMap.isEmpty()) {
                empty = true;
                return false;
            }
            previousMap = currentMap;
            return true;
        }

        private SupermotcleSearchResult getResult() {
            if ((empty) || (previousMap == null)) {
                return ResultUtils.EMPTY_SUPERMOTCLE_SEARCHRESULT;
            }
            TreeSet<SupermotcleSearchResultInfo> sortedSet = new TreeSet<SupermotcleSearchResultInfo>(SupermotcleSearchResultInfoComparator.UNIQUE);
            for (SupermotcleSearchResultInfoBuilder builder : previousMap.values()) {
                sortedSet.add(builder.toSupermotcleSearchResultInfo());
            }
            int size = sortedSet.size();
            SupermotcleSearchResultInfo[] array = new SupermotcleSearchResultInfo[size];
            int p = (size - 1);
            for (SupermotcleSearchResultInfo info : sortedSet) {
                array[p] = info;
                p--;
            }
            return SupermotcleSearchResultBuilder.toSupermotcleSearchResult(array);
        }


        private class InternalValueFilter implements ValueFilter {

            private InternalValueFilter() {
            }

            @Override
            public boolean acceptValue(Object value) {
                if (previousMap == null) {
                    return true;
                }
                return previousMap.containsKey(((Supermotcle) value).getCode());
            }

        }

    }

}
