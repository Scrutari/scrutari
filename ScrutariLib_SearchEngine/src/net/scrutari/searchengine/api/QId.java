/* ScrutariLib_DB - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api;

import java.io.IOException;
import java.text.ParseException;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public final class QId implements Comparable<QId> {

    private final ScrutariDBName scrutariDBName;
    private final int subid;
    private final String qidString;

    public QId(ScrutariDBName scrutariDBName, int subid) {
        if (scrutariDBName == null) {
            throw new IllegalArgumentException("scrutariDBName is null");
        }
        if (subid < 1) {
            throw new IllegalArgumentException("subid < 1");
        }
        this.scrutariDBName = scrutariDBName;
        this.subid = subid;
        this.qidString = scrutariDBName.getName() + "-" + subid;
    }

    public ScrutariDBName getScrutariDBName() {
        return scrutariDBName;
    }

    public int getSubId() {
        return subid;
    }

    @Override
    public int hashCode() {
        return qidString.hashCode();
    }

    @Override
    public String toString() {
        return qidString;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        QId otherQId = (QId) other;
        return this.qidString.equals(otherQId.qidString);
    }

    @Override
    public int compareTo(QId otherQId) {
        return this.qidString.compareTo(otherQId.qidString);
    }

    public static QId parse(String s) throws ParseException {
        int idx = s.lastIndexOf('-');
        if (idx == -1) {
            throw new ParseException("- is missing", 0);
        }
        ScrutariDBName scrutariDBName = ScrutariDBName.parse(s.substring(0, idx));
        int subid;
        try {
            subid = Integer.parseInt(s.substring(idx + 1));
        } catch (NumberFormatException nfe) {
            throw new ParseException("Not integer", idx + 1);
        }
        if (subid < 1) {
            throw new ParseException("Negative integer", idx + 1);
        }
        return new QId(scrutariDBName, subid);
    }

    public static QId fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        try {
            ScrutariDBName scrutariDBName = ScrutariDBName.parse(primitivesReader.readString());
            int subid = primitivesReader.readInt();
            return new QId(scrutariDBName, subid);
        } catch (ParseException pe) {
            throw new IOException("Unable to parse scrutariDBName");
        }
    }

    public static void toPrimitives(PrimitivesWriter primitivesWriter, QId qId) throws IOException {
        primitivesWriter.writeString(qId.scrutariDBName.getName());
        primitivesWriter.writeInt(qId.subid);
    }

}
