/* ScrutariLib_SearchEngine - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api;

import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public final class CanonicalQ implements CharSequence {

    public final static CanonicalQ EMPTY = new CanonicalQ("");
    private final String string;

    private CanonicalQ(String string) {
        this.string = string;
    }

    @Override
    public char charAt(int index) {
        return string.charAt(index);
    }

    @Override
    public int length() {
        return string.length();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return string.subSequence(start, end);
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public int hashCode() {
        return string.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        CanonicalQ otherCanonicalQ = (CanonicalQ) other;
        return otherCanonicalQ.string.equals(this.string);
    }

    public static CanonicalQ newInstance(CharSequence cs) {
        return newInstance(CleanedString.newInstance(cs));
    }

    public static CanonicalQ newInstance(CleanedString cleanedString) {
        if (cleanedString == null) {
            return EMPTY;
        } else {
            return new CanonicalQ(cleanedString.toString());
        }
    }

}
