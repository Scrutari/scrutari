/* ScrutariLib_SearchEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.pertinence;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;
import net.scrutari.lexie.AlineaRank;


/**
 *
 * @author Vincent Calame
 */
public interface FichePertinence {

    public Integer getFicheCode();

    public Integer getCorpusCode();

    public Integer getBaseCode();

    public Lang getFicheLang();

    public short getOperandState(int operandNumber);

    public AlineaRank getBestAlineaRank(int operandNumber);

    @Nullable
    public SubstringPosition getSubstringPosition(int operandNumber);

    /**
     * Compris entre 0 et 1
     */
    public float getMotcleScore(int operandNumber);

    public List<Integer> getAcceptMotcleCodeList();

}
