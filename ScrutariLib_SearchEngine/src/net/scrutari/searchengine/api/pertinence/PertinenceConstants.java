/* ScrutariLib_SearchEngine - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.pertinence;


/**
 *
 * @author Vincent Calame
 */
public interface PertinenceConstants {

    public final static short OCCURRENCE_STATE = 1;
    public final static short NOPERTINENT_STATE = 0;
    public final static short NO_OCCURRENCE_STATE = -1;
}
