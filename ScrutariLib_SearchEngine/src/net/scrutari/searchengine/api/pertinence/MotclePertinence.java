/* ScrutariLib_SearchEngine - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.pertinence;

import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;


/**
 *
 * @author Vincent Calame
 */
public interface MotclePertinence {

    public Integer getMotcleCode();

    public Integer getThesaurusCode();

    public int getScore();

    public short getOperandState(int operandNumber);

    public List<OccurrenceByLang> getOccurrenceByLangList();


    public interface OccurrenceByLang {

        public Lang getLang();

        public SubstringPosition getSubstringPosition(int operandNumber);

    }


}
