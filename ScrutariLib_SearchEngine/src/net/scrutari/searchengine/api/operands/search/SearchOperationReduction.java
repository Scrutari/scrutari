/* ScrutariLib_SearchEngine - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands.search;

import java.util.List;
import net.mapeadores.util.logicaloperation.Operand;


/**
 *
 * @author Vincent Calame
 */
public interface SearchOperationReduction {

    public SearchOperation getOriginal();

    public Operand getRootOperand();

    /**
     * Retourne la liste des nombres compris d'entre 1 et getOriginal().size()
     * qui ne sont pas utilisé
     */
    public List<Integer> getUnusedNumberList();

}
