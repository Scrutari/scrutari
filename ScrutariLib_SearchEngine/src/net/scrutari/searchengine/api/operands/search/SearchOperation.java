/* ScrutariLib_SearchEngine - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands.search;

import java.util.List;
import net.mapeadores.util.logicaloperation.Operand;
import net.scrutari.searchengine.api.CanonicalQ;


/**
 *
 * @author Vincent Calame
 */
public interface SearchOperation {

    /**
     * Instance de SubOperand, SearchTokenOperand ou AllOperand. Les instances
     * de SubOperand ne doivent contenir également que des instances de
     * SubOperand et SearchTokenOperand (et pas de AllOperand).
     */
    public Operand getRootOperand();

    /**
     * Liste à plat des opérandes de rercherche contenu dans getRootOperand
     */
    public List<SearchTokenOperand> getSearchTokenOperandList();

    public CanonicalQ getCanonicalQ();

}
