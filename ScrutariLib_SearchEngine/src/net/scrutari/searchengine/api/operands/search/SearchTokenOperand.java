/* ScrutariLib_SearchEngine - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands.search;

import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.text.search.SearchToken;


/**
 *
 * @author Vincent Calame
 */
public interface SearchTokenOperand extends Operand {

    public SearchToken getSearchToken();

    public boolean isAcceptMode();

    public int getOperandNumber();
    
    public AlineaFilter getAlineaFilter();

}
