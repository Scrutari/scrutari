/* ScrutariLib_SearchEngine - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands;


/**
 *
 * @author Vincent Calame
 */
public interface OperandMessageKeys {

    public final static String SEVERE_OPERAND = "severe.operand";
    public final static String UNKNOWN_OPERAND_TYPE = "_ error.unknown.operandtype";
    public final static String UNKNOWN_FICHESEARCHRESULT = "_ error.unknown.fichesearchresult";
    public final static String UNKNOWN_ATTRIBUTE = "_ error.unknown.attribute";
    public final static String UNKNWON_SCOPE = "_ error.unknown.scope";
    public final static String WRONG_NOT_ALINEA_ATTRIBUTE = "_ error.wrong.notalineaattribute";
    public final static String EMPTY_SEARCHTOKEN = "_ error.empty.searchtoken";
    public final static String WRONG_NOT_LETTERORDIGIT_CHARACTER = "_ error.wrong.notletterordigitcharacter";
    public final static String WRONG_MISSING_WORD = "_ error.wrong.missingword";
    public final static String WRONG_UNCLOSED_QUOTE = "_ error.wrong.unclosedquote";
    public final static String WRONG_UNEXPECTED_SPACE = "_ error.wrong.unexpectedspace";
    public final static String WRONG_ADD_OPERAND = "_ error.wrong.operand_add";
    public final static String WRONG_BBOX_OPERAND = "_ error.wrong.operand_bbox";
    public final static String WRONG_CIRCLE_OPERAND = "_ error.wrong.operand_circle";
    public final static String WRONG_LANG_OPERAND = "_ error.wrong.operand_lang";
    public final static String WRONG_DATE_OPERAND = "_ error.wrong.operand_date";
    public final static String WRONG_QID_OPERAND = "_ error.wrong.operand_qid";
}
