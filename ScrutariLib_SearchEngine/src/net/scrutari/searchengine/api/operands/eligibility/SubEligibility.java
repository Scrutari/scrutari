/* ScrutariLib_SearchEngine - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands.eligibility;


/**
 *
 * @author Vincent Calame
 */
public interface SubEligibility extends Eligibility {

    public short getOperator();

    public int getEligibilityCount();

    public Eligibility getEligibility(int i);

}
