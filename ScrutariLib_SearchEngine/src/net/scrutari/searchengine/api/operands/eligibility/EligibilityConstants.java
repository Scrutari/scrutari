/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands.eligibility;


/**
 *
 * @author Vincent Calame
 */
public interface EligibilityConstants {

    public final static String MOTCLE_SCOPE = "motcle";
    public final static String THESAURUS_SCOPE = "thesaurus";
    public final static String DATE_SCOPE = "date";
    public final static String QID_SCOPE = "qid";
    public final static String ADD_SCOPE = "add";
    public final static String BBOX_SCOPE = "bbox";
    public final static String CIRCLE_SCOPE = "circle";
    public final static String CORPUS_SCOPE = "corpus";
    public final static String BASE_SCOPE = "base";
    public final static String LANG_SCOPE = "lang";

}
