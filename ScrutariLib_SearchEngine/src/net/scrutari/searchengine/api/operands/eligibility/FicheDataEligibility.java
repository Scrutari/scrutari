/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.operands.eligibility;

import net.scrutari.data.FicheData;


/**
 *
 * @author Vincent Calame
 */
public interface FicheDataEligibility extends Eligibility {

    /**
     * FicheData peut être nul.
     */
    public boolean acceptFicheData(FicheData ficheData);

}
