/* ScrutariLib_SearchEngine - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.lexieseek;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.result.SearchTokenReport;


/**
 *
 * @author Vincent Calame
 */
public interface LexieSeekHandler {

    public void addAlineaOccurrence(Integer ficheCode, Integer corpusCode, Integer baseCode, Lang lang, int operandNumber, AlineaRank alineaRank, SubstringPosition sourcePosition);

    public void addLangOccurrence(Integer motcleCode, Integer thesaurusCode, Lang lang, int operandNumber, SubstringPosition sourcePosition, float coverRate);

    public void addSearchTokenReport(SearchTokenReport searchTokenReport);

}
