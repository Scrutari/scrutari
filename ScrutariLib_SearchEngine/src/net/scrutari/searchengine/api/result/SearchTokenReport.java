/* ScrutariLib_SearchEngine - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import java.util.List;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface SearchTokenReport {

    public int getOperandNumber();

    public String getOperandString();

    public List<CanonicalReport> getCanonicalReportList();

    public List<NeighbourReport> getNeighbourReportList();


    public interface CanonicalReport {

        public String getCanonical();

        public List<LangReport> getLangReportList();

    }


    public interface NeighbourReport {

        public String getNeighbour();

        public List<Lang> getLangList();

    }


    public interface LangReport {

        public Lang getLang();

        public int getFicheCount();

        public int getMotcleCount();

    }

}
