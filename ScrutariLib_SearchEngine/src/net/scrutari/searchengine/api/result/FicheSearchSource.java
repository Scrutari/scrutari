/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import net.mapeadores.util.annotation.Nullable;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.options.SearchOptionsDef;


/**
 *
 * @author Vincent Calame
 */
public interface FicheSearchSource {

    /**
     * Peut-être nul
     */
    @Nullable
    public QId getQId();

    /**
     * Ne doit pas être nul
     */
    public CanonicalQ getCanonicalQ();

    /**
     * Ne doit pas être nul
     */
    public SearchOptionsDef getSearchOptionsDef();

}
