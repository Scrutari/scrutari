/* ScrutariLib_SearchEngine - Copyright (c) 2010-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleSearchResult {

    public List<MotcleSearchResultInfo> getMotcleSearchResultInfoList();

}
