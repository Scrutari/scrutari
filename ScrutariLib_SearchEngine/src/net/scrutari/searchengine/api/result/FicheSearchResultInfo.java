/* ScrutariLib_SearchEngine - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import java.util.List;
import net.scrutari.lexie.AlineaRank;


/**
 *
 * @author Vincent Calame
 */
public interface FicheSearchResultInfo {

    /**
     * Retourne le code de la fiche.
     */
    public Integer getCode();

    public List<Integer> getMotcleCodeList();

    public List<InAlineaOccurrences> getInAlineaOccurrencesList();


    /**
     * Retourne la liste des occurences de recherche dans l'alinéa de la fiche
     * indiquée par <em>alineaRank</em>. Retourne <em>null</em> s'il n'y a pas
     * de mot pour ce champ-là.
     */
    public InAlineaOccurrences getInAlineaOccurrencesByAlinea(AlineaRank alineaRank);

    /**
     * Tableau de valeurs comprises entre 0 et 1 000: le score pour les
     * occurences, le score pour les dates, le score pour l'origine, le score
     * pour la langue. Si le tableau est de longueur inférieure, cela correspond
     * à une valeur nulle pour le score en question
     */
    public int[] getScoreArray();

}
