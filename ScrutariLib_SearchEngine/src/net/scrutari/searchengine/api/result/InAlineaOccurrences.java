/* ScrutariLib_SearchEngine - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import java.util.List;
import net.scrutari.lexie.AlineaRank;


/**
 *
 * @author Vincent Calame
 */
public interface InAlineaOccurrences {

    public AlineaRank getAlineaRank();

    public List<SearchTokenOccurrence> getSearchTokenOccurrenceList();

}
