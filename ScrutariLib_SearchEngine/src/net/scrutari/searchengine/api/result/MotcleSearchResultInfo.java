/* ScrutariLib_SearchEngine - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import java.util.List;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleSearchResultInfo {

    /**
     * Retourne le code du mot-clé.
     */
    public Integer getCode();

    public int getScore();

    public List<InLangOccurrences> getInLangOccurrencesList();

    /**
     * Retourne la liste des instances de InLangOccurrences indiquant la
     * présence de mots de la recherche dans le libellé de langue lang. Retourne
     * <em>null</em> si le libellé dans la langue donnée n'a pas participé à la
     * recherche. L'instance de InLangOccurrences retourné est donc soit nul,
     * soit de taille supérieur à 0.
     */
    public default InLangOccurrences getInLangOccurrencesByLang(Lang lang) {
        for (InLangOccurrences inLangOccurrences : getInLangOccurrencesList()) {
            if (inLangOccurrences.getLang().equals(lang)) {
                return inLangOccurrences;
            }
        }
        return null;
    }

}
