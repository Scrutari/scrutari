/* ScrutariLib_SearchEngine - Copyright (c) 2008-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.result;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface FicheSearchResult {

    public FicheSearchSource getFicheSearchSource();

    /**
     * Retourne le nombre de fiches dans le résultat de recherche.
     */
    public int getFicheCount();

    /**
     * Retourne le nombre maximum de fiches qui auraient pu se trouver dans le
     * résultat de recherche. Autrement dit, c'est le nombre de fiches qui
     * répondent aux options de recherche (limitation à certaines bases ou
     * corpus) sur lesquelles ont a fait réellement une recherche de présence de
     * mots.
     */
    public int getFicheMaximum();

    public List<FicheSearchResultGroup> getFicheSearchResultGroupList();

    public List<MotcleSearchResultInfo> getMotcleSearchResultInfoList();

    public List<SearchTokenReport> getSearchTokenReportList();

}
