/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import java.util.List;
import net.scrutari.datauri.ScrutariDataURI;


/**
 *
 * @author Vincent Calame
 */
public interface ListReduction {

    /**
     * Les élements d'une liste sont forcément du même type.
     */
    public short getType();

    public boolean isExclude();

    public List<ScrutariDataURI> getScrutariDataURIList();

}
