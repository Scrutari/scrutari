/* ScrutariLib_SearchEngine - Copyright (c) 2011-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import java.util.List;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;
import net.scrutari.datauri.CorpusURI;


/**
 *
 * @author Vincent Calame
 */
public interface CategoryDef {

    public final static String DEFAULT_NAME = "_default";

    public String getName();

    public int getRank();

    public List<CorpusURI> getCorpusURIList();

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public Phrases getPhrases();

    public default String getTitle(Lang preferredLang) {
        return getTitleLabels().seekLabelString(preferredLang, getName());
    }

}
