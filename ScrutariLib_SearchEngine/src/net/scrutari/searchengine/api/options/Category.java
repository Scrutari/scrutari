/* ScrutariLib_SearchEngine - Copyright (c) 2014-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import java.util.List;
import net.scrutari.db.api.stats.CountStats;
import net.scrutari.db.api.stats.LangStats;


/**
 *
 * @author Vincent Calame
 */
public interface Category {

    public CategoryDef getCategoryDef();

    public CountStats getCountStats();

    public LangStats getFicheStats();

    public List<Integer> getCorpusCodeList();

}
