/* ScrutariLib_SearchEngine - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;


/**
 *
 * @author Vincent Calame
 */
public interface SearchOptionsDef {

    public boolean isEmpty();

    @Nullable
    public Lang getLang();

    /**
     * N'est jamais nul. size() == 0 indique l'absence de filtre sur la langue.
     */
    public Langs getFilterLangs();

    @Nullable
    public ListReduction getListReduction(short type);

    @Nullable
    public EligibilityOperand getFicheEligibilityOperand();

    @Nullable
    public PertinencePonderation getPertinencePonderation();

    public Attributes getAttributes();

}
