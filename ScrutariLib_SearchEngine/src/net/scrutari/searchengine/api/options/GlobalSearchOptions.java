/* ScrutariLib_SearchEngine - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface GlobalSearchOptions {

    public List<Category> getCategoryList();

    public Category getCategoryByRank(int rank);

    public Category getCategoryByName(String categoryName);

    public Category getCategoryByCorpus(Integer corpusCode);

    public PertinencePonderation getPertinencePonderation();

    public default boolean isWithCategory() {
        return !getCategoryList().isEmpty();
    }

}
