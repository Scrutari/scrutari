/* ScrutariLib_SearchEngine - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;


/**
 *
 * @author Vincent Calame
 */
public interface ComputedOptions {

    public SearchOptions getSearchOptions();

    public PertinencePonderation getPertinencePonderation();

    public boolean hasMandatoryOperand();

    public Lang[] getLangArray();

    public boolean isAllLang();

    public boolean isOnFiche();

    /**
     * Vide si isOnFiche = false
     */
    public CorpusData[] getCorpusDataArray();

    public boolean isOnMotcle();

    /**
     * Vide si isOnMotcle = false
     */
    public ThesaurusData[] getThesaurusDataArray();

    public boolean acceptFiche(Integer ficheCode);

    public boolean acceptMotcle(Integer motcleCode);

    public int getFicheMaximum();

}
