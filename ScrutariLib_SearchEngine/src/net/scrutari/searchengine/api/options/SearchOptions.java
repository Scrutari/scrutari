/* ScrutariLib_SearchEngine - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.api.options;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.scrutari.searchengine.api.operands.eligibility.Eligibility;


/**
 *
 * @author Vincent Calame
 */
public interface SearchOptions {

    public boolean isWithCorpusCodes();

    /**
     * Cette liste est une construction à partir des différentes options de la
     * recherche.
     */
    public List<Integer> getCorpusCodeList();

    public boolean isWithThesaurusCodes();

    /**
     * Cette liste est une construction à partir des différentes options de la
     * recherche.
     */
    public List<Integer> getThesaurusCodeList();

    @Nullable
    public Eligibility getFicheEligibility();

    public SearchOptionsDef getSearchOptionsDef();

}
