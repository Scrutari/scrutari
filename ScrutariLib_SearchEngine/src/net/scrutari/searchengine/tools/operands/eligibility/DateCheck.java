/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.text.ParseException;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.primitives.DateFilter;
import net.mapeadores.util.primitives.DateFilterFactory;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.FicheData;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;


/**
 *
 * @author Vincent Calame
 */
final class DateCheck {

    private final static FicheDataEligibility NULL_DATEOPERAND = new NullDateOperand(true);
    private final static FicheDataEligibility NOTNULL_DATEOPERAND = new NullDateOperand(false);

    private DateCheck() {
    }

    static EligibilityCheck check(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        FicheDataEligibility operand = toOperand(simpleOperand, eligibilityCheckParameters);
        if (operand == null) {
            return null;
        }
        return new EligibilityCheck(null, operand);
    }

    private static FicheDataEligibility toOperand(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        if (operandBody.equals("NULL")) {
            if (acceptMode) {
                return NULL_DATEOPERAND;
            } else {
                return NOTNULL_DATEOPERAND;
            }
        }
        DateFilter dateFilter = null;
        int idx = operandBody.indexOf('/');
        if (idx == - 1) {
            try {
                FuzzyDate date = FuzzyDate.parse(operandBody);
                dateFilter = DateFilterFactory.newInstance(date);
            } catch (ParseException pe) {
                eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_DATE_OPERAND, simpleOperand.getOperandString());
            }
        } else {
            String token1 = operandBody.substring(0, idx).trim();
            String token2 = operandBody.substring(idx + 1).trim();
            if (token1.equals("-")) {
                if (token2.equals("-")) {
                    if (!acceptMode) {
                        return NULL_DATEOPERAND;
                    } else {
                        return NOTNULL_DATEOPERAND;
                    }
                } else {
                    try {
                        FuzzyDate date = FuzzyDate.parse(token2);
                        dateFilter = DateFilterFactory.newMaximumFilter(date);
                    } catch (ParseException pe) {
                        eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_DATE_OPERAND, simpleOperand.getOperandString());
                    }
                }
            } else if (token2.equals("-")) {
                try {
                    FuzzyDate date = FuzzyDate.parse(token1);
                    dateFilter = DateFilterFactory.newMinimumFilter(date);
                } catch (ParseException pe) {
                    eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_DATE_OPERAND, simpleOperand.getOperandString());
                }
            } else {
                try {
                    FuzzyDate date1 = FuzzyDate.parse(token1);
                    FuzzyDate date2 = FuzzyDate.parse(token2);
                    dateFilter = DateFilterFactory.newInstance(date1, date2);
                } catch (ParseException pe) {
                    eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_DATE_OPERAND, simpleOperand.getOperandString());
                }
            }
        }
        if (dateFilter != null) {
            return new DateOperand(dateFilter, acceptMode, operandBody);
        } else {
            return null;
        }
    }


    private static class NullDateOperand implements FicheDataEligibility {

        private final boolean acceptMode;

        private NullDateOperand(boolean acceptMode) {
            this.acceptMode = acceptMode;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return acceptMode;
            }
            boolean test = (ficheData.getDate() == null);
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.DATE_SCOPE + ":NULL";
            } else {
                return "!" + EligibilityConstants.DATE_SCOPE + ":NULL";
            }
        }

    }


    private static class DateOperand implements FicheDataEligibility {

        private final boolean acceptMode;
        private final DateFilter filter;
        private final String operandBody;

        private DateOperand(DateFilter filter, boolean acceptMode, String operandBody) {
            this.filter = filter;
            this.acceptMode = acceptMode;
            this.operandBody = operandBody;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return !acceptMode;
            }
            FuzzyDate date = ficheData.getDate();
            boolean test;
            if (date == null) {
                test = false;
            } else {
                test = filter.containsDate(date);
            }
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.DATE_SCOPE + ":" + operandBody;
            } else {
                return "!" + EligibilityConstants.DATE_SCOPE + ":" + operandBody;
            }
        }

    }

}
