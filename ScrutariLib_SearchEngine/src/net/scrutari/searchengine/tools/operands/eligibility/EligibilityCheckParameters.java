/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import net.mapeadores.util.logging.MessageHandler;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.URITreeProvider;
import net.scrutari.searchengine.api.result.FicheSearchResultHolder;
import net.scrutari.searchengine.tools.operands.search.SearchOperationUtils;


/**
 *
 * @author Vincent Calame
 */
public class EligibilityCheckParameters {

    private DataAccess dataAccess;
    private FicheSearchResultHolder ficheSearchResultHolder;
    private MessageHandler messageHandler;
    private URITreeProvider uriTreeProvider;

    public EligibilityCheckParameters() {
    }

    public DataAccess getDataAccess() {
        return dataAccess;
    }

    public EligibilityCheckParameters setDataAccess(DataAccess dataAccess) {
        this.dataAccess = dataAccess;
        return this;
    }

    public FicheSearchResultHolder getFicheSearchResultHolder() {
        return ficheSearchResultHolder;
    }

    public EligibilityCheckParameters setFicheSearchResultHolder(FicheSearchResultHolder ficheSearchResultHolder) {
        this.ficheSearchResultHolder = ficheSearchResultHolder;
        return this;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public EligibilityCheckParameters setMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        return this;
    }

    public URITreeProvider getUriTreeProvider() {
        return uriTreeProvider;
    }

    public EligibilityCheckParameters setUriTreeProvider(URITreeProvider uriTreeProvider) {
        this.uriTreeProvider = uriTreeProvider;
        return this;
    }

    void addError(String key, Object value) {
        SearchOperationUtils.error(messageHandler, key, value);
    }

}
