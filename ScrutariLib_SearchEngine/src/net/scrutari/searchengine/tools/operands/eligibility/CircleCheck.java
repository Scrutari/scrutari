/* ScrutariLib_SearchEngine - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.data.FicheData;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;


/**
 *
 * @author Vincent Calame
 */
final class CircleCheck {

    private final static double EARTH_RADIUS = 6378;

    private CircleCheck() {

    }

    static EligibilityCheck check(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        FicheDataEligibility operand = toOperand(simpleOperand, eligibilityCheckParameters);
        if (operand == null) {
            return null;
        }
        return new EligibilityCheck(null, operand);
    }

    private static FicheDataEligibility toOperand(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        String[] tokens = StringUtils.getTokens(operandBody, ',', StringUtils.EMPTY_INCLUDE);
        int length = tokens.length;
        if (length != 3) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_CIRCLE_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        DegreDecimal lat, lon;
        Decimal radius;
        try {
            lat = DegreDecimal.newInstance(StringUtils.parseDecimal(tokens[0]));
            lon = DegreDecimal.newInstance(StringUtils.parseDecimal(tokens[1]));
            radius = StringUtils.parseDecimal(tokens[2]);
        } catch (NumberFormatException nfe) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_CIRCLE_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        if (!radius.isPositif()) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_CIRCLE_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        return new CircleOperand(acceptMode, lat, lon, radius);
    }


    private static class CircleOperand implements FicheDataEligibility {

        private final boolean acceptMode;
        private final DegreDecimal latitude, longitude;
        private final Decimal radius;
        private final double radiusDouble;
        private final double centerLatCos;
        private final double centerLatSin;
        private final double centerLonRadian;

        private CircleOperand(boolean acceptMode, DegreDecimal latitude, DegreDecimal longitude, Decimal radius) {
            this.acceptMode = acceptMode;
            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
            this.radiusDouble = radius.toDouble();
            double latRadian = latitude.toRadians();
            this.centerLatCos = Math.cos(latRadian);
            this.centerLatSin = Math.sin(latRadian);
            this.centerLonRadian = longitude.toRadians();
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return !acceptMode;
            }
            if (!ficheData.isWithGeo()) {
                return !acceptMode;
            }
            double latRadian = ficheData.getLatitude().toRadians();
            double lonRadian = ficheData.getLongitude().toRadians();
            double cos = (Math.cos(latRadian) * centerLatCos * Math.cos(centerLonRadian - lonRadian))
                    + (Math.sin(latRadian) * centerLatSin);
            if (EARTH_RADIUS * Math.acos(cos) <= radiusDouble) {
                return acceptMode;
            } else {
                return !acceptMode;
            }
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder();
            if (!acceptMode) {
                buf.append('!');
            }
            buf.append(EligibilityConstants.BBOX_SCOPE);
            buf.append(':');
            buf.append(latitude.toString());
            buf.append(',');
            buf.append(longitude.toString());
            buf.append(',');
            buf.append(radius.toString());
            return buf.toString();
        }

    }

}
