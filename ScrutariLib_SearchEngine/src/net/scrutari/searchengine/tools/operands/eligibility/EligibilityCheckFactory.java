/* ScrutariLib_SearchEngine - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.logicaloperation.LogicalOperationConstants;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.scrutari.searchengine.api.operands.eligibility.Eligibility;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.api.operands.eligibility.EligibilitySimpleOperand;
import net.scrutari.searchengine.api.operands.eligibility.EligibilitySubOperand;
import net.scrutari.searchengine.api.operands.eligibility.SubEligibility;


/**
 *
 * @author Vincent Calame
 */
public final class EligibilityCheckFactory {

    private EligibilityCheckFactory() {

    }

    public static EligibilityCheck check(EligibilityOperand operand, Integer[] corpusCodeArray, EligibilityCheckParameters eligibilityCheckParameters) {
        if (corpusCodeArray == null) {
            corpusCodeArray = eligibilityCheckParameters.getDataAccess().getCorpusCodeArray();
        }
        if (operand instanceof EligibilitySubOperand) {
            return checkSubOperand((EligibilitySubOperand) operand, corpusCodeArray, eligibilityCheckParameters);
        } else if (operand instanceof EligibilitySimpleOperand) {
            return checkSimpleOperand((EligibilitySimpleOperand) operand, corpusCodeArray, eligibilityCheckParameters);
        } else {
            throw new IllegalArgumentException("Unkonwn Operand instance = " + operand.getClass().getName());
        }
    }


    private static EligibilityCheck checkSimpleOperand(EligibilitySimpleOperand simpleOperand, Integer[] corpusCodeArray, EligibilityCheckParameters eligibilityCheckParameters) {
        Object scopeObject = simpleOperand.getScopeObject();
        if (scopeObject instanceof AttributeKey) {
            return AttributeCheck.check((AttributeKey) scopeObject, simpleOperand, eligibilityCheckParameters);
        } else {
            String scope = (String) scopeObject;
            switch (scope) {
                case EligibilityConstants.MOTCLE_SCOPE:
                    return MotcleCheck.check(simpleOperand, corpusCodeArray, eligibilityCheckParameters);
                case EligibilityConstants.THESAURUS_SCOPE:
                    return ThesaurusCheck.check(simpleOperand, corpusCodeArray, eligibilityCheckParameters);
                case EligibilityConstants.DATE_SCOPE:
                    return DateCheck.check(simpleOperand, eligibilityCheckParameters);
                case EligibilityConstants.QID_SCOPE:
                    return PreviousResultCheck.check(simpleOperand, eligibilityCheckParameters);
                case EligibilityConstants.ADD_SCOPE:
                    return AddCheck.check(simpleOperand, corpusCodeArray, eligibilityCheckParameters);
                case EligibilityConstants.BBOX_SCOPE:
                    return BboxCheck.check(simpleOperand, eligibilityCheckParameters);
                case EligibilityConstants.CIRCLE_SCOPE:
                    return CircleCheck.check(simpleOperand, eligibilityCheckParameters);
                case EligibilityConstants.CORPUS_SCOPE:
                    return CorpusCheck.check(simpleOperand, eligibilityCheckParameters);
                case EligibilityConstants.BASE_SCOPE:
                    return BaseCheck.check(simpleOperand, eligibilityCheckParameters);
                case EligibilityConstants.LANG_SCOPE:
                    return LangCheck.check(simpleOperand, eligibilityCheckParameters);
                default:
                    throw new SwitchException("Unknown scope = " + scope);
            }
        }
    }

    private static EligibilityCheck checkSubOperand(EligibilitySubOperand subOperand, Integer[] corpusCodeArray, EligibilityCheckParameters eligibilityCheckParameters) {
        SubOperandCheckBuilder builder = new SubOperandCheckBuilder(subOperand);
        int operandLength = subOperand.size();
        for (int i = 0; i < operandLength; i++) {
            EligibilityOperand childOperand = (EligibilityOperand) subOperand.get(i);
            EligibilityCheck eligibilityCheck;
            if (childOperand instanceof SimpleOperand) {
                eligibilityCheck = checkSimpleOperand((EligibilitySimpleOperand) childOperand, corpusCodeArray, eligibilityCheckParameters);
            } else if (childOperand instanceof SubOperand) {
                eligibilityCheck = checkSubOperand((EligibilitySubOperand) childOperand, corpusCodeArray, eligibilityCheckParameters);
            } else {
                throw new IllegalArgumentException("Unkonwn Operand instance = " + childOperand.getClass().getName());
            }
            if (eligibilityCheck != null) {
                if (eligibilityCheck.equals(EligibilityCheck.NONE_ELIGIBILITYCHECK)) {
                    if (subOperand.getOperator() == LogicalOperationConstants.INTERSECTION_OPERATOR) {
                        return EligibilityCheck.NONE_ELIGIBILITYCHECK;
                    }
                } else {
                    builder.addEligibilityCheck(eligibilityCheck);
                }
            }
        }
        return builder.toEligibilityCheck();
    }


    private static class SubOperandCheckBuilder {

        private final List<EligibilityCheck> codeOnlyCheckList = new ArrayList<EligibilityCheck>();
        private final List<EligibilityCheck> otherCheckList = new ArrayList<EligibilityCheck>();
        private final short operator;

        private SubOperandCheckBuilder(SubOperand subOperand) {
            this.operator = subOperand.getOperator();
        }

        private void addEligibilityCheck(EligibilityCheck eligibilityCheck) {
            if (eligibilityCheck.codeOnly()) {
                codeOnlyCheckList.add(eligibilityCheck);
            } else {
                otherCheckList.add(eligibilityCheck);
            }
        }

        private EligibilityCheck toEligibilityCheck() {
            List<EligibilityCheck> finalList = new ArrayList<EligibilityCheck>();
            finalList.addAll(codeOnlyCheckList);
            finalList.addAll(otherCheckList);
            int size = finalList.size();
            if (size == 0) {
                return null;
            } else if (size == 1) {
                return finalList.get(0);
            }
            boolean intersection = (operator == LogicalOperationConstants.INTERSECTION_OPERATOR);
            Eligibility[] array = new Eligibility[size];
            Set<Integer> resultCodeSet = null;
            for (int i = 0; i < size; i++) {
                EligibilityCheck eligibilityCheck = finalList.get(i);
                Set<Integer> codeSet = eligibilityCheck.getCodeSet();
                if (i == 0) {
                    if (codeSet != null) {
                        resultCodeSet = new HashSet<Integer>(codeSet);
                    }
                } else {
                    if (intersection) {
                        if (codeSet != null) {
                            if (resultCodeSet != null) {
                                resultCodeSet.retainAll(codeSet);
                            } else {
                                resultCodeSet = new HashSet<Integer>(codeSet);
                            }
                        }
                    } else {
                        if (codeSet == null) {
                            resultCodeSet = null;
                        } else if (resultCodeSet != null) {
                            resultCodeSet.addAll(codeSet);
                        }
                    }
                }
                array[i] = eligibilityCheck.getEligibility();
            }
            return new EligibilityCheck(resultCodeSet, new InternalSubEligibility(array, operator));
        }

    }


    private static class InternalSubEligibility implements SubEligibility {

        private final Eligibility[] array;
        private final short operator;

        private InternalSubEligibility(Eligibility[] array, short operator) {
            this.array = array;
            this.operator = operator;
        }

        @Override
        public short getOperator() {
            return operator;
        }

        @Override
        public int getEligibilityCount() {
            return array.length;
        }

        @Override
        public Eligibility getEligibility(int i) {
            return array[i];
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder();
            String separator = LogicalOperationUtils.getSeparator(operator);
            boolean next = false;
            for (Eligibility eligibility : array) {
                if (next) {
                    buf.append(separator);
                } else {
                    next = true;
                }
                if (eligibility instanceof SubEligibility) {
                    buf.append("(");
                    buf.append(eligibility.toString());
                    buf.append(")");
                } else {
                    buf.append(eligibility.toString());
                }
            }

            return buf.toString();
        }

    }

}
