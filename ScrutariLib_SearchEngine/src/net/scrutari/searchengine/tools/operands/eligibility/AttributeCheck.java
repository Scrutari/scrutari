/* ScrutariLib_SearchEngine - Copyright (c) 2015-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.TextConstants;
import net.scrutari.data.FicheData;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;


/**
 *
 * @author Vincent Calame
 */
final class AttributeCheck {

    static EligibilityCheck check(AttributeKey attributeKey, SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        FicheDataEligibility operand = toOperand(attributeKey, simpleOperand, eligibilityCheckParameters);
        if (operand == null) {
            return null;
        }
        return new EligibilityCheck(null, operand);
    }

    private static FicheDataEligibility toOperand(AttributeKey attributeKey, SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        if (operandBody.equals("*") || operandBody.equals("**")) {
            return new ExistAttributeOperand(attributeKey, acceptMode);
        }
        String part = operandBody;
        int type = TextConstants.MATCHES;
        if (part.startsWith("*")) {
            type = TextConstants.ENDSWITH;
            part = part.substring(1);
        }
        if (part.endsWith("*")) {
            if (type == TextConstants.ENDSWITH) {
                type = TextConstants.CONTAINS;
            } else {
                type = TextConstants.STARTSWITH;
            }
            part = part.substring(0, part.length() - 1);
        }
        return new ValueAttributeOperand(attributeKey, acceptMode, operandBody, type, part);
    }


    private static class ExistAttributeOperand implements FicheDataEligibility {

        private final AttributeKey attributeKey;
        private final boolean acceptMode;

        private ExistAttributeOperand(AttributeKey attributeKey, boolean acceptMode) {
            this.attributeKey = attributeKey;
            this.acceptMode = acceptMode;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return acceptMode;
            }
            boolean test = (ficheData.getAttributes().getAttribute(attributeKey) != null);
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return attributeKey.toString() + ":*";
            } else {
                return "!" + attributeKey.toString() + ":*";
            }
        }

    }


    private static class ValueAttributeOperand implements FicheDataEligibility {

        private final AttributeKey attributeKey;
        private final boolean acceptMode;
        private final String operandBody;
        private final int type;
        private final String part;


        private ValueAttributeOperand(AttributeKey attributeKey, boolean acceptMode, String operandBody, int type, String part) {
            this.attributeKey = attributeKey;
            this.acceptMode = acceptMode;
            this.operandBody = operandBody;
            this.type = type;
            this.part = part;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return acceptMode;
            }
            boolean test = false;
            Attribute attribute = ficheData.getAttributes().getAttribute(attributeKey);
            if (attribute != null) {
                int valueLength = attribute.size();
                for (int i = 0; i < valueLength; i++) {
                    if (StringUtils.contains(attribute.get(i), part, type)) {
                        test = true;
                        break;
                    }
                }
            }
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return attributeKey.toString() + ":" + operandBody;
            } else {
                return "!" + attributeKey.toString() + ":*" + operandBody;
            }
        }

    }


}
