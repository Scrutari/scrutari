/* ScrutariLib_SearchEngine - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.primitives.DegreDecimal;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.data.FicheData;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;


/**
 *
 * @author Vincent Calame
 */
final class BboxCheck {

    private final static FicheDataEligibility ANY_BBOXOPERAND = new AnyBboxOperand(true);
    private final static FicheDataEligibility NONE_DATEOPERAND = new AnyBboxOperand(false);

    private BboxCheck() {

    }

    static EligibilityCheck check(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        FicheDataEligibility operand = toOperand(simpleOperand, eligibilityCheckParameters);
        if (operand == null) {
            return null;
        }
        return new EligibilityCheck(null, operand);
    }

    private static FicheDataEligibility toOperand(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        if (operandBody.equals("*")) {
            if (acceptMode) {
                return ANY_BBOXOPERAND;
            } else {
                return NONE_DATEOPERAND;
            }
        }
        String[] tokens = StringUtils.getTokens(operandBody, ',', StringUtils.EMPTY_INCLUDE);
        int length = tokens.length;
        if (length != 4) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_BBOX_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        DegreDecimal left, bottom, right, top;
        try {
            left = DegreDecimal.newInstance(StringUtils.parseDecimal(tokens[0]));
            bottom = DegreDecimal.newInstance(StringUtils.parseDecimal(tokens[1]));
            right = DegreDecimal.newInstance(StringUtils.parseDecimal(tokens[2]));
            top = DegreDecimal.newInstance(StringUtils.parseDecimal(tokens[3]));
        } catch (NumberFormatException nfe) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_BBOX_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        if (bottom.compareTo(top) > 0) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_BBOX_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        return new BboxOperand(acceptMode, left, bottom, right, top);
    }


    private static class AnyBboxOperand implements FicheDataEligibility {

        private final boolean acceptMode;

        private AnyBboxOperand(boolean acceptMode) {
            this.acceptMode = acceptMode;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return !acceptMode;
            }
            if (ficheData.isWithGeo()) {
                return acceptMode;
            } else {
                return !acceptMode;
            }
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.BBOX_SCOPE + ":*";
            } else {
                return "!" + EligibilityConstants.BBOX_SCOPE + ":*";
            }
        }

    }


    private static class BboxOperand implements FicheDataEligibility {

        private final boolean acceptMode;
        private final DegreDecimal left, bottom, right, top;
        private final boolean leftGreater;

        private BboxOperand(boolean acceptMode, DegreDecimal left, DegreDecimal bottom, DegreDecimal right, DegreDecimal top) {
            this.acceptMode = acceptMode;
            this.left = left;
            this.bottom = bottom;
            this.right = right;
            this.top = top;
            this.leftGreater = (left.compareTo(right) > 0);
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return !acceptMode;
            }
            if (!ficheData.isWithGeo()) {
                return !acceptMode;
            }
            DegreDecimal latitude = ficheData.getLatitude();
            if (latitude.compareTo(bottom) < 0) {
                return !acceptMode;
            }
            if (latitude.compareTo(top) > 0) {
                return !acceptMode;
            }
            DegreDecimal longitude = ficheData.getLongitude();
            if (leftGreater) {
                if (longitude.compareTo(left) >= 0) {
                    return acceptMode;
                }
                if (longitude.compareTo(right) <= 0) {
                    return acceptMode;
                }
                return !acceptMode;
            } else {
                if (longitude.compareTo(left) < 0) {
                    return !acceptMode;
                }
                if (longitude.compareTo(right) > 0) {
                    return !acceptMode;
                }
                return acceptMode;
            }
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder();
            if (!acceptMode) {
                buf.append('!');
            }
            buf.append(EligibilityConstants.BBOX_SCOPE);
            buf.append(':');
            buf.append(left.toString());
            buf.append(',');
            buf.append(bottom.toString());
            buf.append(',');
            buf.append(right.toString());
            buf.append(',');
            buf.append(top.toString());
            return buf.toString();
        }

    }

}
