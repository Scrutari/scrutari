/* ScrutariLib_SearchEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.URITreeProvider;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.CodeEligibility;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;


/**
 *
 * @author Vincent Calame
 */
final class AddCheck {

    private AddCheck() {
    }

    static EligibilityCheck check(SimpleOperand simpleOperand, Integer[] corpusCodeArray, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        AddParameters addParameters = parse(simpleOperand, eligibilityCheckParameters);
        if (addParameters == null) {
            return null;
        }
        URITreeProvider uriTreeProvider = eligibilityCheckParameters.getUriTreeProvider();
        DataAccess dataAccess = eligibilityCheckParameters.getDataAccess();
        Set<Integer> resultCodeSet = new HashSet<Integer>();
        Set<Integer> resultCorpusSet = new HashSet<Integer>();
        Set<Integer> filterCorpusSet = new HashSet<Integer>();
        for (Integer code : corpusCodeArray) {
            filterCorpusSet.add(code);
        }
        int uriTreeCount = uriTreeProvider.getAddURITreeDateCount();
        int ficheCount = 0;
        for (int i = 0; i < uriTreeCount; i++) {
            ScrutariDBName scrutariDBName = uriTreeProvider.getScrutariDBName(i);
            FuzzyDate date = FuzzyDate.fromDate(scrutariDBName.getDate());
            int test = addParameters.test(date, ficheCount);
            if (test < 0) {
                break;
            }
            if (test == 0) {
                URITree uriTree = uriTreeProvider.getAddURITree(i, dataAccess.getScrutariDataURIChecker());
                ficheCount = ficheCount + check(resultCodeSet, resultCorpusSet, filterCorpusSet, uriTree, dataAccess);
            }
        }
        CodeEligibility operand = new AddOperand(acceptMode, resultCodeSet, operandBody);
        return new EligibilityCheck(resultCorpusSet, operand);
    }

    private static int check(Set<Integer> resultCodeSet, Set<Integer> resultCorpusSet, Set<Integer> filterCorpusSet, URITree uriTree, DataAccess dataAccess) {
        int result = 0;
        for (BaseNode baseNode : uriTree.getBaseNodeList()) {
            BaseURI baseURI = baseNode.getBaseURI();
            BaseData baseData = (BaseData) dataAccess.getScrutariData(baseURI);
            if (baseData == null) {
                continue;
            }
            result = result + checkBase(baseNode, resultCodeSet, resultCorpusSet, filterCorpusSet, dataAccess);
        }
        return result;
    }

    private static int checkBase(BaseNode baseNode, Set<Integer> resultCodeSet, Set<Integer> resultCorpusSet, Set<Integer> filterCorpusSet, DataAccess dataAccess) {
        int result = 0;
        for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
            CorpusURI corpusURI = corpusNode.getCorpusURI();
            CorpusData corpusData = (CorpusData) dataAccess.getScrutariData(corpusURI);
            if (corpusData == null) {
                continue;
            }
            if (!filterCorpusSet.contains(corpusData.getCorpusCode())) {
                continue;
            }
            int ficheCount = checkCorpus(corpusNode, resultCodeSet, dataAccess);
            if (ficheCount > 0) {
                result = result + ficheCount;
                resultCorpusSet.add(corpusData.getCorpusCode());
            }
        }
        return result;
    }

    private static int checkCorpus(CorpusNode corpusNode, Set<Integer> resultCodeSet, DataAccess dataAccess) {
        int result = 0;
        for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
            FicheURI ficheURI = ficheNode.getFicheURI();
            Integer ficheCode = dataAccess.getCode(ficheURI);
            if (ficheCode != null) {
                result++;
                resultCodeSet.add(ficheCode);
            }
        }
        return result;
    }

    private static AddParameters parse(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        String operandBody = simpleOperand.getBody();
        int idx = operandBody.indexOf("/");
        try {
            if (idx > -1) {
                return parsePeriod(simpleOperand, idx);
            } else {
                return parseTokens(simpleOperand);
            }
        } catch (ParseException pe) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_ADD_OPERAND, simpleOperand.getOperandString());
            return null;
        }

    }

    private static AddParameters parsePeriod(SimpleOperand simpleOperand, int separatorIndex) throws ParseException {
        String operandBody = simpleOperand.getBody();
        String floorText = operandBody.substring(0, separatorIndex);
        String ceilText = operandBody.substring(separatorIndex + 1);
        FuzzyDate floorDate = null;
        FuzzyDate ceilDate = null;
        if ((floorText.length() > 0) && (!floorText.equals("-"))) {
            floorDate = FuzzyDate.parse(floorText);
        }
        if ((ceilText.length() > 0) && (!ceilText.equals("-"))) {
            ceilDate = FuzzyDate.parse(ceilText).roll(1);
        }
        if ((floorDate == null) && (ceilDate == null)) {
            throw new ParseException("error", 0);
        }
        return new AddParameters(floorDate, ceilDate, 0);
    }

    private static AddParameters parseTokens(SimpleOperand simpleOperand) throws ParseException {
        String[] tokens = StringUtils.getTechnicalTokens(simpleOperand.getBody(), false);
        int length = tokens.length;
        int dayLimit = 0;
        int minimumFicheCount = 0;
        boolean malformed = false;
        if (length == 0) {
            malformed = true;
        }
        for (int i = 0; i < length; i++) {
            String token = tokens[i];
            String[] subTokens;
            if (token.indexOf('=') != -1) {
                subTokens = StringUtils.getTokens(token, '=', StringUtils.EMPTY_EXCLUDE); //Version antérieure à la révision 1731
            } else {
                subTokens = StringUtils.getTokens(token, '-', StringUtils.EMPTY_EXCLUDE);
            }
            int length2 = subTokens.length;
            if (length2 != 2) {
                malformed = true;
                break;
            }
            int value;
            try {
                value = Integer.parseInt(subTokens[1]);
            } catch (NumberFormatException nfe) {
                malformed = true;
                break;
            }
            if (value < 1) {
                malformed = true;
                break;
            }
            switch (subTokens[0]) {
                case "d":
                    dayLimit = value;
                    break;
                case "f":
                    minimumFicheCount = value;
                    break;

            }
        }
        if ((dayLimit == 0) && (minimumFicheCount == 0)) {
            malformed = true;
        }
        if (malformed) {
            throw new ParseException("error", 0);
        }
        FuzzyDate floorDate = null;
        if (dayLimit != 0) {
            FuzzyDate currentDate = FuzzyDate.current();
            floorDate = currentDate.roll(-dayLimit);
        }
        return new AddParameters(floorDate, null, minimumFicheCount);
    }


    private static class AddParameters {

        private final FuzzyDate floorDate;
        private final FuzzyDate ceilDate;
        private final boolean withDateTest;
        private final int minimumFicheCount;

        private AddParameters(FuzzyDate floorDate, FuzzyDate ceilDate, int minimumFicheCount) {
            this.floorDate = floorDate;
            this.ceilDate = ceilDate;
            this.withDateTest = ((floorDate != null) || (ceilDate != null));
            this.minimumFicheCount = minimumFicheCount;
        }

        private int test(FuzzyDate date, int ficheCount) {
            if (withDateTest) {
                if (floorDate != null) {
                    if (FuzzyDate.compare(date, floorDate) < 0) {
                        return testMinimum(ficheCount);
                    }
                }
                if (ceilDate != null) {
                    if (FuzzyDate.compare(date, ceilDate) >= 0) {
                        return 1;
                    }
                }
                return 0;
            }
            return testMinimum(ficheCount);
        }

        private int testMinimum(int ficheCount) {
            if ((minimumFicheCount != 0) && (ficheCount < minimumFicheCount)) {
                return 0;
            } else {
                return -1;
            }
        }

    }


    private static class AddOperand implements CodeEligibility {

        private final boolean acceptMode;
        private final Set<Integer> codeSet;
        private final String bodyString;

        private AddOperand(boolean acceptMode, Set<Integer> codeSet, String bodyString) {
            this.acceptMode = acceptMode;
            this.codeSet = codeSet;
            this.bodyString = bodyString;
        }

        @Override
        public boolean acceptCode(Integer code) {
            boolean test = codeSet.contains(code);
            if (acceptMode) {
                return test;
            } else {
                return !test;
            }
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder();
            if (!acceptMode) {
                buf.append('!');
            }
            buf.append(EligibilityConstants.ADD_SCOPE);
            buf.append(':');
            buf.append(bodyString);
            return buf.toString();
        }

    }

}
