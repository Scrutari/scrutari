/* ScrutariLib_SearchEngine - Copyright (c) 2015-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.text.ParseException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.scrutari.data.FicheData;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;


/**
 *
 * @author Vincent Calame
 */
final class LangCheck {

    private LangCheck() {

    }

    static EligibilityCheck check(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        FicheDataEligibility operand = toOperand(simpleOperand, eligibilityCheckParameters);
        if (operand == null) {
            return null;
        }
        return new EligibilityCheck(null, operand);
    }

    private static FicheDataEligibility toOperand(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        try {
            Lang lang = Lang.parse(operandBody);
            return new LangOperand(lang, acceptMode, operandBody);
        } catch (ParseException pe) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_LANG_OPERAND, simpleOperand.getOperandString());
            return null;
        }
    }


    private static class LangOperand implements FicheDataEligibility {

        private final boolean acceptMode;
        private final Lang lang;
        private final String operandBody;

        private LangOperand(Lang lang, boolean acceptMode, String operandBody) {
            this.lang = lang;
            this.acceptMode = acceptMode;
            this.operandBody = operandBody;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return !acceptMode;
            }
            boolean test = ficheData.getLang().equals(lang);
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.LANG_SCOPE + ":" + operandBody;
            } else {
                return "!" + EligibilityConstants.LANG_SCOPE + ":" + operandBody;
            }
        }

    }

}
