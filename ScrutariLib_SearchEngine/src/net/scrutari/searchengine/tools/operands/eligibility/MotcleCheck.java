/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.tools.codes.CodeParser;
import net.scrutari.searchengine.api.operands.eligibility.CodeEligibility;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;


/**
 *
 * @author Vincent Calame
 */
final class MotcleCheck {

    private MotcleCheck() {
    }

    static EligibilityCheck check(SimpleOperand simpleOperand, Integer[] corpusCodeArray, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        DataAccess dataAccess = eligibilityCheckParameters.getDataAccess();
        Integer motcleCode = CodeParser.toCode(dataAccess, operandBody, eligibilityCheckParameters.getMessageHandler(), ScrutariDataURI.MOTCLEURI_TYPE);
        if (motcleCode == null) {
            return null;
        }
        Set<Integer> corpusSet = new HashSet<Integer>();
        Set<Integer> ficheSet = new HashSet<Integer>();
        MotcleInfo motcleInfo = dataAccess.getMotcleInfo(motcleCode);
        for (Integer corpusCode : corpusCodeArray) {
            List<Integer> indexationFicheCodeList = motcleInfo.getIndexationCodeList(corpusCode);
            if (!indexationFicheCodeList.isEmpty()) {
                for (Integer ficheCode : indexationFicheCodeList) {
                    ficheSet.add(ficheCode);
                }
                corpusSet.add(corpusCode);
            }
        }
        if (!acceptMode) {
            corpusSet = null;
        }
        return new EligibilityCheck(corpusSet, new MotcleOperand(ficheSet, acceptMode, operandBody));
    }


    private static class MotcleOperand implements CodeEligibility {

        private final Set<Integer> ficheCodeSet;
        private final boolean acceptMode;
        private final String operandBody;

        private MotcleOperand(Set<Integer> ficheCodeSet, boolean acceptMode, String operandBody) {
            this.ficheCodeSet = ficheCodeSet;
            this.acceptMode = acceptMode;
            this.operandBody = operandBody;
        }

        @Override
        public boolean acceptCode(Integer code) {
            boolean test = ficheCodeSet.contains(code);
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.MOTCLE_SCOPE + ":" + operandBody;
            } else {
                return "!" + EligibilityConstants.MOTCLE_SCOPE + ":" + operandBody;
            }
        }

    }

}
