/* ScrutariLib_SearchEngine - Copyright (c) 2017-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.text.ParseException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logicaloperation.LogicalOperationParser;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.OperandException;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.api.operands.eligibility.EligibilitySimpleOperand;
import net.scrutari.searchengine.api.operands.eligibility.EligibilitySubOperand;
import net.scrutari.searchengine.tools.operands.search.SearchOperationUtils;


/**
 *
 * @author Vincent Calame
 */
public final class EligibilityOperandParser {

    private EligibilityOperandParser() {

    }

    public static EligibilityOperand parse(String operationString, CheckedScope defaultScope, MessageHandler messageHandler) throws OperandException {
        Operand operand = LogicalOperationParser.parse(operationString);
        if (operand instanceof SubOperand) {
            return toEligibilityOperand((SubOperand) operand, defaultScope, messageHandler);
        } else if (operand instanceof SimpleOperand) {
            return toEligibilitySimpleOperand((SimpleOperand) operand, defaultScope, messageHandler);
        } else {
            throw new IllegalArgumentException("Unkonwn Operand instance = " + operand.getClass().getName());
        }
    }

    public static CheckedScope checkDefaultScope(String defaultScope) {
        if (defaultScope == null) {
            return null;
        }
        Object defaultScopeObject = checkScopeObject(defaultScope);
        if (defaultScopeObject == null) {
            return null;
        } else {
            return new CheckedScope(defaultScopeObject);
        }
    }

    public static String checkScope(String scope) {
        switch (scope) {
            case EligibilityConstants.MOTCLE_SCOPE:
            case EligibilityConstants.THESAURUS_SCOPE:
            case EligibilityConstants.DATE_SCOPE:
            case EligibilityConstants.QID_SCOPE:
            case EligibilityConstants.ADD_SCOPE:
            case EligibilityConstants.BBOX_SCOPE:
            case EligibilityConstants.CIRCLE_SCOPE:
            case EligibilityConstants.CORPUS_SCOPE:
            case EligibilityConstants.BASE_SCOPE:
            case EligibilityConstants.LANG_SCOPE:
                return scope;
            default:
                return null;
        }
    }

    public static EligibilityOperand toEligibilityOperand(List<EligibilityOperand> list, short operator) {
        int size = list.size();
        switch (size) {
            case 0:
                return null;
            case 1:
                return list.get(0);
            default:
                return new InternalEligibilitySubOperand(list.toArray(new EligibilityOperand[size]), operator);
        }
    }

    private static EligibilityOperand toEligibilityOperand(SubOperand subOperand, CheckedScope defaultScope, MessageHandler messageHandler) {
        List<EligibilityOperand> list = new ArrayList<EligibilityOperand>();
        int operandLength = subOperand.size();
        for (int i = 0; i < operandLength; i++) {
            Operand operand = subOperand.get(i);
            EligibilityOperand scrutariOperand;
            if (operand instanceof SubOperand) {
                scrutariOperand = toEligibilityOperand((SubOperand) operand, defaultScope, messageHandler);
            } else if (operand instanceof SimpleOperand) {
                scrutariOperand = toEligibilitySimpleOperand((SimpleOperand) operand, defaultScope, messageHandler);
            } else {
                throw new IllegalArgumentException("Unkonwn Operand instance = " + operand.getClass().getName());
            }
            if (scrutariOperand != null) {
                list.add(scrutariOperand);
            }
        }
        return toEligibilityOperand(list, subOperand.getOperator());
    }

    private static EligibilitySimpleOperand toEligibilitySimpleOperand(SimpleOperand operand, CheckedScope defaultScope, MessageHandler messageHandler) {
        String body = operand.getBody();
        if (body.isEmpty()) {
            return null;
        }
        String scope = operand.getScope();
        Object scopeObject = null;
        if (scope.isEmpty()) {
            if (defaultScope != null) {
                scopeObject = defaultScope.getScopeObject();
            }
        } else {
            scopeObject = checkScopeObject(scope);
        }
        if (scopeObject == null) {
            SearchOperationUtils.error(messageHandler, OperandMessageKeys.UNKNOWN_OPERAND_TYPE, operand.getOperandString());
            return null;
        }
        return new InternalEligibilitySimpleOperand(operand.isAcceptMode(), scopeObject, body);
    }


    private static Object checkScopeObject(String scope) {
        String checkedScope = checkScope(scope);
        if (checkedScope != null) {
            return checkedScope;
        } else {
            try {
                AttributeKey attributeKey = AttributeKey.parse(scope);
                return attributeKey;
            } catch (ParseException pe) {
                return null;
            }
        }
    }


    public static class CheckedScope {

        private final Object scopeObject;

        private CheckedScope(Object scopeObject) {
            this.scopeObject = scopeObject;
        }

        public Object getScopeObject() {
            return scopeObject;
        }

    }


    private static class InternalEligibilitySubOperand extends AbstractList<Operand> implements EligibilitySubOperand {

        private final EligibilityOperand[] array;
        private final short operator;

        private InternalEligibilitySubOperand(EligibilityOperand[] array, short operator) {
            this.array = array;
            this.operator = operator;
        }

        @Override
        public short getOperator() {
            return operator;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Operand get(int i) {
            return array[i];
        }

    }


    private static class InternalEligibilitySimpleOperand implements EligibilitySimpleOperand {

        private final boolean acceptMode;
        private final Object scopeObject;
        private final String body;

        private InternalEligibilitySimpleOperand(boolean acceptMode, Object scopeObject, String body) {
            this.acceptMode = acceptMode;
            this.scopeObject = scopeObject;
            this.body = body;
        }

        @Override
        public boolean isAcceptMode() {
            return acceptMode;
        }

        @Override
        public String getScope() {
            return scopeObject.toString();
        }

        @Override
        public String getOperandString() {
            if (acceptMode) {
                return scopeObject.toString() + ":" + body;
            } else {
                return "!" + scopeObject.toString() + ":" + body;
            }
        }

        @Override
        public Object getScopeObject() {
            return scopeObject;
        }

        @Override
        public String getBody() {
            return body;
        }

    }

}
