/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.util.Collections;
import java.util.Set;
import net.scrutari.searchengine.api.operands.eligibility.CodeEligibility;
import net.scrutari.searchengine.api.operands.eligibility.Eligibility;
import net.scrutari.searchengine.api.operands.eligibility.SubEligibility;


/**
 *
 * @author Vincent Calame
 */
public class EligibilityCheck {

    public final static Set<Integer> EMPTY_SET = Collections.emptySet();
    public final static EligibilityCheck NONE_ELIGIBILITYCHECK = new EligibilityCheck(EMPTY_SET, new BooleanCodeOperand(false));
    public final static EligibilityCheck ALL_ELIGIBILITYCHECK = new EligibilityCheck(null, new BooleanCodeOperand(true));
    private final Set<Integer> codeSet;
    private final Eligibility eligibility;
    private final boolean codeOnly;

    public EligibilityCheck(Set<Integer> codeSet, Eligibility eligibility) {
        this.codeSet = codeSet;
        this.eligibility = eligibility;
        this.codeOnly = testCodeOnly(eligibility);
    }

    public Set<Integer> getCodeSet() {
        return codeSet;
    }

    public Eligibility getEligibility() {
        return eligibility;
    }

    public boolean codeOnly() {
        return codeOnly;
    }

    private static boolean testCodeOnly(Eligibility eligibility) {
        if (eligibility instanceof CodeEligibility) {
            return true;
        } else if (eligibility instanceof SubEligibility) {
            SubEligibility subEligibility = (SubEligibility) eligibility;
            int count = subEligibility.getEligibilityCount();
            for (int i = 0; i < count; i++) {
                if (!testCodeOnly(subEligibility.getEligibility(i))) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }


    private static class BooleanCodeOperand implements CodeEligibility {

        private final boolean acceptMode;

        private BooleanCodeOperand(boolean acceptMode) {
            this.acceptMode = acceptMode;
        }

        @Override
        public boolean acceptCode(Integer code) {
            return acceptMode;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return "TRUE";
            } else {
                return "FALSE";
            }
        }

    }

}
