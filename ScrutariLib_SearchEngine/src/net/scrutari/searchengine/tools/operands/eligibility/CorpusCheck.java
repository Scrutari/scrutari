/* ScrutariLib_SearchEngine - Copyright (c) 2015-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.scrutari.data.FicheData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.tools.codes.CodeParser;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;


/**
 *
 * @author Vincent Calame
 */
final class CorpusCheck {

    private CorpusCheck() {

    }

    static EligibilityCheck check(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        FicheDataEligibility operand = toOperand(simpleOperand, eligibilityCheckParameters);
        if (operand == null) {
            return null;
        }
        return new EligibilityCheck(null, operand);
    }

    private static FicheDataEligibility toOperand(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        MessageHandler messageHandler = eligibilityCheckParameters.getMessageHandler();
        DataAccess dataAccess = eligibilityCheckParameters.getDataAccess();
        Integer corpusCode = CodeParser.toCode(dataAccess, operandBody, messageHandler, ScrutariDataURI.CORPUSURI_TYPE);
        if (corpusCode == null) {
            return null;
        } else {
            return new CorpusOperand(corpusCode, acceptMode, operandBody);
        }
    }


    private static class CorpusOperand implements FicheDataEligibility {

        private final boolean acceptMode;
        private final int corpusCode;
        private final String operandBody;

        private CorpusOperand(int corpusCode, boolean acceptMode, String operandBody) {
            this.corpusCode = corpusCode;
            this.acceptMode = acceptMode;
            this.operandBody = operandBody;
        }

        @Override
        public boolean acceptFicheData(FicheData ficheData) {
            if (ficheData == null) {
                return !acceptMode;
            }
            int otherCorpusCode = ficheData.getCorpusCode();
            boolean test = (otherCorpusCode == corpusCode);
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.CORPUS_SCOPE + ":" + operandBody;
            } else {
                return "!" + EligibilityConstants.CORPUS_SCOPE + ":" + operandBody;
            }
        }

    }

}
