/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.eligibility;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.eligibility.CodeEligibility;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
final class PreviousResultCheck {

    private PreviousResultCheck() {
    }

    static EligibilityCheck check(SimpleOperand simpleOperand, EligibilityCheckParameters eligibilityCheckParameters) {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String operandBody = simpleOperand.getBody();
        FicheSearchResult result = null;
        try {
            QId qId = QId.parse(operandBody);
            result = eligibilityCheckParameters.getFicheSearchResultHolder().getSearch(qId);
        } catch (ParseException pe) {
        }
        if (result == null) {
            eligibilityCheckParameters.addError(OperandMessageKeys.WRONG_QID_OPERAND, simpleOperand.getOperandString());
            return null;
        }
        Set<Integer> ficheSet = new HashSet<Integer>(2048);
        Set<Integer> corpusSet = null;
        DataAccess dataAccess = null;
        if (acceptMode) {
            corpusSet = new HashSet<Integer>(64);
            dataAccess = eligibilityCheckParameters.getDataAccess();
        }
        for (FicheSearchResultGroup group : result.getFicheSearchResultGroupList()) {
            for (FicheSearchResultInfo ficheInfo : group.getFicheSearchResultInfoList()) {
                Integer ficheCode = ficheInfo.getCode();
                ficheSet.add(ficheCode);
                if (acceptMode) {
                    corpusSet.add(dataAccess.getFicheInfo(ficheCode).getCorpusCode());
                }
            }
        }
        return new EligibilityCheck(corpusSet, new PreviousResultOperand(ficheSet, acceptMode, operandBody));
    }


    private static class PreviousResultOperand implements CodeEligibility {

        private final Set<Integer> ficheCodeSet;
        private final boolean acceptMode;
        private final String operandBody;

        private PreviousResultOperand(Set<Integer> ficheCodeSet, boolean acceptMode, String operandBody) {
            this.ficheCodeSet = ficheCodeSet;
            this.acceptMode = acceptMode;
            this.operandBody = operandBody;
        }

        @Override
        public boolean acceptCode(Integer code) {
            boolean test = ficheCodeSet.contains(code);
            if (!acceptMode) {
                test = !test;
            }
            return test;
        }

        @Override
        public String toString() {
            if (acceptMode) {
                return EligibilityConstants.QID_SCOPE + ":" + operandBody;
            } else {
                return "!" + EligibilityConstants.QID_SCOPE + ":" + operandBody;
            }
        }

    }

}
