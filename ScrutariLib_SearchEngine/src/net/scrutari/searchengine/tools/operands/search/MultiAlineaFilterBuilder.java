/* ScrutariLib_SearchEngine - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;


/**
 *
 * @author Vincent Calame
 */
public class MultiAlineaFilterBuilder {

    private final Map<String, FilterInfo> specialFilterMap = new LinkedHashMap<String, FilterInfo>();
    private final SortedMap<String, FilterInfo> otherFilterMap = new TreeMap<String, FilterInfo>();

    public MultiAlineaFilterBuilder() {
        specialFilterMap.put("titre", null);
        specialFilterMap.put("soustitre", null);
        specialFilterMap.put("supplement", null);
        specialFilterMap.put("motcle", null);
    }

    public void addAlineaFilter(AlineaFilter alineaFilter, boolean acceptMode) {
        if (alineaFilter instanceof InternalMultiAlineaFilter) {
            FilterInfo[] filterInfoArray = ((InternalMultiAlineaFilter) alineaFilter).alineaFilterArray;
            for (FilterInfo filterInfo : filterInfoArray) {
                addAlineaFilter(filterInfo.alineaFilter, filterInfo.acceptMode);
            }
        } else {
            String prefix = alineaFilter.getFilterPrefix();
            if (prefix.length() == 0) {
                return;
            }
            if (specialFilterMap.containsKey(prefix)) {
                FilterInfo currentFilterInfo = specialFilterMap.get(prefix);
                if (currentFilterInfo != null) {
                    if (currentFilterInfo.acceptMode != acceptMode) {
                        specialFilterMap.put(prefix, null);
                    }
                } else {
                    specialFilterMap.put(prefix, new FilterInfo(alineaFilter, acceptMode));
                }
            } else {
                FilterInfo currentFilterInfo = otherFilterMap.get(prefix);
                if (currentFilterInfo != null) {
                    if (currentFilterInfo.acceptMode != acceptMode) {
                        otherFilterMap.put(prefix, null);
                    }
                } else {
                    otherFilterMap.put(prefix, new FilterInfo(alineaFilter, acceptMode));
                }
            }
        }

    }

    public AlineaFilter toAlineaFilter() {
        List<FilterInfo> resultList = new ArrayList<FilterInfo>();
        for (FilterInfo filterInfo : specialFilterMap.values()) {
            if (filterInfo != null) {
                resultList.add(filterInfo);
            }
        }
        for (FilterInfo filterInfo : otherFilterMap.values()) {
            if (filterInfo != null) {
                resultList.add(filterInfo);
            }
        }
        int size = resultList.size();
        if (size == 0) {
            return AlineaFilters.ALL_FILTER;
        }
        FilterInfo[] filterInfoArray = resultList.toArray(new FilterInfo[size]);
        return new InternalMultiAlineaFilter(filterInfoArray);
    }


    private static class InternalMultiAlineaFilter implements AlineaFilter {

        private final FilterInfo[] alineaFilterArray;
        private final boolean onlyRefuse;
        private final boolean onlyAccept;

        private InternalMultiAlineaFilter(FilterInfo[] filterInfoArray) {
            this.alineaFilterArray = filterInfoArray;
            boolean hasAccept = false;
            boolean hasRefuse = false;
            for (FilterInfo filterInfo : filterInfoArray) {
                if (filterInfo.acceptMode) {
                    hasAccept = true;
                } else {
                    hasRefuse = true;
                }
            }
            this.onlyRefuse = !hasAccept;
            this.onlyAccept = !hasRefuse;
        }

        @Override
        public boolean acceptAlinea(Integer corpusCode, AlineaRank alineaRank) {
            if (onlyRefuse) {
                for (FilterInfo filterInfo : alineaFilterArray) {
                    if (filterInfo.alineaFilter.acceptAlinea(corpusCode, alineaRank)) {
                        return false;
                    }
                }
                return true;
            } else if (onlyAccept) {
                for (FilterInfo filterInfo : alineaFilterArray) {
                    if (filterInfo.alineaFilter.acceptAlinea(corpusCode, alineaRank)) {
                        return true;
                    }
                }
                return false;
            } else {
                boolean result = false;
                for (FilterInfo filterInfo : alineaFilterArray) {
                    if (filterInfo.alineaFilter.acceptAlinea(corpusCode, alineaRank)) {
                        if (filterInfo.acceptMode) {
                            result = true;
                        } else {
                            return false;
                        }
                    }
                }
                return result;
            }
        }

        @Override
        public boolean acceptMotcle(Integer thesaurusCode) {
            if (onlyRefuse) {
                for (FilterInfo filterInfo : alineaFilterArray) {
                    if (filterInfo.alineaFilter.acceptMotcle(thesaurusCode)) {
                        return false;
                    }
                }
                return true;
            } else if (onlyAccept) {
                for (FilterInfo filterInfo : alineaFilterArray) {
                    if (filterInfo.alineaFilter.acceptMotcle(thesaurusCode)) {
                        return true;
                    }
                }
                return false;
            } else {
                boolean result = false;
                for (FilterInfo filterInfo : alineaFilterArray) {
                    if (filterInfo.alineaFilter.acceptMotcle(thesaurusCode)) {
                        if (filterInfo.acceptMode) {
                            result = true;
                        } else {
                            return false;
                        }
                    }
                }
                return result;
            }
        }

        @Override
        public String getFilterPrefix() {
            StringBuilder buf = new StringBuilder();
            boolean next = false;
            for (FilterInfo filterInfo : alineaFilterArray) {
                if (next) {
                    buf.append(',');
                } else {
                    if (!filterInfo.acceptMode) {
                        buf.append(',');
                    }
                    next = true;
                }
                if (!filterInfo.acceptMode) {
                    buf.append('!');
                }
                buf.append(filterInfo.alineaFilter.getFilterPrefix());
            }
            return buf.toString();
        }

    }


    private static class FilterInfo {

        private AlineaFilter alineaFilter;
        private boolean acceptMode;

        private FilterInfo(AlineaFilter alineaFilter, boolean acceptMode) {
            this.alineaFilter = alineaFilter;
            this.acceptMode = acceptMode;
        }

    }

}
