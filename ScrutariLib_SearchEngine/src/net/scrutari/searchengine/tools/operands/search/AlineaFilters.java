/* ScrutariLib_SearchEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.exceptions.InternalResourceException;
import net.mapeadores.util.ini.IniParser;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FieldRank;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;


/**
 *
 * @author Vincent Calame
 */
public final class AlineaFilters {

    public final static AlineaFilter ALL_FILTER = new AllAlineaFilter();
    public final static AlineaFilter TITRE_FILTER = new TypeAlineaFilter(FieldRank.TITRE_TYPE, "titre");
    public final static AlineaFilter SOUSTITRE_FILTER = new TypeAlineaFilter(FieldRank.SOUSTITRE_TYPE, "soustitre");
    public final static AlineaFilter MOTCLE_FILTER = new MotcleAlineaFilter("motcle");
    public final static AlineaFilter SUPPLEMENT_FILTER = new SupplementAlineaFilter("supplement");
    private final static Map<String, AlineaFilter> specialFilterMap = new HashMap<String, AlineaFilter>();

    static {
        initSpecialFilterMap(TITRE_FILTER);
        initSpecialFilterMap(SOUSTITRE_FILTER);
        initSpecialFilterMap(MOTCLE_FILTER);
        initSpecialFilterMap(SUPPLEMENT_FILTER);
    }

    private AlineaFilters() {

    }

    public static AlineaFilter getSpecialFilter(String scope) {
        return specialFilterMap.get(scope);
    }

    public static AlineaFilter getFieldFilter(String prefix, FieldRank fieldRank) {
        return new FieldFilter(prefix, fieldRank.getValue());
    }

    private static void initSpecialFilterMap(AlineaFilter specialAlineaFilter) {
        String prefix = specialAlineaFilter.getFilterPrefix();
        specialFilterMap.put(prefix, specialAlineaFilter);
        InputStream inputStream = AlineaFilters.class.getResourceAsStream("l10n-" + prefix + ".ini");
        if (inputStream != null) {
            Map<String, String> stringMap = new HashMap<String, String>();
            try (InputStream is = inputStream) {
                IniParser.parseIni(is, stringMap);
            } catch (IOException ioe) {
                throw new InternalResourceException(ioe);
            }
            for (String value : stringMap.values()) {
                specialFilterMap.put(value, specialAlineaFilter);
            }
        }
    }


    private static class AllAlineaFilter implements AlineaFilter {

        private AllAlineaFilter() {

        }

        @Override
        public boolean acceptAlinea(Integer corpusCode, AlineaRank alineaRank) {
            return true;
        }

        @Override
        public boolean acceptMotcle(Integer thesaurusCode) {
            return true;
        }

        @Override
        public String getFilterPrefix() {
            return "";
        }

    }


    private static class MotcleAlineaFilter implements AlineaFilter {

        private final String prefix;

        private MotcleAlineaFilter(String prefix) {
            this.prefix = prefix;
        }

        @Override
        public boolean acceptAlinea(Integer corpusCode, AlineaRank alineaRank) {
            return false;
        }

        @Override
        public boolean acceptMotcle(Integer thesaurusCode) {
            return true;
        }

        @Override
        public String getFilterPrefix() {
            return prefix;
        }

    }


    private static class TypeAlineaFilter implements AlineaFilter {

        private final int type;
        private final String prefix;

        private TypeAlineaFilter(int type, String prefix) {
            this.type = type;
            this.prefix = prefix;
        }

        @Override
        public boolean acceptAlinea(Integer corpusCode, AlineaRank alineaRank) {
            return (alineaRank.getType() == type);
        }

        @Override
        public boolean acceptMotcle(Integer thesaurusCode) {
            return false;
        }

        @Override
        public String getFilterPrefix() {
            return prefix;
        }

    }


    private static class SupplementAlineaFilter implements AlineaFilter {

        private final String prefix;

        private SupplementAlineaFilter(String prefix) {
            this.prefix = prefix;
        }

        @Override
        public boolean acceptAlinea(Integer corpusCode, AlineaRank alineaRank) {
            switch (alineaRank.getType()) {
                case FieldRank.TITRE_TYPE:
                case FieldRank.SOUSTITRE_TYPE:
                    return false;
                default:
                    return true;
            }
        }

        @Override
        public boolean acceptMotcle(Integer thesaurusCode) {
            return false;
        }

        @Override
        public String getFilterPrefix() {
            return prefix;
        }

    }


    private static class FieldFilter implements AlineaFilter {

        private final String prefix;
        private final int filterFieldRankValue;

        private FieldFilter(String prefix, int filterFieldRankValue) {
            this.prefix = prefix;
            this.filterFieldRankValue = filterFieldRankValue;
        }

        @Override
        public boolean acceptAlinea(Integer corpusCode, AlineaRank alineaRank) {
            return (alineaRank.getFieldRankValue() == filterFieldRankValue);
        }

        @Override
        public boolean acceptMotcle(Integer thesaurusCode) {
            return false;
        }

        @Override
        public String getFilterPrefix() {
            return prefix;
        }

    }


}
