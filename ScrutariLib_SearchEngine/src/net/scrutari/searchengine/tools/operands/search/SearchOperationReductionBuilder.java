/* ScrutariLib_SearchEngine - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.text.TextConstants;
import net.mapeadores.util.text.lexie.LexieFilter;
import net.mapeadores.util.text.search.MultiSearchToken;
import net.mapeadores.util.text.search.SearchToken;
import net.mapeadores.util.text.search.SearchUtils;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.searchengine.api.operands.search.AllOperand;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchOperationReduction;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;


/**
 *
 * @author Vincent Calame
 */
public class SearchOperationReductionBuilder {

    private final List<Integer> unusedNumberList = new ArrayList<Integer>();
    private final LexieFilter wordFilter;
    private final SearchOperation searchOperation;
    private Operand rootOperand;

    private SearchOperationReductionBuilder(LexieFilter wordFilter, SearchOperation searchOperation) {
        this.wordFilter = wordFilter;
        this.searchOperation = searchOperation;
    }

    public static SearchOperationReduction lexieReduction(SearchOperation searchOperation, LexieFilter wordFilter) {
        SearchOperationReductionBuilder builder = new SearchOperationReductionBuilder(wordFilter, searchOperation);
        builder.reduce();
        return builder.toSearchOperationReduction();
    }

    private SearchOperationReduction toSearchOperationReduction() {
        return new InternalSearchOperationReduction(searchOperation, rootOperand, PrimUtils.wrap(unusedNumberList.toArray(new Integer[unusedNumberList.size()])));
    }

    private void reduce() {
        Operand currentRootOperand = searchOperation.getRootOperand();
        if (currentRootOperand instanceof AllOperand) {
            throw new IllegalStateException("Cannot be called if searchOperation.getRootOperand() instance of AllOperand");
        } else {
            rootOperand = reduce(currentRootOperand);
        }
    }

    private Operand reduce(Operand operand) {
        if (operand instanceof SearchTokenOperand) {
            return reduceSearchTokenOperand((SearchTokenOperand) operand);
        } else if (operand instanceof SubOperand) {
            return reduceSubOperand((SubOperand) operand);
        } else {
            throw new ImplementationException("Wrong operand implementation = " + operand.getClass().getName());
        }
    }

    private Operand reduceSubOperand(SubOperand subOperand) {
        int operandLength = subOperand.size();
        List<Operand> newList = new ArrayList<Operand>();
        for (int i = 0; i < operandLength; i++) {
            Operand newOperand = reduce(subOperand.get(i));
            if (newOperand != null) {
                newList.add(newOperand);
            }
        }
        int size = newList.size();
        if (size == 0) {
            return null;
        }
        if (size == 1) {
            return newList.get(0);
        }
        return LogicalOperationUtils.toSubOperand(newList.toArray(new Operand[size]), subOperand.getOperator());
    }

    private SearchTokenOperand reduceSearchTokenOperand(SearchTokenOperand searchTokenOperand) {
        SearchToken searchToken = searchTokenOperand.getSearchToken();
        SearchTokenOperand newSearchTokenOperand;
        if (searchToken instanceof SimpleSearchToken) {
            newSearchTokenOperand = reduceSimpleTokenOperand(searchTokenOperand, (SimpleSearchToken) searchToken);
        } else {
            newSearchTokenOperand = reduceMultiTokenOperand(searchTokenOperand, (MultiSearchToken) searchToken);
        }
        if (newSearchTokenOperand == null) {
            unusedNumberList.add(searchTokenOperand.getOperandNumber());
        }
        return newSearchTokenOperand;
    }

    private SearchTokenOperand reduceMultiTokenOperand(SearchTokenOperand searchTokenOperand, MultiSearchToken multiToken) {
        int subCount = multiToken.getSubtokenCount();
        List<SimpleSearchToken> newSubtokenList = new ArrayList<SimpleSearchToken>();
        for (int j = 0; j < subCount; j++) {
            SimpleSearchToken subToken = multiToken.getSubtoken(j);
            if ((subToken.getSearchType() != TextConstants.MATCHES) || (wordFilter.acceptLexie(subToken.getTokenString()))) {
                newSubtokenList.add(subToken);
            }
        }
        int size = newSubtokenList.size();
        if (size == 0) {
            return null;
        }
        MultiSearchToken newMultiSearchToken = SearchUtils.toMultiSearchToken(newSubtokenList.toArray(new SimpleSearchToken[size]));
        return SearchOperationUtils.derive(searchTokenOperand, newMultiSearchToken);
    }

    private SearchTokenOperand reduceSimpleTokenOperand(SearchTokenOperand searchTokenOperand, SimpleSearchToken simpleToken) {
        int searchType = simpleToken.getSearchType();
        if ((searchType == TextConstants.MATCHES) || (searchType == TextConstants.STARTSWITH)) {
            if (!wordFilter.acceptLexie(simpleToken.getTokenString())) {
                return null;
            }
        }
        return SearchOperationUtils.derive(searchTokenOperand, simpleToken);
    }


    private static class InternalSearchOperationReduction implements SearchOperationReduction {

        private final SearchOperation searchOperation;
        private final Operand rootOperand;
        private final List<Integer> unusedList;

        private InternalSearchOperationReduction(SearchOperation searchOperation, Operand rootOperand, List<Integer> unusedList) {
            this.searchOperation = searchOperation;
            this.rootOperand = rootOperand;
            this.unusedList = unusedList;
        }

        @Override
        public SearchOperation getOriginal() {
            return searchOperation;
        }

        @Override
        public Operand getRootOperand() {
            return rootOperand;
        }

        @Override
        public List<Integer> getUnusedNumberList() {
            return unusedList;
        }

    }

}
