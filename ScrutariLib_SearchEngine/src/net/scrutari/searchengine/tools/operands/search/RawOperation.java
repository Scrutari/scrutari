/* ScrutariLib_SearchEngine - Copyright (c) 2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

/**
 *
 * @author Vincent Calame
 */
public class RawOperation {

    private final String q;
    private final String scope;
    private final short operator;

    public RawOperation(String q, String scope, short operator) {
        this.q = q;
        this.scope = scope;
        this.operator = operator;
    }

    public String getQ() {
        return q;
    }

    public String getScope() {
        return scope;
    }

    public short getOperator() {
        return operator;
    }
}
