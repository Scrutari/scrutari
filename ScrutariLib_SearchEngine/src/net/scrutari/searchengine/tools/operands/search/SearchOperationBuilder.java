/* ScrutariLib_SearchEngine - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import java.util.List;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.Operand;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;


/**
 *
 * @author Vincent Calame
 */
public class SearchOperationBuilder {

    private SearchOperationBuilder() {
    }

    public static SearchOperation build(Operand rootOperand) {
        return new InternalSearchOperation(rootOperand, SearchOperationUtils.toSearchTokenOperandList(rootOperand), CanonicalQ.newInstance(LogicalOperationUtils.toInformatiqueString(rootOperand)));
    }


    private static class InternalSearchOperation implements SearchOperation {

        private final Operand rootOperand;
        private final List<SearchTokenOperand> searchTokenOperandList;
        private final CanonicalQ canonicalQ;

        private InternalSearchOperation(Operand rootOperand, List<SearchTokenOperand> searchTokenOperandList, CanonicalQ canonicalQ) {
            this.rootOperand = rootOperand;
            this.searchTokenOperandList = searchTokenOperandList;
            this.canonicalQ = canonicalQ;
        }

        @Override
        public Operand getRootOperand() {
            return rootOperand;
        }

        @Override
        public List<SearchTokenOperand> getSearchTokenOperandList() {
            return searchTokenOperandList;
        }

        @Override
        public CanonicalQ getCanonicalQ() {
            return canonicalQ;
        }

    }

}
