/* ScrutariLib_SearchEngine - Copyright (c) 2015-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.text.search.SearchToken;
import net.mapeadores.util.text.search.SearchTokenParser;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;


/**
 *
 * @author Vincent Calame
 */
public final class RawSearchOperationEngine {

    private final MessageHandler messageHandler;
    private final FieldRankManager fieldRankManager;
    private final int defaultSearchType;
    private int currentOperandNumber = 1;

    private RawSearchOperationEngine(MessageHandler messageHandler, FieldRankManager fieldRankManager, int defaultSearchType) {
        this.messageHandler = messageHandler;
        this.fieldRankManager = fieldRankManager;
        this.defaultSearchType = defaultSearchType;
    }

    public static SearchOperation parse(List<RawOperation> rawOperationList, MessageHandler messageHandler, FieldRankManager fieldRankManager, int defaultSearchType, short mainOperator) {
        RawSearchOperationEngine rawSearchOperationEngine = new RawSearchOperationEngine(messageHandler, fieldRankManager, defaultSearchType);
        Operand rootOperand = rawSearchOperationEngine.convert(rawOperationList, mainOperator);
        return rawSearchOperationEngine.toSearchOperation(rootOperand);
    }

    public static SearchOperation parse(RawOperation rawOperation, MessageHandler messageHandler, FieldRankManager fieldRankManager, int defaultSearchType) {
        RawSearchOperationEngine rawSearchOperationEngine = new RawSearchOperationEngine(messageHandler, fieldRankManager, defaultSearchType);
        Operand rootOperand = rawSearchOperationEngine.convert(rawOperation);
        return rawSearchOperationEngine.toSearchOperation(rootOperand);
    }

    private SearchOperation toSearchOperation(Operand rootOperand) {
        if (rootOperand == null) {
            return null;
        }
        return SearchOperationBuilder.build(rootOperand);
    }

    private Operand convert(List<RawOperation> rawOperationList, short mainOperator) {
        int size = rawOperationList.size();
        if (size == 1) {
            return convert(rawOperationList.get(0));
        } else {
            List<Operand> resultList = new ArrayList<Operand>();
            for (int i = 0; i < size; i++) {
                Operand operand = convert(rawOperationList.get(i));
                if (operand != null) {
                    resultList.add(operand);
                }
            }
            int resultSize = resultList.size();
            switch (resultSize) {
                case 0:
                    return null;
                case 1:
                    return resultList.get(0);
                default:
                    return LogicalOperationUtils.toSubOperand(resultList.toArray(new Operand[resultSize]), mainOperator);
            }
        }
    }

    private Operand convert(RawOperation rawOperation) {
        List<SearchToken> searchTokenList = SearchTokenParser.parse(rawOperation.getQ(), defaultSearchType);
        int size = searchTokenList.size();
        if (size == 0) {
            return null;
        }
        AlineaFilter alineaFilter = SearchOperationUtils.parse(rawOperation.getScope(), messageHandler, fieldRankManager);
        if (alineaFilter == null) {
            alineaFilter = AlineaFilters.ALL_FILTER;
        }
        Operand rootOperand;
        if (size == 1) {
            rootOperand = SearchOperationUtils.toSearchTokenOperand(currentOperandNumber, true, searchTokenList.get(0), alineaFilter);
            currentOperandNumber++;
        } else {
            SearchTokenOperand[] array = new SearchTokenOperand[size];
            for (int i = 0; i < size; i++) {
                array[i] = SearchOperationUtils.toSearchTokenOperand(currentOperandNumber, true, searchTokenList.get(i), alineaFilter);
                currentOperandNumber++;
            }
            rootOperand = LogicalOperationUtils.toSubOperand(array, rawOperation.getOperator());
        }
        return rootOperand;
    }

}
