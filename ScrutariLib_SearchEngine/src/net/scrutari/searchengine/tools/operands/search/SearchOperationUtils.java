/* ScrutariLib_SearchEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import java.text.ParseException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logicaloperation.LogicalOperationConstants;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.TextConstants;
import net.mapeadores.util.text.search.MultiSearchToken;
import net.mapeadores.util.text.search.SearchToken;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.lexie.FieldRank;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;
import net.scrutari.searchengine.api.operands.search.AllOperand;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;


/**
 *
 * @author Vincent Calame
 */
public final class SearchOperationUtils {

    public final static AllOperand ALL_OPERAND = new InternalAllOperand();
    public final static List<SearchTokenOperand> EMPTY_SEARCHTOKENOPERANDLIST = Collections.emptyList();

    private SearchOperationUtils() {
    }

    public static List<SearchTokenOperand> toSearchTokenOperandList(Operand rootOperand) {
        if (rootOperand instanceof AllOperand) {
            return EMPTY_SEARCHTOKENOPERANDLIST;
        }
        if (rootOperand instanceof SearchTokenOperand) {
            return Collections.singletonList((SearchTokenOperand) rootOperand);
        }
        List<SearchTokenOperand> arrayList = new ArrayList<SearchTokenOperand>();
        addSearchTokenOperands(arrayList, (SubOperand) rootOperand);
        return wrap(arrayList.toArray(new SearchTokenOperand[arrayList.size()]));
    }

    private static void addSearchTokenOperands(List<SearchTokenOperand> list, SubOperand subOperand) {
        int count = subOperand.size();
        for (int i = 0; i < count; i++) {
            Operand operand = subOperand.get(i);
            if (operand instanceof SearchTokenOperand) {
                list.add((SearchTokenOperand) operand);
            } else {
                addSearchTokenOperands(list, (SubOperand) operand);
            }
        }
    }

    public static AlineaFilter parse(String scope, MessageHandler messageHandler, FieldRankManager fieldRankManager) {
        if ((scope == null) || (scope.length() == 0)) {
            return null;
        }
        String[] scopeTokens = StringUtils.getTokens(scope, ',', StringUtils.EMPTY_EXCLUDE);
        int length = scopeTokens.length;
        if (length == 0) {
            SearchOperationUtils.error(messageHandler, OperandMessageKeys.UNKNWON_SCOPE, scope);
            return null;
        } else if ((length == 1) && (!scopeTokens[0].startsWith("!"))) {
            return parseSimpleScope(scopeTokens[0], messageHandler, fieldRankManager);
        } else {
            MultiAlineaFilterBuilder builder = new MultiAlineaFilterBuilder();
            for (String scopeToken : scopeTokens) {
                boolean acceptMode = true;
                if (scopeToken.startsWith("!")) {
                    acceptMode = false;
                    scopeToken = scopeToken.substring(1);
                }
                AlineaFilter alineaFilter = parseSimpleScope(scopeToken, messageHandler, fieldRankManager);
                if (alineaFilter != null) {
                    builder.addAlineaFilter(alineaFilter, acceptMode);
                }
            }
            return builder.toAlineaFilter();
        }
    }

    private static AlineaFilter parseSimpleScope(String scope, MessageHandler messageHandler, FieldRankManager fieldRankManager) {
        AlineaFilter alineaFilter = AlineaFilters.getSpecialFilter(scope);
        if (alineaFilter != null) {
            return alineaFilter;
        }
        try {
            AttributeKey attributeKey = AttributeKey.parse(scope);
            FieldRank fieldRank = fieldRankManager.getAttributeFieldRank(attributeKey);
            switch (fieldRank.getType()) {
                case FieldRank.TECHNICAL_TYPE:
                    SearchOperationUtils.error(messageHandler, OperandMessageKeys.WRONG_NOT_ALINEA_ATTRIBUTE, scope);
                    return null;
                case FieldRank.UNKNOWN_TYPE:
                    SearchOperationUtils.error(messageHandler, OperandMessageKeys.UNKNOWN_ATTRIBUTE, scope);
                    return null;
                default:
                    return AlineaFilters.getFieldFilter(scope, fieldRank);
            }
        } catch (ParseException pe) {

        }
        SearchOperationUtils.error(messageHandler, OperandMessageKeys.UNKNWON_SCOPE, scope);
        return null;
    }

    public static int[] toAcceptOperandArray(SearchOperation searchOperation) {
        List<Integer> list = new ArrayList<Integer>();
        Operand operand = searchOperation.getRootOperand();
        if (operand instanceof AllOperand) {
            throw new IllegalStateException("Cannot be called if searchOperation.getRootOperand() instance of AllOperand");
        } else if (operand instanceof SearchTokenOperand) {
            checkAcceptOperand((SearchTokenOperand) operand, list);
        } else {
            checkAcceptOperand((SubOperand) operand, list);
        }
        int size = list.size();
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = list.get(i);
        }
        return result;
    }

    private static void checkAcceptOperand(SearchTokenOperand operand, List<Integer> list) {
        if (operand.isAcceptMode()) {
            list.add(operand.getOperandNumber());
        }
    }

    private static void checkAcceptOperand(SubOperand subOperand, List<Integer> list) {
        int count = subOperand.size();
        for (int i = 0; i < count; i++) {
            Operand operand = subOperand.get(i);
            if (operand instanceof SearchTokenOperand) {
                checkAcceptOperand((SearchTokenOperand) operand, list);
            } else {
                checkAcceptOperand((SubOperand) operand, list);
            }
        }
    }

    public static SearchTokenOperand toSearchTokenOperand(int operandNumber, boolean acceptMode, SearchToken searchToken, AlineaFilter alineaFilter) {
        return new DefaultSearchTokenOperand(operandNumber, acceptMode, searchToken, alineaFilter);
    }

    public static SearchTokenOperand derive(SearchTokenOperand searchTokenOperand, SearchToken searchToken) {
        return new DefaultSearchTokenOperand(searchTokenOperand.getOperandNumber(), searchTokenOperand.isAcceptMode(), searchToken, searchTokenOperand.getAlineaFilter());
    }

    public static boolean hasMandatoryOperand(SearchOperation searchOperation) {
        Operand rootOperand = searchOperation.getRootOperand();
        if (rootOperand instanceof AllOperand) {
            return false;
        } else if (rootOperand instanceof SearchTokenOperand) {
            return ((SearchTokenOperand) rootOperand).isAcceptMode();
        } else {
            return hasMandatoryOperand((SubOperand) rootOperand);
        }
    }

    private static boolean hasMandatoryOperand(SubOperand subOperand) {
        int count = subOperand.size();
        short operator = subOperand.getOperator();
        boolean mandatory;
        if (operator == LogicalOperationConstants.INTERSECTION_OPERATOR) {
            mandatory = false;
            for (int i = 0; i < count; i++) {
                Operand operand = subOperand.get(i);
                if (operand instanceof SearchTokenOperand) {
                    if (((SearchTokenOperand) operand).isAcceptMode()) {
                        mandatory = true;
                        break;
                    }
                } else {
                    SubOperand subsubOperand = (SubOperand) operand;
                    boolean test = hasMandatoryOperand(subsubOperand);
                    if (test) {
                        mandatory = true;
                        break;
                    }
                }
            }
        } else {
            mandatory = true;
            for (int i = 0; i < count; i++) {
                Operand operand = subOperand.get(i);
                if (operand instanceof SearchTokenOperand) {
                    if (!((SearchTokenOperand) operand).isAcceptMode()) {
                        mandatory = false;
                        break;
                    }
                } else {
                    SubOperand subsubOperand = (SubOperand) operand;
                    boolean test = hasMandatoryOperand(subsubOperand);
                    if (!test) {
                        mandatory = false;
                        break;
                    }
                }
            }
        }
        return mandatory;
    }

    private static void appendSimpleSearchToken(StringBuilder buf, SimpleSearchToken simpleSearchToken) {
        int searchType = simpleSearchToken.getSearchType();
        if ((searchType & TextConstants.ENDSWITH) != 0) {
            buf.append('*');
        }
        buf.append(simpleSearchToken.getTokenString());
        if ((searchType & TextConstants.STARTSWITH) != 0) {
            buf.append('*');
        }
    }

    public static void error(MessageHandler messageHandler, String key, Object... values) {
        error(messageHandler, LocalisationUtils.toMessage(key, values));
    }

    public static void error(MessageHandler messageHandler, Message message) {
        messageHandler.addMessage("severe.operand", message);
    }


    private static class DefaultSearchTokenOperand implements SearchTokenOperand {

        private final int operandNumber;
        private final boolean acceptMode;
        private final SearchToken searchToken;
        private final AlineaFilter alineaFilter;

        private DefaultSearchTokenOperand(int operandNumber, boolean acceptMode, SearchToken searchToken, AlineaFilter alineaFilter) {
            this.operandNumber = operandNumber;
            this.acceptMode = acceptMode;
            this.searchToken = searchToken;
            this.alineaFilter = alineaFilter;
        }

        @Override
        public SearchToken getSearchToken() {
            return searchToken;
        }

        @Override
        public boolean isAcceptMode() {
            return acceptMode;
        }

        @Override
        public int getOperandNumber() {
            return operandNumber;
        }

        @Override
        public AlineaFilter getAlineaFilter() {
            return alineaFilter;
        }

        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder();
            if (!acceptMode) {
                buf.append('!');
            }
            String filterPrefix = alineaFilter.getFilterPrefix();
            if (filterPrefix.length() > 0) {
                buf.append(filterPrefix);
                buf.append(':');
            }
            if (searchToken instanceof SimpleSearchToken) {
                appendSimpleSearchToken(buf, (SimpleSearchToken) searchToken);
            } else {
                MultiSearchToken multiSearchToken = (MultiSearchToken) searchToken;
                buf.append('"');
                int count = multiSearchToken.getSubtokenCount();
                for (int i = 0; i < count; i++) {
                    if (i != 0) {
                        buf.append(' ');
                    }
                    appendSimpleSearchToken(buf, multiSearchToken.getSubtoken(i));
                }
                buf.append('"');
            }
            return buf.toString();
        }

    }


    private static class InternalAllOperand implements AllOperand {

        private InternalAllOperand() {
        }

        @Override
        public String toString() {
            return "*";
        }

    }

    public static List<SearchTokenOperand> wrap(SearchTokenOperand[] array) {
        return new SearchTokenOperandList(array);
    }


    private static class SearchTokenOperandList extends AbstractList<SearchTokenOperand> implements RandomAccess {

        private final SearchTokenOperand[] array;

        private SearchTokenOperandList(SearchTokenOperand[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SearchTokenOperand get(int index) {
            return array[index];
        }

    }

}
