/* ScrutariLib_SearchEngine - Copyright (c) 2015-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.operands.search;

import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.logicaloperation.LogicalOperationParser;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.OperandException;
import net.mapeadores.util.logicaloperation.SimpleOperand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.TextConstants;
import net.mapeadores.util.text.search.SearchToken;
import net.mapeadores.util.text.search.SearchUtils;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.searchengine.api.operands.OperandMessageKeys;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;


/**
 *
 * @author Vincent Calame
 */
public final class LogicalSearchOperationEngine {

    private final MessageHandler messageHandler;
    private final FieldRankManager fieldRankManager;

    private LogicalSearchOperationEngine(MessageHandler messageHandler, FieldRankManager fieldRankManager) {
        this.messageHandler = messageHandler;
        this.fieldRankManager = fieldRankManager;
    }

    public static SearchOperation parse(String q, MessageHandler messageHandler, FieldRankManager fieldRankManager) {
        if (q.equals("*")) {
            return SearchOperationBuilder.build(SearchOperationUtils.ALL_OPERAND);
        }
        Operand operand;
        try {
            operand = LogicalOperationParser.parse(q);
        } catch (OperandException oe) {
            SearchOperationUtils.error(messageHandler, oe.getErrorKey(), String.valueOf(oe.getCharIndex()));
            return null;
        }
        LogicalSearchOperationEngine engine = new LogicalSearchOperationEngine(messageHandler, fieldRankManager);
        try {
            return engine.convert(operand);
        } catch (ErrorMessageException errorMessageException) {
            SearchOperationUtils.error(messageHandler, errorMessageException.getErrorMessage());
            return null;
        }
    }

    private SearchOperation convert(Operand rootOperand) throws ErrorMessageException {
        Operand checkedOperand;
        if (rootOperand instanceof SimpleOperand) {
            checkedOperand = toSearchTokenOperand(1, (SimpleOperand) rootOperand);
        } else {
            NumberProvider numberProvider = new NumberProvider();
            checkedOperand = convertSubOperand(numberProvider, (SubOperand) rootOperand);
        }
        return SearchOperationBuilder.build(checkedOperand);
    }

    private Operand convertSubOperand(NumberProvider numberProvider, SubOperand subOperand) throws ErrorMessageException {
        int operandLength = subOperand.size();
        Operand[] result = new Operand[operandLength];
        for (int i = 0; i < operandLength; i++) {
            Operand operand = subOperand.get(i);
            if (operand instanceof SimpleOperand) {
                result[i] = toSearchTokenOperand(numberProvider.newNumber(), (SimpleOperand) operand);
            } else {
                result[i] = convertSubOperand(numberProvider, (SubOperand) operand);
            }
        }
        return LogicalOperationUtils.toSubOperand(result, subOperand.getOperator());
    }

    private SearchTokenOperand toSearchTokenOperand(int operandNumber, SimpleOperand simpleOperand) throws ErrorMessageException {
        boolean acceptMode = simpleOperand.isAcceptMode();
        String scope = simpleOperand.getScope();
        AlineaFilter alineaFilter = null;
        if (!scope.isEmpty()) {
            alineaFilter = SearchOperationUtils.parse(scope, messageHandler, fieldRankManager);
        }
        if (alineaFilter == null) {
            alineaFilter = AlineaFilters.ALL_FILTER;
        }
        SearchToken searchToken = parseSearchToken(simpleOperand.getBody(), simpleOperand.getOperandString());
        if (searchToken == null) {
            return null;
        }
        return SearchOperationUtils.toSearchTokenOperand(operandNumber, acceptMode, searchToken, alineaFilter);
    }

    private SearchToken parseSearchToken(String tokenString, String operandString) throws ErrorMessageException {
        int length = tokenString.length();
        if (length == 0) {
            throw new ErrorMessageException(OperandMessageKeys.EMPTY_SEARCHTOKEN, operandString);
        }
        boolean onQuote = false;
        if (tokenString.charAt(0) == '"') {
            if (tokenString.charAt(length - 1) != '"') {
                throw new ErrorMessageException(OperandMessageKeys.WRONG_UNCLOSED_QUOTE, operandString);
            }
            tokenString = tokenString.substring(1, length - 1).trim();
            length = tokenString.length();
            onQuote = true;
            if (length == 0) {
                throw new ErrorMessageException(OperandMessageKeys.EMPTY_SEARCHTOKEN, operandString);
            }
        }
        String[] tokenArray = StringUtils.getTokens(tokenString, ' ', StringUtils.EMPTY_EXCLUDE);
        if (onQuote) {
            int tokenLength = tokenArray.length;
            SimpleSearchToken[] array = new SimpleSearchToken[tokenLength];
            for (int i = 0; i < tokenLength; i++) {
                array[i] = parseSimpleSearchToken(tokenArray[i], operandString);
            }
            return SearchUtils.toMultiSearchToken(array);
        } else {
            if (tokenArray.length > 1) {
                throw new ErrorMessageException(OperandMessageKeys.WRONG_UNEXPECTED_SPACE, operandString);
            }
            return parseSimpleSearchToken(tokenArray[0], operandString);
        }
    }

    private SimpleSearchToken parseSimpleSearchToken(String token, String operandString) throws ErrorMessageException {
        int length = token.length();
        int startIndex = 0;
        int currentType = TextConstants.MATCHES;
        if (token.charAt(0) == '*') {
            startIndex = 1;
            currentType = TextConstants.ENDSWITH;
        }
        if (token.charAt(length - 1) == '*') {
            length = length - 1;
            currentType = currentType | TextConstants.STARTSWITH;
        }
        if (startIndex >= length) {
            throw new ErrorMessageException(OperandMessageKeys.WRONG_MISSING_WORD, operandString);
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = startIndex; i < length; i++) {
            char carac = token.charAt(i);
            if (!Character.isLetterOrDigit(carac)) {
                throw new ErrorMessageException(OperandMessageKeys.WRONG_NOT_LETTERORDIGIT_CHARACTER, operandString);
            }
            stringBuilder.append(carac);
        }
        return SearchUtils.toSimpleSearchToken(stringBuilder.toString(), currentType);
    }


    private static class NumberProvider {

        private int currentOperandNumber = 1;

        private NumberProvider() {

        }

        private int newNumber() {
            int current = currentOperandNumber;
            currentOperandNumber++;
            return current;
        }


    }

}
