/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logicaloperation.LogicalOperationConstants;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.operands.eligibility.CodeEligibility;
import net.scrutari.searchengine.api.operands.eligibility.Eligibility;
import net.scrutari.searchengine.api.operands.eligibility.FicheDataEligibility;
import net.scrutari.searchengine.api.operands.eligibility.SubEligibility;


/**
 *
 * @author Vincent Calame
 */
public class EligibilityBuffer {

    private final DataAccess dataAccess;
    private final Map<Integer, Boolean> bufferMap = new HashMap<Integer, Boolean>();
    private final Eligibility ficheEligibility;
    private Set<Lang> langCheckSet;
    private FicheData currentFicheData;
    private boolean currentFicheDataInit;

    public EligibilityBuffer(DataAccess dataAccess, Eligibility ficheEligibility) {
        this.dataAccess = dataAccess;
        this.ficheEligibility = ficheEligibility;
    }

    public void addLangCheck(Lang[] langArray) {
        langCheckSet = new HashSet<Lang>();
        for (Lang lang : langArray) {
            langCheckSet.add(lang);
        }
    }

    public boolean acceptFiche(Integer ficheCode) {
        if ((ficheEligibility == null) & (langCheckSet == null)) {
            return true;
        }
        Boolean bool = bufferMap.get(ficheCode);
        if (bool != null) {
            return bool;
        }
        boolean bl = checkAcceptFicheCode(ficheCode);
        bufferMap.put(ficheCode, bl);
        return bl;
    }

    public boolean acceptMotcle(Integer motcleCode) {
        return true;
    }

    private void clearCurrent() {
        currentFicheData = null;
        currentFicheDataInit = false;
    }

    private boolean checkAcceptFicheCode(Integer code) {
        boolean test = true;
        if (ficheEligibility != null) {
            test = testEligibility(ficheEligibility, code);
        }
        if ((test) && (langCheckSet != null)) {
            Lang ficheLang;
            if (currentFicheData != null) {
                ficheLang = currentFicheData.getLang();
            } else {
                ficheLang = dataAccess.getFicheLang(code);
            }
            if (!langCheckSet.contains(ficheLang)) {
                test = false;
            }
        }
        clearCurrent();
        return test;
    }

    private boolean testEligibility(Eligibility eligibility, Integer code) {
        if (eligibility instanceof CodeEligibility) {
            return ((CodeEligibility) eligibility).acceptCode(code);
        } else if (eligibility instanceof FicheDataEligibility) {
            return ((FicheDataEligibility) eligibility).acceptFicheData(getFicheData(code));
        } else if (eligibility instanceof SubEligibility) {
            SubEligibility subEligibility = (SubEligibility) eligibility;
            switch (subEligibility.getOperator()) {
                case LogicalOperationConstants.UNION_OPERATOR:
                    return testUnion(subEligibility, code);
                case LogicalOperationConstants.INTERSECTION_OPERATOR:
                    return testIntersection(subEligibility, code);
                default:
                    throw new SwitchException("Unknown operator = " + subEligibility.getOperator());
            }
        } else {
            throw new SwitchException("Unknown eligibility instance = " + eligibility.getClass().getName());
        }

    }

    private boolean testUnion(SubEligibility subEligibility, Integer code) {
        int count = subEligibility.getEligibilityCount();
        for (int i = 0; i < count; i++) {
            Eligibility eligibility = subEligibility.getEligibility(i);
            boolean test = testEligibility(eligibility, code);
            if (test) {
                return true;
            }
        }
        return false;
    }

    private boolean testIntersection(SubEligibility subEligibility, Integer code) {
        int count = subEligibility.getEligibilityCount();
        for (int i = 0; i < count; i++) {
            Eligibility eligibility = subEligibility.getEligibility(i);
            boolean test = testEligibility(eligibility, code);
            if (!test) {
                return false;
            }
        }
        return true;
    }

    private FicheData getFicheData(Integer code) {
        if (!currentFicheDataInit) {
            currentFicheDataInit = true;
            currentFicheData = dataAccess.getFicheData(code);
        }
        return currentFicheData;
    }

}
