/* ScrutariLib_SearchEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.dom;

import java.text.ParseException;
import java.util.function.Consumer;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logicaloperation.OperandException;
import net.mapeadores.util.xml.DOMUtils;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityOperandParser;
import net.scrutari.searchengine.tools.options.ListReductionBuilder;
import net.scrutari.searchengine.tools.options.PertinencePonderationBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchSourceBuilder;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchSourceDOMReader {

    private final FicheSearchSourceBuilder ficheSearchSourceBuilder;

    public FicheSearchSourceDOMReader(FicheSearchSourceBuilder ficheSearchSourceBuilder) {
        this.ficheSearchSourceBuilder = ficheSearchSourceBuilder;
    }

    public void readFicheSearchSource(Element element) {
        DOMUtils.readChildren(element, new RootConsumer());
    }


    private class RootConsumer implements Consumer<Element> {

        private RootConsumer() {
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("q")) {
                String q = DOMUtils.readSimpleElement(element);
                ficheSearchSourceBuilder.setCanonicalQ(CanonicalQ.newInstance(q));
            } else if (tagName.equals("q-info")) {
                String q = element.getAttribute("q");
                ficheSearchSourceBuilder.setCanonicalQ(CanonicalQ.newInstance(q));
            } else if (tagName.equals("options")) {
                SearchOptionsDefBuilder searchOptionsDefBuilder = new SearchOptionsDefBuilder();
                String langString = element.getAttribute("lang");
                if (langString.length() == 0) {
                    langString = element.getAttribute("lang-ui");
                }
                try {
                    Lang lang = Lang.parse(langString);
                    searchOptionsDefBuilder.setLang(lang);
                } catch (ParseException pe) {
                }
                DOMUtils.readChildren(element, new SearchOptionsConsumer(searchOptionsDefBuilder));
                ficheSearchSourceBuilder.setSearchOptionsDef(searchOptionsDefBuilder.toSearchOptionsDef());
            }
        }

    }


    private class SearchOptionsConsumer implements Consumer<Element> {

        private final LangsConsumer langsConsumer;
        private final SearchOptionsDefBuilder searchOptionsDefBuilder;

        private SearchOptionsConsumer(SearchOptionsDefBuilder searchOptionsDefBuilder) {
            this.searchOptionsDefBuilder = searchOptionsDefBuilder;
            this.langsConsumer = new LangsConsumer(searchOptionsDefBuilder);
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if ((tagName.equals("lang-list")) || (tagName.equals("langs"))) {
                DOMUtils.readChildren(element, langsConsumer);
            } else if (tagName.equals("fiche-list")) {
                readReductionList(ScrutariDataURI.FICHEURI_TYPE, element);
            } else if (tagName.equals("base-list")) {
                readReductionList(ScrutariDataURI.BASEURI_TYPE, element);
            } else if (tagName.equals("corpus-list")) {
                readReductionList(ScrutariDataURI.CORPUSURI_TYPE, element);
            } else if (tagName.equals("thesaurus-list")) {
                readReductionList(ScrutariDataURI.THESAURUSURI_TYPE, element);
            } else if (tagName.equals("ponderation")) {
                int occurrence = 0;
                int date = 0;
                int origin = 0;
                int lang = 0;
                try {
                    occurrence = Integer.parseInt(element.getAttribute("occurrence"));
                } catch (NumberFormatException nfe) {
                }
                try {
                    date = Integer.parseInt(element.getAttribute("date"));
                } catch (NumberFormatException nfe) {
                }
                try {
                    origin = Integer.parseInt(element.getAttribute("origin"));
                } catch (NumberFormatException nfe) {
                }
                try {
                    lang = Integer.parseInt(element.getAttribute("lang"));
                } catch (NumberFormatException nfe) {
                }
                searchOptionsDefBuilder.setPertinencePonderation(PertinencePonderationBuilder.toPertinencePonderation(occurrence, date, origin, lang));
            } else if (tagName.equals("fiche-eligibility")) {
                EligibilityOperand eligibilityOperand = toEligibilityOperand(element);
                if (eligibilityOperand != null) {
                    searchOptionsDefBuilder.addFicheEligibilityOperand(eligibilityOperand);
                }
            } else if (tagName.equals("attr")) {
                AttributeUtils.readAttrElement(searchOptionsDefBuilder.getAttributesBuilder(), element);
            }
        }

        private EligibilityOperand toEligibilityOperand(Element element) {
            String operationString = DOMUtils.readSimpleElement(element);
            try {
                return EligibilityOperandParser.parse(operationString, null, LogUtils.NULL_MULTIMESSAGEHANDLER);
            } catch (OperandException pe) {
                return null;
            }
        }

        private void readReductionList(short type, Element element) {
            ListReductionBuilder listReductionBuilder = new ListReductionBuilder(type);
            String modeAttribute = element.getAttribute("mode");
            if (modeAttribute.equals("exclude")) {
                listReductionBuilder.setExclude(true);
            }
            DOMUtils.readChildren(element, new ListReductionConsumer(listReductionBuilder));
            searchOptionsDefBuilder.putListReduction(listReductionBuilder.toListReduction());
        }

    }


    private static class LangsConsumer implements Consumer<Element> {

        private final SearchOptionsDefBuilder searchOptionsDefBuilder;

        private LangsConsumer(SearchOptionsDefBuilder searchOptionsDefBuilder) {
            this.searchOptionsDefBuilder = searchOptionsDefBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lang")) {
                try {
                    Lang lang = Lang.parse(DOMUtils.readSimpleElement(element));
                    searchOptionsDefBuilder.addFilterLang(lang);
                } catch (ParseException pe) {
                }
            }
        }

    }


    private static class ListReductionConsumer implements Consumer<Element> {

        private final ListReductionBuilder listReductionBuilder;

        private ListReductionConsumer(ListReductionBuilder listReductionBuilder) {
            this.listReductionBuilder = listReductionBuilder;
        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("uri")) {
                try {
                    ScrutariDataURI scrutariDataURI = URIParser.parse(DOMUtils.readSimpleElement(element));
                    listReductionBuilder.add(scrutariDataURI);
                } catch (URIParseException | IllegalArgumentException e) {

                }
            }
        }

    }


}
