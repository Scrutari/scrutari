/* ScrutariLib_SearchEngine - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.exceptions.SwitchException;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.tools.codes.CodesUtils;
import net.scrutari.searchengine.api.options.ListReduction;


/**
 *
 * @author Vincent Calame
 */
public class ListReductionBuilder {

    private final short type;
    private final Set<ScrutariDataURI> uriSet = new LinkedHashSet<ScrutariDataURI>();
    private boolean exclude;


    public ListReductionBuilder(short type) {
        this.type = type;
    }

    public short getType() {
        return type;
    }

    public void setExclude(boolean exclude) {
        this.exclude = exclude;
    }

    public boolean isEmpty() {
        return uriSet.isEmpty();
    }

    public void add(ScrutariDataURI scrutariDataURI) {
        if (scrutariDataURI.getType() != type) {
            throw new IllegalArgumentException("Type don't matching: " + type + " / " + scrutariDataURI.getType());
        }
        uriSet.add(scrutariDataURI);
    }

    public ListReduction toListReduction() {
        List<ScrutariDataURI> scrutariDataURIList = CodesUtils.wrap(uriSet.toArray(new ScrutariDataURI[uriSet.size()]));
        return new InternalListReduction(type, exclude, scrutariDataURIList);
    }

    public static ListReduction build(DataAccess dataAccess, short type, boolean exclude, Collection<Integer> codes) {
        ListReductionBuilder builder = new ListReductionBuilder(type);
        builder.setExclude(exclude);
        for (Integer code : codes) {
            switch (type) {
                case ScrutariDataURI.BASEURI_TYPE:
                    BaseData baseData = dataAccess.getBaseData(code);
                    if (baseData != null) {
                        builder.uriSet.add(baseData.getBaseURI());
                    }
                    break;
                case ScrutariDataURI.CORPUSURI_TYPE:
                    CorpusData corpusData = dataAccess.getCorpusData(code);
                    if (corpusData != null) {
                        builder.uriSet.add(corpusData.getCorpusURI());
                    }
                    break;
                case ScrutariDataURI.THESAURUSURI_TYPE:
                    ThesaurusData thesaurusData = dataAccess.getThesaurusData(code);
                    if (thesaurusData != null) {
                        builder.uriSet.add(thesaurusData.getThesaurusURI());
                    }
                    break;
                case ScrutariDataURI.FICHEURI_TYPE:
                    FicheInfo ficheInfo = dataAccess.getFicheInfo(code);
                    if (ficheInfo != null) {
                        builder.uriSet.add(ficheInfo.getFicheURI());
                    }
                    break;
                case ScrutariDataURI.MOTCLEURI_TYPE:
                    MotcleInfo motcleInfo = dataAccess.getMotcleInfo(code);
                    if (motcleInfo != null) {
                        builder.uriSet.add(motcleInfo.getMotcleURI());
                    }
                    break;
                default:
                    throw new SwitchException("Unkown type: " + type);
            }
        }
        return builder.toListReduction();
    }


    private static class InternalListReduction implements ListReduction {

        private final short type;
        private final boolean exclude;
        private final List<ScrutariDataURI> scrutariDataURIList;

        private InternalListReduction(short type, boolean exclude, List<ScrutariDataURI> scrutariDataURIList) {
            this.type = type;
            this.exclude = exclude;
            this.scrutariDataURIList = scrutariDataURIList;
        }

        @Override
        public short getType() {
            return type;
        }

        @Override
        public boolean isExclude() {
            return exclude;
        }

        @Override
        public List<ScrutariDataURI> getScrutariDataURIList() {
            return scrutariDataURIList;
        }

    }

}
