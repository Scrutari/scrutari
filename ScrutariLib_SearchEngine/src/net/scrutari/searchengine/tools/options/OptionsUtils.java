/* ScrutariLib_SearchEngine - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.RandomAccess;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.tools.EligibilityBuffer;


/**
 *
 * @author Vincent Calame
 */
public final class OptionsUtils {

    public final static List<Category> EMPTY_CATEGORYLIST = Collections.emptyList();

    private OptionsUtils() {
    }

    public static Collection<Integer> getSelectionThesaurusCodes(DataAccess dataAccess, Integer[] corpusCodeArray) {
        Set<Integer> thesaurusSet = new LinkedHashSet<Integer>();
        for (Integer corpusCode : corpusCodeArray) {
            for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                if (dataAccess.hasIndexation(corpusCode, thesaurusData.getThesaurusCode())) {
                    thesaurusSet.add(thesaurusData.getThesaurusCode());
                }
            }
        }
        return thesaurusSet;
    }

    public static List<Integer> buildFicheCodeList(DataAccess dataAccess, SearchOptions searchOptions) {
        EligibilityBuffer eligibilityBuffer = new EligibilityBuffer(dataAccess, searchOptions.getFicheEligibility());
        SearchOptionsDef searchOptionsDef = searchOptions.getSearchOptionsDef();
        Set<Lang> langSet = null;
        Langs langs = searchOptionsDef.getFilterLangs();
        if (!langs.isEmpty()) {
            langSet = new HashSet<Lang>(langs);
        }
        List<Integer> resultList = new ArrayList<Integer>();
        if (searchOptions.isWithCorpusCodes()) {
            for (Integer corpusCode : searchOptions.getCorpusCodeList()) {
                CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
                populateFicheCode(corpusData, resultList, dataAccess, langSet, eligibilityBuffer);
            }
        } else {
            for (CorpusData corpusData : dataAccess.getCorpusDataList()) {
                populateFicheCode(corpusData, resultList, dataAccess, langSet, eligibilityBuffer);
            }
        }
        return resultList;
    }

    private static void populateFicheCode(CorpusData corpusData, List<Integer> resultList, DataAccess dataAccess, Set<Lang> langSet, EligibilityBuffer eligibilityBuffer) {
        for (Integer ficheCode : corpusData.getFicheCodeList()) {
            boolean accept = true;
            if (langSet != null) {
                Lang lang = dataAccess.getFicheLang(ficheCode);
                if (!langSet.contains(lang)) {
                    accept = false;
                }
            }
            if ((accept) && (!eligibilityBuffer.acceptFiche(ficheCode))) {
                accept = false;
            }
            if (accept) {
                resultList.add(ficheCode);
            }
        }
    }

    public static List<Integer> buildMotcleCodeList(DataAccess dataAccess, SearchOptions searchOptions) {
        EligibilityBuffer eligibilityBuffer = new EligibilityBuffer(dataAccess, searchOptions.getFicheEligibility());
        List<Integer> resultList = new ArrayList<Integer>();
        if (searchOptions.isWithThesaurusCodes()) {
            for (Integer thesaurusCode : searchOptions.getThesaurusCodeList()) {
                ThesaurusData thesaurusData = dataAccess.getThesaurusData(thesaurusCode);
                populateMotcleCode(thesaurusData, resultList, eligibilityBuffer);
            }
        } else {
            for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                populateMotcleCode(thesaurusData, resultList, eligibilityBuffer);
            }
        }
        return resultList;
    }

    private static void populateMotcleCode(ThesaurusData thesaurusData, List<Integer> resultList, EligibilityBuffer eligibilityBuffer) {
        for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
            if (eligibilityBuffer.acceptMotcle(motcleCode)) {
                resultList.add(motcleCode);
            }
        }
    }

    public static CorpusData[] toCorpusDataArray(List<Integer> codeList, DataAccess dataAccess) {
        int size = codeList.size();
        CorpusData[] result = new CorpusData[size];
        for (int i = 0; i < size; i++) {
            result[i] = dataAccess.getCorpusData(codeList.get(i));
        }
        return result;
    }

    public static ThesaurusData[] toThesaurusDataArray(List<Integer> codeList, DataAccess dataAccess) {
        int size = codeList.size();
        ThesaurusData[] result = new ThesaurusData[size];
        for (int i = 0; i < size; i++) {
            result[i] = dataAccess.getThesaurusData(codeList.get(i));
        }
        return result;
    }

    public static List<CorpusData> toCorpusDataList(List<Integer> codeList, DataAccess dataAccess) {
        int size = codeList.size();
        List<CorpusData> result = new ArrayList<CorpusData>();
        for (int i = 0; i < size; i++) {
            result.add(dataAccess.getCorpusData(codeList.get(i)));
        }
        return result;
    }

    public static List<ThesaurusData> toThesaurusDataList(List<Integer> codeList, DataAccess dataAccess) {
        int size = codeList.size();
        List<ThesaurusData> result = new ArrayList<ThesaurusData>();
        for (int i = 0; i < size; i++) {
            result.add(dataAccess.getThesaurusData(codeList.get(i)));
        }
        return result;
    }

    public static List<Category> wrap(Category[] array) {
        return new CategoryList(array);
    }


    private static class CategoryList extends AbstractList<Category> implements RandomAccess {

        private final Category[] array;

        private CategoryList(Category[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Category get(int index) {
            return array[index];
        }

    }

}
