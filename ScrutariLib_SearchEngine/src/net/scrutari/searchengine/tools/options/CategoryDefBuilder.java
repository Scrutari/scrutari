/* ScrutariLib_SearchEngine - Copyright (c) 2014-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.misc.DefBuilder;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.db.tools.codes.CodesUtils;
import net.scrutari.searchengine.api.options.CategoryDef;


/**
 *
 * @author Vincent Calame
 */
public class CategoryDefBuilder extends DefBuilder {

    private final String name;
    private final List<CorpusURI> uriList = new ArrayList<CorpusURI>();
    private int rank;

    public CategoryDefBuilder(String name) {
        this.name = name;
    }

    public void addCorpusURI(CorpusURI corpusURI) {
        uriList.add(corpusURI);
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public CategoryDef toCategoryDef() {
        List<CorpusURI> corpusURIList = CodesUtils.wrap(uriList.toArray(new CorpusURI[uriList.size()]));
        Labels titleLabels = toLabels();
        Attributes attributes = toAttributes();
        Phrases phrases = toPhrases();
        return new InternalCategoryDef(name, rank, corpusURIList, phrases, titleLabels, attributes);
    }


    private static class InternalCategoryDef implements CategoryDef {

        private final String name;
        private final int rank;
        private final List<CorpusURI> corpusURIList;
        private final Labels titleLabels;
        private final Phrases phrases;
        private final Attributes attributes;


        private InternalCategoryDef(String name, int rank, List<CorpusURI> corpusURIList, Phrases phrases,
                Labels titleLabels, Attributes attributes) {
            this.name = name;
            this.rank = rank;
            this.corpusURIList = corpusURIList;
            this.titleLabels = titleLabels;
            this.phrases = phrases;
            this.attributes = attributes;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<CorpusURI> getCorpusURIList() {
            return corpusURIList;
        }

        @Override
        public int getRank() {
            return rank;
        }

        @Override
        public Labels getTitleLabels() {
            return titleLabels;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

        @Override
        public Phrases getPhrases() {
            return phrases;
        }

    }

}
