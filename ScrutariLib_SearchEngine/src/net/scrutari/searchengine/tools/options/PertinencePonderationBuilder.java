/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import net.scrutari.searchengine.api.options.PertinencePonderation;


/**
 *
 * @author Vincent Calame
 */
public final class PertinencePonderationBuilder {

    public final static PertinencePonderation DEFAULT_PONDERATION = new InternalPertinencePonderation(0.55f, 0.25f, 0.15f, 0.05f);

    private PertinencePonderationBuilder() {
    }

    public static PertinencePonderation toPertinencePonderation(int occurrence, int date, int origin, int lang) {
        if (date < 0) {
            date = 0;
        }
        if (origin < 0) {
            origin = 0;
        }
        if (occurrence < 0) {
            occurrence = 0;
        }
        if (lang < 0) {
            lang = 0;
        }
        float somme = date + origin + occurrence + lang;
        if (somme == 0) {
            return DEFAULT_PONDERATION;
        }
        float occurrenceF = ((float) occurrence) / somme;
        float dateF = ((float) date) / somme;
        float originF = ((float) origin) / somme;
        float langF = ((float) lang) / somme;
        return new InternalPertinencePonderation(occurrenceF, dateF, originF, langF);
    }


    private static class InternalPertinencePonderation implements PertinencePonderation {

        private final float origin;
        private final float date;
        private final float occurrence;
        private final float lang;

        private InternalPertinencePonderation(float occurrence, float date, float origin, float lang) {
            this.occurrence = occurrence;
            this.date = date;
            this.origin = origin;
            this.lang = lang;
        }

        @Override
        public float getOriginPonderation() {
            return origin;
        }

        @Override
        public float getDatePonderation() {
            return date;
        }

        @Override
        public float getOccurrencePonderation() {
            return occurrence;
        }

        @Override
        public float getLangPonderation() {
            return lang;
        }

    }

}
