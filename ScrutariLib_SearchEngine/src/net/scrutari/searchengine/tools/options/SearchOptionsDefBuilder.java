/* ScrutariLib_SearchEngine - Copyright (c) 2014-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logicaloperation.LogicalOperationConstants;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityOperandParser;


/**
 *
 * @author Vincent Calame
 */
public class SearchOptionsDefBuilder {

    public final static SearchOptionsDef EMPTY_SEARCHOPTIONSDEF = new InternalSearchOptionsDef(true, null, null, new HashMap<Short, ListReduction>(), null, null, AttributeUtils.EMPTY_ATTRIBUTES);
    private final AttributesBuilder attributesBuilder = new AttributesBuilder();
    private final List<EligibilityOperand> ficheOperandList = new ArrayList<EligibilityOperand>();
    private final Map<Short, ListReduction> listReductionMap = new HashMap<Short, ListReduction>();
    private final Set<Lang> filterLangSet = new LinkedHashSet<Lang>();
    private Lang lang;
    private PertinencePonderation pertinencePonderation;

    public SearchOptionsDefBuilder() {
    }

    public SearchOptionsDefBuilder setLang(Lang lang) {
        this.lang = lang;
        return this;
    }

    public SearchOptionsDefBuilder addFilterLang(Lang filterLang) {
        filterLangSet.add(filterLang);
        return this;
    }

    public boolean hasFilterLang() {
        return !filterLangSet.isEmpty();
    }

    public Lang reduceToUniqueLang() {
        if (filterLangSet.isEmpty()) {
            return null;
        }
        boolean first = true;
        Lang result = null;
        for (Iterator<Lang> it = filterLangSet.iterator(); it.hasNext();) {
            if (first) {
                result = it.next();
                first = false;
            } else {
                it.next();
                it.remove();
            }
        }
        return result;
    }

    public SearchOptionsDefBuilder setPertinencePonderation(PertinencePonderation pertinencePonderation) {
        this.pertinencePonderation = pertinencePonderation;
        return this;
    }

    public SearchOptionsDefBuilder addFicheEligibilityOperand(EligibilityOperand operand) {
        if (operand != null) {
            ficheOperandList.add(operand);
        }
        return this;
    }

    public SearchOptionsDefBuilder putListReduction(ListReduction listReduction) {
        short type = listReduction.getType();
        if (listReduction.getScrutariDataURIList().isEmpty()) {
            listReductionMap.remove(type);
        } else {
            listReductionMap.put(type, listReduction);
        }
        return this;
    }

    public AttributesBuilder getAttributesBuilder() {
        return attributesBuilder;
    }

    public SearchOptionsDef toSearchOptionsDef() {
        EligibilityOperand ficheRootOperand = EligibilityOperandParser.toEligibilityOperand(ficheOperandList, LogicalOperationConstants.INTERSECTION_OPERATOR);
        Map<Short, ListReduction> finalListReductionMap = new HashMap<Short, ListReduction>(listReductionMap);
        Langs filterLangs = LangsUtils.wrap(filterLangSet);
        return new InternalSearchOptionsDef(false, lang, filterLangs, finalListReductionMap, ficheRootOperand, pertinencePonderation, attributesBuilder.toAttributes());
    }

    public static SearchOptionsDefBuilder init() {
        return new SearchOptionsDefBuilder();
    }


    private static class InternalSearchOptionsDef implements SearchOptionsDef {

        private final boolean empty;
        private final Lang lang;
        private final Langs filterLangs;
        private final Map<Short, ListReduction> listReductionMap;
        private final EligibilityOperand ficheEligibilityOperand;
        private final PertinencePonderation pertinencePonderation;
        private final Attributes attributes;

        private InternalSearchOptionsDef(boolean empty, Lang lang, Langs filterLangs, Map<Short, ListReduction> listReductionMap, EligibilityOperand ficheEligibilityOperand, PertinencePonderation pertinencePonderation, Attributes attributes) {
            this.empty = empty;
            this.lang = lang;
            this.filterLangs = filterLangs;
            this.ficheEligibilityOperand = ficheEligibilityOperand;
            this.pertinencePonderation = pertinencePonderation;
            this.listReductionMap = listReductionMap;
            this.attributes = attributes;
        }

        @Override
        public boolean isEmpty() {
            return empty;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public Langs getFilterLangs() {
            return filterLangs;
        }

        @Override
        public EligibilityOperand getFicheEligibilityOperand() {
            return ficheEligibilityOperand;
        }

        @Override
        public PertinencePonderation getPertinencePonderation() {
            return pertinencePonderation;
        }

        @Override
        public ListReduction getListReduction(short type) {
            return listReductionMap.get(type);
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

    }

}
