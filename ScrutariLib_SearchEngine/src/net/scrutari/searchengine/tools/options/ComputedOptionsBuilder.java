/* ScrutariLib_SearchEngine - Copyright (c) 2015-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.tools.EligibilityBuffer;


/**
 *
 * @author Vincent Calame
 */
public class ComputedOptionsBuilder {

    private final static CorpusData[] EMPTY_CORPUSARRAY = new CorpusData[0];
    private final static ThesaurusData[] EMPTY_THESAURUSARRAY = new ThesaurusData[0];
    private final SearchOptions searchOptions;
    private boolean mandatoryOperand = false;
    private boolean allLang = false;
    private Lang[] langArray = null;
    private boolean onFiche = false;
    private CorpusData[] corpusCodeArray = EMPTY_CORPUSARRAY;
    private boolean onMotcle = false;
    private ThesaurusData[] thesaurusCodeArray = EMPTY_THESAURUSARRAY;
    private Set<Integer> ficheCodeSet = null;
    private EligibilityBuffer eligibilityBuffer = null;
    private int ficheMaximum = 0;

    public ComputedOptionsBuilder(SearchOptions searchOptions) {
        this.searchOptions = searchOptions;
    }

    public ComputedOptionsBuilder setMandatoryOperand(boolean mandatoryOperand) {
        this.mandatoryOperand = mandatoryOperand;
        return this;
    }

    public ComputedOptionsBuilder setEligibilityBuffer(EligibilityBuffer eligibilityBuffer) {
        this.eligibilityBuffer = eligibilityBuffer;
        return this;
    }

    public ComputedOptionsBuilder setCorpusDataArray(CorpusData[] corpusDataArray) {
        if ((corpusDataArray == null) || (corpusDataArray.length == 0)) {
            this.onFiche = false;
            this.corpusCodeArray = EMPTY_CORPUSARRAY;
        } else {
            this.onFiche = true;
            this.corpusCodeArray = corpusDataArray;
        }
        return this;
    }

    public ComputedOptionsBuilder setThesaurusDataArray(ThesaurusData[] thesaurusDataArray) {
        if ((thesaurusDataArray == null) || (thesaurusDataArray.length == 0)) {
            this.onMotcle = false;
            this.thesaurusCodeArray = EMPTY_THESAURUSARRAY;
        } else {
            this.onMotcle = true;
            this.thesaurusCodeArray = thesaurusDataArray;
        }
        return this;
    }

    public ComputedOptionsBuilder setLangArray(boolean allLang, Lang[] langArray) {
        this.allLang = allLang;
        this.langArray = langArray;
        return this;
    }

    public void addFicheCode(Integer ficheCode) {
        if (ficheCodeSet == null) {
            ficheCodeSet = new HashSet<Integer>();
        }
        ficheCodeSet.add(ficheCode);
    }

    public ComputedOptionsBuilder setFicheMaximum(int ficheMaximum) {
        this.ficheMaximum = ficheMaximum;
        return this;
    }

    public ComputedOptions toComputedOptions() {
        return new InternalComputedOptions(searchOptions, mandatoryOperand, allLang, langArray, onFiche, corpusCodeArray, onMotcle, thesaurusCodeArray, ficheCodeSet, eligibilityBuffer, ficheMaximum);
    }

    public static ComputedOptionsBuilder init(SearchOptions searchOptions) {
        return new ComputedOptionsBuilder(searchOptions);
    }


    private static class InternalComputedOptions implements ComputedOptions {

        private final SearchOptions searchOptions;
        private final boolean mandatoryOperand;
        private final boolean allLang;
        private final Lang[] langArray;
        private final boolean onFiche;
        private final CorpusData[] corpusDataArray;
        private final boolean onMotcle;
        private final ThesaurusData[] thesaurusDataArray;
        private final Set<Integer> ficheCodeSet;
        private final EligibilityBuffer eligibilityBuffer;
        private final int ficheMaximum;

        private InternalComputedOptions(SearchOptions searchOptions, boolean mandatoryOperand, boolean allLang, Lang[] langArray, boolean onFiche, CorpusData[] corpusDataArray, boolean onMotcle, ThesaurusData[] thesaurusDataArray, Set<Integer> ficheCodeSet, EligibilityBuffer eligibilityBuffer, int ficheMaximum) {
            this.mandatoryOperand = mandatoryOperand;
            this.searchOptions = searchOptions;
            this.allLang = allLang;
            this.langArray = langArray;
            this.onFiche = onFiche;
            this.corpusDataArray = corpusDataArray;
            this.onMotcle = onMotcle;
            this.thesaurusDataArray = thesaurusDataArray;
            this.ficheCodeSet = ficheCodeSet;
            this.eligibilityBuffer = eligibilityBuffer;
            this.ficheMaximum = ficheMaximum;
        }

        @Override
        public SearchOptions getSearchOptions() {
            return searchOptions;
        }

        @Override
        public PertinencePonderation getPertinencePonderation() {
            if (searchOptions == null) {
                return null;
            } else {
                return searchOptions.getSearchOptionsDef().getPertinencePonderation();
            }
        }

        @Override
        public boolean hasMandatoryOperand() {
            return mandatoryOperand;
        }

        @Override
        public Lang[] getLangArray() {
            return langArray;
        }

        @Override
        public boolean isAllLang() {
            return allLang;
        }

        @Override
        public boolean isOnFiche() {
            return onFiche;
        }

        @Override
        public CorpusData[] getCorpusDataArray() {
            return corpusDataArray;
        }

        @Override
        public boolean isOnMotcle() {
            return onMotcle;
        }

        @Override
        public ThesaurusData[] getThesaurusDataArray() {
            return thesaurusDataArray;
        }

        @Override
        public boolean acceptFiche(Integer ficheCode) {
            if (ficheCodeSet != null) {
                if (!ficheCodeSet.contains(ficheCode)) {
                    return false;
                }
            }
            if (eligibilityBuffer == null) {
                return true;
            }
            return eligibilityBuffer.acceptFiche(ficheCode);
        }

        @Override
        public boolean acceptMotcle(Integer motcleCode) {
            if (eligibilityBuffer == null) {
                return true;
            }
            return eligibilityBuffer.acceptMotcle(motcleCode);
        }

        @Override
        public int getFicheMaximum() {
            return ficheMaximum;
        }

    }

}
