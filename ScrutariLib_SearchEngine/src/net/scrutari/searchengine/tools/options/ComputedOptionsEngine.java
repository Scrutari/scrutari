/* ScrutariLib_SearchEngine - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.tools.util.DataUtils;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.tools.EligibilityBuffer;
import net.scrutari.searchengine.tools.operands.search.SearchOperationUtils;


/**
 *
 * @author Vincent Calame
 */
public final class ComputedOptionsEngine {

    private ComputedOptionsEngine() {
    }

    public static ComputedOptions compute(ScrutariDB scrutariDB, DataAccess dataAccess, SearchOperation searchOperation, SearchOptions searchOptions, boolean withFiches, boolean withMotscles) {
        boolean mandatoryOperand = SearchOperationUtils.hasMandatoryOperand(searchOperation);
        EligibilityBuffer eligibilityBuffer = null;
        Lang[] langArray = null;
        boolean allLang = true;
        if (searchOptions != null) {
            SearchOptionsDef searchOptionsDef = searchOptions.getSearchOptionsDef();
            Langs langs = searchOptionsDef.getFilterLangs();
            if (!langs.isEmpty()) {
                langArray = langs.toArray(new Lang[langs.size()]);
            }
            eligibilityBuffer = new EligibilityBuffer(dataAccess, searchOptions.getFicheEligibility());
            if ((langArray != null) && (!mandatoryOperand)) {
                eligibilityBuffer.addLangCheck(langArray);
            }
        }
        if (langArray == null) {
            langArray = scrutariDB.getStats().getFicheEngineLangStats().getLangArray();
        } else {
            allLang = false;
        }
        ComputedOptionsBuilder builder = ComputedOptionsBuilder.init(searchOptions)
                .setMandatoryOperand(mandatoryOperand)
                .setEligibilityBuffer(eligibilityBuffer)
                .setLangArray(allLang, langArray);
        if (withFiches) {
            CorpusData[] corpusDataArray;
            if ((searchOptions != null) && (searchOptions.isWithCorpusCodes())) {
                corpusDataArray = OptionsUtils.toCorpusDataArray(searchOptions.getCorpusCodeList(), dataAccess);
            } else {
                corpusDataArray = dataAccess.getCorpusDataArray();
            }
            builder
                    .setCorpusDataArray(corpusDataArray)
                    .setFicheMaximum(DataUtils.getFicheCountByCorpus(dataAccess, corpusDataArray, langArray, scrutariDB.getStats()));
        }
        if (withMotscles) {
            ThesaurusData[] thesaurusDataArray;
            if ((searchOptions != null) && (searchOptions.isWithThesaurusCodes())) {
                thesaurusDataArray = OptionsUtils.toThesaurusDataArray(searchOptions.getThesaurusCodeList(), dataAccess);
            } else {
                thesaurusDataArray = dataAccess.getThesaurusDataArray();
            }
            builder.setThesaurusDataArray(thesaurusDataArray);
        }
        return builder.toComputedOptions();
    }

}
