/* ScrutariLib_SearchEngine - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.options;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.primitives.PrimUtils;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.URITreeProvider;
import net.scrutari.searchengine.api.operands.eligibility.Eligibility;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchResultHolder;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityCheck;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityCheckFactory;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityCheckParameters;


/**
 *
 *
 * @author Vincent Calame
 */
public final class SearchOptionsBuilder {

    public final static SearchOptions EMPTY_SEARCHOPTIONS = new InternalSearchOptions(SearchOptionsDefBuilder.EMPTY_SEARCHOPTIONSDEF, null, false, PrimUtils.EMPTY_LIST, false, PrimUtils.EMPTY_LIST);
    private final SearchOptionsDef searchOptionsDef;
    private Eligibility ficheEligibility;
    private Set<Integer> corpusSet = null;
    private Set<Integer> thesaurusSet = null;

    public SearchOptionsBuilder(SearchOptionsDef searchOptionsDef) {
        this.searchOptionsDef = searchOptionsDef;
    }

    public SearchOptionsDef getSearchOptionsDef() {
        return searchOptionsDef;
    }

    public SearchOptionsBuilder reduceCorpusCodes(Collection<Integer> corpusCodes) {
        if (corpusCodes == null) {
            return this;
        }
        if ((corpusSet != null) && (corpusSet.isEmpty())) {
            return this;
        }
        if (corpusCodes.isEmpty()) {
            corpusSet = Collections.emptySet();
            return this;
        }
        if (corpusSet == null) {
            corpusSet = new LinkedHashSet<Integer>();
            corpusSet.addAll(corpusCodes);
            return this;
        }
        Set<Integer> newCorpusSet = new HashSet<Integer>();
        for (Integer code : corpusCodes) {
            if (corpusSet.contains(code)) {
                newCorpusSet.add(code);
            }
        }
        corpusSet = newCorpusSet;
        return this;
    }

    public SearchOptionsBuilder reduceThesaurusCodes(Collection<Integer> thesaurusCodes) {
        if (thesaurusCodes == null) {
            return this;
        }
        if ((thesaurusSet != null) && (thesaurusSet.isEmpty())) {
            return this;
        }
        if (thesaurusCodes.isEmpty()) {
            thesaurusSet = Collections.emptySet();
            return this;
        }
        if (thesaurusSet == null) {
            thesaurusSet = new LinkedHashSet<Integer>();
            thesaurusSet.addAll(thesaurusCodes);
            return this;
        }
        Set<Integer> newThesaurusSet = new HashSet<Integer>();
        for (Integer code : thesaurusCodes) {
            if (thesaurusSet.contains(code)) {
                newThesaurusSet.add(code);
            }
        }
        thesaurusSet = newThesaurusSet;
        return this;
    }

    public Integer[] getCurrentCorpusCodeArray() {
        if (corpusSet != null) {
            return corpusSet.toArray(new Integer[corpusSet.size()]);
        } else {
            return null;
        }
    }

    public SearchOptions toSearchOptions() {
        boolean withCorpusCodes = false;
        List<Integer> corpusCodeList;
        if (corpusSet != null) {
            withCorpusCodes = true;
            corpusCodeList = new ArrayList<Integer>(corpusSet);
        } else {
            corpusCodeList = PrimUtils.EMPTY_LIST;
        }
        boolean withThesaurusCodes = false;
        List<Integer> thesaurusCodeList;
        if (thesaurusSet != null) {
            withThesaurusCodes = true;
            thesaurusCodeList = new ArrayList<Integer>(thesaurusSet);
        } else {
            thesaurusCodeList = PrimUtils.EMPTY_LIST;
        }
        return new InternalSearchOptions(searchOptionsDef, ficheEligibility, withCorpusCodes, corpusCodeList, withThesaurusCodes, thesaurusCodeList);
    }

    public void checkEligibility(DataAccess dataAccess, FicheSearchResultHolder ficheSearchResultHolder, MessageHandler messageHandler, URITreeProvider uriTreeProvider) {
        EligibilityCheckParameters eligibilityCheckParameters = new EligibilityCheckParameters()
                .setDataAccess(dataAccess)
                .setFicheSearchResultHolder(ficheSearchResultHolder)
                .setMessageHandler(messageHandler)
                .setUriTreeProvider(uriTreeProvider);
        EligibilityOperand ficheEligibilityOperand = searchOptionsDef.getFicheEligibilityOperand();
        if (ficheEligibilityOperand == null) {
            return;
        }
        EligibilityCheck eligibilityCheck = EligibilityCheckFactory.check(ficheEligibilityOperand, getCurrentCorpusCodeArray(), eligibilityCheckParameters);
        if (eligibilityCheck == null) {
            return;
        }
        Set<Integer> codeSet = eligibilityCheck.getCodeSet();
        if (codeSet != null) {
            if (corpusSet == null) {
                corpusSet = new HashSet<Integer>(codeSet);
            } else {
                corpusSet.retainAll(codeSet);
            }
        }
        this.ficheEligibility = eligibilityCheck.getEligibility();
    }

    public static SearchOptionsBuilder init(SearchOptionsDef searchOptionsDef) {
        return new SearchOptionsBuilder(searchOptionsDef);
    }


    private static class InternalSearchOptions implements SearchOptions {

        private final SearchOptionsDef searchOptionsDef;
        private final Eligibility ficheEligibility;
        private final boolean withCorpusCodes;
        private final List<Integer> corpusCodeList;
        private final boolean withThesaurusCodes;
        private final List<Integer> thesaurusCodeList;


        private InternalSearchOptions(SearchOptionsDef searchOptionsDef, Eligibility ficheEligibility, boolean withCorpusCodes, List<Integer> corpusCodeList, boolean withThesaurusCodes, List<Integer> thesaurusCodeList) {
            this.searchOptionsDef = searchOptionsDef;
            this.ficheEligibility = ficheEligibility;
            this.withCorpusCodes = withCorpusCodes;
            this.corpusCodeList = corpusCodeList;
            this.withThesaurusCodes = withThesaurusCodes;
            this.thesaurusCodeList = thesaurusCodeList;
        }

        @Override
        public SearchOptionsDef getSearchOptionsDef() {
            return searchOptionsDef;
        }

        @Override
        public Eligibility getFicheEligibility() {
            return ficheEligibility;
        }

        @Override
        public boolean isWithCorpusCodes() {
            return withCorpusCodes;
        }

        @Override
        public List<Integer> getCorpusCodeList() {
            return corpusCodeList;
        }

        @Override
        public boolean isWithThesaurusCodes() {
            return withThesaurusCodes;
        }

        @Override
        public List<Integer> getThesaurusCodeList() {
            return thesaurusCodeList;
        }

    }

}
