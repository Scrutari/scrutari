/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchSource;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchSourceBuilder {

    private QId qId;
    private CanonicalQ canonicalQ;
    private SearchOptionsDef searchOptionsDef = SearchOptionsDefBuilder.EMPTY_SEARCHOPTIONSDEF;

    public FicheSearchSourceBuilder() {
    }

    public FicheSearchSourceBuilder setQId(QId qId) {
        this.qId = qId;
        return this;
    }

    public FicheSearchSourceBuilder setCanonicalQ(CanonicalQ canonicalQ) {
        this.canonicalQ = canonicalQ;
        return this;
    }

    public FicheSearchSourceBuilder setSearchOptionsDef(SearchOptionsDef searchOptionsDef) {
        if (searchOptionsDef == null) {
            this.searchOptionsDef = SearchOptionsDefBuilder.EMPTY_SEARCHOPTIONSDEF;
        } else {
            this.searchOptionsDef = searchOptionsDef;
        }
        return this;
    }

    public boolean isValid() {
        return (canonicalQ != null);
    }

    public FicheSearchSource toFicheSearchSource() {
        if (canonicalQ == null) {
            throw new IllegalStateException("qInfo is not defined");
        }
        return new InternalFicheSearchSource(qId, canonicalQ, searchOptionsDef);
    }

    public static FicheSearchSource build(QId qId, CanonicalQ canonicalQ, SearchOptionsDef searchOptionsDef) {
        if (canonicalQ == null) {
            throw new IllegalArgumentException("qInfo is null");
        }
        return new InternalFicheSearchSource(qId, canonicalQ, searchOptionsDef);
    }

    public static FicheSearchSourceBuilder init() {
        return new FicheSearchSourceBuilder();
    }


    private static class InternalFicheSearchSource implements FicheSearchSource {

        private final QId qId;
        private final CanonicalQ canonicalQ;
        private final SearchOptionsDef searchOptionsDef;

        private InternalFicheSearchSource(QId qId, CanonicalQ canonicalQ, SearchOptionsDef searchOptionsDef) {
            this.qId = qId;
            this.canonicalQ = canonicalQ;
            this.searchOptionsDef = searchOptionsDef;
        }

        @Override
        public QId getQId() {
            return qId;
        }

        @Override
        public CanonicalQ getCanonicalQ() {
            return canonicalQ;
        }

        @Override
        public SearchOptionsDef getSearchOptionsDef() {
            return searchOptionsDef;
        }

    }

}
