/* ScrutariLib_SearchEngine - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;


/**
 *
 * @author Vincent Calame
 */
public class OccurrencesBuilder {

    private final SubstringPosition[] positionArray;
    private final int[] operandNumberArray;
    private int size = 0;

    public OccurrencesBuilder(int operandMaxNumber) {
        positionArray = new SubstringPosition[operandMaxNumber];
        operandNumberArray = new int[operandMaxNumber];
    }

    public void add(int operandNumber, SubstringPosition substringPosition) {
        boolean insert = false;
        for (int i = 0; i < size; i++) {
            if (substringPosition.compareTo(positionArray[i]) < 0) {
                System.arraycopy(positionArray, i, positionArray, i + 1, size - i);
                System.arraycopy(operandNumberArray, i, operandNumberArray, i + 1, size - i);
                operandNumberArray[i] = operandNumber;
                positionArray[i] = substringPosition;
                insert = true;
                break;
            }
        }
        if (!insert) {
            operandNumberArray[size] = operandNumber;
            positionArray[size] = substringPosition;
        }
        size++;
    }

    public List<SearchTokenOccurrence> toSearchTokenOccurrenceList() {
        return ResultUtils.wrap(toSearchTokenOccurrenceArray());
    }

    public InAlineaOccurrences toInAlineaOccurrences(AlineaRank alineaRank) {
        return ResultUtils.toInAlineaOccurrences(alineaRank, toSearchTokenOccurrenceArray());
    }

    public InLangOccurrences toInLangOccurrences(Lang lang) {
        return ResultUtils.toInLangOccurrences(lang, toSearchTokenOccurrenceArray());
    }

    private SearchTokenOccurrence[] toSearchTokenOccurrenceArray() {
        SearchTokenOccurrence[] searchTokenPositionArray = new SearchTokenOccurrence[size];
        for (int i = 0; i < size; i++) {
            searchTokenPositionArray[i] = SearchTokenOccurrenceBuilder.toSearchTokenOccurrence(operandNumberArray[i], positionArray[i].getBeginIndex(), positionArray[i].getLength());
        }
        return searchTokenPositionArray;
    }

}
