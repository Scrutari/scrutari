/* ScrutariLib_SearchEngine - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchSource;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.searchengine.api.result.SearchTokenReport;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchResultBuilder {

    private final List<MotcleSearchResultInfo> motcleList = new ArrayList<MotcleSearchResultInfo>();
    private final List<FicheSearchResultGroup> groupList = new ArrayList<FicheSearchResultGroup>();
    private final SortedMap<Integer, SearchTokenReport> searchTokenReportMap = new TreeMap<Integer, SearchTokenReport>();
    private final FicheSearchSource ficheSearchSource;
    private int ficheCount;
    private int ficheMaximum;

    public FicheSearchResultBuilder(FicheSearchSource ficheSearchSource) {
        this.ficheSearchSource = ficheSearchSource;
    }

    public FicheSearchResultBuilder setFicheMaximum(int ficheMaximum) {
        this.ficheMaximum = ficheMaximum;
        return this;
    }

    public FicheSearchResultBuilder setFicheCount(int ficheCount) {
        this.ficheCount = ficheCount;
        return this;
    }

    public FicheSearchResultBuilder addFicheSearchResultGroup(FicheSearchResultGroup ficheSearchResultGroup) {
        groupList.add(ficheSearchResultGroup);
        return this;
    }

    public FicheSearchResultBuilder addMotcleSearchResultInfo(MotcleSearchResultInfo motcleSearchResultInfo) {
        motcleList.add(motcleSearchResultInfo);
        return this;
    }

    public FicheSearchResultBuilder addSearchTokenReport(SearchTokenReport searchTokenReport) {
        searchTokenReportMap.put(searchTokenReport.getOperandNumber(), searchTokenReport);
        return this;
    }

    public FicheSearchResult toFicheSearchResult() {
        List<FicheSearchResultGroup> ficheSearchResultGroupList = ResultUtils.wrap(groupList.toArray(new FicheSearchResultGroup[groupList.size()]));
        List<MotcleSearchResultInfo> motcleSearchResultInfoList = ResultUtils.wrap(motcleList.toArray(new MotcleSearchResultInfo[motcleList.size()]));
        List<SearchTokenReport> searchTokenReportList = ResultUtils.wrap(searchTokenReportMap.values().toArray(new SearchTokenReport[searchTokenReportMap.size()]));
        return new InternalFicheSearchResult(ficheSearchSource, ficheCount, ficheMaximum, ficheSearchResultGroupList, motcleSearchResultInfoList, searchTokenReportList);
    }

    public static FicheSearchResultBuilder init(FicheSearchSource ficheSearchSource) {
        return new FicheSearchResultBuilder(ficheSearchSource);
    }


    private static class InternalFicheSearchResult implements FicheSearchResult {

        private final FicheSearchSource ficheSearchSource;
        private final int ficheCount;
        private final int ficheMaximum;
        private final List<FicheSearchResultGroup> ficheSearchResultGroupList;
        private final List<MotcleSearchResultInfo> motcleSearchResultInfoList;
        private final List<SearchTokenReport> searchTokenReportList;

        private InternalFicheSearchResult(FicheSearchSource ficheSearchSource,
                int ficheCount, int ficheMaximum, List<FicheSearchResultGroup> ficheSearchResultGroupList, List<MotcleSearchResultInfo> motcleSearchResultInfoList, List<SearchTokenReport> searchTokenReportList) {
            this.ficheSearchSource = ficheSearchSource;
            this.ficheCount = ficheCount;
            this.ficheMaximum = ficheMaximum;
            this.ficheSearchResultGroupList = ficheSearchResultGroupList;
            this.motcleSearchResultInfoList = motcleSearchResultInfoList;
            this.searchTokenReportList = searchTokenReportList;
        }

        @Override
        public FicheSearchSource getFicheSearchSource() {
            return ficheSearchSource;
        }

        @Override
        public int getFicheCount() {
            return ficheCount;
        }

        @Override
        public int getFicheMaximum() {
            return ficheMaximum;
        }

        @Override
        public List<FicheSearchResultGroup> getFicheSearchResultGroupList() {
            return ficheSearchResultGroupList;
        }

        @Override
        public List<MotcleSearchResultInfo> getMotcleSearchResultInfoList() {
            return motcleSearchResultInfoList;
        }

        @Override
        public List<SearchTokenReport> getSearchTokenReportList() {
            return searchTokenReportList;
        }

    }

}
