/* ScrutariLib_SearchEngine - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import net.scrutari.searchengine.api.result.SearchTokenOccurrence;


/**
 *
 * @author Vincent Calame
 */
public class SearchTokenOccurrenceBuilder {

    private InternalSearchTokenOccurrence searchTokenOccurrence;

    public SearchTokenOccurrenceBuilder() {
    }

    public void setOperandNumber(int operandNumber) {
        searchTokenOccurrence.operandNumber = operandNumber;
    }

    public void setBeginIndex(int beginIndex) {
        searchTokenOccurrence.beginIndex = beginIndex;
    }

    public void setLength(int length) {
        searchTokenOccurrence.length = length;
    }

    public SearchTokenOccurrence toSearchTokenOccurrence() {
        return searchTokenOccurrence;
    }

    public static SearchTokenOccurrence toSearchTokenOccurrence(int operandNumber, int beginIndex, int length) {
        return new InternalSearchTokenOccurrence(operandNumber, beginIndex, length);
    }


    private static class InternalSearchTokenOccurrence implements SearchTokenOccurrence {

        private int operandNumber;
        private int beginIndex;
        private int length;

        private InternalSearchTokenOccurrence(int operandNumber, int beginIndex, int length) {
            this.operandNumber = operandNumber;
            this.beginIndex = beginIndex;
            this.length = length;
        }

        public int getOperandNumber() {
            return operandNumber;
        }

        public int getBeginIndex() {
            return beginIndex;
        }

        public int getLength() {
            return length;
        }

    }

}
