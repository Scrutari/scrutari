/* ScrutariLib_SearchEngine - Copyright (c) 2018-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;
import net.scrutari.searchengine.api.result.SearchTokenReport;


/**
 *
 * @author Vincent Calame
 */
public class SearchTokenReportBuilder {

    private final int operandNumber;
    private final String operandString;
    private final SearchTokenOperand operand;
    private final SortedMap<String, CanonicalReportBuilder> canonicalReportBuilderMap = new TreeMap<String, CanonicalReportBuilder>();
    private final SortedMap<String, NeighbourReportBuilder> neighbourReportBuilderMap = new TreeMap<String, NeighbourReportBuilder>();
    private final SortedSet<Lang> testedLangSet = new TreeSet<Lang>();

    public SearchTokenReportBuilder(SearchTokenOperand operand) {
        this.operand = operand;
        this.operandNumber = operand.getOperandNumber();
        this.operandString = operand.toString();
    }

    public SearchTokenReportBuilder(int operandNumber, String operandString) {
        this.operand = null;
        this.operandNumber = operandNumber;
        this.operandString = operandString;
    }

    public int getOperandNumber() {
        return operandNumber;
    }

    public void addTestedLang(Lang lang) {
        testedLangSet.add(lang);
    }

    public boolean hasFailed() {
        return ((operand != null) && (!testedLangSet.isEmpty()) && (canonicalReportBuilderMap.isEmpty()));
    }

    public SortedSet<Lang> getTestedLangSet() {
        return testedLangSet;
    }

    public SearchTokenOperand getSearchTokenOperand() {
        return operand;
    }

    public void addCanonical(String canonical, Lang lang, int ficheCount, int motcleCount) {
        CanonicalReportBuilder canonicalReportBuilder = canonicalReportBuilderMap.get(canonical);
        if (canonicalReportBuilder == null) {
            canonicalReportBuilder = new CanonicalReportBuilder(canonical);
            canonicalReportBuilderMap.put(canonical, canonicalReportBuilder);
        }
        canonicalReportBuilder.addLang(lang, ficheCount, motcleCount);
    }

    public void addNeighbour(String neighbour, Lang lang) {
        NeighbourReportBuilder neighbourReportBuilder = neighbourReportBuilderMap.get(neighbour);
        if (neighbourReportBuilder == null) {
            neighbourReportBuilder = new NeighbourReportBuilder(neighbour);
            neighbourReportBuilderMap.put(neighbour, neighbourReportBuilder);
        }
        neighbourReportBuilder.addLang(lang);
    }


    public SearchTokenReport toSearchTokenReport() {
        int size = canonicalReportBuilderMap.size();
        SearchTokenReport.CanonicalReport[] array = new SearchTokenReport.CanonicalReport[size];
        int p = 0;
        for (CanonicalReportBuilder builder : canonicalReportBuilderMap.values()) {
            array[p] = builder.toCanonicalReport();
            p++;
        }
        List<SearchTokenReport.CanonicalReport> canonicalReportList = ResultUtils.wrap(array);
        List<SearchTokenReport.NeighbourReport> neighbourReportList = ResultUtils.EMPTY_NEIGHBOURREPORTLIST;
        if (!neighbourReportBuilderMap.isEmpty()) {
            int neighbourSize = neighbourReportBuilderMap.size();
            SearchTokenReport.NeighbourReport[] neighbourArray = new SearchTokenReport.NeighbourReport[neighbourSize];
            int q = 0;
            for (NeighbourReportBuilder neighbourReportBuilder : neighbourReportBuilderMap.values()) {
                neighbourArray[q] = neighbourReportBuilder.toNeighbourReport();
                q++;
            }
            neighbourReportList = ResultUtils.wrap(neighbourArray);
        }
        return new InternalSearchTokenReport(operandNumber, operandString, canonicalReportList, neighbourReportList);
    }


    private class CanonicalReportBuilder {

        private final String canonical;
        private final List<SearchTokenReport.LangReport> langReportList = new ArrayList<SearchTokenReport.LangReport>();

        private CanonicalReportBuilder(String canonical) {
            this.canonical = canonical;
        }

        private void addLang(Lang lang, int ficheCount, int motcleCount) {
            langReportList.add(new InternalLangReport(lang, ficheCount, motcleCount));
        }

        private SearchTokenReport.CanonicalReport toCanonicalReport() {
            return new InternalCanonicalReport(canonical, ResultUtils.wrap(langReportList.toArray(new SearchTokenReport.LangReport[langReportList.size()])));
        }

    }


    private class NeighbourReportBuilder {

        private final String neighbour;
        private final List<Lang> langList = new ArrayList<Lang>();

        private NeighbourReportBuilder(String neighbour) {
            this.neighbour = neighbour;
        }

        private void addLang(Lang lang) {
            langList.add(lang);
        }

        private SearchTokenReport.NeighbourReport toNeighbourReport() {
            return new InternalNeighbourReport(neighbour, LangsUtils.wrap(langList));
        }

    }


    private static class InternalSearchTokenReport implements SearchTokenReport {

        private final int operandNumber;
        private final String operandString;
        private final List<CanonicalReport> canonicalReportList;
        private final List<SearchTokenReport.NeighbourReport> neighbourReportList;

        private InternalSearchTokenReport(int operandNumber, String operandString, List<CanonicalReport> canonicalReportList, List<SearchTokenReport.NeighbourReport> neighbourReportList) {
            this.operandNumber = operandNumber;
            this.operandString = operandString;
            this.canonicalReportList = canonicalReportList;
            this.neighbourReportList = neighbourReportList;
        }

        @Override
        public int getOperandNumber() {
            return operandNumber;
        }

        @Override
        public String getOperandString() {
            return operandString;
        }

        @Override
        public List<CanonicalReport> getCanonicalReportList() {
            return canonicalReportList;
        }

        @Override
        public List<NeighbourReport> getNeighbourReportList() {
            return neighbourReportList;
        }

    }


    private static class InternalCanonicalReport implements SearchTokenReport.CanonicalReport {

        private final String canonical;
        private final List<SearchTokenReport.LangReport> langReportList;

        private InternalCanonicalReport(String canonical, List<SearchTokenReport.LangReport> langReportList) {
            this.canonical = canonical;
            this.langReportList = langReportList;
        }

        @Override
        public String getCanonical() {
            return canonical;
        }

        @Override
        public List<SearchTokenReport.LangReport> getLangReportList() {
            return langReportList;
        }

    }


    private static class InternalNeighbourReport implements SearchTokenReport.NeighbourReport {

        private final String canonical;
        private final List<Lang> langList;

        private InternalNeighbourReport(String canonical, List<Lang> langList) {
            this.canonical = canonical;
            this.langList = langList;
        }

        @Override
        public String getNeighbour() {
            return canonical;
        }

        @Override
        public List<Lang> getLangList() {
            return langList;
        }

    }


    private static class InternalLangReport implements SearchTokenReport.LangReport {

        private final Lang lang;
        private final int ficheCount;
        private final int motcleCount;

        private InternalLangReport(Lang lang, int ficheCount, int motcleCount) {
            this.lang = lang;
            this.ficheCount = ficheCount;
            this.motcleCount = motcleCount;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public int getFicheCount() {
            return ficheCount;
        }

        @Override
        public int getMotcleCount() {
            return motcleCount;
        }

    }

}
