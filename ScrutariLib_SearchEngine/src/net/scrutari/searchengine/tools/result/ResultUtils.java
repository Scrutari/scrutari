/* ScrutariLib_SearchEngine - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;
import net.scrutari.searchengine.api.result.SearchTokenReport;
import net.scrutari.searchengine.api.result.SupermotcleSearchResult;
import net.scrutari.searchengine.api.result.SupermotcleSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public final class ResultUtils {

    public final static List<SearchTokenOccurrence> EMPTY_SEARCHTOKENOCCURRENCELIST = Collections.emptyList();
    public final static List<InAlineaOccurrences> EMPTY_INALINEAOCCURRENCESLIST = Collections.emptyList();
    public final static List<InLangOccurrences> EMPTY_INLANGOCCURRENCESLIST = Collections.emptyList();
    public final static List<SupermotcleSearchResultInfo> EMPTY_SUPERMOTCLESEARCHRESULTINFOLIST = Collections.emptyList();
    public final static List<SearchTokenReport.NeighbourReport> EMPTY_NEIGHBOURREPORTLIST = Collections.emptyList();
    public final static SupermotcleSearchResult EMPTY_SUPERMOTCLE_SEARCHRESULT = new EmptySupermotcleSearchResult();

    private ResultUtils() {
    }

    public static InAlineaOccurrences toInAlineaOccurrences(AlineaRank alineaRank, SearchTokenOccurrence[] array) {
        return new InternalInAlineaOccurrences(alineaRank, array);
    }

    public static InLangOccurrences toInLangOccurrences(Lang lang, SearchTokenOccurrence[] array) {
        return new InternalInLangOccurrences(lang, array);
    }

    public static MotcleSearchResultInfo toMotcleSearchResultInfo(Integer code, int score, InLangOccurrences[] inLangOccurrenceListArray) {
        List<InLangOccurrences> occurrencesList;
        if ((inLangOccurrenceListArray == null) || (inLangOccurrenceListArray.length == 0)) {
            occurrencesList = EMPTY_INLANGOCCURRENCESLIST;
        } else {
            occurrencesList = wrap(inLangOccurrenceListArray);
        }
        return new InternalMotcleSearchResultInfo(code, score, occurrencesList);
    }

    public static List<SearchTokenOccurrence> wrap(SearchTokenOccurrence[] array) {
        return new SearchTokenOccurrenceList(array);
    }

    public static int compare(List<SearchTokenOccurrence> list1, List<SearchTokenOccurrence> list2) {
        int size1 = list1.size();
        int size2 = list2.size();
        if (size1 > size2) {
            return 1;
        }
        if (size1 < size2) {
            return -1;
        }
        int diff = 0;
        for (int i = 0; i < size1; i++) {
            SearchTokenOccurrence tokenOccurrence1 = list1.get(i);
            SearchTokenOccurrence tokenOccurrence2 = list2.get(i);
            int tk1 = tokenOccurrence1.getOperandNumber();
            int tk2 = tokenOccurrence2.getOperandNumber();
            if (tk1 < tk2) {
                return 1;
            }
            if (tk1 > tk1) {
                return -1;
            }
            int start1 = tokenOccurrence1.getBeginIndex();
            int start2 = tokenOccurrence2.getBeginIndex();
            if (start1 < start2) {
                return 1;
            }
            if (start1 > start2) {
                return -1;
            }
            if (diff == 0) {
                int ln1 = tokenOccurrence1.getLength();
                int ln2 = tokenOccurrence2.getLength();
                if (ln1 < ln2) {
                    diff = 1;
                }
                if (ln1 > ln2) {
                    diff = -1;
                }
            }
        }
        if (diff != 0) {
            return diff;
        }
        return 0;
    }

    public static List<InAlineaOccurrences> wrap(InAlineaOccurrences[] array) {
        return new InAlineaOccurrencesList(array);
    }

    public static List<InLangOccurrences> wrap(InLangOccurrences[] array) {
        return new InLangOccurrencesList(array);
    }

    public static List<SupermotcleSearchResultInfo> wrap(SupermotcleSearchResultInfo[] array) {
        return new SupermotcleSearchResultInfoList(array);
    }

    public static List<MotcleSearchResultInfo> wrap(MotcleSearchResultInfo[] array) {
        return new MotcleSearchResultInfoList(array);
    }

    public static List<FicheSearchResultGroup> wrap(FicheSearchResultGroup[] array) {
        return new FicheSearchResultGroupList(array);
    }

    public static List<FicheSearchResultInfo> wrap(FicheSearchResultInfo[] array) {
        return new FicheSearchResultInfoList(array);
    }

    public static List<SearchTokenReport> wrap(SearchTokenReport[] array) {
        return new SearchTokenReportList(array);
    }

    public static List<SearchTokenReport.LangReport> wrap(SearchTokenReport.LangReport[] array) {
        return new LangReportList(array);
    }

    public static List<SearchTokenReport.CanonicalReport> wrap(SearchTokenReport.CanonicalReport[] array) {
        return new CanonicalReportList(array);
    }

    public static List<SearchTokenReport.NeighbourReport> wrap(SearchTokenReport.NeighbourReport[] array) {
        return new NeighbourReportList(array);
    }


    private static class EmptySupermotcleSearchResult implements SupermotcleSearchResult {

        private EmptySupermotcleSearchResult() {

        }

        @Override
        public List<SupermotcleSearchResultInfo> getSupermotcleSearchResultInfoList() {
            return EMPTY_SUPERMOTCLESEARCHRESULTINFOLIST;
        }

    }


    private static class InternalInAlineaOccurrences implements InAlineaOccurrences {

        private final AlineaRank alineaRank;
        private final List<SearchTokenOccurrence> list;

        private InternalInAlineaOccurrences(AlineaRank alineaRank, SearchTokenOccurrence[] array) {
            this.alineaRank = alineaRank;
            this.list = wrap(array);
        }

        @Override
        public AlineaRank getAlineaRank() {
            return alineaRank;
        }

        @Override
        public List<SearchTokenOccurrence> getSearchTokenOccurrenceList() {
            return list;
        }

    }


    private static class InternalInLangOccurrences implements InLangOccurrences {

        private final Lang lang;
        private final List<SearchTokenOccurrence> list;

        private InternalInLangOccurrences(Lang lang, SearchTokenOccurrence[] array) {
            this.lang = lang;
            this.list = wrap(array);
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public List<SearchTokenOccurrence> getSearchTokenOccurrenceList() {
            return list;
        }

    }


    private static class InternalMotcleSearchResultInfo implements MotcleSearchResultInfo {

        private final Integer code;
        private final int score;
        private final List<InLangOccurrences> inLangOccurrencesList;

        private InternalMotcleSearchResultInfo(Integer code, int score, List<InLangOccurrences> inLangOccurrencesList) {
            this.code = code;
            this.score = score;
            this.inLangOccurrencesList = inLangOccurrencesList;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public int getScore() {
            return score;
        }

        @Override
        public List<InLangOccurrences> getInLangOccurrencesList() {
            return inLangOccurrencesList;
        }

    }


    private static class SearchTokenOccurrenceList extends AbstractList<SearchTokenOccurrence> implements RandomAccess {

        private final SearchTokenOccurrence[] array;

        private SearchTokenOccurrenceList(SearchTokenOccurrence[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SearchTokenOccurrence get(int index) {
            return array[index];
        }

    }


    private static class InAlineaOccurrencesList extends AbstractList<InAlineaOccurrences> implements RandomAccess {

        private final InAlineaOccurrences[] array;

        private InAlineaOccurrencesList(InAlineaOccurrences[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public InAlineaOccurrences get(int index) {
            return array[index];
        }

    }


    private static class InLangOccurrencesList extends AbstractList<InLangOccurrences> implements RandomAccess {

        private final InLangOccurrences[] array;

        private InLangOccurrencesList(InLangOccurrences[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public InLangOccurrences get(int index) {
            return array[index];
        }

    }


    private static class SupermotcleSearchResultInfoList extends AbstractList<SupermotcleSearchResultInfo> implements RandomAccess {

        private final SupermotcleSearchResultInfo[] array;

        private SupermotcleSearchResultInfoList(SupermotcleSearchResultInfo[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SupermotcleSearchResultInfo get(int index) {
            return array[index];
        }

    }


    private static class MotcleSearchResultInfoList extends AbstractList<MotcleSearchResultInfo> implements RandomAccess {

        private final MotcleSearchResultInfo[] array;

        private MotcleSearchResultInfoList(MotcleSearchResultInfo[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MotcleSearchResultInfo get(int index) {
            return array[index];
        }

    }


    private static class FicheSearchResultGroupList extends AbstractList<FicheSearchResultGroup> implements RandomAccess {

        private final FicheSearchResultGroup[] array;

        private FicheSearchResultGroupList(FicheSearchResultGroup[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public FicheSearchResultGroup get(int index) {
            return array[index];
        }

    }


    private static class FicheSearchResultInfoList extends AbstractList<FicheSearchResultInfo> implements RandomAccess {

        private final FicheSearchResultInfo[] array;

        private FicheSearchResultInfoList(FicheSearchResultInfo[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public FicheSearchResultInfo get(int index) {
            return array[index];
        }

    }


    private static class SearchTokenReportList extends AbstractList<SearchTokenReport> implements RandomAccess {

        private final SearchTokenReport[] array;

        private SearchTokenReportList(SearchTokenReport[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SearchTokenReport get(int index) {
            return array[index];
        }

    }


    private static class LangReportList extends AbstractList<SearchTokenReport.LangReport> implements RandomAccess {

        private final SearchTokenReport.LangReport[] array;

        private LangReportList(SearchTokenReport.LangReport[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SearchTokenReport.LangReport get(int index) {
            return array[index];
        }

    }


    private static class CanonicalReportList extends AbstractList<SearchTokenReport.CanonicalReport> implements RandomAccess {

        private final SearchTokenReport.CanonicalReport[] array;

        private CanonicalReportList(SearchTokenReport.CanonicalReport[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SearchTokenReport.CanonicalReport get(int index) {
            return array[index];
        }

    }


    private static class NeighbourReportList extends AbstractList<SearchTokenReport.NeighbourReport> implements RandomAccess {

        private final SearchTokenReport.NeighbourReport[] array;

        private NeighbourReportList(SearchTokenReport.NeighbourReport[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public SearchTokenReport.NeighbourReport get(int index) {
            return array[index];
        }

    }

}
