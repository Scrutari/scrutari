/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.List;
import net.scrutari.searchengine.api.result.SupermotcleSearchResult;
import net.scrutari.searchengine.api.result.SupermotcleSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleSearchResultBuilder {

    public SupermotcleSearchResultBuilder() {
    }

    public static SupermotcleSearchResult toSupermotcleSearchResult(SupermotcleSearchResultInfo[] array) {
        List<SupermotcleSearchResultInfo> infoList;
        if ((array == null) || (array.length == 0)) {
            infoList = ResultUtils.EMPTY_SUPERMOTCLESEARCHRESULTINFOLIST;
        } else {
            infoList = ResultUtils.wrap(array);
        }
        return new InternalSupermotcleSearchResult(infoList);
    }


    private static class InternalSupermotcleSearchResult implements SupermotcleSearchResult {

        private final List<SupermotcleSearchResultInfo> infoList;

        private InternalSupermotcleSearchResult(List<SupermotcleSearchResultInfo> infoList) {
            this.infoList = infoList;
        }

        @Override
        public List<SupermotcleSearchResultInfo> getSupermotcleSearchResultInfoList() {
            return infoList;
        }

    }

}
