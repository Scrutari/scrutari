/* ScrutariLib_SearchEngine - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.scrutari.data.AttributesPrimitives;
import net.scrutari.data.URIPrimitives;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.FicheSearchSource;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;
import net.scrutari.searchengine.api.result.SearchTokenReport;
import net.scrutari.searchengine.tools.options.ListReductionBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchResultBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchResultGroupBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchResultInfoBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchSourceBuilder;
import net.scrutari.searchengine.tools.result.ResultUtils;
import net.scrutari.searchengine.tools.result.SearchTokenOccurrenceBuilder;
import net.scrutari.searchengine.tools.result.SearchTokenReportBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class FicheSearchResultStorage {

    private final static int CACHE_VERSION = 8;

    private FicheSearchResultStorage() {
    }

    public static void write(File file, FicheSearchResult ficheSearchResult) throws IOException {
        try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(file))) {
            PrimitivesWriter primWriter = PrimitivesIOFactory.newWriter(os);
            FicheSearchResultStorage.write(primWriter, ficheSearchResult);
        }
    }

    public static void write(PrimitivesWriter primitivesWriter, FicheSearchResult ficheSearchResult) throws IOException {
        primitivesWriter.writeInt(CACHE_VERSION);
        writeFicheSearchSource(primitivesWriter, ficheSearchResult.getFicheSearchSource());
        List<SearchTokenReport> searchTokenReportList = ficheSearchResult.getSearchTokenReportList();
        int reportLength = searchTokenReportList.size();
        primitivesWriter.writeInt(reportLength);
        for (int i = 0; i < reportLength; i++) {
            writeSearchTokenReport(primitivesWriter, searchTokenReportList.get(i));
        }
        primitivesWriter.writeInt(ficheSearchResult.getFicheCount());
        primitivesWriter.writeInt(ficheSearchResult.getFicheMaximum());
        List<FicheSearchResultGroup> groupList = ficheSearchResult.getFicheSearchResultGroupList();
        int groupLength = groupList.size();
        primitivesWriter.writeInt(groupLength);
        for (int i = 0; i < groupLength; i++) {
            FicheSearchResultGroup ficheSearchResultGroup = groupList.get(i);
            primitivesWriter.writeInt(ficheSearchResultGroup.getCategoryRank());
            List<FicheSearchResultInfo> ficheInfoList = ficheSearchResultGroup.getFicheSearchResultInfoList();
            int ficheLength = ficheInfoList.size();
            primitivesWriter.writeInt(ficheLength);
            for (int j = 0; j < ficheLength; j++) {
                writeFicheSearchResultInfo(primitivesWriter, ficheInfoList.get(j));
            }
        }
        List<MotcleSearchResultInfo> motcleInfoList = ficheSearchResult.getMotcleSearchResultInfoList();
        int motcleLength = motcleInfoList.size();
        primitivesWriter.writeInt(motcleLength);
        for (int i = 0; i < motcleLength; i++) {
            writeMotcleSearchResultInfo(primitivesWriter, motcleInfoList.get(i));
        }
    }

    public static FicheSearchResult read(File file, LexieAccess lexieAccess) throws IOException {
        FicheSearchResult ficheSearchResult;
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(file))) {
            PrimitivesReader primReader = PrimitivesIOFactory.newReader(is);
            ficheSearchResult = FicheSearchResultStorage.read(primReader, lexieAccess);
        }
        return ficheSearchResult;
    }

    public static FicheSearchResult read(PrimitivesReader primitivesReader, LexieAccess lexieAccess) throws IOException {
        int version = primitivesReader.readInt();
        if (version != CACHE_VERSION) {
            return null;
        }
        FicheSearchSource ficheSearchSource = readFicheSearchSource(primitivesReader);
        FicheSearchResultBuilder builder = new FicheSearchResultBuilder(ficheSearchSource);
        int searchTokenReportLength = primitivesReader.readInt();
        for (int i = 0; i < searchTokenReportLength; i++) {
            builder.addSearchTokenReport(readSearchTokenReport(primitivesReader, lexieAccess));
        }
        int ficheCount = primitivesReader.readInt();
        builder.setFicheCount(ficheCount);
        builder.setFicheMaximum(primitivesReader.readInt());
        int groupLength = primitivesReader.readInt();
        if (ficheCount > 0) {
            for (int i = 0; i < groupLength; i++) {
                FicheSearchResultGroupBuilder groupBuilder = new FicheSearchResultGroupBuilder();
                groupBuilder.setCategoryRank(primitivesReader.readInt());
                int ficheInfoLength = primitivesReader.readInt();
                groupBuilder.ensureInfoCapacity(ficheInfoLength);
                for (int j = 0; j < ficheInfoLength; j++) {
                    groupBuilder.addFicheSearchResultInfo(readFicheSearchResultInfo(primitivesReader));
                }
                builder.addFicheSearchResultGroup(groupBuilder.toFicheSearchResultGroup());
            }
        }
        int motcleLength = primitivesReader.readInt();
        for (int i = 0; i < motcleLength; i++) {
            builder.addMotcleSearchResultInfo(readMotcleSearchResultInfo(primitivesReader));
        }
        return builder.toFicheSearchResult();
    }

    private static void writeFicheSearchResultInfo(PrimitivesWriter primitivesWriter, FicheSearchResultInfo ficheSearchResultInfo) throws IOException {
        primitivesWriter.writeInt(ficheSearchResultInfo.getCode());
        primitivesWriter.writeIntArray(ficheSearchResultInfo.getMotcleCodeList());
        List<InAlineaOccurrences> occurrencesList = ficheSearchResultInfo.getInAlineaOccurrencesList();
        int occurrencesLength = occurrencesList.size();
        primitivesWriter.writeInt(occurrencesLength);
        for (int i = 0; i < occurrencesLength; i++) {
            InAlineaOccurrences inAlineaOccurrenceList = occurrencesList.get(i);
            primitivesWriter.writeInt(inAlineaOccurrenceList.getAlineaRank().getValue());
            writeSearchTokenPositionList(primitivesWriter, inAlineaOccurrenceList.getSearchTokenOccurrenceList());
        }
        int[] scoreArray = ficheSearchResultInfo.getScoreArray();
        int scoreLength = scoreArray.length;
        primitivesWriter.writeInt(scoreLength);
        for (int i = 0; i < scoreLength; i++) {
            primitivesWriter.writeInt(scoreArray[i]);
        }
    }

    private static void writeMotcleSearchResultInfo(PrimitivesWriter primitivesWriter, MotcleSearchResultInfo motcleSearchResultInfo) throws IOException {
        primitivesWriter.writeInt(motcleSearchResultInfo.getCode());
        primitivesWriter.writeInt(motcleSearchResultInfo.getScore());
        List<InLangOccurrences> occurrencesList = motcleSearchResultInfo.getInLangOccurrencesList();
        int occurrencesLength = occurrencesList.size();
        primitivesWriter.writeInt(occurrencesLength);
        for (int i = 0; i < occurrencesLength; i++) {
            InLangOccurrences inLangOccurrences = occurrencesList.get(i);
            primitivesWriter.writeString(inLangOccurrences.getLang().toString());
            writeSearchTokenPositionList(primitivesWriter, inLangOccurrences.getSearchTokenOccurrenceList());
        }
    }

    private static void writeSearchTokenPositionList(PrimitivesWriter primitivesWriter, List<SearchTokenOccurrence> searchTokenOccurrenceList) throws IOException {
        int size = searchTokenOccurrenceList.size();
        primitivesWriter.writeInt(size);
        for (int j = 0; j < size; j++) {
            SearchTokenOccurrence tokenOccurrence = searchTokenOccurrenceList.get(j);
            primitivesWriter.writeInt(tokenOccurrence.getOperandNumber());
            primitivesWriter.writeInt(tokenOccurrence.getBeginIndex());
            primitivesWriter.writeInt(tokenOccurrence.getLength());
        }
    }

    private static void writeSearchTokenReport(PrimitivesWriter primitivesWriter, SearchTokenReport searchTokenReport) throws IOException {
        primitivesWriter.writeInt(searchTokenReport.getOperandNumber());
        primitivesWriter.writeString(searchTokenReport.getOperandString());
        List<SearchTokenReport.CanonicalReport> canonicalReportList = searchTokenReport.getCanonicalReportList();
        int canonicalLength = canonicalReportList.size();
        primitivesWriter.writeInt(canonicalLength);
        for (int i = 0; i < canonicalLength; i++) {
            SearchTokenReport.CanonicalReport canonicalReport = canonicalReportList.get(i);
            primitivesWriter.writeString(canonicalReport.getCanonical());
            List<SearchTokenReport.LangReport> langReportList = canonicalReport.getLangReportList();
            int langLength = langReportList.size();
            primitivesWriter.writeInt(langLength);
            for (int j = 0; j < langLength; j++) {
                SearchTokenReport.LangReport langReport = langReportList.get(j);
                primitivesWriter.writeString(langReport.getLang().toString());
                primitivesWriter.writeInt(langReport.getFicheCount());
                primitivesWriter.writeInt(langReport.getMotcleCount());
            }
        }
        List<SearchTokenReport.NeighbourReport> neighbourReportList = searchTokenReport.getNeighbourReportList();
        int neighbourLength = neighbourReportList.size();
        primitivesWriter.writeInt(neighbourLength);
        for (int i = 0; i < neighbourLength; i++) {
            SearchTokenReport.NeighbourReport neighbourReport = neighbourReportList.get(i);
            primitivesWriter.writeString(neighbourReport.getNeighbour());
            List<Lang> langList = neighbourReport.getLangList();
            int langLength = langList.size();
            primitivesWriter.writeInt(langLength);
            for (int j = 0; j < langLength; j++) {
                Lang lang = langList.get(j);
                primitivesWriter.writeString(lang.toString());
            }
        }
    }

    private static FicheSearchResultInfo readFicheSearchResultInfo(PrimitivesReader primitivesReader) throws IOException {
        Integer code = primitivesReader.readInt();
        int motcleCount = primitivesReader.readInt();
        List<Integer> motcleCodeList;
        if (motcleCount > 0) {
            int[] motcleCodeArray = new int[motcleCount];
            for (int i = 0; i < motcleCount; i++) {
                motcleCodeArray[i] = primitivesReader.readInt();
            }
            motcleCodeList = PrimUtils.wrap(motcleCodeArray);
        } else {
            motcleCodeList = PrimUtils.EMPTY_LIST;
        }
        int inAlineaOccurrenceListCount = primitivesReader.readInt();
        InAlineaOccurrences[] inAlineaOccurrenceListArray = null;
        if (inAlineaOccurrenceListCount > 0) {
            inAlineaOccurrenceListArray = new InAlineaOccurrences[inAlineaOccurrenceListCount];
            for (int i = 0; i < inAlineaOccurrenceListCount; i++) {
                AlineaRank alineaRank = AlineaRank.build(primitivesReader.readInt());
                SearchTokenOccurrence[] searchTokenOccurrenceArray = readSearchTokenPositionArray(primitivesReader);
                inAlineaOccurrenceListArray[i] = ResultUtils.toInAlineaOccurrences(alineaRank, searchTokenOccurrenceArray);
            }
        }
        int scoreLength = primitivesReader.readInt();
        int[] scoreArray = new int[scoreLength];
        for (int i = 0; i < scoreLength; i++) {
            scoreArray[i] = primitivesReader.readInt();
        }
        return FicheSearchResultInfoBuilder.toFicheSearchResultInfo(code, motcleCodeList, inAlineaOccurrenceListArray, scoreArray);
    }

    private static MotcleSearchResultInfo readMotcleSearchResultInfo(PrimitivesReader primitivesReader) throws IOException {
        Integer code = primitivesReader.readInt();
        int score = primitivesReader.readInt();
        int occurrencesLength = primitivesReader.readInt();
        InLangOccurrences[] inLangOccurrencesArray = null;
        if (occurrencesLength > 0) {
            inLangOccurrencesArray = new InLangOccurrences[occurrencesLength];
            for (int i = 0; i < occurrencesLength; i++) {
                Lang lang = Lang.build(primitivesReader.readString());
                SearchTokenOccurrence[] searchTokenOccurrenceArray = readSearchTokenPositionArray(primitivesReader);
                inLangOccurrencesArray[i] = ResultUtils.toInLangOccurrences(lang, searchTokenOccurrenceArray);
            }
        }
        return ResultUtils.toMotcleSearchResultInfo(code, score, inLangOccurrencesArray);
    }

    private static SearchTokenOccurrence[] readSearchTokenPositionArray(PrimitivesReader primitivesReader) throws IOException {
        int searchTokenPositionCount = primitivesReader.readInt();
        if (searchTokenPositionCount == 0) {
            return null;
        }
        SearchTokenOccurrence[] result = new SearchTokenOccurrence[searchTokenPositionCount];
        for (int i = 0; i < searchTokenPositionCount; i++) {
            int operandNumber = primitivesReader.readInt();
            int beginIndex = primitivesReader.readInt();
            int length = primitivesReader.readInt();
            result[i] = SearchTokenOccurrenceBuilder.toSearchTokenOccurrence(operandNumber, beginIndex, length);
        }
        return result;
    }

    private static void writeFicheSearchSource(PrimitivesWriter primitivesWriter, FicheSearchSource ficheSearchSource) throws IOException {
        QId qId = ficheSearchSource.getQId();
        String qIdString;
        if (qId != null) {
            qIdString = qId.toString();
        } else {
            qIdString = "";
        }
        primitivesWriter.writeString(qIdString);
        primitivesWriter.writeString(ficheSearchSource.getCanonicalQ().toString());
        SearchOptionsDef searchOptionsDef = ficheSearchSource.getSearchOptionsDef();
        if (searchOptionsDef.isEmpty()) {
            primitivesWriter.writeBoolean(false);
            return;
        } else {
            primitivesWriter.writeBoolean(true);
        }
        Lang lang = searchOptionsDef.getLang();
        if (lang != null) {
            primitivesWriter.writeString(lang.toString());
        } else {
            primitivesWriter.writeString("");
        }
        Langs filterLangs = searchOptionsDef.getFilterLangs();
        primitivesWriter.writeInt(filterLangs.size());
        for (Lang filterLang : filterLangs) {
            primitivesWriter.writeString(filterLang.toString());
        }
        writeListReduction(primitivesWriter, searchOptionsDef, ScrutariDataURI.BASEURI_TYPE);
        writeListReduction(primitivesWriter, searchOptionsDef, ScrutariDataURI.CORPUSURI_TYPE);
        writeListReduction(primitivesWriter, searchOptionsDef, ScrutariDataURI.THESAURUSURI_TYPE);
        writeListReduction(primitivesWriter, searchOptionsDef, ScrutariDataURI.FICHEURI_TYPE);
        writeListReduction(primitivesWriter, searchOptionsDef, ScrutariDataURI.MOTCLEURI_TYPE);
        AttributesPrimitives.writeAttributes(searchOptionsDef.getAttributes(), primitivesWriter);
    }

    private static void writeListReduction(PrimitivesWriter primitivesWriter, SearchOptionsDef searchOptionsDef, short type) throws IOException {
        ListReduction listReduction = searchOptionsDef.getListReduction(type);
        if (listReduction == null) {
            primitivesWriter.writeInt(0);
        } else {
            List<ScrutariDataURI> uriList = listReduction.getScrutariDataURIList();
            primitivesWriter.writeInt(uriList.size());
            if (!uriList.isEmpty()) {
                primitivesWriter.writeBoolean(listReduction.isExclude());
                for (ScrutariDataURI uri : uriList) {
                    URIPrimitives.writeScrutariDataURI(uri, primitivesWriter);
                }
            }
        }
    }

    private static FicheSearchSource readFicheSearchSource(PrimitivesReader primitivesReader) throws IOException {
        String qIdString = primitivesReader.readString();
        QId qId = null;
        if (qIdString.length() > 0) {
            try {
                qId = QId.parse(qIdString);
            } catch (ParseException pe) {
            }
        }
        String q = primitivesReader.readString();
        CanonicalQ canonicalQ = CanonicalQ.newInstance(q);
        SearchOptionsDef searchOptionsDef;
        boolean withOptions = primitivesReader.readBoolean();
        if (withOptions) {
            SearchOptionsDefBuilder searchOptionsDefBuilder = new SearchOptionsDefBuilder();
            String langString = primitivesReader.readString();
            if (!langString.isEmpty()) {
                searchOptionsDefBuilder.setLang(Lang.build(langString));
            }
            int langCount = primitivesReader.readInt();
            for (int i = 0; i < langCount; i++) {
                searchOptionsDefBuilder.addFilterLang(Lang.build(primitivesReader.readString()));
            }
            readListReduction(primitivesReader, searchOptionsDefBuilder, ScrutariDataURI.BASEURI_TYPE);
            readListReduction(primitivesReader, searchOptionsDefBuilder, ScrutariDataURI.CORPUSURI_TYPE);
            readListReduction(primitivesReader, searchOptionsDefBuilder, ScrutariDataURI.THESAURUSURI_TYPE);
            readListReduction(primitivesReader, searchOptionsDefBuilder, ScrutariDataURI.FICHEURI_TYPE);
            readListReduction(primitivesReader, searchOptionsDefBuilder, ScrutariDataURI.MOTCLEURI_TYPE);
            AttributesPrimitives.readAttributes(primitivesReader, searchOptionsDefBuilder.getAttributesBuilder());
            searchOptionsDef = searchOptionsDefBuilder.toSearchOptionsDef();
        } else {
            searchOptionsDef = SearchOptionsDefBuilder.EMPTY_SEARCHOPTIONSDEF;
        }
        return FicheSearchSourceBuilder.init()
                .setQId(qId)
                .setCanonicalQ(canonicalQ)
                .setSearchOptionsDef(searchOptionsDef)
                .toFicheSearchSource();
    }

    private static void readListReduction(PrimitivesReader primitivesReader, SearchOptionsDefBuilder searchOptionsDefBuilder, short type) throws IOException {
        int size = primitivesReader.readInt();
        if (size == 0) {
            return;
        }
        ListReductionBuilder listReductionBuilder = new ListReductionBuilder(type);
        boolean exclude = primitivesReader.readBoolean();
        listReductionBuilder.setExclude(exclude);
        for (int i = 0; i < size; i++) {
            listReductionBuilder.add(URIPrimitives.readScrutariDataURI(primitivesReader));
        }
        searchOptionsDefBuilder.putListReduction(listReductionBuilder.toListReduction());
    }

    private static SearchTokenReport readSearchTokenReport(PrimitivesReader primitivesReader, LexieAccess lexieAccess) throws IOException {
        int number = primitivesReader.readInt();
        String operandString = primitivesReader.readString();
        SearchTokenReportBuilder searchTokenReportBuilder = new SearchTokenReportBuilder(number, operandString);
        int canonicalLength = primitivesReader.readInt();
        for (int i = 0; i < canonicalLength; i++) {
            String canonicalString = primitivesReader.readString();
            int langLength = primitivesReader.readInt();
            for (int j = 0; j < langLength; j++) {
                String langString = primitivesReader.readString();
                int ficheCount = primitivesReader.readInt();
                int motcleCount = primitivesReader.readInt();
                searchTokenReportBuilder.addCanonical(canonicalString, Lang.build(langString), ficheCount, motcleCount);
            }
        }
        int neighbourLength = primitivesReader.readInt();
        for (int i = 0; i < neighbourLength; i++) {
            String neighbourString = primitivesReader.readString();
            int langLength = primitivesReader.readInt();
            for (int j = 0; j < langLength; j++) {
                String langString = primitivesReader.readString();
                searchTokenReportBuilder.addNeighbour(neighbourString, Lang.build(langString));
            }
        }
        return searchTokenReportBuilder.toSearchTokenReport();
    }

}
