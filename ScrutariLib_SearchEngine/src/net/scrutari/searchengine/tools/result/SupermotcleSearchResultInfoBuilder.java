/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.List;
import net.mapeadores.util.text.SubstringPosition;
import net.mapeadores.util.text.collation.CollationUnit;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.scrutari.searchengine.api.result.SearchTokenOccurrence;
import net.scrutari.searchengine.api.result.SupermotcleSearchResultInfo;
import net.scrutari.supermotcle.api.Supermotcle;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleSearchResultInfoBuilder {

    private Supermotcle supermotcle;
    private OccurrencesBuilder occurrencesBuilder;

    public SupermotcleSearchResultInfoBuilder(Supermotcle supermotcle, int operandMaxNumber) {
        this.supermotcle = supermotcle;
        this.occurrencesBuilder = new OccurrencesBuilder(operandMaxNumber);
    }

    public void addSearchResultUnit(int operandNumber, SearchResultUnit sru) {
        CollationUnit collationUnit = supermotcle.getCollationUnit();
        SubstringPosition positionInSourceString = collationUnit.getSubstringPositionInSourceString(sru.getSearchTextPositionInCollatedKey());
        occurrencesBuilder.add(operandNumber, positionInSourceString);
    }

    public SupermotcleSearchResultInfo toSupermotcleSearchResultInfo() {
        return new InternalSupermotcleSearchResultInfo(supermotcle, occurrencesBuilder.toSearchTokenOccurrenceList());
    }


    private static class InternalSupermotcleSearchResultInfo implements SupermotcleSearchResultInfo {

        private final Supermotcle supermotcle;
        private List<SearchTokenOccurrence> searchTokenPositionList;


        private InternalSupermotcleSearchResultInfo(Supermotcle supermotcle, List<SearchTokenOccurrence> searchTokenPositionList) {
            this.supermotcle = supermotcle;
            this.searchTokenPositionList = searchTokenPositionList;
        }

        @Override
        public Supermotcle getSupermotcle() {
            return supermotcle;
        }

        @Override
        public List<SearchTokenOccurrence> getSearchTokenOccurrenceList() {
            return searchTokenPositionList;
        }

    }

}
