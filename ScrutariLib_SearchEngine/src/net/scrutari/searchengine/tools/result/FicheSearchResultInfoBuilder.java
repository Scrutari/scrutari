/* ScrutariLib_SearchEngine - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.List;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchResultInfoBuilder {

    private FicheSearchResultInfoBuilder() {
    }

    public static FicheSearchResultInfo toFicheSearchResultInfo(Integer code, List<Integer> motcleCodeList, InAlineaOccurrences[] inAlineaOccurrenceListArray, int[] scoreArray) {
        List<InAlineaOccurrences> list;
        if ((inAlineaOccurrenceListArray == null) || (inAlineaOccurrenceListArray.length == 0)) {
            list = ResultUtils.EMPTY_INALINEAOCCURRENCESLIST;
        } else {
            list = ResultUtils.wrap(inAlineaOccurrenceListArray);
        }
        return new InternalFicheSearchResultInfo(code, motcleCodeList, list, scoreArray);
    }


    private static class InternalFicheSearchResultInfo implements FicheSearchResultInfo {

        private final Integer code;
        private final List<Integer> motcleCodeList;
        private final List<InAlineaOccurrences> inAlineaOccurrencesList;
        private final int[] scoreArray;

        private InternalFicheSearchResultInfo(Integer code, List<Integer> motcleCodeList, List<InAlineaOccurrences> inAlineaOccurrencesList, int[] scoreArray) {
            this.code = code;
            this.motcleCodeList = motcleCodeList;
            this.inAlineaOccurrencesList = inAlineaOccurrencesList;
            this.scoreArray = scoreArray;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public List<Integer> getMotcleCodeList() {
            return motcleCodeList;
        }

        @Override
        public List<InAlineaOccurrences> getInAlineaOccurrencesList() {
            return inAlineaOccurrencesList;
        }

        @Override
        public InAlineaOccurrences getInAlineaOccurrencesByAlinea(AlineaRank alineaRank) {
            int count = inAlineaOccurrencesList.size();
            for (int i = 0; i < count; i++) {
                InAlineaOccurrences inAlineaOccurrenceList = inAlineaOccurrencesList.get(i);
                if (inAlineaOccurrenceList.getAlineaRank().equals(alineaRank)) {
                    return inAlineaOccurrenceList;
                }
            }
            return null;
        }

        @Override
        public int[] getScoreArray() {
            return scoreArray;
        }

    }

}
