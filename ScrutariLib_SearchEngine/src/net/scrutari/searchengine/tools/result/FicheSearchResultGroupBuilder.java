/* ScrutariLib_SearchEngine - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.ArrayList;
import java.util.List;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchResultGroupBuilder {

    private final ArrayList<FicheSearchResultInfo> infoList = new ArrayList<FicheSearchResultInfo>();
    private int categoryRank;

    public FicheSearchResultGroupBuilder() {
    }

    public void setCategoryRank(int categoryRank) {
        this.categoryRank = categoryRank;
    }

    public void ensureInfoCapacity(int size) {
        infoList.ensureCapacity(size);
    }

    public void addFicheSearchResultInfo(FicheSearchResultInfo ficheSearchResultInfo) {
        infoList.add(ficheSearchResultInfo);
    }

    public FicheSearchResultGroup toFicheSearchResultGroup() {
        return new InternalFicheSearchResultGroup(categoryRank, ResultUtils.wrap(infoList.toArray(new FicheSearchResultInfo[infoList.size()])));
    }


    private static class InternalFicheSearchResultGroup implements FicheSearchResultGroup {

        private final int categoryRank;
        private final List<FicheSearchResultInfo> ficheSearchResultInfoList;

        private InternalFicheSearchResultGroup(int categoryRank, List<FicheSearchResultInfo> ficheSearchResultInfoArray) {
            this.categoryRank = categoryRank;
            this.ficheSearchResultInfoList = ficheSearchResultInfoArray;
        }

        @Override
        public int getCategoryRank() {
            return categoryRank;
        }

        @Override
        public List<FicheSearchResultInfo> getFicheSearchResultInfoList() {
            return ficheSearchResultInfoList;
        }

    }

}
