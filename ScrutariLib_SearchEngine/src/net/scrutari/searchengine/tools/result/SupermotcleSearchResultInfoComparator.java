/* ScrutariLib_SearchEngine - Copyright (c) 2013-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.Comparator;
import net.scrutari.searchengine.api.result.SupermotcleSearchResultInfo;
import net.scrutari.supermotcle.api.Supermotcle;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleSearchResultInfoComparator implements Comparator<SupermotcleSearchResultInfo> {

    public final static SupermotcleSearchResultInfoComparator UNIQUE = new SupermotcleSearchResultInfoComparator();

    private SupermotcleSearchResultInfoComparator() {
    }

    @Override
    public int compare(SupermotcleSearchResultInfo s1, SupermotcleSearchResultInfo s2) {
        Supermotcle smc1 = s1.getSupermotcle();
        Supermotcle smc2 = s2.getSupermotcle();
        int cd1 = smc1.getCode();
        int cd2 = smc2.getCode();
        if (cd1 == cd2) {
            return 0;
        }
        int baseCount1 = smc1.getIndexationByBaseList().size();
        int baseCount2 = smc2.getIndexationByBaseList().size();
        if (baseCount1 > baseCount2) {
            return 1;
        }
        if (baseCount2 < baseCount1) {
            return -1;
        }
        int indexationCount1 = smc1.getIndexationCount();
        int indexationCount2 = smc2.getIndexationCount();
        if (indexationCount1 > indexationCount2) {
            return 1;
        }
        if (indexationCount1 < indexationCount2) {
            return -1;
        }
        int compare = ResultUtils.compare(s1.getSearchTokenOccurrenceList(), s2.getSearchTokenOccurrenceList());
        if (compare != 0) {
            return compare;
        }
        int ssl1 = smc1.getCollationUnit().getSourceString().length();
        int ssl2 = smc1.getCollationUnit().getSourceString().length();
        if (ssl1 < ssl2) {
            return 1;
        }
        if (ssl1 > ssl2) {
            return -1;
        }
        if (cd1 < cd2) {
            return 1;
        }
        if (cd1 > cd2) {
            return -1;
        }
        return 0;
    }


}
