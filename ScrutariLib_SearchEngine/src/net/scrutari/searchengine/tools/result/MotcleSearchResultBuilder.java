/* ScrutariLib_SearchEngine - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.result;

import java.util.ArrayList;
import java.util.List;
import net.scrutari.searchengine.api.result.MotcleSearchResult;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public class MotcleSearchResultBuilder {

    private final List<MotcleSearchResultInfo> motcleList = new ArrayList<MotcleSearchResultInfo>();

    public MotcleSearchResultBuilder() {
    }

    public MotcleSearchResultBuilder addMotcleSearchResultInfo(MotcleSearchResultInfo motcleSearchResultInfo) {
        motcleList.add(motcleSearchResultInfo);
        return this;
    }

    public MotcleSearchResult toMotcleSearchResult() {
        List<MotcleSearchResultInfo> motcleSearchResultInfoList = ResultUtils.wrap(motcleList.toArray(new MotcleSearchResultInfo[motcleList.size()]));
        return new InternalMotcleSearchResult(motcleSearchResultInfoList);
    }


    private class InternalMotcleSearchResult implements MotcleSearchResult {

        private final List<MotcleSearchResultInfo> list;

        private InternalMotcleSearchResult(List<MotcleSearchResultInfo> list) {
            this.list = list;
        }

        @Override
        public List<MotcleSearchResultInfo> getMotcleSearchResultInfoList() {
            return list;
        }

    }

}
