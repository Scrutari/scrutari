/* ScrutariLib_SearchEngine - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.exceptions.ShouldNotOccurException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.primitives.ByteArrayKey;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.api.options.SearchOptionsDef;


/**
 *
 * @author Vincent Calame
 */
public final class SearchEngineUtils {

    private SearchEngineUtils() {
    }

    public static ByteArrayKey toKey(CanonicalQ canonicalQ, SearchOptionsDef searchOptionsDef) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            PrimitivesWriter primWriter = PrimitivesIOFactory.newWriter(stream);
            primWriter.writeString(canonicalQ.toString());
            writeSearchOptions(searchOptionsDef, primWriter);
            return new ByteArrayKey(stream.toByteArray());
        } catch (IOException ioe) {
            throw new ShouldNotOccurException(ioe);
        }
    }

    private static void writeSearchOptions(SearchOptionsDef searchOptionsDef, PrimitivesWriter primWriter) throws IOException {
        if (searchOptionsDef.isEmpty()) {
            primWriter.writeBoolean(false);
            return;
        }
        primWriter.writeBoolean(true);
        Lang lang = searchOptionsDef.getLang();
        if (lang != null) {
            primWriter.writeString(lang.toString());
        } else {
            primWriter.writeString("");
        }
        Langs filterLangs = searchOptionsDef.getFilterLangs();
        if (!filterLangs.isEmpty()) {
            primWriter.writeInt(1);
            primWriter.writeInt(filterLangs.size());
            for (Lang filterLang : filterLangs) {
                primWriter.writeString(filterLang.toString());
            }
        }
        writeReduction(2, ScrutariDataURI.BASEURI_TYPE, searchOptionsDef, primWriter);
        writeReduction(3, ScrutariDataURI.CORPUSURI_TYPE, searchOptionsDef, primWriter);
        writeReduction(4, ScrutariDataURI.THESAURUSURI_TYPE, searchOptionsDef, primWriter);
        writeReduction(5, ScrutariDataURI.FICHEURI_TYPE, searchOptionsDef, primWriter);
        writeReduction(6, ScrutariDataURI.MOTCLEURI_TYPE, searchOptionsDef, primWriter);
        Operand ficheEligibilityOperand = searchOptionsDef.getFicheEligibilityOperand();
        if (ficheEligibilityOperand != null) {
            primWriter.writeInt(7);
            primWriter.writeString(LogicalOperationUtils.toInformatiqueString(ficheEligibilityOperand));
        }
        PertinencePonderation pertinencePonderation = searchOptionsDef.getPertinencePonderation();
        if (pertinencePonderation != null) {
            primWriter.writeInt(8);
            primWriter.writeInt(toPertinenceInt(pertinencePonderation.getOccurrencePonderation()));
            primWriter.writeInt(toPertinenceInt(pertinencePonderation.getDatePonderation()));
            primWriter.writeInt(toPertinenceInt(pertinencePonderation.getOriginPonderation()));
            primWriter.writeInt(toPertinenceInt(pertinencePonderation.getLangPonderation()));
        }
        Attributes attributes = searchOptionsDef.getAttributes();
        int attributeLength = attributes.size();
        if (attributeLength > 0) {
            primWriter.writeInt(9);
            primWriter.writeInt(attributeLength);
            if (attributeLength == 1) {
                writeAttribute(attributes.get(0), primWriter);
            } else {
                SortedMap<String, Attribute> attributeMap = new TreeMap<String, Attribute>();
                for (int i = 0; i < attributeLength; i++) {
                    Attribute attribute = attributes.get(i);
                    attributeMap.put(attribute.getAttributeKey().toString(), attribute);
                }
                for (Attribute attribute : attributeMap.values()) {
                    writeAttribute(attribute, primWriter);
                }
            }
        }
    }

    private static void writeAttribute(Attribute attribute, PrimitivesWriter primWriter) throws IOException {
        primWriter.writeString(attribute.getAttributeKey().toString());
        int valueLength = attribute.size();
        if (valueLength == 1) {
            primWriter.writeInt(1);
            primWriter.writeString(attribute.getFirstValue());
        } else {
            SortedSet<String> valSet = new TreeSet<String>();
            for (int i = 0; i < valueLength; i++) {
                valSet.add(attribute.get(i));
            }
            primWriter.writeInt(valSet.size());
            for (String s : valSet) {
                primWriter.writeString(s);
            }
        }
    }

    private static void writeReduction(int numero, short type, SearchOptionsDef searchOptionsDef, PrimitivesWriter primWriter) throws IOException {
        ListReduction listReduction = searchOptionsDef.getListReduction(type);
        if (listReduction != null) {
            List<ScrutariDataURI> uriList = listReduction.getScrutariDataURIList();
            primWriter.writeInt(numero);
            primWriter.writeBoolean(listReduction.isExclude());
            primWriter.writeInt(uriList.size());
            for (ScrutariDataURI uri : uriList) {
                primWriter.writeString(uri.toString());
            }
        }
    }

    private static int toPertinenceInt(float f) {
        return (int) (f * 100);
    }

}
