/* ScrutariLib_SearchEngine - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.lexieseek;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LocalisationLexieFilters;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.mapeadores.util.text.search.MultiSearchToken;
import net.mapeadores.util.text.search.SearchToken;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.searchengine.api.lexieseek.LexieSeekHandler;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchOperationReduction;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.tools.operands.search.SearchOperationReductionBuilder;
import net.scrutari.searchengine.tools.result.SearchTokenReportBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class LexieSeekEngine {

    private final LexieAccess lexieAccess;
    private final SearchOperation searchOperation;
    private final ComputedOptions computedOptions;
    private final LexieSeekHandler lexieSeekHandler;
    private final Map<Integer, CorpusData> ficheMap = new LinkedHashMap<Integer, CorpusData>();
    private final Map<Integer, ThesaurusData> motcleMap = new LinkedHashMap<Integer, ThesaurusData>();
    private final SearchTokenReportBuilder[] reportBuilderArray;

    private LexieSeekEngine(LexieAccess lexieAccess, SearchOperation searchOperation, ComputedOptions computedOptions, LexieSeekHandler lexieSeekHandler) {
        this.lexieAccess = lexieAccess;
        this.searchOperation = searchOperation;
        this.computedOptions = computedOptions;
        this.lexieSeekHandler = lexieSeekHandler;
        List<SearchTokenOperand> list = searchOperation.getSearchTokenOperandList();
        int length = list.size();
        this.reportBuilderArray = new SearchTokenReportBuilder[length];
        for (int i = 0; i < length; i++) {
            this.reportBuilderArray[i] = new SearchTokenReportBuilder(list.get(i));
        }
    }

    private void run() {
        Lang[] langArray = computedOptions.getLangArray();
        for (Lang lang : langArray) {
            SearchOperationReduction operationReduction = SearchOperationReductionBuilder.lexieReduction(searchOperation, LocalisationLexieFilters.getLexieFilter(lang));
            ficheMap.clear();
            motcleMap.clear();
            Operand operand = operationReduction.getRootOperand();
            if (operand == null) {
                continue;
            }
            seekOperand(operand, lang);
            List<Integer> unusedList = operationReduction.getUnusedNumberList();
            if (!unusedList.isEmpty()) {
                Integer[] unusedArray = unusedList.toArray(new Integer[unusedList.size()]);
                for (Map.Entry<Integer, CorpusData> entry : ficheMap.entrySet()) {
                    Integer ficheCode = entry.getKey();
                    CorpusData corpusData = entry.getValue();
                    for (int unused : unusedArray) {
                        lexieSeekHandler.addAlineaOccurrence(ficheCode, corpusData.getCorpusCode(), corpusData.getBaseCode(), lang, unused, null, null);
                    }
                }
                for (Map.Entry<Integer, ThesaurusData> entry : motcleMap.entrySet()) {
                    Integer motcleCode = entry.getKey();
                    ThesaurusData thesaurusData = entry.getValue();
                    for (int unused : unusedArray) {
                        lexieSeekHandler.addLangOccurrence(motcleCode, thesaurusData.getThesaurusCode(), lang, unused, null, 0.0f);
                    }
                }
            }
        }
        for (SearchTokenReportBuilder builder : reportBuilderArray) {
            if (builder.hasFailed()) {
                seekNeighbours(builder);
            }
        }
        for (SearchTokenReportBuilder builder : reportBuilderArray) {
            lexieSeekHandler.addSearchTokenReport(builder.toSearchTokenReport());
        }
    }

    private void seekOperand(Operand operand, Lang lang) {
        if (operand instanceof SubOperand) {
            seekSubOperand((SubOperand) operand, lang);
        } else {
            SearchTokenOperand searchOperand = (SearchTokenOperand) operand;
            AlineaFilter alineaFilter = searchOperand.getAlineaFilter();
            int operandNumber = searchOperand.getOperandNumber();
            SearchTokenReportBuilder reportBuilder = reportBuilderArray[operandNumber - 1];
            SearchToken token = searchOperand.getSearchToken();
            if (token instanceof MultiSearchToken) {
                MultiSearchToken multiToken = (MultiSearchToken) token;
                int count = multiToken.getSubtokenCount();
                if (count > 1) {
                    MultiTokenSeeker.seek(multiToken, operandNumber, lang, this, alineaFilter);
                } else if (count == 1) {
                    SimpleTokenSeeker.seek(multiToken.getSubtoken(0), lang, this, alineaFilter, reportBuilder);
                }
            } else {
                SimpleTokenSeeker.seek((SimpleSearchToken) token, lang, this, alineaFilter, reportBuilder);
            }
        }
    }

    private void seekSubOperand(SubOperand subOperand, Lang lang) {
        int operandLength = subOperand.size();
        for (int i = 0; i < operandLength; i++) {
            seekOperand(subOperand.get(i), lang);
        }
    }

    private void seekNeighbours(SearchTokenReportBuilder builder) {
        SearchToken token = builder.getSearchTokenOperand().getSearchToken();
        if (token instanceof SimpleSearchToken) {
            SimpleTokenSeeker.seekNeighbours((SimpleSearchToken) token, this, builder);
        }
    }


    LexieAccess getLexieAccess() {
        return lexieAccess;
    }

    ComputedOptions getComputedOptions() {
        return computedOptions;
    }

    LexieSeekHandler getLexieSeekHandler() {
        return lexieSeekHandler;
    }

    void addFiche(Integer ficheCode, CorpusData corpusData) {
        ficheMap.put(ficheCode, corpusData);
    }

    void addMotcle(Integer motcleCode, ThesaurusData thesaurusData) {
        motcleMap.put(motcleCode, thesaurusData);
    }

    public static void run(LexieAccess lexieAccess, SearchOperation searchOperation, ComputedOptions computedOptions, LexieSeekHandler lexieSeekHandler) {
        LexieSeekEngine engine = new LexieSeekEngine(lexieAccess, searchOperation, computedOptions, lexieSeekHandler);
        engine.run();
    }

}
