/* ScrutariLib_SearchEngine - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.lexieseek;


import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;
import net.mapeadores.util.text.collation.CollationUnitPosition;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.lexie.AlineaOccurrence;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.ScrutariLexieUnit;
import net.scrutari.searchengine.api.lexieseek.LexieSeekHandler;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.tools.result.SearchTokenReportBuilder;


/**
 *
 * @author Vincent Calame
 */
class SimpleTokenSeeker {

    private SimpleTokenSeeker() {
    }

    static void seek(SimpleSearchToken searchToken, Lang lang, LexieSeekEngine lexieSeekEngine, AlineaFilter alineaFilter, SearchTokenReportBuilder searchTokenReportBuilder) {
        int operandNumber = searchTokenReportBuilder.getOperandNumber();
        searchTokenReportBuilder.addTestedLang(lang);
        List<SearchResultUnit<ScrutariLexieUnit>> list = LexieSeekUtils.search(lexieSeekEngine, searchToken.getTokenString(), lang, searchToken.getSearchType());
        for (SearchResultUnit<ScrutariLexieUnit> searchResultItem : list) {
            add(searchResultItem.getValue(), searchResultItem.getSearchTextPositionInCollatedKey(), lexieSeekEngine, alineaFilter, operandNumber, lang, searchTokenReportBuilder);
        }
    }

    static void seekNeighbours(SimpleSearchToken searchToken, LexieSeekEngine lexieSeekEngine, SearchTokenReportBuilder searchTokenReportBuilder) {
        String tokenString = searchToken.getTokenString();
        if (tokenString.length() < 4) {
            return;
        }
        for (Lang lang : searchTokenReportBuilder.getTestedLangSet()) {
            List<SearchResultUnit<ScrutariLexieUnit>> list = LexieSeekUtils.searchNeighbours(lexieSeekEngine, tokenString, lang, searchToken.getSearchType());
            for (SearchResultUnit<ScrutariLexieUnit> resultUnit : list) {
                ScrutariLexieUnit scrutariLexieUnit = resultUnit.getValue();
                searchTokenReportBuilder.addNeighbour(scrutariLexieUnit.getCanonicalLexie(), lang);
            }
        }
    }

    private static void add(ScrutariLexieUnit scrutariLexieUnit, SubstringPosition searchTextePositionInCollatedKey, LexieSeekEngine lexieSeekEngine, AlineaFilter alineaFilter, int operandNumber, Lang lang, SearchTokenReportBuilder searchTokenReportBuilder) {
        final int[] count = new int[2];
        LexieId lexieId = scrutariLexieUnit.getLexieId();
        LexieAccess lexieAccess = lexieSeekEngine.getLexieAccess();
        final ComputedOptions computedOptions = lexieSeekEngine.getComputedOptions();
        final LexieSeekHandler lexieSeekHandler = lexieSeekEngine.getLexieSeekHandler();
        if (computedOptions.isOnFiche()) {
            for (CorpusData corpusData : computedOptions.getCorpusDataArray()) {
                Integer corpusCode = corpusData.getCorpusCode();
                lexieAccess.checkLexieOccurrences(lexieId, corpusCode, ficheCode -> computedOptions.acceptFiche(ficheCode),
                        lexieOccurrences -> {
                            Integer ficheCode = lexieOccurrences.getCode();
                            for (AlineaOccurrence alineaOccurrence : lexieOccurrences) {
                                AlineaRank alineaRank = alineaOccurrence.getAlineaRank();
                                if (alineaFilter.acceptAlinea(corpusCode, alineaRank)) {
                                    SubstringPosition sourcePosition = alineaOccurrence.getBestPositionInAlinea().convertToPositionInWholeSource(searchTextePositionInCollatedKey);
                                    lexieSeekHandler.addAlineaOccurrence(ficheCode, corpusCode, corpusData.getBaseCode(), lang, operandNumber, alineaRank, sourcePosition);
                                    lexieSeekEngine.addFiche(ficheCode, corpusData);
                                    count[0]++;
                                }
                            }
                        });
            }
        }
        if (computedOptions.isOnMotcle()) {
            for (ThesaurusData thesaurusData : computedOptions.getThesaurusDataArray()) {
                Integer thesaurusCode = thesaurusData.getThesaurusCode();
                if (!alineaFilter.acceptMotcle(thesaurusCode)) {
                    continue;
                }
                lexieAccess.checkLexieOccurrences(lexieId, thesaurusCode, motcleCode -> computedOptions.acceptMotcle(motcleCode),
                        lexieOccurrences -> {
                            Integer motcleCode = lexieOccurrences.getCode();
                            AlineaOccurrence firstOccurrence = lexieOccurrences.get(0);
                            CollationUnitPosition bestCollationUnitPosition = firstOccurrence.getBestPositionInAlinea();
                            SubstringPosition sourcePosition = bestCollationUnitPosition.convertToPositionInWholeSource(searchTextePositionInCollatedKey);
                            float coverRate = ((float) sourcePosition.getLength()) / (firstOccurrence.getAlineaWholeLength());
                            lexieSeekHandler.addLangOccurrence(motcleCode, thesaurusCode, lang, operandNumber, sourcePosition, coverRate);
                            lexieSeekEngine.addMotcle(motcleCode, thesaurusData);
                            count[1]++;
                        });
            }
        }
        searchTokenReportBuilder.addCanonical(scrutariLexieUnit.getCanonicalLexie(), lang, count[0], count[1]);
    }

}
