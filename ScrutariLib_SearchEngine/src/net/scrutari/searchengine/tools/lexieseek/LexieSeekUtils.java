/* ScrutariLib_SearchEngine - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.lexieseek;

import java.util.Collections;
import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.collation.map.CollatedKeyMap;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
final class LexieSeekUtils {

    private final static List<SearchResultUnit<ScrutariLexieUnit>> EMPTY_LIST = Collections.emptyList();

    private LexieSeekUtils() {

    }

    static List<SearchResultUnit<ScrutariLexieUnit>> search(LexieSeekEngine lexieSeekEngine, String searchText, Lang lang, int type) {
        CollatedKeyMap<ScrutariLexieUnit> collatedStringMap = lexieSeekEngine.getLexieAccess().getLexieUnitMapByLang(lang);
        if (collatedStringMap == null) {
            return EMPTY_LIST;
        }
        return collatedStringMap.search(searchText, type, null);
    }

    static List<SearchResultUnit<ScrutariLexieUnit>> searchNeighbours(LexieSeekEngine lexieSeekEngine, String searchText, Lang lang, int type) {
        CollatedKeyMap<ScrutariLexieUnit> collatedStringMap = lexieSeekEngine.getLexieAccess().getLexieUnitMapByLang(lang);
        if (collatedStringMap == null) {
            return EMPTY_LIST;
        }
        return collatedStringMap.searchNeighbours(searchText, type, null, 1);
    }

}
