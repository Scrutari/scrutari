/* ScrutariLib_SearchEngine - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.lexieseek;

import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.mapeadores.util.text.search.MultiSearchToken;
import net.mapeadores.util.text.search.SimpleSearchToken;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.lexie.Alinea;
import net.scrutari.lexie.AlineaOccurrence;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;
import net.scrutari.searchengine.api.lexieseek.LexieSeekHandler;
import net.scrutari.searchengine.api.operands.search.AlineaFilter;
import net.scrutari.searchengine.api.options.ComputedOptions;


/**
 *
 * @author Vincent Calame
 */
class MultiTokenSeeker {

    private MultiTokenSeeker() {
    }

    static void seek(MultiSearchToken multiToken, int operandNumber, Lang lang, LexieSeekEngine lexieSeekEngine, AlineaFilter alineaFilter) {
        PotentialLexieSequence potentialLexieSequence = new PotentialLexieSequence(multiToken, lang, lexieSeekEngine);
        if (!potentialLexieSequence.isInit()) {
            return;
        }
        int length = potentialLexieSequence.getPotentialStartCount();
        for (int i = 0; i < length; i++) {
            LexieId lexieId = potentialLexieSequence.setCurrentPotentialStart(i);
            scanLexieOccurrences(potentialLexieSequence, lexieId, operandNumber, lang, lexieSeekEngine, alineaFilter);
        }
    }

    private static void scanLexieOccurrences(PotentialLexieSequence potentialLexieSequence, LexieId lexieId, int operandNumber, Lang lang, LexieSeekEngine lexieSeekEngine, AlineaFilter alineaFilter) {
        final ComputedOptions computedOptions = lexieSeekEngine.getComputedOptions();
        LexieAccess lexieAccess = lexieSeekEngine.getLexieAccess();
        if (computedOptions.isOnFiche()) {
            FicheLexification buffer = new FicheLexification();
            for (CorpusData corpusData : computedOptions.getCorpusDataArray()) {
                lexieAccess.checkLexieOccurrences(lexieId, corpusData.getCorpusCode(), ficheCode -> computedOptions.acceptFiche(ficheCode),
                        lexieOccurrences -> {
                            scanFicheLexification(potentialLexieSequence, corpusData, buffer, lexieOccurrences, operandNumber, lang, lexieSeekEngine, alineaFilter);
                        });
            }
        }
        if (computedOptions.isOnMotcle()) {
            MotcleLexification buffer = new MotcleLexification();
            for (ThesaurusData thesaurusData : computedOptions.getThesaurusDataArray()) {
                Integer thesaurusCode = thesaurusData.getThesaurusCode();
                if (!alineaFilter.acceptMotcle(thesaurusCode)) {
                    continue;
                }
                lexieAccess.checkLexieOccurrences(lexieId, thesaurusCode, motcleCode -> computedOptions.acceptMotcle(motcleCode),
                        lexieOccurrences -> {
                            scanMotcleLexification(potentialLexieSequence, thesaurusData, buffer, lexieOccurrences, operandNumber, lang, lexieSeekEngine);
                        });
            }
        }
    }

    private static void scanFicheLexification(PotentialLexieSequence potentialLexieSequence, CorpusData corpusData, FicheLexification buffer, LexieOccurrences lexieOccurrences, int operandNumber, Lang lang, LexieSeekEngine lexieSeekEngine, AlineaFilter alineaFilter) {
        LexieSeekHandler lexieSeekHandler = lexieSeekEngine.getLexieSeekHandler();
        LexieAccess lexieAccess = lexieSeekEngine.getLexieAccess();
        Integer ficheCode = lexieOccurrences.getCode();
        Integer corpusCode = corpusData.getCorpusCode();
        buffer.clear();
        lexieAccess.populateFicheLexification(ficheCode, buffer);
        for (AlineaOccurrence alineaOccurrence : lexieOccurrences) {
            AlineaRank alineaRank = alineaOccurrence.getAlineaRank();
            if (alineaFilter.acceptAlinea(corpusCode, alineaRank)) {
                Alinea alinea = buffer.getAlinea(alineaRank);
                for (int lexieIndex : alineaOccurrence.getLexieIndices()) {
                    SubstringPosition substringPosition = potentialLexieSequence.testAlinea(alinea, lexieIndex);
                    if (substringPosition != null) {
                        lexieSeekHandler.addAlineaOccurrence(ficheCode, corpusCode, corpusData.getBaseCode(), lang, operandNumber, alineaRank, substringPosition);
                        lexieSeekEngine.addFiche(ficheCode, corpusData);
                    }
                }
            }
        }
    }

    private static void scanMotcleLexification(PotentialLexieSequence potentialLexieSequence, ThesaurusData thesaurusData, MotcleLexification buffer, LexieOccurrences lexieOccurrences, int operandNumber, Lang lang, LexieSeekEngine lexieSeekEngine) {
        LexieSeekHandler lexieSeekHandler = lexieSeekEngine.getLexieSeekHandler();
        LexieAccess lexieAccess = lexieSeekEngine.getLexieAccess();
        Integer motcleCode = lexieOccurrences.getCode();
        buffer.clear();
        lexieAccess.populateMotcleLexification(motcleCode, buffer);
        Alinea alinea = buffer.getAlinea(lang);
        for (int lexieIndex : lexieOccurrences.get(0).getLexieIndices()) {
            SubstringPosition substringPosition = potentialLexieSequence.testAlinea(alinea, lexieIndex);
            if (substringPosition != null) {
                float coverRate = ((float) substringPosition.getLength()) / (alinea.getWholeLength());
                lexieSeekHandler.addLangOccurrence(motcleCode, thesaurusData.getThesaurusCode(), lang, operandNumber, substringPosition, coverRate);
                lexieSeekEngine.addMotcle(motcleCode, thesaurusData);
            }
        }
    }


    private static class PotentialLexieSequence {

        private LexieId[][] lexieIdArrays;
        private SubstringPosition[] firstSearchTextePositionArray;
        private SubstringPosition[] lastSearchTextePositionArray;
        private int currentPotentialStart = 0;

        private PotentialLexieSequence(MultiSearchToken subtokenArray, Lang lang, LexieSeekEngine lexieSeekEngine) {
            int length = subtokenArray.getSubtokenCount();
            lexieIdArrays = new LexieId[length][];
            for (int i = 0; i < length; i++) {
                SimpleSearchToken token = subtokenArray.getSubtoken(i);
                List<SearchResultUnit<ScrutariLexieUnit>> list = LexieSeekUtils.search(lexieSeekEngine, token.getTokenString(), lang, token.getSearchType());
                if (list.isEmpty()) {
                    lexieIdArrays = null;
                    return;
                }
                int size = list.size();
                LexieId[] array = new LexieId[size];
                SubstringPosition[] searchTextePositionArray = null;
                if (i == 0) {
                    firstSearchTextePositionArray = new SubstringPosition[size];
                    searchTextePositionArray = firstSearchTextePositionArray;
                } else if (i == (length - 1)) {
                    lastSearchTextePositionArray = new SubstringPosition[size];
                    searchTextePositionArray = lastSearchTextePositionArray;
                }
                for (int j = 0; j < size; j++) {
                    SearchResultUnit<ScrutariLexieUnit> searchResultItem = list.get(j);
                    array[j] = searchResultItem.getValue().getLexieId();
                    if (searchTextePositionArray != null) {
                        searchTextePositionArray[j] = searchResultItem.getSearchTextPositionInCollatedKey();
                    }
                }
                lexieIdArrays[i] = array;
            }

        }

        private boolean isInit() {
            return (lexieIdArrays != null);
        }

        /*
         * Retourne le nombre de départs possibles pour la séquence. Exemple, si
         * la séquence est "gouv* eau", les départs peuvent être gouvernement ou
         * gouvernance.
         */
        private int getPotentialStartCount() {
            return lexieIdArrays[0].length;
        }

        /*
         * Indique que les tests (voir testLexification) vont se faire sur le
         * i-ème départ possible et retourne l'identifiant de ce i-ème départ
         * (voir getPotentialStartCount ci-dessus)
         */
        private LexieId setCurrentPotentialStart(int i) {
            currentPotentialStart = i;
            return lexieIdArrays[0][i];
        }

        private SubstringPosition testAlinea(Alinea alinea, int index) {
            int length = lexieIdArrays.length;
            if ((index + length) > alinea.getLexieCount()) {
                return null;
            }
            int lastTokenLexieIndex = -1;
            for (int i = 1; i < length; i++) {
                LexieId lexieId = alinea.getLexieId(index + i);
                int test = test(lexieId, i);
                if (test == -1) {
                    return null;
                } else if (i == (length - 1)) {
                    lastTokenLexieIndex = test;
                }
            }
            SubstringPosition firstSubstringPosition = alinea.getCollationUnitPosition(index).convertToPositionInWholeSource(firstSearchTextePositionArray[currentPotentialStart]);
            SubstringPosition lastSubstringPosition = alinea.getCollationUnitPosition(index + length - 1).convertToPositionInWholeSource(lastSearchTextePositionArray[lastTokenLexieIndex]);
            SubstringPosition finalSubstringPosition = SubstringPosition.merge(firstSubstringPosition, lastSubstringPosition);
            return finalSubstringPosition;
        }

        private int test(LexieId lexieId, int tokenIndex) {
            LexieId[] potentialArray = lexieIdArrays[tokenIndex];
            int length = potentialArray.length;
            for (int i = 0; i < length; i++) {
                if (potentialArray[i].equals(lexieId)) {
                    return i;
                }
            }
            return -1;
        }

    }

}
