/* ScrutariLib_SearchEngine - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import net.mapeadores.util.logicaloperation.LogicalOperationConstants;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.logicaloperation.SubOperand;
import net.scrutari.searchengine.api.operands.search.AllOperand;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.operands.search.SearchTokenOperand;
import net.scrutari.searchengine.api.pertinence.FichePertinence;
import net.scrutari.searchengine.api.pertinence.MotclePertinence;
import net.scrutari.searchengine.api.pertinence.PertinenceConstants;


/**
 *
 * @author Vincent Calame
 */
public final class PredicateFactory {

    private final static int[] EMPTY_INTARRAY = new int[0];
    private final static OperandTest[] EMPTY_TESTARRAY = new OperandTest[0];

    private PredicateFactory() {

    }

    public static Predicate<FichePertinence> toFichePertinencePredicate(SearchOperation searchOperation) {
        OperandTest rootOperandTest = init(searchOperation);
        return new FichePertinencePredicate(rootOperandTest);
    }

    public static Predicate<MotclePertinence> toMotclePertinencePredicate(SearchOperation searchOperation) {
        OperandTest rootOperandTest = init(searchOperation);
        return new MotclePertinencePredicate(rootOperandTest);
    }

    private static OperandTest init(SearchOperation searchOperation) {
        Operand rootOperand = searchOperation.getRootOperand();
        if (rootOperand instanceof AllOperand) {
            throw new IllegalStateException("Cannot be called if searchOperation.getRootOperand() instance of AllOperand");
        } else if (rootOperand instanceof SearchTokenOperand) {
            SearchTokenOperand searchTokenOperand = (SearchTokenOperand) rootOperand;
            int[] array = new int[1];
            array[0] = searchTokenOperand.getOperandNumber();
            if (searchTokenOperand.isAcceptMode()) {
                return new OperandTest(false, EMPTY_TESTARRAY, array, EMPTY_INTARRAY);
            } else {
                return new OperandTest(false, EMPTY_TESTARRAY, EMPTY_INTARRAY, array);
            }
        } else {
            return init((SubOperand) rootOperand);
        }
    }

    private static OperandTest init(SubOperand subOperand) {
        boolean isUnionMode = (subOperand.getOperator() == LogicalOperationConstants.UNION_OPERATOR);
        OperandTestBuilder builder = new OperandTestBuilder(isUnionMode);
        int operandLength = subOperand.size();
        for (int i = 0; i < operandLength; i++) {
            Operand operand = subOperand.get(i);
            if (operand instanceof SearchTokenOperand) {
                SearchTokenOperand searchTokenOperand = (SearchTokenOperand) operand;
                int operandNumber = searchTokenOperand.getOperandNumber();
                if (searchTokenOperand.isAcceptMode()) {
                    builder.addAccept(operandNumber);
                } else {
                    builder.addReject(operandNumber);
                }
            } else {
                builder.addOperandTest(init((SubOperand) operand));
            }
        }
        return builder.toOperandTest();
    }

    private static boolean testOperand(OperandTest operandTest, FichePertinence fichePertinence) throws NoPertinentException {
        if (operandTest.unionOperator) {
            return testUnion(operandTest, fichePertinence);
        } else {
            return testIntersection(operandTest, fichePertinence);
        }
    }

    private static boolean testUnion(OperandTest operandTest, FichePertinence fichePertinence) throws NoPertinentException {
        boolean withPertinent = false;
        int acceptLength = operandTest.acceptOperandNumberArray.length;
        for (int i = 0; i < acceptLength; i++) {
            short state = fichePertinence.getOperandState(operandTest.acceptOperandNumberArray[i]);
            if (state == PertinenceConstants.OCCURRENCE_STATE) {
                return true;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int rejectLength = operandTest.rejectOperandNumberArray.length;
        for (int i = 0; i < rejectLength; i++) {
            short state = fichePertinence.getOperandState(operandTest.rejectOperandNumberArray[i]);
            if (state == PertinenceConstants.NO_OCCURRENCE_STATE) {
                return true;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int subLength = operandTest.subTestArray.length;
        for (int i = 0; i < subLength; i++) {
            if (testOperand(operandTest.subTestArray[i], fichePertinence)) {
                return true;
            }
        }
        if (!withPertinent) {
            throw new NoPertinentException();
        }
        return false;
    }

    private static boolean testIntersection(OperandTest operandTest, FichePertinence fichePertinence) throws NoPertinentException {
        boolean withPertinent = false;
        int acceptLength = operandTest.acceptOperandNumberArray.length;
        for (int i = 0; i < acceptLength; i++) {
            short state = fichePertinence.getOperandState(operandTest.acceptOperandNumberArray[i]);
            if (state == PertinenceConstants.NO_OCCURRENCE_STATE) {
                return false;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int rejectLength = operandTest.rejectOperandNumberArray.length;
        for (int i = 0; i < rejectLength; i++) {
            short state = fichePertinence.getOperandState(operandTest.rejectOperandNumberArray[i]);
            if (state == PertinenceConstants.OCCURRENCE_STATE) {
                return false;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int subLength = operandTest.subTestArray.length;
        for (int i = 0; i < subLength; i++) {
            try {
                if (!testOperand(operandTest.subTestArray[i], fichePertinence)) {
                    return false;
                }
                withPertinent = true;
            } catch (NoPertinentException npe) {

            }
        }
        if (!withPertinent) {
            throw new NoPertinentException();
        }
        return true;
    }

    private static boolean testOperand(OperandTest operandTest, MotclePertinence motclePertinence) throws NoPertinentException {
        if (operandTest.unionOperator) {
            return testUnion(operandTest, motclePertinence);
        } else {
            return testIntersection(operandTest, motclePertinence);
        }
    }

    private static boolean testUnion(OperandTest operandTest, MotclePertinence motclePertinence) throws NoPertinentException {
        boolean withPertinent = false;
        int acceptLength = operandTest.acceptOperandNumberArray.length;
        for (int i = 0; i < acceptLength; i++) {
            short state = motclePertinence.getOperandState(operandTest.acceptOperandNumberArray[i]);
            if (state == PertinenceConstants.OCCURRENCE_STATE) {
                return true;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int rejectLength = operandTest.rejectOperandNumberArray.length;
        for (int i = 0; i < rejectLength; i++) {
            short state = motclePertinence.getOperandState(operandTest.rejectOperandNumberArray[i]);
            if (state == PertinenceConstants.NO_OCCURRENCE_STATE) {
                return true;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int subLength = operandTest.subTestArray.length;
        for (int i = 0; i < subLength; i++) {
            if (testOperand(operandTest.subTestArray[i], motclePertinence)) {
                return true;
            }
        }
        if (!withPertinent) {
            throw new NoPertinentException();
        }
        return false;
    }

    private static boolean testIntersection(OperandTest operandTest, MotclePertinence motclePertinence) throws NoPertinentException {
        boolean withPertinent = false;
        int acceptLength = operandTest.acceptOperandNumberArray.length;
        for (int i = 0; i < acceptLength; i++) {
            short state = motclePertinence.getOperandState(operandTest.acceptOperandNumberArray[i]);
            if (state == PertinenceConstants.NO_OCCURRENCE_STATE) {
                return false;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int rejectLength = operandTest.rejectOperandNumberArray.length;
        for (int i = 0; i < rejectLength; i++) {
            short state = motclePertinence.getOperandState(operandTest.rejectOperandNumberArray[i]);
            if (state == PertinenceConstants.OCCURRENCE_STATE) {
                return false;
            }
            if (state != PertinenceConstants.NOPERTINENT_STATE) {
                withPertinent = true;
            }
        }
        int subLength = operandTest.subTestArray.length;
        for (int i = 0; i < subLength; i++) {
            try {
                if (!testOperand(operandTest.subTestArray[i], motclePertinence)) {
                    return false;
                }
                withPertinent = true;
            } catch (NoPertinentException npe) {

            }
        }
        if (!withPertinent) {
            throw new NoPertinentException();
        }
        return true;
    }


    private static class OperandTestBuilder {

        private final boolean unionMode;
        private final List<OperandTest> subList = new ArrayList<OperandTest>();
        private final List<Integer> acceptList = new ArrayList<Integer>();
        private final List<Integer> rejectList = new ArrayList<Integer>();

        private OperandTestBuilder(boolean unionMode) {
            this.unionMode = unionMode;
        }

        private void addOperandTest(OperandTest operandTest) {
            subList.add(operandTest);
        }

        private void addReject(int operandNumber) {
            rejectList.add(operandNumber);
        }

        private void addAccept(int operandNumber) {
            acceptList.add(operandNumber);
        }

        private OperandTest toOperandTest() {
            int[] acceptOperandNumberArray;
            int acceptSize = acceptList.size();
            if (acceptSize > 0) {
                acceptOperandNumberArray = new int[acceptSize];
                for (int i = 0; i < acceptSize; i++) {
                    acceptOperandNumberArray[i] = acceptList.get(i);
                }
            } else {
                acceptOperandNumberArray = EMPTY_INTARRAY;
            }
            int[] rejectOperandNumberArray;
            int rejectSize = rejectList.size();
            if (rejectSize > 0) {
                rejectOperandNumberArray = new int[rejectSize];
                for (int i = 0; i < rejectSize; i++) {
                    rejectOperandNumberArray[i] = rejectList.get(i);
                }
            } else {
                rejectOperandNumberArray = EMPTY_INTARRAY;
            }
            OperandTest[] subTestArray;
            int subSize = subList.size();
            if (subSize > 0) {
                subTestArray = subList.toArray(new OperandTest[subSize]);
            } else {
                subTestArray = EMPTY_TESTARRAY;
            }
            return new OperandTest(unionMode, subTestArray, acceptOperandNumberArray, rejectOperandNumberArray);
        }

    }


    private static class OperandTest {

        private final boolean unionOperator;
        private final OperandTest[] subTestArray;
        private final int[] acceptOperandNumberArray;
        private final int[] rejectOperandNumberArray;

        private OperandTest(boolean unionOperator, OperandTest[] subTestArray, int[] acceptOperandNumberArray, int[] rejectOperandNumberArray) {
            this.unionOperator = unionOperator;
            this.subTestArray = subTestArray;
            this.acceptOperandNumberArray = acceptOperandNumberArray;
            this.rejectOperandNumberArray = rejectOperandNumberArray;
        }

    }


    private static class FichePertinencePredicate implements Predicate<FichePertinence> {

        private final OperandTest rootOperandTest;

        private FichePertinencePredicate(OperandTest rootOperandTest) {
            this.rootOperandTest = rootOperandTest;
        }

        @Override
        public boolean test(FichePertinence fichePertinence) {
            try {
                return testOperand(rootOperandTest, fichePertinence);
            } catch (NoPertinentException npe) {
                return false;
            }
        }

    }


    private static class MotclePertinencePredicate implements Predicate<MotclePertinence> {

        private final OperandTest rootOperandTest;

        private MotclePertinencePredicate(OperandTest rootOperandTest) {
            this.rootOperandTest = rootOperandTest;
        }

        @Override
        public boolean test(MotclePertinence motclePertinence) {
            try {
                return testOperand(rootOperandTest, motclePertinence);
            } catch (NoPertinentException npe) {
                return false;
            }
        }

    }


    private static class NoPertinentException extends Exception {

        private NoPertinentException() {

        }

    }

}
