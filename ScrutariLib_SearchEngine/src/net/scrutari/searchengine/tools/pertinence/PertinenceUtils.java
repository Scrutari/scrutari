/* ScrutariLib_SearchEngine - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.tools.pertinence;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.text.SubstringPosition;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.pertinence.FichePertinence;
import net.scrutari.searchengine.api.pertinence.MotclePertinence;
import net.scrutari.searchengine.api.pertinence.PertinenceConstants;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;
import net.scrutari.searchengine.api.result.InAlineaOccurrences;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.tools.result.FicheSearchResultInfoBuilder;
import net.scrutari.searchengine.tools.result.OccurrencesBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class PertinenceUtils {

    private PertinenceUtils() {
    }

    public static FicheSearchResultInfo toFicheSearchResultInfo(FichePertinence fichePertinence, int[] acceptOperandArray, int[] scoreArray) {
        return FicheSearchResultInfoBuilder.toFicheSearchResultInfo(fichePertinence.getFicheCode(),
                fichePertinence.getAcceptMotcleCodeList(), getInAlineaOccurrencesArray(fichePertinence, acceptOperandArray), scoreArray);
    }

    public static InAlineaOccurrences[] getInAlineaOccurrencesArray(FichePertinence fichePertinence, int[] acceptOperandArray) {
        int acceptLength = acceptOperandArray.length;
        Map<AlineaRank, OccurrencesBuilder> builderByAlineaMap = new LinkedHashMap<AlineaRank, OccurrencesBuilder>();
        for (int i = 0; i < acceptLength; i++) {
            int operandNumber = acceptOperandArray[i];
            AlineaRank alineaRank = fichePertinence.getBestAlineaRank(operandNumber);
            if (alineaRank == null) {
                continue;
            }
            SubstringPosition substringPosition = fichePertinence.getSubstringPosition(operandNumber);
            OccurrencesBuilder builder = builderByAlineaMap.get(alineaRank);
            if (builder == null) {
                builder = new OccurrencesBuilder(acceptLength);
                builderByAlineaMap.put(alineaRank, builder);
            }
            builder.add(operandNumber, substringPosition);
        }
        int size = builderByAlineaMap.size();
        InAlineaOccurrences[] occurrencesArray = new InAlineaOccurrences[size];
        int p = 0;
        for (Map.Entry<AlineaRank, OccurrencesBuilder> entry : builderByAlineaMap.entrySet()) {
            occurrencesArray[p] = entry.getValue().toInAlineaOccurrences(entry.getKey());
            p++;
        }
        return occurrencesArray;
    }

    public static InLangOccurrences[] getInLangOccurrencesArray(MotclePertinence motclePertinence, int[] acceptOperandArray) {
        List<MotclePertinence.OccurrenceByLang> occurrenceByLangList = motclePertinence.getOccurrenceByLangList();
        int langCount = occurrenceByLangList.size();
        OccurrencesBuilder[] builderArray = new OccurrencesBuilder[langCount];
        Lang[] langArray = new Lang[langCount];
        int acceptLength = acceptOperandArray.length;
        for (int i = 0; i < langCount; i++) {
            MotclePertinence.OccurrenceByLang langOccurrence = occurrenceByLangList.get(i);
            langArray[i] = langOccurrence.getLang();
            for (int j = 0; j < acceptLength; j++) {
                int operandNumber = acceptOperandArray[j];
                SubstringPosition substringPosition = langOccurrence.getSubstringPosition(operandNumber);
                if (substringPosition == null) {
                    continue;
                }
                OccurrencesBuilder builder = builderArray[i];
                if (builder == null) {
                    builder = new OccurrencesBuilder(acceptLength);
                    builderArray[i] = builder;
                }
                builder.add(operandNumber, substringPosition);
            }
        }
        List<InLangOccurrences> list = new ArrayList<InLangOccurrences>(langCount);
        for (int i = 0; i < langCount; i++) {
            Lang lang = langArray[i];
            OccurrencesBuilder builder = builderArray[i];
            if (builder != null) {
                list.add(builder.toInLangOccurrences(lang));
            }
        }
        return list.toArray(new InLangOccurrences[list.size()]);
    }

    public static float getDateScore(FuzzyDate date, int currentYear) {
        if (date == null) {
            return 0;
        }
        int year = date.getYear();
        float x = (float) Math.abs(year - currentYear);
        return 1 - (x / (x + 6));
    }

    public static float getOccurrenceScore(FichePertinence fichePertinence, int[] acceptOperandArray, FieldRankManager fieldRankManager) {
        AlineaRank motclesRank = fieldRankManager.getSpecialAlineaRank("motcles");
        int orderSize = fieldRankManager.getOrderSize();
        int length = acceptOperandArray.length;
        AlineaRank bestAlinea = null;
        int bestBegin = -1;
        int note = 0;
        for (int i = 0; i < length; i++) {
            int operandNumber = acceptOperandArray[i];
            int operandNote = 0;
            AlineaRank alineaRank = fichePertinence.getBestAlineaRank(operandNumber);
            int motcleNote = (int) (15 * fichePertinence.getMotcleScore(operandNumber));
            if (alineaRank != null) {
                if ((bestAlinea == null) || (alineaRank.compareTo(bestAlinea) < 0)) {
                    bestAlinea = alineaRank;
                    bestBegin = fichePertinence.getSubstringPosition(operandNumber).getBeginIndex();
                } else if (alineaRank.equals(bestAlinea)) {
                    if (bestBegin == -1) {
                        bestBegin = fichePertinence.getSubstringPosition(operandNumber).getBeginIndex();
                    } else {
                        bestBegin = Math.min(bestBegin, fichePertinence.getSubstringPosition(operandNumber).getBeginIndex());
                    }
                }
                operandNote = getOperandNote(alineaRank, motcleNote, orderSize);
            } else if (motcleNote > 0) {
                operandNote = (motclesRank.getMainOrder() * 20) - 5 + motcleNote;
                if ((bestAlinea == null) || (bestAlinea.compareTo(motclesRank) > 0)) {
                    bestAlinea = motclesRank;
                    bestBegin = 10;
                }
            }
            note += operandNote;
        }
        if (bestAlinea != null) {
            note += getOperandNote(bestAlinea, 0, orderSize); //On double la note du meilleur champ
            note += getBestIndexPrime(bestBegin);
        }
        int notemaximale = ((20 * orderSize) + 15) * (length + 1) + 4;
        return ((float) note) / notemaximale;
    }

    public static FichePertinence toNoOccurrencePertinence(Integer baseCode, Integer corpusCode, Integer ficheCode, Lang ficheLang) {
        return new NoOccurrenceFichePertinence(ficheCode, corpusCode, baseCode, ficheLang);
    }

    public static float getGlobalSorce(float[] scoreArray) {
        float result = 0;
        for (float score : scoreArray) {
            result += score;
        }
        return result;
    }

    public static int[] round(float[] scoreArray) {
        int length = scoreArray.length;
        int[] result = new int[length];
        for (int i = 0; i < length; i++) {
            result[i] = round(scoreArray[i]);
        }
        return result;
    }

    private static int round(float f) {
        return (Math.round(f * 1000));
    }

    private static int getBestIndexPrime(int bestIndex) {
        if (bestIndex == 0) {
            return 4;
        } else if (bestIndex < 5) {
            return 3;
        } else if (bestIndex < 20) {
            return 2;
        } else if (bestIndex < 50) {
            return 1;
        } else {
            return 0;
        }
    }

    private static int getOperandNote(AlineaRank alineaRank, int motcleNote, int orderSize) {
        int mainOrder = alineaRank.getMainOrder();
        if (mainOrder == 0) {
            return 0;
        }
        int note = (20 * (orderSize - mainOrder + 1));
        return note + motcleNote;
    }


    private static class NoOccurrenceFichePertinence implements FichePertinence {

        private final Integer ficheCode;
        private final Integer corpusCode;
        private final Integer baseCode;
        private final Lang ficheLang;

        private NoOccurrenceFichePertinence(Integer ficheCode, Integer corpusCode, Integer baseCode, Lang ficheLang) {
            this.ficheCode = ficheCode;
            this.corpusCode = corpusCode;
            this.ficheLang = ficheLang;
            this.baseCode = baseCode;
        }

        @Override
        public short getOperandState(int operandNumber) {
            return PertinenceConstants.NO_OCCURRENCE_STATE;
        }

        @Override
        public Integer getFicheCode() {
            return ficheCode;
        }

        @Override
        public Integer getCorpusCode() {
            return corpusCode;
        }

        @Override
        public Integer getBaseCode() {
            return baseCode;
        }

        @Override
        public Lang getFicheLang() {
            return ficheLang;
        }

        @Override
        public AlineaRank getBestAlineaRank(int operandNumber) {
            return null;
        }

        @Override
        public SubstringPosition getSubstringPosition(int operandNumber) {
            return null;
        }

        @Override
        public List<Integer> getAcceptMotcleCodeList() {
            return PrimUtils.EMPTY_LIST;
        }

        @Override
        public float getMotcleScore(int operandNumber) {
            return 0;
        }

    }


}
