/* ScrutariLib_SearchEngine - Copyright (c) 2005-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.17
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine;

import java.util.List;
import java.util.function.Predicate;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.CorpusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.operands.search.AllOperand;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.pertinence.FichePertinence;
import net.scrutari.searchengine.api.pertinence.MotclePertinence;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.SearchTokenReport;
import net.scrutari.searchengine.tools.PredicateFactory;
import net.scrutari.searchengine.tools.lexieseek.LexieSeekEngine;
import net.scrutari.searchengine.tools.operands.search.SearchOperationUtils;
import net.scrutari.searchengine.tools.options.ComputedOptionsEngine;
import net.scrutari.searchengine.tools.pertinence.PertinenceUtils;
import net.scrutari.searchengine.tools.result.FicheSearchResultBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchSourceBuilder;
import net.scrutari.searchengine.tools.result.ResultUtils;


/**
 *
 * @author Vincent Calame
 */
public final class FicheSearchEngine {

    private final static int[] EMPTY_ARRAY = new int[0];

    private FicheSearchEngine() {
    }

    public static FicheSearchResult search(ScrutariDB scrutariDB, SearchOperation searchOperation, SearchOptions searchOptions, GlobalSearchOptions globalSearchOptions, QId qId) {
        FicheSearchResultBuilder builder = new FicheSearchResultBuilder(FicheSearchSourceBuilder.init().setQId(qId).setCanonicalQ(searchOperation.getCanonicalQ()).setSearchOptionsDef(searchOptions.getSearchOptionsDef()).toFicheSearchSource());
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            Operand rootOperand = searchOperation.getRootOperand();
            if (rootOperand instanceof AllOperand) {
                ComputedOptions computedOptions = ComputedOptionsEngine.compute(scrutariDB, dataAccess, searchOperation, searchOptions, true, false);
                builder.setFicheMaximum(computedOptions.getFicheMaximum());
                allFiche(builder, scrutariDB, dataAccess, computedOptions, globalSearchOptions);
            } else {
                ComputedOptions computedOptions = ComputedOptionsEngine.compute(scrutariDB, dataAccess, searchOperation, searchOptions, true, true);
                builder.setFicheMaximum(computedOptions.getFicheMaximum());
                search(builder, scrutariDB, dataAccess, searchOperation, computedOptions, globalSearchOptions);
            }
        }
        return builder.toFicheSearchResult();
    }

    private static void search(FicheSearchResultBuilder builder, ScrutariDB scrutariDB, DataAccess dataAccess, SearchOperation searchOperation, ComputedOptions computedOptions, GlobalSearchOptions globalSearchOptions) {
        int operandMaxNumber = searchOperation.getSearchTokenOperandList().size();
        int[] acceptOperandArray = SearchOperationUtils.toAcceptOperandArray(searchOperation);
        SearchEngine searchEngine = new SearchEngine(operandMaxNumber, acceptOperandArray);
        try (LexieAccess lexieAccess = scrutariDB.openLexieAccess()) {
            LexieSeekEngine.run(lexieAccess, searchOperation, computedOptions, searchEngine);
        }
        if (computedOptions.isOnMotcle()) {
            searchEngine.checkMotclePertinence(dataAccess, computedOptions);
        }
        FicheCollector ficheCollector = new FicheCollector(scrutariDB.getFieldRankManager(), globalSearchOptions, acceptOperandArray, computedOptions, dataAccess);
        Predicate<FichePertinence> fichePertinencePredicate = PredicateFactory.toFichePertinencePredicate(searchOperation);
        if (computedOptions.hasMandatoryOperand()) {
            List<FichePertinence> list = searchEngine.getFichePertinenceList();
            for (FichePertinence fichePertinence : list) {
                if (fichePertinencePredicate.test(fichePertinence)) {
                    FuzzyDate date = dataAccess.getFicheDate(fichePertinence.getFicheCode());
                    ficheCollector.addFiche(fichePertinence, date);
                }
            }
        } else {
            for (CorpusData corpusData : computedOptions.getCorpusDataArray()) {
                for (Integer ficheCode : corpusData.getFicheCodeList()) {
                    if (computedOptions.acceptFiche(ficheCode)) {
                        FichePertinence fichePertinence = searchEngine.getFichePertinence(ficheCode);
                        if (fichePertinence == null) {
                            FuzzyDate date = dataAccess.getFicheDate(ficheCode);
                            Lang ficheLang = dataAccess.getFicheLang(ficheCode);
                            ficheCollector.addFiche(PertinenceUtils.toNoOccurrencePertinence(corpusData.getBaseCode(), corpusData.getCorpusCode(), ficheCode, ficheLang), date);
                        } else if (fichePertinencePredicate.test(fichePertinence)) {
                            FuzzyDate date = dataAccess.getFicheDate(ficheCode);
                            ficheCollector.addFiche(fichePertinence, date);
                        }
                    }
                }
            }
        }
        ficheCollector.populateFicheSearchResult(builder);
        for (MotclePertinence mtclPert : searchEngine.getMotclePertinenceList()) {
            InLangOccurrences[] array = PertinenceUtils.getInLangOccurrencesArray(mtclPert, acceptOperandArray);
            builder.addMotcleSearchResultInfo(ResultUtils.toMotcleSearchResultInfo(mtclPert.getMotcleCode(), mtclPert.getScore(), array));
        }
        for (SearchTokenReport searchTokenReport : searchEngine.getSearchTokenReportList()) {
            builder.addSearchTokenReport(searchTokenReport);
        }
    }

    private static void allFiche(FicheSearchResultBuilder builder, ScrutariDB scrutariDB, DataAccess dataAccess, ComputedOptions computedOptions, GlobalSearchOptions globalSearchOptions) {
        builder.setFicheMaximum(computedOptions.getFicheMaximum());
        FicheCollector ficheCollector = new FicheCollector(scrutariDB.getFieldRankManager(), globalSearchOptions, EMPTY_ARRAY, computedOptions, dataAccess);
        for (CorpusData corpusData : computedOptions.getCorpusDataArray()) {
            for (Integer ficheCode : corpusData.getFicheCodeList()) {
                if (computedOptions.acceptFiche(ficheCode)) {
                    FuzzyDate date = dataAccess.getFicheDate(ficheCode);
                    Lang ficheLang = dataAccess.getFicheLang(ficheCode);
                    ficheCollector.addFiche(PertinenceUtils.toNoOccurrencePertinence(corpusData.getBaseCode(), corpusData.getCorpusCode(), ficheCode, ficheLang), date);
                }
            }
        }
        ficheCollector.populateFicheSearchResult(builder);
    }

}
