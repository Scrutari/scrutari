/* ScrutariLib_SearchEngine - Copyright (c) 2010-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine;

import java.util.List;
import java.util.function.Predicate;
import net.mapeadores.util.logicaloperation.Operand;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.searchengine.api.operands.search.AllOperand;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.pertinence.MotclePertinence;
import net.scrutari.searchengine.api.result.InLangOccurrences;
import net.scrutari.searchengine.api.result.MotcleSearchResult;
import net.scrutari.searchengine.tools.PredicateFactory;
import net.scrutari.searchengine.tools.lexieseek.LexieSeekEngine;
import net.scrutari.searchengine.tools.operands.search.SearchOperationUtils;
import net.scrutari.searchengine.tools.options.ComputedOptionsEngine;
import net.scrutari.searchengine.tools.pertinence.PertinenceUtils;
import net.scrutari.searchengine.tools.result.MotcleSearchResultBuilder;
import net.scrutari.searchengine.tools.result.ResultUtils;


/**
 *
 * @author Vincent Calame
 */
public final class MotcleSearchEngine {


    private MotcleSearchEngine() {

    }

    public static MotcleSearchResult search(ScrutariDB scrutariDB, SearchOperation searchOperation, SearchOptions searchOptions) {
        MotcleSearchResultBuilder builder = new MotcleSearchResultBuilder();
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            Operand rootOperand = searchOperation.getRootOperand();
            if (rootOperand instanceof AllOperand) {
                throw new UnsupportedOperationException("Not implemented yet");
            } else {
                ComputedOptions computedOptions = ComputedOptionsEngine.compute(scrutariDB, dataAccess, searchOperation, searchOptions, false, true);
                search(builder, scrutariDB, dataAccess, searchOperation, computedOptions);
            }
        }
        return builder.toMotcleSearchResult();
    }

    private static void search(MotcleSearchResultBuilder builder, ScrutariDB scrutariDB, DataAccess dataAccess, SearchOperation searchOperation, ComputedOptions computedOptions) {
        int operandMaxNumber = searchOperation.getSearchTokenOperandList().size();
        int[] acceptOperandArray = SearchOperationUtils.toAcceptOperandArray(searchOperation);
        SearchEngine searchEngine = new SearchEngine(operandMaxNumber, acceptOperandArray);
        try (LexieAccess lexieAccess = scrutariDB.openLexieAccess()) {
            LexieSeekEngine.run(lexieAccess, searchOperation, computedOptions, searchEngine);
        }
        Predicate<MotclePertinence> motclePertinencePredicate = PredicateFactory.toMotclePertinencePredicate(searchOperation);
        if (computedOptions.hasMandatoryOperand()) {
            List<MotclePertinence> motclePertinenceList = searchEngine.getMotclePertinenceList();
            for (MotclePertinence motclePertinence : motclePertinenceList) {
                if (motclePertinencePredicate.test(motclePertinence)) {
                    InLangOccurrences[] inLangOccurrenceListArray = PertinenceUtils.getInLangOccurrencesArray(motclePertinence, acceptOperandArray);
                    builder.addMotcleSearchResultInfo(ResultUtils.toMotcleSearchResultInfo(motclePertinence.getMotcleCode(), motclePertinence.getScore(), inLangOccurrenceListArray));
                }
            }
        }
    }


}
