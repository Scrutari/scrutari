/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine.xml;

import java.io.IOException;
import net.mapeadores.util.attr.AttributeUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.logicaloperation.LogicalOperationUtils;
import net.mapeadores.util.logicaloperation.Operand;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchSource;


/**
 *
 * @author Vincent Calame
 */
public class FicheSearchSourceXMLPart extends XMLPart {

    public FicheSearchSourceXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void appendFicheSearchSource(FicheSearchSource ficheSearchSource) throws IOException {
        openTag("fiche-search");
        addSimpleElement("q", ficheSearchSource.getCanonicalQ().toString());
        appendSearchOptionsDef(ficheSearchSource.getSearchOptionsDef());
        closeTag("fiche-search");
    }

    private void appendSearchOptionsDef(SearchOptionsDef searchOptionsDef) throws IOException {
        if (searchOptionsDef.isEmpty()) {
            return;
        }
        startOpenTag("options");
        Lang lang = searchOptionsDef.getLang();
        if (lang != null) {
            addAttribute("lang", lang.toString());
        }
        endOpenTag();
        Langs filterLangs = searchOptionsDef.getFilterLangs();
        if (!filterLangs.isEmpty()) {
            openTag("lang-list");
            for (Lang filterLang : filterLangs) {
                addSimpleElement("lang", filterLang.toString());
            }
            closeTag("lang-list");
        }
        addReduction("base-list", ScrutariDataURI.BASEURI_TYPE, searchOptionsDef);
        addReduction("corpus-list", ScrutariDataURI.CORPUSURI_TYPE, searchOptionsDef);
        addReduction("thesaurus-list", ScrutariDataURI.THESAURUSURI_TYPE, searchOptionsDef);
        addReduction("fiche-list", ScrutariDataURI.FICHEURI_TYPE, searchOptionsDef);
        addReduction("motcle-list", ScrutariDataURI.MOTCLEURI_TYPE, searchOptionsDef);
        Operand ficheEligibilityOperand = searchOptionsDef.getFicheEligibilityOperand();
        if (ficheEligibilityOperand != null) {
            addSimpleElement("fiche-eligibility", LogicalOperationUtils.toInformatiqueString(ficheEligibilityOperand));
        }
        PertinencePonderation pertinencePonderation = searchOptionsDef.getPertinencePonderation();
        if (pertinencePonderation != null) {
            startOpenTag("ponderation");
            addAttribute("occurrence", toPertinenceInt(pertinencePonderation.getOccurrencePonderation()));
            addAttribute("date", toPertinenceInt(pertinencePonderation.getDatePonderation()));
            addAttribute("origin", toPertinenceInt(pertinencePonderation.getOriginPonderation()));
            addAttribute("lang", toPertinenceInt(pertinencePonderation.getLangPonderation()));
            closeEmptyTag();
        }
        AttributeUtils.addAttributes(this, searchOptionsDef.getAttributes());
        closeTag("options");
    }

    private void addReduction(String tagName, short type, SearchOptionsDef searchOptionsDef) throws IOException {
        ListReduction listReduction = searchOptionsDef.getListReduction(type);
        if (listReduction != null) {
            startOpenTag(tagName);
            if (listReduction.isExclude()) {
                addAttribute("mode", "exclude");
            }
            endOpenTag();
            for (ScrutariDataURI uri : listReduction.getScrutariDataURIList()) {
                addSimpleElement("uri", uri.toString());
            }
            closeTag(tagName);
        }
    }

    private int toPertinenceInt(float f) {
        return (int) (f * 100);
    }

}
