/* ScrutariLib_SearchEngine - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.RelSpace;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.PertinencePonderation;
import net.scrutari.searchengine.api.pertinence.FichePertinence;
import net.scrutari.searchengine.tools.pertinence.PertinenceUtils;
import net.scrutari.searchengine.tools.result.FicheSearchResultBuilder;
import net.scrutari.searchengine.tools.result.FicheSearchResultGroupBuilder;


/**
 *
 * @author Vincent Calame
 */
class FicheCollector {

    private final static Comparator<PertinencePack> COMPARATOR = new PertinencePackComparator();
    private final FieldRankManager fieldRankManager;
    private final GlobalSearchOptions globalSearchOptions;
    private final Group defaultGroup;
    private final SortedMap<Integer, Group> groupMap;
    private final FuzzyDate currentDate = FuzzyDate.current();
    private final int currentYear = currentDate.getYear();
    private final int[] acceptOperandArray;
    private final DataAccess dataAccess;
    private final Lang preferredLang;
    private final boolean withAlternate;
    private final Set<Integer> excludeSet = new HashSet<Integer>();
    private final Map<Integer, PertinencePack> packMap = new HashMap<Integer, PertinencePack>();
    private final Set<Integer> baseSet = new HashSet<Integer>();


    FicheCollector(FieldRankManager fieldRankManager, GlobalSearchOptions globalSearchOptions, int[] acceptOperandArray, ComputedOptions computedOptions, DataAccess dataAccess) {
        this.fieldRankManager = fieldRankManager;
        this.globalSearchOptions = globalSearchOptions;
        this.acceptOperandArray = acceptOperandArray;
        this.dataAccess = dataAccess;
        this.preferredLang = computedOptions.getSearchOptions().getSearchOptionsDef().getLang();
        PertinencePonderation pertinencePonderation = computedOptions.getPertinencePonderation();
        if (pertinencePonderation == null) {
            pertinencePonderation = globalSearchOptions.getPertinencePonderation();
        }
        List<Category> categoryList = globalSearchOptions.getCategoryList();
        if (categoryList.isEmpty()) {
            defaultGroup = new Group(0, pertinencePonderation);
            groupMap = null;
        } else {
            defaultGroup = null;
            groupMap = new TreeMap<Integer, Group>(Comparator.reverseOrder());
            for (Category category : categoryList) {
                int rank = category.getCategoryDef().getRank();
                groupMap.put(rank, new Group(rank, pertinencePonderation));
            }
        }
        this.withAlternate = dataAccess.containsRelAttributeFor(RelSpace.ALTERNATE_KEY);
    }

    void addFiche(FichePertinence fichePertinence, FuzzyDate date) {
        Integer ficheCode = fichePertinence.getFicheCode();
        FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
        if (excludeSet.contains(ficheCode)) {
            return;
        }
        if ((withAlternate) && (preferredLang != null)) {
            if (fichePertinence.getFicheLang().equals(preferredLang)) {
                for (Integer otherFicheCode : ficheInfo.getRelAttributeCodeList(RelSpace.ALTERNATE_KEY)) {
                    excludeSet.add(otherFicheCode);
                    packMap.remove(otherFicheCode);
                }
            }
        }
        packMap.put(ficheCode, new PertinencePack(fichePertinence, date));
        baseSet.add(fichePertinence.getBaseCode());
    }

    void populateFicheSearchResult(FicheSearchResultBuilder builder) {
        if (defaultGroup != null) {
            populateUniqueGroup(builder);
        } else {
            populateMultiGroup(builder);
        }
    }

    private void populateUniqueGroup(FicheSearchResultBuilder builder) {
        for (PertinencePack pertinencePack : packMap.values()) {
            defaultGroup.addFiche(pertinencePack);
        }
        int ficheCount = populate(builder, defaultGroup);
        builder.setFicheCount(ficheCount);
    }

    private void populateMultiGroup(FicheSearchResultBuilder builder) {
        int globalFicheCount = 0;
        Map<Integer, Group> groupByCorpusMap = new HashMap<Integer, Group>();
        for (PertinencePack pertinencePack : packMap.values()) {
            Integer corpusCode = pertinencePack.getFichePertinence().getCorpusCode();
            Group group = groupByCorpusMap.get(corpusCode);
            if (group == null) {
                Category category = globalSearchOptions.getCategoryByCorpus(corpusCode);
                group = groupMap.get(category.getCategoryDef().getRank());
                groupByCorpusMap.put(corpusCode, group);
            }
            group.addFiche(pertinencePack);
        }
        for (Group group : groupMap.values()) {
            globalFicheCount += populate(builder, group);
        }
        builder.setFicheCount(globalFicheCount);
    }

    private int populate(FicheSearchResultBuilder builder, Group group) {
        if (group.isEmpty()) {
            return 0;
        }
        List<PertinencePack> list = group.sort();
        int size = list.size();
        FicheSearchResultGroupBuilder groupBuilder = new FicheSearchResultGroupBuilder();
        groupBuilder.setCategoryRank(group.getCategoryRank());
        groupBuilder.ensureInfoCapacity(size);
        for (PertinencePack pertinencePack : list) {
            groupBuilder.addFicheSearchResultInfo(PertinenceUtils.toFicheSearchResultInfo(pertinencePack.getFichePertinence(), acceptOperandArray, PertinenceUtils.round(pertinencePack.scoreArray)));
        }
        builder.addFicheSearchResultGroup(groupBuilder.toFicheSearchResultGroup());
        return size;
    }

    private static void addOriginScore(List<PertinencePack> set, float originPonderation, DataAccess dataAccess) {
        Map<Integer, Integer> avanceMap = new HashMap<Integer, Integer>();
        for (PertinencePack pertinencePack : set) {
            Integer baseCode = pertinencePack.getFichePertinence().getBaseCode();
            Integer avance = avanceMap.get(baseCode);
            float originScore = 0;
            if (avance == null) {
                originScore = originPonderation;
                avanceMap.put(baseCode, 128); //la seconde valeur sera à deux tiers 128 / 192
            } else {
                int newAvance = avance;
                if (newAvance > 1) {
                    originScore = originPonderation * newAvance / 192;
                    newAvance = newAvance / 2;
                    avanceMap.put(baseCode, newAvance);
                }
            }
            if (originScore != 0) {
                pertinencePack.setOriginScore(originScore);
            }
        }
    }


    private class Group {

        private final float originPonderation;
        private final float datePonderation;
        private final float occurrencePonderation;
        private final float langPonderation;
        private final int categoryRank;
        private final List<PertinencePack> pertinencePackList = new ArrayList<PertinencePack>();

        private Group(int categoryRank, PertinencePonderation pertinencePonderation) {
            this.categoryRank = categoryRank;
            this.originPonderation = pertinencePonderation.getOriginPonderation();
            this.datePonderation = pertinencePonderation.getDatePonderation();
            this.occurrencePonderation = pertinencePonderation.getOccurrencePonderation();
            this.langPonderation = pertinencePonderation.getLangPonderation();
        }

        private int getCategoryRank() {
            return categoryRank;
        }

        private boolean isEmpty() {
            return pertinencePackList.isEmpty();
        }

        private void addFiche(PertinencePack pertinencePack) {
            FichePertinence fichePertinence = pertinencePack.getFichePertinence();
            int isPreferredLang = 0;
            if ((preferredLang != null) && (fichePertinence.getFicheLang().equals(preferredLang))) {
                isPreferredLang = 1;
            }
            float[] scoreArray = new float[4];
            scoreArray[0] = occurrencePonderation * PertinenceUtils.getOccurrenceScore(fichePertinence, acceptOperandArray, fieldRankManager);
            scoreArray[1] = datePonderation * PertinenceUtils.getDateScore(pertinencePack.getDate(), currentYear);
            scoreArray[2] = 0;
            scoreArray[3] = isPreferredLang * langPonderation;
            pertinencePack.setScoreArray(scoreArray);
            pertinencePackList.add(pertinencePack);
        }

        private List<PertinencePack> sort() {
            Collections.sort(pertinencePackList, COMPARATOR);
            if ((originPonderation != 0) && (baseSet.size() > 1)) {
                addOriginScore(pertinencePackList, originPonderation, dataAccess);
                Collections.sort(pertinencePackList, COMPARATOR);
            }
            return pertinencePackList;
        }

    }


    private static class PertinencePack {

        private final FichePertinence fichePertinence;
        private final FuzzyDate date;
        private float[] scoreArray;
        private float globalScore;


        private PertinencePack(FichePertinence fichePertinence, FuzzyDate date) {
            this.fichePertinence = fichePertinence;
            this.date = date;
        }

        private FuzzyDate getDate() {
            return date;
        }

        private FichePertinence getFichePertinence() {
            return fichePertinence;
        }

        private void setScoreArray(float[] scoreArray) {
            this.scoreArray = scoreArray;
            this.globalScore = PertinenceUtils.getGlobalSorce(scoreArray);
        }

        private void setOriginScore(float originScore) {
            scoreArray[2] = originScore;
            this.globalScore = PertinenceUtils.getGlobalSorce(scoreArray);
        }

    }


    private static class PertinencePackComparator implements Comparator<PertinencePack> {

        private PertinencePackComparator() {

        }

        @Override
        public int compare(PertinencePack pack1, PertinencePack pack2) {
            if (pack1.globalScore > pack2.globalScore) {
                return -1;
            }
            if (pack1.globalScore < pack2.globalScore) {
                return 1;
            }
            int comp = FuzzyDate.compare(pack1.date, pack2.date);
            if (comp != 0) {
                return -comp;
            }
            int code1 = pack1.getFichePertinence().getFicheCode();
            int code2 = pack2.getFichePertinence().getFicheCode();
            if (code1 > code2) {
                return -1;
            }
            if (code1 < code2) {
                return 1;
            }
            return 0;
        }

    }

}
