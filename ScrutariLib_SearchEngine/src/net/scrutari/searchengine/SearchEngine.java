/* ScrutariLib_SearchEngine - Copyright (c) 2010-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.searchengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.SubstringPosition;
import net.scrutari.data.CorpusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.searchengine.api.lexieseek.LexieSeekHandler;
import net.scrutari.searchengine.api.options.ComputedOptions;
import net.scrutari.searchengine.api.pertinence.FichePertinence;
import net.scrutari.searchengine.api.pertinence.MotclePertinence;
import net.scrutari.searchengine.api.pertinence.PertinenceConstants;
import net.scrutari.searchengine.api.result.SearchTokenReport;


/**
 *
 * @author Vincent Calame
 */
class SearchEngine implements LexieSeekHandler {

    private final static int HERE = 1;
    private final static int NO_PERTINENT = -2;
    private final int operandMaxNumber;
    private final Map<Integer, InternalFichePertinence> fichePertinenceMap = new HashMap<Integer, InternalFichePertinence>();
    private final Map<Integer, InternalMotclePertinence> motclePertinenceMap = new HashMap<Integer, InternalMotclePertinence>();
    private final List<SearchTokenReport> searchTokenReportList = new ArrayList<SearchTokenReport>();
    private List<MotclePertinence> motclePertinenceList = new ArrayList<MotclePertinence>();
    private final int[] acceptOperandArray;

    SearchEngine(int operandMaxNumber, int[] acceptOperandArray) {
        this.operandMaxNumber = operandMaxNumber;
        this.acceptOperandArray = acceptOperandArray;
    }

    @Override
    public void addAlineaOccurrence(Integer ficheCode, Integer corpusCode, Integer baseCode, Lang lang, int operandNumber, AlineaRank alineaRank, SubstringPosition sourcePosition) {
        InternalFichePertinence fichePertinence = fichePertinenceMap.get(ficheCode);
        if (fichePertinence == null) {
            fichePertinence = new InternalFichePertinence(ficheCode, corpusCode, baseCode, lang, operandMaxNumber);
            fichePertinenceMap.put(ficheCode, fichePertinence);
        }
        fichePertinence.addOccurrence(operandNumber, alineaRank, sourcePosition);
    }

    @Override
    public void addLangOccurrence(Integer motcleCode, Integer thesaurusCode, Lang lang, int operandNumber, SubstringPosition sourcePosition, float coverRate) {
        InternalMotclePertinence motclePertinence = (InternalMotclePertinence) motclePertinenceMap.get(motcleCode);
        if (motclePertinence == null) {
            motclePertinence = new InternalMotclePertinence(motcleCode, thesaurusCode, operandMaxNumber);
            motclePertinenceMap.put(motcleCode, motclePertinence);
            motclePertinenceList.add(motclePertinence);
        }
        motclePertinence.addOccurrence(operandNumber, lang, sourcePosition, coverRate);
    }

    @Override
    public void addSearchTokenReport(SearchTokenReport searchTokenReport) {
        searchTokenReportList.add(searchTokenReport);
    }

    FichePertinence getFichePertinence(Integer ficheCode) {
        return fichePertinenceMap.get(ficheCode);
    }

    List<FichePertinence> getFichePertinenceList() {
        List<FichePertinence> list = new ArrayList<FichePertinence>();
        list.addAll(fichePertinenceMap.values());
        return list;
    }

    List<MotclePertinence> getMotclePertinenceList() {
        return motclePertinenceList;
    }

    List<SearchTokenReport> getSearchTokenReportList() {
        return searchTokenReportList;
    }

    void checkMotclePertinence(DataAccess dataAccess, ComputedOptions computedOptions) {
        Lang[] langArray = computedOptions.getLangArray();
        boolean allLang = computedOptions.isAllLang();
        List<MotclePertinence> newMotclePertinenceList = new ArrayList<MotclePertinence>();
        for (InternalMotclePertinence motclePertinence : motclePertinenceMap.values()) {
            float[] motcleNoteArray = motclePertinence.checkMotcleScoreArray(acceptOperandArray);
            Integer motcleCode = motclePertinence.getMotcleCode();
            boolean isUsed = false;
            MotcleInfo motcleInfo = dataAccess.getMotcleInfo(motcleCode);
            for (CorpusData corpusData : computedOptions.getCorpusDataArray()) {
                Integer corpusCode = corpusData.getCorpusCode();
                List<Integer> indexationFicheCodeList = motcleInfo.getIndexationCodeList(corpusCode);
                for (Integer ficheCode : indexationFicheCodeList) {
                    InternalFichePertinence fichePertinence = fichePertinenceMap.get(ficheCode);
                    if (fichePertinence == null) {
                        if (!computedOptions.acceptFiche(ficheCode)) {
                            continue;
                        }
                        boolean add = false;
                        Lang lang = dataAccess.getFicheLang(ficheCode);
                        if (allLang) {
                            add = true;
                        } else {
                            boolean contains = false;
                            int langLength = langArray.length;
                            for (int j = 0; j < langLength; j++) {
                                if (langArray[j].equals(lang)) {
                                    contains = true;
                                    break;
                                }
                            }
                            if (contains) {
                                add = true;
                            }
                        }
                        if (add) {
                            fichePertinence = new InternalFichePertinence(ficheCode, corpusCode, corpusData.getBaseCode(), lang, operandMaxNumber);
                            fichePertinenceMap.put(ficheCode, fichePertinence);
                        } else {
                            continue;
                        }
                    }
                    isUsed = true;
                    fichePertinence.addMotcleScore(motcleNoteArray);
                    if (motclePertinence.isWithAccept()) {
                        fichePertinence.addMotcle(motcleCode);
                    }
                }
            }
            if ((isUsed) && (motclePertinence.isWithAccept())) {
                newMotclePertinenceList.add(motclePertinence);
            }
        }
        this.motclePertinenceList = newMotclePertinenceList;
    }


    private static class InternalFichePertinence implements FichePertinence {

        private final Integer ficheCode;
        private final Integer corpusCode;
        private final Integer baseCode;
        private final Lang ficheLang;
        private final AlineaRank[] alineaRankArray;
        private final boolean[] noPertinentArray;
        private final float[] motcleScoreArray;
        private final SubstringPosition[] substringPositionArray;
        private final List<Integer> motcleCodeList = new ArrayList<Integer>();

        private InternalFichePertinence(Integer ficheCode, Integer corpusCode, Integer baseCode, Lang ficheLang, int operandMaxNumber) {
            this.ficheCode = ficheCode;
            this.corpusCode = corpusCode;
            this.baseCode = baseCode;
            this.ficheLang = ficheLang;
            this.alineaRankArray = new AlineaRank[operandMaxNumber];
            this.noPertinentArray = new boolean[operandMaxNumber];
            this.motcleScoreArray = new float[operandMaxNumber];
            for (int i = 0; i < operandMaxNumber; i++) {
                motcleScoreArray[i] = -1;
            }
            this.substringPositionArray = new SubstringPosition[operandMaxNumber];
        }

        private void addOccurrence(int operandNumber, AlineaRank alineaRank, SubstringPosition sourcePosition) {
            int idx = operandNumber - 1;
            if (sourcePosition == null) {
                noPertinentArray[idx] = true;
                alineaRankArray[idx] = null;
                substringPositionArray[idx] = null;
            } else {
                AlineaRank currentAlineaRank = alineaRankArray[idx];
                if ((currentAlineaRank == null) || (currentAlineaRank.compareTo(alineaRank) > 0)) {
                    alineaRankArray[idx] = alineaRank;
                    substringPositionArray[idx] = sourcePosition;
                } else if (currentAlineaRank.equals(alineaRank)) {
                    if (sourcePosition.getBeginIndex() < substringPositionArray[idx].getBeginIndex()) {
                        substringPositionArray[idx] = sourcePosition;
                    }
                }
            }
        }

        @Override
        public short getOperandState(int operandNumber) {
            int index = operandNumber - 1;
            AlineaRank alineaRank = alineaRankArray[index];
            if (alineaRank != null) {
                return PertinenceConstants.OCCURRENCE_STATE;
            }
            float motcleScore = motcleScoreArray[index];
            if (motcleScore > 0) {
                return PertinenceConstants.OCCURRENCE_STATE;
            }
            if ((noPertinentArray[index]) || (motcleScore < -1)) {
                return PertinenceConstants.NOPERTINENT_STATE;
            }
            return PertinenceConstants.NO_OCCURRENCE_STATE;
        }

        @Override
        public Integer getFicheCode() {
            return ficheCode;
        }

        @Override
        public Integer getCorpusCode() {
            return corpusCode;
        }

        @Override
        public Integer getBaseCode() {
            return baseCode;
        }

        @Override
        public Lang getFicheLang() {
            return ficheLang;
        }

        @Override
        public AlineaRank getBestAlineaRank(int operandNumber) {
            if (noPertinentArray[operandNumber - 1]) {
                return null;
            }
            return alineaRankArray[operandNumber - 1];
        }

        @Override
        public SubstringPosition getSubstringPosition(int operandNumber) {
            return substringPositionArray[operandNumber - 1];
        }

        @Override
        public List<Integer> getAcceptMotcleCodeList() {
            return motcleCodeList;
        }

        @Override
        public float getMotcleScore(int operandNumber) {
            float score = motcleScoreArray[operandNumber - 1];
            if (score < 0) {
                return 0;
            }
            if (score > 1) {
                return 1;
            }
            return score;
        }

        private void addMotcleScore(float[] array) {
            int length = array.length;
            for (int i = 0; i < length; i++) {
                float current = motcleScoreArray[i];
                float newNote = array[i];
                if (newNote > 0) {
                    if ((current < 0) || (newNote > current)) {
                        motcleScoreArray[i] = newNote;
                    }
                } else if (newNote < -1) {
                    if (current < 0) {
                        motcleScoreArray[i] = newNote;
                    }
                }
            }
        }

        private void addMotcle(Integer motcleCode) {
            motcleCodeList.add(motcleCode);
        }

    }


    private static class InternalMotclePertinence implements MotclePertinence {

        private final Integer motcleCode;
        private final Integer thesaurusCode;
        private final int operandMaxNumber;
        private final List<OccurrenceByLang> occurrenceByLangList = new ArrayList<OccurrenceByLang>();
        private final int[] operandStateArray;
        private boolean withAccept;
        private int score;

        private InternalMotclePertinence(Integer motcleCode, Integer thesaurusCode, int operandMaxNumber) {
            this.motcleCode = motcleCode;
            this.thesaurusCode = thesaurusCode;
            this.operandMaxNumber = operandMaxNumber;
            this.operandStateArray = new int[operandMaxNumber];
        }

        @Override
        public Integer getMotcleCode() {
            return motcleCode;
        }

        @Override
        public Integer getThesaurusCode() {
            return thesaurusCode;
        }

        @Override
        public List<OccurrenceByLang> getOccurrenceByLangList() {
            return occurrenceByLangList;
        }

        @Override
        public short getOperandState(int operandNumber) {
            switch (operandStateArray[operandNumber - 1]) {
                case HERE:
                    return PertinenceConstants.OCCURRENCE_STATE;
                case NO_PERTINENT:
                    return PertinenceConstants.NOPERTINENT_STATE;
                default:
                    return PertinenceConstants.NO_OCCURRENCE_STATE;
            }
        }

        private void addOccurrence(int operandNumber, Lang lang, SubstringPosition sourcePosition, float coverRate) {
            int index = operandNumber - 1;
            if (sourcePosition != null) {
                InternalOccurrenceByLang occurrenceByLang = getOrCreateLangOccurrence(lang);
                occurrenceByLang.addPosition(operandNumber, sourcePosition);
                operandStateArray[index] = HERE;
            } else if (operandStateArray[index] == 0) {
                operandStateArray[index] = NO_PERTINENT;
            }
        }

        @Override
        public int getScore() {
            return score;
        }

        private InternalOccurrenceByLang getOrCreateLangOccurrence(Lang lang) {
            int size = occurrenceByLangList.size();
            for (int i = 0; i < size; i++) {
                OccurrenceByLang occurrenceByLang = occurrenceByLangList.get(i);
                if (occurrenceByLang.getLang().equals(lang)) {
                    return (InternalOccurrenceByLang) occurrenceByLang;
                }
            }
            InternalOccurrenceByLang occurrenceByLang = new InternalOccurrenceByLang(lang, operandMaxNumber);
            occurrenceByLangList.add(occurrenceByLang);
            return occurrenceByLang;
        }

        private float[] checkMotcleScoreArray(int[] acceptOperandArray) {
            float[] result = initMotcleScoreArray();
            int note = 0;
            int length = acceptOperandArray.length;
            if (length == 0) {
                withAccept = false;
            } else {
                for (int operandNumber : acceptOperandArray) {
                    if (isHere(operandNumber)) {
                        note++;
                    }
                }
                for (int operandNumber : acceptOperandArray) {
                    if (isHere(operandNumber)) {
                        result[operandNumber - 1] = ((float) note) / length;
                    }
                }
                if (note == 0) {
                    withAccept = false;
                } else {
                    withAccept = true;
                }
            }
            this.score = note;
            return result;
        }

        private float[] initMotcleScoreArray() {
            int operandLength = operandStateArray.length;
            float[] result = new float[operandLength];
            for (int i = 0; i < operandLength; i++) {
                switch (operandStateArray[i]) {
                    case HERE:
                        result[i] = 9;
                        break;
                    case NO_PERTINENT:
                        result[i] = -2;
                        break;
                    default:
                        result[i] = -1;
                        break;
                }
            }
            return result;
        }

        private boolean isHere(int operandNumber) {
            return (operandStateArray[operandNumber - 1] == HERE);
        }

        private boolean isWithAccept() {
            return withAccept;
        }

    }


    private static class InternalOccurrenceByLang implements MotclePertinence.OccurrenceByLang {

        private final Lang lang;
        private final SubstringPosition[] substringPositionArray;

        private InternalOccurrenceByLang(Lang lang, int operandMaxNumber) {
            this.lang = lang;
            this.substringPositionArray = new SubstringPosition[operandMaxNumber];
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public SubstringPosition getSubstringPosition(int operandNumber) {
            return substringPositionArray[operandNumber - 1];
        }

        private void addPosition(int operandNumber, SubstringPosition substringPosition) {
            int index = operandNumber - 1;
            SubstringPosition current = substringPositionArray[index];
            if ((current == null) || (current.getBeginIndex() > substringPosition.getBeginIndex())) {
                substringPositionArray[index] = substringPosition;
            }

        }

    }

}
