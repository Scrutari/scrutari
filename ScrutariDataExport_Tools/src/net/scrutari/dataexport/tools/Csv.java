/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools;

import java.util.ArrayList;
import java.util.List;
import javax.script.Invocable;
import net.mapeadores.util.text.tableparser.CsvParameters;


/**
 *
 * @author Vincent Calame
 */
public class Csv {

    private final List<Col> colList = new ArrayList<Col>();
    private final String name;
    private final CsvParameters csvParameters = new CsvParameters();
    private String file = null;
    private String encoding = "UTF-8";
    private int firstLinesIgnored = 0;

    public Csv(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public CsvParameters getCsvParameters() {
        return csvParameters;
    }

    public int getFirstLinesIgnored() {
        return firstLinesIgnored;
    }

    public void setFirstLinesIgnored(int firstLinesIgnored) {
        if (firstLinesIgnored < 0) {
            firstLinesIgnored = 0;
        }
        this.firstLinesIgnored = firstLinesIgnored;
    }

    public void addCol(Col col) {
        colList.add(col);
    }

    public int getColCount() {
        return colList.size();
    }

    /**
     * Nul si la colonne n'est pas utilisée
     */
    public Col getCol(int index) {
        return colList.get(index);
    }


    public static class Col {

        private final String name;
        private Invocable invocable;
        private char delimiter = ';';

        public Col(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Invocable getInvocable() {
            return invocable;
        }

        public void setInvocable(Invocable invocable) {
            this.invocable = invocable;
        }

        public char getDelimiter() {
            return delimiter;
        }

        public void setDelimiter(char delimiter) {
            this.delimiter = delimiter;
        }

    }

}
