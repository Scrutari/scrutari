/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.download;

import java.io.File;
import java.net.URL;


/**
 *
 * @author Vincent Calame
 */
public class CookieAuth extends Auth {

    private final URL authUrl;
    private final String postQuery;
    private File debugResultFile = null;

    public CookieAuth(URL authUrl, String postQuery) {
        this.authUrl = authUrl;
        this.postQuery = postQuery;
    }

    public URL getAuthUrl() {
        return authUrl;
    }

    public String getPostQuery() {
        return postQuery;
    }

    public File getDebugResultFile() {
        return debugResultFile;
    }

    public void setDebugResultFile(File debugResultFile) {
        this.debugResultFile = debugResultFile;
    }

}
