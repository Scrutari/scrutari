/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.download;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import net.mapeadores.util.io.IOUtils;

/**
 *
 * @author Vincent Calame
 */
public class DownloadEngine {

    private DownloadEngine() {
    }

    public static void run(DownloadConfig downloadConfig, File destinationDirectory) throws IOException {
        if (downloadConfig.isIgnoreUpdate()) {
            boolean updateNeeded = false;
            int csvFileCount = downloadConfig.getCsvFileCount();
            for (int i = 0; i < csvFileCount; i++) {
                File destination = toFile(downloadConfig.getCsvFile(i), destinationDirectory);
                if (!destination.exists()) {
                    updateNeeded = true;
                }
            }
            if (!updateNeeded) {
                return;
            }
        }
        IanBrownCookieManager ianBrownCookieManager = new IanBrownCookieManager();
        Auth auth = downloadConfig.getAuth();
        if (auth instanceof CookieAuth) {
            cookieAuth((CookieAuth) auth, ianBrownCookieManager);
        }
        downloadCsv(downloadConfig, destinationDirectory, ianBrownCookieManager);
    }

    private static void cookieAuth(CookieAuth cookieAuth, IanBrownCookieManager ianBrownCookieManager) throws IOException {
        File debugResultFile = cookieAuth.getDebugResultFile();
        URLConnection connection = cookieAuth.getAuthUrl().openConnection();
        if (debugResultFile == null) {
            ((HttpURLConnection) connection).setInstanceFollowRedirects(false);
        }
        connection.setDoOutput(true);
        ((HttpURLConnection) connection).setRequestMethod("POST");
        OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
        out.write(cookieAuth.getPostQuery());
        out.close();
        ianBrownCookieManager.storeCookies(connection);
        if (debugResultFile != null) {
            InputStream inputStream = connection.getInputStream();
            OutputStream result = new FileOutputStream(debugResultFile);
            IOUtils.copy(inputStream, result);
            inputStream.close();
            result.close();
        }
    }

    private static void downloadCsv(DownloadConfig downloadConfig, File destinationDirectory, IanBrownCookieManager ianBrownCookieManager) throws IOException {
        int csvFileCount = downloadConfig.getCsvFileCount();
        for (int i = 0; i < csvFileCount; i++) {
            CsvFile csvFile = downloadConfig.getCsvFile(i);
            File destination = toFile(csvFile, destinationDirectory);
            if ((downloadConfig.isIgnoreUpdate()) && (destination.exists())) {
                continue;
            }
            OutputStream outputStream = new FileOutputStream(destination);
            URLConnection urlConnection = csvFile.getUrl().openConnection();
            ianBrownCookieManager.setCookies(urlConnection);
            InputStream inputStream = urlConnection.getInputStream();
            IOUtils.copy(inputStream, outputStream);
            outputStream.close();
            inputStream.close();
        }
    }

    private static File toFile(CsvFile csvFile, File destinationDirectory) {
        return new File(destinationDirectory, csvFile.getName() + ".csv");
    }

}
