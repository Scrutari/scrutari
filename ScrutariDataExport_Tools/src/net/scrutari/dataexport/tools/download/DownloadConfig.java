/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.download;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincent Calame
 */
public class DownloadConfig {

    private final List<CsvFile> csvFileList = new ArrayList<CsvFile>();
    private Auth auth = null;
    private boolean ignoreUpdate = false;

    public DownloadConfig() {
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public void addCsvFile(CsvFile csvFile) {
        csvFileList.add(csvFile);
    }

    public int getCsvFileCount() {
        return csvFileList.size();
    }

    public CsvFile getCsvFile(int i) {
        return csvFileList.get(i);
    }

    public boolean isIgnoreUpdate() {
        return ignoreUpdate;
    }

    public void setIgnoreUpdate(boolean ignoreUpdate) {
        this.ignoreUpdate = ignoreUpdate;
    }

}
