/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.download;

import java.net.URL;


/**
 *
 * @author Vincent Calame
 */
public class CsvFile {

    private final String name;
    private final URL url;

    public CsvFile(String name, URL url) {
        this.name = name;
        this.url = url;
    }

    public URL getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

}
