/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.dom;

import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import net.scrutari.dataexport.api.BaseMetadataExport;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class BaseMetadataDOMReader {

    private final DOMLog domLog;

    public BaseMetadataDOMReader(DOMLog domLog) {
        this.domLog = domLog;
    }

    public void readBaseMetadata(BaseMetadataExport baseMetadataExport, Element element) {
        DOMUtils.readChildren(element, new RootHandler(baseMetadataExport));
    }


    private static class RootHandler implements ElementHandler {

        private final BaseMetadataExport baseMetadataExport;

        private RootHandler(BaseMetadataExport baseMetadataExport) {
            this.baseMetadataExport = baseMetadataExport;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("authority-uuid")) {
                String value = DOMUtils.readSimpleElement(element);
                if (value.length() > 0) {
                    baseMetadataExport.setAuthorityUUID(value);
                }
            } else if (tagName.equals("base-name")) {
                String value = DOMUtils.readSimpleElement(element);
                if (value.length() > 0) {
                    baseMetadataExport.setBaseName(value);
                }
            } else if (tagName.equals("base-icon")) {
                String value = DOMUtils.readSimpleElement(element);
                if (value.length() > 0) {
                    baseMetadataExport.setBaseIcon(value);
                }
            } else if (tagName.equals("intitule-short")) {
                DOMUtils.readChildren(element, new IntituleHandler(baseMetadataExport, BaseMetadataExport.INTITULE_SHORT));
            } else if (tagName.equals("intitule-long")) {
                DOMUtils.readChildren(element, new IntituleHandler(baseMetadataExport, BaseMetadataExport.INTITULE_LONG));
            } else if (tagName.equals("langs-ui")) {
                DOMUtils.readChildren(element, new LangUiHandler(baseMetadataExport));
            }
        }

    }


    private static class LangUiHandler implements ElementHandler {

        private final BaseMetadataExport baseMetadataExport;

        private LangUiHandler(BaseMetadataExport baseMetadataExport) {
            this.baseMetadataExport = baseMetadataExport;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lang")) {
                String value = DOMUtils.readSimpleElement(element);
                baseMetadataExport.addLangUI(value);
            }
        }

    }


    private static class IntituleHandler implements ElementHandler {

        private final BaseMetadataExport baseMetadataExport;
        private final int type;

        private IntituleHandler(BaseMetadataExport baseMetadataExport, int type) {
            this.baseMetadataExport = baseMetadataExport;
            this.type = type;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lib")) {
                String lang = element.getAttribute("xml:lang");
                String value = DOMUtils.readSimpleElement(element);
                baseMetadataExport.setIntitule(type, lang, value);
            }
        }

    }

}
