/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.dom;

import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import net.scrutari.dataexport.api.CorpusMetadataExport;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class CorpusDOMReader {

    private final DOMLog domLog;

    public CorpusDOMReader(DOMLog domLog) {
        this.domLog = domLog;
    }

    public void readMetadata(CorpusMetadataExport corpusMetadataExport, Element element) {
        DOMUtils.readChildren(element, new RootHandler(corpusMetadataExport));
    }


    private static class RootHandler implements ElementHandler {

        private final CorpusMetadataExport corpusMetadataExport;

        private RootHandler(CorpusMetadataExport corpusMetadataExport) {
            this.corpusMetadataExport = corpusMetadataExport;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("intitule-corpus")) {
                DOMUtils.readChildren(element, new IntituleHandler(corpusMetadataExport, CorpusMetadataExport.INTITULE_CORPUS));
            } else if (tagName.equals("intitule-fiche")) {
                DOMUtils.readChildren(element, new IntituleHandler(corpusMetadataExport, CorpusMetadataExport.INTITULE_FICHE));
            } else if (tagName.equals("href-parent")) {
                String value = DOMUtils.readSimpleElement(element);
                corpusMetadataExport.setHrefParent(value);
            } else if (tagName.equals("corpus-icon")) {
                String value = DOMUtils.readSimpleElement(element);
                corpusMetadataExport.setCorpusIcon(value);
            } else if (tagName.equals("complement-metadata")) {
                DOMUtils.readChildren(element, new ComplementMetadataHandler(corpusMetadataExport));
            }
        }

    }


    private static class IntituleHandler implements ElementHandler {

        private final CorpusMetadataExport corpusMetadataExport;
        private final int type;

        private IntituleHandler(CorpusMetadataExport corpusMetadataExport, int type) {
            this.corpusMetadataExport = corpusMetadataExport;
            this.type = type;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lib")) {
                String lang = element.getAttribute("xml:lang");
                String value = DOMUtils.readSimpleElement(element);
                corpusMetadataExport.setIntitule(type, lang, value);
            }
        }

    }


    private static class ComplementMetadataHandler implements ElementHandler {

        private final CorpusMetadataExport corpusMetadataExport;
        private final int number;

        private ComplementMetadataHandler(CorpusMetadataExport corpusMetadataExport) {
            this.corpusMetadataExport = corpusMetadataExport;
            this.number = corpusMetadataExport.addComplement();
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lib")) {
                String lang = element.getAttribute("xml:lang");
                String value = DOMUtils.readSimpleElement(element);
                corpusMetadataExport.setComplementIntitule(number, lang, value);
            }
        }

    }

}
