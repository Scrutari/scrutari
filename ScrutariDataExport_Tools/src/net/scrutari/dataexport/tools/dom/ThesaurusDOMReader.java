/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.dom;

import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import net.scrutari.dataexport.api.MotcleExport;
import net.scrutari.dataexport.api.ScrutariDataExport;
import net.scrutari.dataexport.api.ThesaurusMetadataExport;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusDOMReader {

    private final DOMLog domLog;

    public ThesaurusDOMReader(DOMLog domLog) {
        this.domLog = domLog;
    }

    public void readThesaurus(ScrutariDataExport scrutariDataExport, Element element) {
        String thesaurusName = element.getAttribute("thesaurus-name");
        if (thesaurusName.length() == 0) {
            domLog.addMessage(DOMLog.MISSING_ATTRIBUTE, "thesaurus-name");
            return;
        }
        ThesaurusMetadataExport thesaurusMetadataExport = scrutariDataExport.newThesaurus(thesaurusName);
        NodeList childNodes = element.getChildNodes();
        int count = childNodes.getLength();
        for (int i = 0; i < count; i++) {
            Node node = childNodes.item(i);
            if (node instanceof Element) {
                Element el = (Element) node;
                String tagName = el.getTagName();
                if (tagName.equals("thesaurus-metadata")) {
                    DOMUtils.readChildren(el, new ThesaurusMetadataHandler(thesaurusMetadataExport));
                    break;
                }
            }
        }
        DOMUtils.readChildren(element, new MotsclesRootHandler(scrutariDataExport, domLog));
    }

    public void readMetadata(ThesaurusMetadataExport thesaurusMetadataExport, Element element) {
        DOMUtils.readChildren(element, new ThesaurusMetadataHandler(thesaurusMetadataExport));
    }


    private static class MotsclesRootHandler implements ElementHandler {

        private final ScrutariDataExport scrutariDataExport;
        private final DOMLog domLog;

        private MotsclesRootHandler(ScrutariDataExport scrutariDataExport, DOMLog domLog) {
            this.scrutariDataExport = scrutariDataExport;
            this.domLog = domLog;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("motcle")) {
                String motcleId = element.getAttribute("motcle-id");
                if (motcleId.length() == 0) {
                    domLog.addMessage(DOMLog.MISSING_ATTRIBUTE, "motcle-id");
                } else {
                    MotcleExport motcleExport = scrutariDataExport.newMotcle(motcleId);
                    DOMUtils.readChildren(element, new MotcleHandler(motcleExport));
                }
            }
        }

    }


    private static class ThesaurusMetadataHandler implements ElementHandler {

        private final ThesaurusMetadataExport thesaurusMetadataExport;

        private ThesaurusMetadataHandler(ThesaurusMetadataExport thesaurusMetadataExport) {
            this.thesaurusMetadataExport = thesaurusMetadataExport;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("intitule-thesaurus")) {
                DOMUtils.readChildren(element, new IntituleHandler(thesaurusMetadataExport, ThesaurusMetadataExport.INTITULE_THESAURUS));
            }
        }

    }


    private static class IntituleHandler implements ElementHandler {

        private final ThesaurusMetadataExport thesaurusMetadataExport;
        private final int type;

        private IntituleHandler(ThesaurusMetadataExport thesaurusMetadataExport, int type) {
            this.thesaurusMetadataExport = thesaurusMetadataExport;
            this.type = type;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lib")) {
                String lang = element.getAttribute("xml:lang");
                String value = DOMUtils.readSimpleElement(element);
                thesaurusMetadataExport.setIntitule(type, lang, value);
            }
        }

    }


    private static class MotcleHandler implements ElementHandler {

        private final MotcleExport motcleExport;

        private MotcleHandler(MotcleExport motcleExport) {
            this.motcleExport = motcleExport;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lib")) {
                String lang = element.getAttribute("xml:lang");
                String value = DOMUtils.readSimpleElement(element);
                motcleExport.setLibelle(lang, value);
            }
        }

    }

}
