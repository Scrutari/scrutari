/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.dom;

import java.text.ParseException;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.conditions.Condition;
import net.mapeadores.util.conditions.ConditionsXMLStorage;
import net.mapeadores.util.exceptions.NestedScriptException;
import net.mapeadores.util.text.tableparser.CsvParameters;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import net.scrutari.dataexport.tools.BaseConversion;
import net.scrutari.dataexport.tools.CorpusConversion;
import net.scrutari.dataexport.tools.Csv;
import net.scrutari.dataexport.tools.Csv.Col;
import net.scrutari.dataexport.tools.matching.AttributeValues;
import net.scrutari.dataexport.tools.matching.ColValue;
import net.scrutari.dataexport.tools.matching.FieldConcatenation;
import net.scrutari.dataexport.tools.matching.IndexationTokens;
import org.w3c.dom.Element;

/**
 *
 * @author Vincent Calame
 */
public class BaseConversionDOMReader {

    private final static ScriptEngineManager SCRIPT_ENGINE_MANAGER = new ScriptEngineManager();
    private final DOMLog domLog;

    public BaseConversionDOMReader(DOMLog domLog) {
        this.domLog = domLog;
    }

    public void readBaseConversion(BaseConversion baseConversion, Element element) {
        DOMUtils.readChildren(element, new RootHandler(baseConversion));
    }

    private static class RootHandler implements ElementHandler {

        private final BaseConversion baseConversion;

        private RootHandler(BaseConversion baseConversion) {
            this.baseConversion = baseConversion;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("base-metadata")) {
                baseConversion.setBaseMetadataElement(element);
            } else if (tagName.equals("thesaurus")) {
                baseConversion.addThesaurusElement(element);
            } else if (tagName.equals("corpus-conversion")) {
                CorpusConversion corpusConversion = readCorpusConversion(element);
                if (corpusConversion != null) {
                    baseConversion.addCorpusConversion(corpusConversion);
                }
            } else if (tagName.equals("csv")) {
                Csv csv = readCsv(element);
                if (csv != null) {
                    baseConversion.addCsv(csv);
                }
            }
        }

        private CorpusConversion readCorpusConversion(Element element) {
            String corpusName = element.getAttribute("corpus-name");
            if (corpusName.length() == 0) {
                return null;
            }
            CorpusConversion corpusConversion = new CorpusConversion(corpusName);
            DOMUtils.readChildren(element, new CorpusConversionHandler(corpusConversion));
            if (corpusConversion.getMetadataElement() == null) {
                return null;
            } else {
                return corpusConversion;
            }
        }

        private Csv readCsv(Element element) {
            String name = element.getAttribute("name");
            if (name.length() == 0) {
                return null;
            }
            Csv csv = new Csv(name);
            String file = element.getAttribute("file");
            if (file.length() > 0) {
                csv.setFile(file);
            }
            String encoding = element.getAttribute("encoding");
            if (encoding.length() > 0) {
                csv.setEncoding(encoding);
            }
            CsvParameters csvParameters = csv.getCsvParameters();
            String separator = element.getAttribute("delimiter");
            if (separator.length() > 0) {
                csvParameters.setDelimiter(separator.charAt(0));
            }
            String firstLinesIgnored = element.getAttribute("first-lines-ignored");
            if (firstLinesIgnored.length() > 0) {
                try {
                    int fli = Integer.parseInt(firstLinesIgnored);
                    csv.setFirstLinesIgnored(fli);
                } catch (NumberFormatException nfe) {
                }
            }
            DOMUtils.readChildren(element, new CsvHandler(csv));
            return csv;
        }

    }

    private static class CsvHandler implements ElementHandler {

        private final Csv csv;

        private CsvHandler(Csv csv) {
            this.csv = csv;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("col")) {
                String name = element.getAttribute("name");
                if (name.length() == 0) {
                    csv.addCol(null);
                } else {
                    Csv.Col col = new Csv.Col(name);
                    String delimiter = element.getAttribute("delimiter");
                    if (delimiter.length() > 0) {
                        col.setDelimiter(delimiter.charAt(0));
                    }
                    DOMUtils.readChildren(element, new ColHandler(col));
                    csv.addCol(col);
                }

            }
        }

    }

    private static class CorpusConversionHandler implements ElementHandler {

        private final CorpusConversion corpusConversion;

        private CorpusConversionHandler(CorpusConversion corpusConversion) {
            this.corpusConversion = corpusConversion;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("corpus-metadata")) {
                corpusConversion.setMetadataElement(element);
            } else if (tagName.equals("source")) {
                String name = element.getAttribute("name");
                if (name.length() > 0) {
                    CorpusConversion.Source source = new CorpusConversion.Source(name);
                    DOMUtils.readChildren(element, new SourceHandler(source));
                    corpusConversion.addSource(source);
                }
            }
        }

    }

    private static class ColHandler implements ElementHandler {

        private final Col col;

        private ColHandler(Col col) {
            this.col = col;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("script")) {
                String script = DOMUtils.readSimpleElement(element);
                ScriptEngine scriptEngine = SCRIPT_ENGINE_MANAGER.getEngineByName("javascript");
                try {
                    scriptEngine.eval(script);
                    col.setInvocable((Invocable) scriptEngine);
                } catch (ScriptException see) {
                    throw new NestedScriptException(see);
                }
            }
        }

    }

    private static class SourceHandler implements ElementHandler {

        private final CorpusConversion.Source source;

        private SourceHandler(CorpusConversion.Source source) {
            this.source = source;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("col-condition")) {
                String colName = element.getAttribute("col-name");
                if (colName.length() > 0) {
                    Condition condition = ConditionsXMLStorage.getCondition(element);
                    CorpusConversion.ColCondition colCondition = new CorpusConversion.ColCondition(colName, condition);
                    source.addColCondition(colCondition);
                }
            } else if (tagName.equals("field-concatenation")) {
                String fieldName = element.getAttribute("field-name");
                if (fieldName.length() > 0) {
                    FieldConcatenation fieldConcatenation = new FieldConcatenation(fieldName);
                    DOMUtils.readChildren(element, new FieldConcatenationHandler(fieldConcatenation));
                    source.addFieldConcatenation(fieldConcatenation);
                }
            } else if (tagName.equals("attribute-values")) {
                String attributeKeyString = element.getAttribute("attribute-key");
                if (attributeKeyString.length() > 0) {
                    try {
                        AttributeKey attributeKey = AttributeKey.parse(attributeKeyString);
                        AttributeValues attributeValues = new AttributeValues(attributeKey);
                        DOMUtils.readChildren(element, new AttributeValuesHandler(attributeValues));
                        source.addAttributeValues(attributeValues);
                    } catch (ParseException pe) {
                    }
                }
            } else if (tagName.equals("indexation-tokens")) {
                String thesaurusName = element.getAttribute("thesaurus-name");
                if (thesaurusName.length() > 0) {
                    String matchType = element.getAttribute("match-type");
                    if (IndexationTokens.isValidMatchType(matchType)) {
                        IndexationTokens indexationTokens = new IndexationTokens(thesaurusName, matchType);
                        DOMUtils.readChildren(element, new IndexationTokensHandler(indexationTokens));
                        source.addIndexationTokens(indexationTokens);
                    }
                }
            }
        }

    }

    private static class FieldConcatenationHandler implements ElementHandler {

        private final FieldConcatenation fieldConcatenation;

        private FieldConcatenationHandler(FieldConcatenation fieldConcatenation) {
            this.fieldConcatenation = fieldConcatenation;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("col-value")) {
                String colName = element.getAttribute("col-name");
                if (colName.length() > 0) {
                    ColValue colValue = new ColValue(colName);
                    String separator = element.getAttribute("delimiter");
                    if (separator.length() > 0) {
                        colValue.setDelimiter(separator.charAt(0));
                    }
                    fieldConcatenation.addConcatPart(colValue);
                }
            }
        }

    }

    private static class AttributeValuesHandler implements ElementHandler {

        private final AttributeValues attributeValues;

        private AttributeValuesHandler(AttributeValues attributeValues) {
            this.attributeValues = attributeValues;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("col-value")) {
                String colName = element.getAttribute("col-name");
                if (colName.length() > 0) {
                    ColValue colValue = new ColValue(colName);
                    String separator = element.getAttribute("delimiter");
                    if (separator.length() > 0) {
                        colValue.setDelimiter(separator.charAt(0));
                    }
                    attributeValues.addColValue(colValue);
                }
            }
        }

    }

    private static class IndexationTokensHandler implements ElementHandler {

        private final IndexationTokens indexationTokens;

        private IndexationTokensHandler(IndexationTokens indexationTokens) {
            this.indexationTokens = indexationTokens;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("col-value")) {
                String colName = element.getAttribute("col-name");
                if (colName.length() > 0) {
                    ColValue colValue = new ColValue(colName);
                    String separator = element.getAttribute("delimiter");
                    if (separator.length() > 0) {
                        colValue.setDelimiter(separator.charAt(0));
                    }
                    indexationTokens.addColValue(colValue);
                }
            }
        }

    }

}
