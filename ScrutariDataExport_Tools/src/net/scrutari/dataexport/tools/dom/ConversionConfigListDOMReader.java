/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.dom;

import java.io.File;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import net.scrutari.dataexport.tools.app.ConversionConfigList;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ConversionConfigListDOMReader {

    public ConversionConfigListDOMReader() {
    }

    public void readConversionConfigList(ConversionConfigList conversionConfigList, File parentDir, Element element) {
        DOMUtils.readChildren(element, new RootHandler(conversionConfigList, parentDir));
    }


    private static class RootHandler implements ElementHandler {

        private final ConversionConfigList conversionConfigList;
        private final File parentDir;

        private RootHandler(ConversionConfigList conversionConfigList, File parentDir) {
            this.conversionConfigList = conversionConfigList;
            this.parentDir = parentDir;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("conversion")) {
                String destinationName = element.getAttribute("destination-name");
                String configFileString = element.getAttribute("config-file");
                if ((destinationName.length() > 0) && (destinationName.length() > 0)) {
                    File configFile = new File(parentDir, configFileString);
                    conversionConfigList.add(configFile, destinationName);
                }
            }
        }

    }

}
