/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.dom;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import net.scrutari.dataexport.tools.download.CookieAuth;
import net.scrutari.dataexport.tools.download.CsvFile;
import net.scrutari.dataexport.tools.download.DownloadConfig;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class DownloadConfigDOMReader {

    public DownloadConfigDOMReader() {
    }

    public void readDownloadConfig(DownloadConfig downloadConfig, File parentDir, Element element) {
        DOMUtils.readChildren(element, new RootHandler(downloadConfig, parentDir));
    }


    private static class RootHandler implements ElementHandler {

        private final DownloadConfig downloadConfig;
        private final File parentDir;

        private RootHandler(DownloadConfig downloadConfig, File parentDir) {
            this.downloadConfig = downloadConfig;
            this.parentDir = parentDir;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("cookie-auth")) {
                String urlString = element.getAttribute("url");
                String post = element.getAttribute("post");
                String debugResultFileString = element.getAttribute("debug-result-file");
                if ((post.length() > 0) && (urlString.length() > 0)) {
                    try {
                        URL url = new URL(urlString);
                        CookieAuth cookieAuth = new CookieAuth(url, post);
                        if (debugResultFileString.length() > 0) {
                            cookieAuth.setDebugResultFile(new File(parentDir, debugResultFileString));
                        }
                        downloadConfig.setAuth(cookieAuth);
                    } catch (MalformedURLException mue) {
                    }
                }
            } else if (tagName.equals("csv-file")) {
                String urlString = element.getAttribute("url");
                String name = element.getAttribute("name");
                if ((name.length() > 0) && (urlString.length() > 0)) {
                    try {
                        URL url = new URL(urlString);
                        CsvFile csvFile = new CsvFile(name, url);
                        downloadConfig.addCsvFile(csvFile);
                    } catch (MalformedURLException mue) {
                    }
                }
            }
        }

    }

}
