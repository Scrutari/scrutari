/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.matching;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincent Calame
 */
public class IndexationTokens {

    public static final String FICHELANG_MATCHTYPE = "fiche-lang";
    public static final String MOTCLEID_MATCHTYPE = "motcle-id";
    private final String thesaurusName;
    private final String matchType;
    private final List<ColValue> colValueList = new ArrayList<ColValue>();

    public IndexationTokens(String thesaurusName, String matchType) {
        this.thesaurusName = thesaurusName;
        this.matchType = matchType;
    }

    public String getThesaurusName() {
        return thesaurusName;
    }

    public String getMatchType() {
        return matchType;
    }

    public int getColValueCount() {
        return colValueList.size();
    }

    public ColValue getColValue(int i) {
        return colValueList.get(i);
    }

    public void addColValue(ColValue colValue) {
        colValueList.add(colValue);
    }

    public static boolean isValidMatchType(String matchType) {
        if (matchType.equals(FICHELANG_MATCHTYPE)) {
            return true;
        }
        if (matchType.equals(MOTCLEID_MATCHTYPE)) {
            return true;
        }
        return false;
    }

}
