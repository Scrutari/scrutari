/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.matching;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vincent Calame
 */
public class FieldConcatenation {

    public final static String COMPLEMENT_PREFIX = "complement_";
    public final static String FICHE_ID = "fiche-id";
    public final static String TITRE = "titre";
    public final static String SOUSTITRE = "soustitre";
    public final static String HREF = "href";
    public final static String LANG = "lang";
    public final static String DATE = "date";
    private final String fieldName;
    private final List<Object> concatPartList = new ArrayList<Object>();

    public FieldConcatenation(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldName() {
        return fieldName;
    }

    public int getConcatPartCount() {
        return concatPartList.size();
    }

    public Object getConcatPart(int i) {
        return concatPartList.get(i);
    }

    public void addConcatPart(Object concatPart) {
        concatPartList.add(concatPart);
    }

    public static boolean isFieldName(String fieldName) {
        if (fieldName.equals(FICHE_ID)) {
            return true;
        }
        if (fieldName.equals(TITRE)) {
            return true;
        }
        if (fieldName.equals(SOUSTITRE)) {
            return true;
        }
        if (fieldName.equals(HREF)) {
            return true;
        }
        if (fieldName.equals(LANG)) {
            return true;
        }
        if (fieldName.equals(DATE)) {
            return true;
        }
        return fieldName.startsWith(COMPLEMENT_PREFIX);
    }

}
