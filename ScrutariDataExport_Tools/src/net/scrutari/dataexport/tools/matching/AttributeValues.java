/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.matching;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;

/**
 *
 * @author Vincent Calame
 */
public class AttributeValues {

    private final AttributeKey attributeKey;
    private final List<ColValue> colValueList = new ArrayList<ColValue>();

    public AttributeValues(AttributeKey attributeKey) {
        this.attributeKey = attributeKey;
    }

    public AttributeKey getAttributeKey() {
        return attributeKey;
    }

    public int getColValueCount() {
        return colValueList.size();
    }

    public ColValue getColValue(int i) {
        return colValueList.get(i);
    }

    public void addColValue(ColValue colValue) {
        colValueList.add(colValue);
    }

}
