/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.matching;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.attr.AttributeKey;
import net.scrutari.dataexport.tools.CorpusConversion;
import net.scrutari.dataexport.tools.NamedColManager;

/**
 *
 * @author Vincent Calame
 */
public class MatchingManager {

    private final Map<String, FieldConcatenation> fieldConcatenationMap = new HashMap<String, FieldConcatenation>();
    private final List<AttributeValues> attributeValuesList = new ArrayList<AttributeValues>();
    private final List<IndexationTokens> indexationTokensList = new ArrayList<IndexationTokens>();

    private int complementMaxNumber = 0;

    private MatchingManager() {
    }

    public FieldConcatenation getFieldConcatenation(String fieldName) {
        return fieldConcatenationMap.get(fieldName);
    }

    public int getComplementMaxNumber() {
        return complementMaxNumber;
    }

    public int getAttributeValuesCount() {
        return attributeValuesList.size();
    }

    public AttributeValues getAttributeValues(int i) {
        return attributeValuesList.get(i);
    }

    public int getIndexationTokensCount() {
        return indexationTokensList.size();
    }

    public IndexationTokens getIndexationTokens(int i) {
        return indexationTokensList.get(i);
    }

    public static MatchingManager build(CorpusConversion.Source source, NamedColManager namedColManager) {
        MatchingManager matchingManager = new MatchingManager();
        matchingManager.init(source, namedColManager);
        return matchingManager;
    }

    private void init(CorpusConversion.Source source, NamedColManager namedColManager) {
        Map<AttributeKey, AttributeValues> attributeValuesMap = new HashMap<AttributeKey, AttributeValues>();
        int fieldConcatenationcount = source.getFieldConcatenationCount();
        for (int i = 0; i < fieldConcatenationcount; i++) {
            FieldConcatenation fieldConcatenation = source.getFieldConcatenation(i);
            putFieldConcatenation(fieldConcatenation);
        }
        int attributeValuesCount = source.getAttributeValuesCount();
        for (int i = 0; i < attributeValuesCount; i++) {
            AttributeValues attributeValues = source.getAttributeValues(i);
            attributeValuesList.add(attributeValues);
            attributeValuesMap.put(attributeValues.getAttributeKey(), attributeValues);
        }
        int indexationTokensCount = source.getIndexationTokensCount();
        for (int i = 0; i < indexationTokensCount; i++) {
            indexationTokensList.add(source.getIndexationTokens(i));
        }
        int namedColCount = namedColManager.getNamedColCount();
        for (int i = 0; i < namedColCount; i++) {
            String colName = namedColManager.getColName(i);
            ColValue colValue = new ColValue(colName);
            if (FieldConcatenation.isFieldName(colName)) {
                FieldConcatenation current = fieldConcatenationMap.get(colName);
                if (current != null) {
                    current.addConcatPart(colValue);
                } else {
                    FieldConcatenation newFieldConcatenation = new FieldConcatenation(colName);
                    newFieldConcatenation.addConcatPart(colValue);
                    putFieldConcatenation(newFieldConcatenation);
                }
            } else if (colName.indexOf(":") > 0) {
                try {
                    AttributeKey attributeKey = AttributeKey.parse(colName);
                    AttributeValues current = attributeValuesMap.get(attributeKey);
                    if (current != null) {
                        current.addColValue(colValue);
                    } else {
                        AttributeValues attributeValues = new AttributeValues(attributeKey);
                        attributeValues.addColValue(colValue);
                        attributeValuesList.add(attributeValues);
                        attributeValuesMap.put(attributeKey, attributeValues);
                    }
                } catch (ParseException pe) {
                }
            }
        }
    }

    private void putFieldConcatenation(FieldConcatenation fieldConcatenation) {
        String fieldName = fieldConcatenation.getFieldName();
        fieldConcatenationMap.put(fieldConcatenation.getFieldName(), fieldConcatenation);
        if (fieldName.startsWith(FieldConcatenation.COMPLEMENT_PREFIX)) {
            try {
                int number = Integer.parseInt(fieldName.substring(FieldConcatenation.COMPLEMENT_PREFIX.length()));
                complementMaxNumber = Math.max(complementMaxNumber, number);
            } catch (NumberFormatException nfe) {
            }
        }
    }

}
