/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.matching;

/**
 *
 * @author Vincent Calame
 */
public class ColValue {

    private final String colName;
    private char delimiter = ',';

    public ColValue(String colName) {
        this.colName = colName;
    }

    public String getColName() {
        return colName;
    }

    public char getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(char delimiter) {
        this.delimiter = delimiter;
    }

}
