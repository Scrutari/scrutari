/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.thesaurus;

import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.ElementHandler;
import org.w3c.dom.Element;

/**
 *
 * @author Vincent Calame
 */
public class ThesaurusBufferDOMReader {

    public ThesaurusBufferDOMReader() {

    }

    public ThesaurusBuffer readThesaurus(Element element) {
        String thesaurusName = element.getAttribute("thesaurus-name");
        if (thesaurusName.length() == 0) {
            return null;
        }
        ThesaurusBuffer thesaurusBuffer = new ThesaurusBuffer(thesaurusName);
        DOMUtils.readChildren(element, new MotsclesRootHandler(thesaurusBuffer));
        return thesaurusBuffer;
    }

    private static class MotsclesRootHandler implements ElementHandler {

        private final ThesaurusBuffer thesaurusBuffer;

        private MotsclesRootHandler(ThesaurusBuffer thesaurusBuffer) {
            this.thesaurusBuffer = thesaurusBuffer;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("motcle")) {
                String motcleId = element.getAttribute("motcle-id");
                if (motcleId.length() != 0) {
                    DOMUtils.readChildren(element, new MotcleHandler(motcleId, thesaurusBuffer));
                }
            }
        }

    }

    private static class MotcleHandler implements ElementHandler {

        private final String motcleId;
        private final ThesaurusBuffer thesaurusBuffer;

        private MotcleHandler(String motcleId, ThesaurusBuffer thesaurusBuffer) {
            this.motcleId = motcleId;
            this.thesaurusBuffer = thesaurusBuffer;
        }

        @Override
        public void readElement(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("lib")) {
                String lang = element.getAttribute("xml:lang");
                String value = DOMUtils.readSimpleElement(element);
                thesaurusBuffer.putMotcleId(lang, value, motcleId);
            }
        }

    }

}
