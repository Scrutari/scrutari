/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.thesaurus;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Vincent Calame
 */
public class ThesaurusBuffer {

    private final Map<String, LangBuffer> langMap = new HashMap<String, LangBuffer>();
    private final String thesaurusName;

    public ThesaurusBuffer(String thesaurusName) {
        this.thesaurusName = thesaurusName;
    }

    public String getThesaurusName() {
        return thesaurusName;
    }

    public void putMotcleId(String lang, String libelle, String motcleId) {
        LangBuffer langBuffer = langMap.get(lang);
        if (langBuffer == null) {
            langBuffer = new LangBuffer();
            langMap.put(lang, langBuffer);
        }
        langBuffer.putMotcleId(libelle, motcleId);
    }

    public String getMotcleId(String lang, String libelle) {
        LangBuffer langBuffer = langMap.get(lang);
        if (langBuffer == null) {
            return null;
        }
        return langBuffer.getMotcleId(libelle);
    }

    private static class LangBuffer {

        private final Map<String, String> libelleMap = new HashMap<String, String>();

        private LangBuffer() {

        }

        private String getMotcleId(String libelle) {
            return libelleMap.get(libelle);
        }

        private void putMotcleId(String libelle, String motcleId) {
            libelleMap.put(libelle, motcleId);
        }

    }

}
