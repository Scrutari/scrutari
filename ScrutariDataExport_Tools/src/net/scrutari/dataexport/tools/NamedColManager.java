/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools;

import java.util.ArrayList;
import java.util.List;
import javax.script.Invocable;
import javax.script.ScriptException;
import net.mapeadores.util.exceptions.NestedNoSuchMethodException;
import net.mapeadores.util.exceptions.NestedScriptException;
import net.mapeadores.util.text.tableparser.RowChecker;


/**
 *
 * @author Vincent Calame
 */
public class NamedColManager {

    public final static String COMPLEMENT_PREFIX = "complement_";
    private final Csv csv;
    private final NamedCol[] namedColArray;

    private NamedColManager(Csv csv, NamedCol[] namedColArray) {
        this.csv = csv;
        this.namedColArray = namedColArray;
    }

    public static NamedColManager build(Csv csv) {
        int colCount = csv.getColCount();
        List<NamedCol> namedColList = new ArrayList<NamedCol>();
        for (int i = 0; i < colCount; i++) {
            Csv.Col col = csv.getCol(i);
            if (col != null) {
                namedColList.add(new NamedCol(i, col, namedColList.size()));
            }
        }
        return new NamedColManager(csv, namedColList.toArray(new NamedCol[namedColList.size()]));
    }

    public RowChecker getRowChecker() {
        return new InternalRowChecker(namedColArray, csv.getFirstLinesIgnored());
    }

    public String getValue(String[] row, String colName) {
        int index = getNewIndex(colName);
        if (index == -1) {
            return null;
        }
        if (index > (row.length - 1)) {
            return null;
        }
        return row[index];
    }

    public int getNewIndex(String colName) {
        int length = namedColArray.length;
        for (int i = 0; i < length; i++) {
            NamedCol column = namedColArray[i];
            if (column.col.getName().equals(colName)) {
                return column.newIndex;
            }
        }
        return - 1;
    }

    public int getNamedColCount() {
        return namedColArray.length;
    }

    public String getColName(int index) {
        return namedColArray[index].col.getName();
    }


    private static class NamedCol {

        private final int originalIndex;
        private final Csv.Col col;
        private final int newIndex;

        private NamedCol(int originalIndex, Csv.Col col, int newIndex) {
            this.originalIndex = originalIndex;
            this.col = col;
            this.newIndex = newIndex;
        }

    }


    private static class InternalRowChecker implements RowChecker {

        private final NamedCol[] namedColArray;
        private int ignored;

        private InternalRowChecker(NamedCol[] namedColArray, int ignored) {
            this.namedColArray = namedColArray;
            this.ignored = ignored;
        }

        @Override
        public String[] checkRow(String[] row) {
            if (ignored > 0) {
                ignored--;
                return null;
            }
            int currentLength = row.length;
            int namedColLength = namedColArray.length;
            String[] result = new String[namedColLength];
            for (int i = 0; i < namedColLength; i++) {
                NamedCol namedCol = namedColArray[i];
                int index = namedCol.originalIndex;
                if (index < currentLength) {
                    String value = row[index];
                    Invocable invocable = namedCol.col.getInvocable();
                    if (invocable != null) {
                        try {
                            Object obj = invocable.invokeFunction("checkValue", value);
                            value = obj.toString();
                        } catch (NoSuchMethodException noSuchMethodException) {
                            throw new NestedNoSuchMethodException(noSuchMethodException);
                        } catch (ScriptException scriptException) {
                            throw new NestedScriptException(scriptException);
                        }
                    }
                    result[i] = value;
                } else {
                    break;
                }
            }
            return result;
        }

    }

}
