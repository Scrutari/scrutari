/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools.app;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Date;
import net.mapeadores.util.xml.DOMUtils;
import net.scrutari.dataexport.ScrutariDataExportFactory;
import net.scrutari.dataexport.ScrutariInfoUtils;
import net.scrutari.dataexport.api.ScrutariDataExport;
import net.scrutari.dataexport.tools.BaseConversion;
import net.scrutari.dataexport.tools.BaseConversionEngine;
import net.scrutari.dataexport.tools.ConversionDirectories;
import net.scrutari.dataexport.tools.dom.BaseConversionDOMReader;
import net.scrutari.dataexport.tools.dom.ConversionConfigListDOMReader;
import net.scrutari.dataexport.tools.dom.DOMLog;
import net.scrutari.dataexport.tools.dom.DownloadConfigDOMReader;
import net.scrutari.dataexport.tools.download.DownloadConfig;
import net.scrutari.dataexport.tools.download.DownloadEngine;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Vincent Calame
 */
public class Main {

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.err.println("Missing config file argument");
            return;
        }
        File configFile = new File(args[args.length - 1]);
        if (!configFile.exists()) {
            System.err.println("Unknown file: " + args[args.length - 1]);
            return;
        }
        Document document = DOMUtils.readDocument(configFile);
        Element rootElement = document.getDocumentElement();
        File cacheDir = initDir(rootElement, "cache-dir");
        File destinationDir = initDir(rootElement, "destination-dir");
        DownloadConfig downloadConfig = new DownloadConfig();
        DownloadConfigDOMReader downloadConfigDOMReader = new DownloadConfigDOMReader();
        downloadConfigDOMReader.readDownloadConfig(downloadConfig, cacheDir, rootElement);
        testArguments(args, downloadConfig);
        DownloadEngine.run(downloadConfig, cacheDir);
        ConversionConfigList conversionConfigList = new ConversionConfigList();
        ConversionConfigListDOMReader conversionConfigListDOMReader = new ConversionConfigListDOMReader();
        conversionConfigListDOMReader.readConversionConfigList(conversionConfigList, configFile.getParentFile(), rootElement);
        conversion(conversionConfigList, destinationDir, cacheDir);
    }

    private static File initDir(Element rootElement, String name) {
        String value = rootElement.getAttribute(name);
        if (value.length() == 0) {
            System.err.println("Missing attribute: " + name);
            return null;
        }
        File dir = new File(value);
        if (!dir.exists()) {
            System.err.println("Unknown " + name + " directory: " + value);
            return null;
        }
        if (!dir.isDirectory()) {
            System.err.println("Not a " + name + " directory: " + value);
            return null;
        }
        return dir;
    }

    private static void testArguments(String[] args, DownloadConfig downloadConfig) {
        for (String argument : args) {
            if (argument.equals("--ignore-update")) {
                downloadConfig.setIgnoreUpdate(true);
            }
        }
    }

    public static void conversion(ConversionConfigList conversionConfigList, File destinationDir, File cacheDir) throws IOException {
        int count = conversionConfigList.getConversionConfigCount();
        for (int i = 0; i < count; i++) {
            File configFile = conversionConfigList.getConfigFile(i);
            String destinationName = conversionConfigList.getDestinationName(i);
            File scrutariDataFile = new File(destinationDir, destinationName + ".scrutari-data.xml");
            Document document = DOMUtils.readDocument(configFile);
            BufferedWriter bufWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(scrutariDataFile), "UTF-8"));
            ScrutariDataExport scrutariDataExport = ScrutariDataExportFactory.newInstance(bufWriter, 0, true);
            ConversionDirectories conversionDirectories = new ConversionDirectories(cacheDir, cacheDir);
            BaseConversion baseConversion = new BaseConversion(conversionDirectories);
            DOMLog domLog = new DOMLog();
            BaseConversionDOMReader domReader = new BaseConversionDOMReader(domLog);
            domReader.readBaseConversion(baseConversion, document.getDocumentElement());
            BaseConversionEngine.run(baseConversion, scrutariDataExport, domLog);
            bufWriter.close();
            BufferedWriter infoWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(destinationDir, destinationName + ".scrutari-info.xml")), "UTF-8"));
            String[] urlArray = new String[1];
            urlArray[0] = destinationName + ".scrutari-data.xml";
            ScrutariInfoUtils.writeScrutariInfo(infoWriter, 0, true, new Date(), urlArray);
            infoWriter.close();
        }
    }

}
