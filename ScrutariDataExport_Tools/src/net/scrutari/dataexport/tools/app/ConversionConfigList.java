/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools.app;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class ConversionConfigList {

    private final List<File> fileList = new ArrayList<File>();
    private final List<String> nameList = new ArrayList<String>();

    public ConversionConfigList() {
    }

    public int getConversionConfigCount() {
        return fileList.size();
    }

    public File getConfigFile(int i) {
        return fileList.get(i);
    }

    public String getDestinationName(int i) {
        return nameList.get(i);
    }

    public void add(File file, String name) {
        fileList.add(file);
        nameList.add(name);
    }

}
