/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools;

import java.io.File;


/**
 *
 * @author Vincent Calame
 */
public class ConversionDirectories {

    private final File csvRootDir;
    private final File cacheDir;

    public ConversionDirectories(File csvRootDir, File cacheDir) {
        this.csvRootDir = csvRootDir;
        this.cacheDir = cacheDir;
        cacheDir.mkdirs();
    }

    public File getCsvRootDir() {
        return csvRootDir;
    }

    public File getCacheDir() {
        return cacheDir;
    }

}
