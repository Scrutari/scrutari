/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.conditions.TextTestEngine;
import net.mapeadores.util.exceptions.NestedIOException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.StringUtils;
import net.mapeadores.util.text.tableparser.CacheRowHandler;
import net.mapeadores.util.text.tableparser.RowEligibility;
import net.mapeadores.util.text.tableparser.RowIterator;
import net.mapeadores.util.text.tableparser.RowIteratorFactory;
import net.mapeadores.util.text.tableparser.RowParser;
import net.scrutari.dataexport.api.BaseMetadataExport;
import net.scrutari.dataexport.api.CorpusMetadataExport;
import net.scrutari.dataexport.api.FicheExport;
import net.scrutari.dataexport.api.ScrutariDataExport;
import net.scrutari.dataexport.tools.dom.BaseMetadataDOMReader;
import net.scrutari.dataexport.tools.dom.CorpusDOMReader;
import net.scrutari.dataexport.tools.dom.DOMLog;
import net.scrutari.dataexport.tools.dom.ThesaurusDOMReader;
import net.scrutari.dataexport.tools.matching.AttributeValues;
import net.scrutari.dataexport.tools.matching.ColValue;
import net.scrutari.dataexport.tools.matching.FieldConcatenation;
import net.scrutari.dataexport.tools.matching.IndexationTokens;
import net.scrutari.dataexport.tools.matching.MatchingManager;
import net.scrutari.dataexport.tools.thesaurus.ThesaurusBuffer;
import net.scrutari.dataexport.tools.thesaurus.ThesaurusBufferDOMReader;

/**
 *
 * @author Vincent Calame
 */
public class BaseConversionEngine {

    private final static RowEligibility ALL_ROWELIGIBILITY = new AllRowEligibility();
    private final BaseConversion baseConversion;
    private final Map<String, CsvWrapper> csvMap = new HashMap<String, CsvWrapper>();
    private final ScrutariDataExport scrutariDataExport;
    private final BaseMetadataDOMReader baseMetadataDOMReader;
    private final CorpusDOMReader corpusDOMReader;
    private final ThesaurusDOMReader thesaurusDOMReader;
    private final ThesaurusBufferDOMReader thesaurusBufferDOMReader;
    private final Map<String, ThesaurusBuffer> thesaurusBufferMap = new HashMap<String, ThesaurusBuffer>();

    private BaseConversionEngine(BaseConversion baseConversion, ScrutariDataExport scrutariDataExport, DOMLog domLog) {
        this.baseConversion = baseConversion;
        this.scrutariDataExport = scrutariDataExport;
        this.baseMetadataDOMReader = new BaseMetadataDOMReader(domLog);
        this.corpusDOMReader = new CorpusDOMReader(domLog);
        this.thesaurusDOMReader = new ThesaurusDOMReader(domLog);
        this.thesaurusBufferDOMReader = new ThesaurusBufferDOMReader();
    }

    public static void run(BaseConversion baseConversion, ScrutariDataExport scrutariDataExport, DOMLog domLog) {
        BaseConversionEngine engine = new BaseConversionEngine(baseConversion, scrutariDataExport, domLog);
        engine.cacheCsv();
        engine.run();
    }

    private void cacheCsv() {
        ConversionDirectories conversionDirectories = baseConversion.getConversionDirectories();
        int count = baseConversion.getCsvCount();
        for (int i = 0; i < count; i++) {
            Csv csv = baseConversion.getCsv(i);
            String name = csv.getName();
            File csvFile = new File(conversionDirectories.getCsvRootDir(), csv.getFile());
            File cacheFile = new File(conversionDirectories.getCacheDir(), "csv_" + name);
            CsvWrapper csvWrapper = new CsvWrapper(csv);
            CacheRowHandler cacheRowHandler = new CacheRowHandler(cacheFile, csvWrapper.namedColManager.getRowChecker());
            RowParser rowParser = new RowParser(cacheRowHandler, csv.getCsvParameters());
            try {
                InputStreamReader reader = new InputStreamReader(new FileInputStream(csvFile), csv.getEncoding());
                rowParser.parse(reader);
                reader.close();
            } catch (IOException ioe) {
                throw new NestedIOException(ioe);
            }
            csvWrapper.rowIteratorFactory = cacheRowHandler.toRowIteratorFactory();
            csvMap.put(name, csvWrapper);
        }
    }

    private void run() {
        int thesaurusCount = baseConversion.getThesaurusElementCount();
        for (int i = 0; i < thesaurusCount; i++) {
            ThesaurusBuffer thesaurusBuffer = thesaurusBufferDOMReader.readThesaurus(baseConversion.getThesaurusElement(i));
            if (thesaurusBuffer != null) {
                thesaurusBufferMap.put(thesaurusBuffer.getThesaurusName(), thesaurusBuffer);
            }
        }
        BaseMetadataExport baseMetadataExport = scrutariDataExport.startExport();
        baseMetadataDOMReader.readBaseMetadata(baseMetadataExport, baseConversion.getBaseMetadataElement());
        int corpusConversionCount = baseConversion.getCorpusConversionCount();
        for (int i = 0; i < corpusConversionCount; i++) {
            CorpusConversion corpusConversion = baseConversion.getCorpusConversion(i);
            String corpusName = corpusConversion.getCorpusName();
            CorpusMetadataExport corpusMetadataExport = scrutariDataExport.newCorpus(corpusName);
            corpusDOMReader.readMetadata(corpusMetadataExport, corpusConversion.getMetadataElement());
            int sourceCount = corpusConversion.getSourceCount();
            for (int j = 0; j < sourceCount; j++) {
                CorpusConversion.Source source = corpusConversion.getSource(j);
                CsvWrapper csvWrapper = csvMap.get(source.getName());
                if (csvWrapper != null) {
                    exportSource(source, csvWrapper, corpusName);
                }
            }
        }
        for (int i = 0; i < thesaurusCount; i++) {
            thesaurusDOMReader.readThesaurus(scrutariDataExport, baseConversion.getThesaurusElement(i));
        }
        scrutariDataExport.endExport();
    }

    private void exportSource(CorpusConversion.Source source, CsvWrapper csvWrapper, String currentCorpusName) {
        NamedColManager namedColManager = csvWrapper.namedColManager;
        MatchingManager matchingManager = MatchingManager.build(source, namedColManager);
        RowEligibility rowEligibility = toRowEligibility(source, namedColManager);
        RowIterator rowIterator = csvWrapper.rowIteratorFactory.newRowIterator();
        int complementMaxNumber = matchingManager.getComplementMaxNumber();
        int attributeValuesCount = matchingManager.getAttributeValuesCount();
        int indexationTokensCount = matchingManager.getIndexationTokensCount();
        while (rowIterator.hasNext()) {
            String[] row = rowIterator.next();
            if (!rowEligibility.accept(row)) {
                continue;
            }
            String ficheId = concatField(matchingManager, namedColManager, row, FieldConcatenation.FICHE_ID);
            if (ficheId != null) {
                FicheExport ficheExport = scrutariDataExport.newFiche(ficheId);
                String titre = concatField(matchingManager, namedColManager, row, FieldConcatenation.TITRE);
                if (titre != null) {
                    ficheExport.setTitre(titre);
                }
                String soustitre = concatField(matchingManager, namedColManager, row, FieldConcatenation.SOUSTITRE);
                if (soustitre != null) {
                    ficheExport.setSoustitre(soustitre);
                }
                String href = concatField(matchingManager, namedColManager, row, FieldConcatenation.HREF);
                if (href != null) {
                    ficheExport.setHref(href);
                }
                String lang = concatField(matchingManager, namedColManager, row, FieldConcatenation.LANG);
                if (lang != null) {
                    ficheExport.setLang(lang);
                }
                String date = concatField(matchingManager, namedColManager, row, FieldConcatenation.DATE);
                if (date != null) {
                    ficheExport.setDate(date);
                }
                for (int number = 1; number <= complementMaxNumber; number++) {
                    String complement = concatField(matchingManager, namedColManager, row, NamedColManager.COMPLEMENT_PREFIX + number);
                    if (complement != null) {
                        ficheExport.addComplement(number, complement);
                    }
                }
                for (int i = 0; i < attributeValuesCount; i++) {
                    AttributeValues attributeValues = matchingManager.getAttributeValues(i);
                    AttributeKey attributeKey = attributeValues.getAttributeKey();
                    List<String> values = getAttributeValues(attributeValues, namedColManager, row);
                    for (String value : values) {
                        ficheExport.addAttributeValue(attributeKey.getNameSpace(), attributeKey.getLocalKey(), value);
                    }
                }
                for (int i = 0; i < indexationTokensCount; i++) {
                    IndexationTokens indexationTokens = matchingManager.getIndexationTokens(i);
                    String matchType = indexationTokens.getMatchType();
                    String thesaurusName = indexationTokens.getThesaurusName();
                    ThesaurusBuffer thesaurusBuffer = thesaurusBufferMap.get(thesaurusName);
                    List<String> tokens = getIndexationTokens(indexationTokens, namedColManager, row);
                    for (String token : tokens) {
                        String motcleId = null;
                        if (matchType.equals(IndexationTokens.MOTCLEID_MATCHTYPE)) {
                            motcleId = token;
                        } else if (matchType.equals(IndexationTokens.FICHELANG_MATCHTYPE)) {
                            if ((lang != null) && (thesaurusBuffer != null)) {
                                motcleId = thesaurusBuffer.getMotcleId(lang, token);
                            }
                        }
                        if (motcleId != null) {
                            scrutariDataExport.addIndexation(currentCorpusName, ficheId, thesaurusName, motcleId, 1);
                        }
                    }

                }
            }
        }
        rowIterator.close();
    }

    private String concatField(MatchingManager matchingManager, NamedColManager namedColManager, String[] row, String fieldName) {
        FieldConcatenation fieldConcatenation = matchingManager.getFieldConcatenation(fieldName);
        if (fieldConcatenation == null) {
            return null;
        }
        StringBuilder buf = new StringBuilder();
        int count = fieldConcatenation.getConcatPartCount();
        for (int i = 0; i < count; i++) {
            Object concatPart = fieldConcatenation.getConcatPart(i);
            if (concatPart instanceof ColValue) {
                ColValue colValue = (ColValue) concatPart;
                String colName = colValue.getColName();
                String value = namedColManager.getValue(row, colName);
                if (value != null) {
                    buf.append(value);
                }
            }
        }
        if (buf.length() == 0) {
            return null;
        }
        return buf.toString();
    }

    private List<String> getAttributeValues(AttributeValues attributeValues, NamedColManager namedColManager, String[] row) {
        List<String> resultList = new ArrayList<String>();
        int count = attributeValues.getColValueCount();
        for (int i = 0; i < count; i++) {
            ColValue colValue = attributeValues.getColValue(i);
            String colName = colValue.getColName();
            String values = namedColManager.getValue(row, colName);
            if (values != null) {
                String[] valueArray = StringUtils.getTokens(values, colValue.getDelimiter(), StringUtils.EMPTY_EXCLUDE);
                int length = valueArray.length;
                for (int j = 0; j < length; j++) {
                    resultList.add(valueArray[j]);
                }
            }
        }
        return resultList;
    }

    private List<String> getIndexationTokens(IndexationTokens indexationTokens, NamedColManager namedColManager, String[] row) {
        List<String> resultList = new ArrayList<String>();
        int count = indexationTokens.getColValueCount();
        for (int i = 0; i < count; i++) {
            ColValue colValue = indexationTokens.getColValue(i);
            String colName = colValue.getColName();
            String values = namedColManager.getValue(row, colName);
            if (values != null) {
                String[] valueArray = StringUtils.getTokens(values, colValue.getDelimiter(), StringUtils.EMPTY_EXCLUDE);
                int length = valueArray.length;
                for (int j = 0; j < length; j++) {
                    resultList.add(valueArray[j]);
                }
            }
        }
        return resultList;
    }

    private RowEligibility toRowEligibility(CorpusConversion.Source source, NamedColManager namedColManager) {
        int count = source.getColConditionCount();
        if (count == 0) {
            return ALL_ROWELIGIBILITY;
        }
        List<ColTest> colTestList = new ArrayList<ColTest>();
        for (int i = 0; i < count; i++) {
            CorpusConversion.ColCondition colCondition = source.getColCondition(i);
            int newIndex = namedColManager.getNewIndex(colCondition.getColName());
            if (newIndex != -1) {
                TextTestEngine textTestEngine = TextTestEngine.newInstance(colCondition.getCondition(), Lang.build("fr"));
                colTestList.add(new ColTest(newIndex, textTestEngine));
            }
        }
        int size = colTestList.size();
        if (size == 0) {
            return ALL_ROWELIGIBILITY;
        }
        return new InternalRowEligibility(colTestList.toArray(new ColTest[size]));
    }

    private static class CsvWrapper {

        private final Csv csv;
        private final NamedColManager namedColManager;
        private RowIteratorFactory rowIteratorFactory;

        private CsvWrapper(Csv csv) {
            this.csv = csv;
            this.namedColManager = NamedColManager.build(csv);
        }

    }

    private static class AllRowEligibility implements RowEligibility {

        private AllRowEligibility() {
        }

        @Override
        public boolean accept(String[] row) {
            return true;
        }

    }

    private static class InternalRowEligibility implements RowEligibility {

        private final ColTest[] testArray;

        private InternalRowEligibility(ColTest[] testArray) {
            this.testArray = testArray;
        }

        @Override
        public boolean accept(String[] row) {
            int length = testArray.length;
            int lastIndex = row.length - 1;
            for (int i = 0; i < length; i++) {
                ColTest colTest = testArray[i];
                int index = colTest.index;
                String value;
                if (index > lastIndex) {
                    value = "";
                } else {
                    value = row[index];
                }
                if (!colTest.test(value)) {
                    return false;
                }
            }
            return true;
        }

    }

    private static class ColTest {

        private int index;
        private final TextTestEngine textTestEngine;

        private ColTest(int index, TextTestEngine textTestEngine) {
            this.index = index;
            this.textTestEngine = textTestEngine;
        }

        public boolean test(String value) {
            textTestEngine.start();
            textTestEngine.addString(value);
            return textTestEngine.getResult();
        }

    }

}
