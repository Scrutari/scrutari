/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.dataexport.tools;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.conditions.Condition;
import net.scrutari.dataexport.tools.matching.AttributeValues;
import net.scrutari.dataexport.tools.matching.FieldConcatenation;
import net.scrutari.dataexport.tools.matching.IndexationTokens;
import org.w3c.dom.Element;

/**
 *
 * @author Vincent Calame
 */
public class CorpusConversion {
    
    private final String corpusName;
    private final List<Source> sourceList = new ArrayList<Source>();
    private Element metadataElement;
    
    public CorpusConversion(String corpusName) {
        this.corpusName = corpusName;
    }
    
    public String getCorpusName() {
        return corpusName;
    }
    
    public Element getMetadataElement() {
        return metadataElement;
    }
    
    public void setMetadataElement(Element metadataElement) {
        this.metadataElement = metadataElement;
    }
    
    public void addSource(Source source) {
        sourceList.add(source);
    }
    
    public int getSourceCount() {
        return sourceList.size();
    }
    
    public Source getSource(int index) {
        return sourceList.get(index);
    }
    
    public static class Source {
        
        private final String name;
        private final List<ColCondition> colConditionList = new ArrayList<ColCondition>();
        private final List<FieldConcatenation> fieldConcatenationList = new ArrayList<FieldConcatenation>();
        private final List<AttributeValues> attributeValuesList = new ArrayList<AttributeValues>();
        private final List<IndexationTokens> indexationTokensList = new ArrayList<IndexationTokens>();
        
        public Source(String name) {
            this.name = name;
        }
        
        public String getName() {
            return name;
        }
        
        public void addColCondition(ColCondition colCondition) {
            colConditionList.add(colCondition);
        }
        
        public void addFieldConcatenation(FieldConcatenation fieldConcatenation) {
            String fieldName = fieldConcatenation.getFieldName();
            boolean exists = false;
            for (FieldConcatenation current : fieldConcatenationList) {
                if (current.getFieldName().equals(fieldName)) {
                    exists = true;
                    int count = fieldConcatenation.getConcatPartCount();
                    for (int i = 0; i < count; i++) {
                        current.addConcatPart(fieldConcatenation.getConcatPart(i));
                    }
                }
            }
            if (!exists) {
                fieldConcatenationList.add(fieldConcatenation);
            }
        }
        
        public void addAttributeValues(AttributeValues attributeValues) {
            AttributeKey attributeKey = attributeValues.getAttributeKey();
            boolean exists = false;
            for (AttributeValues current : attributeValuesList) {
                if (current.getAttributeKey().equals(attributeKey)) {
                    exists = true;
                    int count = attributeValues.getColValueCount();
                    for (int i = 0; i < count; i++) {
                        current.addColValue(attributeValues.getColValue(i));
                    }
                }
            }
            if (!exists) {
                attributeValuesList.add(attributeValues);
            }
        }
        
        public void addIndexationTokens(IndexationTokens indexationTokens) {
            indexationTokensList.add(indexationTokens);
        }
        
        public int getColConditionCount() {
            return colConditionList.size();
        }
        
        public ColCondition getColCondition(int index) {
            return colConditionList.get(index);
        }
        
        public int getFieldConcatenationCount() {
            return fieldConcatenationList.size();
        }
        
        public FieldConcatenation getFieldConcatenation(int i) {
            return fieldConcatenationList.get(i);
        }
        
        public int getAttributeValuesCount() {
            return attributeValuesList.size();
        }
        
        public AttributeValues getAttributeValues(int i) {
            return attributeValuesList.get(i);
        }
        
        public int getIndexationTokensCount() {
            return indexationTokensList.size();
        }
        
        public IndexationTokens getIndexationTokens(int i) {
            return indexationTokensList.get(i);
        }
        
    }
    
    public static class ColCondition {
        
        private final String colName;
        private final Condition condition;
        
        public ColCondition(String colName, Condition condition) {
            this.colName = colName;
            this.condition = condition;
        }
        
        public String getColName() {
            return colName;
        }
        
        public Condition getCondition() {
            return condition;
        }
        
    }
    
}
