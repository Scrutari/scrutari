/* ScrutariDataExport_Tools - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.dataexport.tools;

import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class BaseConversion {

    private final List<Csv> csvList = new ArrayList<Csv>();
    private final List<CorpusConversion> corpusConversionList = new ArrayList<CorpusConversion>();
    private final List<Element> thesaurusElementList = new ArrayList<Element>();
    private final ConversionDirectories conversionDirectories;
    private Element baseMetadataElement;

    public BaseConversion(ConversionDirectories conversionDirectories) {
        this.conversionDirectories = conversionDirectories;
    }

    public ConversionDirectories getConversionDirectories() {
        return conversionDirectories;
    }

    public Element getBaseMetadataElement() {
        return baseMetadataElement;
    }

    public void setBaseMetadataElement(Element baseMetadataElement) {
        this.baseMetadataElement = baseMetadataElement;
    }

    public void addThesaurusElement(Element thesaurusElement) {
        thesaurusElementList.add(thesaurusElement);
    }

    public int getThesaurusElementCount() {
        return thesaurusElementList.size();
    }

    public Element getThesaurusElement(int index) {
        return thesaurusElementList.get(index);
    }

    public void addCsv(Csv csv) {
        csvList.add(csv);
    }

    public int getCsvCount() {
        return csvList.size();
    }

    public Csv getCsv(int index) {
        return csvList.get(index);
    }

    public void addCorpusConversion(CorpusConversion corpusConversion) {
        corpusConversionList.add(corpusConversion);
    }

    public int getCorpusConversionCount() {
        return corpusConversionList.size();
    }

    public CorpusConversion getCorpusConversion(int index) {
        return corpusConversionList.get(index);
    }

}
