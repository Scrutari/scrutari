/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.SortedMap;
import java.util.TreeMap;
import net.scrutari.dataexport.api.FicheExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlFicheExport extends XmlAttributeExport implements FicheExport {

    private final SortedMap<Integer, String> complementMap = new TreeMap<Integer, String>();
    private String ficheId;
    private String titre;
    private String soustitre;
    private String date;
    private String lang;
    private String href;
    private String ficheIcon;
    private String latitude;
    private String longitude;


    public XmlFicheExport() {
    }

    public void reinit(String ficheId) {
        this.ficheId = ficheId;
        this.titre = null;
        this.soustitre = null;
        this.date = null;
        this.lang = null;
        this.href = null;
        this.ficheIcon = null;
        this.latitude = null;
        this.longitude = null;
        this.complementMap.clear();
        super.clear();
    }

    @Override
    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Override
    public void setSoustitre(String soustitre) {
        this.soustitre = soustitre;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public void setFicheIcon(String ficheIcon) {
        this.ficheIcon = ficheIcon;
    }

    @Override
    public void setGeoloc(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void addComplement(int complementNumber, String complementValue) {
        if (complementNumber < 1) {
            return;
        }
        this.complementMap.put(complementNumber, complementValue);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTagWithAttribute("fiche", "fiche-id", ficheId);
        xmlWriter.addSimpleElement("titre", titre);
        xmlWriter.addSimpleElement("soustitre", soustitre);
        xmlWriter.addSimpleElement("date", date);
        xmlWriter.addSimpleElement("lang", lang);
        xmlWriter.addSimpleElement("href", href);
        xmlWriter.addSimpleElement("fiche-icon", ficheIcon);
        if ((latitude != null) && (longitude != null)) {
            xmlWriter.openTag("geoloc");
            xmlWriter.addSimpleElement("lat", latitude);
            xmlWriter.addSimpleElement("lon", longitude);
            xmlWriter.closeTag("geoloc");
        }
        if (complementMap.size() > 0) {
            int max = complementMap.lastKey();
            for (int i = 1; i <= max; i++) {
                String s = complementMap.get(i);
                if ((s == null) || (s.length() == 0)) {
                    xmlWriter.addEmptyElement("complement", null, null);
                } else {
                    xmlWriter.addSimpleElement("complement", s);
                }
            }
        }
        writeAttributes(xmlWriter);
        xmlWriter.closeTag("fiche");
    }

}
