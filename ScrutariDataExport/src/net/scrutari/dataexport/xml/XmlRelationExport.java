/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */

package net.scrutari.dataexport.xml;

import java.util.ArrayList;
import java.util.List;
import net.scrutari.dataexport.api.RelationExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlRelationExport extends XmlAttributeExport implements RelationExport {

    private final static String[] NAME_ARRAY = {"role", "type", "uri"};
    private final List<String> memberList = new ArrayList<String>();
    private String type;

    public XmlRelationExport() {

    }

    public void reinit(String type) {
        this.type = type;
        this.memberList.clear();
        super.clear();
    }

    @Override
    public void addMember(String role, String type, String uri) {
        memberList.add(role);
        memberList.add(type);
        memberList.add(uri);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        String[] value = new String[3];
        xmlWriter.openTagWithAttribute("relation", "type", type);
        int length = memberList.size() / 3;
        for (int i = 0; i < length; i++) {
            value[0] = memberList.get(i * 3);
            value[1] = memberList.get((i * 3) + 1);
            value[2] = memberList.get((i * 3) + 2);
            xmlWriter.addEmptyElement("member", NAME_ARRAY, value);
        }
        writeAttributes(xmlWriter);
        xmlWriter.closeTag("relation");
    }

}
