/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.LinkedHashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
class PhraseBuffer {

    private final Map<String, String> map = new LinkedHashMap<String, String>();
    private final String name;

    PhraseBuffer(String name) {
        this.name = name;
    }

    void addLabel(String lang, String text) {
        map.put(lang, text);
    }

    void removeLabel(String lang) {
        map.remove(lang);
    }

    void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTagWithAttribute("phrase", "name", name);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xmlWriter.addLabelElement(entry.getKey(), entry.getValue());
        }
        xmlWriter.closeTag("phrase");
    }

}
