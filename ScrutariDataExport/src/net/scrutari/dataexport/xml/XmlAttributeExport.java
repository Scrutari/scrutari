/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */

package net.scrutari.dataexport.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import net.scrutari.dataexport.api.AttributeExport;


/**
 *
 * @author Vincent Calame
 */
public abstract class XmlAttributeExport extends XmlBuilder implements AttributeExport {

    private final Map<String, AttributeBuffer> attributeMap = new LinkedHashMap<String, AttributeBuffer>();

    public XmlAttributeExport() {

    }

    @Override
    public void addAttributeValue(String nameSpace, String localKey, String attributeValue) {
        if (nameSpace.length() == 0) {
            return;
        }
        if (localKey.length() == 0) {
            return;
        }
        if (attributeValue.length() == 0) {
            return;
        }
        String key = nameSpace + ":" + localKey;
        AttributeBuffer buffer = attributeMap.get(key);
        if (buffer == null) {
            buffer = new AttributeBuffer(nameSpace, localKey);
            attributeMap.put(key, buffer);
        }
        buffer.addValue(attributeValue);
    }

    protected void writeAttributes(XmlWriter xmlWriter) {
        for (AttributeBuffer buffer : attributeMap.values()) {
            buffer.writeXML(xmlWriter);
        }
    }

    protected void clear() {
        this.attributeMap.clear();
    }

}
