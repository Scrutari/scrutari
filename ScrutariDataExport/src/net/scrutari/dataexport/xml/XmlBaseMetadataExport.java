/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.scrutari.dataexport.api.BaseMetadataExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlBaseMetadataExport extends XmlMetadataExport implements BaseMetadataExport {

    private final List<String> langUIList = new ArrayList<String>();
    private final Map<String, String> shortMap = new LinkedHashMap<String, String>();
    private final Map<String, String> longMap = new LinkedHashMap<String, String>();
    private String authority;
    private String baseName;
    private String baseIcon;

    public XmlBaseMetadataExport() {
    }

    @Override
    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public void setBaseName(String baseName) {
        this.baseName = baseName;
    }

    @Override
    public void setBaseIcon(String baseIcon) {
        this.baseIcon = baseIcon;
    }

    @Override
    public void setIntitule(int intituleType, String lang, String intituleValue) {
        Map<String, String> map = getMap(intituleType);
        map.put(lang, intituleValue);
    }

    @Override
    public void addLangUI(String lang) {
        langUIList.add(lang);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTag("base-metadata");
        xmlWriter.addSimpleElement("authority", authority);
        xmlWriter.addSimpleElement("base-name", baseName);
        xmlWriter.addSimpleElement("base-icon", baseIcon);
        addMap(INTITULE_SHORT, xmlWriter);
        addMap(INTITULE_LONG, xmlWriter);
        int size = langUIList.size();
        if (size > 0) {
            xmlWriter.openTag("langs-ui");
            for (int i = 0; i < size; i++) {
                xmlWriter.addSimpleElement("lang", langUIList.get(i));
            }
            xmlWriter.closeTag("langs-ui");
        }
        writePhrases(xmlWriter);
        writeAttributes(xmlWriter);
        xmlWriter.closeTag("base-metadata");
    }

    private void addMap(int intituleType, XmlWriter xmlWriter) {
        Map<String, String> map = getMap(intituleType);
        if (map.isEmpty()) {
            return;
        }
        String suffix = getSuffix(intituleType);
        xmlWriter.openTag("intitule-" + suffix);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xmlWriter.addLibElement(entry.getKey(), entry.getValue());
        }
        xmlWriter.closeTag("intitule-" + suffix);
    }

    private Map<String, String> getMap(int type) {
        switch (type) {
            case INTITULE_SHORT:
                return shortMap;
            case INTITULE_LONG:
                return longMap;
            default:
                throw new IllegalArgumentException("Wrong intituleType = " + type);
        }
    }

    private static String getSuffix(int type) {
        switch (type) {
            case INTITULE_SHORT:
                return "short";
            case INTITULE_LONG:
                return "long";
            default:
                throw new IllegalArgumentException("Wrong intituleType = " + type);
        }
    }

}
