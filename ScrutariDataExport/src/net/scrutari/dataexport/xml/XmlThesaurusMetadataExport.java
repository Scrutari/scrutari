/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import net.scrutari.dataexport.api.ThesaurusMetadataExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlThesaurusMetadataExport extends XmlMetadataExport implements ThesaurusMetadataExport {

    private final Map<String, String> thesaurusMap = new LinkedHashMap<String, String>();

    public XmlThesaurusMetadataExport() {

    }

    @Override
    public void setIntitule(int intituleType, String lang, String intituleValue) {
        Map<String, String> map = getMap(intituleType);
        map.put(lang, intituleValue);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTag("thesaurus-metadata");
        addMap(INTITULE_THESAURUS, xmlWriter);
        writePhrases(xmlWriter);
        writeAttributes(xmlWriter);
        xmlWriter.closeTag("thesaurus-metadata");
    }

    private void addMap(int intituleType, XmlWriter xmlWriter) {
        Map<String, String> map = getMap(intituleType);
        if (map.isEmpty()) {
            return;
        }
        String suffix = getSuffix(intituleType);
        xmlWriter.openTag("intitule-" + suffix);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xmlWriter.addLibElement(entry.getKey(), entry.getValue());
        }
        xmlWriter.closeTag("intitule-" + suffix);
    }

    private Map<String, String> getMap(int type) {
        switch (type) {
            case INTITULE_THESAURUS:
                return thesaurusMap;
            default:
                throw new IllegalArgumentException("Wrong intituleType = " + type);
        }
    }

    private static String getSuffix(int type) {
        switch (type) {
            case INTITULE_THESAURUS:
                return "thesaurus";
            default:
                throw new IllegalArgumentException("Wrong intituleType = " + type);
        }
    }

}
