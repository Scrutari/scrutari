/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */

package net.scrutari.dataexport.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import net.scrutari.dataexport.api.MetadataExport;


/**
 *
 * @author Vincent Calame
 */
public abstract class XmlMetadataExport extends XmlAttributeExport implements MetadataExport {

    private final Map<String, PhraseBuffer> phraseMap = new LinkedHashMap<String, PhraseBuffer>();

    public XmlMetadataExport() {

    }

    @Override
    public void addPhraseLabel(String name, String lang, String text) {
        if (name.length() == 0) {
            return;
        }
        if (text == null) {
            text = "";
        } else {
            text = text.trim();
        }
        if (text.isEmpty()) {
            PhraseBuffer phraseBuffer = phraseMap.get(lang);
            if (phraseBuffer != null) {
                phraseBuffer.removeLabel(lang);
            }
        } else {
            PhraseBuffer phraseBuffer = phraseMap.get(lang);
            if (phraseBuffer == null) {
                phraseBuffer = new PhraseBuffer(name);
                phraseMap.put(lang, phraseBuffer);
            }
            phraseBuffer.addLabel(lang, text);
        }
    }

    protected void writePhrases(XmlWriter xmlWriter) {
        for (PhraseBuffer buffer : phraseMap.values()) {
            buffer.writeXML(xmlWriter);
        }
    }


}
