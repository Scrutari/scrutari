/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface CorpusMetadataExport extends MetadataExport {

    public final static int INTITULE_CORPUS = 1;
    public final static int INTITULE_FICHE = 2;

    public void setCorpusIcon(String corpusIcon);

    public void setHrefParent(String hrefParent);

    public int addComplement();

    public void setComplementIntitule(int complementNumber, String lang, String intituleValue);

}
