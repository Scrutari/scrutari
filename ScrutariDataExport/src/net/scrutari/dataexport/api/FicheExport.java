/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface FicheExport extends AttributeExport {

    public void setTitre(String titre);

    public void setSoustitre(String soustitre);

    public void setDate(String date);

    public void setLang(String lang);

    public void setHref(String href);

    public void setFicheIcon(String ficheIcon);

    public void setGeoloc(String latitude, String longitude);

    public void addComplement(int complementNumber, String complementValue);

}
