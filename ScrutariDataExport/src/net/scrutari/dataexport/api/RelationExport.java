/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2020 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */

package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface RelationExport extends AttributeExport {

    public void addMember(String role, String type, String uri);

}
