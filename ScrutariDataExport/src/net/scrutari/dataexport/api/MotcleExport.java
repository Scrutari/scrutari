/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleExport extends AttributeExport {

    public void setLibelle(String lang, String libelleValue);

}
