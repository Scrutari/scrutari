/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2018 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */

package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface AttributeExport {

    public void addAttributeValue(String nameSpace, String localKey, String attributeValue);

}
