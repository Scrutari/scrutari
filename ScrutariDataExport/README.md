# Implémentation en Java de l'API ScrutariDataExport

Ce dépôt contient les classes nécessaires à la « scrutarisation » de données, c'est à dire à la production d'une sortie au format ScrutariData.

L'ensemble est sous licence MIT.

## Pour en savoir plus :
- Scrutari : http://www.scrutari.net/
- API ScrutariDataExport : http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
- sur l'implémentation en Java : http://www.scrutari.net/dokuwiki/scrutaridata:exportapi:java
