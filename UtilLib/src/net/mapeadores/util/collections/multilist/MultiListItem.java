/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.collections.multilist;

import net.mapeadores.util.primitives.BitmaskUtils;


/**
 *
 * @author Vincent Calame
 */
public abstract class MultiListItem {

    private int primaryIndex = -1;
    private int[] secondaryIndex;
    private int mask = 0;
    private MultiList multiList = null;

    public MultiListItem(MultiList multiList, int initialmask) {
        this.mask = initialmask;
        if (multiList != null) {
            multiList.add(this);
        } else {
            clear();
        }
    }

    final public int getPrimaryIndex() {
        return primaryIndex;
    }

    final public int[] getSecondaryIndex() {
        int[] result = new int[secondaryIndex.length];
        System.arraycopy(secondaryIndex, 0, result, 0, secondaryIndex.length);
        return result;
    }

    final public int getMask() {
        return mask;
    }

    final public int getSecondaryListCount() {
        return secondaryIndex.length;
    }

    final public int getSecondaryIndex(int p) {
        return secondaryIndex[p];
    }

    final public boolean isCleared() {
        return (multiList == null);
    }

    final public MultiList getMultiList() {
        return multiList;
    }

    public abstract Object getWrappedObject();

    /*
     * Appel réservé à MultiList.
     */
    void clear() {
        primaryIndex = -1;
        secondaryIndex = new int[0];
        multiList = null;
    }

    /*
     * Appel réservé à MultiList.
     */
    void setMultiList(MultiList multiList) {
        secondaryIndex = new int[multiList.getSecondaryListCount()];
        this.multiList = multiList;
    }

    /*
     * Appel réservé à MultiList.
     */
    void increaseSecondaryIndex(int p) {
        if (secondaryIndex[p] == -1) {
            throw new RuntimeException("decrease");
        }
        secondaryIndex[p]++;
    }

    /*
     * Appel réservé à MultiList.
     */
    void decreaseSecondaryIndex(int p) {
        if (secondaryIndex[p] == -1) {
            throw new RuntimeException("decrease");
        }
        secondaryIndex[p]--;
    }

    /*
     * Appel réservé à MultiList.
     */
    void decreasePrimaryIndex() {
        primaryIndex--;
    }

    /*
     * Appel réservé à MultiList.
     */
    void increasePrimaryIndex() {
        primaryIndex++;
    }

    /*
     * Appel réservé à MultiList.
     */
    void setSecondaryIndex(int p, int i) {
        if (i == -1) {
            mask = mask & (~BitmaskUtils.getBitValue(p));
        } else {
            mask = mask | BitmaskUtils.getBitValue(p);
        }
        secondaryIndex[p] = i;
    }

    /*
     * Appel réservé à MultiList.
     */
    void setPrimaryIndex(int index) {
        this.primaryIndex = index;
    }

}
