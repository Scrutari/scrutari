/* UtilLib - Copyright (c) 2006-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.collections.multilist;

import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.primitives.BitmaskUtils;


/**
 *
 * @author Vincent Calame
 */
public final class MultiList {

    private final MultiListItem[][] secondaryArrays;
    private final int[] secondarySizes;
    private final InternalItemList itemPrimaryList = new InternalItemList(-1);
    private final InternalItemList[] itemSecondaryLists;
    private final InternalWrappedObjectList wrappedObjectPrimaryList = new InternalWrappedObjectList(-1);
    private final InternalWrappedObjectList[] wrappedObjectSecondaryLists;
    private MultiListItem[] primaryArray = new MultiListItem[16];
    private int primarySize = 0;

    public MultiList(int secondaryListCount) {
        secondaryArrays = new MultiListItem[secondaryListCount][16];
        secondarySizes = new int[secondaryListCount];
        itemSecondaryLists = new InternalItemList[secondaryListCount];
        wrappedObjectSecondaryLists = new InternalWrappedObjectList[secondaryListCount];
        for (int i = 0; i < secondaryListCount; i++) {
            itemSecondaryLists[i] = new InternalItemList(i);
            wrappedObjectSecondaryLists[i] = new InternalWrappedObjectList(i);
        }
    }

    public int getPrimarySize() {
        return primarySize;
    }

    public int getSecondarySize(int p) {
        return secondarySizes[p];
    }

    public int getSecondaryListCount() {
        return secondarySizes.length;
    }

    public List<MultiListItem> getItemPrimaryList() {
        return itemPrimaryList;
    }

    public List<MultiListItem> getItemSecondaryList(int p) {
        return itemSecondaryLists[p];
    }

    public List<Object> getWrappedObjectPrimaryList() {
        return wrappedObjectPrimaryList;
    }

    public List<Object> getWrappedObjectSecondaryList(int p) {
        return wrappedObjectSecondaryLists[p];
    }

    public void add(MultiListItem multiListItem) {
        if (!multiListItem.isCleared()) {
            throw new IllegalArgumentException("multiListItem is not cleared");
        }
        multiListItem.setMultiList(this);
        if (primarySize == primaryArray.length) {
            MultiListItem[] ol = new MultiListItem[primarySize * 2];
            System.arraycopy(primaryArray, 0, ol, 0, primarySize);
            primaryArray = ol;
        }
        multiListItem.setPrimaryIndex(primarySize);
        primaryArray[primarySize] = multiListItem;
        primarySize++;
        int mask = multiListItem.getMask();
        for (int i = 0; i < secondaryArrays.length; i++) {
            if (BitmaskUtils.booleanValueAt(mask, i)) {
                addInSecondaryList(multiListItem, i);
            } else {
                multiListItem.setSecondaryIndex(i, -1);
            }
        }
    }

    public void remove(MultiListItem multiListItem) {
        int oldindex = multiListItem.getPrimaryIndex();
        int mask = multiListItem.getMask();
        for (int i = oldindex + 1; i < primarySize; i++) {
            MultiListItem next = primaryArray[i];
            primaryArray[i - 1] = next;
            next.decreasePrimaryIndex();
        }
        if (mask > 0) {
            for (int i = 0; i < secondaryArrays.length; i++) {
                if (BitmaskUtils.booleanValueAt(mask, i)) {
                    removeInSecondaryList(multiListItem, i, false);
                }
            }
        }
        multiListItem.clear();
        primarySize--;
        primaryArray[primarySize] = null;
    }

    public void removeAllInSecondaryList(int p) {
        List<MultiListItem> list = itemSecondaryLists[p];
        int size = list.size();
        for (int i = (size - 1); i >= 0; i--) {
            MultiListItem multiListItem = list.get(i);
            remove(multiListItem);
        }
    }

    public boolean translate(MultiListItem multiListItem, int newIndex) {
        if ((newIndex < 0) || (newIndex >= primarySize)) {
            throw new IndexOutOfBoundsException(String.valueOf(newIndex));
        }
        int oldIndex = multiListItem.getPrimaryIndex();
        if (oldIndex == newIndex) {
            return false;
        }
        int mask = multiListItem.getMask();
        if (mask > 0) {
            for (int p = 0; p < secondaryArrays.length; p++) {
                if (BitmaskUtils.booleanValueAt(mask, p)) {
                    int oldidx = multiListItem.getSecondaryIndex(p);
                    int nwidx = getNewSecondaryIndex(p, oldIndex, newIndex);
                    if (nwidx != oldidx) {
                        translateInSecondaryList(multiListItem, p, nwidx);
                    }
                }
            }
        }
        if (oldIndex < newIndex) {
            for (int i = oldIndex; i < newIndex; i++) {
                MultiListItem o = primaryArray[i + 1];
                primaryArray[i] = o;
                o.decreasePrimaryIndex();
            }
        } else {
            for (int i = oldIndex; i > newIndex; i--) {

                MultiListItem o = primaryArray[i - 1];
                primaryArray[i] = o;
                o.increasePrimaryIndex();
            }
        }
        primaryArray[newIndex] = multiListItem;
        multiListItem.setPrimaryIndex(newIndex);
        return true;
    }

    public int[] disable(MultiListItem multiListItem, int mask) {
        int[] result = new int[secondaryArrays.length];
        for (int p = 0; p < secondaryArrays.length; p++) {
            if (BitmaskUtils.booleanValueAt(mask, p)) {
                int oldidx = multiListItem.getSecondaryIndex(p);
                if (oldidx != -1) {
                    removeInSecondaryList(multiListItem, p, true);
                    result[p] = oldidx;
                } else {
                    result[p] = -1;
                }
            } else {
                result[p] = -1;
            }
        }
        return result;
    }

    public int[] enable(MultiListItem multiListItem, int mask) {
        int[] result = new int[secondaryArrays.length];
        for (int p = 0; p < secondaryArrays.length; p++) {
            if (BitmaskUtils.booleanValueAt(mask, p)) {
                int oldidx = multiListItem.getSecondaryIndex(p);
                if (oldidx == -1) {
                    enableInSecondaryList(multiListItem, p);
                    result[p] = multiListItem.getSecondaryIndex(p);
                } else {
                    result[p] = -1;
                }
            } else {
                result[p] = -1;
            }
        }
        return result;
    }

    private int getNewSecondaryIndex(int p, int oldIndex, int newIndex) {
        if (oldIndex < newIndex) {
            for (int i = newIndex; i >= oldIndex; i--) {
                MultiListItem o = primaryArray[i];
                int idx = o.getSecondaryIndex(p);
                if (idx != -1) {
                    return idx;
                }
            }
        } else {
            for (int i = newIndex; i <= oldIndex; i++) {
                MultiListItem o = primaryArray[i];
                int idx = o.getSecondaryIndex(p);
                if (idx != -1) {
                    return idx;
                }
            }
        }
        throw new IllegalStateException("should not occur");
    }

    private void translateInSecondaryList(MultiListItem objectInList, int p, int newIndex) {
        MultiListItem[] array = secondaryArrays[p];
        int oldIndex = objectInList.getSecondaryIndex(p);
        if (oldIndex < newIndex) {
            for (int i = oldIndex; i < newIndex; i++) {
                MultiListItem o = array[i + 1];
                array[i] = o;
                o.decreaseSecondaryIndex(p);
            }
        } else {
            for (int i = oldIndex; i > newIndex; i--) {
                MultiListItem o = array[i - 1];
                array[i] = o;
                o.increaseSecondaryIndex(p);
            }
        }
        array[newIndex] = objectInList;
        objectInList.setSecondaryIndex(p, newIndex);
    }

    private void addInSecondaryList(MultiListItem objectInList, int p) {
        MultiListItem[] array = secondaryArrays[p];
        int size = secondarySizes[p];
        if (size == array.length) {
            MultiListItem[] ol = new MultiListItem[size * 2];
            System.arraycopy(array, 0, ol, 0, size);
            array = ol;
            secondaryArrays[p] = array;
        }
        array[size] = objectInList;
        objectInList.setSecondaryIndex(p, size);
        secondarySizes[p]++;
    }

    private void removeInSecondaryList(MultiListItem objectInList, int p, boolean maskchange) {
        int oldindex = objectInList.getSecondaryIndex(p);
        if (oldindex == -1) {
            throw new IllegalStateException("should not occur");
        }
        MultiListItem[] array = secondaryArrays[p];
        int size = secondarySizes[p];
        for (int i = oldindex + 1; i < size; i++) {
            MultiListItem next = array[i];
            array[i - 1] = next;
            next.decreaseSecondaryIndex(p);
        }
        if (maskchange) {
            objectInList.setSecondaryIndex(p, -1);
        }
        secondarySizes[p]--;
        array[size - 1] = null;
    }

    private void enableInSecondaryList(MultiListItem multiListItem, int p) {
        MultiListItem[] array = secondaryArrays[p];
        int secondarySize = secondarySizes[p];
        int primaryIndex = multiListItem.getPrimaryIndex();
        int newSecondaryIndex = 0;
        for (int i = 0; i < secondarySize; i++) {
            if (array[i].getPrimaryIndex() < primaryIndex) {
                newSecondaryIndex++;
            } else {
                break;
            }
        }
        if (secondarySize == array.length) {
            MultiListItem[] ol = new MultiListItem[secondarySize * 2];
            System.arraycopy(array, 0, ol, 0, secondarySize);
            array = ol;
            secondaryArrays[p] = array;
        }
        if (newSecondaryIndex < secondarySize) {
            for (int i = secondarySize; i > newSecondaryIndex; i--) {
                MultiListItem previous = array[i - 1];
                array[i] = previous;
                previous.increaseSecondaryIndex(p);
            }
        }
        array[newSecondaryIndex] = multiListItem;
        multiListItem.setSecondaryIndex(p, newSecondaryIndex);
        secondarySizes[p]++;
    }


    private class InternalWrappedObjectList extends AbstractList<Object> implements RandomAccess {

        private final int listIndex;

        InternalWrappedObjectList(int listIndex) {
            this.listIndex = listIndex;
        }

        @Override
        public int size() {
            if (listIndex == -1) {
                return primarySize;
            } else {
                return secondarySizes[listIndex];
            }
        }

        @Override
        public Object get(int index) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("index < 0");
            }
            if (listIndex == -1) {
                if (index >= primarySize) {
                    throw new IndexOutOfBoundsException("index >= size()");
                }
                return primaryArray[index].getWrappedObject();
            } else {
                if (index >= secondarySizes[listIndex]) {
                    throw new IndexOutOfBoundsException("index >= size()");
                }
                return secondaryArrays[listIndex][index].getWrappedObject();
            }
        }

    }


    private class InternalItemList extends AbstractList<MultiListItem> implements RandomAccess {

        private final int listIndex;

        InternalItemList(int listIndex) {
            this.listIndex = listIndex;
        }

        @Override
        public int size() {
            if (listIndex == -1) {
                return primarySize;
            } else {
                return secondarySizes[listIndex];
            }
        }

        @Override
        public MultiListItem get(int index) {
            if (index < 0) {
                throw new IndexOutOfBoundsException("index < 0");
            }
            if (listIndex == -1) {
                if (index >= primarySize) {
                    throw new IndexOutOfBoundsException("index >= size()");
                }
                return primaryArray[index];
            } else {
                if (index >= secondarySizes[listIndex]) {
                    throw new IndexOutOfBoundsException("index >= size()");
                }
                return secondaryArrays[listIndex][index];
            }
        }

    }

}
