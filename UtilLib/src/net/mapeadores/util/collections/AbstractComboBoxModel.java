/* UtilLib - Copyright (c) 2005 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.collections;

import javax.swing.ComboBoxModel;
import javax.swing.AbstractListModel;


/**
 *
 * @author Vincent Calame
 */
public abstract class AbstractComboBoxModel extends AbstractListModel implements ComboBoxModel {

    Object selectedItem;

    public AbstractComboBoxModel() {
    }

    public Object getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(Object anItem) {
        selectedItem = anItem;
    }

    protected void fireAllRemoved(int oldSize) {
        if (oldSize > 0) {
            fireIntervalRemoved(this, 0, oldSize - 1);
        }
    }

    protected void fireAllAdded() {
        int size = getSize();
        if (size > 0) {
            fireIntervalAdded(this, 0, size - 1);
        }
    }

    protected void fireObjectAdded(int index) {
        this.fireIntervalAdded(this, index, index);
    }

    protected void fireObjectRemoved(int index) {
        this.fireIntervalRemoved(this, index, index);
    }

    protected void fireObjectChanged(int index) {
        this.fireContentsChanged(this, index, index);
    }

    protected void fireIndexChanged(int oldindex, int newindex) {
        int index0 = Math.min(oldindex, newindex);
        int index1 = Math.max(oldindex, newindex);
        this.fireContentsChanged(this, index0, index1);
    }

}
