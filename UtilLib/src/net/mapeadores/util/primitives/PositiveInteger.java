/* UtilLib - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

import java.io.Serializable;


/**
 * Représente un entier strictement positif.
 *
 * @author Vincent Calame
 */
public final class PositiveInteger implements Comparable<PositiveInteger>, Serializable {

    private static final long serialVersionUID = 1L;
    private int intValue;

    /**
     *
     * @throws IllegalArgumentException si i <= 0
     */
    public PositiveInteger(int intValue) {
        if (intValue <= 0) {
            throw new IllegalArgumentException("intValue <= 0");
        }
        this.intValue = intValue;
    }

    public int intValue() {
        return intValue;
    }

    @Override
    public int hashCode() {
        return intValue;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PositiveInteger) {
            return (intValue == ((PositiveInteger) obj).intValue());
        }
        return false;
    }

    @Override
    public String toString() {
        return String.valueOf(intValue);
    }

    @Override
    public int compareTo(PositiveInteger other) {
        int otherint = other.intValue;
        if (intValue < otherint) {
            return -1;
        }
        if (intValue > otherint) {
            return 1;
        }
        return 0;
    }

}
