/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;


/**
 *
 * @author Vincent Calame
 */
public class CodeArrayBuilder<E> {

    private Object[] array;
    private int size = 0;

    public CodeArrayBuilder(int startLength) {
        this.array = new Object[startLength];
    }

    public void put(Integer code, E value) {
        int index = code - 1;
        if (index >= array.length) {
            Object[] temp = new Object[index * 2];
            System.arraycopy(array, 0, temp, 0, size);
            array = temp;
        }
        array[index] = value;
        size = Math.max(size, index + 1);
    }

    public void copy(Integer originCode, Integer destinationCode) {
        E originValue = (E) array[originCode - 1];
        put(destinationCode, originValue);
    }

    public E get(Integer code) {
        int index = code - 1;
        if ((index < 0) || (index >= array.length)) {
            return null;
        }
        return (E) array[index];
    }

    public Object[] toArray() {
        Object[] finalArray = new Object[size];
        System.arraycopy(array, 0, finalArray, 0, size);
        return finalArray;
    }

    public CodeArray<E> toCodeArray() {
        return new CodeArray<E>(toArray());
    }

}
