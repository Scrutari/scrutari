/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives.io;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;


/**
 *
 * @author Vincent Calame
 */
public final class PrimitivesIOFactory {

    private PrimitivesIOFactory() {

    }

    public static PrimitivesReader newReader(byte[] byteArray) {
        return new DataInputPrimitivesReader(new DataInputStream(new ByteArrayInputStream(byteArray)));
    }

    public static PrimitivesReader newReader(RandomAccessFile randomAccessFile) {
        return new RandomAccessPrimitivesReader(randomAccessFile);
    }

    public static PrimitivesReader newReader(DataInput dataInput) {
        return new DataInputPrimitivesReader(dataInput);
    }

    public static PrimitivesReader newReader(InputStream inputStream) {
        return new DataInputPrimitivesReader(new DataInputStream(inputStream));
    }

    public static PrimitivesWriter newWriter(RandomAccessFile randomAccessFile) {
        return new RandomAccessPrimitivesWriter(randomAccessFile);
    }

    public static PrimitivesWriter newWriter(DataOutput dataOutput) {
        return new DataOutputPrimitivesWriter(dataOutput);
    }

    public static PrimitivesWriter newWriter(OutputStream outputStream) {
        return new DataOutputPrimitivesWriter(new DataOutputStream(outputStream));
    }

    public static WriterBuffer newWriterBuffer() {
        return new WriterBuffer(new ByteArrayOutputStream());
    }

}
