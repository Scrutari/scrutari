/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives.io;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;


/**
 *
 * @author Vincent Calame
 */
public class WriterBuffer extends DataOutputPrimitivesWriter {

    private final ByteArrayOutputStream byteArrayOutputStream;

    public WriterBuffer(ByteArrayOutputStream byteArrayOutputStream) {
        super(new DataOutputStream(byteArrayOutputStream));
        this.byteArrayOutputStream = byteArrayOutputStream;
    }

    public byte[] toBytes() {
        return byteArrayOutputStream.toByteArray();
    }

}
