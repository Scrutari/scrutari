/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives.io;

import java.io.IOException;
import java.io.RandomAccessFile;
import net.mapeadores.util.primitives.PrimUtils;


/**
 *
 * @author Vincent Calame
 */
public class RandomAccessPrimitivesWriter extends DataOutputPrimitivesWriter {

    private final RandomAccessFile randomAccessFile;
    private final byte[] intBuffer = new byte[4];
    private final byte[] longBuffer = new byte[8];

    public RandomAccessPrimitivesWriter(RandomAccessFile randomAccessFile) {
        super(randomAccessFile);
        this.randomAccessFile = randomAccessFile;
    }

    @Override
    public void writeInt(int i) throws IOException {
        PrimUtils.convertInt(i, intBuffer);
        randomAccessFile.write(intBuffer, 0, 4);
    }

    @Override
    public void writeLong(long l) throws IOException {
        PrimUtils.convertLong(l, longBuffer);
        randomAccessFile.write(longBuffer, 0, 8);
    }

    @Override
    public void write(byte[] buffer, int offset, int length) throws IOException {
        randomAccessFile.write(buffer, offset, length);
    }


}
