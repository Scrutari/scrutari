/* UtilLib - Copyright (c) 2006-2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;


/**
 *
 * @author  Vincent Calame
 */
public class BitmaskUtils {

    static public final int ALLBITMASK = 0xFFFFFFFF;
    static int[] bitValueArray = new int[32];

    static {
        for (int i = 0; i < 31; i++) {
            bitValueArray[i] = (int) Math.pow(2, i);
        }
        bitValueArray[31] = 0x80000000;
    }

    static public int getBitValue(int rank) {
        return bitValueArray[rank];
    }

    static public boolean booleanValueAt(int mask, int rank) {
        if ((rank < 0) || (rank > 31)) {
            throw new IllegalArgumentException();
        }
        return (mask & bitValueArray[rank]) != 0;
    }

    /**
     * Retourne une valeur de 0 à 31 qui indique la première puissance de deux pour laquelle le bitmask est à la valeur true.
     */
    static public int getFirstRank(int mask) {
        for (int i = 0; i < 31; i++) {
            if ((bitValueArray[i] & mask) != 0) {
                return i;
            }
        }
        return -1;
    }

    static public boolean isLoneValue(int mask) {
        if (mask == bitValueArray[31]) {
            return true;
        }
        for (int i = 0; i < 31; i++) {
            int val = bitValueArray[i];
            if (mask < val) {
                return false;
            }
            if (mask == val) {
                return true;
            }
        }
        return false;
    }

}
