/* UtilLib - Copyright (c) 2012-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;


/**
 *
 * @author Vincent Calame
 */
public interface RangeDateFilter extends DateFilter {

    /**
     * Retourne -1 (date antérieure), 0 (date dans la plage) ou 1 (date
     * postérieure)
     */
    public int testInRange(FuzzyDate date);

    public FuzzyDate getMinDate();

    public FuzzyDate getMaxDate();

}
