/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 *
 * @author Vincent Calame
 */
public class CodeArray<E> implements Iterable {

    protected final Object[] array;

    public CodeArray(Object[] array) {
        this.array = array;
    }

    public E get(Integer code) {
        int index = code - 1;
        if ((index < 0) || (index >= array.length)) {
            return null;
        }
        return (E) array[index];
    }

    @Override
    public Iterator<E> iterator() {
        return new InternalIterator(array.length);
    }


    private class InternalIterator implements Iterator<E> {

        private final int size;
        private int cursor;

        private InternalIterator(int size) {
            this.size = size;
            this.cursor = 0;
        }

        @Override
        public E next() {
            if (cursor < size) {
                int index = cursor;
                cursor++;
                return (E) array[index];
            } else {
                throw new NoSuchElementException();
            }
        }

        @Override
        public boolean hasNext() {
            return (cursor < size);
        }

    }

}
