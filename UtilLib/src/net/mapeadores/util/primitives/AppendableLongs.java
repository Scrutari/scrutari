/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.primitives;

import java.util.AbstractList;


/**
 *
 * @author Vincent Calame
 */
public class AppendableLongs extends AbstractList<Long> implements Longs {

    private int size;
    private long[] array;

    public AppendableLongs() {
        this(10);
    }

    public AppendableLongs(int initialCapacity) {
        this.size = 0;
        this.array = new long[initialCapacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Long get(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    @Override
    public long getLong(int index) {
        if (index >= size) {
            throw new IndexOutOfBoundsException();
        }
        return array[index];
    }

    @Override
    public boolean add(Long l) {
        addLong(l);
        return true;
    }

    public void addLong(long l) {
        if (size == array.length) {
            long[] newArray = new long[size * 2];
            System.arraycopy(array, 0, newArray, 0, size);
            array = newArray;
        }
        array[size++] = l;
    }

}
