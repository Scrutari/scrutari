/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;

import javax.script.ScriptException;


/**
 *
 * @author Vincent Calame
 */
public class NestedScriptException extends RuntimeException {

    private final ScriptException scriptException;

    public NestedScriptException(ScriptException scriptException) {
        super(scriptException);
        this.scriptException = scriptException;
    }

    public ScriptException getScriptException() {
        return scriptException;
    }

    @Override
    public String getMessage() {
        return ExceptionsUtils.getMessage(scriptException);
    }

}
