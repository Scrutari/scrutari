/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;


/**
 * Exception encapsulant une exception lancée par une bibliothèque particulière.
 * Par exemple, une exception ParserConfigurationException de javax.xml.transform.*
 *
 * @author Vincent Calame
 */
public class NestedLibraryException extends RuntimeException {

    Exception exception;

    public NestedLibraryException(Exception exception) {
        super(exception);
        this.exception = exception;
    }

    public Exception getException() {
        return exception;
    }

    public String getMessage() {
        return ExceptionsUtils.getMessage(exception);
    }

}
