/* UtilLib - Copyright (c) 2007-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 *
 * @author Vincent Calame
 */
public class ExceptionsUtils {

    private ExceptionsUtils() {
    }

    public static void append(StringBuilder buf, String publicId, String systemId, int lineNumber, int columnNumber) {
        if (publicId != null) {
            buf.append(publicId);
            buf.append(":");
        }
        if (systemId != null) {
            buf.append(systemId);
            buf.append(":");
        }
        buf.append("line#");
        buf.append(lineNumber);
        buf.append(":col#");
        buf.append(columnNumber);
        buf.append(": ");
    }

    public static void append(StringBuilder buf, SAXParseException spe) {
        append(buf, spe.getPublicId(), spe.getSystemId(), spe.getLineNumber(), spe.getColumnNumber());
    }

    public static String getMessage(Exception e) {
        String message = e.getClass().getName();
        String msg = e.getLocalizedMessage();
        if (msg != null) {
            message = message + " : " + msg;
        }
        return message;
    }

    public static String getSAXExceptionMessage(SAXException saxException) {
        if (saxException instanceof SAXParseException) {
            StringBuilder buf = new StringBuilder();
            append(buf, (SAXParseException) saxException);
            buf.append(saxException.getLocalizedMessage());
            return buf.toString();
        } else {
            return getMessage(saxException);
        }
    }

}
