/* UtilLib - Copyright (c) 2007-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;

import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class NestedSAXException extends RuntimeException {

    private SAXException saxException;

    public NestedSAXException(SAXException saxException) {
        super(saxException);
        this.saxException = saxException;
    }

    public SAXException getSAXException() {
        return saxException;
    }

    @Override
    public String getMessage() {
        return ExceptionsUtils.getSAXExceptionMessage(saxException);
    }

}
