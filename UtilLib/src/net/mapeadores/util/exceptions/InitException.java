/* BDF - Copyright (c) 2008-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;


/**
 *
 * @author Vincent Calame
 */
public class InitException extends RuntimeException {

    public InitException(String message) {
        super(message);
    }

    public InitException(Exception e) {
        super(e);
    }

}
