/* UtilLib - Copyright (c) 2007-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;

import java.io.IOException;


/**
 * Encapsule une exception de la famille très courante IOException.
 *
 * @author Vincent Calame
 */
public class NestedIOException extends RuntimeException {

    private final IOException ioException;

    public NestedIOException(IOException ioException) {
        super(ioException);
        this.ioException = ioException;
    }

    public IOException getIOException() {
        return ioException;
    }

    @Override
    public String getMessage() {
        return ExceptionsUtils.getMessage(ioException);
    }

}
