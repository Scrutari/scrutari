/* UtilLib - Copyright (c) 2008 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;


/**
 * Encapsule des exceptions liées au chargement d'une classe (typiquement ClassNotFoundException).
 *
 * @author Vincent Calame
 */
public class NestedClassLoaderException extends RuntimeException {

    Exception exception;

    public NestedClassLoaderException(Exception exception) {
        super(exception);
        this.exception = exception;
    }

    public Exception getException() {
        return exception;
    }

    public String getMessage() {
        return ExceptionsUtils.getMessage(exception);
    }

}
