/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exceptions;


/**
 * Exception liée à l'absence d'une ressource interne au programme et qui ne devrait dont pas arriver.
 *
 * @author Vincent Calame
 */
public class InternalResourceException extends RuntimeException {

    public InternalResourceException(Exception e) {
        super(e.getClass().getName() + " cannot occur", e);
    }

    public InternalResourceException(String message) {
        super(message);
    }

}
