/* UtilLib - Copyright (c) 2005-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.awt.Font;
import java.awt.Rectangle;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import net.mapeadores.util.collections.ArrayUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.PositiveInteger;


/**
 * Ne contient jamais de valeurs nulles
 *
 * @author  Vincent Calame
 */
public class AbstractConf implements Conf, Cloneable {

    public final static short OBSOLETEKEY_RESULT = 2;
    public final static short OK_RESULT = 1;
    public final static short UNKNOWNKEY_RESULT = -1;
    public final static short BADVALUE_RESULT = -2;
    private final Map<String, Object> map = new LinkedHashMap<String, Object>();
    private ConfListener[] confListeners = new ConfListener[0];
    private boolean eventsQueued = false;
    private final List<String> queuedEventList = new ArrayList<String>();
    private final Map<String, String> unknownKeyMap = new LinkedHashMap<String, String>();

    public AbstractConf() {
    }

    protected void addAll(AbstractConf confMap) {
        for (Map.Entry<String, Object> entry : confMap.map.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            if (value instanceof Boolean) {
                putBoolean(key, ((Boolean) value));
            } else if (value instanceof File) {
                putDirectory(key, new File(((File) value).getPath()));
            } else if (value instanceof Integer) {
                putInteger(key, ((Integer) value));
            } else if (value instanceof PositiveInteger) {
                putPositiveInteger(key, ((PositiveInteger) value).intValue());
            } else if (value instanceof String) {
                putString(key, (String) value);
            } else if (value instanceof Font) {
                putFont(key, (Font) value);
            } else if (value instanceof Rectangle) {
                putRectangle(key, new Rectangle((Rectangle) value));
            } else if (value instanceof Lang) {
                putLang(key, (Lang) value);
            } else if (value instanceof MessageFormat) {
                putMessageFormat(key, (MessageFormat) ((MessageFormat) value).clone());
            } else if (value instanceof FileWrapper) {
                putFile(key, ((FileWrapper) value).getFile());
            } else if (value instanceof String[]) {
                String[] val = (String[]) value;
                String[] copy = new String[val.length];
                System.arraycopy(val, 0, copy, 0, val.length);
                putStringArray(key, copy);
            } else if (value instanceof URLWrapper) {
                putURL(key, ((URLWrapper) value).getURL());
            } else {
                throw new IllegalArgumentException("La valeur de clé " + key + " appartient à un classe (" + value.getClass().getName() + ") inconnue de Conf Map");
            }
        }
    }

    public void addConfListener(ConfListener confListener) {
        confListeners = (ConfListener[]) ArrayUtils.addUnique(confListeners, confListener, new ConfListener[confListeners.length + 1]);
    }

    public void removeConfListener(ConfListener confListener) {
        if (confListeners.length > 0) {
            confListeners = (ConfListener[]) ArrayUtils.removeUnique(confListeners, confListener, new ConfListener[confListeners.length - 1]);
        }
    }

    protected void fireConfChanged(String param) {
        if (eventsQueued) {
            queuedEventList.add(param);
        } else {
            ConfEvent confEvent = new ConfEvent(this, param);
            for (ConfListener confListener : confListeners) {
                confListener.confChanged(confEvent);
            }
        }
    }

    protected void putBoolean(String name, boolean b) {
        put(name, b);
    }

    protected void putDirectory(String name, File f) {
        put(name, f);
    }

    protected void putInteger(String name, int i) {
        put(name, i);
    }

    protected void putPositiveInteger(String name, int i) {
        put(name, new PositiveInteger(i));
    }

    protected void putString(String name, String s) {
        put(name, s);
    }

    protected void putFont(String name, Font f) {
        put(name, f);
    }

    protected void putRectangle(String name, Rectangle rectangle) {
        put(name, rectangle);
    }

    protected void putLang(String name, Lang lang) {
        put(name, lang);
    }

    protected void putMessageFormat(String name, MessageFormat messageFormat) {
        put(name, messageFormat);
    }

    protected void putFile(String name, File f) {
        put(name, new FileWrapper(f));
    }

    protected void putStringArray(String name, String[] stringArray) {
        put(name, stringArray);
    }

    protected void putURL(String name, URL url) {
        put(name, new URLWrapper(url));
    }

    /**
     * Doit être surclassé par des classes parentes afin d'indiquer qu'un paramètre ne doit pas être enregistré.
     * Par défaut, retourne false;
     */
    @Override
    public boolean isTransient(String name) {
        return false;
    }

    @Override
    public boolean getBoolean(String name) {
        Boolean bool = (Boolean) get(name);

        return bool;
    }

    @Override
    public File getDirectory(String name) {
        File file = (File) get(name);
        return file;
    }

    @Override
    public int getInteger(String name) {
        Integer itg = (Integer) get(name);
        return itg;
    }

    @Override
    public int getPositiveInteger(String name) {
        PositiveInteger itg = (PositiveInteger) get(name);
        return itg.intValue();
    }

    @Override
    public String getString(String name) {
        String s = (String) get(name);
        return s;
    }

    @Override
    public Font getFont(String name) {
        Font f = (Font) get(name);
        return f;
    }

    @Override
    public Rectangle getRectangle(String name) {
        Rectangle rectangle = (Rectangle) get(name);
        return rectangle;
    }

    @Override
    public Lang getLang(String name) {
        Lang lang = (Lang) get(name);
        return lang;
    }

    @Override
    public MessageFormat getMessageFormat(String name) {
        MessageFormat mf = (MessageFormat) get(name);
        return (MessageFormat) mf.clone();
    }

    /*
     * Peut avoir une valeur nulle.
     */
    @Override
    public File getFile(String name) {
        FileWrapper fileWrapper = (FileWrapper) get(name);
        return fileWrapper.getFile();
    }

    /**
     * N'est jamais nulle. Ce n'est pas une copie d'une tableau mais le tableau lui-même, la modification directe du tableau n'est pas conseillée.
     */
    @Override
    public String[] getStringArray(String name) {
        String[] stringArray = (String[]) get(name);
        return stringArray;
    }

    /**
     * Peut avoir une valeur nulle.
     */
    @Override
    public URL getURL(String name) {
        URLWrapper urlWrapper = (URLWrapper) get(name);
        return urlWrapper.getURL();
    }

    @Override
    public Map<String, String> toStringMap(boolean excludeTransient) {
        Map<String, String> result = new LinkedHashMap<String, String>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            if ((excludeTransient) && (isTransient(key))) {
                continue;
            }
            result.put(key, ConfUtils.toString(entry.getValue()));
        }
        for (Map.Entry<String, String> entry : unknownKeyMap.entrySet()) {
            String key = entry.getKey();
            if ((excludeTransient) && (isTransient(key))) {
                continue;
            }
            result.put(key, entry.getValue());
        }
        return result;
    }

    public void setBoolean(String name, boolean b) {
        Boolean bool = (Boolean) get(name);
        if (bool != b) {
            put(name, b);
            fireConfChanged(name);
        }
    }

    public void setDirectory(String name, File f) {
        File file = (File) get(name);
        if (!f.isDirectory()) {
            f = f.getParentFile();
            if (f == null) {
                throw new IllegalArgumentException("File f is not a directory and has no parent file");
            }
        }
        if (!file.equals(f)) {
            put(name, f);
            fireConfChanged(name);
        }
    }

    public void setInteger(String name, int i) {
        Integer itg = (Integer) get(name);
        if (itg != i) {
            put(name, i);
            fireConfChanged(name);
        }
    }

    public void setPositiveInteger(String name, int i) {
        if (i <= 0) {
            return;
        }
        PositiveInteger itg = (PositiveInteger) get(name);
        if (itg.intValue() != i) {
            put(name, new PositiveInteger(i));
            fireConfChanged(name);
        }
    }

    public void setString(String name, String s) {
        String value = (String) get(name);
        if (!value.equals(s)) {
            put(name, s);
            fireConfChanged(name);
        }
    }

    public void setFont(String name, Font f) {
        Font value = (Font) get(name);
        if (!value.equals(f)) {
            put(name, f);
            fireConfChanged(name);
        }
    }

    public void setRectangle(String name, Rectangle rectangle) {
        Rectangle value = (Rectangle) get(name);
        if (!value.equals(rectangle)) {
            put(name, rectangle);
            fireConfChanged(name);
        }
    }

    public void setLang(String name, Lang lang) {
        Lang currentLang = (Lang) get(name);
        if (!currentLang.equals(lang)) {
            put(name, lang);
            fireConfChanged(name);
        }
    }

    public void setMessageFormat(String name, MessageFormat messageFormat) {
        MessageFormat mf = (MessageFormat) get(name);
        if (!mf.equals(messageFormat)) {
            put(name, messageFormat);
            fireConfChanged(name);
        }
    }

    public void setFile(String name, File f) {
        FileWrapper fileWrapper = (FileWrapper) get(name);
        if ((f != null) && (f.isDirectory())) {
            throw new IllegalArgumentException("File f is a directory");
        }
        if (fileWrapper.replaceFile(f)) {
            fireConfChanged(name);
        }
    }

    public void setStringArray(String name, String[] stringArray) {
        String[] currentArray = (String[]) get(name);
        boolean dif = (currentArray.length != stringArray.length);
        if (!dif) {
            for (int i = 0; i < currentArray.length; i++) {
                if (!currentArray[i].equals(stringArray[i])) {
                    dif = true;
                    break;
                }
            }
        }
        if (dif) {
            put(name, stringArray);
            fireConfChanged(name);
        }
    }

    public void setURL(String name, URL url) {
        URLWrapper urlBundle = (URLWrapper) get(name);
        if (urlBundle.replaceURL(url)) {
            fireConfChanged(name);
        }
    }

    public String getAlias(String name) {
        return null;
    }

    public boolean isObsolete(String name) {
        return false;
    }

    public short set(String name, String value) {
        if (value == null) {
            return BADVALUE_RESULT;
        }
        Object currentValue = map.get(name);
        if (currentValue == null) {
            String alias = getAlias(name);
            if (alias == null) {
                if (!isObsolete(name)) {
                    unknownKeyMap.put(name, value);
                    return UNKNOWNKEY_RESULT;
                } else {
                    return OBSOLETEKEY_RESULT;
                }
            } else {
                name = alias;
                currentValue = map.get(alias);
            }
        }
        if (currentValue instanceof Boolean) {
            Boolean b = ConfUtils.booleanTest(value);
            if (b == null) {
                return BADVALUE_RESULT;
            }
            setBoolean(name, b);
        } else if (currentValue instanceof File) {
            File f = ConfUtils.directoryTest(value);
            if (f == null) {
                return BADVALUE_RESULT;
            }
            setDirectory(name, f);
        } else if (currentValue instanceof Integer) {
            Integer itg = ConfUtils.integerTest(value);
            if (itg == null) {
                return BADVALUE_RESULT;
            }
            setInteger(name, itg);
        } else if (currentValue instanceof PositiveInteger) {
            PositiveInteger itg = ConfUtils.positiveIntegerTest(value);
            if (itg == null) {
                return BADVALUE_RESULT;
            }
            setPositiveInteger(name, itg.intValue());
        } else if (currentValue instanceof String) {
            setString(name, value);
        } else if (currentValue instanceof Font) {
            setFont(name, ConfUtils.toFont(value));
        } else if (currentValue instanceof Rectangle) {
            setRectangle(name, ConfUtils.toRectangle(value));
        } else if (currentValue instanceof Lang) {
            Lang lang = ConfUtils.langTest(value);
            if (lang == null) {
                return BADVALUE_RESULT;
            }
            setLang(name, lang);
        } else if (currentValue instanceof MessageFormat) {
            MessageFormat messageFormat = ConfUtils.messageFormatTest(value);
            if (messageFormat == null) {
                return BADVALUE_RESULT;
            }
            setMessageFormat(name, messageFormat);
        } else if (currentValue instanceof FileWrapper) {
            if (value.length() == 0) {
                setFile(name, null);
            } else {
                File f = new File(value);
                if (f.isDirectory()) {
                    return OK_RESULT;
                }
                setFile(name, f);
            }
        } else if (currentValue instanceof String[]) {
            setStringArray(name, ConfUtils.toStringArray(value));
        } else if (currentValue instanceof URLWrapper) {
            if (value.length() == 0) {
                setURL(name, null);
            } else {
                try {
                    URL url = new URL(value);
                    setURL(name, url);
                } catch (MalformedURLException mue) {
                    return BADVALUE_RESULT;
                }
            }
        } else {
            throw new IllegalArgumentException("La valeur de clé " + name + " appartient à un classe (" + value.getClass().getName() + ") inconnue de Conf Map");
        }
        return OK_RESULT;
    }

    public void saveIntoProperties(Properties properties, String suffixe) {
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            String key = entry.getKey();
            if (!isTransient(key)) {
                if (suffixe != null) {
                    key = suffixe + key;
                }
                properties.put(key, ConfUtils.toString(entry.getValue()));
            }
        }
    }

    public void queueEvents() {
        eventsQueued = true;
    }

    public void flushEvents() {
        if (eventsQueued) {
            eventsQueued = false;
            if (queuedEventList.size() > 0) {
                ConfEvent confEvent = new ConfEvent(this, queuedEventList);
                queuedEventList.clear();
                for (ConfListener confListener : confListeners) {
                    confListener.confChanged(confEvent);
                }
            }
        }
    }

    private void put(String name, Object value) {
        if (value == null) {
            throw new IllegalArgumentException("value is null for " + name);
        }
        map.put(name, value);
    }

    private Object get(String name) {
        Object obj = map.get(name);
        if (obj == null) {
            throw new IllegalArgumentException("illegal name value : " + name);
        }
        return obj;
    }


    private static class FileWrapper {

        private File file;

        private FileWrapper(File f) {
            this.file = f;
        }

        @Override
        public String toString() {
            if (file == null) {
                return "";
            } else {
                return file.getPath();
            }
        }

        private File getFile() {
            return file;
        }

        private boolean replaceFile(File f) {
            if (file == null) {
                if (f == null) {
                    return false;
                } else {
                    file = f;
                    return true;
                }
            }
            if (f == null) {
                file = f;
                return true;
            }
            if (f.equals(file)) {
                return false;
            }
            file = f;
            return true;
        }

    }


    private static class URLWrapper {

        private URL url;

        private URLWrapper(URL url) {
            this.url = url;
        }

        @Override
        public String toString() {
            if (url == null) {
                return "";
            } else {
                return url.toString();
            }
        }

        private URL getURL() {
            return url;
        }

        private boolean replaceURL(URL u) {
            if (url == null) {
                if (u == null) {
                    return false;
                } else {
                    url = u;
                    return true;
                }
            }
            if (u == null) {
                url = u;
                return true;
            }
            if (u.equals(url)) {
                return false;
            }
            url = u;
            return true;
        }

    }

}
