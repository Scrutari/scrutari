/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.io.File;
import java.text.MessageFormat;
import java.text.ParseException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.PositiveInteger;
import net.mapeadores.util.text.tableparser.CsvParameters;
import net.mapeadores.util.text.tableparser.TableParser;


/**
 *
 * @author Vincent Calame
 */
public final class ConfUtils {

    private ConfUtils() {
    }

    public static Boolean booleanTest(String value) {
        if ((value.equalsIgnoreCase("true")) || (value.equals("1"))) {
            return Boolean.TRUE;
        }
        if ((value.equalsIgnoreCase("false")) || (value.equals("0"))) {
            return Boolean.FALSE;
        }
        return null;
    }

    public static Integer integerTest(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static PositiveInteger positiveIntegerTest(String value) {
        try {
            int i = Integer.parseInt(value);
            if (i <= 0) {
                return null;
            }
            return new PositiveInteger(i);
        } catch (NumberFormatException nfe) {
            return null;
        }
    }

    public static Lang langTest(String value) {
        try {
            return Lang.parse(value);
        } catch (ParseException nfe) {
            return null;
        }
    }

    public static MessageFormat messageFormatTest(String value) {
        try {
            return new MessageFormat(value);
        } catch (IllegalArgumentException iae) {
            return null;
        }
    }

    public static File directoryTest(String value) {
        File file = new File(value);
        if (!file.isDirectory()) {
            file = file.getParentFile();
            if (file == null) {
                return null;
            }
        }
        if (!file.exists()) {
            try {
                file.mkdirs();
            } catch (SecurityException ioe) {
                return null;
            }
        }
        return file;
    }

    public static String[] toStringArray(String string) {
        if (string.length() == 0) {
            return new String[0];
        }
        String[][] lignes = TableParser.parse(string, new CsvParameters());
        int lignesLength = lignes.length;
        if (lignesLength == 0) {
            return new String[0];
        }
        if (lignesLength == 1) {
            return lignes[0];
        }
        int total = 0;
        for (int i = 0; i < lignesLength; i++) {
            total = total + lignes[i].length;
        }
        String[] result = new String[total];
        int p = 0;
        for (int i = 0; i < lignesLength; i++) {
            String[] ligne = lignes[i];
            for (String token : ligne) {
                result[p] = token;
                p++;
            }
        }
        return result;
    }

    public static Rectangle toRectangle(String string) {
        String[] tab = string.split(",");
        if (tab.length != 4) {
            return new Rectangle(60, 60, 420, 420);
        }
        int[] intValues = new int[4];
        for (int i = 0; i < 4; i++) {
            try {
                intValues[i] = Integer.parseInt(tab[i]);
            } catch (NumberFormatException nfe) {
                if (i < 2) {
                    intValues[i] = 60;
                } else {
                    intValues[i] = 420;
                }
            }
        }
        return new Rectangle(intValues[0], intValues[1], intValues[2], intValues[3]);
    }

    public static Font toFont(String string) {
        String fontName = null;
        String[] tab = string.split(";");
        int fontStyle = 0;
        int fontSize = 11;
        for (String token : tab) {
            token = token.trim();
            if (!token.startsWith("font-")) {
                continue;
            }
            token = token.substring(5);
            int idx = token.indexOf(':');
            if (idx < 0) {
                continue;
            }
            String value = token.substring(idx + 1).trim();
            if (token.startsWith("family")) {
                int idxvirg = value.indexOf(',');
                if (idxvirg != - 1) {
                    value = value.substring(0, idxvirg).trim();
                }
                if (value.startsWith("\"")) {
                    value = value.substring(1);
                }
                if (value.endsWith("\"")) {
                    value = value.substring(0, value.length() - 1);
                }
                fontName = value;
            } else if (token.startsWith("size")) {
                try {
                    if (value.endsWith("pt")) {
                        fontSize = (int) Float.parseFloat(value.substring(0, value.length() - 2));
                    }
                } catch (NumberFormatException nfe) {
                }
                if (fontSize < 4) {
                    fontSize = 4;
                }
            } else if (token.startsWith("style")) {
                if (value.equals("italic")) {
                    fontStyle = fontStyle | Font.ITALIC;
                }
            } else if (token.startsWith("weight")) {
                if (value.equals("bold")) {
                    fontStyle = fontStyle | Font.BOLD;
                }
            }

        }
        if (fontName != null) {
            String[] fontes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
            boolean existe = false;
            for (String fonte : fontes) {
                if (fonte.compareToIgnoreCase(fontName) == 0) {
                    fontName = fonte;
                    existe = true;
                    break;
                }
            }
            if (!existe) {
                fontName = null;
            }
        }
        if (fontName == null) {
            fontName = "SansSerif";
        }
        return new Font(fontName, fontStyle, fontSize);
    }

    public static String toString(Font font) {
        String fontname = font.getName();
        float fontsize = font.getSize();
        int fontstyle = font.getStyle();
        StringBuilder buf = new StringBuilder();
        buf.append("font-family:");
        if (fontname.indexOf(' ') > -1) {
            buf.append('"');
            buf.append(fontname);
            buf.append('"');
        } else {
            buf.append(fontname);
        }
        buf.append(';');
        buf.append("font-size:");
        buf.append(String.valueOf(fontsize));
        buf.append("pt");
        buf.append(';');
        buf.append("font-style:");
        if ((fontstyle & Font.ITALIC) != 0) {
            buf.append("italic");
        } else {
            buf.append("normal");
        }
        buf.append(';');
        buf.append("font-weight:");
        if ((fontstyle & Font.BOLD) != 0) {
            buf.append("bold");
        } else {
            buf.append("normal");
        }
        buf.append(';');
        return buf.toString();
    }

    public static String toString(Object value) {
        if (value instanceof Boolean) {
            if ((Boolean) value) {
                return "true";
            } else {
                return "false";
            }
        }
        if (value instanceof File) {
            return ((File) value).getPath();
        }
        if (value instanceof Font) {
            return toString((Font) value);
        }
        if (value instanceof Rectangle) {
            return toString((Rectangle) value);
        }
        if (value instanceof MessageFormat) {
            return ((MessageFormat) value).toPattern();
        }
        if (value instanceof String[]) {
            return toString((String[]) value);
        }
        return value.toString();
    }

    public static String toString(Rectangle rectangle) {
        StringBuilder buf = new StringBuilder();
        buf.append(rectangle.x);
        buf.append(',');
        buf.append(rectangle.y);
        buf.append(',');
        buf.append(rectangle.width);
        buf.append(',');
        buf.append(rectangle.height);
        return buf.toString();
    }

    public static String toString(String[] stringArray) {
        StringBuilder buf = new StringBuilder();
        for (int i = 0; i < stringArray.length; i++) {
            if (i != 0) {
                buf.append(',');
            }
            boolean quoted = false;
            StringBuilder smallBuf = new StringBuilder();
            String strg = stringArray[i];
            for (int j = 0; j < strg.length(); j++) {
                char carac = strg.charAt(j);
                if (carac == '\"') {
                    smallBuf.append("\\\"");
                    quoted = true;
                } else if (carac == '\\') {
                    smallBuf.append("\\\\");
                    quoted = true;
                } else {
                    smallBuf.append(carac);
                    if ((carac > '~') || (carac < '!') || (carac == ',') || (carac == ';') || (carac == '\'')) {
                        quoted = true;
                    }
                }
            }
            if (quoted) {
                buf.append('\"');
            }
            buf.append(smallBuf.toString());
            if (quoted) {
                buf.append('\"');
            }
        }
        return buf.toString();
    }

}
