/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.awt.Font;
import java.awt.Rectangle;
import java.io.File;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface Conf {

    /**
     * Indique que le paramère de nom name ne doit pas être conservé d'une session à une autre
     */
    public boolean isTransient(String name);

    /**
     * Doit être un copie modifiable directement
     */
    public Map<String, String> toStringMap(boolean excludeTransient);

    public boolean getBoolean(String name);

    public File getDirectory(String name);

    public int getInteger(String name);

    public int getPositiveInteger(String name);

    public String getString(String name);

    public Font getFont(String name);

    public Rectangle getRectangle(String name);

    public Lang getLang(String name);

    public MessageFormat getMessageFormat(String name);

    public File getFile(String name);

    public String[] getStringArray(String name);

    public URL getURL(String name);

}
