/* UtilLib - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.util.function.Consumer;
import net.mapeadores.util.xml.DOMUtils;
import org.w3c.dom.Element;


/**
 *
 * @author Vincent Calame
 */
public class ConfDOMReader {

    private final ConfDOMErrorHandler errorHandler;
    private final AbstractConf conf;

    public ConfDOMReader(AbstractConf conf, ConfDOMErrorHandler errorHandler) {
        this.errorHandler = errorHandler;
        this.conf = conf;
    }

    public void readConf(Element element_xml) {
        DOMUtils.readChildren(element_xml, new ConfConsumer());
    }


    private class ConfConsumer implements Consumer<Element> {

        private ConfConsumer() {

        }

        @Override
        public void accept(Element element) {
            String tagName = element.getTagName();
            if (tagName.equals("param")) {
                String name = element.getAttribute("name");
                if (name.length() == 0) {
                    errorHandler.missingParamNameAttributeError();
                    return;
                }
                String value = element.getAttribute("value");
                short result = conf.set(name, value);
                if (result == AbstractConf.UNKNOWNKEY_RESULT) {
                    errorHandler.unknownKeyWarning(name);
                } else if (result == AbstractConf.BADVALUE_RESULT) {
                    errorHandler.badValueError(name, value);
                }
            } else {
                errorHandler.unknownTagWarning(tagName);
            }
        }

    }

}
