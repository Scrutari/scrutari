/* UtilLib - Copyright (c) 2009-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.io.IOException;
import java.util.Map;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ConfXMLPart extends XMLPart {

    public ConfXMLPart(XMLWriter xmlWriter) {
        super(xmlWriter);
    }

    public void addConf(Conf conf) throws IOException {
        openTag("conf");
        Map<String, String> map = conf.toStringMap(true);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            startOpenTag("param");
            addAttribute("name", entry.getKey());
            addAttribute("value", entry.getValue());
            closeEmptyTag();
        }
        closeTag("conf");
    }

}
