/* UtilLib - Copyright (c) 2005-2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.util.EventListener;


/**
 *
 * @author  Vincent Calame
 */
public interface ConfListener extends EventListener {

    public void confChanged(ConfEvent confEvent);

}
