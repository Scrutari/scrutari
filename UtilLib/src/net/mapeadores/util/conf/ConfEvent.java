/* UtilLib - Copyright (c) 2005-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.util.EventObject;
import java.util.List;


/**
 *
 * @author  Vincent Calame
 */
public class ConfEvent extends EventObject {

    private final Conf conf;
    private final String[] paramNameArray;

    public ConfEvent(Conf conf, String paramName) {
        super(conf);
        this.conf = conf;
        this.paramNameArray = new String[1];
        this.paramNameArray[0] = paramName;
    }

    public ConfEvent(Conf conf, List<String> paramnameList) {
        super(conf);
        this.conf = conf;
        this.paramNameArray = paramnameList.toArray(new String[paramnameList.size()]);
    }

    public String getParamName(int i) {
        return paramNameArray[i];
    }

    public int getParamNameCount() {
        return paramNameArray.length;
    }

    public Conf getConf() {
        return conf;
    }

}
