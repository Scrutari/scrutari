/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conf;

import java.awt.Font;
import java.awt.Rectangle;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.PositiveInteger;


/**
 *
 * @author Vincent Calame
 */
public class StringMapConf implements Conf {

    private final Map<String, String> stringMap = new HashMap<String, String>();
    private final Conf defaultConf;

    public StringMapConf(Conf defaultConf) {
        this.defaultConf = defaultConf;
    }

    @Override
    public boolean isTransient(String name) {
        return defaultConf.isTransient(name);
    }

    @Override
    public Map<String, String> toStringMap(boolean excludeTransient) {
        Map<String, String> map = defaultConf.toStringMap(excludeTransient);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String name = entry.getKey();
            if ((excludeTransient) && (isTransient(name))) {
                continue;
            }
            map.put(name, entry.getValue());
        }
        return map;
    }

    @Override
    public boolean getBoolean(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            Boolean bool = ConfUtils.booleanTest(value);
            if (bool != null) {
                return bool;
            }
        }
        return defaultConf.getBoolean(name);
    }

    @Override
    public File getDirectory(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            File dir = ConfUtils.directoryTest(value);
            if (dir != null) {
                return dir;
            }
        }
        return defaultConf.getDirectory(name);
    }

    @Override
    public int getInteger(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            Integer itg = ConfUtils.integerTest(value);
            if (itg != null) {
                return itg;
            }
        }
        return defaultConf.getInteger(name);
    }

    @Override
    public int getPositiveInteger(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            PositiveInteger positiveInteger = ConfUtils.positiveIntegerTest(value);
            if (positiveInteger != null) {
                return positiveInteger.intValue();
            }
        }
        return defaultConf.getPositiveInteger(name);
    }

    @Override
    public String getString(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            return value;
        }
        return defaultConf.getString(name);
    }

    @Override
    public Font getFont(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            return ConfUtils.toFont(value);
        }
        return defaultConf.getFont(name);
    }

    @Override
    public Rectangle getRectangle(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            return ConfUtils.toRectangle(value);
        }
        return defaultConf.getRectangle(name);
    }

    @Override
    public Lang getLang(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            Lang lang = ConfUtils.langTest(value);
            if (lang != null) {
                return lang;
            }
        }
        return defaultConf.getLang(name);
    }

    @Override
    public MessageFormat getMessageFormat(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            MessageFormat messageFormat = ConfUtils.messageFormatTest(value);
            if (messageFormat != null) {
                return messageFormat;
            }
        }
        return defaultConf.getMessageFormat(name);
    }

    @Override
    public File getFile(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            File f = new File(value);
            if (!f.isDirectory()) {
                return f;
            }
        }
        return defaultConf.getFile(name);
    }

    @Override
    public String[] getStringArray(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            return ConfUtils.toStringArray(value);
        }
        return defaultConf.getStringArray(name);
    }

    @Override
    public URL getURL(String name) {
        String value = stringMap.get(name);
        if (value != null) {
            try {
                URL url = new URL(value);
                return url;
            } catch (MalformedURLException mue) {
            }
        }
        return defaultConf.getURL(name);
    }

    public void put(String name, String value) {
        if (value == null) {
            stringMap.remove(name);
        } else {
            stringMap.put(name, value);
        }
    }

}
