/* UtilLib - Copyright (c) 2007-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.instruction;


/**
 *
 * @author Vincent Calame
 */
public interface InstructionErrorHandler {

    public void invalidAsciiCharacterError(String part, int row, int col);

    public void invalidEndCharacterError(String part, int row, int col);

    public void invalidSeparatorCharacterError(String part, int row, int col);

}
