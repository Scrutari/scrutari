/* UtilLib - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.instruction;


/**
 * Cette interface fournit des constructeurs alternatifs au constructeur par
 * défaut. Elle permet notamment à une extension de proposer ses propres
 * formatages voire de remplacer le comportement d'un formatage existant.
 *
 * @author Vincent Calame
 */
public interface InstructionResolverProvider {

    /**
     * Prend en argument une classe de formateur (en général les interfaces
     * étendant l'interface Formatter) et retourne un constructeur de
     * formateurs. Retourne null s'il n'y a pas de constructeur de formateurs
     * alternatif pour la classe donnée.
     */
    public InstructionResolver getInstructionResolver(Class destinationClass, Object optionObject);


    public default InstructionResolver getInstructionResolver(Class destinationClass) {
        return getInstructionResolver(destinationClass, null);
    }

}
