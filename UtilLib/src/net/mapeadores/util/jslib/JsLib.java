/* UtilLib - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.jslib;

import java.util.List;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface JsLib {

    public List<JsLib> getDependencyList();

    public List<RelativePath> getJsScriptList();

    public List<ThirdLib> getThirdLibList();

    public List<TemplateFamily> getTemplateFamilyList();

}
