/* @@@@@APPLICATION@@@@@ - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.jslib;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThirdLibBuilder {

    private final String name;
    private final Set<String> extensionSet = new LinkedHashSet<String>();

    public ThirdLibBuilder(String name) {
        if (name == null) {
            throw new IllegalArgumentException("name is null");
        }
        this.name = name;
    }

    public ThirdLibBuilder addExtension(String extensionName) {
        extensionSet.add(extensionName);
        return this;
    }

    public ThirdLib toThirdLib() {
        List<String> finalList = StringUtils.toList(extensionSet);
        return new InternalThirdLib(name, finalList);
    }

    public static ThirdLibBuilder init(String name) {
        return new ThirdLibBuilder(name);
    }


    private static class InternalThirdLib implements ThirdLib {

        private final String name;
        private final List<String> extensionNameList;

        public InternalThirdLib(String name, List<String> extensionNameList) {
            this.name = name;
            this.extensionNameList = extensionNameList;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<String> getExtensionNameList() {
            return extensionNameList;
        }

    }

}
