/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.jslib;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class JsLibManager {

    private final Set<JsLib> jsLibSet = new HashSet<JsLib>();
    private final Map<String, ThirdLibBuffer> thirdLibMap = new HashMap<String, ThirdLibBuffer>();
    private final SortedMap<String, TemplateFamily> templateMap = new TreeMap<String, TemplateFamily>();
    private final Set<RelativePath> jsScriptSet = new LinkedHashSet();

    public JsLibManager() {

    }

    public boolean hasTemplate() {
        return (!templateMap.isEmpty());
    }

    public boolean hasThirdLib() {
        return (!thirdLibMap.isEmpty());
    }

    public ThirdLib getThirdLib(String name) {
        return thirdLibMap.get(name);
    }

    public Collection<TemplateFamily> getTemplates() {
        return templateMap.values();
    }

    public Collection<RelativePath> getScriptPaths() {
        return jsScriptSet;
    }


    public void addJsLib(JsLib jsLib) {
        if (jsLibSet.contains(jsLib)) {
            return;
        } else {
            jsLibSet.add(jsLib);
        }
        for (JsLib dependency : jsLib.getDependencyList()) {
            addJsLib(dependency);
        }
        for (RelativePath relativePath : jsLib.getJsScriptList()) {
            jsScriptSet.add(relativePath);
        }
        for (ThirdLib thirdLib : jsLib.getThirdLibList()) {
            String name = thirdLib.getName();
            ThirdLibBuffer buffer = thirdLibMap.get(name);
            if (buffer == null) {
                buffer = new ThirdLibBuffer(name);
                thirdLibMap.put(name, buffer);
            }
            buffer.addAll(thirdLib.getExtensionNameList());
        }
        for (TemplateFamily templateFamily : jsLib.getTemplateFamilyList()) {
            templateMap.put(templateFamily.getName(), templateFamily);
        }
    }


    private static class ThirdLibBuffer implements ThirdLib {

        private final String name;
        private final List<String> extensionNameList = new ArrayList<String>();

        private ThirdLibBuffer(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<String> getExtensionNameList() {
            return extensionNameList;
        }

        private void addAll(Collection<String> extensionNames) {
            for (String extensionName : extensionNames) {
                if (!extensionNameList.contains(extensionName)) {
                    extensionNameList.add(extensionName);
                }
            }
        }

    }

}
