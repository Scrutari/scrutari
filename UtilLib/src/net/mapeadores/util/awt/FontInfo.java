/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.awt;

import java.awt.Font;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.font.LineMetrics;
import java.awt.font.FontRenderContext;


/**
 *
 * @author Vincent Calame
 */
public class FontInfo {

    private int ascent;
    private int descent;
    private int lineHeight;
    private int mCharWidth;

    public FontInfo(Font font, Component reference) {
        FontMetrics fontMetrics = reference.getFontMetrics(font);
        ascent = fontMetrics.getAscent();
        descent = fontMetrics.getDescent();
        lineHeight = fontMetrics.getHeight();
        mCharWidth = fontMetrics.stringWidth("m");
    }

    public FontInfo(Font font, FontRenderContext fontRenderContext) {
        LineMetrics lm = font.getLineMetrics("Pplj", fontRenderContext);
        ascent = Math.round(lm.getAscent());
        descent = Math.round(lm.getDescent());
        lineHeight = (int) Math.ceil(lm.getHeight());
        mCharWidth = (int) font.getStringBounds("m", fontRenderContext).getWidth();
    }

    public int getAscent() {
        return ascent;
    }

    public int getDescent() {
        return descent;
    }

    public int getLineHeight() {
        return lineHeight;
    }

    public int getMCharWidth() {
        return mCharWidth;
    }

}
