/* UtilLib - Copyright (c) 2005-2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.awt;

import java.awt.Color;


/**
 *
 * @author  Vincent Calame
 */
public class ColorSkin {

    private Color backgroundColor = null;
    private Color selectedBackgroundColor = null;
    private Color textColor = null;
    private Color selectedTextColor = null;
    private ColorSkin parent;
    private String name;

    public ColorSkin() {
        backgroundColor = Color.WHITE;
        selectedBackgroundColor = new Color(128, 128, 255);
        textColor = Color.BLACK;
        selectedTextColor = Color.YELLOW;
        name = "_default";
    }

    ColorSkin(ColorSkin parent, String name) {
        this.name = name;
        this.parent = parent;
    }

    public ColorSkin(String name, Color txtColor, Color bgColor, Color selTxtColor, Color selBgColor) {
        this.name = name;
        if (txtColor != null) {
            this.textColor = txtColor;
        } else {
            textColor = Color.BLACK;
        }
        if (bgColor != null) {
            this.backgroundColor = bgColor;
        } else {
            backgroundColor = Color.WHITE;
        }
        if (selTxtColor != null) {
            this.selectedTextColor = selTxtColor;
        } else {
            selectedTextColor = Color.YELLOW;
        }
        if (selBgColor != null) {
            this.selectedBackgroundColor = selBgColor;
        } else {
            selectedBackgroundColor = new Color(128, 128, 255);
        }
    }

    public ColorSkin(String name, ColorSkin colorSkin) {
        this.name = name;
        this.textColor = colorSkin.textColor;
        this.backgroundColor = colorSkin.backgroundColor;
        this.selectedTextColor = colorSkin.selectedTextColor;
        this.selectedBackgroundColor = colorSkin.selectedBackgroundColor;
    }

    public String getSkinName() {
        return name;
    }

    public Color getTextColor() {
        if (textColor == null) {
            return parent.textColor;
        }
        return textColor;
    }

    public Color getBackgroundColor() {
        if (backgroundColor == null) {
            return parent.backgroundColor;
        }
        return backgroundColor;
    }

    public Color getSelectedTextColor() {
        if (selectedTextColor == null) {
            return parent.selectedTextColor;
        }
        return selectedTextColor;
    }

    public Color getSelectedBackgroundColor() {
        if (selectedBackgroundColor == null) {
            return parent.selectedBackgroundColor;
        }
        return selectedBackgroundColor;
    }

    public ColorSkin deriveColorSkin(String name, Color txtColor, Color bgColor, Color selTxtColor, Color selBgColor) {
        ColorSkin colorSkin = new ColorSkin(this, name);
        if (txtColor != null) {
            colorSkin.textColor = txtColor;
        }
        /*else {
        colorSkin.textColor = this.textColor;
        }*/
        if (bgColor != null) {
            colorSkin.backgroundColor = bgColor;
        }
        /*else {
        colorSkin.backgroundColor = this.backgroundColor;
        }*/
        if (selTxtColor != null) {
            colorSkin.selectedTextColor = selTxtColor;
        }
        /*else {
        colorSkin.selectedTextColor = this.selectedTextColor;
        }*/
        if (selBgColor != null) {
            colorSkin.selectedBackgroundColor = selBgColor;
        }
        /*else {
        colorSkin.selectedBackgroundColor = this.selectedBackgroundColor;
        }*/
        return colorSkin;
    }

    protected void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    protected void setBackgroundColor(Color backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    protected void setSelectedTextColor(Color selectedTextColor) {
        this.selectedTextColor = selectedTextColor;
    }

    protected void setSelectedBackgroundColor(Color selectedBackgroundColor) {
        this.selectedBackgroundColor = selectedBackgroundColor;
    }

    public static ColorSkin createFondColorSkin(Color color) {
        float[] hsb = Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), null);
        float hue = hsb[0] * 360;
        float saturation = hsb[1];
        float brightness = hsb[2];
        Color selectedTextColor = Color.BLACK;
        if (brightness <= 0.7) {
            selectedTextColor = Color.WHITE;
        } else if ((hue >= 200) && (saturation >= 0.3)) {
            selectedTextColor = Color.WHITE;

        } else if ((hue <= 45) && (saturation >= 0.8)) {
            selectedTextColor = Color.WHITE;

        }
        return new ColorSkin("name", Color.BLACK, Color.WHITE, selectedTextColor, color);
    }

}
