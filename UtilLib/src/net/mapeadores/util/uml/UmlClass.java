/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.uml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class UmlClass {

    private final static String INDENT = "  ";
    private final String name;
    private final List<Item> fieldList = new ArrayList<Item>();
    private String title = "";
    private char circleLetter = 0;
    private String circleColor;

    public UmlClass(String name) {
        this.name = name;
    }

    public UmlClass title(String title) {
        if (title == null) {
            this.title = "";
        } else {
            this.title = title;
        }
        return this;
    }

    public UmlClass circle(char circleLetter, String circleColor) {
        this.circleLetter = circleLetter;
        if (circleColor == null) {
            this.circleColor = "";
        } else {
            this.circleColor = circleColor;
        }
        return this;
    }

    public UmlClass addField(String field) {
        fieldList.add(new Field(field));
        return this;
    }

    public UmlClass addSeparator(String title) {
        fieldList.add(new Separator(title));
        return this;
    }


    public void write(Appendable appendable, int indentValue) throws IOException {
        indent(appendable, indentValue);
        appendable.append("class ");
        if (title.length() > 0) {
            appendable.append('"');
            StringUtils.escapeDoubleQuote(title, appendable);
            appendable.append('"');
            appendable.append(" as ");
            appendable.append(name);
        } else {
            appendable.append(name);
        }
        if (circleLetter != 0) {
            appendable.append(" << (");
            appendable.append(circleLetter);
            if (circleColor.length() > 0) {
                appendable.append(',');
                appendable.append(circleColor);
            }
            appendable.append(" )>>");
        }
        if (!fieldList.isEmpty()) {
            appendable.append(" {\n");
            for (Item item : fieldList) {
                indent(appendable, indentValue + 1);
                item.write(appendable);
            }
            indent(appendable, indentValue);
            appendable.append("}\n");
        } else {
            appendable.append("\n");
        }
    }

    public static UmlClass init(String name) {
        return new UmlClass(name);
    }

    private static void indent(Appendable appendable, int indentValue) throws IOException {
        for (int i = 0; i < indentValue; i++) {
            appendable.append(INDENT);
        }
    }


    private static abstract class Item {

        public abstract void write(Appendable appendable) throws IOException;

    }


    private static class Field extends Item {

        private final String value;

        private Field(String value) {
            this.value = value;
        }

        @Override
        public void write(Appendable appendable) throws IOException {
            appendable.append("{field} ");
            appendable.append(value);
            appendable.append("\n");
        }

    }


    private static class Separator extends Item {

        private final String title;

        private Separator(String title) {
            this.title = title;
        }

        @Override
        public void write(Appendable appendable) throws IOException {
            if (title.isEmpty()) {
                appendable.append("..\n");
            } else {
                appendable.append(".. ");
                appendable.append(title);
                appendable.append(" ..\n");
            }
        }

    }

}
