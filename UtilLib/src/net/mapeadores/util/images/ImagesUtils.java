/* UtilLib - Copyright (c) 2010-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.images;

import java.awt.Graphics;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.imageio.ImageIO;
import net.mapeadores.util.logging.ErrorMessageException;


/**
 *
 * @author Vincent Calame
 */
public final class ImagesUtils {

    private ImagesUtils() {

    }

    public static BufferedImage read(InputStream inputStream) throws IOException, ErrorMessageException {
        BufferedImage image = ImageIO.read(inputStream);
        if (image == null) {
            throw new ErrorMessageException("_ error.unsupported.imagetype");
        }
        return image;
    }

    public static BufferedImage read(File file) throws IOException, ErrorMessageException {
        BufferedImage image = ImageIO.read(file);
        if (image == null) {
            throw new ErrorMessageException("_ error.unsupported.imagetype");
        }
        return image;
    }

    public static void write(BufferedImage image, String format, OutputStream outputStream) throws IOException, ErrorMessageException {
        boolean done = ImageIO.write(image, format, outputStream);
        if (!done) {
            throw new ErrorMessageException("_ error.unsupported.imagetype");
        }
    }

    public static void write(BufferedImage image, String format, File file) throws IOException, ErrorMessageException {
        boolean done = ImageIO.write(image, format, file);
        if (!done) {
            throw new ErrorMessageException("_ error.unsupported.imagetype");
        }
    }

    public static void convert(InputStream input, String destinationFormatName, File destination) throws IOException, ErrorMessageException {
        BufferedImage bufferedImage = read(input);
        ImagesUtils.write(bufferedImage, destinationFormatName, destination);
    }

    public static void cropImage(InputStream inputStream, String format, OutputStream outputStream, int x, int y, int width, int height) throws IOException, ErrorMessageException {
        BufferedImage srcImg = read(inputStream);
        int type = (srcImg.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        BufferedImage destImg = new BufferedImage(width, height, type);
        Graphics g = destImg.getGraphics();
        g.drawImage(srcImg, 0, 0, width, height, x, y, x + width, y + height, null);
        g.dispose();
        ImagesUtils.write(destImg, format, outputStream);
    }

    public static boolean testImage(File file) {
        try (FileInputStream is = new FileInputStream(file)) {
            return testImage(is);
        } catch (IOException ioe) {
            return false;
        }
    }

    public static boolean testImage(InputStream inputStream) {
        try {
            BufferedImage srcImg = read(inputStream);
            return true;
        } catch (IOException | ErrorMessageException e) {
            return false;
        }
    }

}
