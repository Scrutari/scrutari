/* UtilLib - Copyright (c) 2016-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public class MessageSourceBuilder implements MessageHandler {

    private final String name;
    private final List<InternalMessageSourceEntry> entryList = new ArrayList<InternalMessageSourceEntry>();

    public MessageSourceBuilder(String name) {
        this.name = name;
    }

    public boolean isEmpty() {
        return (entryList.isEmpty());
    }

    @Override
    public void addMessage(String category, Message message) {
        if (category == null) {
            throw new IllegalArgumentException("category is null");
        }
        if (message == null) {
            throw new IllegalArgumentException("message is null");
        }
        entryList.add(new InternalMessageSourceEntry(category, message));
    }

    public List<MessageSource.Entry> toEntryList() {
        return LogUtils.wrap(entryList.toArray(new MessageSource.Entry[entryList.size()]));
    }

    public List<Message> toMessageList(String category) {
        List<Message> list = new ArrayList<Message>();
        for (InternalMessageSourceEntry entry : entryList) {
            if (entry.getCategory().startsWith(category)) {
                list.add(entry.getMessage());
            }
        }
        return list;
    }

    public MessageSource toMessageSource() {
        List<MessageSource.Entry> finalList = LogUtils.wrap(entryList.toArray(new MessageSource.Entry[entryList.size()]));
        return new InternalMessageSource(name, finalList);
    }


    private static class InternalMessageSource implements MessageSource {

        private final String name;
        private final List<MessageSource.Entry> messageList;

        private InternalMessageSource(String name, List<MessageSource.Entry> messageList) {
            this.name = name;
            this.messageList = messageList;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<MessageSource.Entry> getEntryList() {
            return messageList;
        }

    }


    private static class InternalMessageSourceEntry implements MessageSource.Entry {

        private final String category;
        private final Message message;

        private InternalMessageSourceEntry(String category, Message message) {
            this.category = category;
            this.message = message;
        }

        @Override
        public String getCategory() {
            return category;
        }

        @Override
        public Message getMessage() {
            return message;
        }

    }

}
