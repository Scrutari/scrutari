/* BdfServer - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;


/**
 *
 * @author Vincent Calame
 */
public class SimpleLineMessageHandler implements LineMessageHandler {

    private final SortedMap<Integer, InternalMessageByLine> messageByLineMap = new TreeMap<Integer, InternalMessageByLine>();

    public SimpleLineMessageHandler() {
    }

    @Override
    public void addMessage(String category, LineMessage message) {
        int lineNumber = message.getLineNumber();
        InternalMessageByLine messageByLine = messageByLineMap.get(lineNumber);
        if (messageByLine == null) {
            messageByLine = new InternalMessageByLine(lineNumber);
            messageByLineMap.put(lineNumber, messageByLine);
        }
        messageByLine.add(category, message);
    }

    public boolean hasMessage() {
        return (!messageByLineMap.isEmpty());
    }

    public List<MessageByLine> toMessageByLineList() {
        return LogUtils.wrap(messageByLineMap.values().toArray(new MessageByLine[messageByLineMap.size()]));
    }

    public List<LineMessage> toLineMessageList() {
        List<LineMessage> result = new ArrayList<LineMessage>();
        for (InternalMessageByLine line : messageByLineMap.values()) {
            for (MessageByLine.Category category : line.getCategoryList()) {
                for (LineMessage message : category.getMessageList()) {
                    result.add(message);
                }
            }
        }
        return result;
    }


    private static class InternalMessageByLine implements MessageByLine {

        private final int lineNumber;
        private final Map<String, InternalCategory> categoryMap = new HashMap<String, InternalCategory>();
        private final List<Category> categoryList = new ArrayList<Category>();

        private InternalMessageByLine(int lineNumber) {
            this.lineNumber = lineNumber;
        }

        @Override
        public int getLineNumber() {
            return lineNumber;
        }

        @Override
        public List<Category> getCategoryList() {
            return categoryList;
        }


        private void add(String categoryName, LineMessage message) {
            InternalCategory category = categoryMap.get(categoryName);
            if (category == null) {
                category = new InternalCategory(categoryName);
                categoryMap.put(categoryName, category);
                categoryList.add(category);
            }
            category.add(message);
        }

    }


    private static class InternalCategory implements MessageByLine.Category {

        private final String name;
        private final List<LineMessage> messageList = new ArrayList<LineMessage>();

        private InternalCategory(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<LineMessage> getMessageList() {
            return messageList;
        }

        private void add(LineMessage message) {
            messageList.add(message);
        }

    }

}
