/* UtilLib - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;


/**
 *
 * @author Vincent Calame
 */
public class LineMessageException extends Exception {

    private final String category;
    private final LineMessage lineMessage;

    public LineMessageException(int lineNumber, String category, String messageKey) {
        super(messageKey);
        this.category = category;
        this.lineMessage = LogUtils.toLineMessage(lineNumber, messageKey);
    }

    public LineMessageException(int lineNumber, String category, String messageKey, Object... values) {
        super(messageKey);
        this.category = category;
        this.lineMessage = LogUtils.toLineMessage(lineNumber, messageKey, values);
    }

    public LineMessageException(String category, LineMessage lineMessage) {
        super(lineMessage.getMessageKey());
        this.category = category;
        this.lineMessage = lineMessage;
    }

    public String getCategory() {
        return category;
    }

    public LineMessage getLineMessage() {
        return lineMessage;
    }

}
