/* UtilLib - Copyright (c) 2016-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public class MessageByLineLogBuilder {

    private final Map<String, SimpleLineMessageHandler> handlerMap = new LinkedHashMap<String, SimpleLineMessageHandler>();


    public LineMessageHandler setCurrentURI(String uri) {
        SimpleLineMessageHandler handler = handlerMap.get(uri);
        if (handler == null) {
            handler = new SimpleLineMessageHandler();
            handlerMap.put(uri, handler);
        }
        return handler;
    }

    public MessageByLineLog toMessageByLineLog() {
        List<InternalLogGroup> logGroupList = new ArrayList<InternalLogGroup>();
        for (Map.Entry<String, SimpleLineMessageHandler> entry : handlerMap.entrySet()) {
            SimpleLineMessageHandler handler = entry.getValue();
            if (handler.hasMessage()) {
                logGroupList.add(new InternalLogGroup(entry.getKey(), handler.toMessageByLineList()));
            }
        }
        MessageByLineLog.LogGroup[] array = logGroupList.toArray(new MessageByLineLog.LogGroup[logGroupList.size()]);
        return new InternalMessageByLineLog(new MessageByLineLogGroupList(array));
    }


    private static class InternalMessageByLineLog implements MessageByLineLog {

        private final List<LogGroup> list;

        private InternalMessageByLineLog(List<LogGroup> list) {
            this.list = list;
        }

        @Override
        public List<LogGroup> getLogGroupList() {
            return list;
        }

    }


    private static class InternalLogGroup implements MessageByLineLog.LogGroup {

        private final String uri;
        private final List<MessageByLine> list;

        private InternalLogGroup(String uri, List<MessageByLine> list) {
            this.uri = uri;
            this.list = list;
        }

        @Override
        public String getURI() {
            return uri;
        }

        @Override
        public List<MessageByLine> getMessageByLineList() {
            return list;
        }

    }


    private static class MessageByLineLogGroupList extends AbstractList<MessageByLineLog.LogGroup> implements RandomAccess {

        private final MessageByLineLog.LogGroup[] array;

        private MessageByLineLogGroupList(MessageByLineLog.LogGroup[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public MessageByLineLog.LogGroup get(int index) {
            return array[index];
        }

    }

}
