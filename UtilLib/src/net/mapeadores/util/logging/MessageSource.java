/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.List;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface MessageSource {

    public String getName();

    public List<Entry> getEntryList();

    public default boolean isEmpty() {
        return getEntryList().isEmpty();
    }


    public interface Entry {

        public String getCategory();

        public Message getMessage();

    }

}
