/* BdfServer_API - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface MessageByLine {

    public int getLineNumber();

    public List<Category> getCategoryList();


    public interface Category {

        public String getName();

        public List<LineMessage> getMessageList();

    }

}
