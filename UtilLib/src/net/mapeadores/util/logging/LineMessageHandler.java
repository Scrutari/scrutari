/* FichothequeLib_API - Copyright (c) 2010-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface LineMessageHandler {

    public void addMessage(String category, LineMessage lineMessage);

    public default void addMessage(int lineNumber, String category, Message message) {
        addMessage(category, LogUtils.toLineMessage(lineNumber, message));
    }

    public default void addMessage(int lineNumber, String category, String messageKey, Object... messageValues) {
        addMessage(category, LogUtils.toLineMessage(lineNumber, messageKey, messageValues));
    }

    public default void addMessage(LineMessageException lme) {
        addMessage(lme.getCategory(), lme.getLineMessage());
    }

}
