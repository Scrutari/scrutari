/* UtilLib - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface CommandMessage extends Message {

    public boolean isErrorMessage();


    public List<Message> getMultiErrorList();


    @Nullable
    public MessageLog getMessageLog();

    public default boolean hasMultiError() {
        return !getMultiErrorList().isEmpty();
    }

    public default boolean hasLog() {
        MessageLog messageLog = getMessageLog();
        return ((messageLog != null) && (!messageLog.isEmpty()));
    }

}
