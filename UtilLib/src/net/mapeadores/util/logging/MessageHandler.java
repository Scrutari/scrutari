/* UtilLib - Copyright (c) 2012-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logging;

import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;


/**
 *
 * @author Vincent Calame
 */
public interface MessageHandler {

    public void addMessage(String category, Message message);

    public default void addMessage(String category, String messageKey, Object... messageValues) {
        addMessage(category, LocalisationUtils.toMessage(messageKey, messageValues));
    }

}
