/* UtilLib - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class XMLPart implements XMLWriter {

    private final XMLWriter xmlWriter;

    public XMLPart(XMLWriter xmlWriter) {
        this.xmlWriter = xmlWriter;
    }

    public XMLWriter getXMLWriter() {
        return xmlWriter;
    }

    @Override
    public XMLPart addText(CharSequence charSequence) throws IOException {
        xmlWriter.addText(charSequence);
        return this;
    }

    @Override
    public XMLPart openTag(String name) throws IOException {
        xmlWriter.openTag(name);
        return this;
    }

    @Override
    public XMLPart openTag(String name, boolean indentBefore) throws IOException {
        xmlWriter.openTag(name, indentBefore);
        return this;
    }

    @Override
    public XMLPart startOpenTag(String name) throws IOException {
        xmlWriter.startOpenTag(name);
        return this;
    }

    @Override
    public XMLPart startOpenTag(String name, boolean indentBefore) throws IOException {
        xmlWriter.startOpenTag(name, indentBefore);
        return this;
    }

    @Override
    public XMLPart closeTag(String name) throws IOException {
        xmlWriter.closeTag(name);
        return this;
    }

    @Override
    public XMLPart closeTag(String name, boolean indentBefore) throws IOException {
        xmlWriter.closeTag(name, indentBefore);
        return this;
    }

    @Override
    public XMLPart endOpenTag() throws IOException {
        xmlWriter.endOpenTag();
        return this;
    }

    @Override
    public XMLPart addAttribute(String name, String value) throws IOException {
        xmlWriter.addAttribute(name, value);
        return this;
    }

    @Override
    public XMLPart addAttribute(String name, int value) throws IOException {
        xmlWriter.addAttribute(name, value);
        return this;
    }

    @Override
    public XMLPart closeEmptyTag() throws IOException {
        xmlWriter.closeEmptyTag();
        return this;
    }

    @Override
    public XMLPart addSimpleElement(String tagname, String value) throws IOException {
        xmlWriter.addSimpleElement(tagname, value);
        return this;
    }

    public XMLPart addSimpleElement(String name, String value, String lang) throws IOException {
        xmlWriter.addSimpleElement(name, value, lang);
        return this;
    }

    @Override
    public XMLPart addEmptyElement(String tagname) throws IOException {
        xmlWriter.addEmptyElement(tagname);
        return this;
    }

    @Override
    public XMLPart addCData(CharSequence charSequence) throws IOException {
        xmlWriter.addCData(charSequence);
        return this;
    }

}
