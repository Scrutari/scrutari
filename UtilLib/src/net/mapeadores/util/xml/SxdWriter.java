/* UtilLib - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.awt.Dimension;
import java.awt.Point;
import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class SxdWriter extends DefaultXMLWriter {

    private final String unit = "pt";

    public SxdWriter() {
    }

    public String getUnit() {
        return unit;
    }

    public void openPage(String name, String styleName, String masterPageName) throws IOException {
        startOpenTag("draw:page");
        addAttribute("draw:name", name);
        addAttribute("draw:style-name", styleName);
        addAttribute("draw:master-page-nam", masterPageName);
        endOpenTag();
    }

    public void closePage() throws IOException {
        closeTag("draw:page");
    }

    public void openGroup() throws IOException {
        openTag("draw:g");
    }

    public void closeGroup() throws IOException {
        closeTag("draw:g");
    }

    public void startTextBoxOpenTag() throws IOException {
        startOpenTag("draw:text-box");
    }

    public void addTextElement(String text) throws IOException {
        openTag("text:p");
        addText(text);
        closeTag("text:p");
    }

    public void closeTextBox() throws IOException {
        closeTag("draw:text-box");
    }

    @Override
    public SxdWriter addAttribute(String attributeName, int value) throws IOException {
        addAttribute(attributeName, String.valueOf(value) + unit);
        return this;
    }

    public void addLayerAttribute(String layerName) throws IOException {
        addAttribute("draw:layer", layerName);
    }

    public void addStyleNameAttribute(String styleName) throws IOException {
        addAttribute("draw:style-name", styleName);
    }

    public void addTypeAttribute(String type) throws IOException {
        addAttribute("draw:type", type);
    }

    public void addIdAttribute(String id) throws IOException {
        addAttribute("draw:id", id);
    }

    public void addXAttribute(int x) throws IOException {
        addAttribute("svg:x", String.valueOf(x) + unit);
    }

    public void addYAttribute(int y) throws IOException {
        addAttribute("svg:y", String.valueOf(y) + unit);
    }

    public void addWidthAttribute(int width) throws IOException {
        addAttribute("svg:width", String.valueOf(width) + unit);
    }

    public void addHeightAttribute(int height) throws IOException {
        addAttribute("svg:height", String.valueOf(height) + unit);
    }

    public void addPositionAttributes(Point P) throws IOException {
        addXAttribute(P.x);
        addYAttribute(P.y);
    }

    public void addDimensionAttributes(Dimension dimension) throws IOException {
        addWidthAttribute(dimension.width);
        addHeightAttribute(dimension.height);
    }

    public void addTransformAttribute(String transform) throws IOException {
        addAttribute("draw:transform", transform);
    }

    public void addArcElement(int x, int y, int width, int height, int startAngle, int arcAngle, String layerName, String styleName) throws IOException {
        startOpenTag("draw:ellipse");
        addAttribute("draw:kind", "arc");
        addLayerAttribute(layerName);
        addStyleNameAttribute(styleName);
        addXAttribute(x);
        addYAttribute(y);
        addWidthAttribute(width);
        addHeightAttribute(height);
        addAttribute("draw:start-angle", String.valueOf(startAngle));
        addAttribute("draw:end-angle", String.valueOf(arcAngle + startAngle));
        closeEmptyTag();
    }

    public void addLineElement(Point A, Point B, String layerName, String styleName) throws IOException {
        startOpenTag("draw:line");
        addLayerAttribute(layerName);
        addStyleNameAttribute(styleName);
        addAttribute("svg:x1", A.x);
        addAttribute("svg:y1", A.y);
        addAttribute("svg:x2", B.x);
        addAttribute("svg:y2", B.y);
        closeEmptyTag();
    }

    public void addConnectorElement(ConnectorParameters connectorParameters, String type) throws IOException {
        startOpenTag("draw:connector");
        if ((type != null) && (!type.equals("default"))) {
            addTypeAttribute(type);
        }
        addLayerAttribute(connectorParameters.layer);
        addStyleNameAttribute(connectorParameters.styleName);
        addAttribute("draw:start-shape", connectorParameters.startShape);
        addAttribute("draw:end-shape", connectorParameters.endShape);
        addAttribute("draw:start-glue-point", String.valueOf(connectorParameters.startGluePoint));
        addAttribute("draw:end-glue-point", String.valueOf(connectorParameters.endGluePoint));
        closeEmptyTag();
    }

    public String getRotate(double angle) {
        StringBuilder buf = new StringBuilder();
        buf.append("rotate(");
        buf.append(String.valueOf(angle));
        buf.append(")");
        return buf.toString();
    }

    public String getTranslate(int x, int y) {
        StringBuilder buf = new StringBuilder();
        buf.append("translate(");
        buf.append(x);
        buf.append(unit);
        buf.append(" ");
        buf.append(y);
        buf.append(unit);
        buf.append(")");
        return buf.toString();
    }


    public static class ConnectorParameters {

        private String startShape;
        private String endShape;
        private String layer;
        private String styleName;
        private int startGluePoint;
        private int endGluePoint;

        public void setStartShape(String startShape) {
            this.startShape = startShape;
        }

        public void setEndShape(String endShape) {
            this.endShape = endShape;
        }

        public void setStyleName(String styleName) {
            this.styleName = styleName;
        }

        public void setLayer(String layer) {
            this.layer = layer;
        }

        public void setStartGluePoint(int startGluePoint) {
            this.startGluePoint = startGluePoint;
        }

        public void setEndGluePoint(int endGluePoint) {
            this.endGluePoint = endGluePoint;
        }

    }

}
