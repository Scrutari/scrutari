/* UtilLib - Copyright (c) 2008-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.handlers;

import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface ElementHandler {

    public void processStartElement(String tagname, Attributes attributes);

    /**
     * Procède au traitement de la cloture d'une balise. Retourne <em<true</em>
     * s'il s'agit de la fin de l'élément traité et <em>false</em> s'il s'agit
     * de la fin d'un sous-élément.
     *
     * @param tagname le nom de la balise fermée
     * @return s'il s'agit de la fin de l'élément traité ou de celle d'un de ses
     * enfants
     */
    public boolean processEndElement(String tagname);

    public void processText(char[] ch, int start, int length);

}
