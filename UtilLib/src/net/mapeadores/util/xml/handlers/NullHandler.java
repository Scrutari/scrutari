/* UtilLib - Copyright (c) 2008-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.handlers;

import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class NullHandler implements ElementHandler {

    private int subCount = 0;

    public NullHandler() {
    }

    @Override
    public void processStartElement(String tagname, Attributes attributes) {
        subCount++;
    }

    @Override
    public boolean processEndElement(String tagname) {
        if (subCount == 0) {
            return true;
        }
        subCount--;
        return false;
    }

    @Override
    public void processText(char[] ch, int start, int length) {
    }

}
