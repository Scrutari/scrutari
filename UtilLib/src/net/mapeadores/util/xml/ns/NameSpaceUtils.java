/* UtilLib - Copyright (c) 2008-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.ns;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import net.mapeadores.util.exceptions.InternalResourceException;


/**
 *
 * @author Vincent Calame
 */
public class NameSpaceUtils {

    public final static String ODT_NAMESPACE_MAP = "odt";

    private NameSpaceUtils() {
    }

    public static Map<String, NameSpace> getNameSpaceMap(String listName) {
        if (isValid(listName)) {
            try {
                return getResourceMap(listName);
            } catch (IOException ioe) {
                throw new InternalResourceException(ioe);
            }
        }
        return Collections.emptyMap();
    }

    private static Map<String, NameSpace> getResourceMap(String listName) throws IOException {
        String ligne = null;
        Map<String, NameSpace> result = new LinkedHashMap<String, NameSpace>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(NameSpaceUtils.class.getResourceAsStream(listName + ".txt"), "UTF-8"))) {
            while ((ligne = reader.readLine()) != null) {
                int idx = ligne.indexOf('\t');
                if (idx == -1) {
                    continue;
                }
                String prefix = ligne.substring(0, idx).trim();
                String name = ligne.substring(idx + 1).trim();
                result.put(prefix, new NameSpace(prefix, name));
            }
        }
        return result;
    }

    private static boolean isValid(String listName) {
        if (listName.equals(ODT_NAMESPACE_MAP)) {
            return true;
        } else {
            return false;
        }
    }

}
