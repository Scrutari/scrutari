/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml.svg;

import net.mapeadores.util.xml.XMLWriter;
import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class SVGUtils {

    private SVGUtils() {
    }

    public static void appendSVGNameSpace(XMLWriter xmlWriter) throws IOException {
        xmlWriter.addAttribute("xmlns:svg", "http://www.w3.org/2000/svg");
    }

    public static void appendXLinkNameSpace(XMLWriter xmlWriter) throws IOException {
        xmlWriter.addAttribute("xmlns:xlink", "http://www.w3.org/1999/xlink");
    }

}
