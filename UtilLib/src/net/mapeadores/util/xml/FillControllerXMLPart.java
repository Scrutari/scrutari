/* UtilLib - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public abstract class FillControllerXMLPart implements XMLWriter {

    private XMLWriter xmlWriter;
    private boolean fillControled = false;

    public FillControllerXMLPart(XMLWriter xmlWriter) {
        this.xmlWriter = xmlWriter;
    }

    public XMLWriter getXMLWriter() {
        return xmlWriter;
    }

    public abstract void controlFill() throws IOException;

    public boolean isFillControled() {
        return fillControled;
    }

    @Override
    public FillControllerXMLPart addText(CharSequence charSequence) throws IOException {
        if (charSequence.length() > 0) {
            fillControlTest();
            xmlWriter.addText(charSequence);
        }
        return this;
    }

    @Override
    public FillControllerXMLPart openTag(String name) throws IOException {
        fillControlTest();
        xmlWriter.openTag(name);
        return this;
    }

    @Override
    public FillControllerXMLPart openTag(String name, boolean indentBefore) throws IOException {
        fillControlTest();
        xmlWriter.openTag(name, indentBefore);
        return this;
    }

    @Override
    public FillControllerXMLPart startOpenTag(String name) throws IOException {
        fillControlTest();
        xmlWriter.startOpenTag(name);
        return this;
    }

    @Override
    public FillControllerXMLPart startOpenTag(String name, boolean indentBefore) throws IOException {
        fillControlTest();
        xmlWriter.startOpenTag(name, indentBefore);
        return this;
    }

    @Override
    public FillControllerXMLPart closeTag(String name) throws IOException {
        xmlWriter.closeTag(name);
        return this;
    }

    @Override
    public FillControllerXMLPart closeTag(String name, boolean indentBefore) throws IOException {
        xmlWriter.closeTag(name, indentBefore);
        return this;
    }

    @Override
    public FillControllerXMLPart endOpenTag() throws IOException {
        xmlWriter.endOpenTag();
        return this;
    }

    @Override
    public FillControllerXMLPart addAttribute(String name, String value) throws IOException {
        if ((value != null) && (value.length() > 0)) {
            fillControlTest();
            xmlWriter.addAttribute(name, value);
        }
        return this;
    }

    @Override
    public FillControllerXMLPart addAttribute(String name, int value) throws IOException {
        fillControlTest();
        xmlWriter.addAttribute(name, value);
        return this;
    }

    @Override
    public FillControllerXMLPart closeEmptyTag() throws IOException {
        xmlWriter.closeEmptyTag();
        return this;
    }

    @Override
    public FillControllerXMLPart addSimpleElement(String tagname, String value) throws IOException {
        fillControlTest();
        xmlWriter.addSimpleElement(tagname, value);
        return this;
    }

    @Override
    public FillControllerXMLPart addEmptyElement(String tagname) throws IOException {
        fillControlTest();
        xmlWriter.addEmptyElement(tagname);
        return this;
    }

    @Override
    public FillControllerXMLPart addCData(CharSequence charSequence) throws IOException {
        if (charSequence.length() > 0) {
            fillControlTest();
            xmlWriter.addCData(charSequence);
        }
        return this;
    }

    private void fillControlTest() throws IOException {
        if (!fillControled) {
            controlFill();
            fillControled = true;
        }
    }

}
