/* UtilLib - Copyright (c) 2006-2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.xml;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class SxcWriter extends DefaultXMLWriter {

    public SxcWriter() {
    }

    public void openTable(String name) throws IOException {
        startOpenTag("table:table");
        addAttribute("table:name", name);
        endOpenTag();
    }

    public void endTable() throws IOException {
        closeTag("table:table");
    }

    public void openRow() throws IOException {
        openTag("table:table-row");
    }

    public void closeRow() throws IOException {
        closeTag("table:table-row");
    }

    public void openCell() throws IOException {
        openTag("table:table-cell");
    }

    public void closeCell() throws IOException {
        closeTag("table:table-cell");
    }

    public void addEmptyCell() throws IOException {
        startOpenTag("table:table-cell");
        closeEmptyTag();
    }

    public void addIntegerCellElement(int itg) throws IOException {
        startOpenTag("table:table-cell");
        addAttribute("table:value-type", "float");
        addAttribute("table:value", String.valueOf(itg));
        endOpenTag();
        addText(String.valueOf(itg));
        closeCell();
    }

    public void addStringCellElement(String value) throws IOException {
        openCell();
        addSimpleElement("text:p", value);
        closeCell();
    }

}
