/* UtilLib - Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.table;

import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public interface TableWriter {


    /**
     * Commence une nouvelle ligne et retourne son numéro. Les lignes sont
     * numérotées à partir de 1.
     *
     * @return le numéro de la nouvelle ligne
     */
    public int startRow();

    /**
     * Ajoute une cellule avec une valeur entière. Retourne le numéro de la
     * cellule, les cellules étant numérotées à partir de 1.
     *
     * @param lg valeur entière de la cellule
     * @return le numéro de colonne de la cellule
     */
    public int addIntegerCell(Long lg);

    /**
     * joute une cellule avec une valeur décimale. Retourne le numéro de la
     * cellule, les cellules étant numérotées à partir de 1.
     *
     * @param decimal valeur décimale de la cellule
     * @return le numéro de colonne de la cellule
     */
    public int addDecimalCell(Decimal decimal);

    /**
     * Ajoute une cellule avec une chaine de caractères. Retourne le numéro de
     * la cellule, les cellules étant numérotées à partir de 1.
     *
     * @param s valeur de la cellule
     * @return le numéro de colonne de la cellule
     */
    public int addStringCell(String s);

    /**
     * Ajoute une cellule avec date. Retourne le numéro de la cellule, les
     * cellules étant numérotées à partir de 1.
     *
     * @param date date approximative
     * @return le numéro de colonne de la cellule
     */
    public int addDateCell(FuzzyDate date);

    /**
     * Ajoute une cellule avec un montant monétaire.
     *
     * @param amount montant à rajouter
     * @return le numéro de colonne de la cellule
     */
    public int addMoneyCell(Amount amount);

    /**
     * Ajoute une cellule avec un pourcentage. Retourne le numéro de la cellule,
     * les cellules étant numérotées à partir de 1.
     * <p>
     * La valeur du pourcentage est le nombre correspondant à la division par
     * 100 (12,4% est exprimé sous la forme 0,124)
     *
     * @param decimal valeur du pourcentage
     * @return le numéro de colonne de la cellule
     */
    public int addPercentageCell(Decimal decimal);

    /**
     * Termine la ligne en cours et retourne son numéro. Les lignes sont
     * numérotées à partir de 1.
     *
     * @return le numéro de la ligne terminée
     */
    public int endRow();

    public default int addIntegerCell(int integer) {
        return addIntegerCell(Long.valueOf(integer));
    }

}
