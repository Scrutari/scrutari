/* UtilLib - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.util.List;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface ResourceFolder {

    /**
     * Vide s'il s'agit de la racine.
     *
     * @return
     */
    public String getName();

    public List<ResourceFolder> getSubfolderList();

    public List<String> getResourceNameList();

    @Nullable
    public default ResourceFolder getResourceFolder(RelativePath path) {
        String[] pathArray = path.toArray();
        ResourceFolder currentFolder = this;
        int length = pathArray.length;
        int depth = 0;
        boolean exists = true;
        while (exists) {
            exists = false;
            for (ResourceFolder subfolder : currentFolder.getSubfolderList()) {
                if (subfolder.getName().equals(pathArray[depth])) {
                    exists = true;
                    currentFolder = subfolder;
                    depth++;
                    break;
                }
            }
            if (depth == length) {
                return currentFolder;
            }
        }
        return null;
    }

}
