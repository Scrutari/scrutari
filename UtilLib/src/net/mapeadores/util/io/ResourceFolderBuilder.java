/* UtilLib - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ResourceFolderBuilder {

    private final String name;
    private final SortedMap<String, ResourceFolderBuilder> builderMap = new TreeMap<String, ResourceFolderBuilder>();
    private final SortedSet<String> resourceSet = new TreeSet<String>();

    public ResourceFolderBuilder(String name) {
        if (name == null) {
            throw new NullPointerException("name is null");
        }
        this.name = name;
    }


    public void addResource(String resourceName) {
        if (!builderMap.containsKey(resourceName)) {
            resourceSet.add(resourceName);
        }
    }

    public ResourceFolderBuilder getSubfolderBuilder(String name) {
        ResourceFolderBuilder subfolderBuilder = builderMap.get(name);
        if (subfolderBuilder == null) {
            subfolderBuilder = new ResourceFolderBuilder(name);
            builderMap.put(name, subfolderBuilder);
        }
        resourceSet.remove(name);
        return subfolderBuilder;
    }

    public ResourceFolder toRessourceFolder() {
        List<ResourceFolder> list = new ArrayList<ResourceFolder>();
        for (ResourceFolderBuilder builder : builderMap.values()) {
            list.add(builder.toRessourceFolder());
        }
        return build(name, list, resourceSet);
    }

    public static ResourceFolderBuilder init(String name) {
        return new ResourceFolderBuilder(name);
    }

    public static ResourceFolder build(String name, Collection<ResourceFolder> subfolders, Collection<String> resourceNames) {
        List<ResourceFolder> subnodeList = ResourceUtils.wrap(subfolders.toArray(new ResourceFolder[subfolders.size()]));
        List<String> resourceNameList = StringUtils.toList(resourceNames);
        return new InternalResourceFolder(name, subnodeList, resourceNameList);
    }


    private static class InternalResourceFolder implements ResourceFolder {

        private final String name;
        private final List<ResourceFolder> subnodeList;
        private final List<String> resourceNameList;

        private InternalResourceFolder(String name, List<ResourceFolder> subnodeList, List<String> resourceNameList) {
            this.name = name;
            this.subnodeList = subnodeList;
            this.resourceNameList = resourceNameList;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public List<ResourceFolder> getSubfolderList() {
            return subnodeList;
        }

        @Override
        public List<String> getResourceNameList() {
            return resourceNameList;
        }

    }

}
