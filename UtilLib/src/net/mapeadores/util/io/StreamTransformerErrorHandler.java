/* UtilLib - Copyright (c) 2007-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import javax.xml.transform.TransformerException;


/**
 *
 * @author Vincent Calame
 */
public interface StreamTransformerErrorHandler {

    public void missingResourceError(String resourceURI);

    public void transformerError(TransformerException transformerException);

    public void transformerWarning(TransformerException transformerException);

}
