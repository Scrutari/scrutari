/* UtilLib - Copyright (c) 2010-2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.io.Writer;
import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public class AppendableWriter extends Writer {

    private Appendable appendable;

    public AppendableWriter(Appendable appendable) {
        this.appendable = appendable;
    }

    public void flush() throws IOException {
    }

    public void close() throws IOException {
    }

    @Override
    public void write(int c) throws IOException {
        appendable.append((char) c);
    }

    @Override
    public void write(String str) throws IOException {
        appendable.append(str);
    }

    @Override
    public void write(String str, int off, int len) throws IOException {
        appendable.append(str, off, off + len);
    }

    public void write(char[] cbuf, int off, int len) throws IOException {
        for (int i = off; i < (off + len); i++) {
            appendable.append(cbuf[i]);
        }
    }

}
