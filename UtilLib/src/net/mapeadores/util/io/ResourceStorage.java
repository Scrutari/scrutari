/* UtilLib - Copyright (c) 2013-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.mimetype.MimeTypeResolver;
import net.mapeadores.util.mimetype.MimeTypeUtils;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public interface ResourceStorage {

    public String getName();

    public boolean containsResource(RelativePath path);

    @Nullable
    public DocStream getResourceDocStream(RelativePath path, MimeTypeResolver mimeTypeResolver);

    public ResourceFolder getRoot();

    @Nullable
    public default ResourceFolder getResourceFolder(RelativePath path) {
        if (path.isEmpty()) {
            return getRoot();
        } else {
            return getRoot().getResourceFolder(path);
        }
    }

    public default DocStream getResourceDocStream(RelativePath path) {
        return getResourceDocStream(path, MimeTypeUtils.DEFAULT_RESOLVER);
    }

}
