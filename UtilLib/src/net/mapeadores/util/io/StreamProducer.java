/* UtilLib- Copyright (c) 2010-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io;

import java.io.IOException;
import java.io.OutputStream;


/**
 *
 * @author Vincent Calame
 */
public interface StreamProducer {

    /**
     * Peut être nul.
     */
    public String getMimeType();

    /**
     * Peut être nul.
     */
    public String getCharset();

    /**
     * Peut être nul. S'il n'est pas indiqué, le nom du fichier sera le nom
     * de l'URL qui a conduit à la production de l'instance de StreamProducer
     */
    public String getFileName();

    public void writeStream(OutputStream outputStream) throws IOException;

}
