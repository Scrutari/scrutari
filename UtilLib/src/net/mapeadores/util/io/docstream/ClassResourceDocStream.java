/* UtilLib - Copyright (c) 2010-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io.docstream;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.mimetype.MimeTypeUtils;


/**
 *
 * @author Vincent Calame
 */
public class ClassResourceDocStream implements DocStream {

    /*
     * La date de dernière modification des ressources internes est fixée à la
     * date d'initialisation de cette classe
     */
    private static long lastModified;
    /*
     * Suppression des millisecondes.
     */

    static {
        long l = System.currentTimeMillis();
        l = l / 1000;
        lastModified = l * 1000;
    }

    private Class referenceClass;
    private String resourcePath;
    private String mimeType = MimeTypeConstants.OCTETSTREAM;
    private String charset = null;
    private int length = -1;

    private ClassResourceDocStream(Class referenceClass, String resourcePath) {
        this.referenceClass = referenceClass;
        this.resourcePath = resourcePath;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return referenceClass.getResourceAsStream(resourcePath);
    }

    @Override
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        this.charset = MimeTypeUtils.getDefaultCharset(mimeType);
    }

    public void setMimeType(String mimeType, String charset) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        this.charset = charset;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public long getLastModified() {
        return lastModified;
    }

    /**
     * Note : le nom d'une ressource doit comporter au moins un point (afin de
     * ne pas confondre une ressource avec un répertoire)
     */
    public static ClassResourceDocStream newInstance(Class referenceClass, String resourcePath) {
        String fileName = resourcePath;
        int idx = resourcePath.lastIndexOf("/");
        if (idx != -1) {
            fileName = resourcePath.substring(idx + 1);
        }
        if (fileName.indexOf(".") == -1) {
            return null;
        }
        URL url = referenceClass.getResource(resourcePath);
        if (url == null) {
            return null;
        }
        return new ClassResourceDocStream(referenceClass, resourcePath);
    }

}
