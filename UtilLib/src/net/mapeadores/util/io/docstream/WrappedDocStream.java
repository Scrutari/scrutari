/* UtilLib - Copyright (c) 2010-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.io.docstream;

import java.io.IOException;
import java.io.InputStream;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.mimetype.MimeTypeUtils;


/**
 *
 * @author Vincent Calame
 */
public class WrappedDocStream implements DocStream {

    private final DocStream docStream;
    private String mimeType = MimeTypeConstants.OCTETSTREAM;
    private String charset = null;

    public WrappedDocStream(DocStream docStream) {
        this.docStream = docStream;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return docStream.getInputStream();
    }

    @Override
    public int getLength() {
        return docStream.getLength();
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        this.charset = MimeTypeUtils.getDefaultCharset(mimeType);
    }

    public void setMimeType(String mimeType, String charset) {
        if (mimeType == null) {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
        this.charset = charset;
    }

    @Override
    public String getCharset() {
        return charset;
    }

    @Override
    public long getLastModified() {
        return docStream.getLastModified();
    }

}
