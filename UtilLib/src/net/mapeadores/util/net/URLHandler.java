/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.net;

import java.net.URL;


/**
 *
 * @author Vincent Calame
 */
public interface URLHandler {

    public boolean loadURL(URL url);

}
