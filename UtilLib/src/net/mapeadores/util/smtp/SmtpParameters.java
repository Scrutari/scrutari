/* UtilLib - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.smtp;

import net.mapeadores.util.models.EmailCore;


/**
 *
 * @author Vincent Calame
 */
public interface SmtpParameters {

    public final static String AUTHENTIFICATION_NONE = "none";
    public final static String AUTHENTIFICATION_BASIC = "basic";
    public final static String AUTHENTIFICATION_SSL = "ssl";
    public final static String AUTHENTIFICATION_STARTTLS = "starttls";

    public String getAuthentificationType();

    public String getHost();

    public int getPort();

    public String getUsername();

    public String getPassword();

    public EmailCore getDefaultFrom();

}
