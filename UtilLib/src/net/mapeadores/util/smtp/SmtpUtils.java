/* UtilLib - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.smtp;

import java.text.ParseException;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.models.EmailCore;
import net.mapeadores.util.models.EmailCoreUtils;


/**
 *
 * @author Vincent Calame
 */
public final class SmtpUtils {

    private SmtpUtils() {
    }

    public static SmtpParameters toSmtpParameters(Map<String, String> map, List<String> errorList) {
        SmtpParametersBuilder smtpParametersBuilder = new SmtpParametersBuilder();
        String authentificationType = map.get("authentification");
        if (authentificationType == null) {
            errorList.add("Missing key: authentification");
        } else {
            try {
                smtpParametersBuilder.setAuthentificationType(authentificationType);
            } catch (IllegalArgumentException iae) {
                errorList.add("Wrong value: authentification / " + authentificationType);
            }
        }
        smtpParametersBuilder.setPort(getDefaultPort(authentificationType));
        String host = map.get("host");
        if (host == null) {
            errorList.add("Missing key: host");
        } else {
            smtpParametersBuilder.setHost(host);
        }
        String portString = map.get("port");
        if (portString != null) {
            portString = portString.trim();
            if (portString.length() > 0) {
                try {
                    int port = Integer.parseInt(portString);
                    smtpParametersBuilder.setPort(port);
                } catch (NumberFormatException nfe) {
                    errorList.add("Not integer value: port / " + portString);
                }
            }
        }
        String username = map.get("username");
        if (username != null) {
            smtpParametersBuilder.setUsername(username);
        }
        String password = map.get("password");
        if (password != null) {
            smtpParametersBuilder.setPassword(password);
        }
        String defaultFromString = map.get("defaultfrom");
        if (defaultFromString != null) {
            try {
                EmailCore defaultEmail = EmailCoreUtils.parse(defaultFromString);
                smtpParametersBuilder.setDefaultFrom(defaultEmail);
            } catch (ParseException pe) {
                errorList.add("Parse error: defaultfrom / " + pe.getMessage());
            }
        }
        SmtpParameters smtpParameters = smtpParametersBuilder.toSmtpParameters();
        if (isUserMandatory(authentificationType)) {
            if (smtpParameters.getUsername() == null) {
                errorList.add("Missing key: username");
            }
            if (smtpParameters.getPassword() == null) {
                errorList.add("Missing key: password");
            }
        }
        return smtpParameters;
    }

    public static boolean isUserMandatory(String authentificationType) {
        switch (authentificationType) {
            case SmtpParameters.AUTHENTIFICATION_BASIC:
            case SmtpParameters.AUTHENTIFICATION_SSL:
            case SmtpParameters.AUTHENTIFICATION_STARTTLS:
                return true;
            default:
                return false;
        }
    }

    public static int getDefaultPort(String authentificationType) {
        switch (authentificationType) {
            case SmtpParameters.AUTHENTIFICATION_SSL:
                return 465;
            case SmtpParameters.AUTHENTIFICATION_STARTTLS:
                return 587;
            default:
                return 25;
        }
    }

}
