/* UtilLib - Copyright (c) 2013-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logicaloperation;


/**
 *
 * @author Vincent Calame
 */
public class OperandException extends Exception {

    private final String errorKey;
    private final int charIndex;

    public OperandException(String errorKey, int charIndex) {
        super(errorKey);
        this.charIndex = charIndex;
        this.errorKey = errorKey;
    }

    OperandException(RuntimeOperandException roe) {
        super(roe.getMessage());
        this.charIndex = roe.getCharIndex();
        this.errorKey = roe.getErrorKey();
    }

    public int getCharIndex() {
        return charIndex;
    }

    public String getErrorKey() {
        return errorKey;
    }

}
