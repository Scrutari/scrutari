/* UtilLib - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logicaloperation;


/**
 *
 * @author Vincent Calame
 */
class RuntimeOperandException extends RuntimeException {

    private final String errorKey;
    private final int charIndex;

    RuntimeOperandException(String errorKey, int charIndex) {
        super(errorKey);
        this.charIndex = charIndex;
        this.errorKey = errorKey;
    }

    int getCharIndex() {
        return charIndex;
    }

    String getErrorKey() {
        return errorKey;
    }

}
