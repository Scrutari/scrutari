/* UtilLib - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logicaloperation;

import java.util.List;
import java.util.RandomAccess;


/**
 * N'est jamais vide ou avec un seul élément. On doit toujours avoir size() > 1
 *
 *
 * @author Vincent Calame
 */
public interface SubOperand extends Operand, List<Operand>, RandomAccess {

    public short getOperator();

}
