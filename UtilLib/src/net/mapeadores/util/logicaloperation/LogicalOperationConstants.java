/* UtilLib - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.logicaloperation;


/**
 *
 * @author Vincent Calame
 */
public interface LogicalOperationConstants {

    public static final short INTERSECTION_OPERATOR = 1;
    public static final short UNION_OPERATOR = 2;
    public static final String EMPTY_ERRORKEY = "empty";
    public static final String UNKNOWNOPERATOR_ERRORKEY = "unknwonOperator";
    public static final String OPENINGPARENTHESIS_ERRORKEY = "openingParenthesis";
    public static final String MISSINGSEPARATOR_ERRORKEY = "missingSeparator";
    public static final String TOOMANYCLOSINGPARENTHESIS_ERRORKEY = "tooManyClosingParenthesis";
    public static final String MISSING_OPERATORSECONDARYCHAR_ERRORKEY = "missingOperatorSecondaryChar";
    public static final String DIFFERENTOPERATOR_ERRORKEY = "differentOperator";
    public static final String UNEXPECTEDOPERATOR_ERRORKEY = "unexpectedOperator";
    public static final String MISSINGOPERATOR_ERRORKEY = "missingOperator";
}
