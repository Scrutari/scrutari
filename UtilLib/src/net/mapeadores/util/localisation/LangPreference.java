/* UtilLib - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;


/**
 * Doit toujours contenir un élément et ne contient pas de doublons.
 *
 * @author Vincent Calame
 */
public interface LangPreference extends Langs {

    /**
     * Identifique à get(0)
     */
    public Lang getFirstLang();

}
