/* UtilLib - Copyright (c) 2008-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import net.mapeadores.util.annotation.Nullable;


/**
 * « _ » placé au début d'une clé est ignoré.
 *
 * Si messageKey est la chaine vide, les valeurs sont simplement concaténées
 * sans traitement
 *
 * @author Vincent Calame
 */
public interface MessageLocalisation {

    /**
     * Retourne null si absent
     */
    @Nullable
    public String toString(String messageKey);

    /**
     * Retourne null si absent
     */
    @Nullable
    public String toString(Message message);

    public boolean containsKey(String messageKey);

    /**
     * Retourne null si absent
     */
    @Nullable
    public default String toString(String messageKey, Object... messageValues) {
        if (messageKey.isEmpty()) {
            return LocalisationUtils.joinValues(messageValues);
        }
        return toString(LocalisationUtils.toMessage(messageKey, messageValues));
    }

    public default String toNotNullString(String messageKey) {
        if (messageKey == null) {
            throw new IllegalArgumentException("messageKey is null");
        }
        if (messageKey.length() == 0) {
            throw new IllegalArgumentException("messageKey is empty");
        }
        String result = toString(messageKey);
        if ((result == null) || (result.length() == 0)) {
            return messageKey;
        }
        return result;
    }

}
