/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import net.mapeadores.util.primitives.DegreSexagesimal;


/**
 *
 * @author Vincent Calame
 */
public final class SpecialCodes {

    private SpecialCodes() {

    }

    public static String getLatitudeSpecialCode(DegreSexagesimal degreSexagesimal) {
        if (degreSexagesimal.isPositif()) {
            return "North";
        } else {
            return "South";
        }
    }

    public static String getLongitudeSpecialCode(DegreSexagesimal degreSexagesimal) {
        if (degreSexagesimal.isPositif()) {
            return "East";
        } else {
            return "West";
        }
    }

}
