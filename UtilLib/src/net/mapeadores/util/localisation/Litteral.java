/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;


/**
 *
 * @author Vincent Calame
 */
public class Litteral {

    private final String wrappedString;

    public Litteral(String wrappedString) {
        this.wrappedString = wrappedString;
    }

    @Override
    public String toString() {
        return wrappedString;
    }

}
