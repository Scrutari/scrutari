/* UtilLib - Copyright (c) 2014-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.localisation;

import java.util.AbstractList;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public class LangPreferenceBuilder {

    private final Set<Lang> langSet = new LinkedHashSet<Lang>();

    public LangPreferenceBuilder() {

    }

    public boolean isEmpty() {
        return langSet.isEmpty();
    }

    public void addLang(Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        langSet.add(lang);
    }

    public void addLangs(Langs langs) {
        int size = langs.size();
        for (int i = 0; i < size; i++) {
            langSet.add(langs.get(i));
        }
    }

    public LangPreference toLangPreference() {
        int size = langSet.size();
        if (size == 0) {
            throw new IllegalStateException("No lang defined");
        }
        return new InternalLangPreference(langSet.toArray(new Lang[size]));
    }

    public static LangPreference build(Lang lang) {
        if (lang == null) {
            throw new IllegalArgumentException("lang is null");
        }
        Lang[] array = {lang};
        return new InternalLangPreference(array);
    }


    private static class InternalLangPreference extends AbstractList<Lang> implements LangPreference {

        private final Lang[] langArray;

        private InternalLangPreference(Lang[] langArray) {
            this.langArray = langArray;
        }

        @Override
        public int size() {
            return langArray.length;
        }

        @Override
        public Lang get(int index) {
            return langArray[index];
        }

        @Override
        public Lang getFirstLang() {
            return langArray[0];
        }

    }

}
