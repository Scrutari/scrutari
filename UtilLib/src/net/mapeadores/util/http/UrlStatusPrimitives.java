/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.http;

import java.io.IOException;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;


/**
 *
 * @author Vincent Calame
 */
public final class UrlStatusPrimitives {

    private UrlStatusPrimitives() {

    }

    public static void writeUrlStatus(UrlStatus urlStatus, PrimitivesWriter primitivesWriter) throws IOException {
        String state = urlStatus.getState();
        int specialCode = UrlStatusUtils.getSpecialCode(state);
        primitivesWriter.writeInt(specialCode);
        if (specialCode == 0) {
            primitivesWriter.writeString(state);
            int responseCode = urlStatus.getResponseCode();
            primitivesWriter.writeInt(responseCode);
            if (responseCode > 0) {
                primitivesWriter.writeString(urlStatus.getResponseMessage());
                RedirectionUrl redirectionUrl = urlStatus.getRedirectionUrl();
                if (redirectionUrl == null) {
                    primitivesWriter.writeBoolean(false);
                } else {
                    primitivesWriter.writeBoolean(true);
                    writeRedirectionUrl(redirectionUrl, primitivesWriter);
                }

            }
        }
    }

    public static void writeRedirectionUrl(RedirectionUrl redirectionUrl, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeString(redirectionUrl.getUrl());
        writeUrlStatus(redirectionUrl.getUrlStatus(), primitivesWriter);
    }

    public static UrlStatus readUrlStatus(PrimitivesReader primitivesReader) throws IOException {
        int specialCode = primitivesReader.readInt();
        UrlStatus specialUrlStatus = UrlStatusUtils.getSpecial(specialCode);
        if (specialUrlStatus != null) {
            return specialUrlStatus;
        }
        String state = primitivesReader.readString();
        state = UrlStatusUtils.checkState(state);
        int responseCode = primitivesReader.readInt();
        if (responseCode > 0) {
            String responseMessage = primitivesReader.readString();
            boolean withRedirection = primitivesReader.readBoolean();
            RedirectionUrl redirectionUrl = null;
            if (withRedirection) {
                redirectionUrl = readRedirectionUrl(primitivesReader);
            }
            return new UrlStatus(state, responseCode, responseMessage, redirectionUrl);
        } else {
            return new UrlStatus(state);
        }
    }

    public static RedirectionUrl readRedirectionUrl(PrimitivesReader primitivesReader) throws IOException {
        String url = primitivesReader.readString();
        UrlStatus urlStatus = readUrlStatus(primitivesReader);
        return new RedirectionUrl(url, urlStatus);
    }

}
