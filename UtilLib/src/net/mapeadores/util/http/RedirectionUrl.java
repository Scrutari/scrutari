/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.http;


/**
 *
 * @author Vincent Calame
 */
public class RedirectionUrl {

    private final UrlStatus urlStatus;
    private final String url;

    public RedirectionUrl(String url, UrlStatus urlStatus) {
        this.url = url;
        this.urlStatus = urlStatus;
    }

    public String getUrl() {
        return url;
    }

    public UrlStatus getUrlStatus() {
        return urlStatus;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (!url.isEmpty()) {
            buf.append(url);
            buf.append(" = ");
        }
        buf.append(urlStatus.toString());
        return buf.toString();
    }

}
