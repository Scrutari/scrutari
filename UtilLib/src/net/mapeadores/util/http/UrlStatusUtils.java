/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.http;


/**
 *
 * @author Vincent Calame
 */
public final class UrlStatusUtils {

    public final static UrlStatus MALFORMED = new UrlStatus(UrlStatus.MALFORMED);
    public final static UrlStatus RELATIVE = new UrlStatus(UrlStatus.RELATIVE);
    public final static UrlStatus UNKNOWN_PROTOCOL = new UrlStatus(UrlStatus.UNKNOWN_PROTOCOL);
    public final static UrlStatus UNREACHABLE = new UrlStatus(UrlStatus.UNREACHABLE);
    public final static UrlStatus TIMEOUT = new UrlStatus(UrlStatus.TIMEOUT);
    public final static UrlStatus BAD_HTTP = new UrlStatus(UrlStatus.BAD_HTTP);
    public final static UrlStatus MISSING_REDIRECTION = new UrlStatus(UrlStatus.MISSING_REDIRECTION);
    public final static UrlStatus REDIRECTION_LIMIT = new UrlStatus(UrlStatus.REDIRECTION_LIMIT);
    public final static UrlStatus OK = new UrlStatus(UrlStatus.OK);

    private UrlStatusUtils() {

    }

    public static String checkState(String state) {
        switch (state) {
            case UrlStatus.BAD_HTTP:
                return UrlStatus.BAD_HTTP;
            case UrlStatus.HTTP_ERROR:
                return UrlStatus.HTTP_ERROR;
            case UrlStatus.MALFORMED:
                return UrlStatus.MALFORMED;
            case UrlStatus.MISSING_REDIRECTION:
                return UrlStatus.MISSING_REDIRECTION;
            case UrlStatus.NOT_FOUND:
                return UrlStatus.NOT_FOUND;
            case UrlStatus.OK:
                return UrlStatus.OK;
            case UrlStatus.REDIRECTION:
                return UrlStatus.REDIRECTION;
            case UrlStatus.REDIRECTION_LIMIT:
                return UrlStatus.REDIRECTION_LIMIT;
            case UrlStatus.RELATIVE:
                return UrlStatus.RELATIVE;
            case UrlStatus.UNKNOWN_PROTOCOL:
                return UrlStatus.UNKNOWN_PROTOCOL;
            case UrlStatus.UNREACHABLE:
                return UrlStatus.UNREACHABLE;
            case UrlStatus.TIMEOUT:
                return UrlStatus.TIMEOUT;
            default:
                return state;
        }
    }

    static int getSpecialCode(String state) {
        switch (state) {
            case UrlStatus.BAD_HTTP:
                return 1;
            case UrlStatus.MALFORMED:
                return 2;
            case UrlStatus.MISSING_REDIRECTION:
                return 3;
            case UrlStatus.OK:
                return 4;
            case UrlStatus.REDIRECTION_LIMIT:
                return 5;
            case UrlStatus.RELATIVE:
                return 6;
            case UrlStatus.UNKNOWN_PROTOCOL:
                return 7;
            case UrlStatus.UNREACHABLE:
                return 8;
            case UrlStatus.TIMEOUT:
                return 9;
            default:
                return 0;
        }
    }

    static UrlStatus getSpecial(int specialCode) {
        switch (specialCode) {
            case 1:
                return BAD_HTTP;
            case 2:
                return MALFORMED;
            case 3:
                return MISSING_REDIRECTION;
            case 4:
                return OK;
            case 5:
                return REDIRECTION_LIMIT;
            case 6:
                return RELATIVE;
            case 7:
                return UNKNOWN_PROTOCOL;
            case 8:
                return UNREACHABLE;
            case 9:
                return TIMEOUT;
            default:
                return null;
        }
    }

}
