/* UtilLib - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.http;


/**
 *
 * @author Vincent Calame
 */
public final class UrlStatus {

    public final static String BAD_HTTP = "bad_http";
    public final static String HTTP_ERROR = "http_error";
    public final static String MALFORMED = "malformed";
    public final static String MISSING_REDIRECTION = "missing_redirection";
    public final static String NOT_FOUND = "not_found";
    public final static String OK = "ok";
    public final static String REDIRECTION = "redirection";
    public final static String REDIRECTION_LIMIT = "redirection_limit";
    public final static String RELATIVE = "relative";
    public final static String TIMEOUT = "timeout";
    public final static String UNKNOWN_PROTOCOL = "unknown_protocol";
    public final static String UNREACHABLE = "unreachable";
    private final String state;
    private final int responseCode;
    private final String responseMessage;
    private final RedirectionUrl redirectionUrl;

    UrlStatus(String state) {
        this.state = state;
        this.responseCode = -1;
        this.responseMessage = null;
        this.redirectionUrl = null;
    }

    UrlStatus(String state, int responseCode, String responseMessage, RedirectionUrl redirectionUrl) {
        this.state = state;
        this.responseCode = responseCode;
        if (responseMessage == null) {
            responseMessage = "";
        }
        this.responseMessage = responseMessage;
        this.redirectionUrl = redirectionUrl;
    }

    public String getState() {
        return state;
    }

    public boolean hasResponse() {
        return (responseCode > 0);
    }

    public int getResponseCode() {
        return responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public RedirectionUrl getRedirectionUrl() {
        return redirectionUrl;
    }

    @Override
    public String toString() {
        if (responseCode > 0) {
            StringBuilder buf = new StringBuilder();
            buf.append(state);
            buf.append(" (");
            buf.append(responseCode);
            if (!responseMessage.isEmpty()) {
                buf.append(" ");
                buf.append(responseMessage);
            }
            buf.append(")");
            if (redirectionUrl != null) {
                buf.append(" / ");
                buf.append(redirectionUrl.toString());
            }
            return buf.toString();
        } else {
            return state;
        }
    }

}
