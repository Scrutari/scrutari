/* UtilLib - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.format;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface FormatDef {

    public Object getParameterValue(String paramKey);

    public List<Pattern> getPatternList();

    public List<InnerSeparator> getInnerSeparatorList();

    public List<SourceSeparator> getSourceSeparatorList();


    public interface Pattern {

        public boolean isDefault();

        public String getPattern();

    }


    public interface InnerSeparator {

        public int getSourceIndex();

        public String getSeparator();

    }


    public interface SourceSeparator {

        /**
         * getSourceIndex1() < getSourceIndex2()
         */
        public int getSourceIndex1();

        public int getSourceIndex2();

        public String getSeparator();

    }

}
