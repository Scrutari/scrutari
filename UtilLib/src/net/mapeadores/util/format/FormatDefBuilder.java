/* UtilLib - Copyright (c) 2007-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.format;

import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.primitives.PrimUtils;


/**
 *
 * @author Vincent Calame
 */
public abstract class FormatDefBuilder {

    private boolean formatPatternListInit = false;
    private final Map<Integer, InternalInnerSeparator> innerSeparatorMap = new HashMap<Integer, InternalInnerSeparator>();
    private final Map<Long, InternalSourceSeparator> sourceSeparatorMap = new HashMap<Long, InternalSourceSeparator>();

    public FormatDefBuilder() {
    }

    public void addFormatPattern(String fP) {
        if (fP != null) {
            fP = fP.trim();
            if (fP.length() == 0) {
                fP = null;
            }
        }
        if ((fP == null) && (!formatPatternListInit)) {
            return;
        }
        formatPatternListInit = true;
        addCleanedFormatPattern(fP);
    }

    public void setCastType(short castType) {
        put(FormatConstants.CAST_PARAMKEY, castType);
    }

    public void setFixedChar(char carac) {
        put(FormatConstants.FIXEDCHAR_PARAMKEY, carac);
    }

    public void putBooleanValue(String key, boolean value) {
        put(key, value);
    }

    public void putStringValue(String key, String value) {
        if (value == null) {
            remove(key);
        } else {
            put(key, value);
        }
    }

    public void putIntValue(String key, int value) {
        if (value < 0) {
            value = -1;
        }
        put(key, value);
    }

    public void setCalcul(Calcul calcul) {
        if (calcul == null) {
            remove(FormatConstants.CALCUL_PARAMKEY);
        } else {
            put(FormatConstants.CALCUL_PARAMKEY, calcul);
        }
    }

    public void setSum(boolean withSum, short sumCastType) {
        if (withSum) {
            put(FormatConstants.SUM_PARAMKEY, sumCastType);
        } else {
            remove(FormatConstants.SUM_PARAMKEY);
        }
    }

    public void setFormula(boolean isFormula, short formulaCastType) {
        if (isFormula) {
            put(FormatConstants.FORMULA_PARAMKEY, formulaCastType);
        } else {
            remove(FormatConstants.FORMULA_PARAMKEY);
        }
    }

    public void setInternalSeparator(int sourceIndex, String separator) {
        if (separator == null) {
            throw new IllegalArgumentException("separator is null");
        }
        InternalInnerSeparator innerSeparator = innerSeparatorMap.get(sourceIndex);
        if (innerSeparator == null) {
            innerSeparator = new InternalInnerSeparator(sourceIndex, separator);
            innerSeparatorMap.put(sourceIndex, innerSeparator);
            addInnerSeparator(innerSeparator);
        } else {
            innerSeparator.setSeparator(separator);
        }
    }

    public void setSourceSeparator(int sourceIndex1, int sourceIndex2, String separator) {
        if (separator == null) {
            throw new IllegalArgumentException("separator is null");
        }
        if (sourceIndex1 == sourceIndex2) {
            throw new IllegalArgumentException("sourceIndex1 == sourceIndex2");
        }
        if (sourceIndex1 > sourceIndex2) {
            int tmp = sourceIndex1;
            sourceIndex1 = sourceIndex2;
            sourceIndex2 = tmp;
        }
        long l = PrimUtils.toLong(sourceIndex1, sourceIndex2);
        InternalSourceSeparator sourceSeparator = sourceSeparatorMap.get(l);
        if (sourceSeparator == null) {
            sourceSeparator = new InternalSourceSeparator(sourceIndex1, sourceIndex2, separator);
            sourceSeparatorMap.put(l, sourceSeparator);
            addSourceSeparator(sourceSeparator);
        } else {
            sourceSeparator.setSeparator(separator);
        }
    }

    protected abstract void put(String paramName, Object paramValue);

    protected abstract void remove(String paramName);

    protected abstract void addInnerSeparator(FormatDef.InnerSeparator innerSeparator);

    protected abstract void addSourceSeparator(FormatDef.SourceSeparator sourceSeparator);

    protected abstract void addCleanedFormatPattern(String formatPattern);


    private static class InternalInnerSeparator implements FormatDef.InnerSeparator {

        private final int sourceIndex;
        private String separator;

        private InternalInnerSeparator(int sourceIndex, String separator) {
            this.sourceIndex = sourceIndex;
            this.separator = separator;
        }

        @Override
        public int getSourceIndex() {
            return sourceIndex;
        }

        @Override
        public String getSeparator() {
            return separator;
        }

        private void setSeparator(String separator) {
            this.separator = separator;
        }

    }


    private static class InternalSourceSeparator implements FormatDef.SourceSeparator {

        private final int sourceIndex1;
        private final int sourceIndex2;
        private String separator;

        private InternalSourceSeparator(int sourceIndex1, int sourceIndex2, String separator) {
            this.sourceIndex1 = sourceIndex1;
            this.sourceIndex2 = sourceIndex2;
            this.separator = separator;
        }

        @Override
        public int getSourceIndex1() {
            return sourceIndex1;
        }

        @Override
        public int getSourceIndex2() {
            return sourceIndex2;
        }

        @Override
        public String getSeparator() {
            return separator;
        }

        private void setSeparator(String separator) {
            this.separator = separator;
        }

    }

}
