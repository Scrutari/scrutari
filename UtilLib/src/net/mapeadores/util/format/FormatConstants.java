/* UtilLib - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.format;


/**
 *
 * @author Vincent Calame
 */
public interface FormatConstants {

    /**
     * Catégorie des erreurs sur la source
     */
    public final static String SEVERE_SOURCE = "severe.format.source";
    /**
     * Catégorie des erreurs sur le gabarit
     */
    public final static String SEVERE_PATTERN = "severe.format.pattern";
    /**
     * Catégorie des erreurs sur les instructions
     */
    public final static String SEVERE_INSTRUCTION = "severe.format.instruction";
    /**
     * Catégorie des avertissements sur les instructions
     */
    public final static String WARNING_INSTRUCTION = "warning.format.instruction";
    /**
     * Catégorie des erreurs diverses sur la syntaxe
     */
    public final static String SEVERE_SYNTAX = "severe.format.syntax";
    /**
     * Catégorie des avertissements divers sur la syntaxe
     */
    public final static String WARNING_SYNTAX = "warning.format.syntax";
    /**
     * Catégorie des avertissements sur le contenu de la fichothèque (par
     * exemple, un thésaurus qui n'existe pas)
     */
    public final static String WARNING_FICHOTHEQUE = "warning.format.fichotheque";
    public final static int MAX_POSITION = 999999999;
    public final static int UNDEFINED = -9;
    public final static short NO_CAST = 0;
    public final static short INTEGER_CAST = 1;
    public final static short DECIMAL_CAST = 2;
    public final static short DATE_CAST = 3;
    public final static short MONEY_CAST = 4;
    public final static short PERCENTAGE_CAST = 5;
    public final static short JSON_CAST = 6;
    public final static String CALCUL_PARAMKEY = "calc";
    public final static String CAST_PARAMKEY = "cast";
    public final static String MAXLENGTH_PARAMKEY = "maxlength";
    public final static String FIXEDLENGTH_PARAMKEY = "fixedlength";
    public final static String FIXEDCHAR_PARAMKEY = "fixedchar";
    public final static String FIXEDEMPTY_PARAMKEY = "fixedempty";
    public final static String DEFAULTVALUE_PARAMKEY = "defval";
    public final static String GLOBALSELECT_PARAMKEY = "globalselect";
    public final static String JSONARRAY_PARAMKEY = "jsonarray";
    public final static String LIMIT_PARAMKEY = "limit";
    public final static String EMPTYTONULL_PARAMKEY = "null";
    public final static String POSITION_PARAMKEY = "pos";
    public final static String POSTTRANSFORM_PARAMKEY = "posttransform";
    public final static String PREFIX_PARAMKEY = "prefix";
    public final static String SEPARATOR_PARAMKEY = "sep";
    public final static String SUFFIX_PARAMKEY = "suffix";
    public final static String SUM_PARAMKEY = "sum";
    public final static String COLUMNSUM_PARAMKEY = "columnsum";
    public final static String UNIQUETEST_PARAMKEY = "unique";
    public final static String FORMULA_PARAMKEY = "formula";
    public final static String DATE_PARAMVALUE = "date";
    public final static String DECIMAL_PARAMVALUE = "decimal";
    public final static String INTEGER_PARAMVALUE = "integer";
    public final static String MONEY_PARAMVALUE = "money";
    public final static String PERCENTAGE_PARAMVALUE = "percentage";
    public final static String JSON_PARAMVALUE = "json";
    public final static String LAST_PARAMVALUE = "last";

}
