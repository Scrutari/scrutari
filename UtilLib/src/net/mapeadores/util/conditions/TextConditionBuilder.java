/* UtilLib - Copyright (c) 2016-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conditions;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class TextConditionBuilder {

    private final String logicalOperator;
    private final List<TextTest> excludingList = new ArrayList<TextTest>();
    private final List<TextTest> includingList = new ArrayList<TextTest>();

    public TextConditionBuilder(String logicalOperator) {
        this.logicalOperator = logicalOperator;
    }

    public TextConditionBuilder addTextTest(String text) {
        TextTest textTest = ConditionsUtils.parseTextTest(text);
        if (textTest != null) {
            addTextTest(textTest);
        }
        return this;
    }

    public TextConditionBuilder addTextTest(TextTest textTest) {
        short testType = textTest.getTestType();
        if (ConditionsUtils.isExcludingPartTestType(testType)) {
            excludingList.add(ConditionsUtils.cloneTextTest(textTest));
        } else if (ConditionsUtils.isIncludingPartTestType(testType)) {
            includingList.add(ConditionsUtils.cloneTextTest(textTest));
        }
        return this;
    }

    public TextCondition toTextCondition() {
        TextTest[] excludingArray = excludingList.toArray(new TextTest[excludingList.size()]);
        TextTest[] includingArray = includingList.toArray(new TextTest[includingList.size()]);
        return new InternalTextCondition(logicalOperator, ConditionsUtils.wrap(excludingArray), ConditionsUtils.wrap(includingArray));
    }

    public static TextConditionBuilder init(String logicalOperator) {
        return new TextConditionBuilder(logicalOperator);
    }

    public static TextCondition build(TextTest textTest) {
        return TextConditionBuilder.init(ConditionsConstants.LOGICALOPERATOR_AND)
                .addTextTest(textTest)
                .toTextCondition();
    }


    private static class InternalTextCondition implements TextCondition {

        private final String logicalOperator;
        private final List<TextTest> excludingList;
        private final List<TextTest> includingList;

        public InternalTextCondition(String logicalOperator, List<TextTest> excludingList, List<TextTest> includingList) {
            this.logicalOperator = logicalOperator;
            this.excludingList = excludingList;
            this.includingList = includingList;
        }

        @Override
        public String getLogicalOperator() {
            return logicalOperator;
        }

        @Override
        public List<TextTest> getExcludingTextTestList() {
            return excludingList;
        }

        @Override
        public List<TextTest> getIncludingTextTestList() {
            return includingList;
        }


    }

}
