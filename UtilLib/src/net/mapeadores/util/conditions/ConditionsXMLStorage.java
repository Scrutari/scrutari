/* UtilLib - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conditions;

import java.io.IOException;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 *
 * @author Vincent Calame
 */
public final class ConditionsXMLStorage {

    private ConditionsXMLStorage() {
    }

    public static void appendTestText(XMLWriter xmlWriter, TextCondition textCondition) throws IOException {
        for (TextTest textTest : textCondition.getIncludingTextTestList()) {
            appendTestText(xmlWriter, textTest);
        }
        for (TextTest textTest : textCondition.getExcludingTextTestList()) {
            appendTestText(xmlWriter, textTest);
        }
    }

    public static void appendTestText(XMLWriter xmlWriter, TextTest textTest) throws IOException {
        xmlWriter.startOpenTag("text-test");
        xmlWriter.addAttribute("type", ConditionsUtils.testTypeToString(textTest.getTestType()));
        xmlWriter.endOpenTag();
        xmlWriter.addText(textTest.getText());
        xmlWriter.closeTag("text-test", false);
    }

    public static TextCondition readTextCondition(Element element) {
        String logicalOperator = element.getAttribute("operator");
        if (logicalOperator.isEmpty()) {
            logicalOperator = element.getAttribute("mode");
        }
        logicalOperator = ConditionsConstants.checkLogicalOperator(logicalOperator, ConditionsConstants.LOGICALOPERATOR_OR);
        String q = element.getAttribute("q");
        if (q.isEmpty()) {
            q = element.getAttribute("raw");
        }
        if (q.length() > 0) {
            String qType = element.getAttribute("q-type");
            if (qType.equals("simple")) {
                return ConditionsUtils.parseSimpleCondition(q, logicalOperator);
            } else {
                return ConditionsUtils.parseCondition(q, logicalOperator);
            }
        }
        TextConditionBuilder conditionBuilder = new TextConditionBuilder(logicalOperator);
        NodeList liste = element.getChildNodes();
        for (int i = 0; i < liste.getLength(); i++) {
            Node nd = liste.item(i);
            if (nd.getNodeType() == Node.ELEMENT_NODE) {
                Element child = (Element) nd;
                switch (child.getTagName()) {
                    case "text-test":
                    case "condition": {
                        TextTest textTest = readTextTest(child);
                        if (textTest != null) {
                            conditionBuilder.addTextTest(textTest);
                        }
                        break;
                    }
                }
            }
        }
        return conditionBuilder.toTextCondition();
    }

    private static TextTest readTextTest(Element element) {
        String text = XMLUtils.getData(element);
        String type = element.getAttribute("type");
        short testType;
        if (type.isEmpty()) {
            testType = ConditionsConstants.CONTAINS_TEST;
        } else {
            try {
                testType = ConditionsUtils.testTypeToShort(type);
            } catch (IllegalArgumentException iae) {
                return null;
            }
        }
        return ConditionsUtils.toTextTest(testType, text);
    }

}
