/* UtilLib - Copyright (c) 2006-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.conditions;

import net.mapeadores.util.text.TextConstants;


/**
 *
 * @author Vincent Calame
 */
public interface ConditionsConstants {

    public final static short STARTSWITH_TEST = 11;
    public final static short ENDSWITH_TEST = 12;
    public final static short CONTAINS_TEST = 13;
    public final static short MATCHES_TEST = 14;
    public final static short EMPTY_TEST = 15;
    public final static short NOT_STARTSWITH_TEST = 16;
    public final static short NOT_ENDSWITH_TEST = 17;
    public final static short NOT_CONTAINS_TEST = 18;
    public final static short NOT_MATCHES_TEST = 19;
    public final static short NOT_EMPTY_TEST = 20;
    public final static String LOGICALOPERATOR_AND = "and";
    public final static String LOGICALOPERATOR_OR = "or";
    public final static short UNKNOWN_CONDITION = 0;
    public final static short NEUTRAL_CONDITION = 1;
    public final static short IMPOSSIBLE_CONDITION = 2;
    public final static short EMPTY_CONDITION = 3;
    public final static short NOTEMPTY_CONDITION = 4;
    public final static short PARTIAL_CONDITION = 5;
    public final static short PARTIALOREMPTY_CONDITION = 6;
    public final static short INDEPENDANT_COVER = 0;
    public final static short IMPOSSIBLE_COVER = 1;
    public final static short CONTAINS_COVER = 2;
    public final static short ISCONTAINED_COVER = 3;


    public static String checkLogicalOperator(String value, String defaultLogicalOperator) {
        if (value == null) {
            return defaultLogicalOperator;
        }
        switch (value) {
            case LOGICALOPERATOR_AND:
            case LOGICALOPERATOR_OR:
                return value;
            default:
                return defaultLogicalOperator;
        }
    }

    public static short convertSearchType(int searchType) {
        switch (searchType) {
            case TextConstants.CONTAINS:
                return CONTAINS_TEST;
            case TextConstants.ENDSWITH:
                return ENDSWITH_TEST;
            case TextConstants.STARTSWITH:
                return STARTSWITH_TEST;
            case TextConstants.MATCHES:
            default:
                return MATCHES_TEST;
        }
    }

}
