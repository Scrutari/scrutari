/* UtilLib - Copyright (c) 2008-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class PageHtmlPrinter extends HtmlPrinter {

    private final List<HtmlAttributes> iconAttributesList = new ArrayList<HtmlAttributes>();
    private final List<String> cssUrlList = new ArrayList<String>();
    private final List<String> jsUrlList = new ArrayList<String>();
    private HtmlAttributes bodyHTMLAttributes = null;
    private String bodyCssClass = null;
    private List<TrustedHtml> cssStyleList = null;


    public PageHtmlPrinter() {
    }

    public final PageHtmlPrinter setBodyHTMLAttributes(HtmlAttributes bodyHTMLAttributes) {
        this.bodyHTMLAttributes = bodyHTMLAttributes;
        return this;
    }

    public final PageHtmlPrinter setBodyCssClass(String bodyCssClass) {
        this.bodyCssClass = bodyCssClass;
        return this;
    }

    public final void addStyle(TrustedHtml cssStyle) {
        if (cssStyleList == null) {
            cssStyleList = new ArrayList<TrustedHtml>();
        }
        cssStyleList.add(cssStyle);
    }

    public PageHtmlPrinter addCssUrl(String cssUrl) {
        cssUrlList.add(cssUrl);
        return this;
    }

    public PageHtmlPrinter addJsUrl(String jsUrl) {
        jsUrlList.add(jsUrl);
        return this;
    }

    public void start(Lang lang, String title) {
        this
                .DOCTYPE_html5()
                .HTML(lang);
        this
                .HEAD();
        this
                .TITLE()
                .__escape(title)
                ._TITLE();
        this
                .META_htmlcontenttype();
        if (!jsUrlList.isEmpty()) {
            this
                    .META(HA.httpEquiv("Content-Script-Type").content("text/javascript"));
        }
        for (String cssUrl : cssUrlList) {
            this
                    .LINK(HA.type("text/css").href(cssUrl).rel("stylesheet"));
        }
        for (HtmlAttributes iconAttributes : iconAttributesList) {
            this
                    .LINK(iconAttributes);
        }
        if (!jsUrlList.isEmpty()) {
            HtmlAttributes scriptAttributes = HA.type("text/javascript");
            int length = jsUrlList.size();
            for (int i = 0; i < length; i++) {
                scriptAttributes.src(jsUrlList.get(i));
                this
                        .SCRIPT(scriptAttributes)
                        ._SCRIPT();
            }
        }
        if (cssStyleList != null) {
            this
                    .STYLE();
            for (TrustedHtml cssStyle : cssStyleList) {
                this
                        .__append(cssStyle);
            }
            this
                    ._STYLE();
        }
        this
                ._HEAD();
        if (bodyHTMLAttributes != null) {
            this
                    .BODY(bodyHTMLAttributes);

        } else {
            this
                    .BODY(bodyCssClass);
        }
    }

    public void end() {
        this
                ._BODY()
                ._HTML();
    }

    public void addIconPng(String path, String size) {
        addIcon(path, size + "x" + size, "image/png", "icon");
    }

    public void addIcon(String path, String sizes, String mimeType, String relType) {
        iconAttributesList.add(HA.href(path).type(mimeType).rel(relType).attr("sizes", sizes));
    }

    public void addIcon(HtmlAttributes iconAttributes) {
        iconAttributesList.add(iconAttributes);
    }

}
