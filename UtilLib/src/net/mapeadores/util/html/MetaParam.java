/* UtilLib_Html - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;


/**
 *
 * @author Vincent Calame
 */
public final class MetaParam {

    private final String protocol;
    private final String name;

    public MetaParam(String protocol, String name) {
        this.protocol = protocol;
        this.name = name;
    }

    public String getProtocol() {
        return protocol;
    }

    public String getName() {
        return name;
    }

}
