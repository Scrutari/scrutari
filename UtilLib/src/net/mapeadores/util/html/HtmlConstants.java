/* UtilLib - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;


/**
 *
 * @author Vincent Calame
 */
public interface HtmlConstants {

    public final static String MULTIPART_ENCTYPE = "multipart/form-data";
    public final static String CHECKBOX_TYPE = "checkbox";
    public final static String RADIO_TYPE = "radio";
    public final static String HIDDEN_TYPE = "hidden";
    public final static String TEXT_TYPE = "text";
    public final static String FILE_TYPE = "file";
    public final static String SUBMIT_TYPE = "submit";
    public final static String BUTTON_TYPE = "button";
    public final static String PASSWORD_TYPE = "password";
    public final static String GET_METHOD = "GET";
    public final static String POST_METHOD = "POST";
    public final static String TOP_TARGET = "_top";
    public final static String BLANK_TARGET = "_blank";
}
