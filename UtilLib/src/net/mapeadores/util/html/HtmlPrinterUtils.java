/* UtilLib - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LineMessage;
import net.mapeadores.util.logging.LogUtils;


/**
 *
 * @author Vincent Calame
 */
public final class HtmlPrinterUtils {

    private HtmlPrinterUtils() {
    }

    public static boolean printCommandMessage(HtmlPrinter hp, @Nullable CommandMessage commandMessage, String doneClasses, String errorClasses) {
        if (commandMessage == null) {
            return false;
        }
        String classes;
        if (commandMessage.isErrorMessage()) {
            classes = errorClasses;
        } else {
            classes = doneClasses;
        }
        printCommandMessage(hp, commandMessage, classes);
        return true;
    }

    public static boolean printCommandMessage(HtmlPrinter hp, @Nullable CommandMessage commandMessage, HtmlAttributes doneAttributes, HtmlAttributes errorAttributes) {
        if (commandMessage == null) {
            return false;
        }
        HtmlAttributes spanAttr = doneAttributes;
        if (commandMessage.isErrorMessage()) {
            spanAttr = errorAttributes;
        }
        printCommandMessage(hp, commandMessage, spanAttr);
        return true;
    }

    private static boolean printCommandMessage(HtmlPrinter hp, CommandMessage commandMessage, HtmlAttributes attributes) {
        hp
                .P()
                .SPAN(attributes)
                .__localize(commandMessage)
                ._SPAN();
        if (commandMessage.hasMultiError()) {
            hp
                    .SPAN(attributes)
                    .SMALL();
            for (Message message : commandMessage.getMultiErrorList()) {
                printSubMessage(hp, message);
            }
            hp
                    ._SMALL()
                    ._SPAN();
        } else if (commandMessage.hasLog()) {
            hp
                    .BR()
                    .PRE()
                    .__escape(LogUtils.toString(commandMessage.getMessageLog()), true)
                    ._PRE();
        }
        hp
                ._P();
        return true;
    }

    private static boolean printSubMessage(HtmlPrinter hp, Message message) {
        hp
                .BR();
        if (message instanceof LineMessage) {
            hp
                    .__append(((LineMessage) message).getLineNumber())
                    .__escape(": ");
        }
        hp
                .__localize(message);
        return true;
    }

    private static boolean printCommandMessage(HtmlPrinter hp, CommandMessage commandMessage, String classes) {
        hp
                .P()
                .SPAN(classes)
                .__localize(commandMessage)
                ._SPAN();
        if (commandMessage.hasMultiError()) {
            hp
                    .SPAN(classes)
                    .SMALL();
            for (Message message : commandMessage.getMultiErrorList()) {
                printSubMessage(hp, message);
            }
            hp
                    ._SMALL()
                    ._SPAN();
        } else if (commandMessage.hasLog()) {
            hp
                    .BR()
                    .PRE()
                    .__escape(LogUtils.toString(commandMessage.getMessageLog()), true)
                    ._PRE();
        }
        hp
                ._P();
        return true;
    }

}
