/* UtilLib - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.text.DecimalFormatSymbols;
import net.mapeadores.util.money.Amount;
import net.mapeadores.util.primitives.Decimal;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.table.TableWriter;


/**
 *
 * @author Vincent Calame
 */
public class HtmlTableWriter implements TableWriter {

    private final HtmlPrinter hp;
    private final DecimalFormatSymbols symbols;
    private int rowNumber;
    private int columnNumber;

    public HtmlTableWriter(HtmlPrinter hp, DecimalFormatSymbols symbols) {
        this.hp = hp;
        this.symbols = symbols;
        this.rowNumber = 0;
    }

    @Override
    public int startRow() {
        rowNumber++;
        hp
                .TR();
        columnNumber = 0;
        return rowNumber;
    }

    @Override
    public int addIntegerCell(Long lg) {
        columnNumber++;
        if (lg == null) {
            hp
                    .TD()
                    ._TD();
        } else {
            hp
                    .TD()
                    .__append(lg)
                    ._TD();
        }
        return columnNumber;
    }

    @Override
    public int addDecimalCell(Decimal decimal) {
        columnNumber++;
        if (decimal == null) {
            hp
                    .TD()
                    ._TD();
        } else {
            hp
                    .TD()
                    .__escape(decimal)
                    ._TD();
        }
        return columnNumber;
    }

    @Override
    public int addStringCell(String s) {
        columnNumber++;
        if (s == null) {
            hp
                    .TD()
                    ._TD();
        } else {
            hp
                    .TD()
                    .__(printStringValue(hp, s))
                    ._TD();
        }
        return columnNumber;
    }

    @Override
    public int addDateCell(FuzzyDate date) {
        columnNumber++;
        if (date == null) {
            hp
                    .TD()
                    ._TD();
        } else {
            hp
                    .TD()
                    .__escape(date.toISOString())
                    ._TD();
        }
        return columnNumber;
    }

    @Override
    public int addMoneyCell(Amount amount) {
        columnNumber++;
        if (amount == null) {
            hp
                    .TD()
                    ._TD();
        } else {
            hp
                    .TD()
                    .__escape(amount.toLitteralString(symbols, false))
                    ._TD();
        }
        return columnNumber;
    }

    @Override
    public int addPercentageCell(Decimal decimal) {
        columnNumber++;
        if (decimal == null) {
            hp
                    .TD()
                    ._TD();
        } else {
            hp
                    .TD()
                    .__escape(decimal)
                    ._TD();
        }
        return columnNumber;
    }

    @Override
    public int endRow() {
        hp
                ._TR();
        return rowNumber;
    }

    public static boolean printStringValue(HtmlPrinter hp, String s) {
        int length = s.length();
        for (int i = 0; i < length; i++) {
            char carac = s.charAt(i);
            switch (carac) {
                case '\r':
                    if (i < (length - 1)) {
                        if (s.charAt(i + 1) == '\n') {
                            i++;
                        }
                    }
                case '\n':
                    hp
                            .BR();
                    break;
                default:
                    hp
                            .__escape(carac);
            }
        }
        return true;
    }

}
