/* UtilLib - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.function.Consumer;


/**
 *
 * @author Vincent Calame
 */
public class HA {

    private HA() {
    }

    public static HtmlAttributes id(String id) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.id(id);
    }

    public static HtmlAttributes forId(String forId) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.forId(forId);
    }

    public static HtmlAttributes classes(String classes) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.classes(classes);
    }

    public static HtmlAttributes classes(boolean addCondition, String classes) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        if (addCondition) {
            return htmlAttributes.classes(classes);
        } else {
            return htmlAttributes;
        }
    }

    public static HtmlAttributes name(String name) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.name(name);
    }

    public static HtmlAttributes action(String action) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.action(action);
    }

    public static HtmlAttributes type(String type) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.type(type);
    }

    public static HtmlAttributes value(String value) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.value(value);
    }

    public static HtmlAttributes width(String width) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.width(width);
    }

    public static HtmlAttributes style(String style) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.style(style);
    }

    public static HtmlAttributes src(CharSequence src) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.src(src.toString());
    }

    public static HtmlAttributes href(CharSequence href) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.href(href.toString());
    }

    public static HtmlAttributes label(String label) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.label(label);
    }

    public static HtmlAttributes colspan(int colspan) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.colspan(colspan);
    }

    public static HtmlAttributes rowspan(int rowspan) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.colspan(rowspan);
    }

    public static HtmlAttributes httpEquiv(String httpEquiv) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.httpEquiv(httpEquiv);
    }

    public static HtmlAttributes attr(String name, String value) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.attr(name, value);
    }

    public static HtmlAttributes open(boolean open) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        return htmlAttributes.open(open);
    }

    public static HtmlAttributes populate(Consumer<HtmlAttributes> consumer) {
        HtmlAttributes htmlAttributes = new HtmlAttributes();
        consumer.accept(htmlAttributes);
        return htmlAttributes;
    }

}
