/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.function.BiConsumer;
import java.util.function.Consumer;


/**
 *
 * @author Vincent Calame
 */
public final class WrapperFactory {

    private WrapperFactory() {

    }

    public static HtmlWrapper li(String cssClasses) {
        return new LiWrapper(cssClasses);
    }

    public static HtmlWrapper ul(String cssClasses) {
        return new UlWrapper(cssClasses);
    }

    public static HtmlWrapper div(String cssClasses) {
        return new DivWrapper(cssClasses);
    }

    public static HtmlWrapper div(HtmlAttributes htmlAttributes) {
        return new AttrDivWrapper(htmlAttributes);
    }


    private static class LiWrapper implements HtmlWrapper {

        private final String cssClasses;

        private LiWrapper(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void wrap(HtmlPrinter hp, Consumer<HtmlPrinter> consumer) {
            hp
                    .LI(cssClasses);
            consumer.accept(hp);
            hp
                    ._LI();
        }

        @Override
        public void wrap(HtmlPrinter hp, Runnable runnable) {
            hp
                    .LI(cssClasses);
            runnable.run();
            hp
                    ._LI();
        }

        @Override
        public void wrap(HtmlPrinter hp, BiConsumer<HtmlPrinter, Object> consumer, Object argument) {
            hp
                    .LI(cssClasses);
            consumer.accept(hp, argument);
            hp
                    ._LI();
        }

    }


    private static class UlWrapper implements HtmlWrapper {

        private final String cssClasses;

        private UlWrapper(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void wrap(HtmlPrinter hp, Consumer<HtmlPrinter> consumer) {
            hp
                    .UL(cssClasses);
            consumer.accept(hp);
            hp
                    ._UL();
        }

        @Override
        public void wrap(HtmlPrinter hp, Runnable runnable) {
            hp
                    .UL(cssClasses);
            runnable.run();
            hp
                    ._UL();
        }

        @Override
        public void wrap(HtmlPrinter hp, BiConsumer<HtmlPrinter, Object> consumer, Object argument) {
            hp
                    .UL(cssClasses);
            consumer.accept(hp, argument);
            hp
                    ._UL();
        }

    }


    private static class DivWrapper implements HtmlWrapper {

        private final String cssClasses;

        private DivWrapper(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void wrap(HtmlPrinter hp, Consumer<HtmlPrinter> consumer) {
            hp
                    .DIV(cssClasses);
            consumer.accept(hp);
            hp
                    ._DIV();
        }

        @Override
        public void wrap(HtmlPrinter hp, Runnable runnable) {
            hp
                    .DIV(cssClasses);
            runnable.run();
            hp
                    ._DIV();
        }

        @Override
        public void wrap(HtmlPrinter hp, BiConsumer<HtmlPrinter, Object> consumer, Object argument) {
            hp
                    .DIV(cssClasses);
            consumer.accept(hp, argument);
            hp
                    ._DIV();
        }

    }


    private static class AttrDivWrapper implements HtmlWrapper {

        private final HtmlAttributes htmlAttributes;

        private AttrDivWrapper(HtmlAttributes htmlAttributes) {
            this.htmlAttributes = htmlAttributes;
        }

        @Override
        public void wrap(HtmlPrinter hp, Consumer<HtmlPrinter> consumer) {
            hp
                    .DIV(htmlAttributes);
            consumer.accept(hp);
            hp
                    ._DIV();
        }

        @Override
        public void wrap(HtmlPrinter hp, Runnable runnable) {
            hp
                    .DIV(htmlAttributes);
            runnable.run();
            hp
                    ._DIV();
        }

        @Override
        public void wrap(HtmlPrinter hp, BiConsumer<HtmlPrinter, Object> consumer, Object argument) {
            hp
                    .DIV(htmlAttributes);
            consumer.accept(hp, argument);
            hp
                    ._DIV();
        }

    }

}
