/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.function.Consumer;


/**
 *
 * @author Vincent Calame
 */
public final class ConsumerFactory {

    private ConsumerFactory() {

    }

    public static Consumer<HtmlPrinter> p(String cssClasses, String locKey) {
        return new LocP(cssClasses, locKey);
    }

    public static Consumer<HtmlPrinter> span(String cssClasses) {
        return new EmptySpan(cssClasses);
    }


    private static class LocP implements Consumer<HtmlPrinter> {

        private final String cssClasses;
        private final String locKey;

        private LocP(String cssClasses, String locKey) {
            this.cssClasses = cssClasses;
            this.locKey = locKey;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .P(cssClasses)
                    .__localize(locKey)
                    ._P();
        }

    }


    private static class EmptySpan implements Consumer<HtmlPrinter> {

        private final String cssClasses;

        private EmptySpan(String cssClasses) {
            this.cssClasses = cssClasses;
        }

        @Override
        public void accept(HtmlPrinter hp) {
            hp
                    .SPAN(cssClasses)
                    ._SPAN();
        }

    }

}
