/* UtilLib_Html - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.text.ParseException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public final class MetaReportUtils {

    private static final MetaParam[] TITLE = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Title"),
        new MetaParam(MetaReport.OPENGRAPH, "og:title"),
        new MetaParam(MetaReport.SCHEMA, "name"),
        new MetaParam(MetaReport.HTML, "title")};
    private static final MetaParam[] DESCRIPTION = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Description"),
        new MetaParam(MetaReport.OPENGRAPH, "og:description"),
        new MetaParam(MetaReport.SCHEMA, "abstract"),
        new MetaParam(MetaReport.HTML, "description")};
    private static final MetaParam[] AUTHORS = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Creator"),
        new MetaParam(MetaReport.OPENGRAPH, "article:author"),
        new MetaParam(MetaReport.SCHEMA, "author"),
        new MetaParam(MetaReport.OPENGRAPH, "book:author"),
        new MetaParam(MetaReport.HTML, "author")};
    private static final MetaParam[] DATE = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Date"),
        new MetaParam(MetaReport.OPENGRAPH, "article:published_time"),
        new MetaParam(MetaReport.SCHEMA, "datePublished"),
        new MetaParam(MetaReport.OPENGRAPH, "book:release_date")};
    private static final MetaParam[] LANG = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Language"),
        new MetaParam(MetaReport.OPENGRAPH, "og:locale"),
        new MetaParam(MetaReport.SCHEMA, "inLanguage"),
        new MetaParam(MetaReport.HTML, "lang"),};
    private static final MetaParam[] PUBLISHERS = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Publisher"),
        new MetaParam(MetaReport.SCHEMA, "name"),
        new MetaParam(MetaReport.HTML, "publisher")};
    private static final MetaParam[] KEYWORDS = {
        new MetaParam(MetaReport.DUBLIN_CORE, "Subject"),
        new MetaParam(MetaReport.OPENGRAPH, "article:tag"),
        new MetaParam(MetaReport.SCHEMA, "keywords"),
        new MetaParam(MetaReport.OPENGRAPH, "book:tag"),
        new MetaParam(MetaReport.HTML, "keywords")};


    private MetaReportUtils() {

    }

    public static String getTitle(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(TITLE);
        if (multiStringable != null) {
            return multiStringable.getStringValue(0);
        } else {
            return null;
        }
    }

    public static String getDescription(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(DESCRIPTION);
        if (multiStringable != null) {
            return multiStringable.toString("\n");
        } else {
            return null;
        }
    }

    public static String[] getAuthors(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(AUTHORS);
        if (multiStringable != null) {
            return multiStringable.toStringArray();
        } else {
            return null;
        }
    }

    public static FuzzyDate getDate(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(DATE);
        if (multiStringable != null) {
            String value = cleanDate(multiStringable.getStringValue(0));
            try {
                return FuzzyDate.parse(value);
            } catch (ParseException pe) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static Lang getLang(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(LANG);
        if (multiStringable != null) {
            String value = cleanLang(multiStringable.getStringValue(0));
            try {
                return Lang.parse(value);
            } catch (ParseException pe) {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String[] getPublishers(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(PUBLISHERS);
        if (multiStringable != null) {
            return multiStringable.toStringArray();
        } else {
            return null;
        }
    }

    public static String[] getKeywords(MetaReport metaReport) {
        MultiStringable multiStringable = metaReport.getValues(KEYWORDS);
        if (multiStringable != null) {
            return multiStringable.toStringArray();
        } else {
            return null;
        }
    }

    private static String cleanDate(String dateString) {
        int idx = dateString.indexOf('T');
        if (idx > 0) {
            return dateString.substring(0, idx);
        } else {
            return dateString;
        }
    }

    private static String cleanLang(String langString) {
        return langString.replace('_', '-');
    }

}
