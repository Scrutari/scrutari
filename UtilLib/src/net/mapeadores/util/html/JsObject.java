/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public class JsObject {

    private final Map<String, Object> map = new LinkedHashMap<String, Object>();

    public JsObject() {

    }

    public JsObject put(String name, Object value) {
        map.put(name, value);
        return this;
    }

    public boolean print(HtmlPrinter hp) {
        boolean next = false;
        hp
                .__escape("{")
                .__newLine();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (next) {
                hp
                        .__escape(',')
                        .__newLine();
            } else {
                next = true;
            }
            hp
                    .__escape(entry.getKey())
                    .__escape(": ")
                    .__(printValue(hp, entry.getValue()));

        }
        hp
                .__newLine()
                .__escape("}");
        return true;
    }

    private static boolean printValue(HtmlPrinter hp, Object value) {
        if (value == null) {
            hp
                    .__escape("null");
        } else if (value instanceof Integer) {
            hp
                    .__escape(value);
        } else if (value instanceof Long) {
            hp
                    .__escape(value);
        } else if (value instanceof Boolean) {
            if (value.equals(Boolean.TRUE)) {
                hp
                        .__escape("true");
            } else {
                hp
                        .__escape("false");
            }
        } else if (value instanceof JsObject) {
            ((JsObject) value).print(hp);
        } else if (value instanceof List) {
            List list = (List) value;
            hp
                    .__escape("[");
            boolean next = false;
            for (Object item : list) {
                if (next) {
                    hp
                            .__escape(",");
                } else {
                    next = true;
                }
                printValue(hp, item);
            }
            hp
                    .__escape("]");
        } else if (value instanceof Object[]) {
            Object[] array = (Object[]) value;
            hp
                    .__escape("[");
            boolean next = false;
            for (Object item : array) {
                if (next) {
                    hp
                            .__escape(",");
                } else {
                    next = true;
                }
                printValue(hp, item);
            }
            hp
                    .__escape("]");
        } else {
            hp
                    .__scriptLiteral(value.toString());
        }
        return true;
    }

    public static JsObject init() {
        return new JsObject();
    }

}
