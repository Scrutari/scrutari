/* UtilLib - Copyright (c) 2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;


/**
 *
 * @author Vincent Calame
 */
public interface AttributeConstants {

    public final static String COLOR_FORMAT = "color";
    public final static String URL_FORMAT = "url";
    public final static String DATATION_FORMAT = "datation";
    public final static String INTEGER_PREFIX = "integer";
    public final static String INTEGER_FORMAT = "integer";
    public final static String POSITIVE_INTEGER_FORMAT = "integer_positive";
    public final static String STRICTPOSITIVE_INTEGER_FORMAT = "integer_strictpositive";
}
