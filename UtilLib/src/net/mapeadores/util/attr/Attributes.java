/* UtilLib - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import java.util.List;
import java.util.RandomAccess;


/**
 *
 * @author Vincent Calame
 */
public interface Attributes extends List<Attribute>, RandomAccess {

    public Attribute getAttribute(AttributeKey key);

    public default String getFirstValue(AttributeKey key) {
        Attribute attribute = getAttribute(key);
        if (attribute == null) {
            return null;
        } else {
            return attribute.getFirstValue();
        }
    }

}
