/* UtilLib - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import net.mapeadores.util.text.CleanedString;


/**
 *
 * @author Vincent Calame
 */
public interface AttributeBuffer {

    public AttributeBuffer appendValue(AttributeKey attributeKey, CleanedString cleanedString);

    public default AttributeBuffer appendValue(AttributeKey attributeKey, String value) {
        if (value == null) {
            return this;
        }
        CleanedString cleanedString = CleanedString.newInstance(value);
        if (cleanedString != null) {
            appendValue(attributeKey, cleanedString);
        }
        return this;
    }

}
