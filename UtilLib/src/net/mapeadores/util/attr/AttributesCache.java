/* UtilLib - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import java.util.SortedMap;
import java.util.TreeMap;


/**
 *
 * @author Vincent Calame
 */
public class AttributesCache {

    private final SortedMap<AttributeKey, Attribute> attributeMap = new TreeMap<AttributeKey, Attribute>();
    private Attributes cachedAttributes;

    public AttributesCache() {

    }

    public synchronized void clear() {
        attributeMap.clear();
        cachedAttributes = null;
    }

    public synchronized void putAttributes(Attributes attributes) {
        for (Attribute attribute : attributes) {
            attributeMap.put(attribute.getAttributeKey(), attribute);
        }
        cachedAttributes = null;
    }

    public synchronized boolean putAttribute(Attribute attribute) {
        AttributeKey attributeKey = attribute.getAttributeKey();
        Attribute current = attributeMap.get(attributeKey);
        if (current != null) {
            if (AttributeUtils.isEqual(current, attribute)) {
                return false;
            }
        }
        attributeMap.put(attributeKey, attribute);
        cachedAttributes = null;
        return true;
    }

    public synchronized boolean removeAttribute(AttributeKey attributeKey) {
        Attribute attribute = attributeMap.remove(attributeKey);
        if (attribute != null) {
            cachedAttributes = null;
            return true;
        } else {
            return false;
        }
    }

    public Attributes getAttributes() {
        Attributes result = cachedAttributes;
        if (result == null) {
            result = cache();
        }
        return result;
    }

    private synchronized Attributes cache() {
        Attributes cache = build();
        cachedAttributes = cache;
        return cache;
    }

    private synchronized Attributes build() {
        int size = attributeMap.size();
        switch (size) {
            case 0:
                return AttributeUtils.EMPTY_ATTRIBUTES;
            case 1:
                return AttributeUtils.toSingletonAttributes(attributeMap.values().iterator().next());
            default:
                Attribute[] attributeArray = attributeMap.values().toArray(new Attribute[size]);
                return AttributesBuilder.toAttributes(attributeArray);
        }
    }

}
