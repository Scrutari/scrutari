/* UtilLib - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.attr;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrases;


/**
 *
 * @author Vincent Calame
 */
public interface AttributeDef {

    public AttributeKey getAttributeKey();

    @Nullable
    public Object getDefObject(String objectName);

    public Labels getTitleLabels();

    public Attributes getAttributes();

    public Phrases getPhrases();

    public default String getTitle(Lang preferredLang) {
        return getTitleLabels().seekLabelString(preferredLang, getAttributeKey().toString());
    }

}
