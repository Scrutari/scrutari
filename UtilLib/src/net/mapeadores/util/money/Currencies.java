/* UtilLib - Copyright (c) 2018-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.money;

import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.text.MultiStringable;


/**
 *
 * @author Vincent Calame
 */
public interface Currencies extends List<ExtendedCurrency>, RandomAccess, MultiStringable {

    @Override
    public default String[] toStringArray() {
        int length = size();
        String[] currencyArray = new String[length];
        for (int i = 0; i < length; i++) {
            currencyArray[i] = get(i).getCurrencyCode();
        }
        return currencyArray;
    }

    @Override
    public default String toString(String separator) {
        StringBuilder buf = new StringBuilder();
        int length = size();
        for (int i = 0; i < length; i++) {
            if (i > 0) {
                buf.append(separator);
            }
            buf.append(get(i).getCurrencyCode());
        }
        return buf.toString();
    }

    @Override
    public default int getStringSize() {
        return size();
    }

    @Override
    public default String getStringValue(int index) {
        return get(index).getCurrencyCode();
    }

}
