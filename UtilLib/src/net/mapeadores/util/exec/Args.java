/* BdfTool - Copyright (c) 2019-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exec;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface Args {

    public CommandDef getCommandDef();

    public boolean containsParameter(char parameterChar);

    public String getParameterValue(char parameterChar);

    public boolean containsParameter(String parameterName);

    public String getParameterValue(String parameterName);

    public List<String> getValueList();

}
