/* UtilLib - Copyright (c) 2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.exec;


/**
 *
 * @author Vincent Calame
 */
public interface ParameterDef {

    public char getLetter();

    public String getTitleLocKey();

    public boolean requireValue();

    public boolean isValidValue(String value);

}
