/* FichothequeLib_API - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.models;


/**
 *
 * @author Vincent Calame
 */
public interface PersonCore {

    /**
     * N'est jamais <em>null</em>.
     */
    public String getSurname();

    /**
     * N'est jamais <em>null</em>.
     */
    public String getForename();

    /**
     * N'est jamais <em>null</em>.
     */
    public String getNonlatin();

    public boolean isSurnameFirst();

    public default String getLatin() {
        String surname = getSurname();
        String forename = getForename();
        boolean surnameFirst = isSurnameFirst();
        if (forename.length() == 0) {
            if (surname.length() == 0) {
                return "";
            }
            return surname;
        } else if (surname.length() == 0) {
            return forename;
        } else if (surnameFirst) {
            return surname + " " + forename;
        } else {
            return forename + " " + surname;
        }
    }

    public default String toStandardStyle() {
        String nonlatin = getNonlatin();
        String latin = getLatin();
        if (nonlatin.length() == 0) {
            return latin;
        } else {
            if (latin.length() == 0) {
                return nonlatin;
            }
            return nonlatin + " (" + latin + ")";
        }
    }

    public default String toDirectoryStyle(boolean uppercase) {
        String surname = getSurname();
        String forename = getForename();
        String nonlatin = getNonlatin();
        StringBuilder buf = new StringBuilder();
        if (uppercase) {
            buf.append(surname.toUpperCase());
        } else {
            buf.append(surname);
        }
        if (forename.length() > 0) {
            buf.append(" ");
            buf.append(forename);
        }
        if (nonlatin.length() > 0) {
            buf.append(" (");
            buf.append(nonlatin);
            buf.append(")");
        }
        return buf.toString();
    }

    public default String toBiblioStyle(boolean uppercase) {
        String surname = getSurname();
        String forename = getForename();
        String nonlatin = getNonlatin();
        StringBuilder buf = new StringBuilder();
        if (uppercase) {
            buf.append(surname.toUpperCase());
        } else {
            buf.append(surname);
        }
        if (forename.length() > 0) {
            buf.append(", ");
            buf.append(forename);
        }
        if (nonlatin.length() > 0) {
            buf.append(" (");
            buf.append(nonlatin);
            buf.append(")");
        }
        return buf.toString();
    }

}
