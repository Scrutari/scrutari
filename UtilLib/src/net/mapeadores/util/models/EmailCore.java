/* UtilLib - Copyright (c) 2013-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.models;


/**
 *
 * @author Vincent Calame
 */
public interface EmailCore {

    /**
     * Jamais nul, doit respecter la forme name@domain.
     */
    public String getAddrSpec();

    /**
     * Jamais nul mais peut être de longueur nulle.
     */
    public String getRealName();

    public default String getComputedRealName() {
        return getRealName();
    }

    public default String toCompleteString() {
        return toCompleteString(false);
    }

    public default String toCompleteString(boolean computed) {
        String realName;
        if (computed) {
            realName = getComputedRealName();
        } else {
            realName = getRealName();
        }
        String addrSpec = getAddrSpec();
        if (realName.length() == 0) {
            return addrSpec;
        }
        StringBuilder buf = new StringBuilder();
        boolean needQuotes = false;
        boolean whitespace = false;
        for (int i = 0; i < realName.length(); i++) {
            char carac = realName.charAt(i);
            if (Character.isWhitespace(carac)) {
                whitespace = true;
            }
            if (Character.isISOControl(carac)) {
                continue;
            }
            if (whitespace) {
                whitespace = false;
                buf.append(' ');
            }
            if ((carac == '\"') || (carac == '\\')) {
                needQuotes = true;
                buf.append('\\');
            } else if ((carac == '>') || (carac == '<')) {
                needQuotes = true;
            }
            buf.append(carac);
        }
        if (needQuotes) {
            buf.insert(0, '"');
            buf.append('"');
        }
        buf.append(" <");
        buf.append(addrSpec);
        buf.append(">");
        return buf.toString();
    }

}
