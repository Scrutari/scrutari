/* UtilLib - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.script;

import javax.script.ScriptEngine;
import javax.script.ScriptException;


/**
 *
 * @author Vincent Calame
 */
public interface ScriptFamily {

    public boolean hasScriptFor(String actionName);

    public Object evalScriptFor(String actionName, ScriptEngine scriptEngine) throws ScriptException;

}
