/* UtilLib - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.json;

import java.io.IOException;


/**
 *
 * @author Vincent Calame
 */
public final class JsonUtils {

    public final static PropertyEligibility ALL_ELIGIBILITY = new BooleanPropertyEligibility(true);
    public final static PropertyEligibility NONE_ELIGIBILITY = new BooleanPropertyEligibility(false);
    public final static JsonProperty EMPTY_JSONPROPERTY = new EmptyJsonProperty();


    private JsonUtils() {

    }


    private static class BooleanPropertyEligibility implements PropertyEligibility {

        private final boolean bool;

        private BooleanPropertyEligibility(boolean bool) {
            this.bool = bool;
        }

        @Override
        public boolean includeProperty(String propertyName) {
            return bool;
        }

    }


    private static class EmptyJsonProperty implements JsonProperty {

        private EmptyJsonProperty() {

        }

        @Override
        public String getName() {
            return "";
        }

        @Override
        public void writeValue(JSONWriter jw) throws IOException {
            jw.value("");
        }

    }

}
