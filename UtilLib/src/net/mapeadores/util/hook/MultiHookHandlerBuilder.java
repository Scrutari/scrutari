/* UtilLib - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.hook;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class MultiHookHandlerBuilder {

    private List<HookHandler> handlerList = new ArrayList<HookHandler>();

    public MultiHookHandlerBuilder() {

    }

    public MultiHookHandlerBuilder addHookHandler(HookHandler hookHandler) {
        if (hookHandler != null) {
            handlerList.add(hookHandler);
        }
        return this;
    }

    public boolean isEmpty() {
        return handlerList.isEmpty();
    }

    public HookHandler toHookHandler() {
        return new MultiHookHandler(handlerList.toArray(new HookHandler[handlerList.size()]));
    }

    public static MultiHookHandlerBuilder init() {
        return new MultiHookHandlerBuilder();
    }


    private static class MultiHookHandler implements HookHandler {

        private final HookHandler[] array;

        private MultiHookHandler(HookHandler[] array) {
            this.array = array;
        }

        @Override
        public void handle(String hookName, Object... arguments) {
            for (HookHandler hookHandler : array) {
                hookHandler.handle(hookName, arguments);
            }
        }

    }


}
