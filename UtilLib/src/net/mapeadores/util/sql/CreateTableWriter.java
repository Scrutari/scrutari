/* UtilLib - Copyright (c) 2007-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.sql;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class CreateTableWriter {

    private final Writer writer;
    private final short mode;
    private final List<String> primaryKeyList = new ArrayList<String>();
    private boolean firstField = true;

    public CreateTableWriter(Writer writer, short mode) {
        this.writer = writer;
        this.mode = mode;
    }

    public void startTable(String tableName) throws IOException {
        firstField = true;
        primaryKeyList.clear();
        writer.write("CREATE TABLE");
        if (mode == SqlConstants.MYSQL_MODE) {
            writer.write(" IF NOT EXISTS");
        }
        writer.write(" ");
        writer.write(tableName);
        writer.write(" (");
    }

    public void addIntegerField(String fieldName, boolean isPrimary, String title) throws IOException {
        testFirstField();
        writer.write(fieldName);
        writer.write(" INTEGER");
        if (isPrimary) {
            primaryKeyList.add(fieldName);
        }
        writeTitle(title);
    }

    public void addDateField(String fieldName, boolean isPrimary, String title) throws IOException {
        testFirstField();
        writer.write(fieldName);
        writer.write(" DATE");
        if (isPrimary) {
            primaryKeyList.add(fieldName);
        }
        writeTitle(title);
    }

    public void addDoubleField(String fieldName, boolean isPrimary, String title) throws IOException {
        testFirstField();
        writer.write(fieldName);
        writer.write(" DOUBLE");
        if (isPrimary) {
            primaryKeyList.add(fieldName);
        }
        writeTitle(title);
    }

    public void addTextField(String fieldName, boolean isPrimary, String title) throws IOException {
        testFirstField();
        writer.write(fieldName);
        if (isPrimary) {
            writer.write(" VARCHAR(255)");
            primaryKeyList.add(fieldName);
        } else {
            if (mode == SqlConstants.MYSQL_MODE) {
                writer.write(" LONGTEXT");
            } else {
                writer.write(" TEXT");
            }
        }
        writeTitle(title);
    }

    private void writeTitle(String title) throws IOException {
        if (title == null) {
            return;
        }
        int length = title.length();
        writer.write(" COMMENT '");
        for (int i = 0; i < length; i++) {
            char carac = title.charAt(i);
            if (carac == '\'') {
                writer.write("''");
            } else {
                writer.write(carac);
            }
        }
        writer.write("''");
    }

    public void endTable(String createOptions) throws IOException {
        int size = primaryKeyList.size();
        if (size > 0) {
            writer.write(", PRIMARY KEY (");
            for (int i = 0; i < size; i++) {
                if (i > 0) {
                    writer.write(", ");
                }
                writer.write((String) primaryKeyList.get(i));
            }
            writer.write(")");
        }
        writer.write(")");
        if (createOptions != null) {
            writer.write(" ");
            writer.write(createOptions);
        }
    }

    private void testFirstField() throws IOException {
        if (firstField) {
            firstField = false;
        } else {
            writer.write(", ");
        }
    }

}
