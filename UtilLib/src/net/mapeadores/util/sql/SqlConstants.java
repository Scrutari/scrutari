/* UtilLib - Copyright (c) 2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.sql;


/**
 *
 * @author Vincent Calame
 */
public interface SqlConstants {

    public static final short STANDARD_MODE = 1;
    public static final short MYSQL_MODE = 2;
}
