/* UtilLib - Copyright (c) 2007-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.collation.map;

import java.text.RuleBasedCollator;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public class UnmodifiableCollatedKeyMap<E> implements CollatedKeyMap<E> {

    private final CollatedKeyMap<E> collatedKeyMap;

    public UnmodifiableCollatedKeyMap(CollatedKeyMap<E> collatedKeyMap) {
        this.collatedKeyMap = collatedKeyMap;
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    /**
     * Retourne l'instance de Collator utilisée pour la création des «
     * collatedKey » et la recherche.
     */
    @Override
    public RuleBasedCollator getCollator() {
        return collatedKeyMap.getCollator();
    }

    /**
     * Retourne l'objet Locale qui a servi à la construction de l'objet.
     */
    @Override
    public Locale getLocale() {
        return collatedKeyMap.getLocale();
    }

    /**
     * Effectue une recherche sur les clés de l'instance de CollatedKeyHashMap
     * et retourne le résultat sous la forme d'une instance de
     * SearchResultUnitList. Cette méthode vérifie la présence de searchText
     * dans les clés suivant trois conditions : TextConstants.CONTAINS,
     * TextConstants.ENDSWITH, TextConstants.STARTSWITH searchText subit
     * évidemment une « collation » pour que les conditions répondent aux règles
     * du Collator.
     */
    @Override
    public List<SearchResultUnit<E>> search(String searchText, int type, @Nullable ValueFilter valueFilter) {
        return collatedKeyMap.search(searchText, type, valueFilter);
    }

    @Override
    public List<SearchResultUnit<E>> searchNeighbours(String searchText, int type, @Nullable ValueFilter valueFilter, int distance) {
        return collatedKeyMap.searchNeighbours(searchText, type, valueFilter, distance);
    }

    /**
     * Retourne la valeur stockée à la clé « s ». La chaîne s subit d'abord une
     * « collation ». Cette méthode est équivalente à
     * getValueByCollatedKey(toCollateKey(key))
     */
    @Override
    public E getValue(String key) {
        return collatedKeyMap.getValue(key);
    }

    /**
     * Retourne la valeur stockée à la clé déjà « collatée », collatedKey.
     */
    @Override
    public E getValueByCollatedKey(String collatedKey) {
        return collatedKeyMap.getValueByCollatedKey(collatedKey);
    }

    @Override
    public void putValue(String key, E value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putValueByCollatedKey(String collatedKey, E value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E removeValue(String key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E removeValueByCollatedKey(String collatedKey) {
        throw new UnsupportedOperationException();
    }

    /**
     * Retourne le nombre de valeurs stockées dans l'instance de
     * CollatedKeyHashMap.
     */
    @Override
    public int size() {
        return collatedKeyMap.size();
    }

    @Override
    public Collection<E> values() {
        return Collections.unmodifiableCollection(collatedKeyMap.values());
    }

}
