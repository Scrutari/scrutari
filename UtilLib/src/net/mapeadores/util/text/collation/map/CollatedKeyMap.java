/* UtilLib - Copyright (c) 2007-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.collation.map;

import java.text.RuleBasedCollator;
import java.util.Collection;
import java.util.List;
import java.util.Locale;


/**
 *
 * @author Vincent Calame
 */
public interface CollatedKeyMap<E> {

    public void clear();

    /**
     * Retourne l'instance de Collator utilisée pour la création des «
     * collatedKey » et la recherche.
     */
    public RuleBasedCollator getCollator();

    /**
     * Retourne l'objet Locale qui a servi à la construction de l'objet.
     */
    public Locale getLocale();

    /**
     * Effectue une recherche sur les clés de l'instance de CollatedKeyHashMap
     * et retourne le résultat sous la forme d'une instance de
     * SearchResultUnitList. Cette méthode vérifie la présence de searchText
     * dans les clés suivant trois conditions : TextConstants.CONTAINS,
     * TextConstants.ENDSWITH, TextConstants.STARTSWITH searchText subit
     * évidemment une « collation » pour que les conditions répondent aux règles
     * du Collator.
     */
    public List<SearchResultUnit<E>> search(String searchText, int type, ValueFilter valueFilter);

    public List<SearchResultUnit<E>> searchNeighbours(String searchText, int type, ValueFilter valueFilter, int distance);

    /**
     * Retourne la valeur stockée à la clé « s ». La chaîne s subit d'abord une
     * « collation ». Cette méthode est équivalente à
     * getValueByCollatedKey(toCollateKey(key))
     */
    public E getValue(String key);

    /**
     * Retourne la valeur stockée à la clé déjà « collatée », collatedKey.
     */
    public E getValueByCollatedKey(String collatedKey);

    /**
     * Stocke la valeur à la clé Key. Équivalent de
     * putValueByCollatedKey(toCollatedKey(key),value).
     */
    public void putValue(String key, E value);

    /**
     * Stocke la valeur à la clé déjà « collatée » collatedKey.
     */
    public void putValueByCollatedKey(String collatedKey, E value);

    /**
     * Supprime la valeur référencée à la clé key.
     */
    public E removeValue(String key);

    /**
     * Supprime la valeur à la clé déjà « collatée » collatedKey.
     */
    public E removeValueByCollatedKey(String collatedKey);

    /**
     * Retourne le nombre de valeurs stockées dans l'instance de CollatedKeyMap.
     */
    public int size();

    public Collection<E> values();

}
