/* UtilLib - Copyright (c) 2007-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.collation.map;

import net.mapeadores.util.text.SubstringPosition;


/**
 *
 * @author Vincent Calame
 */
public interface SearchResultUnit<E> {

    public E getValue();

    public String getCollatedKey();

    public SubstringPosition getSearchTextPositionInCollatedKey();

}
