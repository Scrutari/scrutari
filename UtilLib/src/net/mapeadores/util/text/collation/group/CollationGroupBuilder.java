/* UtilLib - Copyright (c) 2007-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.collation.group;

import java.text.Collator;
import java.text.RuleBasedCollator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.mapeadores.util.text.collation.CollationUnit;


/**
 *
 * @author Vincent Calame
 */
public class CollationGroupBuilder {

    private final RuleBasedCollator collator;
    private final List<DefaultCollationGroup> groupList = new ArrayList<DefaultCollationGroup>();
    private final Map<Character, DefaultCollationGroup> groupMap = new HashMap<Character, DefaultCollationGroup>();

    public CollationGroupBuilder(Locale locale) {
        collator = (RuleBasedCollator) Collator.getInstance(locale);
        collator.setStrength(Collator.PRIMARY);
        for (char c = '0'; c <= '9'; c++) {
            addInitiale(c);
        }
        groupList.add(new DefaultCollationGroup(CollationGroup.NOLETTER_INITIAL));
        for (char c = 'A'; c <= 'Z'; c++) {
            addInitiale(c);
        }
        groupList.add(new DefaultCollationGroup(CollationGroup.NOEUROPEAN_INITIAL));
    }

    private void addInitiale(char c) {
        DefaultCollationGroup collationGroup = new DefaultCollationGroup(c);
        char[] array = {c};
        String collatedKey = CollationUnit.collate(new String(array), collator);
        if (collatedKey.length() > 1) {
            throw new IllegalStateException("should not occur : initiale = " + c);
        }
        groupMap.put(collatedKey.charAt(0), collationGroup);
        groupList.add(collationGroup);
    }

    public void addObjectWrapper(ObjectWrapper objectWrapper) {
        String string = objectWrapper.getString();
        if (string.length() == 0) {
            groupList.get(10).addObject(CollationUnit.collate(objectWrapper.getSourceString(), collator), objectWrapper);
        } else {
            String collatedKey = CollationUnit.collate(objectWrapper.getString(), collator);
            Character initial = collatedKey.charAt(0);
            DefaultCollationGroup collationGroup = (DefaultCollationGroup) groupMap.get(initial);
            if (collationGroup == null) {
                collationGroup = groupList.get(37);
            }
            collationGroup.addObject(collatedKey, objectWrapper);
        }
    }

    /**
     * implémente RandomAccess
     */
    public List<CollationGroup> getCollationGroupList() {
        List<CollationGroup> result = new ArrayList<CollationGroup>();
        int size = groupList.size();
        for (int i = 0; i < size; i++) {
            DefaultCollationGroup collationGroup = groupList.get(i);
            if (!collationGroup.isEmpty()) {
                result.add(collationGroup);
            }
        }
        return result;
    }

}
