/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.collation.group;

import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public interface CollationGroup {

    public static char NOLETTER_INITIAL = '?';
    public static char NOEUROPEAN_INITIAL = '_';

    public char getInitial();

    /**
     * Implémente RandomAccess.
     */
    public List<ObjectWrapper> getSortedObjectWrapperList();

}
