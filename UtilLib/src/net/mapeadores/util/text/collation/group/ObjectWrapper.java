/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.collation.group;

import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class ObjectWrapper {

    private String string;
    private Object object;
    private String sourceString;

    private ObjectWrapper(String string, String sourceString, Object object) {
        this.string = string;
        this.object = object;
        this.sourceString = sourceString;
    }

    public String getString() {
        return string;
    }

    public Object getObject() {
        return object;
    }

    public String getSourceString() {
        return sourceString;
    }

    public static ObjectWrapper newCleanedInstance(String sourceString, Object object) {
        String string = "";
        if (sourceString != null) {
            string = StringUtils.reduceToLetterAndDigit(sourceString);
        } else {
            sourceString = "";
        }
        return new ObjectWrapper(string, sourceString, object);
    }

}
