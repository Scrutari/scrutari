/* UtilLib - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.io.Serializable;
import java.text.ParseException;


/**
 *
 * @author Vincent Calame
 */
public final class FileName implements Serializable {

    private static final long serialVersionUID = 1L;
    private final String basename;
    private final String extension;

    private FileName(String basename, String extension) {
        this.basename = basename;
        this.extension = extension;
    }

    /**
     * Jamais nul ou de longueur nulle
     */
    public String getBasename() {
        return basename;
    }

    /**
     * Jamais nul ou de longueur nulle
     */
    public String getExtension() {
        return extension;
    }

    @Override
    public String toString() {
        return basename + "." + extension;
    }

    public static FileName parse(String s) throws ParseException {
        if (s == null) {
            throw new ParseException("String null", 0);
        }
        int separatorIdx = s.indexOf('/');
        if (separatorIdx != -1) {
            throw new ParseException("Containing /", separatorIdx);
        }
        int length = s.length();
        if (length < 3) {
            throw new ParseException("length < 3", 2);
        }
        int idx = s.lastIndexOf('.');
        if (idx < 1) {
            throw new ParseException("Missing dot", 0);
        }
        if (idx == (length - 1)) {
            throw new ParseException("Dot at last", (length - 1));
        }
        return new FileName(s.substring(0, idx), s.substring(idx + 1));
    }

    public static FileName build(String s) {
        try {
            return parse(s);
        } catch (ParseException pe) {
            throw new IllegalArgumentException(pe.getMessage());
        }
    }

    public static FileName parse(String s, String defaultExtension) throws ParseException {
        if (s == null) {
            throw new ParseException("String null", 0);
        }
        int length = s.length();
        if (length == 0) {
            throw new ParseException("Empty", 0);
        }
        int separatorIdx = s.indexOf('/');
        if (separatorIdx != -1) {
            throw new ParseException("Containing /", separatorIdx);
        }
        int idx = s.lastIndexOf('.');
        if (idx == -1) {
            return new FileName(s, defaultExtension);
        } else if (idx == 0) {
            if (length == 1) {
                throw new ParseException("Dot at last", 1);
            }
            return new FileName(s.substring(1), defaultExtension);
        } else if (idx == (length - 1)) {
            return new FileName(s.substring(0, length - 1), defaultExtension);
        } else {
            return new FileName(s.substring(0, idx), s.substring(idx + 1));
        }
    }

    public static String getExtension(String fileName) {
        int idx = fileName.lastIndexOf('.');
        if (idx < 1) {
            return "";
        } else if (idx == (fileName.length() - 1)) {
            return "";
        } else {
            return fileName.substring(idx + 1);
        }
    }

}
