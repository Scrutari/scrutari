/* UtilLib - Copyright (c) 2007-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.Locale;


/**
 *
 * @author Vincent Calame
 */
public class QuoteOptions {

    private char openingQuote = '«';
    private char closingQuote = '»';
    private boolean withNonBreakingSpaces = true;
    public final static QuoteOptions FRENCH_STYLE = new QuoteOptions();
    public final static QuoteOptions ENGLISH_STYLE = new QuoteOptions('\u201C', '\u201D');
    public final static QuoteOptions SPANISH_STYLE = new QuoteOptions('«', '»');
    public final static QuoteOptions GERMAN_STYLE = new QuoteOptions('»', '«');

    private QuoteOptions() {
    }

    private QuoteOptions(char openQuote, char closeQuote) {
        this.openingQuote = openQuote;
        this.closingQuote = closeQuote;
        this.withNonBreakingSpaces = false;
    }

    public QuoteOptions(char openQuote, char closeQuote, boolean withNonBreakingSpaces) {
        this.openingQuote = openQuote;
        this.closingQuote = closeQuote;
        this.withNonBreakingSpaces = withNonBreakingSpaces;
    }

    public char getOpeningQuote() {
        return openingQuote;
    }

    public char getClosingQuote() {
        return closingQuote;
    }

    public boolean isWithNonBreakingSpaces() {
        return withNonBreakingSpaces;
    }

    public static QuoteOptions getQuoteOptions(Locale locale) {
        String lang = locale.getLanguage();
        String country = locale.getCountry();
        if (country.equals("CH")) {
            return SPANISH_STYLE;
        }
        if (lang.equals("fr")) {
            return FRENCH_STYLE;
        } else if (lang.equals("de")) {
            return GERMAN_STYLE;
        } else if (lang.equals("es")) {
            return SPANISH_STYLE;
        } else if (lang.equals("en")) {
            return ENGLISH_STYLE;
        } else if (lang.equals("pt")) {
            return ENGLISH_STYLE;
        }
        return FRENCH_STYLE;
    }

}
