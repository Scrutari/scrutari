/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;


/**
 *
 * @author Vincent Calame
 */
public interface AccoladeArgumentFactory {

    /**
     * name et mode doivent respecter les conventions (name de longueur non nulle, mode null ou de longueur non nulle).
     * Peut retourner null pour indiquer que l'argument n'a pas pu être traité.
     */
    public AccoladeArgument getAccoladeArgument(String name, String mode);

}
