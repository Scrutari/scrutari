/* UtilLib - Copyright (c) 2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.LinkedHashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class LabelsCache {

    private final Map<Lang, Label> labelMap = new LinkedHashMap<Lang, Label>();
    private Labels cachedLabels;

    public LabelsCache() {

    }

    public synchronized void clear() {
        labelMap.clear();
        cachedLabels = null;
    }

    public synchronized void putLabels(Labels labels) {
        for (Label label : labels) {
            labelMap.put(label.getLang(), label);
        }
        cachedLabels = null;
    }

    public synchronized boolean putLabel(Label label) {
        Lang lang = label.getLang();
        Label current = labelMap.get(lang);
        if (current != null) {
            if (LabelUtils.areEquals(current, label)) {
                return false;
            }
        }
        labelMap.put(lang, label);
        cachedLabels = null;
        return true;
    }

    public synchronized boolean removeLabel(Lang lang) {
        Label label = labelMap.remove(lang);
        if (label != null) {
            cachedLabels = null;
            return true;
        } else {
            return false;
        }
    }

    public Labels getLabels() {
        Labels result = cachedLabels;
        if (result == null) {
            result = cache();
        }
        return result;
    }

    private synchronized Labels cache() {
        Labels cache = LabelUtils.toLabels(labelMap);
        cachedLabels = cache;
        return cache;
    }

}
