/* UtilLib - Copyright (c) 2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.search;


/**
 *
 * @author Vincent Calame
 */
public interface SimpleSearchToken extends SearchToken {

    public int getSearchType();

    public String getTokenString();

}
