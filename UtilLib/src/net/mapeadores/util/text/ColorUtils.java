/* UtilLib - Copyright (c) 2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.awt.Color;


/**
 *
 * @author Vincent Calame
 */
public class ColorUtils {

    private ColorUtils() {
    }

    /**
     * Retourne l'expression de la couleur en argument sous la forme de la chaîne utlisée en HTML #00FF00.
     **/
    public static String toHexFormat(Color color) {
        StringBuilder buf = new StringBuilder();
        buf.append('#');
        appendHexString(color.getRed(), buf);
        appendHexString(color.getGreen(), buf);
        appendHexString(color.getBlue(), buf);
        return buf.toString();
    }

    /*
     * Méthode privée utilisée par toHexFormat pour traduire un entier en un hexadécimal de deux lettres.
     */
    private static void appendHexString(int codecouleur, StringBuilder buf) {
        String hex = Integer.toHexString(codecouleur);
        if (hex.length() == 1) {
            buf.append('0');
        }
        buf.append(hex);
    }

}
