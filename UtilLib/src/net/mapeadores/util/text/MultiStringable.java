/* UtilLib - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;


/**
 *
 * @author Vincent Calame
 */
public interface MultiStringable {

    public String[] toStringArray();

    public default String toString(String separator) {
        StringBuilder buf = new StringBuilder();
        for (String token : toStringArray()) {
            if (buf.length() > 0) {
                buf.append(separator);
            }
            buf.append(token);
        }
        return buf.toString();
    }

    public default int getStringSize() {
        return toStringArray().length;
    }

    public default String getStringValue(int index) {
        String[] array = toStringArray();
        return array[index];
    }

}
