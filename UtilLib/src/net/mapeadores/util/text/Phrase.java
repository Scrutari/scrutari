/* UtilLib - Copyright (c) 2017-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;


/**
 *
 * @author Vincent Calame
 */
public interface Phrase extends Labels {

    /**
     * N'est jamais nul.
     *
     * @return
     */
    public String getName();

}
