/* UtilLib - Copyright (c) 2006 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.resources;

import java.util.ListResourceBundle;


/**
 * Utilisé dans DateResourceBundle
 *
 * @author Vincent Calame
 */
public class DateResourceBundle extends ListResourceBundle {

    public Object[][] getContents() {
        return contents;
    }

    static final Object[][] contents = {
        // LOCALIZE THIS
        {"s1", "S1"}, //
        {"s2", "S2"},
        {"t1", "T1"},
        {"t2", "T2"},
        {"t3", "T3"},
        {"t4", "T4"}
    // END OF MATERIAL TO LOCALIZE
    };
}
