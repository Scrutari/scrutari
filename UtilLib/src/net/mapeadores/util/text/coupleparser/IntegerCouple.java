/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.coupleparser;


/**
 *
 * @author Vincent Calame
 */
public class IntegerCouple extends Couple {

    private int integer;

    public IntegerCouple(int integer, int lineNumber, String value) {
        super(lineNumber, value);
        this.integer = integer;
    }

    public int getInteger() {
        return integer;
    }

}
