/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.coupleparser;


/**
 *
 * @author Vincent Calame
 */
public class IdalphaCouple extends Couple {

    private String idalpha;

    public IdalphaCouple(String idalpha, int lineNumber, String value) {
        super(lineNumber, value);
        this.idalpha = idalpha;
    }

    public String getIdalpha() {
        return idalpha;
    }

}
