/* UtilLib - Copyright (c) 2007 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.lexie;


/**
 *
 * @author Vincent Calame
 */
public class LexieSource {

    private Object sourceObject;
    private String sourceText;
    private String sourceId;

    public LexieSource(String sourceText, Object sourceObject, String sourceId) {
        this.sourceText = sourceText;
        this.sourceObject = sourceObject;
        this.sourceId = sourceId;
    }

    public Object getSourceObject() {
        return sourceObject;
    }

    public String getSourceText() {
        return sourceText;
    }

    public String getSourceId() {
        return sourceId;
    }

}
