/* UtilLib - Copyright (c) 2008-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.lexie;


/**
 * 
 *
 * @author Vincent Calame
 */
public interface TextLexieUnit extends LexieUnit {

    /**
     * Retourne un identifiant de la lexie.
     */
    public String getLexieId();

    public int getOccurrenceCount();

    public Occurrence getOccurrence(int index);

}
