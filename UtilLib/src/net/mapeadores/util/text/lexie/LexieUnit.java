/* UtilLib - Copyright (c) 2008 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.lexie;


/**
 *
 * @author Vincent Calame
 */
public interface LexieUnit {

    /**
     * Retourne la lexie sous forme « collatée », c'est à dire sous la forme
     * permettant le tri et la recherche et illisible pour un humain
     */
    public String getCollatedLexie();

    /**
     * Retourne la forme « lisible » pour un humain, essaie de prendre la forme
     * la plus pertinente (exemple ; manœuvre plutôt que manoeuvre)
     */
    public String getCanonicalLexie();

}
