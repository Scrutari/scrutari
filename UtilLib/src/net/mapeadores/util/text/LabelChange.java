/* UtilLib - Copyright (c) 2011-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.List;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface LabelChange {

    public Labels getChangedLabels();

    public List<Lang> getRemovedLangList();

    public default boolean isEmpty() {
        return ((getChangedLabels().isEmpty()) && (getRemovedLangList().isEmpty()));
    }

}
