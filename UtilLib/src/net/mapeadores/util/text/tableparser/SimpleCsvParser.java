/* UtilLib - Copyright (c) 2011-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text.tableparser;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class SimpleCsvParser implements CsvParser {

    public SimpleCsvParser() {
    }

    @Override
    public String[][] parse(String s, CsvParameters csvParameters) {
        InternalLineHandler lineHandler = new InternalLineHandler();
        RowParser lineParser = new RowParser(lineHandler, csvParameters);
        try {
            StringReader stringReader = new StringReader(s);
            lineParser.parse(stringReader);
        } catch (IOException ioe) {
        }
        return lineHandler.toArray();
    }


    private static class InternalLineHandler implements RowHandler {

        private final List<String[]> lineList = new ArrayList<String[]>();

        private InternalLineHandler() {
        }

        @Override
        public void addRow(String[] line) {
            lineList.add(line);
        }

        public String[][] toArray() {
            return lineList.toArray(new String[lineList.size()][]);
        }

    }

}
