/* UtilLib - Copyright (c) -2009-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;


/**
 *
 * @author Vincent Calame
 */
public class SeparatorOptions {

    private char separator = ';';
    private boolean lastInclude = false;
    private boolean spaceInclude = false;

    public SeparatorOptions() {
    }

    public SeparatorOptions(char separator, boolean lastInclude, boolean spaceInclude) {
        this.separator = separator;
        this.lastInclude = lastInclude;
        this.spaceInclude = spaceInclude;
    }

    public char getSeparator() {
        return separator;
    }

    public boolean isLastInclude() {
        return lastInclude;
    }

    public boolean isSpaceInclude() {
        return spaceInclude;
    }

}
