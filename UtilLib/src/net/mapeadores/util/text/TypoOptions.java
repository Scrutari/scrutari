/* BdfServer - Copyright (c) 2007-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.util.Locale;


/**
 *
 * @author Vincent Calame
 */
public class TypoOptions {

    private QuoteOptions quoteOptions;

    public TypoOptions() {
    }

    public void setQuoteOptions(QuoteOptions quoteOptions) {
        this.quoteOptions = quoteOptions;
    }

    public QuoteOptions getQuoteOptions() {
        return quoteOptions;
    }

    public static TypoOptions getTypoOptions(Locale locale) {
        TypoOptions typoOptions = new TypoOptions();
        QuoteOptions quoteOptions = QuoteOptions.getQuoteOptions(locale);
        typoOptions.setQuoteOptions(quoteOptions);
        return typoOptions;
    }

}
