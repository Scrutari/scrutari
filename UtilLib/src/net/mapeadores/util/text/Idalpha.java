/* UtilLib - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import java.text.ParseException;


/**
 *
 * @author Vincent Calame
 */
public final class Idalpha {

    private Idalpha() {
    }

    public static boolean isSignificant(String idalpha) {
        if (idalpha == null) {
            return false;
        }
        if (idalpha.isEmpty()) {
            return false;
        }
        if (idalpha.startsWith("_")) {
            return false;
        }
        return true;
    }

    /**
     *
     * @throws IllegalArgumentException si idalpha est nul ou de longueur nulle
     * @throws ParseException si idalpha possède des caractères incorrects.
     */
    public static void test(String idalpha) throws ParseException {
        if (idalpha == null) {
            throw new IllegalArgumentException("idalpha is null");
        }
        if (idalpha.length() == 0) {
            throw new IllegalArgumentException("idalpha is empty");
        }
        int length = idalpha.length();
        for (int i = 0; i < length; i++) {
            char carac = idalpha.charAt(i);
            if (carac == ' ') {
                if (i == 0) {
                    throw new ParseException("starting with space : " + idalpha, i);
                }
                if (i == length - 1) {
                    throw new ParseException("idalpha ending with space : " + idalpha, i);
                }
                if (idalpha.charAt(i - 1) == ' ') {
                    throw new ParseException("two following spaces (idalpha : " + idalpha, i);
                }
            } else if (!isValidNoSpaceChar(carac)) {
                throw new ParseException("Wrong character = " + carac + " (idalpha=" + idalpha + ")", i);
            }
        }
    }

    /**
     * Équivalent de testIdalpha mais retourne un booléen plutôt que des
     * exceptions.
     */
    public static boolean isValid(String idalpha) {
        try {
            test(idalpha);
            return true;
        } catch (ParseException | IllegalArgumentException e) {
            return false;
        }
    }

    public static boolean isValidNoSpaceChar(char carac) {
        if ((carac >= 'a') && (carac <= 'z')) {
            return true;
        }
        if ((carac >= 'A') && (carac <= 'Z')) {
            return true;
        }
        if ((carac >= '0') && (carac <= '9')) {
            return true;
        }
        switch (carac) {
            case '[':
            case ']':
            case '(':
            case ')':
            case '_':
            case '-':
            case '.':
            case '/':
            case '@':
            case ':':
            case '~':
                return true;
            default:
                return false;
        }
    }

}
