/* UtilLib - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.text;

import net.mapeadores.util.annotation.Nullable;


/**
 *
 * @author Vincent Calame
 */
public final class CleanedString implements CharSequence {

    private final String string;

    private CleanedString(String string) {
        this.string = string;
    }

    @Override
    public char charAt(int index) {
        return string.charAt(index);
    }

    @Override
    public int length() {
        return string.length();
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return string.subSequence(start, end);
    }

    @Override
    public String toString() {
        return string;
    }

    @Override
    public int hashCode() {
        return string.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        CleanedString otherCleanedString = (CleanedString) other;
        return otherCleanedString.string.equals(this.string);
    }

    @Nullable
    public static CleanedString newInstance(@Nullable CharSequence cs) {
        if (cs == null) {
            return null;
        }
        if (cs instanceof CleanedString) {
            return (CleanedString) cs;
        }
        String s = StringUtils.cleanString(cs);
        if (s.length() == 0) {
            return null;
        } else if (s.equals(cs)) {
            return new CleanedString((String) cs);
        } else {
            return new CleanedString(s);
        }
    }

}
