/* UtilLib - Copyright (c) 2006-2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.css;

import java.io.IOException;
import java.io.Writer;
import java.awt.Color;
import net.mapeadores.util.text.ColorUtils;


/**
 *
 * @author Vincent Calame
 */
public class CssWriter {

    Writer writer;
    boolean onSelector = false;

    public CssWriter(Writer writer) {
        this.writer = writer;
    }

    public void startSelector(String selector) throws IOException {
        closeCurrent();
        onSelector = true;
        writer.write(selector);
        writer.write("  {\n");
    }

    public void startIdSelector(String id) throws IOException {
        startSelector("#" + id);
    }

    public void startClassSelector(String className) throws IOException {
        startSelector("." + className);
    }

    public void startTagClassSelector(String tagName, String className) throws IOException {
        startSelector(tagName + "." + className);
    }

    public void addProperty(String propertyName, String propertyValue) throws IOException {
        writer.write("\t");
        writer.write(propertyName);
        writer.write(": ");
        writer.write(propertyValue);
        writer.write(";\n");
    }

    public void addProperty(String propertyName, Color color) throws IOException {
        if (color == null) {
            addProperty(propertyName, "none");
        } else {
            addProperty(propertyName, ColorUtils.toHexFormat(color));
        }
    }

    public void flush() throws IOException {
        closeCurrent();
        writer.flush();
    }

    private void closeCurrent() throws IOException {
        if (onSelector) {
            writer.write("}\n\n");
            onSelector = false;
        }
    }

}
