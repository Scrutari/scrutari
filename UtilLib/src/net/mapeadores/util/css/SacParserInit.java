/* UtilLib - Copyright (c) 2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.css;


/**
 *
 * @author Vincent Calame
 */
public class SacParserInit {

    public final static String BATIK = "org.apache.batik.css.parser.Parser";
    public final static String SACPARSER = "com.steadystate.css.parser.SACParserCSS2";
    public final static String FLUTE = "org.w3c.flute.parser.Parser";
    public final static String UTILLIB = "net.mapeadores.util.css.parser.Parser";

    private SacParserInit() {
    }

    public static void init() {
        String current = System.getProperty("org.w3c.css.sac.parser");
        if (current == null) {
            init(UTILLIB);
        }
    }

    public static void init(String parserClassName) {
        System.setProperty("org.w3c.css.sac.parser", parserClassName);
    }

}
