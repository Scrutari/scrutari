/* UtilLib - Copyright (c) 2012 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.request;


/**
 *
 * @author Vincent Calame
 */
public interface JsonType {

    public final static short JSON_TYPE = 1;
    public final static short JSON_P_TYPE = 2;
    public final static short JSON_IN_HTML_TYPE = 3;
    public final static String JSON_STRING = "json";
    public final static String JSON_P_STRING = "jsonp";
    public final static String JSON_IN_HTML_STRING = "jsonh";
}
