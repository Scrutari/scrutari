/* UtilLib - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public class TableIteratorRequestMap implements RequestMap {

    private final ValueBuffer[] valueBufferArray;
    private final Map<String, ValueBuffer> valueBufferMap = new HashMap<String, ValueBuffer>();

    public TableIteratorRequestMap(String[] columnNameArray) {
        int length = columnNameArray.length;
        valueBufferArray = new ValueBuffer[length];
        for (int i = 0; i < length; i++) {
            String name = columnNameArray[i];
            ValueBuffer valuesBuffer = valueBufferMap.get(name);
            if (valuesBuffer == null) {
                valuesBuffer = new ValueBuffer();
                valueBufferMap.put(name, valuesBuffer);
            }
            valueBufferArray[i] = valuesBuffer;
        }
    }

    @Override
    public FileValue getFileValue(String name) {
        return null;
    }

    @Override
    public FileValue[] getFileValues(String name) {
        return null;
    }

    @Override
    public String getParameter(String name) {
        ValueBuffer valueBuffer = valueBufferMap.get(name);
        if (valueBuffer == null) {
            return null;
        }
        return valueBuffer.getValue();
    }

    @Override
    public String[] getParameterValues(String name) {
        ValueBuffer valueBuffer = valueBufferMap.get(name);
        if (valueBuffer == null) {
            return null;
        }
        return valueBuffer.getValueArray();
    }

    @Override
    public Set<String> getParameterNameSet() {
        return valueBufferMap.keySet();
    }

    public void updateRow(String[] valueArray) {
        clear();
        int length = Math.min(valueArray.length, valueBufferArray.length);
        for (int i = 0; i < length; i++) {
            valueBufferArray[i].add(valueArray[i]);
        }
    }

    public void clear() {
        for (ValueBuffer valueBuffer : valueBufferMap.values()) {
            valueBuffer.clear();
        }
    }

    @Override
    public Locale[] getAcceptableLocaleArray() {
        return null;
    }

    @Override
    public Object getSourceObject() {
        return null;
    }


    private static class ValueBuffer {

        private List<String> values = new ArrayList<String>();

        private ValueBuffer() {
        }

        private void clear() {
            values.clear();
        }

        private void add(String value) {
            values.add(value);
        }

        private String getValue() {
            if (values.isEmpty()) {
                return "";
            } else {
                return values.get(0);
            }
        }

        private String[] getValueArray() {
            int size = values.size();
            if (size == 0) {
                String[] result = new String[1];
                result[0] = "";
                return result;
            } else {
                String[] result = new String[size];
                for (int i = 0; i < size; i++) {
                    result[i] = values.get(i);
                }
                return result;
            }
        }

    }

}
