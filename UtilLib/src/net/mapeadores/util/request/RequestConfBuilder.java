/* UtilLib - Copyright (c) 2012-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.request;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.attr.AttributesBuilder;


/**
 *
 * @author Vincent Calame
 */
public class RequestConfBuilder {

    private final Map<String, InternalSupplementaryParameter> map = new LinkedHashMap<String, InternalSupplementaryParameter>();
    private final AttributesBuilder attributesBuilder = new AttributesBuilder();

    public RequestConfBuilder() {

    }

    public RequestConfBuilder addParameter(String name, Collection<String> values) {
        if (values.isEmpty()) {
            return this;
        }
        InternalSupplementaryParameter supplementaryParameter = map.get(name);
        if (supplementaryParameter == null) {
            supplementaryParameter = new InternalSupplementaryParameter(name);
            map.put(name, supplementaryParameter);
        }
        supplementaryParameter.add(values);
        return this;
    }

    public AttributesBuilder getAttributesBuilder() {
        return attributesBuilder;
    }

    public RequestConf toRequestConf() {
        List<SupplementaryParameter> supplementaryParameterList = RequestUtils.wrap(map.values().toArray(new SupplementaryParameter[map.size()]));
        return new InternalRequestConf(supplementaryParameterList, attributesBuilder.toAttributes());
    }

    public static RequestConfBuilder init() {
        return new RequestConfBuilder();
    }


    private static class InternalRequestConf implements RequestConf {

        private final List<SupplementaryParameter> supplementaryParameterList;
        private final Attributes attributes;

        private InternalRequestConf(List<SupplementaryParameter> supplementaryParameterList, Attributes attributes) {
            this.supplementaryParameterList = supplementaryParameterList;
            this.attributes = attributes;
        }

        @Override
        public List<SupplementaryParameter> getSupplementaryParameterList() {
            return supplementaryParameterList;
        }

        @Override
        public Attributes getAttributes() {
            return attributes;
        }

    }


    private static class InternalSupplementaryParameter implements SupplementaryParameter {

        private final String name;
        private final boolean append = false;
        private final List<String> valueList = new ArrayList<String>();

        private InternalSupplementaryParameter(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }


        @Override
        public boolean appendToExisting() {
            return append;
        }

        @Override
        public boolean isArray() {
            return (valueList.size() == 1);
        }

        @Override
        public String getValue() {
            return valueList.get(0);
        }

        @Override
        public String[] getValueArray() {
            return valueList.toArray(new String[valueList.size()]);
        }

        private void add(Collection<String> values) {
            valueList.addAll(values);
        }

    }

}
