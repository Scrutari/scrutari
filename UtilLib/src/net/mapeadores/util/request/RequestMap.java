/* UtilLib - Copyright (c) 2006-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.request;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public interface RequestMap {

    @Nullable
    public FileValue getFileValue(String name);

    @Nullable
    public FileValue[] getFileValues(String name);

    @Nullable
    public String getParameter(String name);

    @Nullable
    public String[] getParameterValues(String name);

    public Set<String> getParameterNameSet();

    /**
     * Peut retourner null si l'information n'est pas disponible
     */
    @Nullable
    public Locale[] getAcceptableLocaleArray();

    @Nullable
    public Object getSourceObject();

    public default boolean isTrue(String name) {
        return StringUtils.isTrue(getParameter(name));
    }

    public default String getTrimedParameter(String name) {
        String value = getParameter(name);
        if (value == null) {
            return "";
        }
        return value.trim();
    }

    @Nullable
    public default String[] getParameterTokens(String name, boolean includeSpaceSeparator) {
        String[] values = getParameterValues(name);
        if (values == null) {
            return null;
        }
        if (values.length == 1) {
            return StringUtils.getTechnicalTokens(values[0], includeSpaceSeparator);
        }
        List<String> tokenList = new ArrayList<String>();
        for (String value : values) {
            String[] tokens = StringUtils.getTechnicalTokens(value, includeSpaceSeparator);
            for (String token : tokens) {
                tokenList.add(token);
            }
        }
        return tokenList.toArray(new String[tokenList.size()]);
    }


}
