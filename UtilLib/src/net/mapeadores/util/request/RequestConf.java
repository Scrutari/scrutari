/* UtilLib - Copyright (c) 2012-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.request;

import java.util.List;
import net.mapeadores.util.attr.Attributes;


/**
 *
 * @author Vincent Calame
 */
public interface RequestConf {

    public List<SupplementaryParameter> getSupplementaryParameterList();

    public Attributes getAttributes();

}
