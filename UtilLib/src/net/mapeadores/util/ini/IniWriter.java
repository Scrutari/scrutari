/* UtilLib - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.ini;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public final class IniWriter {

    private IniWriter() {

    }

    public static String mapToString(Map<String, String> map) {
        StringBuilder buf = new StringBuilder();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            buf.append(entry.getKey());
            buf.append('=');
            buf.append(entry.getValue());
            buf.append('\n');
        }
        return buf.toString();
    }

    public static void writeMap(OutputStream outputStream, Map<String, String> map) throws IOException {
        BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
        for (Map.Entry<String, String> entry : map.entrySet()) {
            buf.append(entry.getKey());
            buf.append('=');
            buf.append(entry.getValue());
            buf.newLine();
        }
    }

}
