/* OdLib_Io - Copyright (c) 2013-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.money.ExtendedCurrency;


/**
 *
 * @author Vincent Calame
 */
public final class OdCellKey {

    private final short styleFamily;
    private final ExtendedCurrency currency;
    private final String styleName;

    private OdCellKey(short styleFamily, ExtendedCurrency currency, String styleName) {
        this.styleFamily = styleFamily;
        this.currency = currency;
        this.styleName = styleName;
    }

    public short getStyleFamliy() {
        return styleFamily;
    }

    @Nullable
    public ExtendedCurrency getCurrency() {
        return currency;
    }

    @Nullable
    public String getStyleName() {
        return styleName;
    }

    @Override
    public int hashCode() {
        int code = styleFamily;
        if (currency != null) {
            code += currency.hashCode();
        }
        if (styleName != null) {
            code += styleName.hashCode();
        }
        return code;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        OdCellKey odCellKey = (OdCellKey) other;
        if (odCellKey.styleFamily != this.styleFamily) {
            return false;
        }
        if (odCellKey.currency == null) {
            if (this.currency != null) {
                return false;
            }
        } else if (this.currency == null) {
            return false;
        } else if (!odCellKey.currency.equals(this.currency)) {
            return false;
        }
        if (odCellKey.styleName == null) {
            if (this.styleName == null) {
                return true;
            } else {
                return false;
            }
        } else if (this.styleName == null) {
            return false;
        } else {
            return odCellKey.styleName.equals(this.styleName);
        }
    }

    public static OdCellKey newInstance(short styleFamily, String styleName) {
        if (styleFamily == OdColumnDef.CURRENCY_STYLE_FAMILY) {
            throw new IllegalArgumentException("Not use with styleFamily = OdColumnDef.CURRENCY_STYLE_FAMILY");
        }
        return new OdCellKey(styleFamily, null, styleName);
    }

    public static OdCellKey newInstance(short styleFamily, ExtendedCurrency currency, String styleName) {
        if (styleFamily != OdColumnDef.CURRENCY_STYLE_FAMILY) {
            currency = null;
        }
        return new OdCellKey(styleFamily, currency, styleName);
    }

    public static OdCellKey newStandardInstance(String styleName) {
        return new OdCellKey(OdColumnDef.STANDARD_STYLE_FAMILY, null, styleName);
    }

    public static OdCellKey newDateInstance(String styleName) {
        return new OdCellKey(OdColumnDef.DATE_STYLE_FAMILY, null, styleName);
    }

    public static OdCellKey newCurrencyInstance(ExtendedCurrency currency, String styleName) {
        return new OdCellKey(OdColumnDef.CURRENCY_STYLE_FAMILY, currency, styleName);
    }

}
