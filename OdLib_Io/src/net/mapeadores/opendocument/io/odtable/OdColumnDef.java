/* OdLib_Io - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;

import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.money.ExtendedCurrency;


/**
 *
 * @author Vincent Calame
 */
public interface OdColumnDef {

    public final static short STANDARD_STYLE_FAMILY = 0;
    public final static short DATE_STYLE_FAMILY = 1;
    public final static short CURRENCY_STYLE_FAMILY = 2;

    public short getColumnStyleFamily();

    @Nullable
    public ExtendedCurrency getCurrency();

    @Nullable
    public String getCustomStyleName();

    @Nullable
    public String getDefaultCellStyleName();

}
