/* OdLib_Io - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io.odtable;


/**
 *
 * @author Vincent Calame
 */
public class CellStyle {

    private final String name;
    private final String dataStyleName;
    private final String parentStyleName;

    public CellStyle(String name, String dataStyleName, String parentStyleName) {
        this.name = name;
        this.dataStyleName = dataStyleName;
        this.parentStyleName = parentStyleName;
    }

    public String getName() {
        return name;
    }

    public String getDataStyleName() {
        return dataStyleName;
    }

    public String getParentStyleName() {
        return parentStyleName;
    }

}
