/* OdLib_Io - Copyright (c) 2007-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io;


/**
 *
 * @author Vincent Calame
 */
public final class OdZip {

    private final short odType;
    private OdSource contentOdSource;
    private OdSource stylesOdSource;
    private OdSource settingsOdSource;
    private Pictures pictures;


    public OdZip(short odType) {
        this.odType = odType;
    }

    public short odType() {
        return odType;
    }

    public OdSource contentOdSource() {
        return contentOdSource;
    }

    public OdZip contentOdSource(OdSource contentOdSource) {
        this.contentOdSource = contentOdSource;
        return this;
    }

    public OdSource stylesOdSource() {
        return stylesOdSource;
    }

    public OdZip stylesOdSource(OdSource stylesOdSource) {
        this.stylesOdSource = stylesOdSource;
        return this;
    }

    public OdSource settingsOdSource() {
        return settingsOdSource;
    }

    public OdZip settingsOdSource(OdSource settingsOdSource) {
        this.settingsOdSource = settingsOdSource;
        return this;
    }

    public Pictures pictures() {
        return pictures;
    }

    public OdZip pictures(Pictures pictures) {
        this.pictures = pictures;
        return this;
    }

    public static OdZip init(short odType) {
        return new OdZip(odType);
    }

    public static OdZip text() {
        return new OdZip(OdUtils.OD_TEXT);
    }

    public static OdZip spreadSheet() {
        return new OdZip(OdUtils.OD_SPREADSHEET);
    }

}
