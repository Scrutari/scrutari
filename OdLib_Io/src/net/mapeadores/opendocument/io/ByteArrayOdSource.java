/* OdLib_Io - Copyright (c) 2008-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.io;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import net.mapeadores.util.io.IOUtils;


/**
 *
 * @author Vincent Calame
 */
public class ByteArrayOdSource implements OdSource {

    private final byte[] byteArray;

    public ByteArrayOdSource(byte[] byteArray) {
        this.byteArray = byteArray;
    }

    @Override
    public void writeStream(OutputStream outputStream) throws IOException {
        try (InputStream is = new ByteArrayInputStream(byteArray)) {
            IOUtils.copy(is, outputStream);
        }
    }

}
