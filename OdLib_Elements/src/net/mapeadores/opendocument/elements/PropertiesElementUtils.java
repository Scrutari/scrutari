/* OdLib_Elements - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;


/**
 *
 * @author Vincent Calame
 */
public class PropertiesElementUtils implements Constants {

    private final static int count = 9;
    private final static int[] attributesTypeArray = new int[count];
    private final static String[] fileNameArray = new String[count];
    private final static String[] tagNameArray = new String[count];

    static {
        attributesTypeArray[0] = TEXT_PROPERTIES_ATTRIBUTES;
        fileNameArray[0] = "text-properties.txt";
        tagNameArray[0] = "style:text-properties";

        attributesTypeArray[1] = PARAGRAPH_PROPERTIES_ATTRIBUTES;
        fileNameArray[1] = "paragraph-properties.txt";
        tagNameArray[1] = "style:paragraph-properties";

        attributesTypeArray[2] = PAGELAYOUT_PROPERTIES_ATTRIBUTES;
        fileNameArray[2] = "pagelayout-properties.txt";
        tagNameArray[2] = "style:page-layout";

        attributesTypeArray[3] = LISTLEVEL_PROPERTIES_ATTRIBUTES;
        fileNameArray[3] = "listlevel-properties.txt";
        tagNameArray[3] = "style:list-level-properties";

        attributesTypeArray[4] = TABLE_PROPERTIES_ATTRIBUTES;
        fileNameArray[4] = "table-properties.txt";
        tagNameArray[4] = "style:table-properties";

        attributesTypeArray[5] = TABLECOLUMN_PROPERTIES_ATTRIBUTES;
        fileNameArray[5] = "tablecolumn-properties.txt";
        tagNameArray[5] = "style:table-column-properties";

        attributesTypeArray[6] = TABLECELL_PROPERTIES_ATTRIBUTES;
        fileNameArray[6] = "tablecell-properties.txt";
        tagNameArray[6] = "style:table-cell-properties";

        attributesTypeArray[7] = TABLEROW_PROPERTIES_ATTRIBUTES;
        fileNameArray[7] = "tablerow-properties.txt";
        tagNameArray[7] = "style:table-row-properties";

        attributesTypeArray[8] = GRAPHIC_PROPERTIES_ATTRIBUTES;
        fileNameArray[8] = "graphic-properties.txt";
        tagNameArray[8] = "style:graphic-properties";
    }

    private PropertiesElementUtils() {
    }

    public static int count() {
        return count;
    }

    public static int getAttributesType(int i) {
        return attributesTypeArray[i];
    }

    public static String getFileName(int i) {
        return fileNameArray[i];
    }

    public static String getTagName(int i) {
        return tagNameArray[i];
    }

}
