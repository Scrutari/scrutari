/* OdLib_Elements- Copyright (c) 2007-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;


/**
 *
 * @author Vincent Calame
 */
public class OdAttribute implements Constants {

    private final int attributeType;
    private final short nameSpace;
    private final String name;

    OdAttribute(int propertiesType, String name, short nameSpace) {
        this.attributeType = propertiesType;
        this.name = name;
        this.nameSpace = nameSpace;
    }

    public int getAttributeType() {
        return attributeType;
    }

    public String getName() {
        return name;
    }

    public short getNameSpace() {
        return nameSpace;
    }

}
