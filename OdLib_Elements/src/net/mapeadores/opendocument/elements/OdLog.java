/* OdLib_Elements - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public class OdLog {

    protected LogItem fatalErrorLogItem;
    protected final List<LogItem> logItemList = new ArrayList<LogItem>();

    public OdLog() {

    }

    public boolean hasFatalError() {
        return (fatalErrorLogItem != null);
    }

    public LogItem getFatalErrorLogItem() {
        return fatalErrorLogItem;
    }

    public boolean hasErrorOrWarning() {
        return (!logItemList.isEmpty());
    }

    public LogItem[] getLogItemArray() {
        return logItemList.toArray(new LogItem[logItemList.size()]);
    }


    public static class LogItem {

        private final String uri;
        private final String message;

        public LogItem(String uri, String message) {
            this.uri = uri;
            this.message = message;
        }

        public String getURI() {
            return uri;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return uri + " / " + message;
        }

    }

}
