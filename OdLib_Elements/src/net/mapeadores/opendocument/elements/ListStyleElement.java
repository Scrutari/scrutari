/* OdLib_Elements- Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.opendocument.elements;


/**
 *
 * @author Vincent Calame
 */
public class ListStyleElement extends OdElement {

    private final String styleName;
    private final ListLevelElement[] listLevelElementArray = new ListLevelElement[10];
    private final short listLevelType;

    public ListStyleElement(String styleName, short listLevelType) {
        String displayName = OdElementUtils.toDisplayNameValue(styleName);
        this.styleName = OdElementUtils.checkStyleNameValue(styleName);
        this.listLevelType = listLevelType;
        putAttribute("display-name", displayName);
    }

    @Override
    public int[] getAvalaibleTypeArray() {
        int[] result = {LISTSTYLE_ATTRIBUTES};
        return result;
    }

    public ListLevelElement getListLevelElement(int level) {
        return listLevelElementArray[level - 1];
    }

    public ListLevelElement getOrCreateListLevelElement(int level) {
        ListLevelElement result = listLevelElementArray[level - 1];
        if (result == null) {
            result = new ListLevelElement(listLevelType, level);
            listLevelElementArray[level - 1] = result;
        }
        return result;
    }

    public String getStyleName() {
        return styleName;
    }

    @Override
    public void importAttributes(OdElement importElement) {
        super.importAttributes(importElement);
        if (importElement instanceof ListStyleElement) {
            importListLevel((ListStyleElement) importElement);
        }
    }

    private void importListLevel(ListStyleElement other) {
        for (int i = 0; i < 10; i++) {
            ListLevelElement importLevel = other.listLevelElementArray[i];
            if (importLevel == null) {
                continue;
            }
            ListLevelElement current = listLevelElementArray[i];
            if (current == null) {
                listLevelElementArray[i] = importLevel;
            } else {
                current.importAttributes(importLevel);
            }
        }
    }

}
