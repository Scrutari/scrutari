/* ScrutariLib_Xml - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.logging.MessageHandler;
import net.scrutari.data.DataValidator;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import net.scrutari.db.api.DataCoder;
import net.scrutari.db.api.caches.DataCacheWriter;


/**
 *
 * @author Vincent Calame
 */
class SecondPass {

    final DataCacheWriter dataCacheWriter;
    final DataCoder dataCoder;
    final MessageHandler messageHandler;
    final DataValidator dataValidator;
    final BaseURI baseURI;

    SecondPass(DataCacheWriter dataCacheWriter, DataCoder dataCoder, MessageHandler messageHandler, DataValidator dataValidator, BaseURI baseURI) {
        this.dataCacheWriter = dataCacheWriter;
        this.dataCoder = dataCoder;
        this.messageHandler = messageHandler;
        this.dataValidator = dataValidator;
        this.baseURI = baseURI;
    }

    Integer getCode(ScrutariDataURI scrutariDataURI) {
        return dataCoder.getCode(scrutariDataURI, false);
    }

    void xml(String key, String xpath, String attribute) {
        SctXmlUtils.xml(messageHandler, key, xpath, attribute);
    }

    void format(String key, String xpath, String attribute, String value) {
        SctXmlUtils.format(messageHandler, key, xpath, attribute, value);
    }

    void reference(String key, String xpath, String attribute, String value) {
        SctXmlUtils.reference(messageHandler, key, xpath, attribute, value);
    }

    CorpusURI parseCorpusURI(String corpuspath, String xpath) {
        CorpusURI corpusURI = null;
        try {
            ScrutariDataURI scrutariDataURI = parseAbsolute(corpuspath, ScrutariDataURI.CORPUSURI_TYPE);
            if (scrutariDataURI instanceof CorpusURI) {
                corpusURI = (CorpusURI) scrutariDataURI;
            } else {
                SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", xpath, "corpus-path", corpuspath);
            }
        } catch (URIParseException uriParseException) {
            try {
                corpusURI = CorpusURI.check(baseURI, corpuspath);
            } catch (URIParseException upe) {
                SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", xpath, "corpus-path", corpuspath);
            }
        }
        if (corpusURI != null) {
            if (!dataCoder.containsCode(corpusURI)) {
                SctXmlUtils.reference(messageHandler, "_ error.unknown.xml.attributevalue", xpath, "corpus-path", corpuspath);
                corpusURI = null;
            }
        }
        return corpusURI;
    }

    ThesaurusURI parseThesaurusURI(String thesauruspath, String xpath) {
        ThesaurusURI thesaurusURI = null;
        try {
            ScrutariDataURI scrutariDataURI = parseAbsolute(thesauruspath, ScrutariDataURI.THESAURUSURI_TYPE);
            if (scrutariDataURI instanceof ThesaurusURI) {
                thesaurusURI = (ThesaurusURI) scrutariDataURI;
            } else {
                SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", xpath, "thesaurus-path", thesauruspath);
            }
        } catch (URIParseException uriParseException) {
            try {
                thesaurusURI = ThesaurusURI.check(baseURI, thesauruspath);
            } catch (URIParseException upe) {
                SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", xpath, "thesaurus-path", thesauruspath);
            }
        }
        if (thesaurusURI != null) {
            if (!dataCoder.containsCode(thesaurusURI)) {
                SctXmlUtils.reference(messageHandler, "_ error.unknown.xml.attributevalue", xpath, "thesaurus-path", thesauruspath);
            }
        }
        return thesaurusURI;
    }

    ScrutariDataURI parseAbsolute(String path, short defaultScheme) throws URIParseException {
        if (path.startsWith("/")) {
            path = ScrutariDataURI.typeToSchemeString(defaultScheme) + ":" + path;
        }
        return URIParser.parse(path);
    }

}
