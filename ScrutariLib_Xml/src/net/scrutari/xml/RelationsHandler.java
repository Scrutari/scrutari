/* ScrutariLib_Xml - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.RelationData;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class RelationsHandler extends StackElementHandler {

    private final SecondPass secondPass;
    private final String relationsXpath;
    private String defaultRelationType;
    private short defaultMemberType = 0;
    private String defaultRole;
    private ThesaurusURI defaultThesaurusURI;
    private CorpusURI defaultCorpusURI;


    RelationsHandler(SecondPass secondPass, String xpath) {
        this.secondPass = secondPass;
        this.relationsXpath = xpath;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        if (tagname.equals("relation")) {
            String xpath = relationsXpath + "/relation";
            String type = attributes.getValue("type");
            if (type == null) {
                type = defaultRelationType;
            } else {
                xpath = xpath + "[@type='" + type + "']";
            }
            RelationData.Builder builder = new RelationData.Builder(type);
            return new RelationHandler(builder, xpath);
        }
        return null;
    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {
        if (elementHandler instanceof RelationHandler) {
            secondPass.dataCacheWriter.cacheRelationData(((RelationHandler) elementHandler).toRelationData());
        }

    }

    static RelationsHandler init(SecondPass secondPass, Attributes attributes, String xpath) {
        RelationsHandler relationsHandler = new RelationsHandler(secondPass, xpath);
        relationsHandler.defaultRelationType = attributes.getValue("default-relation-type");
        relationsHandler.defaultRole = attributes.getValue("default-role");
        String defaultMemberTypeString = attributes.getValue("default-member-type");
        if (defaultMemberTypeString != null) {
            short defaultMemberType = typeToShort(defaultMemberTypeString);
            if (defaultMemberType != -1) {
                relationsHandler.defaultMemberType = defaultMemberType;
            } else {
                secondPass.format("_ error.wrong.xml.attributevalue", xpath, "default-member-type", defaultMemberTypeString);
            }
        }
        String defaultThesaurusName = attributes.getValue("default-thesaurus");
        if (defaultThesaurusName != null) {
            try {
                relationsHandler.defaultThesaurusURI = ThesaurusURI.check(secondPass.baseURI, defaultThesaurusName);
            } catch (URIParseException pe) {
                secondPass.format("_ error.wrong.xml.attributevalue", xpath, "default-thesaurus", defaultThesaurusName);
            }
        }
        String defaultCorpusName = attributes.getValue("default-corpus");
        if (defaultCorpusName != null) {
            try {
                relationsHandler.defaultCorpusURI = CorpusURI.check(secondPass.baseURI, defaultCorpusName);
            } catch (URIParseException pe) {
                secondPass.format("_ error.wrong.xml.attributevalue", xpath, "default-corpus", defaultCorpusName);
            }
        }
        return relationsHandler;
    }


    class RelationHandler extends StackElementHandler {

        private final RelationData.Builder builder;
        private final String xpath;


        RelationHandler(RelationData.Builder builder, String xpath) {
            this.builder = builder;
            this.xpath = xpath;
        }

        RelationData toRelationData() {
            return builder.toRelationData();
        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            switch (tagname) {
                case "member":
                    addMember(attributes);
                    return null;
                case "attr":
                    AttributeKey attributeKey = SctXmlUtils.parseAttributeKey(attributes, secondPass.messageHandler, xpath);
                    if (attributeKey != null) {
                        return new AttrHandler(attributeKey, secondPass.messageHandler, SctXmlUtils.toXpath(xpath, attributeKey), secondPass.dataValidator);
                    } else {
                        return null;
                    }
                default:
                    return null;
            }
        }

        @Override
        public void closeSubHandler(ElementHandler subElementHandler) {
            if (subElementHandler instanceof AttrHandler) {
                AttrHandler attrHandler = (AttrHandler) subElementHandler;
                builder.addAttribute(attrHandler.getAttributeKey(), attrHandler.getValueList());
            }
        }

        private void addMember(Attributes attributes) {
            String role = attributes.getValue("role");
            if (role == null) {
                role = defaultRole;
            }
            String uri = attributes.getValue("uri");
            if (uri == null) {
                secondPass.xml("_ error.empty.xml.attribute", xpath, "uri");
                return;
            }
            ScrutariDataURI scrutariDataURI = parseURI(uri, attributes.getValue("type"));
            if (scrutariDataURI == null) {
                return;
            }
            Integer code = secondPass.getCode(scrutariDataURI);
            if (code == null) {
                secondPass.reference("_ error.unknown.xml.attributevalue", xpath, "uri", uri);
            } else {
                builder.addMember(code, role);
            }
        }

        private ScrutariDataURI parseURI(String uri, String typeString) {
            short type;
            if (typeString == null) {
                type = defaultMemberType;
            } else {
                type = typeToShort(typeString);
            }
            if (type == 0) {
                if (!uri.contains(":")) {
                    secondPass.xml("_ error.empty.xml.attribute", xpath, "type");
                    return null;
                }
                try {
                    return URIParser.parse(uri);
                } catch (URIParseException pe) {
                    secondPass.format("_ error.wrong.xml.attributevalue", xpath, "uri", uri);
                    return null;
                }
            } else {
                try {
                    return secondPass.parseAbsolute(uri, type);
                } catch (URIParseException pe) {
                    try {
                        return parseRelative(uri, type);
                    } catch (URIParseException pe2) {
                        secondPass.format("_ error.wrong.xml.attributevalue", xpath, "uri", uri);
                        return null;
                    }
                }
            }
        }

        private ScrutariDataURI parseRelative(String uri, short type) throws URIParseException {
            switch (type) {
                case ScrutariDataURI.THESAURUSURI_TYPE:
                    return ThesaurusURI.check(secondPass.baseURI, uri);
                case ScrutariDataURI.CORPUSURI_TYPE:
                    return CorpusURI.check(secondPass.baseURI, uri);
                case ScrutariDataURI.MOTCLEURI_TYPE: {
                    int idx = uri.indexOf("/");
                    if (idx != -1) {
                        ThesaurusURI thesaurusURI = ThesaurusURI.check(secondPass.baseURI, uri.substring(0, idx));
                        return MotcleURI.check(thesaurusURI, uri.substring(idx + 1));
                    } else if (defaultThesaurusURI != null) {
                        return MotcleURI.check(defaultThesaurusURI, uri.substring(idx + 1));
                    } else {
                        secondPass.format("_ error.wrong.xml.attributevalue", xpath, "uri", uri);
                        return null;
                    }
                }
                case ScrutariDataURI.FICHEURI_TYPE: {
                    int idx = uri.indexOf("/");
                    if (idx != -1) {
                        CorpusURI corpusURI = CorpusURI.check(secondPass.baseURI, uri.substring(0, idx));
                        return FicheURI.check(corpusURI, uri.substring(idx + 1));
                    } else if (defaultCorpusURI != null) {
                        return FicheURI.check(defaultCorpusURI, uri.substring(idx + 1));
                    } else {
                        secondPass.format("_ error.wrong.xml.attributevalue", xpath, "uri", uri);
                        return null;
                    }
                }
                default:
                    return null;
            }
        }

    }

    private static short typeToShort(String stringType) {
        switch (stringType) {
            case "motcle":
                return ScrutariDataURI.MOTCLEURI_TYPE;
            case "fiche":
                return ScrutariDataURI.FICHEURI_TYPE;
            case "corpus":
                return ScrutariDataURI.CORPUSURI_TYPE;
            case "thesaurus":
                return ScrutariDataURI.THESAURUSURI_TYPE;
            default:
                return -1;
        }
    }

}
