/* ScrutariLib_Xml - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.LabelElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.LabelConsumer;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
public class PhraseHandler extends StackElementHandler {

    private final LabelConsumer labelConsumer;
    private final MessageHandler messageHandler;
    private final String name;
    private final String xpath;

    public PhraseHandler(LabelConsumer labelConsumer, MessageHandler messageHandler, String name, String xpath) {
        this.labelConsumer = labelConsumer;
        this.messageHandler = messageHandler;
        this.name = name;
        this.xpath = xpath;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        switch (tagname) {
            case "lib":
            case "label":
                try {
                    return new LabelElementHandler(attributes, xpath + "/" + tagname);
                } catch (ErrorMessageException eme) {
                    SctXmlUtils.lang(messageHandler, eme.getErrorMessage());
                    return null;
                }
            default:
                return null;
        }
    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {
        labelConsumer.addLabel(name, SctXmlUtils.testLabel(elementHandler));
    }

}
