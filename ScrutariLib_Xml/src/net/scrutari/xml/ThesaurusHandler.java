/* ScrutariLib_Xml - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.LabelElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.DataConstants;
import net.scrutari.data.DataValidator;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.db.api.DataCoder;
import net.scrutari.db.api.caches.DataCacheWriter;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class ThesaurusHandler extends StackElementHandler {

    private final DataCacheWriter dataCacheWriter;
    private final DataCoder dataCoder;
    private final DataValidator dataValidator;
    private final MessageHandler messageHandler;
    private final ThesaurusURI thesaurusURI;
    private final Integer baseCode;
    private final Integer thesaurusCode;
    private final ThesaurusData.Builder thesaurusDataBuilder;
    private final MotcleHandler motcleHandler = new MotcleHandler();
    private final String thesaurusxpath;
    private boolean metadataDone = false;

    ThesaurusHandler(ThesaurusURI thesaurusURI, Parameters parameters) {
        this.thesaurusURI = thesaurusURI;
        String thesaurusName = thesaurusURI.getThesaurusName();
        this.messageHandler = parameters.getMessageHandler();
        this.dataValidator = parameters.getDataValidator();
        this.thesaurusxpath = "/base/thesaurus[@thesaurus-name='" + thesaurusName + "']";
        this.dataCacheWriter = parameters.getDataCacheWriter();
        this.dataCoder = parameters.getDataCoder();
        if (dataCoder.containsCode(thesaurusURI)) {
            SctXmlUtils.reference(messageHandler, "_ error.existing.xml.attributevalue", thesaurusxpath, "thesaurus-name", thesaurusName);
            throw new IllegalArgumentException();
        }
        this.thesaurusCode = dataCoder.getCode(thesaurusURI, true);
        this.baseCode = parameters.getBaseCode();
        thesaurusDataBuilder = new ThesaurusData.Builder(thesaurusURI);
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        if (!metadataDone) {
            if (tagname.equals("thesaurus-metadata")) {
                metadataDone = true;
                return new MetadataHandler();
            } else {
                SctXmlUtils.xml(messageHandler, "_ error.unsupported.xml.missingtag", thesaurusxpath, "thesaurus-metadata");
                return null;
            }
        } else {
            if (tagname.equals("motcle")) {
                String motcleId = getAttributeString(attributes, "motcle-id");
                if (motcleId == null) {
                    SctXmlUtils.xml(messageHandler, "_ error.empty.xml.attribute", thesaurusxpath + "/motcle", "motcle-id");
                    return null;
                }
                MotcleURI motcleURI;
                try {
                    motcleURI = MotcleURI.check(thesaurusURI, motcleId);
                } catch (URIParseException upe) {
                    SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", thesaurusxpath + "/motcle", "motcle-id", motcleId);
                    return null;
                }
                if (dataCoder.containsCode(motcleURI)) {
                    SctXmlUtils.reference(messageHandler, "_ error.existing.xml.attributevalue", thesaurusxpath + "/motcle[motcle-id='" + motcleId + "']", "motcle-id", motcleId);
                    return null;
                }
                Integer motcleCode = dataCoder.getCode(motcleURI, true);
                motcleHandler.newMotcle(motcleURI, motcleCode);
                return motcleHandler;
            } else {
                return null;
            }
        }
    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {
        if (elementHandler instanceof MotcleHandler) {
            MotcleHandler mh = (MotcleHandler) elementHandler;
            MotcleData.Builder motcleDataBuilder = mh.getMotcleDataBuilder();
            Integer motcleCode = mh.getMotcleCode();
            MotcleData motcleData = motcleDataBuilder.toMotcleData(motcleCode, thesaurusCode, baseCode);
            dataCacheWriter.cacheMotcleData(motcleData);
            thesaurusDataBuilder.addMotcleCode(motcleCode);
        }
    }

    ThesaurusData flushThesaurusData() {
        if (!metadataDone) {
            return null;
        }
        ThesaurusData thesaurusData = thesaurusDataBuilder.toThesaurusData(thesaurusCode, baseCode);
        dataCacheWriter.cacheThesaurusData(thesaurusData);
        return thesaurusData;
    }


    private class MetadataHandler extends StackElementHandler {

        private MetadataHandler() {

        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            String xpath = thesaurusxpath + "/thesaurus-metadata/" + tagname;
            switch (tagname) {
                case "intitule-thesaurus":
                    return new PhraseHandler(thesaurusDataBuilder, messageHandler, DataConstants.THESAURUS_TITLE, xpath);
                case "phrase":
                    String name = SctXmlUtils.parsePhraseName(attributes, messageHandler, xpath);
                    if (name != null) {
                        return new PhraseHandler(thesaurusDataBuilder, messageHandler, name, xpath);
                    } else {
                        return null;
                    }
                case "attr":
                    AttributeKey attributeKey = SctXmlUtils.parseAttributeKey(attributes, messageHandler, xpath);
                    if (attributeKey != null) {
                        return new AttrHandler(attributeKey, messageHandler, SctXmlUtils.toXpath(xpath, attributeKey), dataValidator);
                    } else {
                        return null;
                    }
                default:
                    return null;
            }
        }

        @Override
        public void closeSubHandler(ElementHandler elementHandler) {
            if (elementHandler instanceof AttrHandler) {
                AttrHandler attrHandler = (AttrHandler) elementHandler;
                thesaurusDataBuilder.addAttribute(attrHandler.getAttributeKey(), attrHandler.getValueList());
            }
        }

    }


    private class MotcleHandler extends StackElementHandler {

        private MotcleURI motcleURI;
        private Integer motcleCode;
        private MotcleData.Builder motcleDataBuilder;

        private MotcleHandler() {
        }

        private void newMotcle(MotcleURI motcleURI, Integer motcleCode) {
            this.motcleURI = motcleURI;
            this.motcleCode = motcleCode;
            motcleDataBuilder = new MotcleData.Builder(motcleURI);
        }

        private MotcleData.Builder getMotcleDataBuilder() {
            return motcleDataBuilder;
        }

        private int getMotcleCode() {
            return motcleCode;
        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            String xpath = thesaurusxpath + "/motcle[motcle-id='" + motcleURI.getMotcleId() + "']/" + tagname;
            switch (tagname) {
                case "lib":
                    try {
                    return new LabelElementHandler(attributes, thesaurusxpath + "/motcle[motcle-id='" + motcleURI.getMotcleId() + "']/lib");
                } catch (ErrorMessageException eme) {
                    SctXmlUtils.lang(messageHandler, eme.getErrorMessage());
                    return null;
                }
                case "attr":
                    AttributeKey attributeKey = SctXmlUtils.parseAttributeKey(attributes, messageHandler, xpath);
                    if (attributeKey != null) {
                        return new AttrHandler(attributeKey, messageHandler, SctXmlUtils.toXpath(xpath, attributeKey), dataValidator);
                    } else {
                        return null;
                    }
            }
            return null;
        }

        @Override
        public void closeSubHandler(ElementHandler elementHandler) {
            if (elementHandler instanceof LabelElementHandler) {
                motcleDataBuilder.addLabel(SctXmlUtils.testLabel(elementHandler));
            } else if (elementHandler instanceof AttrHandler) {
                AttrHandler attrHandler = (AttrHandler) elementHandler;
                motcleDataBuilder.addAttribute(attrHandler.getAttributeKey(), attrHandler.getValueList());
            }
        }

    }

}
