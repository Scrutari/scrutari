/* ScrutariLib_Xml - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.util.Set;
import net.mapeadores.util.primitives.DuoKey;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.IndexationData;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.URIParseException;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class IndexationGroupHandler extends StackElementHandler {

    private final SecondPass secondPass;
    private final CorpusURI corpusURI;
    private final ThesaurusURI thesaurusURI;
    private final Set<DuoKey> indexationKeySet;
    private final String xpath;

    IndexationGroupHandler(SecondPass secondPass, Set<DuoKey> indexationKeySet, CorpusURI corpusURI, ThesaurusURI thesaurusURI, String xpath) {
        this.secondPass = secondPass;
        this.indexationKeySet = indexationKeySet;
        this.corpusURI = corpusURI;
        this.thesaurusURI = thesaurusURI;
        this.xpath = xpath;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        if (tagname.equals("indexation")) {
            boolean error = false;
            String ficheid = attributes.getValue("fiche-id");
            String motcleid = attributes.getValue("motcle-id");
            String poidsString = attributes.getValue("poids");
            if (poidsString == null) {
                poidsString = attributes.getValue("weight");
            }
            if (ficheid == null) {
                secondPass.xml("_ error.empty.xml.attribute", xpath + "/" + tagname, "fiche-id");
                error = true;
            }
            if (motcleid == null) {
                secondPass.xml("_ error.empty.xml.attribute", xpath + "/" + tagname, "motcle-id");
                error = true;
            }
            if (!error) {
                String indexationxpath = xpath + "/" + tagname + "[@fiche-id='" + ficheid + "' @motcle-id='" + motcleid + "']";
                FicheURI ficheURI = null;
                try {
                    ficheURI = FicheURI.check(corpusURI, ficheid);
                } catch (URIParseException upe) {
                    secondPass.format("_ error.wrong.xml.attributevalue", xpath + "/" + tagname, "fiche-id", ficheid);
                    error = true;
                }
                MotcleURI motcleURI = null;
                try {
                    motcleURI = MotcleURI.check(thesaurusURI, motcleid);
                } catch (URIParseException upe) {
                    secondPass.format("_ error.wrong.xml.attributevalue", xpath + "/" + tagname, "motcle-id", motcleid);
                    error = true;
                }
                if (!error) {
                    Integer ficheCode = secondPass.getCode(ficheURI);
                    Integer motcleCode = secondPass.getCode(motcleURI);
                    if (ficheCode == null) {
                        secondPass.reference("_ error.unknown.xml.attributevalue", indexationxpath, "fiche-id", ficheid);
                        error = true;
                    }
                    if (motcleCode == null) {
                        secondPass.reference("_ error.unknown.xml.attributevalue", indexationxpath, "motcle-id", motcleid);
                        error = true;
                    }
                    if (!error) {
                        DuoKey indexationKey = new DuoKey(ficheCode, motcleCode);
                        if (indexationKeySet.contains(indexationKey)) {
                            secondPass.reference("_ error.existing.xml.attributevalue", indexationxpath, "fiche-id|motcle-id", ficheid + "|" + motcleid);
                            error = true;
                        }
                        if (!error) {
                            indexationKeySet.add(indexationKey);
                            int poids = 1;
                            if (poidsString != null) {
                                try {
                                    poids = Integer.parseInt(poidsString);
                                    if (poids < 1) {
                                        poids = 1;
                                    }
                                } catch (NumberFormatException nfe) {
                                }
                            }
                            secondPass.dataCacheWriter.cacheIndexationData(new IndexationData(ficheCode, motcleCode, poids));
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {

    }

}
