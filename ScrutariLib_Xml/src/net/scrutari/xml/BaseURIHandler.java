/* ScrutariLib_Xml - Copyright (c) 2018-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.SimpleElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.URIParseException;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class BaseURIHandler extends StackElementHandler {

    private static final int AUTHORITY = 1;
    private static final int BASE_NAME = 2;
    private final MessageHandler messageHandler;
    private String authority = null;
    private String baseName = null;


    BaseURIHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        if ((tagname.equals("authority")) || (tagname.equals("authority-uuid"))) {
            return new SimpleElementHandler(AUTHORITY);
        }
        if (tagname.equals("base-name")) {
            return new SimpleElementHandler(BASE_NAME);
        }
        return null;
    }

    @Override
    public void closeSubHandler(ElementHandler subElementHandler) {
        if (subElementHandler instanceof SimpleElementHandler) {
            SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
            int type = simpleElementHandler.getType();
            String text = simpleElementHandler.getText();
            if (text.length() == 0) {
                text = null;
            }
            switch (type) {
                case AUTHORITY:
                    authority = text;
                    break;
                case BASE_NAME:
                    baseName = text;
                    break;
            }
        }
    }

    BaseURI getBaseURI() {
        if (authority == null) {
            xml("_ error.unsupported.xml.missingtag", "/base/base-metadata", "authority");
        }
        if (baseName == null) {
            xml("_ error.unsupported.xml.missingtag", "/base/base-metadata", "base-name");
        }
        if ((authority != null) && (baseName != null)) {
            try {
                return BaseURI.check(authority, baseName);
            } catch (URIParseException upe) {
                xml("_ error.wrong.xml.tagvalue", "/base/base-metadata", "authority|base-name");
                return null;
            }
        } else {
            return null;
        }
    }

    private void xml(String key, Object... values) {
        DomMessages.invalid(messageHandler, key, values);
    }

}
