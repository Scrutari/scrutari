/* ScrutariLib_Xml - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.mapeadores.util.exceptions.NestedLibraryException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.primitives.DuoKey;
import net.scrutari.data.DataValidator;
import net.scrutari.datauri.BaseURI;
import net.scrutari.db.api.DataCoder;
import net.scrutari.db.api.caches.DataCacheWriter;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author Vincent Calame
 */
public class SecondPassEngine {

    private final SAXParserFactory factory;
    private final DataCacheWriter dataCacheWriter;
    private final DataCoder dataCoder;
    private final MessageHandler messageHandler;
    private final DataValidator dataValidator;
    private final Set<DuoKey> indexationKeySet = new HashSet<DuoKey>();

    public SecondPassEngine(DataCacheWriter dataCacheWriter, DataCoder dataCoder, MessageHandler messageHandler, DataValidator dataValidator) {
        this.dataCacheWriter = dataCacheWriter;
        this.dataCoder = dataCoder;
        this.messageHandler = messageHandler;
        this.dataValidator = dataValidator;
        this.factory = SAXParserFactory.newInstance();
    }

    public void run(InputStream inputStream, BaseURI baseURI) throws SAXException, IOException {
        SAXParser saxParser;
        try {
            saxParser = factory.newSAXParser();

        } catch (ParserConfigurationException pce) {
            throw new NestedLibraryException(pce);
        }
        try {
            SecondPass secondPass = new SecondPass(dataCacheWriter, dataCoder, messageHandler, dataValidator, baseURI);
            saxParser.parse(inputStream, new SaxHandler(secondPass));
        } catch (StopException se) {

        }
    }


    private class SaxHandler extends DefaultHandler {

        private final SecondPass secondPass;
        private SecondPassRootHandler rootHandler;

        private SaxHandler(SecondPass secondPass) {
            this.secondPass = secondPass;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (rootHandler == null) {
                if (qName.equals("base")) {
                    rootHandler = new SecondPassRootHandler(secondPass, indexationKeySet);
                } else {
                    throw new StopException();
                }
            } else {
                rootHandler.processStartElement(qName, attributes);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            rootHandler.processEndElement(qName);
        }

    }

}
