/* ScrutariLib_Xml - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.mapeadores.util.exceptions.NestedLibraryException;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.data.BaseData;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author Vincent Calame
 */
public class FirstPassEngine {

    private final SAXParserFactory factory;

    public FirstPassEngine() {
        this.factory = SAXParserFactory.newInstance();
    }

    public void run(BaseData.Builder baseDataBuilder, Parameters parameters, InputStream inputStream, boolean includeBaseMetadata) throws SAXException, IOException {
        SAXParser saxParser;
        try {
            saxParser = factory.newSAXParser();

        } catch (ParserConfigurationException pce) {
            throw new NestedLibraryException(pce);
        }
        try {
            saxParser.parse(inputStream, new SaxHandler(baseDataBuilder, parameters, includeBaseMetadata));
        } catch (StopException se) {

        }
    }


    private class SaxHandler extends DefaultHandler {

        private final BaseData.Builder baseDataBuilder;
        private final Parameters parameters;
        private final boolean includeBaseMetadata;
        private FirstPassRootHandler rootHandler = null;

        private SaxHandler(BaseData.Builder baseDataBuilder, Parameters parameters, boolean includeBaseMetadata) {
            this.baseDataBuilder = baseDataBuilder;
            this.parameters = parameters;
            this.includeBaseMetadata = includeBaseMetadata;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (rootHandler == null) {
                if (qName.equals("base")) {
                    rootHandler = new FirstPassRootHandler(baseDataBuilder, parameters, includeBaseMetadata);
                } else {
                    DomMessages.invalid(parameters.getMessageHandler(), "_ error.wrong.xml.root", qName, "base");
                    throw new StopException();
                }
            } else {
                rootHandler.processStartElement(qName, attributes);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            rootHandler.processEndElement(qName);
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (rootHandler != null) {
                rootHandler.processText(ch, start, length);
            }
        }

    }

}
