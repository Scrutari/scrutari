/* UtilLib - Copyright (c) 2015-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.mapeadores.util.exceptions.NestedLibraryException;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author Vincent Calame
 */
public class XmlChecker {

    private final SAXParserFactory factory;

    public XmlChecker() {
        this.factory = SAXParserFactory.newInstance();
    }

    public void check(InputStream inputStream) throws SAXException, IOException {
        try {
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(inputStream, new DefaultHandler());
        } catch (ParserConfigurationException pce) {
            throw new NestedLibraryException(pce);
        }
    }

}
