/* ScrutariLib_Xml - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.util.Set;
import net.mapeadores.util.primitives.DuoKey;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.ThesaurusURI;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class SecondPassRootHandler extends StackElementHandler {

    private final SecondPass secondPass;
    private final Set<DuoKey> indexationKeySet;

    SecondPassRootHandler(SecondPass secondPass, Set<DuoKey> indexationKeySet) {
        this.secondPass = secondPass;
        this.indexationKeySet = indexationKeySet;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        String xpath = "/base/" + tagname;
        switch (tagname) {
            case "indexation-group": {
                boolean error = false;
                String corpuspath = attributes.getValue("corpus-path");
                if (corpuspath == null) {
                    secondPass.xml("_ error.empty.xml.attribute", xpath, "corpus-path");
                    error = true;
                }
                String thesauruspath = attributes.getValue("thesaurus-path");
                if (thesauruspath == null) {
                    secondPass.xml("_ error.empty.xml.attribute", xpath, "thesaurus-path");
                    error = true;
                }
                if (error) {
                    return null;
                }
                xpath = xpath + "[@corpus-path='" + corpuspath + "' and @thesaurus-path='" + thesauruspath + "']";
                CorpusURI corpusURI = secondPass.parseCorpusURI(corpuspath, xpath);
                ThesaurusURI thesaurusURI = secondPass.parseThesaurusURI(thesauruspath, xpath);
                if ((corpusURI == null) || (thesaurusURI == null)) {
                    return null;
                }
                return new IndexationGroupHandler(secondPass, indexationKeySet, corpusURI, thesaurusURI, xpath);
            }
            case "relations":
                return RelationsHandler.init(secondPass, attributes, xpath);
            default:
                return null;
        }

    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {

    }


}
