/* ScrutariLib_Xml - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.logging.MessageHandler;
import net.scrutari.data.DataValidator;
import net.scrutari.datauri.BaseURI;
import net.scrutari.db.api.DataCoder;
import net.scrutari.db.api.caches.DataCacheWriter;


/**
 *
 * @author Vincent Calame
 */
public class Parameters {

    private final DataCacheWriter dataCacheWriter;
    private final DataCoder dataCoder;
    private final DataValidator dataValidator;
    private final MessageHandler messageHandler;
    private final BaseURI baseURI;
    private final Integer baseCode;

    public Parameters(DataCacheWriter dataCacheWriter, DataCoder dataCoder, DataValidator dataValidator, MessageHandler messageHandler, BaseURI baseURI, Integer baseCode) {
        this.dataCacheWriter = dataCacheWriter;
        this.dataCoder = dataCoder;
        this.dataValidator = dataValidator;
        this.messageHandler = messageHandler;
        this.baseURI = baseURI;
        this.baseCode = baseCode;
    }

    public DataCacheWriter getDataCacheWriter() {
        return dataCacheWriter;
    }

    public DataCoder getDataCoder() {
        return dataCoder;
    }

    public DataValidator getDataValidator() {
        return dataValidator;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public BaseURI getBaseURI() {
        return baseURI;
    }

    public Integer getBaseCode() {
        return baseCode;
    }

}
