/* ScrutariLib_Xml - Copyright (c) 2018-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataValidator;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.URIParseException;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class FirstPassRootHandler extends StackElementHandler {

    private final BaseData.Builder baseDataBuilder;
    private final Parameters parameters;
    private final MessageHandler messageHandler;
    private final DataValidator dataValidator;
    private final boolean buildBaseMetadata;

    FirstPassRootHandler(BaseData.Builder baseDataBuilder, Parameters parameters, boolean buildBaseMetadata) {
        this.baseDataBuilder = baseDataBuilder;
        this.parameters = parameters;
        this.messageHandler = parameters.getMessageHandler();
        this.dataValidator = parameters.getDataValidator();
        this.buildBaseMetadata = buildBaseMetadata;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        switch (tagname) {
            case "base-metadata":
                if (buildBaseMetadata) {
                    return new BaseMetadataHandler(baseDataBuilder, parameters.getMessageHandler(), dataValidator, "/base/base-metadata");
                } else {
                    return null;
                }
            case "corpus":
                String corpusName = getAttributeString(attributes, "corpus-name");
                if (corpusName == null) {
                    SctXmlUtils.xml(messageHandler, "_ error.empty.xml.attribute", "/base/corpus", "corpus-name");
                    return null;
                }
                CorpusURI corpusURI;
                try {
                    corpusURI = CorpusURI.check(parameters.getBaseURI(), corpusName);
                } catch (URIParseException upe) {
                    SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", "/base/corpus", "corpus-name", corpusName);
                    return null;
                }
                try {
                    return new CorpusHandler(corpusURI, parameters);
                } catch (IllegalArgumentException iae) {
                    return null;
                }
            case "thesaurus":
                String thesaurusName = getAttributeString(attributes, "thesaurus-name");
                if (thesaurusName == null) {
                    SctXmlUtils.xml(messageHandler, "_ error.empty.xml.attribute", "/base/thesaurus", "thesaurus-name");
                    return null;
                }
                ThesaurusURI thesaurusURI;
                try {
                    thesaurusURI = ThesaurusURI.check(parameters.getBaseURI(), thesaurusName);
                } catch (URIParseException upe) {
                    SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", "/base/thesaurus", "thesaurus-name", thesaurusName);
                    return null;
                }
                try {
                    return new ThesaurusHandler(thesaurusURI, parameters);
                } catch (IllegalArgumentException iae) {
                    return null;
                }
            default:
                return null;
        }
    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {
        if (elementHandler instanceof CorpusHandler) {
            CorpusData corpusData = ((CorpusHandler) elementHandler).flushCorpusData();
            if (corpusData != null) {
                baseDataBuilder.addCorpusCode(corpusData.getCorpusCode());
            }
        } else if (elementHandler instanceof ThesaurusHandler) {
            ThesaurusData thesaurusData = ((ThesaurusHandler) elementHandler).flushThesaurusData();
            if (thesaurusData != null) {
                baseDataBuilder.addThesaurusCode(thesaurusData.getThesaurusCode());
            }
        }
    }

}
