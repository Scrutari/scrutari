/* ScrutariLib_Xml - Copyright (c) 2011-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.localisation.Message;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.LabelElementHandler;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
public final class SctXmlUtils {

    private SctXmlUtils() {
    }

    public static Label testLabel(ElementHandler elementHandler) {
        if (elementHandler instanceof LabelElementHandler) {
            return ((LabelElementHandler) elementHandler).toLabel();
        } else {
            return null;
        }
    }

    public static URI testURI(String href) throws MalformedURLException, URISyntaxException {
        if (href == null) {
            return null;
        }
        href = href.trim();
        if (href.length() == 0) {
            return null;
        }
        href = href.replace(" ", "%20");
        URI uri = new URI(href);
        if (uri.isAbsolute()) {
            new URL(uri.toString());
        } else {
            new URL(new URL("http://www.scrutari.net"), uri.toString());
        }
        return uri;
    }

    public static AttributeKey parseAttributeKey(Attributes attributes, MessageHandler messageHandler, String xpath) {
        String localKey = attributes.getValue("key");
        if (localKey != null) {
            localKey = localKey.trim();
        }
        if ((localKey == null) || (localKey.isEmpty())) {
            xml(messageHandler, "_ error.empty.xml.attribute", xpath, "key");
            return null;
        }
        String nameSpace = attributes.getValue("ns");
        if (nameSpace != null) {
            nameSpace = nameSpace.trim();
        }
        if ((nameSpace == null) || (nameSpace.isEmpty())) {
            xml(messageHandler, "_ error.empty.xml.attribute", xpath + "[@key='" + localKey + "']", "ns");
            return null;
        }
        try {
            AttributeKey attributeKey = AttributeKey.parse(nameSpace + ":" + localKey);
            return attributeKey;
        } catch (ParseException pe) {
            format(messageHandler, "_ error.wrong.xml.attributevalue", xpath + "[@key='" + localKey + "' AND ns='" + nameSpace + "']", "key|ns", nameSpace + ":" + localKey);
            return null;
        }
    }

    public static String parsePhraseName(Attributes attributes, MessageHandler messageHandler, String xpath) {
        String name = attributes.getValue("name");
        if (name == null) {
            name = "";
        } else {
            name = name.trim();
        }
        if (name.isEmpty()) {
            xml(messageHandler, "_ error.empty.xml.attribute", xpath, "name");
            return null;
        } else {
            return name;
        }
    }

    public static void format(MessageHandler messageHandler, String key, Object... values) {
        messageHandler.addMessage("severe.scrutaridata.format", LocalisationUtils.toMessage(key, values));
    }

    public static void lang(MessageHandler messageHandler, String key, Object... values) {
        lang(messageHandler, LocalisationUtils.toMessage(key, values));
    }

    public static void lang(MessageHandler messageHandler, Message message) {
        messageHandler.addMessage("severe.scrutaridata.lang", message);
    }

    public static void reference(MessageHandler messageHandler, String key, Object... values) {
        messageHandler.addMessage("severe.scrutaridata.reference", LocalisationUtils.toMessage(key, values));
    }

    public static void xml(MessageHandler messageHandler, String key, Object... values) {
        DomMessages.invalid(messageHandler, key, values);
    }

    public static String toXpath(String xpath, AttributeKey attributeKey) {
        return xpath + "[@key='" + attributeKey.getLocalKey() + "' AND ns='" + attributeKey.getNameSpace() + "']";
    }

    public static String checkLangAlias(String lang) {
        switch (lang) {
            case "pt_br":
                return "pt";
            default:
                return lang;
        }
    }

}
