/* ScrutariLib_Xml - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.SimpleElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.DataValidator;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class BaseMetadataHandler extends StackElementHandler {

    private static final int BASE_ICON = 1;
    private static final int LANG = 2;
    private final BaseData.Builder baseDataBuilder;
    private final MessageHandler messageHandler;
    private final DataValidator dataValidator;
    private final String xpath;


    BaseMetadataHandler(BaseData.Builder baseDataBuilder, MessageHandler messageHandler, DataValidator dataValidator, String xpath) {
        this.baseDataBuilder = baseDataBuilder;
        this.messageHandler = messageHandler;
        this.dataValidator = dataValidator;
        this.xpath = xpath;
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        String tagXpath = xpath + "/" + tagname;
        switch (tagname) {
            case "base-icon":
                return new SimpleElementHandler(BASE_ICON);
            case "intitule-short":
                return new PhraseHandler(baseDataBuilder, messageHandler, DataConstants.BASE_TITLE, tagXpath);
            case "intitule-long":
                return new PhraseHandler(baseDataBuilder, messageHandler, DataConstants.BASE_LONGTITLE, tagXpath);
            case "phrase":
                String name = SctXmlUtils.parsePhraseName(attributes, messageHandler, xpath);
                if (name != null) {
                    return new PhraseHandler(baseDataBuilder, messageHandler, name, xpath);
                } else {
                    return null;
                }
            case "langs-ui":
                return new AvailableLangHandler(baseDataBuilder, messageHandler, tagXpath);
            case "attr":
                AttributeKey attributeKey = SctXmlUtils.parseAttributeKey(attributes, messageHandler, tagXpath);
                if (attributeKey != null) {
                    return new AttrHandler(attributeKey, messageHandler, SctXmlUtils.toXpath(tagXpath, attributeKey), dataValidator);
                } else {
                    return null;
                }
            default:
                return null;
        }
    }


    @Override
    public void closeSubHandler(ElementHandler subElementHandler) {
        if (subElementHandler instanceof AttrHandler) {
            AttrHandler attrHandler = (AttrHandler) subElementHandler;
            baseDataBuilder.addAttribute(attrHandler.getAttributeKey(), attrHandler.getValueList());
        } else if (subElementHandler instanceof SimpleElementHandler) {
            SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
            int type = simpleElementHandler.getType();
            String text = simpleElementHandler.getText();
            switch (type) {
                case BASE_ICON:
                    boolean done = false;
                    try {
                        done = baseDataBuilder.setBaseIcon(SctXmlUtils.testURI(text));
                    } catch (MalformedURLException | URISyntaxException e) {
                    }
                    if (!done) {
                        SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", xpath + "/base-icon", text);
                    }
                    break;
            }
        }
    }


    private static class AvailableLangHandler extends StackElementHandler {

        private final BaseData.Builder baseDataBuilder;
        private final MessageHandler messageHandler;
        private final String xpath;

        private AvailableLangHandler(BaseData.Builder baseDataBuilder, MessageHandler messageHandler, String xpath) {
            this.baseDataBuilder = baseDataBuilder;
            this.messageHandler = messageHandler;
            this.xpath = xpath;
        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            if (tagname.equals("lang")) {
                return new SimpleElementHandler(LANG);
            }
            return null;
        }

        @Override
        public void closeSubHandler(ElementHandler subElementHandler) {
            if (subElementHandler instanceof SimpleElementHandler) {
                SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
                int type = simpleElementHandler.getType();
                String text = simpleElementHandler.getText();
                try {
                    Lang lang = Lang.parse(text);
                    baseDataBuilder.addAvailableLang(lang);
                } catch (ParseException ile) {
                    SctXmlUtils.lang(messageHandler, "_ error.wrong.xml.tagvalue", xpath + "/lang", text);
                }
            }
        }

    }

}
