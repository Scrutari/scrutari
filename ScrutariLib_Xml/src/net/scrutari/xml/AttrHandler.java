/* ScrutariLib_Xml - Copyright (c) 2017-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.text.CleanedString;
import net.mapeadores.util.text.StringChecker;
import net.mapeadores.util.text.StringSplitter;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.SimpleElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.DataValidator;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class AttrHandler extends StackElementHandler {

    private static final int VALUE = 1;
    private final AttributeKey attributeKey;
    private final List<CleanedString> valueList = new ArrayList<CleanedString>();
    private final MessageHandler messageHandler;
    private final String xpath;
    private final DataValidator dataValidator;
    private final boolean unique;
    private final StringChecker stringChecker;
    private final StringSplitter stringSplitter;


    AttrHandler(AttributeKey attributeKey, MessageHandler messageHandler, String xpath, DataValidator dataValidator) {
        this.attributeKey = attributeKey;
        this.messageHandler = messageHandler;
        this.xpath = xpath;
        this.dataValidator = dataValidator;
        this.stringChecker = dataValidator.getFormatChecker(attributeKey);
        this.unique = dataValidator.isUnique(attributeKey);
        this.stringSplitter = dataValidator.getSplitter(attributeKey);
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        switch (tagname) {
            case "val":
                return new SimpleElementHandler(VALUE);
            default:
                return null;
        }
    }

    @Override
    public void closeSubHandler(ElementHandler subElementHandler) {
        if (subElementHandler instanceof SimpleElementHandler) {
            SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
            int type = simpleElementHandler.getType();
            String text = simpleElementHandler.getText();
            switch (type) {
                case VALUE:
                    addValue(CleanedString.newInstance(text));
                    break;
            }
        }
    }

    AttributeKey getAttributeKey() {
        return attributeKey;
    }

    List<CleanedString> getValueList() {
        return valueList;
    }

    private void addValue(CleanedString cs) {
        if (cs == null) {
            return;
        }
        cs = check(cs);
        if (cs == null) {
            return;
        }
        if ((unique) && (valueList.size() > 1)) {
            return;
        }
        if (stringSplitter != null) {
            for (String token : stringSplitter.split(cs.toString())) {
                CleanedString cleanToken = CleanedString.newInstance(token);
                if (cleanToken != null) {
                    valueList.add(cleanToken);
                    if (unique) {
                        break;
                    }
                }
            }
        } else {
            valueList.add(cs);
        }
    }

    private CleanedString check(CleanedString cs) {
        if (stringChecker == null) {
            return cs;
        }
        String orignalText = cs.toString();
        String checkedText = stringChecker.check(orignalText);
        if (checkedText == null) {
            SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", xpath + "/val", orignalText);
            return null;
        } else if (!checkedText.equals(orignalText)) {
            return CleanedString.newInstance(checkedText);
        } else {
            return cs;
        }
    }

}
