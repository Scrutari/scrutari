/* ScrutariLib_Xml - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.mapeadores.util.exceptions.NestedLibraryException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataValidator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author Vincent Calame
 */
public class ConfMetadataEngine {

    private final SAXParserFactory factory;

    public ConfMetadataEngine() {
        this.factory = SAXParserFactory.newInstance();
    }

    public void run(BaseData.Builder baseDataBuilder, InputStream inputStream, MessageHandler messageHandler, DataValidator dataValidator) throws SAXException, IOException {
        SAXParser saxParser;
        try {
            saxParser = factory.newSAXParser();
        } catch (ParserConfigurationException pce) {
            throw new NestedLibraryException(pce);
        }
        try {
            saxParser.parse(inputStream, new SaxHandler(baseDataBuilder, messageHandler, dataValidator));
        } catch (StopException se) {

        }
    }


    private class SaxHandler extends DefaultHandler {

        private final BaseData.Builder baseDataBuilder;
        private final MessageHandler messageHandler;
        private final DataValidator dataValidator;
        private BaseMetadataHandler metadataRootHandler = null;

        private SaxHandler(BaseData.Builder baseDataBuilder, MessageHandler messageHandler, DataValidator dataValidator) {
            this.baseDataBuilder = baseDataBuilder;
            this.messageHandler = messageHandler;
            this.dataValidator = dataValidator;
        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (metadataRootHandler == null) {
                if (qName.equals("base-metadata")) {
                    metadataRootHandler = new BaseMetadataHandler(baseDataBuilder, messageHandler, dataValidator, "/base-metadata");
                } else {
                    DomMessages.invalid(messageHandler, "_ error.wrong.xml.root", qName, "base-metadata");
                    throw new StopException();
                }
            } else {
                metadataRootHandler.processStartElement(qName, attributes);
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            metadataRootHandler.processEndElement(qName);
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (metadataRootHandler != null) {
                metadataRootHandler.processText(ch, start, length);
            }
        }

    }

}
