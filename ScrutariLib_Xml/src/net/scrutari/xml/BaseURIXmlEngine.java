/* ScrutariLib_Xml - Copyright (c) 2016-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import net.mapeadores.util.exceptions.NestedLibraryException;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.xml.DomMessages;
import net.scrutari.datauri.BaseURI;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;


/**
 *
 * @author Vincent Calame
 */
public class BaseURIXmlEngine {

    private static final short START = 0;
    private static final short ON_BASE = 1;
    private static final short ON_METADATA = 2;
    private final SAXParserFactory factory;
    private final MessageHandler messageHandler;

    public BaseURIXmlEngine(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        this.factory = SAXParserFactory.newInstance();
    }

    public BaseURI read(InputStream inputStream) throws SAXException, IOException {
        SAXParser saxParser;
        try {
            saxParser = factory.newSAXParser();

        } catch (ParserConfigurationException pce) {
            throw new NestedLibraryException(pce);
        }
        try {
            saxParser.parse(inputStream, new SaxHandler());
        } catch (StopException stopException) {
            return stopException.getBaseURI();
        }
        return null;
    }

    private void xml(String key, Object... values) {
        DomMessages.invalid(messageHandler, key, values);
    }


    private class SaxHandler extends DefaultHandler {

        BaseURIHandler metadataHandler = null;
        short state = START;

        private SaxHandler() {

        }

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            switch (state) {
                case START:
                    if (qName.equals("base")) {
                        state = ON_BASE;
                        break;
                    } else {
                        xml("_ error.wrong.xml.root", qName, "base");
                        throw new StopException(null);
                    }
                case ON_BASE:
                    if (qName.equals("base-metadata")) {
                        state = ON_METADATA;
                        metadataHandler = new BaseURIHandler(messageHandler);
                        break;
                    } else {
                        xml("_ error.unsupported.xml.missingtag", "/base", "base-metadata");
                        throw new StopException(null);
                    }
                case ON_METADATA:
                    metadataHandler.processStartElement(qName, null);
                    break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) throws SAXException {
            if (metadataHandler != null) {
                boolean done = metadataHandler.processEndElement(qName);
                if (done) {
                    throw new StopException(metadataHandler.getBaseURI());
                }
            }
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            if (metadataHandler != null) {
                metadataHandler.processText(ch, start, length);
            }
        }

    }


    private static class StopException extends RuntimeException {

        private final BaseURI baseURI;

        private StopException(BaseURI baseURI) {
            this.baseURI = baseURI;
        }

        private BaseURI getBaseURI() {
            return baseURI;
        }

    }

}
