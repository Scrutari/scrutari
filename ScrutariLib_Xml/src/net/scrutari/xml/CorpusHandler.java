/* ScrutariLib_Xml - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.xml;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageHandler;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.xml.handlers.ElementHandler;
import net.mapeadores.util.xml.handlers.SimpleElementHandler;
import net.mapeadores.util.xml.handlers.StackElementHandler;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.DataValidationException;
import net.scrutari.data.DataValidator;
import net.scrutari.data.FicheData;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.db.api.DataCoder;
import net.scrutari.db.api.caches.DataCacheWriter;
import org.xml.sax.Attributes;


/**
 *
 * @author Vincent Calame
 */
class CorpusHandler extends StackElementHandler {

    private static final int HREF_PARENT = 1;
    private static final int TITRE = 3;
    private static final int SOUSTITRE = 4;
    private static final int DATE = 5;
    private static final int LANG = 6;
    private static final int HREF = 7;
    private static final int COMPLEMENT = 8;
    private static final int FICHE_ICON = 9;
    private static final int LATITUDE = 10;
    private static final int LONGITUDE = 11;
    private final DataCacheWriter dataCacheWriter;
    private final DataCoder dataCoder;
    private final DataValidator dataValidator;
    private final MessageHandler messageHandler;
    private final CorpusURI corpusURI;
    private final Integer baseCode;
    private final Integer corpusCode;
    private final CorpusData.Builder corpusDataBuilder;
    private final FicheHandler ficheHandler = new FicheHandler();
    private final String corpusxpath;
    private boolean metadataDone = false;

    CorpusHandler(CorpusURI corpusURI, Parameters parameters) {
        this.corpusURI = corpusURI;
        String corpusName = corpusURI.getCorpusName();
        this.messageHandler = parameters.getMessageHandler();
        this.dataValidator = parameters.getDataValidator();
        this.corpusxpath = "/base/corpus[@corpus-name='" + corpusName + "']";
        this.dataCacheWriter = parameters.getDataCacheWriter();
        this.dataCoder = parameters.getDataCoder();
        if (dataCoder.containsCode(corpusURI)) {
            SctXmlUtils.reference(messageHandler, "_ error.existing.xml.attributevalue", corpusxpath, "corpus-name", corpusName);
            throw new IllegalArgumentException();
        }
        this.corpusCode = dataCoder.getCode(corpusURI, true);
        this.baseCode = parameters.getBaseCode();
        corpusDataBuilder = new CorpusData.Builder(corpusURI);
    }

    @Override
    public ElementHandler newSubHandler(String tagname, Attributes attributes) {
        if (!metadataDone) {
            if (tagname.equals("corpus-metadata")) {
                metadataDone = true;
                return new MetadataHandler();
            } else {
                SctXmlUtils.xml(messageHandler, "_ error.unsupported.xml.missingtag", corpusxpath, "corpus-metadata");
                return null;
            }
        } else {
            if (tagname.equals("fiche")) {
                String ficheId = getAttributeString(attributes, "fiche-id");
                if (ficheId == null) {
                    SctXmlUtils.xml(messageHandler, "_ error.empty.xml.attribute", corpusxpath + "/fiche", "fiche-id");
                    return null;
                }
                String ficheXpath = corpusxpath + "/fiche[@fiche-id='" + ficheId + "']";
                FicheURI ficheURI;
                try {
                    ficheURI = FicheURI.check(corpusURI, ficheId);
                } catch (URIParseException upe) {
                    SctXmlUtils.format(messageHandler, "_ error.wrong.xml.attributevalue", corpusxpath + "/fiche", "fiche-id", ficheId);
                    return null;
                }
                if (dataCoder.containsCode(ficheURI)) {
                    SctXmlUtils.reference(messageHandler, "_ error.existing.xml.attributevalue", ficheXpath, "fiche-id", ficheId);
                    return null;
                }
                ficheHandler.newFiche(ficheURI, ficheXpath);
                return ficheHandler;
            } else {
                return null;
            }
        }
    }

    @Override
    public void closeSubHandler(ElementHandler elementHandler) {
        if (elementHandler instanceof FicheHandler) {
            FicheHandler fh = (FicheHandler) elementHandler;
            FicheData.Builder ficheDataBuilder = fh.getFicheDataBuilder();
            try {
                ficheDataBuilder.validate(dataValidator);
                Integer ficheCode = dataCoder.getCode(fh.getFicheURI(), true);
                FicheData ficheData = ficheDataBuilder.toFicheData(ficheCode, corpusDataBuilder.getHrefParent(), corpusCode, baseCode);
                dataCacheWriter.cacheFicheData(ficheData);
                corpusDataBuilder.addFicheCode(ficheCode);
            } catch (DataValidationException dve) {

            }
        }
    }

    CorpusData flushCorpusData() {
        if (!metadataDone) {
            return null;
        }
        CorpusData corpusData = corpusDataBuilder.toCorpusData(corpusCode, baseCode);
        dataCacheWriter.cacheCorpusData(corpusData);
        return corpusData;
    }


    private class MetadataHandler extends StackElementHandler {

        private MetadataHandler() {

        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            String xpath = corpusxpath + "/corpus-metadata/" + tagname;
            switch (tagname) {
                case "intitule-corpus":
                    return new PhraseHandler(corpusDataBuilder, messageHandler, DataConstants.CORPUS_TITLE, xpath);
                case "intitule-fiche":
                    return new PhraseHandler(corpusDataBuilder, messageHandler, DataConstants.CORPUS_FICHE, xpath);
                case "complement-metadata":
                    int complementNumber = corpusDataBuilder.createNewComplement();
                    return new PhraseHandler(corpusDataBuilder, messageHandler, DataConstants.CORPUS_COMPLEMENTPREFIX + complementNumber, xpath);
                case "phrase":
                    String name = SctXmlUtils.parsePhraseName(attributes, messageHandler, xpath);
                    if (name != null) {
                        return new PhraseHandler(corpusDataBuilder, messageHandler, name, xpath);
                    } else {
                        return null;
                    }
                case "href-parent":
                    return new SimpleElementHandler(HREF_PARENT);
                case "corpus-icon":
                    return null;
                case "attr":
                    AttributeKey attributeKey = SctXmlUtils.parseAttributeKey(attributes, messageHandler, xpath);
                    if (attributeKey != null) {
                        return new AttrHandler(attributeKey, messageHandler, SctXmlUtils.toXpath(xpath, attributeKey), dataValidator);
                    } else {
                        return null;
                    }
                default:
                    return null;
            }
        }

        @Override
        public void closeSubHandler(ElementHandler subElementHandler) {
            if (subElementHandler instanceof AttrHandler) {
                AttrHandler attrHandler = (AttrHandler) subElementHandler;
                corpusDataBuilder.addAttribute(attrHandler.getAttributeKey(), attrHandler.getValueList());
            } else if (subElementHandler instanceof SimpleElementHandler) {
                SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
                int type = simpleElementHandler.getType();
                String text = simpleElementHandler.getText();
                switch (type) {
                    case HREF_PARENT:
                        boolean hrefParentDone = false;
                        try {
                            hrefParentDone = corpusDataBuilder.setHrefParent(SctXmlUtils.testURI(text));
                        } catch (MalformedURLException | URISyntaxException e) {

                        }
                        if (!hrefParentDone) {
                            SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", corpusxpath + "/corpus-metadata/href-parent", text);
                        }
                        break;
                }
            }
        }

    }


    private class FicheHandler extends StackElementHandler {

        private String ficheXpath;
        private FicheURI ficheURI;
        private FicheData.Builder ficheDataBuilder;

        private FicheHandler() {
        }

        private void newFiche(FicheURI ficheURI, String ficheXpath) {
            this.ficheURI = ficheURI;
            this.ficheXpath = ficheXpath;
            ficheDataBuilder = new FicheData.Builder(ficheURI, corpusDataBuilder.getComplementMaxNumber());
        }

        private FicheData.Builder getFicheDataBuilder() {
            return ficheDataBuilder;
        }

        private FicheURI getFicheURI() {
            return ficheURI;
        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            switch (tagname) {
                case "titre":
                    return new SimpleElementHandler(TITRE);
                case "soustitre":
                    return new SimpleElementHandler(SOUSTITRE);
                case "date":
                    return new SimpleElementHandler(DATE);
                case "lang":
                    return new SimpleElementHandler(LANG);
                case "href":
                    return new SimpleElementHandler(HREF);
                case "complement":
                    return new SimpleElementHandler(COMPLEMENT);
                case "fiche-icon":
                    return new SimpleElementHandler(FICHE_ICON);
                case "geoloc":
                    return new GeolocHandler();
                case "attr":
                    AttributeKey attributeKey = SctXmlUtils.parseAttributeKey(attributes, messageHandler, ficheXpath + "/attr");
                    if (attributeKey != null) {
                        return new AttrHandler(attributeKey, messageHandler, SctXmlUtils.toXpath(ficheXpath + "/attr", attributeKey), dataValidator);
                    } else {
                        return null;
                    }
                default:
                    return null;
            }
        }


        @Override
        public void closeSubHandler(ElementHandler subElementHandler) {
            if (subElementHandler instanceof GeolocHandler) {
                GeolocHandler geolocHandler = (GeolocHandler) subElementHandler;
                String latitude = geolocHandler.latitude;
                String longitude = geolocHandler.longitude;
                boolean done = true;
                if (latitude == null) {
                    done = false;
                    SctXmlUtils.xml(messageHandler, "_ error.unsupported.xml.missingtag", ficheXpath + "/geoloc", "lat");
                }
                if (longitude == null) {
                    done = false;
                    SctXmlUtils.xml(messageHandler, "_ error.unsupported.xml.missingtag", ficheXpath + "/geoloc", "lon");
                }
                if (done) {
                    done = ficheDataBuilder.setGeoloc(latitude, longitude);
                    if (!done) {
                        SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", ficheXpath + "/geoloc/[lat OR lon]", latitude + " / " + longitude);
                    }
                }
            } else if (subElementHandler instanceof AttrHandler) {
                AttrHandler attrHandler = (AttrHandler) subElementHandler;
                ficheDataBuilder.addAttribute(attrHandler.getAttributeKey(), attrHandler.getValueList());
            } else if (subElementHandler instanceof SimpleElementHandler) {
                SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
                closeSimpleSubElement(simpleElementHandler.getType(), simpleElementHandler.getText());
            }
        }

        private void closeSimpleSubElement(int type, String text) {
            switch (type) {
                case TITRE:
                    ficheDataBuilder.setTitre(text);
                    break;
                case SOUSTITRE:
                    ficheDataBuilder.setSoustitre(text);
                    break;
                case DATE:
                    try {
                    if ((text != null) && (text.length() != 0)) {
                        ficheDataBuilder.setDate(FuzzyDate.parse(text));
                    }
                } catch (ParseException pe) {
                    SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", ficheXpath + "/date", text);
                }
                break;
                case LANG:
                    try {
                    if ((text != null) && (text.length() != 0)) {
                        text = SctXmlUtils.checkLangAlias(text);
                        ficheDataBuilder.setLang(Lang.parse(text));
                    }
                } catch (ParseException pe) {
                    SctXmlUtils.lang(messageHandler, "_ error.wrong.xml.tagvalue", ficheXpath + "/lang", text);
                }
                break;
                case HREF:
                    boolean hrefDone = false;
                    try {
                        hrefDone = ficheDataBuilder.setHref(SctXmlUtils.testURI(text));
                    } catch (MalformedURLException | URISyntaxException e) {

                    }
                    if (!hrefDone) {
                        SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", ficheXpath + "/href", text);
                    }
                    break;
                case COMPLEMENT:
                    boolean done = ficheDataBuilder.addComplement(text);
                    if (!done) {
                        SctXmlUtils.xml(messageHandler, "_ error.unsupported.xml.toomanytags", ficheXpath + "/complement");
                    }
                    break;
                case FICHE_ICON:
                    boolean ficheIconDone = false;
                    try {
                        ficheIconDone = ficheDataBuilder.setFicheIcon(SctXmlUtils.testURI(text));
                    } catch (MalformedURLException | URISyntaxException e) {

                    }
                    if (!ficheIconDone) {
                        SctXmlUtils.format(messageHandler, "_ error.wrong.xml.tagvalue", ficheXpath + "/fiche-icon", text);
                    }
                    break;
            }
        }

    }


    private class GeolocHandler extends StackElementHandler {

        private String latitude;
        private String longitude;

        private GeolocHandler() {
        }

        @Override
        public ElementHandler newSubHandler(String tagname, Attributes attributes) {
            switch (tagname) {
                case "lat":
                    return new SimpleElementHandler(LATITUDE);
                case "lon":
                    return new SimpleElementHandler(LONGITUDE);
                default:
                    return null;
            }
        }

        @Override
        public void closeSubHandler(ElementHandler subElementHandler) {
            if (subElementHandler instanceof SimpleElementHandler) {
                SimpleElementHandler simpleElementHandler = (SimpleElementHandler) subElementHandler;
                int type = simpleElementHandler.getType();
                String text = simpleElementHandler.getText();
                switch (type) {
                    case LATITUDE:
                        latitude = text;
                        break;
                    case LONGITUDE:
                        longitude = text;
                        break;
                }
            }
        }

    }


}
