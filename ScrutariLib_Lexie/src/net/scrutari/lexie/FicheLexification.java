/* ScrutariLib_Lexie - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;


/**
 *
 * @author Vincent Calame
 */
public class FicheLexification {

    private final Map<AlineaRank, Alinea> map = new HashMap<AlineaRank, Alinea>();

    public FicheLexification() {
    }

    public Alinea getAlinea(AlineaRank alineaRank) {
        return map.get(alineaRank);
    }

    public void putAlinea(AlineaRank alineaRank, Alinea lexification) {
        map.put(alineaRank, lexification);
    }

    public void clear() {
        map.clear();
    }

    public static void fromPrimitives(PrimitivesReader primitivesReader, FicheLexification ficheLexification) throws IOException {
        int size = primitivesReader.readInt();
        for (int i = 0; i < size; i++) {
            AlineaRank alineaRank = AlineaRank.build(primitivesReader.readInt());
            Alinea alinea = Alinea.fromPrimitives(primitivesReader);
            ficheLexification.putAlinea(alineaRank, alinea);
        }
    }

    public static void toPrimitives(FicheLexification ficheLexification, PrimitivesWriter primitivesWriter) throws IOException {
        Map<AlineaRank, Alinea> map = ficheLexification.map;
        int size = map.size();
        primitivesWriter.writeInt(size);
        for (Map.Entry<AlineaRank, Alinea> entry : map.entrySet()) {
            primitivesWriter.writeInt(entry.getKey().getValue());
            Alinea.toPrimitives(entry.getValue(), primitivesWriter);
        }
    }

}
