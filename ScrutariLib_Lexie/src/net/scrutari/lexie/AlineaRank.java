/* ScrutariLib_Lexie - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public final class AlineaRank implements Comparable<AlineaRank> {

    private final static Map<Integer, AlineaRank> internMap = new HashMap<Integer, AlineaRank>();
    private final int value;

    private AlineaRank(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getType() {
        return FieldRank.getType(getFieldRankValue());
    }

    public int getMainOrder() {
        return FieldRank.getMainOrder(getFieldRankValue());
    }

    public int getFieldRankValue() {
        return (value - 1) / 10000;
    }

    public FieldRank getFieldRank() {
        return FieldRank.build(getFieldRankValue());
    }

    public boolean isAttribute() {
        return FieldRank.isAttribute(getFieldRankValue());
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        AlineaRank otherAlineaRank = (AlineaRank) other;
        return (otherAlineaRank.value == this.value);
    }

    @Override
    public int compareTo(AlineaRank other) {
        int otherValue = other.value;
        if (value < otherValue) {
            return -1;
        }
        if (value > otherValue) {
            return 1;
        }
        return 0;
    }

    public static AlineaRank build(int value) {
        AlineaRank alineaRank = internMap.get(value);
        if (alineaRank != null) {
            return alineaRank;
        }
        alineaRank = new AlineaRank(value);
        intern(alineaRank);
        return alineaRank;
    }

    public static AlineaRank build(FieldRank fieldRank, int alineaPosition) {
        return build(computeValue(fieldRank, alineaPosition));
    }

    private static int computeValue(FieldRank fieldRank, int alineaPosition) {
        return fieldRank.getValue() * 10000 + alineaPosition;
    }

    private synchronized static void intern(AlineaRank alineaRank) {
        internMap.put(alineaRank.getValue(), alineaRank);
    }

}
