/* ScrutariLib_Lexie - Copyright (c) 2008-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.io.IOException;
import java.util.AbstractList;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.SubstringPosition;
import net.mapeadores.util.text.collation.CollationUnitPosition;


/**
 *
 * @author Vincent Calame
 */
public final class LexieUtils {

    public final static List<ScrutariLexieUnit> EMPTY_SCRUTARILEXIEUNITLIST = Collections.emptyList();
    public final static List<LexieOccurrences> EMPTY_LEXIEOCCURRENCESLIST = Collections.emptyList();

    private LexieUtils() {
    }

    public static void writeCollationUnitPosition(CollationUnitPosition collationUnitPosition, PrimitivesWriter primitivesWriter) throws IOException {
        SubstringPosition sp = collationUnitPosition.getPositionInWholeSource();
        primitivesWriter.writeInt(sp.getBeginIndex());
        primitivesWriter.writeInt(sp.getLength());
        primitivesWriter.writeIntArray(collationUnitPosition.getCorrespondances());
    }

    public static CollationUnitPosition readCollationUnitPosition(PrimitivesReader primitivesReader) throws IOException {
        int beginIndex = primitivesReader.readInt();
        int length = primitivesReader.readInt();
        int[] correspondances = primitivesReader.readIntArray();
        return new CollationUnitPosition(new SubstringPosition(beginIndex, length), correspondances);
    }

    public static void skipCollationUnitPosition(PrimitivesReader primitivesReader) throws IOException {
        primitivesReader.skipInts(2);
        primitivesReader.skipIntArray();
    }

    public static List<ScrutariLexieUnit> toScrutariLexieUnitList(ScrutariLexieUnit[] scrutariLexieUnitArray) {
        if (scrutariLexieUnitArray == null) {
            return EMPTY_SCRUTARILEXIEUNITLIST;
        }
        if (scrutariLexieUnitArray.length == 0) {
            return EMPTY_SCRUTARILEXIEUNITLIST;
        }
        return new ScrutariLexieUnitList(scrutariLexieUnitArray);
    }

    public static List<ScrutariLexieUnit> toScrutariLexieUnitList(ScrutariLexieUnit scrutariLexieUnit) {
        if (scrutariLexieUnit == null) {
            return EMPTY_SCRUTARILEXIEUNITLIST;
        }
        return Collections.singletonList(scrutariLexieUnit);
    }

    public static List<LexieOccurrences> wrap(LexieOccurrences[] lexieOccurrencesArray) {
        if (lexieOccurrencesArray == null) {
            return EMPTY_LEXIEOCCURRENCESLIST;
        }
        if (lexieOccurrencesArray.length == 0) {
            return EMPTY_LEXIEOCCURRENCESLIST;
        }
        return new LexieOccurrencesList(lexieOccurrencesArray);
    }

    public static List<LexieOccurrences> wrap(LexieOccurrences lexieOccurrences) {
        if (lexieOccurrences == null) {
            return EMPTY_LEXIEOCCURRENCESLIST;
        }
        return Collections.singletonList(lexieOccurrences);
    }

    public static ScrutariLexieUnit toScrutariLexieUnit(LexieId lexieId, Lang lang, String collatedLexie, String canonicalLexie) {
        return new InternalScrutariLexieUnit(lexieId, lang, collatedLexie, canonicalLexie);
    }

    public static ScrutariLexieUnit derive(ScrutariLexieUnit origin, String canonicalLexie) {
        return new InternalScrutariLexieUnit(origin.getLexieId(), origin.getLang(), origin.getCollatedLexie(), canonicalLexie);
    }

    public static ScrutariLexieUnit readScrutariLexieUnit(PrimitivesReader primitivesReader) throws IOException {
        LexieId lexieId = new LexieId(primitivesReader.readLong());
        Lang lang = Lang.build(primitivesReader.readString());
        String collatedLexie = primitivesReader.readString();
        String canonicalLexie = primitivesReader.readString();
        return toScrutariLexieUnit(lexieId, lang, collatedLexie, canonicalLexie);
    }

    public static void writeScrutariLexieUnit(ScrutariLexieUnit scrutariLexieUnit, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeLong(scrutariLexieUnit.getLexieId().getValue());
        primitivesWriter.writeString(scrutariLexieUnit.getLang().toString());
        primitivesWriter.writeString(scrutariLexieUnit.getCollatedLexie());
        primitivesWriter.writeString(scrutariLexieUnit.getCanonicalLexie());
    }


    private static class LexieOccurrencesList extends AbstractList<LexieOccurrences> implements RandomAccess {

        private final LexieOccurrences[] array;

        private LexieOccurrencesList(LexieOccurrences[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public LexieOccurrences get(int index) {
            return array[index];
        }

    }


    private static class ScrutariLexieUnitList extends AbstractList<ScrutariLexieUnit> implements RandomAccess {

        private final ScrutariLexieUnit[] array;

        private ScrutariLexieUnitList(ScrutariLexieUnit[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ScrutariLexieUnit get(int index) {
            return array[index];
        }

    }


    private static class InternalScrutariLexieUnit implements ScrutariLexieUnit {

        private final LexieId lexieId;
        private final Lang lang;
        private final String collatedLexie;
        private final String canonicalLexie;

        private InternalScrutariLexieUnit(LexieId lexieId, Lang lang, String collatedLexie, String canonicalLexie) {
            this.lexieId = lexieId;
            this.lang = lang;
            this.collatedLexie = collatedLexie;
            this.canonicalLexie = canonicalLexie;
        }

        @Override
        public LexieId getLexieId() {
            return lexieId;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public String getCollatedLexie() {
            return collatedLexie;

        }

        @Override
        public String getCanonicalLexie() {
            return canonicalLexie;
        }

    }

}
