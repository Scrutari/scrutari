/* ScrutariLib_Lexie - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public final class FieldRank implements Comparable<FieldRank> {

    private final static Map<Integer, FieldRank> internMap = new HashMap<Integer, FieldRank>();
    public final static int TITRE_TYPE = 1;
    public final static int SOUSTITRE_TYPE = 2;
    public final static int COMPLEMENT_TYPE = 3;
    public final static int PRIMARY_TYPE = 4;
    public final static int SECONDARY_TYPE = 5;
    public final static int MOTCLES_TYPE = 6;
    public final static int TECHNICAL_TYPE = -1;
    public final static int UNKNOWN_TYPE = -9;
    public final static FieldRank TECHNICAL_ATTRIBUTE = new FieldRank(-1);
    public final static FieldRank UNKNOWN_ATTRIBUTE = new FieldRank(-9);
    private final int value;

    private FieldRank(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public boolean isTextField() {
        return (value > 0);
    }

    public int getType() {
        return getType(value);
    }

    @Override
    public int hashCode() {
        return value;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        FieldRank otherFieldRank = (FieldRank) other;
        return (otherFieldRank.value == this.value);
    }

    @Override
    public int compareTo(FieldRank other) {
        int otherValue = other.value;
        if (value < otherValue) {
            return -1;
        }
        if (value > otherValue) {
            return 1;
        }
        return 0;
    }

    public static FieldRank build(int value) {
        if (value < 0) {
            switch (value) {
                case -1:
                    return TECHNICAL_ATTRIBUTE;
                default:
                    return UNKNOWN_ATTRIBUTE;
            }
        }
        FieldRank fieldRank = internMap.get(value);
        if (fieldRank != null) {
            return fieldRank;
        }
        fieldRank = new FieldRank(value);
        intern(fieldRank);
        return fieldRank;
    }

    public static FieldRank build(int mainOrder, int position, int type) {
        return build(computeValue(mainOrder, position, type));
    }

    public static int getMainOrder(int fieldRankValue) {
        if (fieldRankValue < 0) {
            return 0;
        }
        return fieldRankValue / 10000;
    }

    public static int getType(int fieldRankValue) {
        if (fieldRankValue < 0) {
            switch (fieldRankValue) {
                case -1:
                    return TECHNICAL_TYPE;
                default:
                    return UNKNOWN_TYPE;

            }
        }
        return fieldRankValue % 10;
    }

    public static boolean isAttribute(int fieldRankValue) {
        switch (getType(fieldRankValue)) {
            case PRIMARY_TYPE:
            case SECONDARY_TYPE:
                return true;
            default:
                return false;
        }
    }

    private static int computeValue(int mainOrder, int position, int type) {
        return mainOrder * 10000 + position * 10 + type;
    }

    private synchronized static void intern(FieldRank fieldRank) {
        internMap.put(fieldRank.getValue(), fieldRank);
    }

}
