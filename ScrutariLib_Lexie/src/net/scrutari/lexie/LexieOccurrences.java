/* ScrutariLib_Lexie - Copyright (c) 2011-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.io.IOException;
import java.util.AbstractList;
import java.util.RandomAccess;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.collation.CollationUnitPosition;


/**
 *
 * @author Vincent Calame
 */
public class LexieOccurrences extends AbstractList<AlineaOccurrence> implements RandomAccess {

    private final Integer code;
    private final AlineaOccurrence[] alineaOccurrenceArray;

    public LexieOccurrences(Integer code, AlineaOccurrence[] alineaOccurrenceArray) {
        this.code = code;
        this.alineaOccurrenceArray = alineaOccurrenceArray;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public int size() {
        return alineaOccurrenceArray.length;
    }

    @Override
    public AlineaOccurrence get(int i) {
        return alineaOccurrenceArray[i];
    }

    public static LexieOccurrences fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        int code = primitivesReader.readInt();
        return fromPrimitives(primitivesReader, code);
    }

    public static LexieOccurrences fromPrimitives(PrimitivesReader primitivesReader, int code) throws IOException {
        int length = primitivesReader.readInt();
        AlineaOccurrence[] alineaOccurrenceArray = new AlineaOccurrence[length];
        for (int i = 0; i < length; i++) {
            AlineaRank alineaRank = AlineaRank.build(primitivesReader.readInt());
            CollationUnitPosition bestPositionInfield = LexieUtils.readCollationUnitPosition(primitivesReader);
            int alineaWholeLength = primitivesReader.readInt();
            int[] array = primitivesReader.readIntArray();
            alineaOccurrenceArray[i] = new AlineaOccurrence(alineaRank, bestPositionInfield, alineaWholeLength, array);
        }
        return new LexieOccurrences(code, alineaOccurrenceArray);
    }

    public static void skipAfterCode(PrimitivesReader primitivesReader) throws IOException {
        int length = primitivesReader.readInt();
        for (int i = 0; i < length; i++) {
            primitivesReader.skipInts(1);
            LexieUtils.skipCollationUnitPosition(primitivesReader);
            primitivesReader.skipInts(1);
            primitivesReader.skipIntArray();
        }
    }

    public static void toPrimitives(LexieOccurrences lexieOccurrences, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(lexieOccurrences.getCode());
        primitivesWriter.writeInt(lexieOccurrences.alineaOccurrenceArray.length);
        for (AlineaOccurrence alineaOccurrence : lexieOccurrences.alineaOccurrenceArray) {
            primitivesWriter.writeInt(alineaOccurrence.getAlineaRank().getValue());
            LexieUtils.writeCollationUnitPosition(alineaOccurrence.getBestPositionInAlinea(), primitivesWriter);
            primitivesWriter.writeInt(alineaOccurrence.getAlineaWholeLength());
            primitivesWriter.writeIntArray(alineaOccurrence.getLexieIndices());
        }
    }

}
