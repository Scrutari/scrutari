/* ScrutariLib_Lexie - Copyright (c) 2008-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.io.IOException;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.collation.CollationUnitPosition;


/**
 *
 * @author Vincent Calame
 */
public class Alinea {

    private LexieId[] lexieIdArray;
    private CollationUnitPosition[] collationUnitPositionArray;
    private int wholeLength;

    public Alinea() {
    }

    public int getLexieCount() {
        return lexieIdArray.length;
    }

    public LexieId getLexieId(int i) {
        return lexieIdArray[i];
    }

    public CollationUnitPosition getCollationUnitPosition(int i) {
        return collationUnitPositionArray[i];
    }

    public void setLexieIdArray(LexieId[] lexieIdArray) {
        this.lexieIdArray = lexieIdArray;
    }

    public void setCollationUnitPositionArray(CollationUnitPosition[] collationUnitPositionArray) {
        this.collationUnitPositionArray = collationUnitPositionArray;
    }

    public int getWholeLength() {
        return wholeLength;
    }

    public void setWholeLength(int wholeLength) {
        this.wholeLength = wholeLength;
    }

    public static Alinea fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        Alinea alinea = new Alinea();
        alinea.setWholeLength(primitivesReader.readInt());
        int length = primitivesReader.readInt();
        LexieId[] lexieIdArray = new LexieId[length];
        CollationUnitPosition[] collationUnitPositionArray = new CollationUnitPosition[length];
        for (int i = 0; i < length; i++) {
            lexieIdArray[i] = new LexieId(primitivesReader.readLong());
            collationUnitPositionArray[i] = LexieUtils.readCollationUnitPosition(primitivesReader);
        }
        alinea.setLexieIdArray(lexieIdArray);
        alinea.setCollationUnitPositionArray(collationUnitPositionArray);
        return alinea;
    }

    public static void toPrimitives(Alinea alinea, PrimitivesWriter primitivesWriter) throws IOException {
        primitivesWriter.writeInt(alinea.getWholeLength());
        int count = alinea.getLexieCount();
        primitivesWriter.writeInt(count);
        for (int i = 0; i < count; i++) {
            primitivesWriter.writeLong(alinea.getLexieId(i).getValue());
            LexieUtils.writeCollationUnitPosition(alinea.getCollationUnitPosition(i), primitivesWriter);
        }
    }

}
