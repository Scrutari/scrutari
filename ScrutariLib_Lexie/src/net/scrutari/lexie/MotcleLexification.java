/* ScrutariLib_Lexie - Copyright (c) 2008-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;


/**
 *
 * @author Vincent Calame
 */
public class MotcleLexification {

    private final Map<Lang, Alinea> map = new HashMap<Lang, Alinea>();

    public MotcleLexification() {
    }

    public Alinea getAlinea(Lang lang) {
        return map.get(lang);
    }

    public void putAlinea(Lang lang, Alinea alinea) {
        map.put(lang, alinea);
    }

    public void clear() {
        map.clear();
    }

    public static void fromPrimitives(PrimitivesReader primitivesReader, MotcleLexification motcleLexification) throws IOException {
        int size = primitivesReader.readInt();
        for (int i = 0; i < size; i++) {
            Lang lang = Lang.build(primitivesReader.readString());
            Alinea alinea = Alinea.fromPrimitives(primitivesReader);
            motcleLexification.putAlinea(lang, alinea);
        }
    }

    public static void toPrimitives(MotcleLexification motcleLexification, PrimitivesWriter primitivesWriter) throws IOException {
        Map<Lang, Alinea> map = motcleLexification.map;
        int size = map.size();
        primitivesWriter.writeInt(size);
        for (Map.Entry<Lang, Alinea> entry : map.entrySet()) {
            primitivesWriter.writeString(entry.getKey().toString());
            Alinea.toPrimitives(entry.getValue(), primitivesWriter);
        }
    }

}
