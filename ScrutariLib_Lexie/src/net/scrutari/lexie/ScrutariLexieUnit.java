/* ScrutariLib_Lexie - Copyright (c) 2005-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.lexie.LexieUnit;


/**
 * Point particulier des unités lexicales : elles correspondent à une langue
 * donnée.
 *
 * @author Vincent Calame
 */
public interface ScrutariLexieUnit extends LexieUnit {

    public Lang getLang();

    public LexieId getLexieId();

}
