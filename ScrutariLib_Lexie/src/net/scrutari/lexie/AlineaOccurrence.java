/* ScrutariLib_Lexie - Copyright (c) 2018-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.lexie;

import net.mapeadores.util.text.collation.CollationUnitPosition;


/**
 *
 * @author Vincent Calame
 */
public class AlineaOccurrence {

    private final AlineaRank alineaRank;
    private final CollationUnitPosition bestPositionInAlinea;
    private final int alineaWholeLength;
    private final int[] lexieIndices;

    public AlineaOccurrence(AlineaRank alineaRank, CollationUnitPosition bestPositionInAlinea, int alineaWholeLength, int[] lexieIndices) {
        this.alineaRank = alineaRank;
        this.bestPositionInAlinea = bestPositionInAlinea;
        this.alineaWholeLength = alineaWholeLength;
        this.lexieIndices = lexieIndices;
    }

    public AlineaRank getAlineaRank() {
        return alineaRank;
    }

    public CollationUnitPosition getBestPositionInAlinea() {
        return bestPositionInAlinea;
    }

    public int getAlineaWholeLength() {
        return alineaWholeLength;
    }

    public int[] getLexieIndices() {
        return lexieIndices;
    }

}
