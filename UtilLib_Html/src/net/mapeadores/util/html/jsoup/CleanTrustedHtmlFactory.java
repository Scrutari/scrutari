/* UtilLib_Html - Copyright (c) 2017-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.html.jsoup;

import net.mapeadores.util.html.TrustedHtmlFactory;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;


/**
 *
 * @author Vincent Calame
 */
public class CleanTrustedHtmlFactory extends TrustedHtmlFactory {

    public final static CleanTrustedHtmlFactory NONE = new CleanTrustedHtmlFactory(Safelist.none());
    public final static CleanTrustedHtmlFactory SIMPLE_TEXT = new CleanTrustedHtmlFactory(Safelist.simpleText());
    public final static CleanTrustedHtmlFactory BASIC = new CleanTrustedHtmlFactory(Safelist.basic());
    public final static CleanTrustedHtmlFactory BASIC_WITH_IMAGES = new CleanTrustedHtmlFactory(Safelist.basicWithImages());
    public final static CleanTrustedHtmlFactory RELAXED = new CleanTrustedHtmlFactory(Safelist.relaxed());
    public final static CleanTrustedHtmlFactory EXTENDED = new CleanTrustedHtmlFactory(extended());
    public final static CleanTrustedHtmlFactory EXTENDED_WITH_STYLE = new CleanTrustedHtmlFactory(extendedWithStyle());
    private final Safelist safeList;

    public CleanTrustedHtmlFactory(Safelist safelist) {
        this.safeList = safelist;
    }

    @Override
    public String check(String html) {
        return Jsoup.clean(html, safeList);
    }

    public static Safelist extended() {
        return Safelist.relaxed()
                .addTags("hr", "iframe", "video", "audio", "source")
                .addAttributes("iframe", "src", "name", "allowfullscreen", "height", "width", "frameborder", "scrolling", "marginwidth", "marginheight")
                .addAttributes("video", "src", "crossorigin", "poster", "preload", "autoplay", "mediagroup", "loop", "muted", "controls", "height", "width")
                .addAttributes("audio", "src", "crossorigin", "preload", "autoplay", "mediagroup", "loop", "controls")
                .addAttributes("source", "src", "type")
                .addAttributes("a", "target")
                .addProtocols("iframe", "src", "http", "https")
                .addProtocols("video", "src", "http", "https")
                .addProtocols("audio", "src", "http", "https")
                .addProtocols("source", "src", "http", "https")
                .addAttributes(":all", "id", "class", "lang");
    }

    public static Safelist extendedWithStyle() {
        return extended()
                .addAttributes(":all", "style");
    }


}
