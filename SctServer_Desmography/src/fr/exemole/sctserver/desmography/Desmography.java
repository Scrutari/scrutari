/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography;

import fr.exemole.sctserver.request.ParameterErrorMessageKeys;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.request.WarningHandler;
import java.util.Collection;
import java.util.List;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.request.RequestMap;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.DataInfo;


/**
 *
 * @author Vincent Calame
 */
public class Desmography {

    private final ThesaurusData[] array;
    private final DataInfo[] infoArray;

    public Desmography(ThesaurusData[] array, DataInfo[] infoArray) {
        this.array = array;
        this.infoArray = infoArray;
    }

    public ThesaurusData[] getThesaurusDataArray() {
        return array;
    }

    public DataInfo[] getThesaurusInfoArray() {
        return infoArray;
    }

    public List<Integer> getThesaurusCodeList() {
        int length = array.length;
        Integer[] result = new Integer[length];
        for (int i = 0; i < length; i++) {
            result[i] = array[i].getThesaurusCode();
        }
        return PrimUtils.wrap(result);
    }

    public static Desmography build(DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler) throws ErrorMessageException {
        String desmographyName = RequestMapUtils.getMandatoryParam(DesmographyParameters.DESMOGRAPHY, requestMap);
        warningHandler.setCurrentParameter(DesmographyParameters.DESMOGRAPHY);
        Collection<Integer> codes = RequestMapUtils.toCodes(dataAccess, desmographyName, warningHandler, ScrutariDataURI.THESAURUSURI_TYPE);
        if ((codes == null) || (codes.isEmpty())) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, DesmographyParameters.DESMOGRAPHY, desmographyName);
        }
        int p = 0;
        ThesaurusData[] array = new ThesaurusData[codes.size()];
        DataInfo[] infoArray = new DataInfo[codes.size()];
        for (Integer thesaurusCode : codes) {
            array[p] = dataAccess.getThesaurusData(thesaurusCode);
            infoArray[p] = dataAccess.getDataInfo(thesaurusCode);
            p++;
        }
        return new Desmography(array, infoArray);
    }

}
