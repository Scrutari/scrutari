/* SctServer_Desmography - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.gexf;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.desmography.Desmography;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.WarningHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import net.mapeadores.util.xml.XmlProducer;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.data.SctSpace;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public final class GexfXmlProducerFactory {

    private GexfXmlProducerFactory() {

    }

    public static XmlProducer getXmlProducer(ScrutariSession scrutariSession, RequestMap requestMap) {
        WarningHandler warningHandler = new WarningHandler();
        Desmography desmography;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
            Lang lang = DesmographyUtils.getLang(requestMap);
            return new GexfXmlProducer(desmography, dataAccess, lang);
        } catch (ErrorMessageException eme) {
            return new ErrorXmlProducer(eme.getErrorMessage());
        }
    }


    private static class GexfXmlProducer implements XmlProducer {

        private final Desmography desmography;
        private final DataAccess dataAccess;
        private final Lang lang;

        private GexfXmlProducer(Desmography desmography, DataAccess dataAccess, Lang lang) {
            this.desmography = desmography;
            this.dataAccess = dataAccess;
            this.lang = lang;
        }

        @Override
        public void writeXml(Appendable appendable) throws IOException {
            String separator = " / ";
            Set<RelationData> set = new HashSet<RelationData>();
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(appendable, 0);
            xmlWriter
                    .appendXMLDeclaration();
            xmlWriter
                    .startOpenTag("gexf")
                    .addAttribute("xmlns", "http://gexf.net/1.3")
                    .addAttribute("xmlns:viz", "http://gexf.net/1.3/viz")
                    .addAttribute("version", "1.3")
                    .endOpenTag()
                    .startOpenTag("graph")
                    .addAttribute("mode", "static")
                    .addAttribute("defaultedgetype", "directed")
                    .endOpenTag();
            xmlWriter
                    .openTag("nodes");
            for (ThesaurusData thesaurusData : desmography.getThesaurusDataArray()) {
                for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
                    MotcleInfo motcleInfo = dataAccess.getMotcleInfo(motcleCode);
                    MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                    Attribute aliasAttribute = motcleData.getAttributes().getAttribute(SctSpace.ALIAS_KEY);
                    Label label = motcleData.getLabels().getLangPartCheckedLabel(lang);
                    String title = motcleData.getAttributes().getFirstValue(SctSpace.ALIAS_KEY);
                    if (title == null) {
                        title = "";
                    }
                    if (label != null) {
                        if (title.length() > 0) {
                            title = title + separator + label.getLabelString();
                        } else {
                            title = label.getLabelString();
                        }
                    }
                    xmlWriter
                            .startOpenTag("node")
                            .addAttribute("id", motcleCode)
                            .addAttribute("label", title)
                            .endOpenTag();
                    if (aliasAttribute != null) {
                        xmlWriter
                                .startOpenTag("viz:shape")
                                .addAttribute("value", "square")
                                .closeEmptyTag();
                        xmlWriter
                                .startOpenTag("viz:color")
                                .addAttribute("hex", "#ff0000")
                                .closeEmptyTag();
                    }
                    xmlWriter
                            .closeTag("node");
                    List<RelationData> relationList = motcleInfo.getRelationList("hierarchy", "superior");
                    for (RelationData relationData : relationList) {
                        set.add(relationData);
                    }
                }
            }
            xmlWriter
                    .closeTag("nodes");
            xmlWriter
                    .openTag("edges");
            for (RelationData relationData : set) {
                writeEdges(xmlWriter, relationData);
            }
            xmlWriter
                    .closeTag("edges");
            xmlWriter
                    .closeTag("graph")
                    .closeTag("gexf");
        }

        private void writeEdges(XMLWriter xmlWriter, RelationData relationData) throws IOException {
            List<Integer> superiorList = new ArrayList<Integer>();
            List<Integer> inferiorList = new ArrayList<Integer>();
            for (RelationData.Member member : relationData.getMemberList()) {
                switch (member.getRole()) {
                    case "superior":
                        superiorList.add(member.getCode());
                        break;
                    case "inferior":
                        inferiorList.add(member.getCode());
                        break;
                }
            }
            for (Integer superior : superiorList) {
                for (Integer inferior : inferiorList) {
                    xmlWriter
                            .startOpenTag("edge")
                            .addAttribute("source", superior)
                            .addAttribute("target", inferior)
                            .closeEmptyTag();
                }
            }
        }

    }


    private static class ErrorXmlProducer implements XmlProducer {

        private final CommandMessage commandMessage;

        private ErrorXmlProducer(CommandMessage commandMessage) {
            this.commandMessage = commandMessage;
        }

        @Override
        public void writeXml(Appendable appendable) throws IOException {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(appendable, 0);
            xmlWriter
                    .appendXMLDeclaration();
            xmlWriter
                    .startOpenTag("error")
                    .addAttribute("key", commandMessage.getMessageKey())
                    .closeEmptyTag();
        }

    }

}
