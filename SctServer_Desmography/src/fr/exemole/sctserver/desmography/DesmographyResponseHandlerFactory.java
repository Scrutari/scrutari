/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.desmography.gexf.GexfXmlProducerFactory;
import fr.exemole.sctserver.desmography.json.DesmographyJsonProducerFactory;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.handlers.JsonResponseHandler;
import net.mapeadores.util.servlets.handlers.XmlResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class DesmographyResponseHandlerFactory {

    private DesmographyResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        switch (relativePath.toLowerCase()) {
            case "json": {
                JsonProducer jsonProducer = DesmographyJsonProducerFactory.getJsonProducer(engine.getScrutariSession(), requestMap);
                return JsonResponseHandler.init(jsonProducer, requestMap.getParameter("callback"))
                        .accessControl(JsonResponseHandler.ALL_ORIGIN);
            }
            case "gexf": {
                return XmlResponseHandler.init(GexfXmlProducerFactory.getXmlProducer(engine.getScrutariSession(), requestMap));
            }
        }
        return null;
    }


}
