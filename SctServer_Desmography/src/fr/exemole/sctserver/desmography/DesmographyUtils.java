/* SctServer_Desmography - Copyright (c) 2023-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.request.Parameters;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.tools.EngineUtils;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.Langs;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.stats.LangStat;
import net.scrutari.db.api.stats.ScrutariDBStats;


/**
 *
 * @author Vincent Calame
 */
public final class DesmographyUtils {

    private DesmographyUtils() {

    }

    public static Lang getLang(RequestMap requestMap) throws ErrorMessageException {
        String langString = RequestMapUtils.getMandatoryParam(Parameters.LANG, requestMap);
        try {
            return Lang.parse(langString);
        } catch (ParseException pe) {
            throw new ErrorMessageException("_ error.wrong.lang", langString);
        }
    }

    public static Lang getPreferredLang(RequestMap requestMap, List<Lang> availableLangs) {
        String userLangsString = requestMap.getParameter(DesmographyParameters.USERLANGS);
        if (userLangsString == null) {
            return availableLangs.get(0);
        }
        Langs userLangs = LangsUtils.toCleanLangs(userLangsString);
        if (userLangs.isEmpty()) {
            return availableLangs.get(0);
        }
        for (Lang userLang : userLangs) {
            Lang lang = LangsUtils.checkLang(userLang, availableLangs);
            if (lang != null) {
                return lang;
            }
        }
        return availableLangs.get(0);
    }

    public static List<Lang> getAvailableLangList(Desmography desmography, ScrutariSession scrutariSession) {
        Set<Lang> result = new LinkedHashSet<Lang>();
        ScrutariDBStats scrutariDBStats = scrutariSession.getScrutariDB().getStats();
        for (ThesaurusData thesaurusData : desmography.getThesaurusDataArray()) {
            for (LangStat langStat : scrutariDBStats.getLangStats(thesaurusData.getThesaurusCode())) {
                result.add(langStat.getLang());
            }
        }
        if (result.isEmpty()) {
            result.add(EngineUtils.getDefaultLang(scrutariSession));
        }
        return new ArrayList<Lang>(result);
    }

    public static FieldVariant getFieldVariant(ScrutariSession scrutariSession) {
        return scrutariSession.getEngine().getFieldVariantManager().getFieldVariant("desmography");
    }

}
