/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography;


/**
 *
 * @author Vincent Calame
 */
public interface DesmographyParameters {

    public final static String TYPE = "type";
    public final static String WARNINGS = "warnings";
    public final static String DESMOGRAPHY = "desmography";
    public final static String VERSION = "version";
    public final static String LANG = "lang";
    public final static String USERLANGS = "userlangs";
    public final static String XML = "xml";
    public final static String TERM = "term";
    public final static String TERMS = "terms";
    public final static String RESOURCES = "resources";
    public final static String Q = "q";
    public final static String INCLUDE = "include";
}
