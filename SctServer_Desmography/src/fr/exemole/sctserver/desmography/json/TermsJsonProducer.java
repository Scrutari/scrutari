/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class TermsJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final Collection<Integer> terms;
    private final Lang lang;
    private final boolean withIndexation;

    public TermsJsonProducer(ScrutariSession scrutariSession, Collection<Integer> terms, Lang lang, boolean withIndexation) {
        this.scrutariSession = scrutariSession;
        this.terms = terms;
        this.lang = lang;
        this.withIndexation = withIndexation;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        FieldVariant desmographyFieldVariant = DesmographyUtils.getFieldVariant(scrutariSession);
        Extract extract = new Extract();
        List<Integer> refList = new ArrayList<Integer>();
        for (Integer termCode : terms) {
            MotcleInfo motcleInfo = dataAccess.getMotcleInfo(termCode);
            if (motcleInfo != null) {
                refList.add(extract.addTerm(motcleInfo));
            }
        }
        jw.key("term_refs");
        jw.array();
        for (Integer termIndex : refList) {
            jw.value(termIndex);
        }
        jw.endArray();
        TermJson.properties(jw, dataAccess, scrutariSession.getGlobalSearchOptions(), extract, lang, withIndexation);
        if (withIndexation) {
            ResourceJson.properties(jw, dataAccess, extract, lang, desmographyFieldVariant);
        }
    }

}
