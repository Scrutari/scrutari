/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.json.JsonUtils;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.tools.util.BaseCheck;
import net.scrutari.db.tools.util.BaseChecker;


/**
 *
 * @author Vincent Calame
 */
public final class ResourceJson {

    private ResourceJson() {

    }

    public static void properties(JSONWriter jw, DataAccess dataAccess, Extract extract, Lang lang) throws IOException {
        properties(jw, dataAccess, extract, lang, null);
    }

    public static void properties(JSONWriter jw, DataAccess dataAccess, Extract extract, Lang lang, FieldVariant fieldVariant) throws IOException {
        BaseChecker baseChecker = new BaseChecker(lang);
        jw.key("resources");
        jw.array();
        for (Iterator<FicheInfo> it = extract.getResourceIterator(); it.hasNext();) {
            FicheInfo ficheInfo = it.next();
            FicheData ficheData = ficheInfo.getFicheData(dataAccess);
            BaseCheck baseCheck = baseChecker.getBaseCheck(dataAccess, ficheData);
            jw.object();
            {
                jw.key("name")
                        .value(ficheInfo.getFicheURI().getPath());
                jw.key("title")
                        .value(ficheData.getTitre());
                String href = ficheData.getHref(baseCheck.getCheckedLang());
                if (href != null) {
                    jw.key("href")
                            .value(href);
                }
                if (fieldVariant != null) {
                    jw.key("properties");
                    jw.object();
                    List<FieldVariant.Alias> aliasList = fieldVariant.getFicheAliasList();
                    for (FieldVariant.Alias alias : aliasList) {
                        String aliasValue = JsonUtils.getAliasValue(alias, ficheData, baseCheck);
                        if (aliasValue.length() > 0) {
                            jw.key(alias.getName())
                                    .value(aliasValue);
                        }
                    }
                    jw.endObject();
                }
                List<Integer> indexationList = extract.getIndexationList(ficheInfo);
                if (indexationList != null) {
                    jw.key("indexation");
                    jw.object();
                    {
                        jw.key("_default");
                        jw.array();
                        for (Integer termIndex : indexationList) {
                            jw.value(termIndex);
                        }
                        jw.endArray();
                    }
                    jw.endObject();
                }
            }
            jw.endObject();
        }
        jw.endArray();
    }

}
