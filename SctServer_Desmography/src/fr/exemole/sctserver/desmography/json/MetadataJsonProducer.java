/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.desmography.Desmography;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import java.util.List;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Labels;
import net.scrutari.data.DataConstants;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.DataInfo;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class MetadataJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final Desmography desmography;
    private final Lang lang;
    private final List<Lang> availableLangList;

    public MetadataJsonProducer(ScrutariSession scrutariSession, Desmography desmography, Lang lang, List<Lang> availableLangList) {
        this.scrutariSession = scrutariSession;
        this.desmography = desmography;
        this.lang = lang;
        this.availableLangList = availableLangList;

    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        DataInfo thesaurusInfo = desmography.getThesaurusInfoArray()[0];
        ThesaurusData thesaurusData = desmography.getThesaurusDataArray()[0];
        Attributes attributes = thesaurusData.getAttributes();
        jw.key("lang")
                .value(lang.toString());
        Labels desmographyTitleLabels = thesaurusData.getPhrases().getPhrase("desmography.title");
        if (desmographyTitleLabels == null) {
            desmographyTitleLabels = thesaurusData.getPhrases().getPhrase(DataConstants.THESAURUS_TITLE);
        }
        if (desmographyTitleLabels != null) {
            TermJson.labelProperties(jw, desmographyTitleLabels, lang);
        }
        jw.key("langs");
        jw.array();
        for (Lang authorizedLang : availableLangList) {
            jw.value(authorizedLang.toString());
        }
        jw.endArray();
        TermJson.attributesProperties(jw, attributes);
        jw.key("home");
        jw.object();
        for (RelationData relation : thesaurusInfo.getRelationList("home", "desmography")) {
            boolean done = false;
            for (RelationData.Member member : relation.getMemberList()) {
                if (member.getRole().equals("root")) {
                    MotcleInfo motcleInfo = dataAccess.getMotcleInfo(member.getCode());
                    if (motcleInfo != null) {
                        jw.key("name")
                                .value(motcleInfo.getMotcleURI().getPath());
                        done = true;
                        break;
                    }
                }
            }
            if (done) {
                break;
            }
        }
        jw.endObject();
    }


}
