/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class FamiliesJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final Collection<Integer> terms;
    private final Lang lang;

    public FamiliesJsonProducer(ScrutariSession scrutariSession, Collection<Integer> terms, Lang lang) {
        this.scrutariSession = scrutariSession;
        this.terms = terms;
        this.lang = lang;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        Set<Integer> usedTermSet = new HashSet<Integer>(terms);
        jw.key("families");
        jw.array();
        for (Integer term : terms) {
            writeTerm(jw, dataAccess, term, usedTermSet);
        }
        jw.endArray();
    }

    private void writeTerm(JSONWriter jw, DataAccess dataAccess, Integer code, Set<Integer> usedTermSet) throws IOException {
        MotcleInfo motcleInfo = dataAccess.getMotcleInfo(code);
        if (motcleInfo == null) {
            return;
        }
        MotcleData motcleData = dataAccess.getMotcleData(code);
        jw.object();
        {
            TermJson.dataProperties(jw, motcleData, lang);
            TermJson.rosetteProperty(jw, motcleInfo, dataAccess);
            jw.key("subfamilies");
            jw.array();
            for (RelationData relationData : motcleInfo.getRelationList("subfamilies", "superior")) {
                for (RelationData.Member member : relationData.getMemberList()) {
                    if (member.getRole().equals("inferior")) {
                        Integer subsectorCode = member.getCode();
                        if (!usedTermSet.contains(subsectorCode)) {
                            usedTermSet.add(subsectorCode);
                            writeTerm(jw, dataAccess, subsectorCode, usedTermSet);
                        }
                    }
                }
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
