/* SctServer_Desmography - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.RelationData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class AxesJsonProducer extends AbstractJsonProducer {

    private final static Predicate<MotcleInfo> ALWAYS = new AlwaysPredicate();
    private final ScrutariSession scrutariSession;
    private final Collection<Integer> terms;
    private final String[] axes;
    private final Lang lang;
    private final boolean ignoreEmptyLeaf;


    public AxesJsonProducer(ScrutariSession scrutariSession, Collection<Integer> terms, String[] axes, Lang lang, boolean ignoreEmptyLeaf) {
        this.scrutariSession = scrutariSession;
        this.terms = terms;
        this.axes = axes;
        this.lang = lang;
        this.ignoreEmptyLeaf = ignoreEmptyLeaf;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        FieldVariant desmographyFieldVariant = DesmographyUtils.getFieldVariant(scrutariSession);
        Map<Integer, TermBuffer> existingMap = new LinkedHashMap<Integer, TermBuffer>();
        Extract extract = new Extract();
        jw.key("axes");
        jw.object();
        for (Integer code : terms) {
            MotcleInfo motcleInfo = dataAccess.getMotcleInfo(code);
            if (motcleInfo == null) {
                continue;
            }
            extract.addTerm(motcleInfo);
            jw.key(motcleInfo.getMotcleURI().getPath());
            jw.object();
            for (String axe : axes) {
                Predicate<MotcleInfo> predicate;
                List<RelationData> relationList;
                String otherRole;
                if (axe.equals("upwards")) {
                    relationList = motcleInfo.getRelationList("hierarchy", "inferior");
                    otherRole = "superior";
                    predicate = ALWAYS;
                } else if (axe.equals("downwards")) {
                    relationList = motcleInfo.getRelationList("hierarchy", "superior");
                    otherRole = "inferior";
                    if (ignoreEmptyLeaf) {
                        predicate = new NotEmptyLeafPredicate(dataAccess);
                    } else {
                        predicate = ALWAYS;
                    }
                } else {
                    break;
                }
                existingMap.clear();
                for (RelationData relationData : relationList) {
                    Integer relationIndex = extract.addRelation(relationData);
                    for (RelationData.Member member : relationData.getMemberList()) {
                        if (member.getRole().equals(otherRole)) {
                            Integer otherCode = member.getCode();
                            MotcleInfo otherInfo = dataAccess.getMotcleInfo(otherCode);
                            if (otherInfo != null) {
                                if (predicate.test(otherInfo)) {
                                    TermBuffer termBuffer = existingMap.get(otherCode);
                                    if (termBuffer == null) {
                                        Integer otherIndex = extract.addTerm(otherInfo);
                                        termBuffer = new TermBuffer(otherIndex);
                                        existingMap.put(otherCode, termBuffer);
                                    }
                                    termBuffer.addRelation(relationIndex);
                                }
                            }
                        }
                    }
                }
                jw.key(axe);
                jw.array();
                for (TermBuffer termBuffer : existingMap.values()) {
                    termBuffer.write(jw);
                }
                jw.endArray();
            }
            jw.endObject();
        }
        jw.endObject();
        RelationJson.properties(jw, dataAccess, extract);
        TermJson.properties(jw, dataAccess, scrutariSession.getGlobalSearchOptions(), extract, lang, true);
        ResourceJson.properties(jw, dataAccess, extract, lang, desmographyFieldVariant);
    }


    private static class TermBuffer {

        private final Integer termIndex;
        private final List<Integer> relationList = new ArrayList<Integer>();

        private TermBuffer(Integer termIndex) {
            this.termIndex = termIndex;
        }

        private void addRelation(Integer relationIndex) {
            relationList.add(relationIndex);
        }

        private void write(JSONWriter jw) throws IOException {
            jw.object();
            {
                jw.key("term_ref")
                        .value(termIndex);
                jw.key("relation_refs");
                jw.array();
                for (Integer relationIndex : relationList) {
                    jw.value(relationIndex);
                }
                jw.endArray();
            }
            jw.endObject();
        }

    }


    private static class AlwaysPredicate implements Predicate<MotcleInfo> {

        private AlwaysPredicate() {

        }

        @Override
        public boolean test(MotcleInfo motcleInfo) {
            return true;
        }

    }


    private static class NotEmptyLeafPredicate implements Predicate<MotcleInfo> {

        private final DataAccess dataAccess;
        private final Map<Integer, Boolean> bufferMap = new HashMap<Integer, Boolean>();

        private NotEmptyLeafPredicate(DataAccess dataAccess) {
            this.dataAccess = dataAccess;
        }

        @Override
        public boolean test(MotcleInfo motcleInfo) {
            return internalTest(motcleInfo, motcleInfo.getCode());
        }

        private boolean internalTest(MotcleInfo motcleInfo, Integer startCode) {
            Boolean bufferResult = bufferMap.get(motcleInfo.getCode());
            if (bufferResult != null) {
                return bufferResult;
            }
            boolean test = run(motcleInfo, startCode);
            bufferMap.put(motcleInfo.getCode(), test);
            return test;
        }

        private boolean run(MotcleInfo motcleInfo, Integer startCode) {
            if (motcleInfo.hasIndexation()) {
                return true;
            }
            List<RelationData> relationList = motcleInfo.getRelationList("hierarchy", "superior");
            if (relationList.isEmpty()) {
                return false;
            }
            for (RelationData relationData : relationList) {
                for (RelationData.Member member : relationData.getMemberList()) {
                    if (member.getRole().equals("inferior")) {
                        if (!member.getCode().equals(startCode)) {
                            MotcleInfo otherInfo = dataAccess.getMotcleInfo(member.getCode());
                            if (otherInfo != null) {
                                if (test(otherInfo)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

    }

}
