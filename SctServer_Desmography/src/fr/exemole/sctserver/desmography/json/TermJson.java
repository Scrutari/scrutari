/* BdfExt_EXM_Desmography - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.data.SctSpace;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.RoleKey;
import net.scrutari.db.tools.util.RelUtils;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;


/**
 *
 * @author Vincent Calame
 */
public final class TermJson {

    private final static RoleKey ROSETTEDEFAULT_ROOT = RoleKey.build("rosette.default", "root");
    private final static RoleKey FAMILY_INFERIOR = RoleKey.build("family", "inferior");

    private TermJson() {

    }

    public static void properties(JSONWriter jw, DataAccess dataAccess, GlobalSearchOptions globalSearchOptions, Extract extract, Lang lang, boolean withIndexation) throws IOException {
        Group[] groupArray = null;
        if (withIndexation) {
            groupArray = toGroupArray(dataAccess, globalSearchOptions);
        }
        jw.key("terms");
        jw.array();
        for (Iterator<MotcleInfo> it = extract.getTermIterator(); it.hasNext();) {
            MotcleInfo motcleInfo = it.next();
            jw.object();
            {
                dataProperties(jw, motcleInfo.getMotcleData(dataAccess), lang);
                familiesProperty(jw, motcleInfo, dataAccess, extract);
                rosetteProperty(jw, motcleInfo, dataAccess);
                if (withIndexation) {
                    indexationProperty(jw, motcleInfo, groupArray, dataAccess, extract, lang);
                }
            }
            jw.endObject();
        }
        jw.endArray();
    }

    public static void dataProperties(JSONWriter jw, MotcleData motcleData, Lang lang) throws IOException {
        Attributes attributes = motcleData.getAttributes();
        Attribute aliasAttribute = attributes.getAttribute(SctSpace.ALIAS_KEY);
        jw.key("name")
                .value(motcleData.getMotcleURI().getPath());
        if (aliasAttribute != null) {
            jw.key("alias")
                    .value(aliasAttribute.getFirstValue());
        }
        labelProperties(jw, motcleData.getLabels(), lang);
        attributesProperties(jw, attributes);
    }

    public static void attributesProperties(JSONWriter jw, Attributes attributes) throws IOException {
        jw.key("attributes");
        jw.object();
        for (Attribute attribute : attributes) {
            AttributeKey attributeKey = attribute.getAttributeKey();
            if (testNamespace(attributeKey.getNameSpace())) {
                jw.key(attributeKey.toString());
                jw.array();
                for (String value : attribute) {
                    jw.value(value);
                }
                jw.endArray();
            }
        }
        jw.endObject();
    }

    public static void familiesProperty(JSONWriter jw, MotcleInfo motcleInfo, DataAccess dataAccess, Extract extract) throws IOException {
        List<RelationData> list = motcleInfo.getRelationList(FAMILY_INFERIOR);
        jw.key("families");
        jw.array();
        for (RelationData relationData : list) {
            for (RelationData.Member member : relationData.getMemberList()) {
                if (member.getRole().equals("superior")) {
                    Integer familyCode = member.getCode();
                    MotcleInfo familyInfo = dataAccess.getMotcleInfo(familyCode);
                    if (familyInfo != null) {
                        jw.value(extract.addTerm(familyInfo));
                    }
                }
            }
        }
        jw.endArray();
    }

    public static void labelProperties(JSONWriter jw, Labels labels, Lang lang) throws IOException {
        Label label = labels.getLangPartCheckedLabel(lang);
        String title;
        if (label != null) {
            title = label.getLabelString();
        } else {
            title = "";
        }
        jw.key("title")
                .value(title);
        if (label != null) {
            if (labels.size() > 1) {
                jw.key("translations");
                jw.array();
                for (Label otherLabel : labels) {
                    if (!otherLabel.equals(label)) {
                        jw.object();
                        {
                            jw.key("lang")
                                    .value(otherLabel.getLang().toString());
                            jw.key("text")
                                    .value(otherLabel.getLabelString());
                        }
                        jw.endObject();
                    }
                }
                jw.endArray();
            }
        } else if (!labels.isEmpty()) {
            jw.key("translations");
            jw.array();
            for (Label otherLabel : labels) {
                jw.object();
                {
                    jw.key("lang")
                            .value(otherLabel.getLang().toString());
                    jw.key("text")
                            .value(otherLabel.getLabelString());
                }
                jw.endObject();
            }
            jw.endArray();
        }
    }

    public static void rosetteProperty(JSONWriter jw, MotcleInfo motcleInfo, DataAccess dataAccess) throws IOException {
        List<RelationData> list = motcleInfo.getRelationList(ROSETTEDEFAULT_ROOT);
        if (list.isEmpty()) {
            return;
        }
        jw.key("rosette");
        jw.object();
        for (RelationData relationData : list) {
            boolean done = false;
            for (RelationData.Member member : relationData.getMemberList()) {
                if (!member.getRole().equals("root")) {
                    Integer otherCode = member.getCode();
                    MotcleInfo otherInfo = dataAccess.getMotcleInfo(otherCode);
                    if (otherInfo != null) {
                        jw.key("default");
                        jw.object();
                        {
                            jw.key("mode")
                                    .value(member.getRole());
                            jw.key("name")
                                    .value(otherInfo.getMotcleURI().getPath());
                        }
                        jw.endObject();
                        done = true;
                        break;
                    }
                }
            }
            if (done) {
                break;
            }
        }
        jw.endObject();
    }

    public static boolean testNamespace(String namespace) {
        switch (namespace) {
            case "dsm":
            case "desmodojs":
                return true;
            default:
                if (namespace.startsWith("dsm.")) {
                    return true;
                } else {
                    return false;
                }
        }
    }

    public static void indexationProperty(JSONWriter jw, MotcleInfo motcleInfo, DataAccess dataAccess, GlobalSearchOptions globalSearchOptions, Extract extract, Lang lang) throws IOException {
        indexationProperty(jw, motcleInfo, toGroupArray(dataAccess, globalSearchOptions), dataAccess, extract, lang);
    }

    private static void indexationProperty(JSONWriter jw, MotcleInfo motcleInfo, Group[] groupArray, DataAccess dataAccess, Extract extract, Lang lang) throws IOException {
        jw.key("indexation");
        jw.object();
        for (Group group : groupArray) {
            boolean next = false;
            for (Integer corpusCode : group.corpusCodeArray) {
                Collection<Integer> list = motcleInfo.getIndexationCodeList(corpusCode);
                if (!list.isEmpty()) {
                    list = RelUtils.filterAlternate(dataAccess, lang, list);
                    if (!next) {
                        next = true;
                        jw.key(group.name);
                        jw.array();
                    }
                    for (Integer ficheCode : list) {
                        FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
                        jw.value(extract.addResource(ficheInfo));
                    }
                }
            }
            if (next) {
                jw.endArray();
            }
        }
        jw.endObject();
    }

    private static Group[] toGroupArray(DataAccess dataAccess, GlobalSearchOptions globalSearchOptions) {
        List<Category> categoryList = globalSearchOptions.getCategoryList();
        int size = categoryList.size();
        Group[] result;
        if (size == 0) {
            result = new Group[1];
            result[0] = new Group("_default", dataAccess.getCorpusCodeArray());
        } else {
            result = new Group[size];
            int p = 0;
            for (Category category : categoryList) {
                String name = category.getCategoryDef().getName();
                List<Integer> codeCorpusList = category.getCorpusCodeList();
                Integer[] corpusCodeArray = codeCorpusList.toArray(new Integer[codeCorpusList.size()]);
                result[p] = new Group(name, corpusCodeArray);
                p++;
            }
        }
        return result;
    }


    private static class Group {

        private final String name;
        private final Integer[] corpusCodeArray;

        private Group(String name, Integer[] corpusCodeArray) {
            this.name = name;
            this.corpusCodeArray = corpusCodeArray;
        }

    }

}
