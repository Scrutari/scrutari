/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.json.JSONWriter;
import net.scrutari.data.RelationData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public final class RelationJson {

    private RelationJson() {

    }

    public static void properties(JSONWriter jw, DataAccess dataAccess, Extract extract) throws IOException {
        jw.key("relations");
        jw.array();
        for (Iterator<RelationData> it = extract.getRelationIterator(); it.hasNext();) {
            RelationData relation = it.next();
            jw.object();
            RelationJson.properties(jw, dataAccess, relation, extract);
            jw.endObject();
        }
        jw.endArray();
    }

    public static void properties(JSONWriter jw, DataAccess dataAccess, RelationData relation, Extract extract) throws IOException {
        Map<String, List<Integer>> roleMap = new LinkedHashMap<String, List<Integer>>();
        for (RelationData.Member member : relation.getMemberList()) {
            Integer code = member.getCode();
            MotcleInfo motcleInfo = dataAccess.getMotcleInfo(code);
            if (motcleInfo != null) {
                String role = member.getRole();
                Integer termIndex = extract.addTerm(motcleInfo);
                List<Integer> indexList = roleMap.get(role);
                if (indexList == null) {
                    indexList = new ArrayList<Integer>();
                    roleMap.put(role, indexList);
                }
                indexList.add(termIndex);
            }
        }
        jw.key("type")
                .value(relation.getType());
        jw.key("roles");
        jw.object();
        for (Map.Entry<String, List<Integer>> entry : roleMap.entrySet()) {
            jw.key(entry.getKey());
            jw.array();
            for (Integer integerIndex : entry.getValue()) {
                jw.value(integerIndex);
            }
            jw.endArray();
        }
        jw.endObject();
    }

}
