/* SctServer_Desmography - Copyright (c) 2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.desmography.Desmography;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.RoleKey;


/**
 *
 * @author Vincent Calame
 */
public class AllJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final Lang lang;
    private final Desmography desmography;
    private final boolean withIndexation;

    public AllJsonProducer(ScrutariSession scrutariSession, Desmography desmography, Lang lang, boolean withIndexation) {
        this.scrutariSession = scrutariSession;
        this.desmography = desmography;
        this.lang = lang;
        this.withIndexation = withIndexation;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        Extract extract = new Extract();
        for (ThesaurusData thesaurusData : desmography.getThesaurusDataArray()) {
            for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
                MotcleInfo motcleInfo = dataAccess.getMotcleInfo(motcleCode);
                extract.addTerm(motcleInfo);
                for (RoleKey roleKey : motcleInfo.getAvalaibleRoleKeySet()) {
                    if (!roleKey.getRole().equals("inferior")) {
                        for (RelationData relationData : motcleInfo.getRelationList(roleKey)) {
                            extract.addRelation(relationData);
                        }
                    }
                }
            }
        }
        RelationJson.properties(jw, dataAccess, extract);
        TermJson.properties(jw, dataAccess, scrutariSession.getGlobalSearchOptions(), extract, lang, withIndexation);
        if (withIndexation) {
            FieldVariant desmographyFieldVariant = DesmographyUtils.getFieldVariant(scrutariSession);
            ResourceJson.properties(jw, dataAccess, extract, lang, desmographyFieldVariant);
        }
    }

}
