/* SctServer_Desmography - Copyright (c) 2020-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.desmography.Desmography;
import fr.exemole.sctserver.desmography.DesmographyParameters;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.Parameters;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.request.WarningHandler;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import fr.exemole.sctserver.request.json.ErrorJsonProducer;
import fr.exemole.sctserver.tools.EngineUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.data.RelationData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.DataInfo;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.searchengine.MotcleSearchEngine;
import net.scrutari.searchengine.api.operands.search.SearchOperation;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.api.result.MotcleSearchResult;
import net.scrutari.searchengine.api.result.MotcleSearchResultInfo;
import net.scrutari.searchengine.tools.options.SearchOptionsBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class DesmographyJsonProducerFactory {

    private DesmographyJsonProducerFactory() {

    }

    public static JsonProducer getJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap) {
        WarningHandler warningHandler = new WarningHandler();
        AbstractJsonProducer abstractJsonProducer;
        try {
            abstractJsonProducer = getJsonProducer(scrutariSession, requestMap, warningHandler);
        } catch (ErrorMessageException eme) {
            Lang lang = RequestMapUtils.getLang(requestMap, warningHandler);
            abstractJsonProducer = new ErrorJsonProducer(eme.getErrorMessage(), EngineUtils.getMessageLocalisation(scrutariSession.getEngine(), lang));
        }
        if ((!warningHandler.isEmpty()) && (requestMap.isTrue(DesmographyParameters.WARNINGS))) {
            abstractJsonProducer.setWarningHandler(warningHandler);
        }
        return abstractJsonProducer;
    }

    private static AbstractJsonProducer getJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, WarningHandler warningHandler) throws ErrorMessageException {
        String type = RequestMapUtils.getMandatoryParam(DesmographyParameters.TYPE, requestMap);
        String version = RequestMapUtils.getMandatoryParam(DesmographyParameters.VERSION, requestMap);
        switch (type) {
            case "all":
                return getAllJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "axes":
                return getAxesJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "families":
                return getFamiliesJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "grids":
                return getGridsJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "index":
                return getIndexJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "metadata":
                return getMetadataJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "resources":
                return getResourcesJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "summary":
                return getSummaryJsonProducer(scrutariSession, requestMap, version, warningHandler);
            case "terms":
                return getTermsJsonProducer(scrutariSession, requestMap, version, warningHandler);
            default:
                throw new ErrorMessageException("_ error.unknown.parametervalue", Parameters.TYPE, type);

        }
    }

    private static AbstractJsonProducer getAllJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Desmography desmography;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
        }
        Lang lang = DesmographyUtils.getLang(requestMap);
        return new AllJsonProducer(scrutariSession, desmography, lang, false);
    }

    private static AbstractJsonProducer getMetadataJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Desmography desmography;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
        }
        List<Lang> availableLangList = DesmographyUtils.getAvailableLangList(desmography, scrutariSession);
        Lang lang = DesmographyUtils.getPreferredLang(requestMap, availableLangList);
        return new MetadataJsonProducer(scrutariSession, desmography, lang, availableLangList);
    }

    private static AbstractJsonProducer getIndexJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Lang lang = DesmographyUtils.getLang(requestMap);
        /*Lang lang = getLang(requestMap);
        return new IndexJsonProducer(desmography, lang);*/
        return null;
    }

    private static AbstractJsonProducer getSummaryJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Lang lang = DesmographyUtils.getLang(requestMap);
        MotcleInfo motcleInfo;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            motcleInfo = getTerm(dataAccess, requestMap, warningHandler);
        }
        return new SummaryJsonProducer(scrutariSession, motcleInfo, lang);
    }

    private static AbstractJsonProducer getFamiliesJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Lang lang = DesmographyUtils.getLang(requestMap);
        Set<Integer> familySet = new LinkedHashSet<Integer>();
        Desmography desmography;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
            for (DataInfo thesaurusInfo : desmography.getThesaurusInfoArray()) {
                List<RelationData> relationDataList = thesaurusInfo.getRelationList("rootfamilies", "desmography");
                for (RelationData relationData : relationDataList) {
                    for (RelationData.Member member : relationData.getMemberList()) {
                        if (member.getRole().equals("inferior")) {
                            Integer code = member.getCode();
                            if (dataAccess.isMotcle(code)) {
                                familySet.add(member.getCode());
                            }
                        }
                    }
                }
            }
        }
        return new FamiliesJsonProducer(scrutariSession, familySet, lang);
    }

    private static AbstractJsonProducer getGridsJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Lang lang = DesmographyUtils.getLang(requestMap);
        Set<Integer> gridSet = new LinkedHashSet<Integer>();
        Desmography desmography;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
            for (DataInfo thesaurusInfo : desmography.getThesaurusInfoArray()) {
                List<RelationData> relationDataList = thesaurusInfo.getRelationList("grids", "desmography");
                for (RelationData relationData : relationDataList) {
                    for (RelationData.Member member : relationData.getMemberList()) {
                        if (member.getRole().equals("inferior")) {
                            Integer code = member.getCode();
                            if (dataAccess.isMotcle(code)) {
                                gridSet.add(member.getCode());
                            }
                        }
                    }
                }
            }
        }
        return new GridsJsonProducer(scrutariSession, gridSet, lang);
    }


    private static AbstractJsonProducer getAxesJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        String axesParam = RequestMapUtils.getMandatoryParam("axes", requestMap);
        String[] axes = StringUtils.getTechnicalTokens(axesParam, true);
        boolean ignoreEmptyLeaf = false;
        String ignoreParam = requestMap.getParameter("ignore");
        if (ignoreParam != null) {
            if (ignoreParam.contains("empty_leaf")) {
                ignoreEmptyLeaf = true;
            }
        }
        Lang lang = DesmographyUtils.getLang(requestMap);
        Collection<Integer> motcleCodes;
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            motcleCodes = getTerms(dataAccess, requestMap, warningHandler, true);
        }
        return new AxesJsonProducer(scrutariSession, motcleCodes, axes, lang, ignoreEmptyLeaf);
    }

    private static AbstractJsonProducer getResourcesJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        Lang lang = DesmographyUtils.getLang(requestMap);
        Desmography desmography;
        Collection<Integer> codes;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            codes = getResources(dataAccess, requestMap, warningHandler, true);
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
        }
        boolean withIndexation = false;
        String fields = requestMap.getParameter("fields");
        if (fields != null) {
            String[] fieldTokens = StringUtils.getTechnicalTokens(fields, true);
            for (String token : fieldTokens) {
                switch (token) {
                    case "indexation":
                        withIndexation = true;
                        break;
                }
            }
        }
        return new ResourcesJsonProducer(scrutariSession, desmography, codes, lang, withIndexation);
    }

    private static AbstractJsonProducer getTermsJsonProducer(ScrutariSession scrutariSession, RequestMap requestMap, String version, WarningHandler warningHandler) throws ErrorMessageException {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        Lang lang = DesmographyUtils.getLang(requestMap);
        Collection<Integer> terms;
        Desmography desmography;
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            desmography = Desmography.build(dataAccess, requestMap, warningHandler);
        }
        if (requestMap.getParameter(DesmographyParameters.TERMS) != null) {
            try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
                terms = getTerms(dataAccess, requestMap, warningHandler, true);
            }
        } else if (requestMap.getParameter(Parameters.Q) != null) {
            SearchOperation searchOperation = RequestMapUtils.getSearchOperation(requestMap, scrutariDB.getFieldRankManager(), warningHandler);
            SearchOptions searchOptions = SearchOptionsBuilder
                    .init(SearchOptionsDefBuilder.init()
                            .setLang(lang)
                            .addFilterLang(lang)
                            .toSearchOptionsDef())
                    .reduceThesaurusCodes(desmography.getThesaurusCodeList())
                    .toSearchOptions();
            MotcleSearchResult searchResult = MotcleSearchEngine.search(scrutariDB, searchOperation, searchOptions);
            terms = new ArrayList<Integer>();
            for (MotcleSearchResultInfo info : searchResult.getMotcleSearchResultInfoList()) {
                terms.add(info.getCode());
            }
        } else {
            terms = PrimUtils.EMPTY_LIST;
        }
        return new TermsJsonProducer(scrutariSession, terms, lang, isWithIndexation(requestMap));
    }

    private static boolean isWithIndexation(RequestMap requestMap) {
        String includeValue = requestMap.getParameter(DesmographyParameters.INCLUDE);
        if (includeValue == null) {
            return false;
        }
        if (includeValue.contains("indexation")) {
            return true;
        } else {
            return false;
        }
    }

    private static MotcleInfo getTerm(DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler) throws ErrorMessageException {
        String termValue = RequestMapUtils.getMandatoryParam(DesmographyParameters.TERM, requestMap);
        warningHandler.setCurrentParameter(DesmographyParameters.TERM);
        Collection<Integer> codes = RequestMapUtils.toCodes(dataAccess, termValue, warningHandler, ScrutariDataURI.MOTCLEURI_TYPE);
        if ((codes == null) || (codes.isEmpty())) {
            throw new ErrorMessageException("_ error.unknown.parametervalue", DesmographyParameters.TERM, termValue);
        } else {
            return dataAccess.getMotcleInfo(codes.iterator().next());
        }
    }

    private static Collection<Integer> getTerms(DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler, boolean mandatory) throws ErrorMessageException {
        String termsValue;
        if (mandatory) {
            termsValue = RequestMapUtils.getMandatoryParam(DesmographyParameters.TERMS, requestMap);
        } else {
            termsValue = requestMap.getParameter(DesmographyParameters.TERMS);
            if ((termsValue == null) || (termsValue.isEmpty())) {
                return null;
            }
        }
        warningHandler.setCurrentParameter(DesmographyParameters.TERMS);
        Collection<Integer> codes = RequestMapUtils.toCodes(dataAccess, termsValue, warningHandler, ScrutariDataURI.MOTCLEURI_TYPE);
        if (codes == null) {
            return PrimUtils.EMPTY_LIST;
        } else {
            return codes;
        }
    }

    private static Collection<Integer> getResources(DataAccess dataAccess, RequestMap requestMap, WarningHandler warningHandler, boolean mandatory) throws ErrorMessageException {
        String resourcesValue;
        if (mandatory) {
            resourcesValue = RequestMapUtils.getMandatoryParam(DesmographyParameters.RESOURCES, requestMap);
        } else {
            resourcesValue = requestMap.getParameter(DesmographyParameters.RESOURCES);
            if ((resourcesValue == null) || (resourcesValue.isEmpty())) {
                return null;
            }
        }
        warningHandler.setCurrentParameter(DesmographyParameters.RESOURCES);
        Collection<Integer> codes = RequestMapUtils.toCodes(dataAccess, resourcesValue, warningHandler, ScrutariDataURI.FICHEURI_TYPE);
        if (codes == null) {
            return PrimUtils.EMPTY_LIST;
        } else {
            return codes;
        }
    }

}
