/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import net.scrutari.data.RelationData;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class Extract {

    private final Map<RelationData, Integer> relationMap = new HashMap<RelationData, Integer>();
    private final List<RelationData> relationList = new ArrayList<RelationData>();
    private final Map<Integer, Integer> termMap = new HashMap<Integer, Integer>();
    private final List<MotcleInfo> termList = new ArrayList<MotcleInfo>();
    private final Map<Integer, Integer> resourceMap = new HashMap<Integer, Integer>();
    private final List<FicheInfo> resourceList = new ArrayList<FicheInfo>();
    private final Map<Integer, List<Integer>> indexationMap = new HashMap<Integer, List<Integer>>();

    public Extract() {
    }

    public Integer addRelation(RelationData relation) {
        Integer index = relationMap.get(relation);
        if (index != null) {
            return index;
        } else {
            index = relationList.size();
            relationMap.put(relation, index);
            relationList.add(relation);
            return index;
        }
    }

    public Integer getRelationIndex(RelationData relation) {
        return relationMap.get(relation);
    }

    public Integer addTerm(MotcleInfo motcleInfo) {
        Integer index = termMap.get(motcleInfo.getCode());
        if (index != null) {
            return index;
        } else {
            index = termList.size();
            termMap.put(motcleInfo.getCode(), index);
            termList.add(motcleInfo);
            return index;
        }
    }

    public Integer getTermIndex(Integer termCode) {
        return termMap.get(termCode);
    }

    public Integer addResource(FicheInfo ficheInfo) {
        Integer index = resourceMap.get(ficheInfo.getCode());
        if (index != null) {
            return index;
        } else {
            index = resourceList.size();
            resourceMap.put(ficheInfo.getCode(), index);
            resourceList.add(ficheInfo);
            return index;
        }
    }

    public Integer getResourceIndex(Integer resourceCode) {
        return resourceMap.get(resourceCode);
    }

    public Iterator<RelationData> getRelationIterator() {
        return relationList.iterator();
    }

    public Iterator<MotcleInfo> getTermIterator() {
        return new TermIterator();
    }

    public Iterator<FicheInfo> getResourceIterator() {
        return resourceList.iterator();
    }

    public void putIndexationList(FicheInfo ficheInfo, List<Integer> termIndexList) {
        indexationMap.put(ficheInfo.getCode(), termIndexList);
    }

    public List<Integer> getIndexationList(FicheInfo ficheInfo) {
        return indexationMap.get(ficheInfo.getCode());
    }


    private class TermIterator implements Iterator<MotcleInfo> {

        private int cursor = 0;

        private TermIterator() {

        }

        @Override
        public boolean hasNext() {
            return (cursor < termList.size());
        }

        @Override
        public MotcleInfo next() {
            int current = cursor;
            cursor++;
            return termList.get(current);
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }

}
