/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import net.mapeadores.util.json.CommonJson;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.RoleKey;


/**
 *
 * @author Vincent Calame
 */
public class SummaryJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final MotcleInfo term;
    private final Lang lang;

    public SummaryJsonProducer(ScrutariSession scrutariSession, MotcleInfo term, Lang lang) {
        this.scrutariSession = scrutariSession;
        this.term = term;
        this.lang = lang;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        FieldVariant desmographyFieldVariant = DesmographyUtils.getFieldVariant(scrutariSession);
        Extract extract = new Extract();
        MotcleData motcleData = term.getMotcleData(dataAccess);
        jw.key("summary");
        jw.object();
        {
            extract.addTerm(term);
            TermJson.dataProperties(jw, motcleData, lang);
            jw.key("labels");
            CommonJson.object(jw, motcleData.getLabels());
            TermJson.familiesProperty(jw, term, dataAccess, extract);
            jw.key("relationship");
            jw.object();
            writeRelationship(jw, extract);
            jw.endObject();
            TermJson.indexationProperty(jw, term, dataAccess, scrutariSession.getGlobalSearchOptions(), extract, lang);
        }
        jw.endObject();
        RelationJson.properties(jw, dataAccess, extract);
        TermJson.properties(jw, dataAccess, scrutariSession.getGlobalSearchOptions(), extract, lang, false);
        ResourceJson.properties(jw, dataAccess, extract, lang, desmographyFieldVariant);
    }

    private void writeRelationship(JSONWriter jw, Extract extract) throws IOException {
        String currentType = null;
        for (RoleKey roleKey : term.getAvalaibleRoleKeySet()) {
            String type = roleKey.getType();
            if ((type.equals("family")) && (roleKey.getRole().equals("inferior"))) {
                continue;
            }
            if (currentType == null) {
                jw.key(type);
                jw.object();
                currentType = type;
            } else if (!currentType.equals(type)) {
                jw.endObject();
                jw.key(type);
                jw.object();
                currentType = type;
            }
            jw.key(roleKey.getRole());
            jw.array();
            for (RelationData relation : term.getRelationList(roleKey)) {
                jw.value(extract.addRelation(relation));
            }
            jw.endArray();
        }
        if (currentType != null) {
            jw.endObject();
        }
    }

}
