/* SctServer_Desmography - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.desmography.json;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.desmography.Desmography;
import fr.exemole.sctserver.desmography.DesmographyUtils;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import net.mapeadores.util.json.JSONWriter;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class ResourcesJsonProducer extends AbstractJsonProducer {

    private final ScrutariSession scrutariSession;
    private final Desmography desmography;
    private final Collection<Integer> resources;
    private final Lang lang;
    private final boolean withIndexation;

    public ResourcesJsonProducer(ScrutariSession scrutariSession, Desmography desmography, Collection<Integer> resources, Lang lang, boolean withIndexation) {
        this.scrutariSession = scrutariSession;
        this.desmography = desmography;
        this.resources = resources;
        this.lang = lang;
        this.withIndexation = withIndexation;
    }

    @Override
    public void writeJson(Appendable appendable) throws IOException {
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            JSONWriter jw = new JSONWriter(appendable);
            jw.object();
            {
                writeVersion1(jw, dataAccess);
                writeWarnings(jw);
            }
            jw.endObject();
        }
    }

    private void writeVersion1(JSONWriter jw, DataAccess dataAccess) throws IOException {
        FieldVariant desmographyFieldVariant = DesmographyUtils.getFieldVariant(scrutariSession);
        Extract extract = new Extract();
        ThesaurusData[] array = desmography.getThesaurusDataArray();
        List<Integer> refList = new ArrayList<Integer>();
        for (Integer resourceCode : resources) {
            FicheInfo ficheInfo = dataAccess.getFicheInfo(resourceCode);
            if (ficheInfo != null) {
                refList.add(extract.addResource(ficheInfo));
                if (withIndexation) {
                    scanIndexation(dataAccess, ficheInfo, extract, array);
                }
            }

        }
        jw.key("resource_refs");
        jw.array();
        for (Integer code : refList) {
            jw.value(code);
        }
        jw.endArray();
        if (withIndexation) {
            TermJson.properties(jw, dataAccess, scrutariSession.getGlobalSearchOptions(), extract, lang, true);
        }
        ResourceJson.properties(jw, dataAccess, extract, lang, desmographyFieldVariant);
    }

    private void scanIndexation(DataAccess dataAccess, FicheInfo ficheInfo, Extract extract, ThesaurusData[] array) {
        List<Integer> termIndexList = new ArrayList<Integer>();
        for (ThesaurusData thesaurusData : array) {
            List<Integer> termList = ficheInfo.getIndexationCodeList(thesaurusData.getThesaurusCode());
            for (Integer termCode : termList) {
                MotcleInfo motcleInfo = dataAccess.getMotcleInfo(termCode);
                if (motcleInfo != null) {
                    termIndexList.add(extract.addTerm(motcleInfo));
                }
            }
        }
        extract.putIndexationList(ficheInfo, termIndexList);
    }

}
