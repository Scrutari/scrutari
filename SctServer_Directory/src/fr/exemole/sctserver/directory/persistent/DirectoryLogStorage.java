/* SctServer_Directory - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.persistent;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.log.QLog;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.tools.log.SctLogUtils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import net.mapeadores.util.io.FileUtils;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryLogStorage implements LogStorage {

    private final File logsDirectory;

    public DirectoryLogStorage(File logsDirectory) {
        this.logsDirectory = logsDirectory;
    }

    @Override
    public synchronized void log(ScrutariSession scrutariSession, QLog qLog) {
        FuzzyDate currentDate = FuzzyDate.current();
        File logFile = getLogFile(currentDate, true);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(logFile, true), "UTF-8"))) {
            writeString(writer, qLog.getQ());
            writer.write('\t');
            writeString(writer, qLog.getOrigin());
            writer.write('\t');
            int ficheCount = qLog.getFicheCount();
            writer.write(String.valueOf(ficheCount));
            int ficheMaximum = qLog.getFicheMaximum();
            if (ficheMaximum < 0) {
                ficheMaximum = scrutariSession.getScrutariDB().getStats().getEngineCountStats().getFicheCount();
            }
            writer.write('\t');
            writer.write(String.valueOf(ficheMaximum));
            writer.write('\n');
        } catch (IOException ioe) {
        }

    }

    @Override
    public FuzzyDate[] getDateArray(short dateType, FuzzyDate dateFilter) {
        if (dateType == FuzzyDate.YEAR_TYPE) {
            return getYearArray();
        }
        if (dateType == FuzzyDate.MONTH_TYPE) {
            if ((dateFilter != null) && (dateFilter.getType() == FuzzyDate.YEAR_TYPE)) {
                return DirectoryLogStorage.this.getMonthArray(dateFilter.getYear());
            } else {
                return getMonthArray();
            }
        }
        if (dateType == FuzzyDate.DAY_TYPE) {
            if ((dateFilter != null) && (dateFilter.getType() == FuzzyDate.MONTH_TYPE)) {
                return DirectoryLogStorage.this.getDayArray(dateFilter);
            } else {
                return getDayArray();
            }
        }
        return new FuzzyDate[0];
    }

    @Override
    public String getQLog(FuzzyDate date) {
        File logFile = getLogFile(date, false);
        if (!logFile.exists()) {
            return null;
        }
        try (FileInputStream is = new FileInputStream(logFile)) {
            String content = IOUtils.toString(is, "UTF-8");
            return content;
        } catch (IOException ioe) {
            return null;
        }
    }

    @Override
    public void log(String dir, String name, String content) {
        if (!SctLogUtils.isValidLogDir(dir)) {
            return;
        }
        File subDir = new File(logsDirectory, dir);
        File logFile = new File(subDir, name);
        if ((content == null) || (content.isEmpty())) {
            if (logFile.exists()) {
                logFile.delete();
            }
        } else {
            subDir.mkdirs();
            try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(logFile), "UTF-8")) {
                writer.write(content);
            } catch (IOException ioe) {
            }
        }
    }

    @Override
    public PrintWriter getPrintWriter(String dir, String name) {
        if (!SctLogUtils.isValidLogDir(dir)) {
            return null;
        }
        File subDir = new File(logsDirectory, dir);
        File logFile = new File(subDir, name);
        subDir.mkdirs();
        try {
            return new PrintWriter(logFile, "UTF-8");
        } catch (IOException ioe) {
            return null;
        }
    }

    @Override
    public void removeDir(String logDir) {
        if (!SctLogUtils.isValidLogDir(logDir)) {
            return;
        }
        File subDir = new File(logsDirectory, logDir);
        if (subDir.exists()) {
            try {
                FileUtils.deleteDirectory(subDir);
            } catch (IOException ioe) {
                return;
            }
        }
    }

    @Override
    public String getLog(String dir, String name) {
        if (!SctLogUtils.isValidLogDir(dir)) {
            return null;
        }
        File subDir = new File(logsDirectory, dir);
        File logFile = new File(subDir, name);
        if (!logFile.exists()) {
            return null;
        } else {
            try (FileInputStream is = new FileInputStream(logFile)) {
                String content = IOUtils.toString(is, "UTF-8");
                return content;
            } catch (IOException ioe) {
                return null;
            }
        }
    }

    @Override
    public List<String> getAvailableLogs(String dir) {
        List<String> result = new ArrayList<String>();
        if (!SctLogUtils.isValidLogDir(dir)) {
            return result;
        }
        File subDir = new File(logsDirectory, dir);
        if (!subDir.exists()) {
            return result;
        }
        for (File file : subDir.listFiles()) {
            if (!file.isDirectory()) {
                String name = file.getName();
                if (!name.startsWith(".")) {
                    result.add(name);
                }
            }
        }
        return result;
    }

    private void writeString(Writer writer, String text) throws IOException {
        int length = text.length();
        for (int i = 0; i < length; i++) {
            char carac = text.charAt(i);
            switch (carac) {
                case '\t':
                case '\n':
                case '\r':
                    writer.write(' ');
                    break;
                default:
                    writer.write(carac);
            }
        }
    }

    private File getLogFile(FuzzyDate date, boolean mkdirs) {
        StringBuilder buf = new StringBuilder();
        buf.append("q");
        buf.append(File.separatorChar);
        int annee = date.getYear();
        buf.append(annee);
        buf.append(File.separatorChar);
        int mois = date.getMonth();
        if (mois < 10) {
            buf.append('0');
        }
        buf.append(mois);
        File dir = new File(logsDirectory, buf.toString());
        if (mkdirs) {
            dir.mkdirs();
        }
        int jour = date.getDay();
        String name;
        if (jour < 10) {
            name = "0" + jour + ".txt";
        } else {
            name = jour + ".txt";
        }
        return new File(dir, name);
    }

    private FuzzyDate[] getYearArray() {
        SortedSet<FuzzyDate> sortedSet = new TreeSet<FuzzyDate>();
        File dir = new File(logsDirectory, "q");
        if (dir.exists()) {
            for (File d : dir.listFiles()) {
                if (!d.isDirectory()) {
                    continue;
                }
                try {
                    FuzzyDate date = FuzzyDate.parse(d.getName());
                    if (date.getType() == FuzzyDate.YEAR_TYPE) {
                        sortedSet.add(date);
                    }
                } catch (ParseException pe) {
                }
            }
        }
        return toArray(sortedSet);
    }

    private FuzzyDate[] getMonthArray(int annee) {
        SortedSet< FuzzyDate> sortedSet = new TreeSet< FuzzyDate>();
        File dir = new File(logsDirectory, "q" + File.separator + annee);
        if (dir.exists()) {
            addMonth(sortedSet, dir);
        }
        return toArray(sortedSet);
    }

    private FuzzyDate[] getMonthArray() {
        SortedSet< FuzzyDate> sortedSet = new TreeSet< FuzzyDate>();
        File dir = new File(logsDirectory, "q");
        if (dir.exists()) {
            for (File d : dir.listFiles()) {
                if (!d.isDirectory()) {
                    continue;
                }
                addMonth(sortedSet, d);
            }
        }
        return toArray(sortedSet);
    }

    private void addMonth(SortedSet<FuzzyDate> sortedSet, File dir) {
        String prefix = dir.getName() + "-";
        File[] list = dir.listFiles();
        for (File d : dir.listFiles()) {
            if (!d.isDirectory()) {
                continue;
            }
            try {
                FuzzyDate date = FuzzyDate.parse(prefix + d.getName());
                if (date.getType() == FuzzyDate.MONTH_TYPE) {
                    sortedSet.add(date);
                }
            } catch (ParseException pe) {
            }
        }
    }

    private FuzzyDate[] getDayArray(FuzzyDate date) {
        SortedSet<FuzzyDate> sortedSet = new TreeSet<FuzzyDate>();
        int year = date.getYear();
        StringBuilder buf = new StringBuilder();
        buf.append("q");
        buf.append(File.separatorChar);
        buf.append(year);
        buf.append(File.separatorChar);
        int month = date.getMonth();
        if (month < 10) {
            buf.append("0");
        }
        buf.append(month);
        File dir = new File(logsDirectory, buf.toString());
        if (dir.exists()) {
            addDays(sortedSet, year, dir);
        }
        return toArray(sortedSet);
    }

    private FuzzyDate[] getDayArray() {
        SortedSet<FuzzyDate> sortedSet = new TreeSet<FuzzyDate>();
        File dir = new File(logsDirectory, "q");
        for (File d : dir.listFiles()) {
            if (!d.isDirectory()) {
                continue;
            }
            int annee;
            try {
                annee = Integer.parseInt(d.getName());
            } catch (NumberFormatException nfe) {
                continue;
            }
            File[] list2 = d.listFiles();
            int length2 = list2.length;
            for (int j = 0; j < length2; j++) {
                File d2 = list2[j];
                if (!d2.isDirectory()) {
                    continue;
                }
                addDays(sortedSet, annee, d2);
            }

        }
        return toArray(sortedSet);
    }

    private void addDays(SortedSet<FuzzyDate> sortedSet, int year, File dir) {
        String prefix = year + "-" + dir.getName() + "-";
        for (File f : dir.listFiles()) {
            if (f.isDirectory()) {
                continue;
            }
            String name = f.getName();
            int idx = name.lastIndexOf('.');
            if (idx == -1) {
                continue;
            }
            name = name.substring(0, idx);
            try {
                FuzzyDate date = FuzzyDate.parse(prefix + name);
                if (date.getType() == FuzzyDate.DAY_TYPE) {
                    sortedSet.add(date);
                }
            } catch (ParseException pe) {
            }
        }
    }

    private static FuzzyDate[] toArray(Set<FuzzyDate> set) {
        return set.toArray(new FuzzyDate[set.size()]);
    }

}
