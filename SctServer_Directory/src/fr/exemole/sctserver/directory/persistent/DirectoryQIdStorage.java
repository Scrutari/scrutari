/* SctServer_Directory - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.persistent;

import fr.exemole.sctserver.api.storage.QIdStorage;
import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import javax.xml.parsers.DocumentBuilder;
import net.mapeadores.util.primitives.ByteArrayKey;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.DOMUtils;
import net.mapeadores.util.xml.XMLUtils;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.searchengine.api.CanonicalQ;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.options.SearchOptionsDef;
import net.scrutari.searchengine.api.result.FicheSearchSource;
import net.scrutari.searchengine.tools.SearchEngineUtils;
import net.scrutari.searchengine.tools.dom.FicheSearchSourceDOMReader;
import net.scrutari.searchengine.tools.result.FicheSearchSourceBuilder;
import net.scrutari.searchengine.xml.FicheSearchSourceXMLPart;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryQIdStorage implements QIdStorage {

    private final static int CACHE_VERSION = 3;
    private final File qidDirectory;
    private final File qidCacheFile;
    private final Map<ByteArrayKey, QId> qIdMap = new HashMap<ByteArrayKey, QId>();

    public DirectoryQIdStorage(File qidDirectory, File cacheDirectory) {
        this.qidDirectory = qidDirectory;
        cacheDirectory.mkdirs();
        this.qidCacheFile = new File(cacheDirectory, "qid.primitives");
        buildQIdMap();
    }

    @Override
    public QId containsQId(CanonicalQ canonicalQ, SearchOptionsDef searchOptionsDef) {
        ByteArrayKey key = SearchEngineUtils.toKey(canonicalQ, searchOptionsDef);
        return qIdMap.get(key);
    }

    @Override
    public FicheSearchSource getFicheSearchSource(QId qId) {
        File qIdFile = getQIdFile(qId, false);
        if (!qIdFile.exists()) {
            return null;
        }
        DocumentBuilder documentBuilder = DOMUtils.newDocumentBuilder();
        Document doc;
        try {
            doc = documentBuilder.parse(qIdFile);
        } catch (SAXException | IOException e) {
            return null;
        }
        FicheSearchSourceBuilder builder = new FicheSearchSourceBuilder();
        builder.setQId(qId);
        FicheSearchSourceDOMReader reader = new FicheSearchSourceDOMReader(builder);
        reader.readFicheSearchSource(doc.getDocumentElement());
        if (builder.isValid()) {
            return builder.toFicheSearchSource();
        } else {
            return null;
        }
    }

    @Override
    public void saveFicheSearchSource(FicheSearchSource ficheSearchSource) {
        QId qId = ficheSearchSource.getQId();
        if (qId == null) {
            return;
        }
        ByteArrayKey key = SearchEngineUtils.toKey(ficheSearchSource.getCanonicalQ(), ficheSearchSource.getSearchOptionsDef());
        File qIdFile = getQIdFile(qId, true);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(qIdFile), "UTF-8"))) {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(writer);
            xmlWriter.appendXMLDeclaration();
            FicheSearchSourceXMLPart ficheSearchXMLPart = new FicheSearchSourceXMLPart(xmlWriter);
            ficheSearchXMLPart.appendFicheSearchSource(ficheSearchSource);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        synchronized (qIdMap) {
            qIdMap.put(key, qId);
            boolean empty = (qidCacheFile.length() == 0);
            try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(qidCacheFile, true))) {
                PrimitivesWriter dataPrimitivesWriter = PrimitivesIOFactory.newWriter(os);
                if (empty) {
                    dataPrimitivesWriter.writeInt(CACHE_VERSION);
                }
                ByteArrayKey.toPrimitives(dataPrimitivesWriter, key);
                QId.toPrimitives(dataPrimitivesWriter, qId);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }
    }

    @Override
    public int getMaxSubId(ScrutariDBName scrutariDBName) {
        String year = scrutariDBName.getYear();
        File dir = new File(qidDirectory, year + File.separator + scrutariDBName.getName());
        if (!dir.exists()) {
            return 0;
        }
        int max = 0;
        for (File centuryDir : dir.listFiles()) {
            if (centuryDir.isDirectory()) {
                for (File file : centuryDir.listFiles()) {
                    if (file.isFile()) {
                        String name = file.getName();
                        if (name.endsWith(".xml")) {
                            try {
                                int number = Integer.parseInt(name.substring(0, name.length() - 4));
                                max = Math.max(max, number);
                            } catch (NumberFormatException nfe) {

                            }
                        }
                    }
                }
            }
        }
        return max;
    }

    private File getQIdFile(QId qId, boolean mkdirs) {
        ScrutariDBName scrutariDBName = qId.getScrutariDBName();
        int subId = qId.getSubId();
        String century = String.valueOf(subId / 100) + "00";
        String year = scrutariDBName.getYear();
        File dbDir = new File(qidDirectory, year + File.separator + scrutariDBName.getName() + File.separator + century);
        if (mkdirs) {
            dbDir.mkdirs();
        }
        return new File(dbDir, subId + ".xml");
    }

    private void buildQIdMap() {
        boolean done = false;
        if (qidCacheFile.exists()) {
            try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(qidCacheFile))) {
                PrimitivesReader primitivesReader = PrimitivesIOFactory.newReader(is);
                int cacheVersion = primitivesReader.readInt();
                if (cacheVersion != CACHE_VERSION) {
                    done = false;
                } else {
                    try {
                        while (true) {
                            ByteArrayKey byteArrayKey = ByteArrayKey.fromPrimitives(primitivesReader);
                            QId qId = QId.fromPrimitives(primitivesReader);
                            qIdMap.put(byteArrayKey, qId);
                        }
                    } catch (EOFException eof) {
                        done = true;
                    }

                }
            } catch (IOException ioe) {

            }
        }
        if (!done) {
            scan();
            qidCacheFile.delete();
            try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(qidCacheFile))) {
                PrimitivesWriter dataPrimitivesWriter = PrimitivesIOFactory.newWriter(os);
                dataPrimitivesWriter.writeInt(CACHE_VERSION);
                for (Map.Entry<ByteArrayKey, QId> entry : qIdMap.entrySet()) {
                    ByteArrayKey.toPrimitives(dataPrimitivesWriter, entry.getKey());
                    QId.toPrimitives(dataPrimitivesWriter, entry.getValue());
                }
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }
    }

    private void scan() {
        DocumentBuilder documentBuilder = DOMUtils.newDocumentBuilder();
        TreeMap<ScrutariDBName, File> dbMap = new TreeMap<ScrutariDBName, File>();
        File[] yearList = qidDirectory.listFiles();
        int yearCount = yearList.length;
        for (int i = 0; i < yearCount; i++) {
            File dir = yearList[i];
            if (dir.isDirectory()) {
                File[] dbList = dir.listFiles();
                int dbCount = dbList.length;
                for (int j = 0; j < dbCount; j++) {
                    File dbDir = dbList[j];
                    if (dbDir.isDirectory()) {
                        try {
                            ScrutariDBName scrutariDBName = ScrutariDBName.parse(dbDir.getName());
                            dbMap.put(scrutariDBName, dbDir);
                        } catch (ParseException pe) {
                        }
                    }
                }
            }
        }
        for (Map.Entry<ScrutariDBName, File> entry : dbMap.entrySet()) {
            ScrutariDBName scrutariDBName = entry.getKey();
            File[] list = entry.getValue().listFiles();
            int length = list.length;
            for (int i = 0; i < length; i++) {
                File subDir = list[i];
                File[] files = subDir.listFiles();
                int subCount = files.length;
                for (int j = 0; j < subCount; j++) {
                    File f = files[j];
                    if (!f.isDirectory()) {
                        String name = f.getName();
                        if (name.endsWith(".xml")) {
                            try {
                                int numero = Integer.parseInt(name.substring(0, name.length() - 4));
                                if (numero > 0) {
                                    QId qId = new QId(scrutariDBName, numero);
                                    try {
                                        Document doc = documentBuilder.parse(f);
                                        FicheSearchSourceBuilder builder = new FicheSearchSourceBuilder();
                                        builder.setQId(qId);
                                        FicheSearchSourceDOMReader reader = new FicheSearchSourceDOMReader(builder);
                                        reader.readFicheSearchSource(doc.getDocumentElement());
                                        if (builder.isValid()) {
                                            FicheSearchSource ficheSearchSource = builder.toFicheSearchSource();
                                            ByteArrayKey key = SearchEngineUtils.toKey(ficheSearchSource.getCanonicalQ(), ficheSearchSource.getSearchOptionsDef());
                                            qIdMap.put(key, qId);
                                        }
                                    } catch (SAXException | IOException e) {
                                    }
                                }
                            } catch (NumberFormatException nfe) {
                            }
                        }
                    }
                }
            }
        }
    }

}
