/* SctServer_Directory - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.CodeArray;
import net.mapeadores.util.primitives.CodeArrayBuilder;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.primitives.io.CacheRef;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.IndexationData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.caches.CacheIterator;
import net.scrutari.db.api.caches.DataCacheReader;
import net.scrutari.db.api.caches.DataCacheReaderFactory;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryDataCacheReaderFactory implements DataCacheReaderFactory {

    private final CodeArray<CacheRef> refMap;
    private final File[] dataFileArray;

    public DirectoryDataCacheReaderFactory(CodeArray<CacheRef> refMap, File[] dataFileArray) {
        this.refMap = refMap;
        this.dataFileArray = dataFileArray;
    }

    @Override
    public DataCacheReader newReader() {
        return new InternalDataCacheReader();
    }

    @Override
    public CacheIterator<BaseData> newBaseDataIterator() {
        return CacheUtils.newBaseDataIterator(dataFileArray[CacheUtils.BASE_INDEX]);
    }

    @Override
    public CacheIterator<CorpusData> newCorpusDataIterator() {
        return CacheUtils.newCorpusDataIterator(dataFileArray[CacheUtils.CORPUS_INDEX]);
    }

    @Override
    public CacheIterator<ThesaurusData> newThesaurusDataIterator() {
        return CacheUtils.newThesaurusDataIterator(dataFileArray[CacheUtils.THESAURUS_INDEX]);
    }

    @Override
    public CacheIterator<FicheData> newFicheDataIterator() {
        return CacheUtils.newFicheDataIterator(dataFileArray[CacheUtils.FICHE_INDEX]);
    }

    @Override
    public CacheIterator<MotcleData> newMotcleDataIterator() {
        return CacheUtils.newMotcleDataIterator(dataFileArray[CacheUtils.MOTCLE_INDEX]);
    }

    @Override
    public CacheIterator<IndexationData> newIndexationDataIterator() {
        return CacheUtils.newIndexationDataIterator(dataFileArray[CacheUtils.INDEXATION_INDEX]);
    }

    @Override
    public CacheIterator<RelationData> newRelationDataIterator() {
        return CacheUtils.newRelationDataIterator(dataFileArray[CacheUtils.RELATION_INDEX]);
    }

    public static DataCacheReaderFactory loadFromCache(File cacheDirectory) {
        File[] dataFileArray = new File[CacheUtils.LENGTH];
        try {
            dataFileArray[CacheUtils.BASE_INDEX] = testExistence(cacheDirectory, "base");
            dataFileArray[CacheUtils.CORPUS_INDEX] = testExistence(cacheDirectory, "corpus");
            dataFileArray[CacheUtils.THESAURUS_INDEX] = testExistence(cacheDirectory, "thesaurus");
            dataFileArray[CacheUtils.FICHE_INDEX] = testExistence(cacheDirectory, "fiche");
            dataFileArray[CacheUtils.MOTCLE_INDEX] = testExistence(cacheDirectory, "motcle");
            dataFileArray[CacheUtils.INDEXATION_INDEX] = testExistence(cacheDirectory, "indexation");
            dataFileArray[CacheUtils.RELATION_INDEX] = testExistence(cacheDirectory, "relation");
        } catch (FileNotFoundException ioe) {
            return null;
        }
        CodeArrayBuilder codeArrayBuilder = new CodeArrayBuilder(65536);
        try {
            CacheUtils.checkFichePosition(dataFileArray[CacheUtils.FICHE_INDEX], codeArrayBuilder);
            CacheUtils.checkMotclePosition(dataFileArray[CacheUtils.MOTCLE_INDEX], codeArrayBuilder);
        } catch (IOException ioe) {
            return null;
        }
        return new DirectoryDataCacheReaderFactory(codeArrayBuilder.toCodeArray(), dataFileArray);
    }

    private static File testExistence(File cacheDirectory, String name) throws FileNotFoundException {
        File f = new File(cacheDirectory, "data-" + name + ".primitives");
        if ((!f.exists()) || (f.isDirectory())) {
            throw new FileNotFoundException(f.getPath());
        }
        return f;
    }


    private class InternalDataCacheReader implements DataCacheReader {

        private final DataReadInfo[] dataReadInfoArray;

        private InternalDataCacheReader() {
            dataReadInfoArray = new DataReadInfo[CacheUtils.LENGTH];
            dataReadInfoArray[CacheUtils.FICHE_INDEX] = initDataReadInfo(CacheUtils.FICHE_INDEX);
            dataReadInfoArray[CacheUtils.MOTCLE_INDEX] = initDataReadInfo(CacheUtils.MOTCLE_INDEX);
        }

        @Override
        public FicheData getFicheData(Integer ficheCode) {
            CacheRef cacheRef = refMap.get(ficheCode);
            if (cacheRef == null) {
                return null;
            }
            try {
                return FicheData.fromPrimitives(getBytesReader(CacheUtils.FICHE_INDEX, cacheRef));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public MotcleData getMotcleData(Integer motcleCode) {
            CacheRef cacheRef = refMap.get(motcleCode);
            if (cacheRef == null) {
                return null;
            }
            try {
                return MotcleData.fromPrimitives(getBytesReader(CacheUtils.MOTCLE_INDEX, cacheRef));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public Lang getFicheLang(Integer ficheCode) {
            CacheRef cacheRef = refMap.get(ficheCode);
            if (cacheRef == null) {
                return FicheData.UNDETERMINED_LANG;
            }
            try {
                return FicheData.getLangFromPrimitives(getFicheRandomAccessReader(cacheRef));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public FuzzyDate getFicheDate(Integer ficheCode) {
            CacheRef cacheRef = refMap.get(ficheCode);
            if (cacheRef == null) {
                return null;
            }
            try {
                return FicheData.getDateFromPrimitives(getFicheRandomAccessReader(cacheRef));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            for (int i = 0; i < CacheUtils.LENGTH; i++) {
                DataReadInfo dataReadInfo = dataReadInfoArray[i];
                if (dataReadInfo != null) {
                    dataReadInfo.close();
                    dataReadInfoArray[i] = null;
                }
            }
        }

        private DataReadInfo initDataReadInfo(int index) {
            try {
                RandomAccessFile dataRandomAccessFile = new RandomAccessFile(dataFileArray[index], "r");
                return new DataReadInfo(dataRandomAccessFile);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        private PrimitivesReader getBytesReader(int index, CacheRef cacheRef) throws IOException {
            return PrimitivesIOFactory.newReader(dataReadInfoArray[index].getBytes(cacheRef));
        }

        private PrimitivesReader getFicheRandomAccessReader(CacheRef cacheRef) throws IOException {
            RandomAccessFile dataRandomAccessFile = dataReadInfoArray[CacheUtils.FICHE_INDEX].dataRandomAccessFile;
            dataRandomAccessFile.seek(cacheRef.getPosition());
            return PrimitivesIOFactory.newReader(dataRandomAccessFile);
        }

    }


    private static class DataReadInfo {

        private final RandomAccessFile dataRandomAccessFile;

        private DataReadInfo(RandomAccessFile dataRandomAccessFile) {
            this.dataRandomAccessFile = dataRandomAccessFile;
        }

        private byte[] getBytes(CacheRef cacheRef) throws IOException {
            return CacheRef.read(dataRandomAccessFile, cacheRef);
        }

        private void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }

}
