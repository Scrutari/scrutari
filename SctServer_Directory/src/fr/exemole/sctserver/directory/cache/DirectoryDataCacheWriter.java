/* SctServer_Directory - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import net.mapeadores.util.primitives.CodeArrayBuilder;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.CacheRef;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.WriterBuffer;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.IndexationData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.caches.DataCacheReaderFactory;
import net.scrutari.db.api.caches.DataCacheWriter;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryDataCacheWriter implements DataCacheWriter {

    private final CodeArrayBuilder<CacheRef> codeArrayBuilder = new CodeArrayBuilder<CacheRef>(65536);
    private final DataWriteInfo[] dataWriteInfoArray;

    public DirectoryDataCacheWriter(File cacheDirectory) {
        dataWriteInfoArray = new DataWriteInfo[CacheUtils.LENGTH];
        dataWriteInfoArray[CacheUtils.BASE_INDEX] = initDataWriteInfo(cacheDirectory, "base");
        dataWriteInfoArray[CacheUtils.CORPUS_INDEX] = initDataWriteInfo(cacheDirectory, "corpus");
        dataWriteInfoArray[CacheUtils.THESAURUS_INDEX] = initDataWriteInfo(cacheDirectory, "thesaurus");
        dataWriteInfoArray[CacheUtils.FICHE_INDEX] = initDataWriteInfo(cacheDirectory, "fiche");
        dataWriteInfoArray[CacheUtils.MOTCLE_INDEX] = initDataWriteInfo(cacheDirectory, "motcle");
        dataWriteInfoArray[CacheUtils.INDEXATION_INDEX] = initDataWriteInfo(cacheDirectory, "indexation");
        dataWriteInfoArray[CacheUtils.RELATION_INDEX] = initDataWriteInfo(cacheDirectory, "relation");
    }

    @Override
    public void cacheBaseData(BaseData baseData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            BaseData.toPrimitives(baseData, writerBuffer);
            write(CacheUtils.BASE_INDEX, writerBuffer);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheCorpusData(CorpusData corpusData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            CorpusData.toPrimitives(corpusData, writerBuffer);
            write(CacheUtils.CORPUS_INDEX, writerBuffer);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheThesaurusData(ThesaurusData thesaurusData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            ThesaurusData.toPrimitives(thesaurusData, writerBuffer);
            write(CacheUtils.THESAURUS_INDEX, writerBuffer);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheFicheData(FicheData ficheData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            FicheData.toPrimitives(ficheData, writerBuffer);
            CacheRef cacheRef = write(CacheUtils.FICHE_INDEX, writerBuffer);
            codeArrayBuilder.put(ficheData.getFicheCode(), cacheRef);

        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheMotcleData(MotcleData motcleData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            MotcleData.toPrimitives(motcleData, writerBuffer);
            CacheRef cacheRef = write(CacheUtils.MOTCLE_INDEX, writerBuffer);
            codeArrayBuilder.put(motcleData.getMotcleCode(), cacheRef);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheIndexationData(IndexationData indexationData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            IndexationData.toPrimitives(indexationData, writerBuffer);
            write(CacheUtils.INDEXATION_INDEX, writerBuffer);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheRelationData(RelationData relationData) {
        try {
            WriterBuffer writerBuffer = PrimitivesIOFactory.newWriterBuffer();
            RelationData.toPrimitives(relationData, writerBuffer);
            write(CacheUtils.RELATION_INDEX, writerBuffer);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public DataCacheReaderFactory endCacheWrite() {
        File[] dataFileArray = new File[CacheUtils.LENGTH];
        for (int i = 0; i < CacheUtils.LENGTH; i++) {
            DataWriteInfo dataFileWriter = dataWriteInfoArray[i];
            dataFileArray[i] = dataFileWriter.dataFile;
            dataFileWriter.close();
            dataWriteInfoArray[i] = null;
        }
        return new DirectoryDataCacheReaderFactory(codeArrayBuilder.toCodeArray(), dataFileArray);
    }

    private CacheRef write(int index, WriterBuffer writerBuffer) throws IOException {
        return dataWriteInfoArray[index].write(writerBuffer.toBytes());
    }

    private DataWriteInfo initDataWriteInfo(File cacheDirectory, String name) {
        File f = new File(cacheDirectory, "data-" + name + ".primitives");
        if (f.exists()) {
            f.delete();
        }
        RandomAccessFile dataRandomAccessFile;
        try {
            dataRandomAccessFile = new RandomAccessFile(f, "rw");
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        return new DataWriteInfo(f, dataRandomAccessFile);
    }


    private static class DataWriteInfo {

        private final byte[] declaration = new byte[8];
        private final File dataFile;
        private final RandomAccessFile dataRandomAccessFile;

        private DataWriteInfo(File dataFile, RandomAccessFile dataRandomAccessFile) {
            this.dataFile = dataFile;
            this.dataRandomAccessFile = dataRandomAccessFile;
        }

        private CacheRef write(byte[] buffer) throws IOException {
            PrimUtils.convertInt(buffer.length, declaration);
            dataRandomAccessFile.write(declaration, 0, 4);
            return CacheRef.write(dataRandomAccessFile, buffer);
        }

        private void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


}
