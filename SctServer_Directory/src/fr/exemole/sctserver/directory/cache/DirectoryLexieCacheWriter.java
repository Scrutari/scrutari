/* SctServer_Directory - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.primitives.CodeArrayBuilder;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.CacheRef;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.primitives.io.WriterBuffer;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheWriter;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.LexieUtils;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryLexieCacheWriter implements LexieCacheWriter {

    private final File scrutaridbCacheDirectory;
    private final File occurrencesTmpFile;
    private final File lexificationFile;
    private final File unitFile;
    private final RandomAccessFile unorderedOccurrencesRandomAccessFile;
    private final RandomAccessFile lexificationRandomAccessFile;
    private final CodeArrayBuilder<CacheRef> lexificationCodeArrayBuilder = new CodeArrayBuilder<CacheRef>(65536);
    private final Map<OccurrencesKey, List<CacheRef>> unorderedOccurrencesRefMap = new HashMap<OccurrencesKey, List<CacheRef>>();

    public DirectoryLexieCacheWriter(File scrutaridbCacheDirectory) {
        this.scrutaridbCacheDirectory = scrutaridbCacheDirectory;
        this.occurrencesTmpFile = new File(scrutaridbCacheDirectory, "_tmp_lexie-occurrences.primitives");
        if (occurrencesTmpFile.exists()) {
            occurrencesTmpFile.delete();
        }
        try {
            this.unorderedOccurrencesRandomAccessFile = new RandomAccessFile(occurrencesTmpFile, "rw");
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        this.lexificationFile = new File(scrutaridbCacheDirectory, "lexie-lexification.primitives");
        if (lexificationFile.exists()) {
            lexificationFile.delete();
        }
        try {
            this.lexificationRandomAccessFile = new RandomAccessFile(lexificationFile, "rw");
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        File unit = new File(scrutaridbCacheDirectory, "lexie-unit.primitives");
        if (unit.exists()) {
            unit.delete();
        }
        this.unitFile = unit;
    }

    @Override
    public void cacheLexieOccurrences(LexieId lexieId, Integer subsetCode, LexieOccurrences lexieOccurrences) {
        try {
            WriterBuffer buffer = PrimitivesIOFactory.newWriterBuffer();
            LexieOccurrences.toPrimitives(lexieOccurrences, buffer);
            CacheRef cacheRef = CacheRef.write(unorderedOccurrencesRandomAccessFile, buffer.toBytes());
            OccurrencesKey occurrencesKey = new OccurrencesKey(lexieId.getValue(), subsetCode);
            List<CacheRef> list = unorderedOccurrencesRefMap.get(occurrencesKey);
            if (list == null) {
                list = new ArrayList<CacheRef>();
                unorderedOccurrencesRefMap.put(occurrencesKey, list);
            }
            list.add(cacheRef);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheFicheLexification(Integer ficheCode, FicheLexification ficheLexification) {
        try {
            WriterBuffer buffer = PrimitivesIOFactory.newWriterBuffer();
            FicheLexification.toPrimitives(ficheLexification, buffer);
            CacheRef cacheRef = writeLexificationBuffer(ficheCode, 'f', buffer);
            lexificationCodeArrayBuilder.put(ficheCode, cacheRef);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void cacheMotcleLexification(Integer motcleCode, MotcleLexification motcleLexification) {
        try {
            WriterBuffer buffer = PrimitivesIOFactory.newWriterBuffer();
            MotcleLexification.toPrimitives(motcleLexification, buffer);
            CacheRef cacheRef = writeLexificationBuffer(motcleCode, 'm', buffer);
            lexificationCodeArrayBuilder.put(motcleCode, cacheRef);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public LexieCacheReaderFactory endCacheWrite(List<ScrutariLexieUnit> scrutariLexieUnitList) {
        try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(unitFile))) {
            PrimitivesWriter unitPrimitivesWriter = PrimitivesIOFactory.newWriter(os);
            unitPrimitivesWriter.writeInt(scrutariLexieUnitList.size());
            for (ScrutariLexieUnit scrutariLexieUnit : scrutariLexieUnitList) {
                LexieUtils.writeScrutariLexieUnit(scrutariLexieUnit, unitPrimitivesWriter);
            }
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        File occurrencesFile = new File(scrutaridbCacheDirectory, "lexie-occurrences.primitives");
        if (occurrencesFile.exists()) {
            occurrencesFile.delete();
        }
        RandomAccessFile occurrencesRandomAccessFile;
        try {
            occurrencesRandomAccessFile = new RandomAccessFile(occurrencesFile, "rw");
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        Map<OccurrencesKey, CacheRef> occurrencesRefMap;
        try {
            occurrencesRefMap = orderOccurrences(occurrencesRandomAccessFile);
            occurrencesRandomAccessFile.close();
            unorderedOccurrencesRandomAccessFile.close();
            lexificationRandomAccessFile.close();
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        occurrencesTmpFile.delete();
        CacheUtils.saveCacheVersion(scrutaridbCacheDirectory);
        return new DirectoryLexieCacheReaderFactory(occurrencesFile, lexificationFile, unitFile, lexificationCodeArrayBuilder.toCodeArray(), occurrencesRefMap);
    }

    private Map<OccurrencesKey, CacheRef> orderOccurrences(RandomAccessFile occurrencesRandomAccessFile) throws IOException {
        byte[] occurrenceKeyBuffer = new byte[16];
        Map<OccurrencesKey, CacheRef> resultMap = new HashMap<OccurrencesKey, CacheRef>();
        for (Map.Entry<OccurrencesKey, List<CacheRef>> entry : unorderedOccurrencesRefMap.entrySet()) {
            OccurrencesKey occurrencesKey = entry.getKey();
            List<CacheRef> list = entry.getValue();
            byte[] bytes = getOccurrencesBytes(list);
            PrimUtils.convertLong(occurrencesKey.getLexiedIdValue(), occurrenceKeyBuffer);
            PrimUtils.convertInt(occurrencesKey.getSubsetCode(), occurrenceKeyBuffer, 8);
            PrimUtils.convertInt(bytes.length, occurrenceKeyBuffer, 12);
            occurrencesRandomAccessFile.write(occurrenceKeyBuffer);
            CacheRef ref = CacheRef.write(occurrencesRandomAccessFile, bytes);
            resultMap.put(occurrencesKey, ref);
        }
        return resultMap;
    }

    private CacheRef writeLexificationBuffer(int code, char type, WriterBuffer buffer) throws IOException {
        byte[] bytes = buffer.toBytes();
        int bytesLength = bytes.length;
        byte[] declaration = new byte[10];
        PrimUtils.convertInt(code, declaration);
        PrimUtils.convertChar(type, declaration, 4);
        PrimUtils.convertInt(bytesLength, declaration, 6);
        lexificationRandomAccessFile.write(declaration, 0, 10);
        CacheRef cacheRef = CacheRef.write(lexificationRandomAccessFile, bytes);
        return cacheRef;
    }

    private byte[] getOccurrencesBytes(List<CacheRef> list) throws IOException {
        int length = list.size();
        WriterBuffer writeBuffer = PrimitivesIOFactory.newWriterBuffer();
        writeBuffer.writeInt(length);
        for (CacheRef cacheRef : list) {
            byte[] occurrencesCache = CacheRef.read(unorderedOccurrencesRandomAccessFile, cacheRef);
            LexieOccurrences lexieOccurrences = LexieOccurrences.fromPrimitives(PrimitivesIOFactory.newReader(occurrencesCache));
            LexieOccurrences.toPrimitives(lexieOccurrences, writeBuffer);
        }
        return writeBuffer.toBytes();
    }

}
