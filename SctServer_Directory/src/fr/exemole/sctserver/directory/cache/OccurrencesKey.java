/* SctServer_Directory - Copyright (c) 2014-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;


/**
 *
 * @author Vincent Calame
 */
public final class OccurrencesKey {

    private final long lexieIdValue;
    private final int subsetCode;

    public OccurrencesKey(long lexieIdValue, int subsetCode) {
        this.lexieIdValue = lexieIdValue;
        this.subsetCode = subsetCode;
    }

    public long getLexiedIdValue() {
        return lexieIdValue;
    }

    public int getSubsetCode() {
        return subsetCode;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        OccurrencesKey otherOccurrencesKey = (OccurrencesKey) other;
        return (otherOccurrencesKey.lexieIdValue == this.lexieIdValue) && (otherOccurrencesKey.subsetCode == this.subsetCode);
    }

    @Override
    public int hashCode() {
        return ((int) lexieIdValue) + subsetCode;
    }

}
