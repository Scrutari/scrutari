/* SctServer_Directory - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import net.mapeadores.util.primitives.CodeArrayBuilder;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.primitives.io.CacheRef;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.IndexationData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.caches.CacheIterator;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.LexieUtils;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public final class CacheUtils {

    public final static int CACHE_VERSION = 13;
    public final static int BASE_INDEX = 0;
    public final static int CORPUS_INDEX = 1;
    public final static int THESAURUS_INDEX = 2;
    public final static int FICHE_INDEX = 3;
    public final static int MOTCLE_INDEX = 4;
    public final static int INDEXATION_INDEX = 5;
    public final static int RELATION_INDEX = 6;
    public final static int LENGTH = 7;

    private CacheUtils() {
    }

    public static CacheIterator<BaseData> newBaseDataIterator(File dataFile) {
        return new BaseDataIterator(dataFile);
    }

    public static CacheIterator<CorpusData> newCorpusDataIterator(File dataFile) {
        return new CorpusDataIterator(dataFile);
    }

    public static CacheIterator<ThesaurusData> newThesaurusDataIterator(File dataFile) {
        return new ThesaurusDataIterator(dataFile);
    }

    public static CacheIterator<FicheData> newFicheDataIterator(File dataFile) {
        return new FicheDataIterator(dataFile);
    }

    public static CacheIterator<MotcleData> newMotcleDataIterator(File dataFile) {
        return new MotcleDataIterator(dataFile);
    }

    public static CacheIterator<IndexationData> newIndexationDataIterator(File dataFile) {
        return new IndexationDataIterator(dataFile);
    }

    public static CacheIterator<RelationData> newRelationDataIterator(File dataFile) {
        return new RelationDataIterator(dataFile);
    }

    public static void saveCacheVersion(File scrutaridbCacheDirectory) {
        File versionFile = new File(scrutaridbCacheDirectory.getParent(), "VERSION");
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(versionFile), "UTF-8"))) {
            bw.write("Version: ");
            bw.write(String.valueOf(CacheUtils.CACHE_VERSION));
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    public static int getCacheVersion(File cacheDirectory) {
        File versionFile = new File(cacheDirectory, "VERSION");
        if ((!versionFile.exists()) || (versionFile.isDirectory())) {
            return - 1;
        }
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(versionFile), "UTF-8"))) {
            int version = -1;
            String ligne;
            while ((ligne = br.readLine()) != null) {
                if (ligne.startsWith("Version:")) {
                    try {
                        version = Integer.parseInt(ligne.substring(8).trim());
                        break;
                    } catch (NumberFormatException nfe) {

                    }
                }
            }
            return version;
        } catch (IOException ioe) {
            return -1;
        }
    }

    public static void checkFichePosition(File cacheFile, CodeArrayBuilder builder) throws IOException {
        byte[] declaration = new byte[8];
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(cacheFile, "r")) {
            long filelength = randomAccessFile.length();
            while (randomAccessFile.getFilePointer() < filelength) {
                randomAccessFile.read(declaration);
                int bufferLength = PrimUtils.toInt(declaration);
                Integer ficheCode = PrimUtils.toInt(declaration, 4);
                builder.put(ficheCode, new CacheRef(randomAccessFile.getFilePointer() - 4, bufferLength));
                randomAccessFile.skipBytes(bufferLength - 4);
            }
        }
    }

    public static void checkMotclePosition(File cacheFile, CodeArrayBuilder builder) throws IOException {
        byte[] declaration = new byte[8];
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(cacheFile, "r")) {
            long filelength = randomAccessFile.length();
            while (randomAccessFile.getFilePointer() < filelength) {
                randomAccessFile.read(declaration);
                int bufferLength = PrimUtils.toInt(declaration);
                Integer motcleCode = PrimUtils.toInt(declaration, 4);
                builder.put(motcleCode, new CacheRef(randomAccessFile.getFilePointer() - 4, bufferLength));
                randomAccessFile.skipBytes(bufferLength - 4);
            }
        }
    }

    public static void checkLexificationRef(File cacheFile, CodeArrayBuilder<CacheRef> lexificationCodeArrayBuilder) throws IOException {
        byte[] declaration = new byte[10];
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(cacheFile, "r")) {
            long filelength = randomAccessFile.length();
            while (randomAccessFile.getFilePointer() < filelength) {
                randomAccessFile.read(declaration);
                int code = PrimUtils.toInt(declaration);
                int bufferLength = PrimUtils.toInt(declaration, 6);
                long position = randomAccessFile.getFilePointer();
                lexificationCodeArrayBuilder.put(code, new CacheRef(position, bufferLength));
                randomAccessFile.skipBytes(bufferLength);
            }
        }
    }

    public static void checkOccurrenceRef(File cacheFile, Map<OccurrencesKey, CacheRef> occurrencesRefMap) throws IOException {
        byte[] declaration = new byte[16];
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(cacheFile, "r")) {
            long filelength = randomAccessFile.length();
            while (randomAccessFile.getFilePointer() < filelength) {
                randomAccessFile.read(declaration);
                long lexieId = PrimUtils.toLong(declaration);
                int subsetCode = PrimUtils.toInt(declaration, 8);
                int bufferLength = PrimUtils.toInt(declaration, 12);
                long position = randomAccessFile.getFilePointer();
                occurrencesRefMap.put(new OccurrencesKey(lexieId, subsetCode), new CacheRef(position, bufferLength));
                randomAccessFile.skipBytes(bufferLength);
            }
        }
    }

    public static List<ScrutariLexieUnit> scrutariLexieUnitListFromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        int length = primitivesReader.readInt();
        ScrutariLexieUnit[] lexieOccurrencesArray = new ScrutariLexieUnit[length];
        for (int i = 0; i < length; i++) {
            lexieOccurrencesArray[i] = LexieUtils.readScrutariLexieUnit(primitivesReader);
        }
        return LexieUtils.toScrutariLexieUnitList(lexieOccurrencesArray);
    }

    public static void checkLexieOccurrencesFromPrimitives(PrimitivesReader primitivesReader, IntPredicate codePredicate, Consumer<LexieOccurrences> occurrencesConsumer) throws IOException {
        int length = primitivesReader.readInt();
        for (int i = 0; i < length; i++) {
            int code = primitivesReader.readInt();
            if (codePredicate.test(code)) {
                occurrencesConsumer.accept(LexieOccurrences.fromPrimitives(primitivesReader, code));
            } else {
                LexieOccurrences.skipAfterCode(primitivesReader);
            }
        }
    }

    public static List<LexieOccurrences> lexieOccurrencesListFromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        int length = primitivesReader.readInt();
        switch (length) {
            case 0:
                return LexieUtils.EMPTY_LEXIEOCCURRENCESLIST;
            case 1:
                return LexieUtils.wrap(LexieOccurrences.fromPrimitives(primitivesReader));
            default:
                LexieOccurrences[] lexieOccurrencesArray = new LexieOccurrences[length];
                for (int i = 0; i < length; i++) {
                    lexieOccurrencesArray[i] = LexieOccurrences.fromPrimitives(primitivesReader);
                }
                return LexieUtils.wrap(lexieOccurrencesArray);
        }
    }

    public static void lexieOccurencesListToPrimitives(List<LexieOccurrences> lexieOccurencesList, PrimitivesWriter primitivesWriter) throws IOException {
        int length = lexieOccurencesList.size();
        primitivesWriter.writeInt(length);
        for (int i = 0; i < length; i++) {
            LexieOccurrences.toPrimitives(lexieOccurencesList.get(i), primitivesWriter);
        }
    }


    private static class BaseDataIterator implements CacheIterator<BaseData> {

        private final RandomAccessFile dataRandomAccessFile;

        private BaseDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public BaseData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return BaseData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


    private static class CorpusDataIterator implements CacheIterator<CorpusData> {

        private final RandomAccessFile dataRandomAccessFile;

        private CorpusDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public CorpusData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return CorpusData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


    private static class ThesaurusDataIterator implements CacheIterator<ThesaurusData> {

        private final RandomAccessFile dataRandomAccessFile;

        private ThesaurusDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public ThesaurusData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return ThesaurusData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


    private static class FicheDataIterator implements CacheIterator<FicheData> {

        private final RandomAccessFile dataRandomAccessFile;

        private FicheDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public FicheData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return FicheData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


    private static class MotcleDataIterator implements CacheIterator<MotcleData> {

        private final RandomAccessFile dataRandomAccessFile;

        private MotcleDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public MotcleData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return MotcleData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


    private static class IndexationDataIterator implements CacheIterator<IndexationData> {

        private final RandomAccessFile dataRandomAccessFile;

        private IndexationDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public IndexationData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return IndexationData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }


    private static class RelationDataIterator implements CacheIterator<RelationData> {

        private final RandomAccessFile dataRandomAccessFile;

        private RelationDataIterator(File dataFile) {
            try {
                this.dataRandomAccessFile = new RandomAccessFile(dataFile, "r");
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public boolean hasNext() {
            try {
                return (dataRandomAccessFile.getFilePointer() < dataRandomAccessFile.length());
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public RelationData next() {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            try {
                return RelationData.fromPrimitives(getBufferReader(dataRandomAccessFile));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                dataRandomAccessFile.close();
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

    }

    private static PrimitivesReader getBufferReader(RandomAccessFile dataRandomAccessFile) throws IOException {
        int bufferLength = dataRandomAccessFile.readInt();
        byte[] buffer = new byte[bufferLength];
        dataRandomAccessFile.read(buffer);
        return PrimitivesIOFactory.newReader(buffer);
    }

}
