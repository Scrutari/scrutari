/* SctServer_Directory - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import net.mapeadores.util.primitives.CodeArray;
import net.mapeadores.util.primitives.CodeArrayBuilder;
import net.mapeadores.util.primitives.io.CacheRef;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.scrutari.db.api.caches.LexieCacheReader;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.LexieUtils;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryLexieCacheReaderFactory implements LexieCacheReaderFactory {

    private final File occurrencesFile;
    private final File lexificationFile;
    private final File unitFile;
    private final CodeArray<CacheRef> lexificationRefMap;
    private final Map<OccurrencesKey, CacheRef> occurrencesRefMap;

    public DirectoryLexieCacheReaderFactory(File occurrencesFile, File lexificationFile, File unitFile, CodeArray<CacheRef> lexificationRefMap, Map<OccurrencesKey, CacheRef> occurrencesRefMap) {
        this.occurrencesFile = occurrencesFile;
        this.lexificationFile = lexificationFile;
        this.unitFile = unitFile;
        this.lexificationRefMap = lexificationRefMap;
        this.occurrencesRefMap = occurrencesRefMap;
    }

    @Override
    public LexieCacheReader newReader() {
        return new InternalLexieCacheReader();
    }

    public static LexieCacheReaderFactory loadFromCache(File cacheDirectory) {
        File occurrencesFile = new File(cacheDirectory, "lexie-occurrences.primitives");
        if (!occurrencesFile.exists()) {
            return null;
        }
        File lexificationFile = new File(cacheDirectory, "lexie-lexification.primitives");
        if (!lexificationFile.exists()) {
            return null;
        }
        File unitFile = new File(cacheDirectory, "lexie-unit.primitives");
        if (!unitFile.exists()) {
            return null;
        }
        CodeArrayBuilder<CacheRef> lexificationCodeArrayBuilder = new CodeArrayBuilder<CacheRef>(65536);
        Map<OccurrencesKey, CacheRef> occurrencesRefMap = new HashMap<OccurrencesKey, CacheRef>();
        try {
            CacheUtils.checkLexificationRef(lexificationFile, lexificationCodeArrayBuilder);
            CacheUtils.checkOccurrenceRef(occurrencesFile, occurrencesRefMap);
        } catch (IOException ioe) {
            return null;
        }
        return new DirectoryLexieCacheReaderFactory(occurrencesFile, lexificationFile, unitFile, lexificationCodeArrayBuilder.toCodeArray(), occurrencesRefMap);
    }


    private class InternalLexieCacheReader implements LexieCacheReader {

        private RandomAccessFile occurrencesRandomAccessFile;
        private RandomAccessFile lexificationRandomAccessFile;

        private InternalLexieCacheReader() {
        }

        @Override
        public List<ScrutariLexieUnit> getScrutariLexieUnitList() {
            try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(unitFile))) {
                PrimitivesReader unitPrimitivesReader = PrimitivesIOFactory.newReader(is);
                int length = unitPrimitivesReader.readInt();
                ScrutariLexieUnit[] scrutariLexieUnitArray = new ScrutariLexieUnit[length];
                for (int i = 0; i < length; i++) {
                    scrutariLexieUnitArray[i] = LexieUtils.readScrutariLexieUnit(unitPrimitivesReader);
                }
                return LexieUtils.toScrutariLexieUnitList(scrutariLexieUnitArray);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void checkLexieOccurrences(LexieId lexieId, Integer subsetCode, IntPredicate codePredicate, Consumer<LexieOccurrences> occurrencesConsumer) {
            try {
                byte[] bytes = getLexieOccurrencesBytes(lexieId, subsetCode);
                if (bytes == null) {
                    return;
                }
                CacheUtils.checkLexieOccurrencesFromPrimitives(PrimitivesIOFactory.newReader(bytes), codePredicate, occurrencesConsumer);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public List<LexieOccurrences> getLexieOccurrencesList(LexieId lexieId, Integer subsetCode) {
            try {
                byte[] bytes = getLexieOccurrencesBytes(lexieId, subsetCode);
                if (bytes == null) {
                    return LexieUtils.EMPTY_LEXIEOCCURRENCESLIST;
                }
                return CacheUtils.lexieOccurrencesListFromPrimitives(PrimitivesIOFactory.newReader(bytes));
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void readFicheLexification(Integer ficheCode, FicheLexification ficheLexification) {
            try {
                byte[] bytes = getLexificationBytes(ficheCode);
                if (bytes != null) {
                    FicheLexification.fromPrimitives(PrimitivesIOFactory.newReader(bytes), ficheLexification);
                }
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void readMotcleLexification(Integer motcleCode, MotcleLexification motcleLexification) {
            try {
                byte[] bytes = getLexificationBytes(motcleCode);
                if (bytes != null) {
                    MotcleLexification.fromPrimitives(PrimitivesIOFactory.newReader(bytes), motcleLexification);
                }
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        @Override
        public void close() {
            try {
                if (occurrencesRandomAccessFile != null) {
                    occurrencesRandomAccessFile.close();
                    occurrencesRandomAccessFile = null;
                }
                if (lexificationRandomAccessFile != null) {
                    lexificationRandomAccessFile.close();
                    lexificationRandomAccessFile = null;
                }
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }

        private byte[] getLexificationBytes(Integer elementCode) throws IOException {
            CacheRef cacheRef = lexificationRefMap.get(elementCode);
            if (cacheRef == null) {
                return null;
            }
            if (lexificationRandomAccessFile == null) {
                lexificationRandomAccessFile = new RandomAccessFile(lexificationFile, "r");
            }
            return CacheRef.read(lexificationRandomAccessFile, cacheRef);
        }

        private byte[] getLexieOccurrencesBytes(LexieId lexieId, Integer subsetCode) throws IOException {
            CacheRef cacheRef = occurrencesRefMap.get(new OccurrencesKey(lexieId.getValue(), subsetCode));
            if (cacheRef == null) {
                return null;
            }
            if (occurrencesRandomAccessFile == null) {
                occurrencesRandomAccessFile = new RandomAccessFile(occurrencesFile, "r");
            }
            return CacheRef.read(occurrencesRandomAccessFile, cacheRef);
        }

    }

}
