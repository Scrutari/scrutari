/* SctServer_Directory - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.feed.SctFeedCache;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.SortedSet;
import java.util.TreeSet;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public class DirectorySctFeedCache implements SctFeedCache {

    private final File cacheDirectory;

    public DirectorySctFeedCache(File cacheDirectory) {
        this.cacheDirectory = cacheDirectory;
    }

    @Override
    public synchronized String getFeedString(SctFeedOptions sctFeedOptions) {
        String feedName = toString(sctFeedOptions);
        if (sctFeedOptions.getSctFeedType() == SctFeedConstants.FICHES_SCTFEED_TYPE) {
            String requestString = toString(sctFeedOptions.getRequestMap());
            File requestListFile = new File(cacheDirectory, feedName + ".txt");
            if (requestListFile.exists()) {
                try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(requestListFile), "UTF-8"))) {
                    String ligne;
                    int lineNumber = 0;
                    String feedString = null;
                    while ((ligne = bufReader.readLine()) != null) {
                        lineNumber++;
                        if (ligne.equals(requestString)) {
                            File fichesAtom = new File(cacheDirectory, feedName + "_" + lineNumber + ".atom");
                            if (fichesAtom.exists()) {
                                try (FileInputStream is = new FileInputStream(fichesAtom)) {
                                    feedString = IOUtils.toString(is, "UTF-8");
                                }
                                break;
                            }
                        }
                    }
                    return feedString;
                } catch (IOException ioe) {
                    throw new SctStorageException(ioe);
                }
            } else {
                return null;
            }
        } else {
            File f = new File(cacheDirectory, feedName + ".atom");
            if (f.exists()) {
                try (FileInputStream is = new FileInputStream(f)) {
                    return IOUtils.toString(is, "UTF-8");
                } catch (IOException ioe) {
                    throw new SctStorageException(ioe);
                }
            } else {
                return null;
            }
        }

    }

    @Override
    public synchronized void saveFeedString(SctFeedOptions sctFeedOptions, String feedString) {
        String feedName = toString(sctFeedOptions);
        File atomFile;
        if (sctFeedOptions.getSctFeedType() == SctFeedConstants.FICHES_SCTFEED_TYPE) {
            String requestString = toString(sctFeedOptions.getRequestMap());
            int lineNumber = getLineNumber(feedName, requestString);
            atomFile = new File(cacheDirectory, feedName + "_" + lineNumber + ".atom");
        } else {
            atomFile = new File(cacheDirectory, feedName + ".atom");
        }
        try (FileOutputStream os = new FileOutputStream(atomFile)) {
            IOUtils.write(feedString, os, "UTF-8");
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    private int getLineNumber(String feedName, String requestString) {
        File requestListFile = new File(cacheDirectory, feedName + ".txt");
        int lineNumber = 0;
        if (requestListFile.exists()) {
            try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(requestListFile), "UTF-8"))) {
                String ligne;
                while ((ligne = bufReader.readLine()) != null) {
                    lineNumber++;
                    if (ligne.equals(requestString)) {
                        return lineNumber;
                    }
                }
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }
        lineNumber++;
        try (FileOutputStream foe = new FileOutputStream(requestListFile, true)) {
            IOUtils.write(requestString + '\n', foe, "UTF-8");
            return lineNumber;
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    private static String toString(SctFeedOptions sctFeedOptions) {
        StringBuilder buf = new StringBuilder();
        buf.append((int) sctFeedOptions.getSctFeedType());
        buf.append('-');
        buf.append(sctFeedOptions.getOptionMask());
        buf.append('_');
        buf.append(sctFeedOptions.getLang().toString());
        return buf.toString();

    }

    public static String toString(RequestMap requestMap) {
        SortedSet<String> set = new TreeSet<String>(requestMap.getParameterNameSet());
        StringBuilder buf = new StringBuilder();
        boolean next = false;
        for (String paramName : set) {
            if (next) {
                buf.append('\t');
            } else {
                next = true;
            }
            buf.append(paramName);
            buf.append('=');
            String[] values = requestMap.getParameterValues(paramName);
            int length = values.length;
            for (int i = 0; i < length; i++) {
                if (i > 0) {
                    buf.append(';');
                }
                String value = values[i];
                int valLength = value.length();
                for (int j = 0; j < valLength; j++) {
                    char carac = value.charAt(j);
                    switch (carac) {
                        case ' ':
                        case '\n':
                        case '\t':
                        case '\r':
                            buf.append(' ');
                            break;
                        default:
                            buf.append(carac);
                    }
                }
            }
        }
        if (buf.length() == 0) {
            return "-";
        }
        return buf.toString();
    }

}
