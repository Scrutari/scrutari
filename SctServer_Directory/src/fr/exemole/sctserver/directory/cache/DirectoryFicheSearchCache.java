/* SctServer_Directory - Copyright (c) 2013-2015 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory.cache;

import fr.exemole.sctserver.api.storage.FicheSearchCache;
import fr.exemole.sctserver.api.storage.SctStorageException;
import java.io.File;
import java.io.IOException;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.tools.result.io.FicheSearchResultStorage;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryFicheSearchCache implements FicheSearchCache {

    private final File cacheDirectory;

    public DirectoryFicheSearchCache(File cacheDirectory) {
        this.cacheDirectory = cacheDirectory;
    }

    @Override
    public FicheSearchResult getFicheSearchResult(QId qId, LexieAccess lexieAccess) {
        File cache = getFile(qId, false);
        if (!cache.exists()) {
            return null;
        }
        FicheSearchResult searchResult;
        try {
            searchResult = FicheSearchResultStorage.read(cache, lexieAccess);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
        if (searchResult == null) {
            cache.delete();
        }
        return searchResult;
    }

    @Override
    public void saveFicheSearchResult(FicheSearchResult ficheSearchResult) {
        QId qId = ficheSearchResult.getFicheSearchSource().getQId();
        if (qId == null) {
            return;
        }
        File f = getFile(qId, true);
        try {
            FicheSearchResultStorage.write(f, ficheSearchResult);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    private File getFile(QId qId, boolean mkdirs) {
        StringBuilder buf = new StringBuilder();
        buf.append(qId.getScrutariDBName().toString());
        buf.append(File.separatorChar);
        int century = qId.getSubId() / 100;
        buf.append(century);
        buf.append("00");
        File dir = new File(cacheDirectory, buf.toString());
        if (mkdirs) {
            dir.mkdirs();
        }
        return new File(dir, qId.getSubId() + ".primitives");
    }

}
