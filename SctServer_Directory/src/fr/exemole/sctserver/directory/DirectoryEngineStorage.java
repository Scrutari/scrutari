/* SctServer_Directory - Copyright (c) 2008-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.directory;

import fr.exemole.sctserver.api.conf.EngineConf;
import fr.exemole.sctserver.api.feed.SctFeedCache;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.api.storage.FicheSearchCache;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.api.storage.QIdStorage;
import fr.exemole.sctserver.api.storage.SctStorageException;
import fr.exemole.sctserver.directory.cache.CacheUtils;
import fr.exemole.sctserver.directory.cache.DirectoryDataCacheReaderFactory;
import fr.exemole.sctserver.directory.cache.DirectoryDataCacheWriter;
import fr.exemole.sctserver.directory.cache.DirectoryFicheSearchCache;
import fr.exemole.sctserver.directory.cache.DirectoryLexieCacheReaderFactory;
import fr.exemole.sctserver.directory.cache.DirectoryLexieCacheWriter;
import fr.exemole.sctserver.directory.cache.DirectorySctFeedCache;
import fr.exemole.sctserver.directory.persistent.DirectoryLogStorage;
import fr.exemole.sctserver.directory.persistent.DirectoryQIdStorage;
import fr.exemole.sctserver.tools.conf.factory.EngineConfFactory;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.mapeadores.util.io.DirectoryResourceStorage;
import net.mapeadores.util.io.FileUtils;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.io.ResourceStorage;
import net.mapeadores.util.primitives.io.PrimitivesIOFactory;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.datauri.tree.io.TsvReader;
import net.scrutari.datauri.tree.io.TsvWriter;
import net.scrutari.db.api.AliasManager;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.caches.DataCacheReaderFactory;
import net.scrutari.db.api.caches.DataCacheWriter;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheWriter;
import net.scrutari.db.tools.AliasManagerBuilder;
import net.scrutari.db.tools.FieldRankManagerBuilder;


/**
 *
 * @author Vincent Calame
 */
public class DirectoryEngineStorage implements EngineStorage {

    private final static List<StoredData> EMPTY_DATALIST = Collections.emptyList();
    private final static String URLMAP_FILENAME = "urlmap.txt";
    private final File engineConfDir;
    private final File engineVarDir;
    private final File cacheDirectory;
    private final File dataDirectory;
    private final File uriDirectory;
    private final EngineConf engineConf;
    private final DirectoryQIdStorage qIdStorage;
    private final DirectoryLogStorage logStorage;
    private final ResourceStorage engineResourceStorage;

    public DirectoryEngineStorage(File confDir, File varDir) {
        this.engineConfDir = confDir;
        this.engineVarDir = varDir;
        this.engineConf = EngineConfFactory.fromDirectory(confDir);
        this.cacheDirectory = getDirectory("cache");
        this.dataDirectory = getDirectory("data");
        this.uriDirectory = getDirectory("uri");
        this.qIdStorage = new DirectoryQIdStorage(getDirectory("qid"), cacheDirectory);
        this.logStorage = new DirectoryLogStorage(getDirectory("logs"));
        this.engineResourceStorage = new DirectoryResourceStorage("engine", new File(confDir, "resources"));
    }

    @Override
    public DataCacheWriter getDataCacheWriter(ScrutariDBName scrutariDbName) {
        File dbCacheDirectory = new File(cacheDirectory, scrutariDbName.getName() + "/scrutaridb");
        dbCacheDirectory.mkdirs();
        return new DirectoryDataCacheWriter(dbCacheDirectory);
    }

    @Override
    public boolean hasConfBaseMetadata(String scrutariSourceName) {
        File file = getConfBaseMetadataFile(scrutariSourceName);
        return ((file.exists()) && (!file.isDirectory()));
    }

    @Override
    public InputStream getConfBaseMetadataInputstream(String scrutariSourceName) {
        File file = getConfBaseMetadataFile(scrutariSourceName);
        if ((file.exists()) && (!file.isDirectory())) {
            try {
                return new FileInputStream(file);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        } else {
            return null;
        }
    }

    @Override
    public LexieCacheWriter getLexieCacheWriter(ScrutariDBName scrutariDbName) {
        File dbCacheDirectory = new File(cacheDirectory, scrutariDbName.getName() + "/scrutaridb");
        dbCacheDirectory.mkdirs();
        return new DirectoryLexieCacheWriter(dbCacheDirectory);
    }

    @Override
    public SctFeedCache getSctFeedCache(ScrutariDBName scrutariDbName) {
        File feedCacheDirectory = new File(cacheDirectory, scrutariDbName.getName() + "/feed");
        feedCacheDirectory.mkdirs();
        return new DirectorySctFeedCache(feedCacheDirectory);
    }

    @Override
    public FicheSearchCache getFicheSearchCache(ScrutariDBName scrutariDbName) {
        File searchCacheDirectory = new File(cacheDirectory, scrutariDbName.getName() + "/search");
        searchCacheDirectory.mkdirs();
        return new DirectoryFicheSearchCache(searchCacheDirectory);
    }

    @Override
    public void cacheObject(ScrutariDBName scrutariDbName, Object obj) {
        File objectsDir = new File(cacheDirectory, scrutariDbName.getName() + "/objects");
        objectsDir.mkdirs();
        if (obj instanceof AliasManager) {
            File f = new File(objectsDir, "aliasmanager.primitives");
            try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(f))) {
                PrimitivesWriter dataPrimitivesWriter = PrimitivesIOFactory.newWriter(os);
                AliasManagerBuilder.toPrimitives((AliasManager) obj, dataPrimitivesWriter);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        } else if (obj instanceof FieldRankManager) {
            File f = new File(objectsDir, "fieldrankmanager.primitives");
            try (BufferedOutputStream os = new BufferedOutputStream(new FileOutputStream(f))) {
                PrimitivesWriter dataPrimitivesWriter = PrimitivesIOFactory.newWriter(os);
                FieldRankManagerBuilder.toPrimitives((FieldRankManager) obj, dataPrimitivesWriter);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }
    }

    @Override
    public Object getCachedObject(ScrutariDBName scrutariDbName, Class objectClass) {
        File objectsDir = new File(cacheDirectory, scrutariDbName.getName() + "/objects");
        if (!objectsDir.exists()) {
            return null;
        }
        if (objectClass.equals(AliasManager.class)) {
            File f = new File(objectsDir, "aliasmanager.primitives");
            if (!f.exists()) {
                return null;
            }
            try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(f))) {
                PrimitivesReader primReader = PrimitivesIOFactory.newReader(is);
                return AliasManagerBuilder.fromPrimitives(primReader);
            } catch (IOException ioe) {
                return null;
            }
        } else if (objectClass.equals(FieldRankManager.class)) {
            File f = new File(objectsDir, "fieldrankmanager.primitives");
            if (!f.exists()) {
                return null;
            }
            try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(f))) {
                PrimitivesReader primReader = PrimitivesIOFactory.newReader(is);
                return FieldRankManagerBuilder.fromPrimitives(primReader);
            } catch (IOException ioe) {
                return null;
            }
        }
        return null;
    }

    @Override
    public QIdStorage getQIdStorage() {
        return qIdStorage;
    }

    @Override
    public LogStorage getLogStorage() {
        return logStorage;
    }

    @Override
    public void cleanCache(Set<ScrutariDBName> remainingSet) {
        if (!cacheDirectory.exists()) {
            return;
        }
        File[] dirs = cacheDirectory.listFiles();
        int length = dirs.length;
        for (int i = 0; i < length; i++) {
            File dir = dirs[i];
            if (!dir.isDirectory()) {
                continue;
            }
            String name = dir.getName();
            boolean remain;
            try {
                ScrutariDBName dbName = ScrutariDBName.parse(name);
                remain = remainingSet.contains(dbName);
            } catch (ParseException pe) {
                continue;
            }
            if (!remain) {
                try {
                    IOUtils.cleanDirectory(dir);
                    dir.delete();
                } catch (IOException ioe) {
                    throw new SctStorageException(ioe);
                }
            }
        }
    }

    @Override
    public void cleanTransientCache(ScrutariDBName scrutariDbName) {
        File feedCacheDirectory = new File(cacheDirectory, scrutariDbName.getName() + "/feed");
        if (feedCacheDirectory.exists()) {
            try {
                IOUtils.cleanDirectory(feedCacheDirectory);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }
        File searchCacheDirectory = new File(cacheDirectory, scrutariDbName.getName() + "/search");
        if (searchCacheDirectory.exists()) {
            try {
                IOUtils.cleanDirectory(searchCacheDirectory);
            } catch (IOException ioe) {
                throw new SctStorageException(ioe);
            }
        }
    }

    @Override
    public EngineConf getEngineConf() {
        return engineConf;
    }

    private File getDirectory(String name) {
        File directory = new File(engineVarDir, name);
        testDirectory(directory);
        return directory;
    }

    private void testDirectory(File directory) {
        if (!directory.exists()) {
            directory.mkdirs();
        } else if (!directory.isDirectory()) {
            throw new SctStorageException("Not a directory: " + directory.getPath());
        }
    }

    @Override
    public List<StoredData> getDataList(String scrutariSourceName) {
        File dir = new File(dataDirectory, scrutariSourceName);
        if (!dir.exists()) {
            return EMPTY_DATALIST;
        }
        SortedMap<Integer, FileData> map = getDataFileMap(dir);
        EngineStorage.StoredData[] array = map.values().toArray(new EngineStorage.StoredData[map.size()]);
        return new StoredDataList(array);
    }

    @Override
    public boolean hasData(String scrutariSourceName, URL url) {
        File dir = new File(dataDirectory, scrutariSourceName);
        if (!dir.exists()) {
            return false;
        }
        Map<URL, Integer> map = getURLMap(dir);
        Integer number = map.get(url);
        if (number == null) {
            return false;
        }
        File file = new File(dir, "data-" + number + ".xml");
        return file.exists();
    }

    @Override
    public void saveScrutariData(String scrutariSourceName, List<MatchedData> matchedDataList) {
        File dir = new File(dataDirectory, scrutariSourceName);
        Map<URL, Integer> currentURLMap = getURLMap(dir);
        SortedMap<Integer, FileData> currentDataFileMap = getDataFileMap(dir);
        dir.mkdirs();
        StringBuilder buf = new StringBuilder();
        List<File> tempDataFileList = new ArrayList<File>();
        int newNumber = 0;
        for (MatchedData matchedData : matchedDataList) {
            newNumber++;
            File tempDataFile = new File(dir, "tempdata-" + newNumber);
            URL url = matchedData.getMatchingURL();
            if (matchedData.isNotChanged()) {
                Integer currentNumber = currentURLMap.get(url);
                if (currentNumber != null) {
                    FileData currentFileData = currentDataFileMap.get(currentNumber);
                    if (currentFileData != null) {
                        currentFileData.update(tempDataFile);
                        tempDataFileList.add(tempDataFile);
                    }
                }
            } else {
                try (InputStream is = matchedData.getInputStream(); OutputStream os = new FileOutputStream(tempDataFile)) {
                    IOUtils.copy(is, os);
                    tempDataFileList.add(tempDataFile);
                } catch (IOException ioe) {
                    throw new SctStorageException(ioe);
                }
            }
            buf.append(newNumber).append("=").append(url.toString()).append("\n");
        }
        Set<String> prefixSet = new HashSet<String>();
        for (File f : tempDataFileList) {
            String name = f.getName();
            String prefix = "data-" + name.substring(9);
            prefixSet.add(prefix);
            f.renameTo(new File(dir, prefix + ".xml"));
        }
        for (File f : dir.listFiles()) {
            String name = f.getName();
            if (name.startsWith("data-")) {
                int idx = name.indexOf(".");
                boolean delete = true;
                if (idx != -1) {
                    String prefix = name.substring(0, idx);
                    if (prefixSet.contains(prefix)) {
                        delete = false;
                    }
                }
                if (delete) {
                    f.delete();
                }
            }
        }
        try {
            FileUtils.writeStringToFile(new File(dir, URLMAP_FILENAME), buf.toString(), "UTF-8");
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public String getContent(String contentName) {
        File contentFile = getContentFile(contentName);
        if (contentFile == null) {
            return "";
        }
        return toString(contentFile);
    }

    @Override
    public void saveContent(String contentName, String content) {
        File contentFile = getContentFile(contentName);
        if (contentFile == null) {
            throw new IllegalArgumentException("Unknown content : " + contentName);
        }
        try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(contentFile), "UTF-8")) {
            IOUtils.write(content, writer);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void saveWholeURITree(URITree uriTree) {
        File file = getWholeUriTreeFile();
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            TsvWriter.write(writer, uriTree);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public void saveAddedURITree(URITree uriTree, ScrutariDBName scrutariDBName) {
        File file = getAddedUriTreeFile(scrutariDBName);
        try (BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"))) {
            TsvWriter.write(writer, uriTree);
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public URITree getWholeURITree(ScrutariDataURIChecker checker) {
        File file = getWholeUriTreeFile();
        if (!file.exists()) {
            return null;
        }
        WholeParser wholeParser = new WholeParser();
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "UTF-8")) {
            URITree uriTree = TsvReader.readURITree(reader, checker, wholeParser);
            return uriTree;
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public URITree getAddedURITree(ScrutariDBName scrutariDBName, ScrutariDataURIChecker checker) {
        File file = getAddedUriTreeFile(scrutariDBName);
        if (!file.exists()) {
            return null;
        }
        try (InputStreamReader reader = new InputStreamReader(new FileInputStream(file), "UTF-8")) {
            URITree uriTree = TsvReader.readURITree(reader, checker, null);
            return uriTree;
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    @Override
    public ScrutariDBName[] getAddScrutariDBNameArray() {
        Set<ScrutariDBName> set = new TreeSet<ScrutariDBName>();
        File[] dirs = uriDirectory.listFiles();
        int length = dirs.length;
        for (int i = 0; i < length; i++) {
            File dir = dirs[i];
            if (dir.isDirectory()) {
                String year = dir.getName();
                File[] files = dir.listFiles();
                int fileLength = files.length;
                for (int j = 0; j < fileLength; j++) {
                    File f = files[j];
                    String name = f.getName();
                    if (name.endsWith(".txt")) {
                        name = name.substring(0, name.length() - 4);
                        try {
                            ScrutariDBName scrutariDBName = ScrutariDBName.parse(name);
                            if (year.equals(scrutariDBName.getYear())) {
                                set.add(scrutariDBName);
                            }
                        } catch (ParseException pe) {
                        }
                    }
                }

            }
        }
        int size = set.size();
        int p = size - 1;
        ScrutariDBName[] result = new ScrutariDBName[size];
        for (ScrutariDBName scrutariDBName : set) {
            result[p] = scrutariDBName;
            p--;
        }
        return result;
    }

    @Override
    public CacheCheck checkLastCache(ScrutariDBName currentName) {
        SortedMap<ScrutariDBName, File> availableMap = new TreeMap<ScrutariDBName, File>();
        for (File dateCacheDir : cacheDirectory.listFiles()) {
            if (!dateCacheDir.isDirectory()) {
                continue;
            }
            try {
                ScrutariDBName scrutariDBName = ScrutariDBName.parse(dateCacheDir.getName());
                if ((currentName != null) && (scrutariDBName.compareTo(currentName) <= 0)) {
                    continue;
                }
                availableMap.put(scrutariDBName, dateCacheDir);
            } catch (ParseException pe) {
            }
        }
        if (availableMap.isEmpty()) {
            return null;
        }
        ScrutariDBName lastName = availableMap.lastKey();
        File dateCacheDir = availableMap.get(lastName);
        int version = CacheUtils.getCacheVersion(dateCacheDir);
        if (version != CacheUtils.CACHE_VERSION) {
            return null;
        }
        File scrutariDBDir = new File(dateCacheDir, "scrutaridb");
        DataCacheReaderFactory dataCacheReaderFactory = DirectoryDataCacheReaderFactory.loadFromCache(scrutariDBDir);
        if (dataCacheReaderFactory == null) {
            return null;
        }
        LexieCacheReaderFactory lexieCacheReaderFactory = DirectoryLexieCacheReaderFactory.loadFromCache(scrutariDBDir);
        if (lexieCacheReaderFactory == null) {
            return null;
        }
        return new InternalCacheCheck(lastName, dataCacheReaderFactory, lexieCacheReaderFactory);
    }

    @Override
    public ResourceStorage getEngineResourceStorage() {
        return engineResourceStorage;
    }

    private File getWholeUriTreeFile() {
        uriDirectory.mkdirs();
        return new File(uriDirectory, "tree.txt");
    }

    private File getAddedUriTreeFile(ScrutariDBName scrutariDBName) {
        File dir = new File(uriDirectory, scrutariDBName.getYear());
        dir.mkdirs();
        return new File(dir, scrutariDBName.getName() + ".txt");
    }

    private File getContentFile(String contentName) {
        if (contentName.equals(EngineStorage.STATUS_NAME)) {
            dataDirectory.mkdirs();
            return new File(dataDirectory, "status.txt");
        }
        return null;
    }

    private File getConfBaseMetadataFile(String scrutariSourceName) {
        return new File(engineConfDir, "base-metadata" + File.separatorChar + scrutariSourceName + ".xml");
    }

    private String toString(File file) {
        if (!file.exists()) {
            return "";
        }
        try (FileInputStream is = new FileInputStream(file)) {
            String content = IOUtils.toString(is, "UTF-8");
            return content;
        } catch (IOException ioe) {
            throw new SctStorageException(ioe);
        }
    }

    private Map<URL, Integer> getURLMap(File directory) {
        String urlmapString = toString(new File(directory, URLMAP_FILENAME));
        Map<URL, Integer> resultMap = new HashMap<URL, Integer>();
        String[] tokens = StringUtils.getLineTokens(urlmapString, StringUtils.EMPTY_EXCLUDE);
        for (String line : tokens) {
            int idx = line.indexOf('=');
            if (idx > 0) {
                try {
                    int number = Integer.parseInt(line.substring(0, idx).trim());
                    URL url = new URL(line.substring(idx + 1).trim());
                    resultMap.put(url, number);
                } catch (NumberFormatException | MalformedURLException e) {

                }
            }
        }
        return resultMap;
    }

    private SortedMap<Integer, FileData> getDataFileMap(File directory) {
        SortedMap<Integer, FileData> map = new TreeMap<Integer, FileData>();
        if (!directory.exists()) {
            return map;
        }
        Pattern pattern = Pattern.compile("^data-([0-9]+).xml$");
        for (File f : directory.listFiles()) {
            if (f.isDirectory()) {
                continue;
            }
            String name = f.getName();
            Matcher matcher = pattern.matcher(name);
            if (matcher.matches()) {
                File backupFile = new File(directory, name + ".bak");
                File errorFile = new File(directory, name + ".err");
                map.put(Integer.parseInt(matcher.group(1)), new FileData(f, backupFile, errorFile));
            }
        }
        return map;
    }

    private void migrate() {
        //Révision 1750 (2013-11-30) : classement des fichiers d'uri par année plutôt que dans un répertoire unique add
        File dir = new File(uriDirectory, "add");
        if (!dir.exists()) {
            return;
        }
        File[] files = dir.listFiles();
        int length = files.length;
        for (int i = 0; i < length; i++) {
            File f = files[i];
            if (f.isDirectory()) {
                continue;
            }
            String name = f.getName();
            if (name.endsWith(".txt")) {
                try {
                    ScrutariDBName scrutariDBName = ScrutariDBName.parse(name.substring(0, name.length() - 4));
                    File destDir = new File(uriDirectory, scrutariDBName.getYear());
                    destDir.mkdirs();
                    f.renameTo(new File(destDir, name));
                } catch (ParseException pe) {
                }
            }
        }
        try {
            IOUtils.forceDelete(dir);
        } catch (IOException ioe) {
        }
    }


    private static class InternalCacheCheck implements CacheCheck {

        private final ScrutariDBName scrutariDBName;
        private final DataCacheReaderFactory dataCacheReaderFactory;
        private final LexieCacheReaderFactory lexieCacheReaderFactory;

        private InternalCacheCheck(ScrutariDBName scrutariDBName, DataCacheReaderFactory dataCacheReaderFactory, LexieCacheReaderFactory lexieCacheReaderFactory) {
            this.scrutariDBName = scrutariDBName;
            this.dataCacheReaderFactory = dataCacheReaderFactory;
            this.lexieCacheReaderFactory = lexieCacheReaderFactory;
        }

        @Override
        public ScrutariDBName getScrutariDBName() {
            return scrutariDBName;
        }

        @Override
        public DataCacheReaderFactory getDataCacheReaderFactory() {
            return dataCacheReaderFactory;
        }

        @Override
        public LexieCacheReaderFactory getLexieCacheReaderFactory() {
            return lexieCacheReaderFactory;
        }

    }


    private static class StoredDataList extends AbstractList<EngineStorage.StoredData> implements RandomAccess {

        private final EngineStorage.StoredData[] array;

        private StoredDataList(EngineStorage.StoredData[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public EngineStorage.StoredData get(int index) {
            return array[index];
        }

    }


    private static class FileData implements EngineStorage.StoredData {

        private final File file;
        private final File backupFile;
        private final File errorFile;

        private FileData(File file, File backupFile, File errorFile) {
            this.file = file;
            this.backupFile = backupFile;
            this.errorFile = errorFile;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return new FileInputStream(file);
        }

        @Override
        public boolean hasBackup() {
            return ((backupFile.exists()) && (!backupFile.isDirectory()));
        }

        @Override
        public boolean restoreBackup() throws IOException {
            if (!hasBackup()) {
                return false;
            }
            FileUtils.copyFile(file, errorFile);
            FileUtils.copyFile(backupFile, file);
            return true;
        }

        @Override
        public void backup() throws IOException {
            if (backupFile.exists()) {
                FileUtils.deleteQuietly(backupFile);
            }
            FileUtils.copyFile(file, backupFile);
        }

        public void update(File tmpFile) {
            if (errorFile.exists()) {
                FileUtils.deleteQuietly(errorFile);
            }
            file.renameTo(tmpFile);
        }

    }


    private static class WholeParser extends TsvReader.AssociatedObjectParser {

        private WholeParser() {

        }

        @Override
        public Object parse(int lineNumber, String associatedString) {
            try {
                return Integer.parseInt(associatedString);
            } catch (NumberFormatException nfe) {
                return null;
            }
        }

    }

}
