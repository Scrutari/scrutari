/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh;


/**
 *
 * @author Vincent Calame
 */
public class PmhErrorException extends Exception {

    private final String verb;
    private final String errorCode;
    private final String errorText;

    public PmhErrorException(String verb, String errorCode, String errorText) {
        super(verb + "/" + errorCode);
        this.verb = verb;
        this.errorCode = errorCode;
        this.errorText = errorText;
    }

    public String getVerb() {
        return verb;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorText() {
        return errorText;
    }

}
