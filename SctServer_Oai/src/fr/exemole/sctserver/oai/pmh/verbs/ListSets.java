/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh.verbs;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.pmh.PmhErrorException;
import fr.exemole.sctserver.oai.pmh.PmhWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.scrutari.searchengine.api.options.Category;


/**
 *
 * @author Vincent Calame
 */
public class ListSets extends PmhWriter {

    public final static String VERB = "ListSets";
    private final List<Category> categoryList;

    private ListSets(SctEngine sctEngine, RequestMap requestMap, Lang lang, List<Category> categoryList) {
        super(sctEngine, requestMap, lang);
        this.categoryList = categoryList;
    }

    @Override
    public String getVerb() {
        return VERB;
    }

    @Override
    public void write(AppendableXMLWriter xmlWriter) throws IOException {
        xmlWriter
                .openTag(VERB);
        for (Category category : categoryList) {
            xmlWriter
                    .openTag("set")
                    .addSimpleElement("setSpec", category.getCategoryDef().getName())
                    .addSimpleElement("setName", category.getCategoryDef().getTitle(lang))
                    .closeTag("set");
        }
        xmlWriter
                .closeTag(VERB);
    }

    public static PmhWriter build(SctEngine sctEngine, RequestMap requestMap, Lang lang) throws PmhErrorException {
        List<Category> categoryList = sctEngine.getScrutariSession().getGlobalSearchOptions().getCategoryList();
        List<Category> filteredCategoryList = new ArrayList<Category>();
        for (Category category : categoryList) {
            if (category.getCountStats().getFicheCount() > 0) {
                filteredCategoryList.add(category);
            }
        }
        if (filteredCategoryList.isEmpty()) {
            throw new PmhErrorException(ListSets.VERB, "noSetHierarchy", "This repository does not support sets");
        }
        ListSets writer = new ListSets(sctEngine, requestMap, lang, filteredCategoryList);
        return writer;
    }

}
