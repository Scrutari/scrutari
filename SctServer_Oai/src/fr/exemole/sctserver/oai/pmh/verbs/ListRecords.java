/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh.verbs;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.pmh.PmhErrorException;
import fr.exemole.sctserver.oai.pmh.PmhWriter;
import java.io.IOException;
import java.util.Collection;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.tools.options.OptionsUtils;


/**
 *
 * @author Vincent Calame
 */
public class ListRecords extends PmhWriter {

    public final static String VERB = "ListRecords";
    private SearchOptions searchOptions;

    private ListRecords(SctEngine sctEngine, RequestMap requestMap, Lang lang) {
        super(sctEngine, requestMap, lang);
    }

    @Override
    public String getVerb() {
        return VERB;
    }

    @Override
    public void write(AppendableXMLWriter xmlWriter) throws IOException {
        try (DataAccess dataAccess = sctEngine.getScrutariSession().getScrutariDB().openDataAccess()) {
            Collection<Integer> ficheCodes = OptionsUtils.buildFicheCodeList(dataAccess, searchOptions);
            if (ficheCodes.isEmpty()) {
                writeError(xmlWriter, "noRecordsMatch", "No records match");
            } else {
                checkSubjectOption(dataAccess);
                xmlWriter
                        .openTag(VERB);
                for (Integer code : ficheCodes) {
                    writeRecord(xmlWriter, dataAccess.getFicheInfo(code), dataAccess);
                }
                xmlWriter
                        .closeTag(VERB);
            }
        }
    }

    public static PmhWriter build(SctEngine sctEngine, RequestMap requestMap, Lang lang) throws PmhErrorException {
        ListRecords writer = new ListRecords(sctEngine, requestMap, lang);
        writer.checkMetadataPrefix();
        writer.searchOptions = writer.checkSearchOptions();
        return writer;
    }

}
