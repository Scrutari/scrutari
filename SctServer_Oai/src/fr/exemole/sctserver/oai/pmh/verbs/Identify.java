/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh.verbs;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.OaiSpace;
import fr.exemole.sctserver.oai.pmh.PmhWriter;
import java.io.IOException;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class Identify extends PmhWriter {

    public final static String VERB = "Identify";
    private final EngineMetadata engineMetadata;

    private Identify(SctEngine sctEngine, Lang lang) {
        super(sctEngine, null, lang);
        this.engineMetadata = sctEngine.getEngineMetadata();
    }

    @Override
    public String getVerb() {
        return VERB;
    }


    @Override
    public void write(AppendableXMLWriter xmlWriter) throws IOException {
        xmlWriter.openTag(VERB);
        xmlWriter
                .addSimpleElement("repositoryName", engineMetadata.getTitleLabels().seekLabelString(lang, sctEngine.getEngineName()))
                .addSimpleElement("baseURL", getURL())
                .addSimpleElement("protocolVersion", "2.0");
        Attribute email = engineMetadata.getAttributes().getAttribute(OaiSpace.ADMIN_EMAIL_KEY);
        if (email != null) {
            for (String value : email) {
                xmlWriter.addSimpleElement("adminEmail", value);
            }
        }
        xmlWriter
                .addSimpleElement("earliestDatestamp", getEarliestDatestamp())
                .addSimpleElement("deletedRecord", "no")
                .addSimpleElement("granularity", "YYYY-MM-DD");
        xmlWriter.closeTag(VERB);
    }

    private String getEarliestDatestamp() {
        Attribute earliestAttribute = engineMetadata.getAttributes().getAttribute(OaiSpace.EARLIEST_DATESTAMP_KEY);
        if (earliestAttribute != null) {
            return earliestAttribute.getFirstValue();
        }
        ScrutariDBName[] addArray = sctEngine.getEngineStorage().getAddScrutariDBNameArray();
        int length = addArray.length - 1;
        if (length > 0) {
            return addArray[length - 1].toDayFormat();
        }
        return sctEngine.getScrutariSession().getScrutariDB().getScrutariDBName().toDayFormat();
    }

    public static PmhWriter build(SctEngine sctEngine, Lang lang) {
        return new Identify(sctEngine, lang);
    }

}
