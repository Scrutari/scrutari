/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh.verbs;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.pmh.PmhErrorException;
import fr.exemole.sctserver.oai.pmh.PmhWriter;
import java.io.IOException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.AppendableXMLWriter;


/**
 *
 * @author Vincent Calame
 */
public class ListMetadataFormats extends PmhWriter {

    public final static String VERB = "ListMetadataFormats";

    private ListMetadataFormats(SctEngine sctEngine, RequestMap requestMap, Lang lang) {
        super(sctEngine, requestMap, lang);
    }

    @Override
    public String getVerb() {
        return VERB;
    }

    @Override
    public void write(AppendableXMLWriter xmlWriter) throws IOException {
        xmlWriter
                .openTag(VERB)
                .openTag("metadataFormat")
                .addSimpleElement("metadataPrefix", DC)
                .addSimpleElement("schema", "http://www.openarchives.org/OAI/2.0/oai_dc.xsd")
                .addSimpleElement("metadataNamespace", "http://www.openarchives.org/OAI/2.0/oai_dc/")
                .closeTag("metadataFormat")
                .openTag("metadataFormat")
                .addSimpleElement("metadataPrefix", DCTERMS)
                .addSimpleElement("schema", "http://dublincore.org/schemas/xmls/qdc/dcterms.xsd")
                .addSimpleElement("metadataNamespace", "http://purl.org/dc/terms/")
                .closeTag("metadataFormat")
                .closeTag(VERB);
    }


    public static PmhWriter build(SctEngine sctEngine, RequestMap requestMap, Lang lang) throws PmhErrorException {
        ListMetadataFormats writer = new ListMetadataFormats(sctEngine, requestMap, lang);
        writer.checkIdentifier(false);
        return writer;
    }

}
