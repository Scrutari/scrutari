/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh.verbs;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.pmh.PmhErrorException;
import fr.exemole.sctserver.oai.pmh.PmhWriter;
import java.io.IOException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;


/**
 *
 * @author Vincent Calame
 */
public class GetRecord extends PmhWriter {

    public final static String VERB = "GetRecord";
    private FicheInfo ficheInfo;

    private GetRecord(SctEngine sctEngine, RequestMap requestMap, Lang lang) {
        super(sctEngine, requestMap, lang);
    }

    @Override
    public String getVerb() {
        return VERB;
    }

    @Override
    public void write(AppendableXMLWriter xmlWriter) throws IOException {
        xmlWriter
                .openTag(VERB);
        try (DataAccess dataAccess = sctEngine.getScrutariSession().getScrutariDB().openDataAccess()) {
            checkSubjectOption(dataAccess);
            writeRecord(xmlWriter, ficheInfo, dataAccess);
        }
        xmlWriter
                .closeTag(VERB);
    }

    private void setFicheInfo(FicheInfo ficheInfo) {
        this.ficheInfo = ficheInfo;
    }

    public static PmhWriter build(SctEngine sctEngine, RequestMap requestMap, Lang lang) throws PmhErrorException {
        GetRecord writer = new GetRecord(sctEngine, requestMap, lang);
        writer.checkMetadataPrefix();
        writer.setFicheInfo(writer.checkIdentifier(true));
        return writer;
    }


}
