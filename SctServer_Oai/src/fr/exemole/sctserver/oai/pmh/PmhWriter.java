/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.OaiSpace;
import fr.exemole.sctserver.tools.AddURITreeProvider;
import fr.exemole.sctserver.tools.BuildingUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.AttributesBuilder;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.SimpleMessageHandler;
import net.mapeadores.util.logicaloperation.OperandException;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLWriter;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.SctSpace;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityConstants;
import net.scrutari.searchengine.api.operands.eligibility.EligibilityOperand;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.ListReduction;
import net.scrutari.searchengine.api.options.SearchOptions;
import net.scrutari.searchengine.tools.operands.eligibility.EligibilityOperandParser;
import net.scrutari.searchengine.tools.options.ListReductionBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsBuilder;
import net.scrutari.searchengine.tools.options.SearchOptionsDefBuilder;


/**
 *
 * @author Vincent Calame
 */
public abstract class PmhWriter {

    public final static String DC = "oai_dc";
    public final static String DCTERMS = "oai_dcterms";
    private final static EligibilityOperandParser.CheckedScope ADD_SCOPE = EligibilityOperandParser.checkDefaultScope(EligibilityConstants.ADD_SCOPE);
    protected final SctEngine sctEngine;
    protected final RequestMap requestMap;
    protected final Lang lang;
    protected Map<String, String> argumentMap = new HashMap<String, String>();
    protected String format;
    private SubjectManager subjectManager;

    public PmhWriter(SctEngine sctEngine, RequestMap requestMap, Lang lang) {
        this.sctEngine = sctEngine;
        this.requestMap = requestMap;
        this.lang = lang;
    }

    public String getURL() {
        return sctEngine.getCanonicalUrl() + "api/oai/pmh";
    }

    public abstract String getVerb();

    public Map<String, String> getArgumentMap() {
        return argumentMap;
    }

    public FicheInfo checkIdentifier(boolean mandatory) throws PmhErrorException {
        String identifier = requestMap.getParameter("identifier");
        if (identifier == null) {
            if (mandatory) {
                throw new PmhErrorException(getVerb(), "badArgument", "identifier parameter is missing");
            } else {
                return null;
            }
        }
        ScrutariDataURI uri;
        try {
            uri = URIParser.parse(identifier);
        } catch (URIParseException pe) {
            throw new PmhErrorException(getVerb(), "badArgument", "malformed identifier parameter");
        }
        if (uri.getType() != ScrutariDataURI.FICHEURI_TYPE) {
            throw new PmhErrorException(getVerb(), "badArgument", "malformed identifier parameter");
        }
        FicheInfo ficheInfo;
        try (DataAccess dataAccess = sctEngine.getScrutariSession().getScrutariDB().openDataAccess()) {
            Integer code = dataAccess.getCode(uri);
            if (code == null) {
                throw new PmhErrorException(getVerb(), "idDoesNotExist", "The value of the identifier argument is unknown");
            }
            ficheInfo = dataAccess.getFicheInfo(code);
        }
        argumentMap.put("identifier", identifier);
        return ficheInfo;
    }

    public void checkMetadataPrefix() throws PmhErrorException {
        String metadataPrefix = requestMap.getParameter("metadataPrefix");
        if (metadataPrefix == null) {
            throw new PmhErrorException(getVerb(), "badArgument", "metadataPrefix parameter is missing");
        }
        switch (metadataPrefix) {
            case DC:
            case DCTERMS:
                format = metadataPrefix;
                break;
            default:
                throw new PmhErrorException(getVerb(), "cannotDisseminateFormat", "only oai_dc et oai_dcterms format are supported");
        }
        argumentMap.put("metadataPrefix", metadataPrefix);
    }

    public SearchOptions checkSearchOptions() throws PmhErrorException {
        GlobalSearchOptions globalSearchOptions = sctEngine.getScrutariSession().getGlobalSearchOptions();
        SearchOptionsDefBuilder searchOptionsDefBuilder = SearchOptionsDefBuilder.init()
                .setLang(lang);
        ListReduction corpusListReduction = getListReduction(OaiSpace.CORPUS_LIST_KEY, ScrutariDataURI.CORPUSURI_TYPE);
        if (corpusListReduction != null) {
            searchOptionsDefBuilder.putListReduction(corpusListReduction);
        }
        ListReduction baseListReduction = getListReduction(OaiSpace.BASE_LIST_KEY, ScrutariDataURI.BASEURI_TYPE);
        if (baseListReduction != null) {
            searchOptionsDefBuilder.putListReduction(baseListReduction);
        }
        String setParam = requestMap.getParameter("set");
        if (setParam != null) {
            if (!globalSearchOptions.isWithCategory()) {
                throw new PmhErrorException(getVerb(), "noSetHierarchy", "This scrutari engine has no category defined");
            }
            Category category = globalSearchOptions.getCategoryByName(setParam);
            if (category == null) {
                throw new PmhErrorException(getVerb(), "badArgument", "Unknown category (set parameter): " + setParam);
            }
            AttributesBuilder attributesBuilder = searchOptionsDefBuilder.getAttributesBuilder();
            attributesBuilder.appendValue(BuildingUtils.CATEGORY_LIST_KEY, category.getCategoryDef().getName());
        }
        String from = requestMap.getParameter("from");
        String until = requestMap.getParameter("until");
        if ((from != null) || (until != null)) {
            if (from == null) {
                from = "-";
            }
            if (until == null) {
                until = "-";
            }
            try {
                SimpleMessageHandler messageHandler = new SimpleMessageHandler();
                EligibilityOperand eligibilityOperand = EligibilityOperandParser.parse(from + "/" + until, ADD_SCOPE, messageHandler);
                searchOptionsDefBuilder.addFicheEligibilityOperand(eligibilityOperand);
                if (messageHandler.hasMessage()) {
                    throw new PmhErrorException(getVerb(), "badArgument", "form or until parameter malformed: " + messageHandler.toString());
                }
            } catch (OperandException pe) {
                throw new PmhErrorException(getVerb(), "badArgument", "form or until parameter malformed");
            }
        }
        SearchOptionsBuilder searchOptionsBuilder = new SearchOptionsBuilder(searchOptionsDefBuilder.toSearchOptionsDef());
        try (DataAccess dataAccess = sctEngine.getScrutariSession().getScrutariDB().openDataAccess()) {
            BuildingUtils.computeReduction(searchOptionsBuilder, dataAccess, globalSearchOptions);
            SimpleMessageHandler messageHandler = new SimpleMessageHandler();
            searchOptionsBuilder.checkEligibility(dataAccess, sctEngine.getScrutariSession(), messageHandler, new AddURITreeProvider(sctEngine.getEngineStorage()));
        }
        return searchOptionsBuilder.toSearchOptions();
    }

    public abstract void write(AppendableXMLWriter xmlWriter) throws IOException;

    public void checkSubjectOption(DataAccess dataAccess) {
        Attribute attribute = sctEngine.getEngineMetadata().getAttributes().getAttribute(OaiSpace.SUBJECT_THESAURUS_LIST_KEY);
        if (attribute != null) {
            List<Integer> thesaurusList = new ArrayList<Integer>();
            for (String value : attribute) {
                try {
                    ScrutariDataURI thesaurusURI = URIParser.parse(value);
                    if (thesaurusURI.getType() == ScrutariDataURI.THESAURUSURI_TYPE) {
                        Integer code = dataAccess.getCode(thesaurusURI);
                        if (code != null) {
                            thesaurusList.add(code);
                        }
                    }
                } catch (URIParseException pe) {

                }
            }
            if (!thesaurusList.isEmpty()) {
                subjectManager = new SubjectManager(lang, thesaurusList);
            }
        }
    }

    public void writeError(AppendableXMLWriter xmlWriter, String errorCode, String errorText) throws IOException {
        xmlWriter
                .startOpenTag("error")
                .addAttribute("code", errorCode)
                .endOpenTag()
                .addText(errorText)
                .closeTag("error", false);
    }

    public void writeRecord(AppendableXMLWriter xmlWriter, FicheInfo ficheInfo, DataAccess dataAccess) throws IOException {
        xmlWriter
                .openTag("record");
        writeHeader(xmlWriter, ficheInfo);
        writeData(xmlWriter, ficheInfo, dataAccess);
        xmlWriter
                .closeTag("record");
    }

    public void writeHeader(AppendableXMLWriter xmlWriter, FicheInfo ficheInfo) throws IOException {
        String spec = null;
        Category category = sctEngine.getScrutariSession().getGlobalSearchOptions().getCategoryByCorpus(ficheInfo.getCorpusCode());
        if (category != null) {
            spec = category.getCategoryDef().getName();
        }
        xmlWriter
                .openTag("header")
                .addSimpleElement("identifier", ficheInfo.getFicheURI().toString())
                .addSimpleElement("datestamp", ficheInfo.getFirstAdd().toDayFormat())
                .addSimpleElement("setSpec", spec)
                .closeTag("header");
    }

    public void writeData(AppendableXMLWriter xmlWriter, FicheInfo ficheInfo, DataAccess dataAccess) throws IOException {
        switch (format) {
            case DC:
                writeDCData(xmlWriter, ficheInfo, dataAccess);
                break;
            case DCTERMS:
                writeDCTermsData(xmlWriter, ficheInfo, dataAccess);
                break;
        }
    }

    public void writeDCData(AppendableXMLWriter xmlWriter, FicheInfo ficheInfo, DataAccess dataAccess) throws IOException {
        FicheData ficheData = dataAccess.getFicheData(ficheInfo.getCode());
        String ficheLang = ficheData.getLang().toString();
        if (ficheLang.equals("und")) {
            ficheLang = "";
        }
        xmlWriter
                .openTag("metadata")
                .startOpenTag("oai_dc:dc")
                .addAttribute("xmlns:oai_dc", "http://www.openarchives.org/OAI/2.0/oai_dc/")
                .addAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/")
                .addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                .addAttribute("xsi:schemaLocation", "http://www.openarchives.org/OAI/2.0/oai_dc/ http://www.openarchives.org/OAI/2.0/oai_dc.xsd")
                .endOpenTag();
        xmlWriter
                .addSimpleElement("dc:title", ficheData.getTitre(), ficheLang)
                .addSimpleElement("dc:description", ficheData.getSoustitre(), ficheLang)
                .addSimpleElement("dc:language", ficheLang)
                .addSimpleElement("dc:date", getDate(ficheData))
                .addSimpleElement("dc:identifier", ficheData.getHref(lang))
                .addSimpleElement("dc:type", "Text");
        Attribute authors = ficheData.getAttributes().getAttribute(SctSpace.AUTHORS_KEY);
        if (authors != null) {
            for (String value : authors) {
                xmlWriter
                        .addSimpleElement("dc:creator", value);
            }
        }
        if (subjectManager != null) {
            subjectManager.addSubject(dataAccess, ficheInfo, xmlWriter, "dc:subject");
        }
        xmlWriter
                .closeTag("oai_dc:dc")
                .closeTag("metadata");

    }

    public void writeDCTermsData(AppendableXMLWriter xmlWriter, FicheInfo ficheInfo, DataAccess dataAccess) throws IOException {
        FicheData ficheData = dataAccess.getFicheData(ficheInfo.getCode());
        String ficheLang = ficheData.getLang().toString();
        if (ficheLang.equals("und")) {
            ficheLang = "";
        }
        xmlWriter
                .openTag("metadata")
                .startOpenTag("oai_dcterms:dcterms")
                .addAttribute("xmlns:oai_dcterms", "http://www.openarchives.org/OAI/2.0/oai_dcterms/")
                .addAttribute("xmlns:dcterms", "http://purl.org/dc/terms/")
                .addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                .endOpenTag();
        xmlWriter
                .addSimpleElement("dcterms:title", ficheData.getTitre(), ficheLang)
                .addSimpleElement("dcterms:description", ficheData.getSoustitre(), ficheLang)
                .addSimpleElement("dcterms:language", ficheData.getLang().toString())
                .addSimpleElement("dcterms:date", getDate(ficheData))
                .addSimpleElement("dcterms:identifier", ficheData.getHref(lang))
                .addSimpleElement("dcterms:type", "Text");
        Attribute authors = ficheData.getAttributes().getAttribute(SctSpace.AUTHORS_KEY);
        if (authors != null) {
            for (String value : authors) {
                xmlWriter
                        .addSimpleElement("dcterms:creator", value);
            }
        }
        if (subjectManager != null) {
            subjectManager.addSubject(dataAccess, ficheInfo, xmlWriter, "dcterms:subject");
        }
        xmlWriter
                .closeTag("oai_dcterms:dcterms")
                .closeTag("metadata");

    }

    private String getDate(FicheData ficheData) {
        FuzzyDate date = ficheData.getDate();
        if (date == null) {
            return null;
        }
        return String.valueOf(date.getYear());
    }

    private ListReduction getListReduction(AttributeKey attributeKey, short uriType) {
        Attribute attribute = sctEngine.getEngineMetadata().getAttributes().getAttribute(attributeKey/*OaiSpace.CORPUS_LIST_KEY*/);
        if (attribute == null) {
            return null;
        }
        ListReductionBuilder builder = new ListReductionBuilder(uriType);
        for (String value : attribute) {
            try {
                ScrutariDataURI uri = URIParser.parse(value);
                if (uri.getType() == uriType) {
                    builder.add(uri);
                }
            } catch (URIParseException pe) {

            }
        }
        return builder.toListReduction();

    }


    private static class SubjectManager {

        private final Lang lang;
        private final List<Integer> thesaurusCodeList;
        private final Map<Integer, String> labelMap = new HashMap<Integer, String>();


        private SubjectManager(Lang lang, List<Integer> thesaurusCodeList) {
            this.lang = lang;
            this.thesaurusCodeList = thesaurusCodeList;
        }

        private void addSubject(DataAccess dataAccess, FicheInfo ficheInfo, XMLWriter xmlWriter, String tagName) throws IOException {
            for (Integer thesaurusCode : thesaurusCodeList) {
                List<Integer> motcleCodeList = ficheInfo.getIndexationCodeList(thesaurusCode);
                for (Integer motcleCode : motcleCodeList) {
                    String label = labelMap.get(motcleCode);
                    if (label == null) {
                        MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                        label = motcleData.getLabels().seekLabelString(lang, "");
                        labelMap.put(motcleCode, label);
                    }
                    xmlWriter.addSimpleElement(tagName, label);
                }

            }
        }

    }


}
