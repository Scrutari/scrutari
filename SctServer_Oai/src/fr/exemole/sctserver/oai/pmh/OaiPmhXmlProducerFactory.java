/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai.pmh;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.pmh.verbs.GetRecord;
import fr.exemole.sctserver.oai.pmh.verbs.Identify;
import fr.exemole.sctserver.oai.pmh.verbs.ListIdentifiers;
import fr.exemole.sctserver.oai.pmh.verbs.ListMetadataFormats;
import fr.exemole.sctserver.oai.pmh.verbs.ListRecords;
import fr.exemole.sctserver.oai.pmh.verbs.ListSets;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.SimpleTimeZone;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XmlProducer;


/**
 *
 * @author Vincent Calame
 */
public final class OaiPmhXmlProducerFactory {

    private final static SimpleDateFormat ISO_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    static {
        ISO_FORMAT.setTimeZone(new SimpleTimeZone(0, "GMT"));
    }

    private OaiPmhXmlProducerFactory() {

    }

    public static XmlProducer getXmlProducer(SctEngine sctEngine, RequestMap requestMap, Lang lang) {
        PmhWriter pmhWriter = getPmhWriter(sctEngine, requestMap, lang);
        return new OaiPmhXmlProducer(pmhWriter);
    }

    private static PmhWriter getPmhWriter(SctEngine sctEngine, RequestMap requestMap, Lang lang) {
        String verb = requestMap.getParameter("verb");
        try {
            if (verb == null) {
                throw new PmhErrorException(null, "badVerb", "verb parameter is missing");
            }
            switch (verb) {
                case GetRecord.VERB:
                    return GetRecord.build(sctEngine, requestMap, lang);
                case Identify.VERB:
                    return Identify.build(sctEngine, lang);
                case ListIdentifiers.VERB:
                    return ListIdentifiers.build(sctEngine, requestMap, lang);
                case ListMetadataFormats.VERB:
                    return ListMetadataFormats.build(sctEngine, requestMap, lang);
                case ListRecords.VERB:
                    return ListRecords.build(sctEngine, requestMap, lang);
                case ListSets.VERB:
                    return ListSets.build(sctEngine, requestMap, lang);
                default:
                    throw new PmhErrorException(verb, "badVerb", "The verb '" + verb + "' provided in the request is illegal.");
            }
        } catch (PmhErrorException pee) {
            return new Error(sctEngine, lang, pee.getVerb(), pee.getErrorCode(), pee.getErrorText());
        }
    }


    private static class OaiPmhXmlProducer implements XmlProducer {

        private final PmhWriter pmhWriter;

        private OaiPmhXmlProducer(PmhWriter pmhWriter) {
            this.pmhWriter = pmhWriter;

        }

        @Override
        public void writeXml(Appendable appendable) throws IOException {
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(appendable, 0);
            xmlWriter
                    .appendXMLDeclaration();
            xmlWriter
                    .startOpenTag("OAI-PMH")
                    .addAttribute("xmlns", "http://www.openarchives.org/OAI/2.0/")
                    .addAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
                    .addAttribute("xsi:schemaLocation", "http://www.openarchives.org/OAI/2.0/ http://www.openarchives.org/OAI/2.0/OAI-PMH.xsd")
                    .endOpenTag();
            xmlWriter
                    .addSimpleElement("responseDate", ISO_FORMAT.format(new Date()));
            xmlWriter
                    .startOpenTag("request")
                    .addAttribute("verb", pmhWriter.getVerb());
            for (Map.Entry<String, String> entry : pmhWriter.getArgumentMap().entrySet()) {
                xmlWriter
                        .addAttribute(entry.getKey(), entry.getValue());
            }
            xmlWriter
                    .endOpenTag()
                    .addText(pmhWriter.getURL())
                    .closeTag("request", false);
            pmhWriter.write(xmlWriter);
            xmlWriter
                    .closeTag("OAI-PMH");
        }

    }


    private static class Error extends PmhWriter {

        private final String verb;
        private final String errorCode;
        private final String errorText;

        public Error(SctEngine sctEngine, Lang lang, String verb, String errorCode, String errorText) {
            super(sctEngine, null, lang);
            this.verb = verb;
            this.errorCode = errorCode;
            this.errorText = errorText;
        }

        @Override
        public String getVerb() {
            return verb;
        }

        @Override
        public void write(AppendableXMLWriter xmlWriter) throws IOException {
            writeError(xmlWriter, errorCode, errorText);
        }

    }

}
