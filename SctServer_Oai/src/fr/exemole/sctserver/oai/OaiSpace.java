/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai;

import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.CheckedNameSpace;


/**
 *
 * @author Vincent Calame
 */
public final class OaiSpace {

    public final static CheckedNameSpace OAI_NAMESPACE = CheckedNameSpace.build("oai");
    public final static AttributeKey EARLIEST_DATESTAMP_KEY = AttributeKey.build(OAI_NAMESPACE, "earliestDatestamp");
    public final static AttributeKey ADMIN_EMAIL_KEY = AttributeKey.build(OAI_NAMESPACE, "adminEmail");
    public final static AttributeKey CORPUS_LIST_KEY = AttributeKey.build(OAI_NAMESPACE, "corpuslist");
    public final static AttributeKey BASE_LIST_KEY = AttributeKey.build(OAI_NAMESPACE, "baselist");
    public final static AttributeKey SUBJECT_THESAURUS_LIST_KEY = AttributeKey.build(OAI_NAMESPACE, "thesauruslist_subject");

    private OaiSpace() {

    }

}
