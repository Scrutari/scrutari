/* SctServer_Oai - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.oai;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.oai.pmh.OaiPmhXmlProducerFactory;
import fr.exemole.sctserver.tools.EngineUtils;
import java.text.ParseException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.handlers.XmlResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class OaiResponseHandlerFactory {

    private OaiResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        int separatorIndex = relativePath.indexOf('/');
        if (separatorIndex == -1) {
            Lang lang = null;
            String action = relativePath;
            int langIndex = action.indexOf('-');
            if (langIndex > 0) {
                try {
                    lang = Lang.parse(action.substring(langIndex + 1));
                    action = action.substring(0, langIndex);
                } catch (ParseException pe) {
                    return null;
                }
            }
            if (lang == null) {
                lang = EngineUtils.getDefaultLang(engine);
            }
            switch (action) {
                case "pmh":
                    return XmlResponseHandler.init(OaiPmhXmlProducerFactory.getXmlProducer(engine, requestMap, lang));
            }
        }
        return null;
    }

}
