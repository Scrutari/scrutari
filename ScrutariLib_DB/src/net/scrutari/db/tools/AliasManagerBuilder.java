/* ScrutariLib_DB - Copyright (c) 2016-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.exceptions.ImplementationException;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.scrutari.data.URIPrimitives;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.db.api.AliasManager;


/**
 *
 * @author Vincent Calame
 */
public class AliasManagerBuilder {

    private final Map<String, BaseURI> mainMap = new HashMap<String, BaseURI>();
    private final Map<BaseURI, String> baseURIMap = new HashMap<BaseURI, String>();

    public AliasManagerBuilder() {

    }

    public void putMainBaseURI(String sourceName, BaseURI baseURI) {
        mainMap.put(sourceName, baseURI);
        baseURIMap.put(baseURI, sourceName);
    }

    public void putAliasBaseURI(String sourceName, BaseURI baseURI) {
        baseURIMap.put(baseURI, sourceName);
    }

    public boolean containsBaseURI(BaseURI baseURI) {
        return baseURIMap.containsKey(baseURI);
    }

    public String getSourceName(BaseURI baseURI) {
        return baseURIMap.get(baseURI);
    }

    public BaseURI getMainBaseURI(String sourceName) {
        return mainMap.get(sourceName);
    }

    public AliasManager toAliasManager() {
        Map<String, BaseURI> finalMainMap = new HashMap<String, BaseURI>(mainMap);
        Map<BaseURI, String> finalBaseURIMap = new HashMap<BaseURI, String>(baseURIMap);
        return new InternalAliasManager(finalMainMap, finalBaseURIMap);
    }


    public static AliasManager fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        AliasManagerBuilder aliasManagerBuilder = new AliasManagerBuilder();
        int mainSize = primitivesReader.readInt();
        for (int i = 0; i < mainSize; i++) {
            String sourceName = primitivesReader.readString();
            BaseURI baseURI = (BaseURI) URIPrimitives.readScrutariDataURI(primitivesReader);
            aliasManagerBuilder.mainMap.put(sourceName, baseURI);
        }
        int aliasSize = primitivesReader.readInt();
        for (int i = 0; i < aliasSize; i++) {
            BaseURI baseURI = (BaseURI) URIPrimitives.readScrutariDataURI(primitivesReader);
            String sourceName = primitivesReader.readString();
            aliasManagerBuilder.baseURIMap.put(baseURI, sourceName);
        }
        return aliasManagerBuilder.toAliasManager();
    }

    public static void toPrimitives(AliasManager aliasManager, PrimitivesWriter primitivesWriter) throws IOException {
        Set<String> sourceNameSet = aliasManager.getSourceNameSet();
        primitivesWriter.writeInt(sourceNameSet.size());
        for (String sourceName : sourceNameSet) {
            primitivesWriter.writeString(sourceName);
            URIPrimitives.writeScrutariDataURI(aliasManager.getMainBaseURI(sourceName), primitivesWriter);
        }
        Set<BaseURI> baseURISet = aliasManager.getBaseURISet();
        primitivesWriter.writeInt(baseURISet.size());
        for (BaseURI baseURI : baseURISet) {
            URIPrimitives.writeScrutariDataURI(baseURI, primitivesWriter);
            primitivesWriter.writeString(aliasManager.getSourceName(baseURI));
        }
    }


    private static class InternalAliasManager implements AliasManager {

        private final Map<String, BaseURI> mainMap;
        private final Map<BaseURI, String> baseURIMap;

        private InternalAliasManager(Map<String, BaseURI> mainMap, Map<BaseURI, String> baseURIMap) {
            this.mainMap = mainMap;
            this.baseURIMap = baseURIMap;
        }

        @Override
        public BaseURI getMainBaseURI(String sourceName) {
            return mainMap.get(sourceName);
        }

        @Override
        public String getSourceName(BaseURI baseURI) {
            return baseURIMap.get(baseURI);
        }

        @Override
        public Set<String> getSourceNameSet() {
            return Collections.unmodifiableSet(mainMap.keySet());
        }

        @Override
        public Set<BaseURI> getBaseURISet() {
            return Collections.unmodifiableSet(baseURIMap.keySet());
        }

        @Override
        public ScrutariDataURI checkURI(ScrutariDataURI uri) {
            BaseURI baseURI = uri.getBaseURI();
            String sourceName = baseURIMap.get(baseURI);
            if (sourceName == null) {
                return uri;
            }
            BaseURI mainURI = mainMap.get(sourceName);
            if (mainURI == null) {
                return uri;
            }
            if (mainURI.equals(baseURI)) {
                return uri;
            }
            switch (uri.getType()) {
                case ScrutariDataURI.BASEURI_TYPE:
                    return mainURI;
                case ScrutariDataURI.CORPUSURI_TYPE:
                    return ((CorpusURI) uri).derive(mainURI);
                case ScrutariDataURI.THESAURUSURI_TYPE:
                    return ((ThesaurusURI) uri).derive(mainURI);
                case ScrutariDataURI.FICHEURI_TYPE:
                    FicheURI ficheURI = (FicheURI) uri;
                    CorpusURI newCorpusURI = ficheURI.getCorpusURI().derive(mainURI);
                    return ficheURI.derive(newCorpusURI);
                case ScrutariDataURI.MOTCLEURI_TYPE:
                    MotcleURI motcleURI = (MotcleURI) uri;
                    ThesaurusURI newThesaurusURI = motcleURI.getThesaurusURI().derive(mainURI);
                    return motcleURI.derive(newThesaurusURI);
                default:
                    throw new ImplementationException("Unknown implementation: " + uri.getClass().getName());
            }
        }

    }

}
