/* ScrutariLib_DB - Copyright (c) 2005-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.CodeArray;
import net.mapeadores.util.primitives.CodeArrayBuilder;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.primitives.PrimUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.IndexationData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelSpace;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.DataAccessProvider;
import net.scrutari.db.api.DataInfo;
import net.scrutari.db.api.FicheInfo;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.caches.CacheIterator;
import net.scrutari.db.api.caches.DataCacheReader;
import net.scrutari.db.api.caches.DataCacheReaderFactory;


/**
 * Dans toutes les fonctions d'ajout, on suppose qu'il n'y a pas de doublons.
 *
 * @author Vincent Calame
 */
public class DataAccessProviderBuilder {

    private DataAccessProviderBuilder() {
    }

    public static DataAccessProvider build(DataCacheReaderFactory dataCacheReaderFactory, ScrutariDataURIChecker scrutariDataURIChecker, FirstAddProvider firstAddProvider) {
        Map<ScrutariDataURI, Integer> codeByScrutariDataURIMap = new HashMap<ScrutariDataURI, Integer>(65536);
        Map<ScrutariDataURI, ScrutariDataURI> internMap = new HashMap<ScrutariDataURI, ScrutariDataURI>(2048);
        List<BaseInfo> baseInfoList = new ArrayList<BaseInfo>(256);
        List<CorpusInfo> corpusInfoList = new ArrayList<CorpusInfo>(1024);
        List<ThesaurusInfo> thesaurusInfoList = new ArrayList<ThesaurusInfo>(1024);
        CodeArrayBuilder<AbstractDataInfo> codeArrayBuilder = new CodeArrayBuilder<AbstractDataInfo>(65536);
        try (CacheIterator<BaseData> baseDataIterator = dataCacheReaderFactory.newBaseDataIterator()) {
            while (baseDataIterator.hasNext()) {
                BaseData baseData = baseDataIterator.next();
                Integer baseCode = baseData.getBaseCode();
                BaseURI baseURI = baseData.getBaseURI();
                BaseInfo baseInfo = new BaseInfo(baseCode, firstAddProvider.getFirstAdd(baseURI), baseData);
                baseInfoList.add(baseInfo);
                codeArrayBuilder.put(baseCode, baseInfo);
                internMap.put(baseURI, baseURI);
                codeByScrutariDataURIMap.put(baseURI, baseCode);
            }
        }
        try (CacheIterator<CorpusData> corpusDataIterator = dataCacheReaderFactory.newCorpusDataIterator()) {
            while (corpusDataIterator.hasNext()) {
                CorpusData corpusData = corpusDataIterator.next();
                Integer corpusCode = corpusData.getCorpusCode();
                CorpusURI corpusURI = corpusData.getCorpusURI();
                corpusURI = corpusURI.derive((BaseURI) internMap.get(corpusURI.getBaseURI()));
                CorpusInfo corpusInfo = new CorpusInfo(corpusCode, firstAddProvider.getFirstAdd(corpusURI), corpusData);
                corpusInfoList.add(corpusInfo);
                codeArrayBuilder.put(corpusCode, corpusInfo);
                internMap.put(corpusURI, corpusURI);
                codeByScrutariDataURIMap.put(corpusURI, corpusCode);
            }
        }
        try (CacheIterator<ThesaurusData> thesaurusDataIterator = dataCacheReaderFactory.newThesaurusDataIterator()) {
            while (thesaurusDataIterator.hasNext()) {
                ThesaurusData thesaurusData = thesaurusDataIterator.next();
                Integer thesaurusCode = thesaurusData.getThesaurusCode();
                ThesaurusURI thesaurusURI = thesaurusData.getThesaurusURI();
                thesaurusURI = thesaurusURI.derive((BaseURI) internMap.get(thesaurusURI.getBaseURI()));
                ThesaurusInfo thesaurusInfo = new ThesaurusInfo(thesaurusCode, firstAddProvider.getFirstAdd(thesaurusURI), thesaurusData);
                thesaurusInfoList.add(thesaurusInfo);
                codeArrayBuilder.put(thesaurusCode, thesaurusInfo);
                internMap.put(thesaurusURI, thesaurusURI);
                codeByScrutariDataURIMap.put(thesaurusURI, thesaurusCode);
            }
        }
        try (CacheIterator<FicheData> ficheDataIterator = dataCacheReaderFactory.newFicheDataIterator()) {
            while (ficheDataIterator.hasNext()) {
                FicheData ficheData = ficheDataIterator.next();
                Integer ficheCode = ficheData.getFicheCode();
                CorpusData corpusData = ((CorpusInfo) codeArrayBuilder.get(ficheData.getCorpusCode())).getCorpusData();
                FicheURI ficheURI = ficheData.getFicheURI();
                ficheURI = ficheURI.derive((CorpusURI) internMap.get(ficheURI.getCorpusURI()));
                codeArrayBuilder.put(ficheCode, new InternalFicheInfo(ficheCode, firstAddProvider.getFirstAdd(ficheURI), ficheURI, corpusData, ficheData.getLang()));
                codeByScrutariDataURIMap.put(ficheURI, ficheCode);
            }
        }
        try (CacheIterator<MotcleData> motcleDataIterator = dataCacheReaderFactory.newMotcleDataIterator()) {
            while (motcleDataIterator.hasNext()) {
                MotcleData motcleData = motcleDataIterator.next();
                Integer motcleCode = motcleData.getMotcleCode();
                ThesaurusData thesaurusData = ((ThesaurusInfo) codeArrayBuilder.get(motcleData.getThesaurusCode())).getThesaurusData();
                MotcleURI motcleURI = motcleData.getMotcleURI();
                motcleURI = motcleURI.derive((ThesaurusURI) internMap.get(motcleURI.getThesaurusURI()));
                codeArrayBuilder.put(motcleCode, new InternalMotcleInfo(motcleCode, firstAddProvider.getFirstAdd(motcleURI), motcleURI, thesaurusData));
                codeByScrutariDataURIMap.put(motcleURI, motcleCode);
            }
        }
        CodeArray<AbstractDataInfo> codeArray = codeArrayBuilder.toCodeArray();
        Set<Long> indexationSet = new HashSet<Long>();
        initIndexationAndRelation(dataCacheReaderFactory, codeArray, indexationSet);
        Set<String> relSet = initRelAttributeMap(dataCacheReaderFactory, codeByScrutariDataURIMap, codeArray, corpusInfoList, thesaurusInfoList);
        codeByScrutariDataURIMap = new HashMap<ScrutariDataURI, Integer>(codeByScrutariDataURIMap);
        return new InternalDataAccessProvider(dataCacheReaderFactory, scrutariDataURIChecker, codeArray,
                BaseInfo.toBaseDataList(baseInfoList), CorpusInfo.toCorpusDataList(corpusInfoList), ThesaurusInfo.toThesaurusDataList(thesaurusInfoList), codeByScrutariDataURIMap, indexationSet, relSet);
    }

    private static void initIndexationAndRelation(DataCacheReaderFactory dataCacheReaderFactory, CodeArray<AbstractDataInfo> codeArray, Set<Long> indexationSet) {
        try (CacheIterator<IndexationData> iterator = dataCacheReaderFactory.newIndexationDataIterator()) {
            while (iterator.hasNext()) {
                IndexationData indexationData = iterator.next();
                Integer ficheCode = indexationData.getFicheCode();
                Integer motcleCode = indexationData.getMotcleCode();
                InternalFicheInfo ficheInfo = ((InternalFicheInfo) codeArray.get(ficheCode));
                InternalMotcleInfo motcleInfo = ((InternalMotcleInfo) codeArray.get(motcleCode));
                Integer corpusCode = ficheInfo.getCorpusCode();
                Integer thesaurusCode = motcleInfo.getThesaurusCode();
                ficheInfo.addIndexation(thesaurusCode, motcleCode);
                motcleInfo.addIndexation(corpusCode, ficheCode);
                indexationSet.add(PrimUtils.toLong(corpusCode, thesaurusCode));
            }
        }
        try (CacheIterator<RelationData> iterator = dataCacheReaderFactory.newRelationDataIterator()) {
            while (iterator.hasNext()) {
                RelationData relationData = iterator.next();
                for (RelationData.Member member : relationData.getMemberList()) {
                    AbstractDataInfo dataInfo = codeArray.get(member.getCode());
                    dataInfo.addRelation(relationData, member.getRole());
                }
            }
        }
        for (Object obj : codeArray) {
            if ((obj != null) && (obj instanceof AbstractDataInfo)) {
                AbstractDataInfo dataInfo = (AbstractDataInfo) obj;
                dataInfo.flush();
            }
        }
    }

    private static Set<String> initRelAttributeMap(DataCacheReaderFactory dataCacheReaderFactory, Map<ScrutariDataURI, Integer> codeByScrutariDataURIMap, CodeArray<AbstractDataInfo> codeArray, List<CorpusInfo> corpusInfoList, List<ThesaurusInfo> thesaurusInfoList) {
        Set<String> relSet = new HashSet<String>();
        FicheURIChecker ficheChecker = new FicheURIChecker();
        for (CorpusInfo corpusInfo : corpusInfoList) {
            CorpusData corpusData = corpusInfo.getCorpusData();
            ficheChecker.setCurrentCorpusURI(corpusData.getCorpusURI());
            Map<String, List<Integer>> relAttributeMap = getRelAttributeMap(corpusData.getAttributes(), codeByScrutariDataURIMap, ficheChecker);
            if (relAttributeMap != null) {
                relSet.addAll(relAttributeMap.keySet());
                corpusInfo.setRelAttributeMap(relAttributeMap);
            }
        }
        try (CacheIterator<FicheData> ficheDataIterator = dataCacheReaderFactory.newFicheDataIterator()) {
            while (ficheDataIterator.hasNext()) {
                FicheData ficheData = ficheDataIterator.next();
                ficheChecker.setCurrentCorpusURI(ficheData.getFicheURI().getCorpusURI());
                Map<String, List<Integer>> relAttributeMap = getRelAttributeMap(ficheData.getAttributes(), codeByScrutariDataURIMap, ficheChecker);
                if (relAttributeMap != null) {
                    relSet.addAll(relAttributeMap.keySet());
                    ((AbstractDataInfo) codeArray.get(ficheData.getFicheCode())).setRelAttributeMap(relAttributeMap);
                }
            }
        }
        MotcleURIChecker motcleChecker = new MotcleURIChecker();
        for (ThesaurusInfo thesaurusInfo : thesaurusInfoList) {
            ThesaurusData thesaurusData = thesaurusInfo.getThesaurusData();
            motcleChecker.setCurrentThesaurusURI(thesaurusData.getThesaurusURI());
            Map<String, List<Integer>> relAttributeMap = getRelAttributeMap(thesaurusData.getAttributes(), codeByScrutariDataURIMap, motcleChecker);
            if (relAttributeMap != null) {
                relSet.addAll(relAttributeMap.keySet());
                thesaurusInfo.setRelAttributeMap(relAttributeMap);
            }
        }
        try (CacheIterator<MotcleData> motcleDataIterator = dataCacheReaderFactory.newMotcleDataIterator()) {
            while (motcleDataIterator.hasNext()) {
                MotcleData motcleData = motcleDataIterator.next();
                motcleChecker.setCurrentThesaurusURI(motcleData.getMotcleURI().getThesaurusURI());
                Map<String, List<Integer>> relAttributeMap = getRelAttributeMap(motcleData.getAttributes(), codeByScrutariDataURIMap, motcleChecker);
                if (relAttributeMap != null) {
                    relSet.addAll(relAttributeMap.keySet());
                    ((AbstractDataInfo) codeArray.get(motcleData.getMotcleCode())).setRelAttributeMap(relAttributeMap);
                }
            }
        }
        return relSet;
    }

    private static Map<String, List<Integer>> getRelAttributeMap(Attributes attributes, Map<ScrutariDataURI, Integer> codeByScrutariDataURIMap, URIChecker uriChecker) {
        if (attributes.isEmpty()) {
            return null;
        }
        Map<String, List<Integer>> relAttributeMap = new HashMap<String, List<Integer>>();
        List<Integer> bufferList = new ArrayList<Integer>();
        for (Attribute attribute : attributes) {
            AttributeKey attributeKey = attribute.getAttributeKey();
            if (attributeKey.getNameSpace().equals(RelSpace.REL_NAMESPACE.toString())) {
                bufferList.clear();
                for (String value : attribute) {
                    try {
                        ScrutariDataURI uri = uriChecker.checkURI(value);
                        Integer otherMotcleCode = codeByScrutariDataURIMap.get(uri);
                        if (otherMotcleCode != null) {
                            bufferList.add(otherMotcleCode);
                        }
                    } catch (URIParseException pe) {

                    }
                }
                if (!bufferList.isEmpty()) {
                    relAttributeMap.put(attributeKey.toString(), PrimUtils.wrap(PrimUtils.toArray(bufferList)));
                }
            }
        }
        int size = relAttributeMap.size();
        if (size > 0) {
            if (size == 1) {
                Map.Entry<String, List<Integer>> entry = relAttributeMap.entrySet().iterator().next();
                relAttributeMap = Collections.singletonMap(entry.getKey(), entry.getValue());
            }
            return relAttributeMap;
        } else {
            return null;
        }
    }


    private static class InternalDataAccessProvider implements DataAccessProvider {

        private final DataCacheReaderFactory dataCacheReaderFactory;
        private final CodeArray<AbstractDataInfo> dataCodeArray;
        private final List<BaseData> baseDataList;
        private final List<CorpusData> corpusDataList;
        private final List<ThesaurusData> thesaurusDataList;
        private final Map<ScrutariDataURI, Integer> codeByScrutariDataURIMap;
        private final Map<String, BaseData> baseDataBySourceNameMap = new HashMap<String, BaseData>();
        private final Set<String> relSet;
        private final ScrutariDataURIChecker scrutariDataURIChecker;
        private final Set<Long> indexationSet;

        private InternalDataAccessProvider(DataCacheReaderFactory dataCacheReaderFactory, ScrutariDataURIChecker scrutariDataURIChecker,
                CodeArray<AbstractDataInfo> dataCodeArray, List<BaseData> baseDataList, List<CorpusData> corpusDataList, List<ThesaurusData> thesaurusDataList, Map<ScrutariDataURI, Integer> codeByScrutariDataURIMap, Set<Long> indexationSet, Set<String> relSet) {
            this.dataCacheReaderFactory = dataCacheReaderFactory;
            this.scrutariDataURIChecker = scrutariDataURIChecker;
            this.dataCodeArray = dataCodeArray;
            this.baseDataList = baseDataList;
            this.corpusDataList = corpusDataList;
            this.thesaurusDataList = thesaurusDataList;
            this.codeByScrutariDataURIMap = codeByScrutariDataURIMap;
            for (BaseData baseData : baseDataList) {
                baseDataBySourceNameMap.put(baseData.getScrutariSourceName(), baseData);
            }
            this.indexationSet = indexationSet;
            this.relSet = relSet;
        }

        @Override
        public DataAccess openDataAccess() {
            return new InternalDataAccessProvider.InternalDataAccess();
        }


        private class InternalDataAccess implements DataAccess {

            private DataCacheReader dataCacheReader;

            private InternalDataAccess() {
            }

            @Override
            public List<BaseData> getBaseDataList() {
                return baseDataList;
            }

            @Override
            public List<CorpusData> getCorpusDataList() {
                return corpusDataList;
            }

            @Override
            public List<ThesaurusData> getThesaurusDataList() {
                return thesaurusDataList;
            }

            @Override
            public Integer getCode(ScrutariDataURI scrutariDataURI) {
                scrutariDataURI = scrutariDataURIChecker.checkURI(scrutariDataURI);
                return codeByScrutariDataURIMap.get(scrutariDataURI);
            }

            @Override
            public ScrutariDataURIChecker getScrutariDataURIChecker() {
                return scrutariDataURIChecker;
            }

            @Override
            public Object getScrutariData(ScrutariDataURI scrutariDataURI) {
                scrutariDataURI = scrutariDataURIChecker.checkURI(scrutariDataURI);
                Integer code = codeByScrutariDataURIMap.get(scrutariDataURI);
                if (code == null) {
                    return null;
                }
                short type = scrutariDataURI.getType();
                switch (type) {
                    case ScrutariDataURI.BASEURI_TYPE:
                        return getBaseData(code);
                    case ScrutariDataURI.CORPUSURI_TYPE:
                        return getCorpusData(code);
                    case ScrutariDataURI.THESAURUSURI_TYPE:
                        return getThesaurusData(code);
                    case ScrutariDataURI.FICHEURI_TYPE:
                        return getFicheData(code);
                    case ScrutariDataURI.MOTCLEURI_TYPE:
                        return getMotcleData(code);
                    default:
                        throw new SwitchException("Unknown type: " + type);
                }
            }

            @Override
            public DataInfo getDataInfo(Integer code) {
                return dataCodeArray.get(code);
            }

            @Override
            public BaseData getBaseData(Integer baseCode) {
                try {
                    BaseInfo baseInfo = ((BaseInfo) dataCodeArray.get(baseCode));
                    if (baseInfo != null) {
                        return baseInfo.getBaseData();
                    } else {
                        return null;
                    }
                } catch (ClassCastException cce) {
                    return null;
                }
            }

            @Override
            public BaseData getBaseDataByScrutariSourceName(String scrutariSourceName) {
                return baseDataBySourceNameMap.get(scrutariSourceName);
            }

            @Override
            public CorpusData getCorpusData(Integer corpusCode) {
                try {
                    CorpusInfo corpusInfo = ((CorpusInfo) dataCodeArray.get(corpusCode));
                    if (corpusInfo != null) {
                        return corpusInfo.getCorpusData();
                    } else {
                        return null;
                    }
                } catch (ClassCastException cce) {
                    return null;
                }
            }

            @Override
            public FicheData getFicheData(Integer ficheCode) {
                if (dataCacheReader == null) {
                    dataCacheReader = dataCacheReaderFactory.newReader();
                }
                return dataCacheReader.getFicheData(ficheCode);
            }

            @Override
            public ThesaurusData getThesaurusData(Integer thesaurusCode) {
                try {
                    ThesaurusInfo thesaurusInfo = ((ThesaurusInfo) dataCodeArray.get(thesaurusCode));
                    if (thesaurusInfo != null) {
                        return thesaurusInfo.getThesaurusData();
                    } else {
                        return null;
                    }
                } catch (ClassCastException cce) {
                    return null;
                }
            }

            @Override
            public MotcleData getMotcleData(Integer motcleCode) {
                if (dataCacheReader == null) {
                    dataCacheReader = dataCacheReaderFactory.newReader();
                }
                return dataCacheReader.getMotcleData(motcleCode);
            }

            @Override
            public FuzzyDate getFicheDate(Integer ficheCode) {
                if (dataCacheReader == null) {
                    dataCacheReader = dataCacheReaderFactory.newReader();
                }
                return dataCacheReader.getFicheDate(ficheCode);
            }

            @Override
            public boolean containsRelAttributeFor(String relName) {
                return relSet.contains(relName);
            }

            @Override
            public boolean hasIndexation(Integer corpusCode, Integer thesaurusCode) {
                long lg = PrimUtils.toLong(corpusCode, thesaurusCode);
                return indexationSet.contains(lg);
            }

            @Override
            public void close() {
                if (dataCacheReader != null) {
                    dataCacheReader.close();
                }
            }

        }

    }


    private static class InternalFicheInfo extends AbstractDataInfo implements FicheInfo {

        private final FicheURI ficheURI;
        private final CorpusData corpusData;
        private final Lang lang;

        private InternalFicheInfo(Integer code, ScrutariDBName firstAdd, FicheURI ficheURI, CorpusData corpusData, Lang lang) {
            super(code, firstAdd);
            this.ficheURI = ficheURI;
            this.corpusData = corpusData;
            this.lang = lang;
        }

        @Override
        public FicheURI getFicheURI() {
            return ficheURI;
        }

        @Override
        public CorpusData getCorpusData() {
            return corpusData;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

    }


    private static class InternalMotcleInfo extends AbstractDataInfo implements MotcleInfo {

        private final MotcleURI motcleURI;
        private final ThesaurusData thesaurusData;

        private InternalMotcleInfo(Integer code, ScrutariDBName firstAdd, MotcleURI motcleURI, ThesaurusData thesaurusData) {
            super(code, firstAdd);
            this.motcleURI = motcleURI;
            this.thesaurusData = thesaurusData;
        }

        @Override
        public MotcleURI getMotcleURI() {
            return motcleURI;
        }

        @Override
        public ThesaurusData getThesaurusData() {
            return thesaurusData;
        }

    }


    private static abstract class URIChecker {

        public abstract ScrutariDataURI checkURI(String value) throws URIParseException;

    }


    private static class MotcleURIChecker extends URIChecker {

        private ThesaurusURI thesaurusURI = null;

        private MotcleURIChecker() {

        }

        @Override
        public ScrutariDataURI checkURI(String value) throws URIParseException {
            return MotcleURI.check(thesaurusURI, value);
        }

        private void setCurrentThesaurusURI(ThesaurusURI thesaurusURI) {
            this.thesaurusURI = thesaurusURI;
        }

    }


    private static class FicheURIChecker extends URIChecker {

        private CorpusURI corpusURI = null;

        private FicheURIChecker() {

        }

        @Override
        public ScrutariDataURI checkURI(String value) throws URIParseException {
            return FicheURI.check(corpusURI, value);
        }

        private void setCurrentCorpusURI(CorpusURI corpusURI) {
            this.corpusURI = corpusURI;
        }

    }

}
