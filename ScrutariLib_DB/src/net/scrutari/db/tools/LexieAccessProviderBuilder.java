/* ScrutariLib_DB - Copyright (c) 2009-2024 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangsUtils;
import net.mapeadores.util.text.collation.map.CollatedKeyMap;
import net.mapeadores.util.text.collation.map.SortedCollatedKeyMap;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.LexieAccessProvider;
import net.scrutari.db.api.caches.LexieCacheReader;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheWriter;
import net.scrutari.db.tools.lexies.LexificationEngine;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public class LexieAccessProviderBuilder {

    private LexieAccessProviderBuilder() {

    }

    public static LexieAccessProvider build(DataAccess dataAccess, FieldRankManager fieldRankManager, LexieCacheWriter lexieCacheWriter) {
        LexificationEngine.Result result = LexificationEngine.cacheLexification(dataAccess, fieldRankManager, lexieCacheWriter);
        return build(result.getLexieCacheReaderFactory(), result.getScrutariLexieUnits());
    }

    public static LexieAccessProvider build(LexieCacheReaderFactory lexieCacheReaderFactory) {
        List<ScrutariLexieUnit> scrutariLexieUnitList;
        try (LexieCacheReader reader = lexieCacheReaderFactory.newReader()) {
            scrutariLexieUnitList = reader.getScrutariLexieUnitList();
        }
        return build(lexieCacheReaderFactory, scrutariLexieUnitList);
    }

    private static LexieAccessProvider build(LexieCacheReaderFactory lexieCacheReaderFactory, Collection<ScrutariLexieUnit> scrutariLexieUnits) {
        Map<LexieId, ScrutariLexieUnit> scrutariLexieUnitMap = new HashMap<LexieId, ScrutariLexieUnit>();
        Map<Lang, CollatedKeyMap<ScrutariLexieUnit>> lexieUnitMapByLangMap = new HashMap<Lang, CollatedKeyMap<ScrutariLexieUnit>>();
        for (ScrutariLexieUnit scrutariLexieUnit : scrutariLexieUnits) {
            scrutariLexieUnitMap.put(scrutariLexieUnit.getLexieId(), scrutariLexieUnit);
            Lang lang = scrutariLexieUnit.getLang();
            CollatedKeyMap<ScrutariLexieUnit> lexieIdMap = lexieUnitMapByLangMap.get(lang);
            if (lexieIdMap == null) {
                lexieIdMap = new SortedCollatedKeyMap<ScrutariLexieUnit>(lang.toLocale());
                lexieUnitMapByLangMap.put(lang, lexieIdMap);
            }
            lexieIdMap.putValueByCollatedKey(scrutariLexieUnit.getCollatedLexie(), scrutariLexieUnit);
        }
        SortedSet<Lang> langSet = new TreeSet<Lang>(lexieUnitMapByLangMap.keySet());
        List<Lang> langList = LangsUtils.wrap(langSet);
        return new InternalLexieAccessProvider(lexieCacheReaderFactory, langList, scrutariLexieUnitMap, lexieUnitMapByLangMap);
    }


    private static class InternalLexieAccessProvider implements LexieAccessProvider {

        private final LexieCacheReaderFactory lexieCacheReaderFactory;
        private final List<Lang> langList;
        private final Map<LexieId, ScrutariLexieUnit> scrutariLexieUnitMap;
        private final Map<Lang, CollatedKeyMap<ScrutariLexieUnit>> lexieUnitMapByLangMap;

        private InternalLexieAccessProvider(LexieCacheReaderFactory lexieCacheReaderFactory, List<Lang> langList, Map<LexieId, ScrutariLexieUnit> scrutariLexieUnitMap, Map<Lang, CollatedKeyMap<ScrutariLexieUnit>> lexieUnitMapByLangMap) {
            this.lexieCacheReaderFactory = lexieCacheReaderFactory;
            this.langList = langList;
            this.scrutariLexieUnitMap = scrutariLexieUnitMap;
            this.lexieUnitMapByLangMap = lexieUnitMapByLangMap;
        }

        @Override
        public LexieAccess openLexieAccess() {
            return new InternalLexieAccess();
        }


        private class InternalLexieAccess implements LexieAccess {

            private LexieCacheReader lexieCacheReader;

            private InternalLexieAccess() {
            }

            @Override
            public int getLexieCount() {
                return scrutariLexieUnitMap.size();
            }

            @Override
            public List<Lang> getLangList() {
                return langList;
            }

            @Override
            public ScrutariLexieUnit getScrutariLexieUnit(LexieId lexieId) {
                return scrutariLexieUnitMap.get(lexieId);
            }

            @Override
            public CollatedKeyMap<ScrutariLexieUnit> getLexieUnitMapByLang(Lang lang) {
                return lexieUnitMapByLangMap.get(lang);
            }

            @Override
            public void checkLexieOccurrences(LexieId lexieId, Integer subsetCode, IntPredicate codePredicate, Consumer<LexieOccurrences> occurrencesConsumer) {
                testCacheReader();
                lexieCacheReader.checkLexieOccurrences(lexieId, subsetCode, codePredicate, occurrencesConsumer);
            }

            @Override
            public List<LexieOccurrences> getLexieOccurrencesList(LexieId lexieId, Integer subsetCode) {
                testCacheReader();
                return lexieCacheReader.getLexieOccurrencesList(lexieId, subsetCode);
            }

            @Override
            public void populateFicheLexification(Integer ficheCode, FicheLexification ficheLexification) {
                testCacheReader();
                lexieCacheReader.readFicheLexification(ficheCode, ficheLexification);
            }

            @Override
            public void populateMotcleLexification(Integer motcleCode, MotcleLexification motcleLexification) {
                testCacheReader();
                lexieCacheReader.readMotcleLexification(motcleCode, motcleLexification);
            }

            @Override
            public void close() {
                if (lexieCacheReader != null) {
                    lexieCacheReader.close();
                }
            }

            private void testCacheReader() {
                if (lexieCacheReader == null) {
                    lexieCacheReader = lexieCacheReaderFactory.newReader();
                }
            }

        }

    }

}
