/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.util;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.RelSpace;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;


/**
 *
 * @author Vincent Calame
 */
public final class RelUtils {

    private RelUtils() {

    }

    public static Collection<Integer> filterAlternate(DataAccess dataAccess, Lang preferredLang, Collection<Integer> ficheCodes) {
        if (!dataAccess.containsRelAttributeFor(RelSpace.ALTERNATE_KEY)) {
            return ficheCodes;
        }
        Set<Integer> ignoreSet = new HashSet<Integer>();
        Set<Integer> resultSet = new LinkedHashSet<Integer>();
        for (Integer ficheCode : ficheCodes) {
            if (ignoreSet.contains(ficheCode)) {
                continue;
            }
            FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
            Lang lang = ficheInfo.getLang();
            if (lang.equals(preferredLang)) {
                List<Integer> alternateList = ficheInfo.getRelAttributeCodeList(RelSpace.ALTERNATE_KEY);
                for (Integer alternate : alternateList) {
                    ignoreSet.add(alternate);
                    resultSet.remove(alternate);
                }
            }
            resultSet.add(ficheCode);
        }
        return resultSet;
    }

}
