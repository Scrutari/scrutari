/* ScrutariLib_DB - Copyright (c) 2005-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.util;

import java.util.AbstractList;
import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.stats.LangStat;
import net.scrutari.db.api.stats.ScrutariDBStats;
import net.scrutari.db.api.stats.LangStats;


/**
 * Propose des méthodes statitiques utilitaires pour récupérer des Datarmations
 * sur les bases.
 *
 * @author Vincent Calame
 */
public final class DataUtils {

    static final char SEUIL1 = '\u0080';
    static final char SEUIL2 = '\u0800';

    private DataUtils() {
    }

    /**
     * Retourne le nombre de caractères supplémentaires introduit par le codage
     * UTF8 du caractère en argument. Par exemple, la méthode retournera 0 pour
     * le caractère « a » qui en UTF-8 s'écrit « a » et elle retournera 1 pour
     * le caractère « é » qui en UTF-8 s'écrit « Ã© ».
     */
    public static int getDecalageUTF8(char c) {
        if (c < SEUIL1) {
            return 0;
        }
        if (c < SEUIL2) {
            return 1;
        }
        return 2;
    }


    public static String getHref(FicheData ficheData, DataAccess dataAccess, Lang lang) {
        BaseData baseData = dataAccess.getBaseData(dataAccess.getCorpusData(ficheData.getCorpusCode()).getBaseCode());
        Lang checkedLang = baseData.checkAvailableLang(lang);
        return ficheData.getHref(checkedLang);
    }

    public static int getFicheCountByCorpus(DataAccess dataAccess, CorpusData[] corpusDataArray, Lang[] langArray, ScrutariDBStats scrutariDBStats) {
        int resultat = 0;
        if (langArray != null) {
            for (CorpusData corpusData : corpusDataArray) {
                LangStats ficheStats = scrutariDBStats.getLangStats(corpusData.getCorpusCode());
                for (LangStat langStat : ficheStats) {
                    Lang lang = langStat.getLang();
                    if (contains(langArray, lang)) {
                        resultat = resultat + langStat.getCount();
                    }
                }
            }
        } else {
            for (CorpusData corpusData : corpusDataArray) {
                resultat = resultat + corpusData.getFicheCodeList().size();
            }
        }
        return resultat;
    }

    private static boolean contains(Lang[] array, Lang value) {
        for (Lang other : array) {
            if (other.equals(value)) {
                return true;
            }
        }
        return false;
    }

    public static List<BaseData> wrap(BaseData[] array) {
        return new BaseDataList(array);
    }

    public static List<CorpusData> wrap(CorpusData[] array) {
        return new CorpusDataList(array);
    }

    public static List<ThesaurusData> wrap(ThesaurusData[] array) {
        return new ThesaurusDataList(array);
    }


    private static class BaseDataList extends AbstractList<BaseData> implements RandomAccess {

        private final BaseData[] array;

        private BaseDataList(BaseData[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public BaseData get(int index) {
            return array[index];
        }

    }


    private static class CorpusDataList extends AbstractList<CorpusData> implements RandomAccess {

        private final CorpusData[] array;

        private CorpusDataList(CorpusData[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public CorpusData get(int index) {
            return array[index];
        }

    }


    private static class ThesaurusDataList extends AbstractList<ThesaurusData> implements RandomAccess {

        private final ThesaurusData[] array;

        private ThesaurusDataList(ThesaurusData[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ThesaurusData get(int index) {
            return array[index];
        }

    }

}
