/* ScrutariLib_DB - Copyright (c) 2012-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.util;

import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class BaseCheck {

    private final Lang checkedLang;
    private final String baseIcon;

    public BaseCheck(Lang checkedLang, String baseIcon) {
        this.checkedLang = checkedLang;
        this.baseIcon = baseIcon;
    }

    public Lang getCheckedLang() {
        return checkedLang;
    }

    public String getBaseIcon() {
        return baseIcon;
    }

}
