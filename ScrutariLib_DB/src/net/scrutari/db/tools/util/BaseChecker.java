/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.util;

import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.BaseData;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;


/**
 *
 * @author Vincent Calame
 */
public class BaseChecker {

    private final Map<Integer, BaseCheck> bufferMap = new HashMap<Integer, BaseCheck>();
    private final Lang lang;


    public BaseChecker(Lang lang) {
        this.lang = lang;
    }

    public BaseCheck getBaseCheck(DataAccess dataAccess, Integer baseCode) {
        BaseCheck current = bufferMap.get(baseCode);
        if (current != null) {
            return current;
        }
        BaseData baseData = dataAccess.getBaseData(baseCode);
        current = check(baseData, lang);
        bufferMap.put(baseCode, current);
        return current;
    }

    public BaseCheck getBaseCheck(DataAccess dataAccess, FicheData ficheData) {
        return getBaseCheck(dataAccess, ficheData.getBaseCode());
    }

    public static BaseCheck check(BaseData baseData, Lang lang) {
        Lang checkedLang = baseData.checkAvailableLang(lang);
        String baseIcon = baseData.getBaseIcon(checkedLang);
        return new BaseCheck(checkedLang, baseIcon);
    }

}
