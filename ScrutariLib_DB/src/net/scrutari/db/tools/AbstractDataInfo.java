/* ScrutariLib_DB - Copyright (c) 2020-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import java.util.SortedSet;
import java.util.TreeSet;
import net.mapeadores.util.primitives.PrimUtils;
import net.scrutari.data.RelationData;
import net.scrutari.db.api.DataInfo;
import net.scrutari.db.api.RoleKey;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
abstract class AbstractDataInfo implements DataInfo {

    private final static List<RelationData> RELATION_EMPTY_LIST = Collections.emptyList();
    private final static SortedSet<RoleKey> KEY_EMPTY_SET = Collections.emptySortedSet();
    private final Integer code;
    private final ScrutariDBName firstAdd;
    private Map<String, List<Integer>> relAttributeMap = null;
    private Map<Integer, List<Integer>> indexationMap = null;
    private int indexationCount = 0;
    private RelationManager relationManager = null;

    protected AbstractDataInfo(Integer code, ScrutariDBName firstAdd) {
        this.code = code;
        this.firstAdd = firstAdd;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public ScrutariDBName getFirstAdd() {
        return firstAdd;
    }

    @Override
    public int getIndexationCount() {
        return indexationCount;
    }

    @Override
    public List<Integer> getIndexationCodeList(Integer subsetCode) {
        if (indexationMap == null) {
            return PrimUtils.EMPTY_LIST;
        } else {
            List<Integer> list = indexationMap.get(subsetCode);
            if (list == null) {
                return PrimUtils.EMPTY_LIST;
            } else {
                return list;
            }
        }
    }

    @Override
    public boolean hasRelAttribute() {
        return (relAttributeMap != null);
    }

    @Override
    public List<Integer> getRelAttributeCodeList(String relName) {
        if (relAttributeMap == null) {
            return PrimUtils.EMPTY_LIST;
        } else {
            List<Integer> list = relAttributeMap.get(relName);
            if (list == null) {
                return PrimUtils.EMPTY_LIST;
            } else {
                return list;
            }
        }
    }

    @Override
    public boolean hasRelation() {
        return (relationManager == null);
    }

    @Override
    public SortedSet<RoleKey> getAvalaibleRoleKeySet() {
        if (relationManager == null) {
            return KEY_EMPTY_SET;
        } else {
            return relationManager.getAvalaibleRoleKeySet();
        }
    }

    @Override
    public List<RelationData> getRelationList(RoleKey roleKey) {
        if (relationManager == null) {
            return RELATION_EMPTY_LIST;
        }
        return relationManager.getList(roleKey);
    }


    protected void setRelAttributeMap(Map<String, List<Integer>> relAttributeMap) {
        this.relAttributeMap = relAttributeMap;
    }

    protected void addIndexation(Integer subsetCode, Integer elementCode) {
        if (indexationMap == null) {
            indexationMap = new HashMap<Integer, List<Integer>>();
        }
        List<Integer> list = indexationMap.get(subsetCode);
        if (list == null) {
            list = new ArrayList<Integer>();
            indexationMap.put(subsetCode, list);
            list.add(elementCode);
        } else if (!list.contains(elementCode)) {
            list.add(elementCode);
        }
    }

    protected void flush() {
        flushIndexation();
        flushRelation();
    }

    private void flushIndexation() {
        if (indexationMap == null) {
            return;
        }
        int size = indexationMap.size();
        if (size == 1) {
            Map.Entry<Integer, List<Integer>> entry = indexationMap.entrySet().iterator().next();
            List<Integer> codeList = entry.getValue();
            indexationCount = codeList.size();
            indexationMap = Collections.singletonMap(entry.getKey(), PrimUtils.wrap(PrimUtils.toArray(codeList)));
        } else {
            Map<Integer, List<Integer>> newIndexationMap = new HashMap<Integer, List<Integer>>();
            for (Map.Entry<Integer, List<Integer>> entry : indexationMap.entrySet()) {
                List<Integer> codeList = entry.getValue();
                indexationCount = indexationCount + codeList.size();
                newIndexationMap.put(entry.getKey(), PrimUtils.wrap(PrimUtils.toArray(codeList)));
            }
            indexationMap = newIndexationMap;
        }
    }

    private void flushRelation() {
        if (relationManager == null) {
            return;
        }
        this.relationManager = ((BuildRelationManager) relationManager).flush();
    }

    protected void addRelation(RelationData relationData, String role) {
        BuildRelationManager buildRelationManager;
        if (relationManager == null) {
            relationManager = new BuildRelationManager();
        }
        buildRelationManager = (BuildRelationManager) relationManager;
        List<RelationData> list = buildRelationManager.getList(RoleKey.build(relationData.getType(), role));
        if (!list.contains(relationData)) {
            list.add(relationData);
        }
    }

    private static List<RelationData> toImmutableList(List<RelationData> list) {
        return new InternalRelationDataList(list.toArray(new RelationData[list.size()]));
    }


    private static abstract class RelationManager {

        protected abstract List<RelationData> getList(RoleKey roleKey);

        protected abstract SortedSet<RoleKey> getAvalaibleRoleKeySet();

    }


    private static class BuildRelationManager extends RelationManager {

        private final Map<RoleKey, List<RelationData>> listMap = new HashMap<RoleKey, List<RelationData>>();

        BuildRelationManager() {

        }

        @Override
        protected List<RelationData> getList(RoleKey roleKey) {
            List<RelationData> list = listMap.get(roleKey);
            if (list == null) {
                list = new ArrayList<RelationData>();
                listMap.put(roleKey, list);
            }
            return list;
        }

        protected RelationManager flush() {
            int size = listMap.size();
            if (size == 0) {
                return null;
            } else if (size == 1) {
                Map.Entry<RoleKey, List<RelationData>> firstEntry = listMap.entrySet().iterator().next();
                return new UniqueRelationManager(firstEntry.getKey(), toImmutableList(firstEntry.getValue()));
            } else if (size < 10) {
                RoleKey[] roleArray = new RoleKey[size];
                List<RelationData>[] listArray = new List[size];
                int p = 0;
                for (Map.Entry<RoleKey, List<RelationData>> entry : listMap.entrySet()) {
                    roleArray[p] = entry.getKey();
                    listArray[p] = toImmutableList(entry.getValue());
                    p++;
                }
                return new ArrayRelationManager(roleArray, listArray);
            } else {
                Map<RoleKey, List<RelationData>> finalMap = new HashMap<RoleKey, List<RelationData>>();
                for (Map.Entry<RoleKey, List<RelationData>> entry : listMap.entrySet()) {
                    finalMap.put(entry.getKey(), toImmutableList(entry.getValue()));
                }
                return new MapRelationManager(finalMap);
            }
        }

        @Override
        protected SortedSet<RoleKey> getAvalaibleRoleKeySet() {
            return null;
        }

    }


    private static class UniqueRelationManager extends RelationManager {

        private final RoleKey uniqueKey;
        private final List<RelationData> uniqueList;

        private UniqueRelationManager(RoleKey uniqueKey, List<RelationData> uniqueList) {
            this.uniqueKey = uniqueKey;
            this.uniqueList = uniqueList;
        }

        @Override
        protected List<RelationData> getList(RoleKey roleKey) {
            if (roleKey.equals(uniqueKey)) {
                return uniqueList;
            } else {
                return RELATION_EMPTY_LIST;
            }
        }

        @Override
        protected SortedSet<RoleKey> getAvalaibleRoleKeySet() {
            SortedSet<RoleKey> set = new TreeSet<RoleKey>();
            set.add(uniqueKey);
            return set;
        }

    }


    private static class ArrayRelationManager extends RelationManager {

        private final RoleKey[] keyArray;
        private final List<RelationData>[] listArray;

        private ArrayRelationManager(RoleKey[] keyArray, List<RelationData>[] listArray) {
            this.keyArray = keyArray;
            this.listArray = listArray;
        }

        @Override
        protected List<RelationData> getList(RoleKey roleKey) {
            int length = keyArray.length;
            for (int i = 0; i < length; i++) {
                if (keyArray[i].equals(roleKey)) {
                    return listArray[i];
                }
            }
            return RELATION_EMPTY_LIST;
        }

        @Override
        protected SortedSet<RoleKey> getAvalaibleRoleKeySet() {
            SortedSet<RoleKey> set = new TreeSet<RoleKey>();
            int length = keyArray.length;
            for (int i = 0; i < length; i++) {
                set.add(keyArray[i]);
            }
            return set;
        }

    }


    private static class MapRelationManager extends RelationManager {

        private final Map<RoleKey, List<RelationData>> map;

        private MapRelationManager(Map<RoleKey, List<RelationData>> map) {
            this.map = map;
        }

        @Override
        protected List<RelationData> getList(RoleKey roleKey) {
            List<RelationData> list = map.get(roleKey);
            if (list != null) {
                return list;
            } else {
                return RELATION_EMPTY_LIST;
            }
        }

        @Override
        protected SortedSet<RoleKey> getAvalaibleRoleKeySet() {
            SortedSet<RoleKey> set = new TreeSet<RoleKey>();
            set.addAll(map.keySet());
            return set;
        }

    }


    private static class InternalRelationDataList extends AbstractList<RelationData> implements RandomAccess {

        private final RelationData[] array;

        private InternalRelationDataList(RelationData[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public RelationData get(int i) {
            return array[i];
        }

    }

}
