/* ScrutariLib_DB - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.stats;

import java.util.AbstractList;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.mapeadores.util.localisation.Lang;
import net.scrutari.db.api.stats.LangStat;
import net.scrutari.db.api.stats.LangStats;


/**
 *
 * @author Vincent Calame
 */
public class LangStatsBuilder {

    public final static LangStats EMPTY_LANGSTATS = new InternalFicheStats(new LangStat[0]);
    private final Map<Lang, Counter> counterMap = new HashMap<Lang, Counter>();

    public LangStatsBuilder() {

    }

    public void addOne(Lang lang) {
        Counter counter = counterMap.get(lang);
        if (counter == null) {
            counter = new Counter(lang);
            counterMap.put(lang, counter);
        }
        counter.increase(1);
    }

    public void addLangStats(LangStats langStats) {
        for (LangStat langStat : langStats) {
            Lang lang = langStat.getLang();
            Counter counter = counterMap.get(lang);
            if (counter == null) {
                counter = new Counter(lang);
                counterMap.put(lang, counter);
            }
            counter.increase(langStat.getCount());
        }
    }

    public LangStats toLangStats() {
        SortedMap<Lang, Integer> langMap = new TreeMap<Lang, Integer>();
        for (Counter counter : counterMap.values()) {
            langMap.put(counter.lang, counter.value);
        }
        int size = langMap.size();
        LangStat[] langArray = new LangStat[size];
        int p = 0;
        for (Map.Entry<Lang, Integer> entry : langMap.entrySet()) {
            langArray[p] = new InternalLangStat(entry.getKey(), entry.getValue());
            p++;
        }
        return new InternalFicheStats(langArray);
    }


    private static class Counter {

        private final Lang lang;
        private int value = 0;

        private Counter(Lang lang) {
            this.lang = lang;
        }

        private void increase(int plus) {
            value = value + plus;
        }

        private int getCount() {
            return value;
        }

    }


    private static class InternalFicheStats extends AbstractList<LangStat> implements LangStats {

        private final LangStat[] langStatArray;

        private InternalFicheStats(LangStat[] langStatsArray) {
            this.langStatArray = langStatsArray;
        }

        @Override
        public int size() {
            return langStatArray.length;
        }

        @Override
        public LangStat get(int index) {
            return langStatArray[index];
        }

    }


    public static class InternalLangStat implements LangStat {

        private final Lang lang;
        private final int count;

        private InternalLangStat(Lang lang, int count) {
            this.lang = lang;
            this.count = count;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public int getCount() {
            return count;
        }

    }


}
