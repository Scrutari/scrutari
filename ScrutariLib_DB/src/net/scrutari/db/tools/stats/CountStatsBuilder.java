/* ScrutariLib_DB - Copyright (c) 2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.stats;

import net.scrutari.db.api.stats.CountStats;


/**
 *
 * @author Vincent Calame
 */
public class CountStatsBuilder {

    private int ficheCount = 0;
    private int motcleCount = 0;
    private int thesaurusCount = 0;
    private int corpusCount = 0;
    private int baseCount = 0;
    private int lexieCount = 0;
    private int indexationCount = 0;


    private CountStatsBuilder() {

    }

    public static CountStatsBuilder newEngineInstance() {
        CountStatsBuilder builder = new CountStatsBuilder();
        return builder;
    }

    public static CountStatsBuilder newBaseInstance() {
        CountStatsBuilder builder = new CountStatsBuilder();
        builder.lexieCount = -1;
        builder.baseCount = -1;
        return builder;
    }

    public static CountStatsBuilder newCorpusInstance() {
        CountStatsBuilder builder = new CountStatsBuilder();
        builder.lexieCount = -1;
        builder.baseCount = -1;
        builder.corpusCount = -1;
        builder.thesaurusCount = -1;
        return builder;
    }

    public static CountStatsBuilder newThesaurusInstance() {
        CountStatsBuilder builder = new CountStatsBuilder();
        builder.lexieCount = -1;
        builder.baseCount = -1;
        builder.corpusCount = -1;
        builder.thesaurusCount = -1;
        return builder;
    }

    public static CountStatsBuilder newCategoryInstance() {
        CountStatsBuilder builder = new CountStatsBuilder();
        builder.lexieCount = -1;
        builder.baseCount = -1;
        builder.thesaurusCount = -1;
        builder.motcleCount = -1;
        builder.indexationCount = -1;
        return builder;
    }

    public void increaseBaseCount(int count) {
        baseCount = baseCount + count;
    }

    public void increaseCorpusCount(int count) {
        corpusCount = corpusCount + count;
    }

    public void increaseThesaurusCount(int count) {
        thesaurusCount = thesaurusCount + count;
    }

    public void increaseFicheCount(int count) {
        ficheCount = ficheCount + count;
    }

    public void increaseMotcleCount(int count) {
        motcleCount = motcleCount + count;
    }

    public void increaseIndexationCount(int count) {
        indexationCount = indexationCount + count;
    }

    public void increaseLexieCount(int count) {
        lexieCount = lexieCount + count;
    }

    public CountStats toCountStats() {
        return new InternalCountStats(baseCount, corpusCount, thesaurusCount, ficheCount, motcleCount, indexationCount, lexieCount);
    }


    private static class InternalCountStats implements CountStats {

        private final int baseCount;
        private final int corpusCount;
        private final int thesaurusCount;
        private final int ficheCount;
        private final int motcleCount;
        private final int indexationCount;
        private final int lexieCount;

        private InternalCountStats(int baseCount, int corpusCount, int thesaurusCount, int ficheCount, int motcleCount, int indexationCount, int lexieCount) {
            this.baseCount = baseCount;
            this.corpusCount = corpusCount;
            this.thesaurusCount = thesaurusCount;
            this.ficheCount = ficheCount;
            this.motcleCount = motcleCount;
            this.indexationCount = indexationCount;
            this.lexieCount = lexieCount;
        }

        @Override
        public int getBaseCount() {
            return baseCount;
        }

        @Override
        public int getCorpusCount() {
            return corpusCount;
        }

        @Override
        public int getThesaurusCount() {
            return thesaurusCount;
        }

        @Override
        public int getMotcleCount() {
            return motcleCount;
        }

        @Override
        public int getFicheCount() {
            return ficheCount;
        }

        @Override
        public int getIndexationCount() {
            return indexationCount;
        }

        @Override
        public int getLexieCount() {
            return lexieCount;
        }

    }


}
