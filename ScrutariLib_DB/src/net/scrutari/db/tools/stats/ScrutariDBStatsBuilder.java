/* ScrutariLib_DB - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.stats;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.text.Label;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.stats.CountStats;
import net.scrutari.db.api.stats.LangStats;
import net.scrutari.db.api.stats.ScrutariDBStats;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariDBStatsBuilder {

    private final CountStatsBuilder engineCountStatsBuilder = CountStatsBuilder.newEngineInstance();
    private final Map<Integer, CountStats> countStatsMap = new HashMap<Integer, CountStats>();
    private final DataAccess dataAccess;
    private final LexieAccess lexieAccess;
    private final Map<Integer, LangStatsBuilder> langStatsBuilderMap = new HashMap<Integer, LangStatsBuilder>();
    private final LangStatsBuilder ficheEngineStatsBuilder = new LangStatsBuilder();
    private final LangStatsBuilder motcleEngineStatsBuilder = new LangStatsBuilder();
    private final IndexationInfoMap infoMap;

    private ScrutariDBStatsBuilder(DataAccess dataAccess, LexieAccess lexieAccess) {
        this.dataAccess = dataAccess;
        this.lexieAccess = lexieAccess;
        infoMap = new IndexationInfoMap(dataAccess);
    }

    private void check() {
        engineCountStatsBuilder.increaseLexieCount(lexieAccess.getLexieCount());
        for (BaseData baseData : dataAccess.getBaseDataList()) {
            checkBase(baseData);
        }
    }

    private void checkBase(BaseData baseData) {
        engineCountStatsBuilder.increaseBaseCount(1);
        CountStatsBuilder baseCountStatsBuilder = CountStatsBuilder.newBaseInstance();
        LangStatsBuilder baseLangStatsBuilder = new LangStatsBuilder();
        for (Integer corpusCode : baseData.getCorpusCodeList()) {
            CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
            checkCorpus(corpusData, baseCountStatsBuilder, baseLangStatsBuilder);
        }
        langStatsBuilderMap.put(baseData.getBaseCode(), baseLangStatsBuilder);
        for (Integer thesaurusCode : baseData.getThesaurusCodeList()) {
            ThesaurusData thesaurusData = dataAccess.getThesaurusData(thesaurusCode);
            checkThesaurus(thesaurusData, baseCountStatsBuilder);
        }
        countStatsMap.put(baseData.getBaseCode(), baseCountStatsBuilder.toCountStats());
    }

    private void checkCorpus(CorpusData corpusData, CountStatsBuilder baseCountStatsBuilder, LangStatsBuilder baseLangStatsBuilder) {
        baseCountStatsBuilder.increaseCorpusCount(1);
        engineCountStatsBuilder.increaseCorpusCount(1);
        CountStatsBuilder corpusCountStatsBuilder = CountStatsBuilder.newCorpusInstance();
        LangStatsBuilder corpusLangStatsBuilder = new LangStatsBuilder();
        List<Integer> ficheCodeList = corpusData.getFicheCodeList();
        int ficheLength = ficheCodeList.size();
        corpusCountStatsBuilder.increaseFicheCount(ficheLength);
        baseCountStatsBuilder.increaseFicheCount(ficheLength);
        engineCountStatsBuilder.increaseFicheCount(ficheLength);
        for (Integer ficheCode : ficheCodeList) {
            Lang lang = dataAccess.getFicheLang(ficheCode);
            corpusLangStatsBuilder.addOne(lang);
            baseLangStatsBuilder.addOne(lang);
            ficheEngineStatsBuilder.addOne(lang);
        }
        Integer corpusCode = corpusData.getCorpusCode();
        langStatsBuilderMap.put(corpusCode, corpusLangStatsBuilder);
        IndexationInfo indexationInfo = infoMap.getIndexationInfo(corpusCode);
        if (indexationInfo != null) {
            int indexationCount = indexationInfo.getIndexationCount();
            corpusCountStatsBuilder.increaseIndexationCount(indexationCount);
            baseCountStatsBuilder.increaseIndexationCount(indexationCount);
            engineCountStatsBuilder.increaseIndexationCount(indexationCount);
            corpusCountStatsBuilder.increaseMotcleCount(indexationInfo.getIndexedCount());
        }
        countStatsMap.put(corpusCode, corpusCountStatsBuilder.toCountStats());
    }

    private void checkThesaurus(ThesaurusData thesaurusData, CountStatsBuilder baseCountStatsBuilder) {
        baseCountStatsBuilder.increaseThesaurusCount(1);
        engineCountStatsBuilder.increaseThesaurusCount(1);
        CountStatsBuilder thesaurusCountStatsBuilder = CountStatsBuilder.newThesaurusInstance();
        LangStatsBuilder thesaurusLangStatsBuilder = new LangStatsBuilder();
        List<Integer> motcleCodeList = thesaurusData.getMotcleCodeList();
        int motcleLength = motcleCodeList.size();
        thesaurusCountStatsBuilder.increaseMotcleCount(motcleLength);
        baseCountStatsBuilder.increaseMotcleCount(motcleLength);
        engineCountStatsBuilder.increaseMotcleCount(motcleLength);
        for (Integer motcleCode : motcleCodeList) {
            MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
            for (Label label : motcleData.getLabels()) {
                Lang lang = label.getLang();
                thesaurusLangStatsBuilder.addOne(lang);
                motcleEngineStatsBuilder.addOne(lang);
            }
        }
        Integer thesaurusCode = thesaurusData.getThesaurusCode();
        langStatsBuilderMap.put(thesaurusCode, thesaurusLangStatsBuilder);
        IndexationInfo indexationInfo = infoMap.getIndexationInfo(thesaurusCode);
        if (indexationInfo != null) {
            thesaurusCountStatsBuilder.increaseFicheCount(indexationInfo.getIndexedCount());
            thesaurusCountStatsBuilder.increaseIndexationCount(indexationInfo.getIndexationCount());
        }
        countStatsMap.put(thesaurusCode, thesaurusCountStatsBuilder.toCountStats());
    }

    private ScrutariDBStats toScrutariDBStats() {
        Map<Integer, LangStats> langStatsMap = new HashMap<Integer, LangStats>();
        for (Map.Entry<Integer, LangStatsBuilder> entry : langStatsBuilderMap.entrySet()) {
            langStatsMap.put(entry.getKey(), entry.getValue().toLangStats());
        }
        return new InternalScrutariDBStats(engineCountStatsBuilder.toCountStats(), countStatsMap, ficheEngineStatsBuilder.toLangStats(), motcleEngineStatsBuilder.toLangStats(), langStatsMap);
    }

    public static ScrutariDBStats build(DataAccess dataAccess, LexieAccess lexieAccess) {
        ScrutariDBStatsBuilder builder = new ScrutariDBStatsBuilder(dataAccess, lexieAccess);
        builder.check();
        return builder.toScrutariDBStats();

    }


    private static class InternalScrutariDBStats implements ScrutariDBStats {

        private final CountStats engineCountStats;
        private final Map<Integer, CountStats> countStatsMap;
        private final LangStats ficheEngineStats;
        private final LangStats motcleEngineStats;
        private final Map<Integer, LangStats> langStatsMap;

        private InternalScrutariDBStats(CountStats engineCountStats, Map<Integer, CountStats> countStatsMap, LangStats ficheEngineStats, LangStats motcleEngineStats, Map<Integer, LangStats> langStatsMap) {
            this.engineCountStats = engineCountStats;
            this.countStatsMap = countStatsMap;
            this.motcleEngineStats = motcleEngineStats;
            this.ficheEngineStats = ficheEngineStats;
            this.langStatsMap = langStatsMap;
        }

        @Override
        public CountStats getEngineCountStats() {
            return engineCountStats;
        }

        @Override
        public CountStats getCountStats(Integer code) {
            CountStats countStats = countStatsMap.get(code);
            return countStats;
        }

        @Override
        public LangStats getFicheEngineLangStats() {
            return ficheEngineStats;
        }

        @Override
        public LangStats getMotcleEngineLangStats() {
            return motcleEngineStats;
        }


        @Override
        public LangStats getLangStats(Integer code) {
            LangStats ficheStats = langStatsMap.get(code);
            if (ficheStats == null) {
                return LangStatsBuilder.EMPTY_LANGSTATS;
            } else {
                return ficheStats;
            }
        }

    }


    private static class IndexationInfo {

        private int indexationCount = 0;
        private Set<Integer> codeSet;

        private IndexationInfo() {
        }

        public int getIndexationCount() {
            return indexationCount;
        }

        public int getIndexedCount() {
            if (codeSet == null) {
                return 0;
            }
            return codeSet.size();
        }

        private void incrementCount() {
            indexationCount++;
        }

        private void addIndexedCode(int code) {
            if (codeSet == null) {
                codeSet = new HashSet<Integer>();
            }
            codeSet.add(code);
        }

    }


    private static class IndexationInfoMap {

        private final Map<Integer, IndexationInfo> map = new HashMap<Integer, IndexationInfo>();
        private final Set<Long> indexationSet = new HashSet<Long>();

        private IndexationInfoMap(DataAccess dataAccess) {
            List<CorpusData> corpusDataList = dataAccess.getCorpusDataList();
            for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                Integer thesaurusCode = thesaurusData.getThesaurusCode();
                List<Integer> motcleCodeList = thesaurusData.getMotcleCodeList();
                for (Integer motcleCode : motcleCodeList) {
                    MotcleInfo motcleInfo = dataAccess.getMotcleInfo(motcleCode);
                    for (CorpusData corpusData : corpusDataList) {
                        Integer corpusCode = corpusData.getCorpusCode();
                        List<Integer> indexationFicheCodeList = motcleInfo.getIndexationCodeList(corpusCode);
                        if (!indexationFicheCodeList.isEmpty()) {
                            indexationSet.add(PrimUtils.toLong(corpusCode, thesaurusCode));
                            for (Integer ficheCode : indexationFicheCodeList) {
                                IndexationInfo corpusIndexationInfo = getOrCreate(corpusCode);
                                IndexationInfo thesaurusIndexationInfo = getOrCreate(thesaurusCode);
                                corpusIndexationInfo.incrementCount();
                                thesaurusIndexationInfo.incrementCount();
                                corpusIndexationInfo.addIndexedCode(motcleCode);
                                thesaurusIndexationInfo.addIndexedCode(ficheCode);
                            }
                        }
                    }
                }
            }
        }

        public IndexationInfo getIndexationInfo(Integer code) {
            return map.get(code);
        }

        private IndexationInfo getOrCreate(Integer code) {
            IndexationInfo indexationInfo = map.get(code);
            if (indexationInfo == null) {
                indexationInfo = new IndexationInfo();
                map.put(code, indexationInfo);
            }
            return indexationInfo;
        }

    }


}
