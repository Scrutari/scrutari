/* ScrutariLib_DB - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.lexies;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LocalisationLexieFilters;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.collation.CollationUnitPosition;
import net.mapeadores.util.text.lexie.LexieFilter;
import net.mapeadores.util.text.lexie.LexieParseHandler;
import net.mapeadores.util.text.lexie.LexieParser;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.caches.LexieCacheWriter;
import net.scrutari.lexie.Alinea;
import net.scrutari.lexie.AlineaOccurrence;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.FieldRank;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.MotcleLexification;


/**
 *
 * @author Vincent Calame
 */
class OccurrencesBuffer {

    private final FieldRankManager fieldRankManager;
    private final LexificationEngine lexificationEngine;
    private final LexieCacheWriter lexieCacheWriter;
    private final InternalLexieParseHandler lexieParseHandler = new InternalLexieParseHandler();
    private final List<LexieId> currentLexieIdList = new ArrayList<LexieId>();
    private final List<CollationUnitPosition> currentCollationUnitPositionList = new ArrayList<CollationUnitPosition>();
    private final Map<LexieId, LexieOccurrencesBuilder> currentLexieOccurrencesBuilderMap = new HashMap<LexieId, LexieOccurrencesBuilder>();
    private final AlineaRank motcleRank;
    private Integer currentSubsetCode;
    private Lang currentLang;
    private AlineaRank currentAlineaRank;
    private Integer currentCode;
    private int currentWholeLength;
    private LexieFilter currentLexieFilter;


    OccurrencesBuffer(FieldRankManager fieldRankManager, LexificationEngine lexificationEngine, LexieCacheWriter lexieCacheWriter) {
        this.fieldRankManager = fieldRankManager;
        this.lexificationEngine = lexificationEngine;
        this.lexieCacheWriter = lexieCacheWriter;
        this.motcleRank = fieldRankManager.getSpecialAlineaRank("motcles");
    }

    void setCurrentSubsetCode(Integer currentSubsetCode) {
        this.currentSubsetCode = currentSubsetCode;
    }

    void initFicheData(FicheLexification ficheLexification, FicheData ficheData) {
        this.currentCode = ficheData.getFicheCode();
        setCurrentLang(ficheData.getLang());
        initAlinea(fieldRankManager.getSpecialAlineaRank("titre"), ficheLexification, ficheData.getTitre());
        initAlinea(fieldRankManager.getSpecialAlineaRank("soustitre"), ficheLexification, ficheData.getSoustitre());
        int complementMaxNumber = ficheData.getComplementMaxNumber();
        for (int i = 1; i <= complementMaxNumber; i++) {
            String complement = ficheData.getComplementByNumber(i);
            initAlinea(fieldRankManager.getComplementAlineaRank(i), ficheLexification, complement);
        }
        Attributes attributes = ficheData.getAttributes();
        int attributeLength = attributes.size();
        for (int i = 0; i < attributeLength; i++) {
            Attribute attribute = attributes.get(i);
            AttributeKey attributeKey = attribute.getAttributeKey();
            FieldRank fieldRank = fieldRankManager.getAttributeFieldRank(attributeKey);
            if (fieldRank.isTextField()) {
                int valueLength = attribute.size();
                for (int j = 0; j < valueLength; j++) {
                    String value = attribute.get(j);
                    initAlinea(AlineaRank.build(fieldRank, j + 1), ficheLexification, value);
                }
            }
        }
        flushLexieOccurrences();
    }

    void initMotcleData(MotcleLexification motcleLexification, MotcleData motcleData) {
        this.currentCode = motcleData.getMotcleCode();
        setCurrentAlineaRank(motcleRank);
        for (Label label : motcleData.getLabels()) {
            setCurrentLang(label.getLang());
            tokenize(label.getLabelString());
            flushLexification(motcleLexification);
        }
        flushLexieOccurrences();
    }

    void addLexie(LexieId lexieId, CollationUnitPosition collationUnitPosition) {
        LexieOccurrencesBuilder lexieOccurrencesBuilder = currentLexieOccurrencesBuilderMap.get(lexieId);
        if (lexieOccurrencesBuilder == null) {
            lexieOccurrencesBuilder = new LexieOccurrencesBuilder(getCurrentCode(), lexieId);
            currentLexieOccurrencesBuilderMap.put(lexieId, lexieOccurrencesBuilder);
        }
        int lexieIndexInField = currentLexieIdList.size();
        currentLexieIdList.add(lexieId);
        currentCollationUnitPositionList.add(collationUnitPosition);
        lexieOccurrencesBuilder.addOccurrence(lexieIndexInField, collationUnitPosition, getCurrentAlineaRank(), getCurrentWholeLength());
    }

    private void initAlinea(AlineaRank alineaRank, FicheLexification ficheLexification, String text) {
        setCurrentAlineaRank(alineaRank);
        tokenize(text);
        flushLexification(ficheLexification);
    }

    private void tokenize(String chaine) {
        if (chaine == null) {
            return;
        }
        int length = chaine.length();
        if (length == 0) {
            return;
        }
        setCurrentWholeLength(length);
        LexieParser.parse(chaine, lexieParseHandler);
    }


    private void flushLexieOccurrences() {
        for (LexieOccurrencesBuilder lexieOccurrencesBuilder : currentLexieOccurrencesBuilderMap.values()) {
            LexieId lexieId = lexieOccurrencesBuilder.lexieId;
            lexieCacheWriter.cacheLexieOccurrences(lexieId, currentSubsetCode, lexieOccurrencesBuilder.toLexieOccurences());
        }
        currentLexieOccurrencesBuilderMap.clear();
    }

    private Alinea flushAlinea() {
        int size = currentLexieIdList.size();
        if (size == 0) {
            return null;
        }
        LexieId[] lexieIdArray = new LexieId[size];
        CollationUnitPosition[] collationUnitPositionArray = new CollationUnitPosition[size];
        for (int i = 0; i < size; i++) {
            lexieIdArray[i] = currentLexieIdList.get(i);
            collationUnitPositionArray[i] = currentCollationUnitPositionList.get(i);
        }
        Alinea alinea = new Alinea();
        alinea.setWholeLength(getCurrentWholeLength());
        alinea.setLexieIdArray(lexieIdArray);
        alinea.setCollationUnitPositionArray(collationUnitPositionArray);
        currentLexieIdList.clear();
        currentCollationUnitPositionList.clear();
        return alinea;
    }

    private Lang getCurrentLang() {
        return currentLang;
    }

    private void setCurrentLang(Lang currentLang) {
        this.currentLang = currentLang;
        currentLexieFilter = LocalisationLexieFilters.getLexieFilter(currentLang);
    }

    private AlineaRank getCurrentAlineaRank() {
        return currentAlineaRank;
    }

    private void setCurrentAlineaRank(AlineaRank currentAlineaRank) {
        this.currentAlineaRank = currentAlineaRank;
    }

    private Integer getCurrentCode() {
        return currentCode;
    }

    private int getCurrentWholeLength() {
        return currentWholeLength;
    }

    private void setCurrentWholeLength(int currentWholeLength) {
        this.currentWholeLength = currentWholeLength;
    }

    private void flushLexification(FicheLexification ficheLexification) {
        Alinea alinea = flushAlinea();
        if (alinea != null) {
            ficheLexification.putAlinea(getCurrentAlineaRank(), alinea);
        }
    }

    private void flushLexification(MotcleLexification motcleLexification) {
        Alinea lexification = flushAlinea();
        if (lexification != null) {
            motcleLexification.putAlinea(getCurrentLang(), lexification);
        }
    }


    private static class LexieOccurrencesBuilder {

        private final Integer code;
        private LexieId lexieId;
        private final List<AlineaOccurrenceBuilder> alineaOccurrenceBuilderList = new ArrayList<AlineaOccurrenceBuilder>();
        private AlineaOccurrenceBuilder lastOccurrenceByAlinea;

        private LexieOccurrencesBuilder(Integer code, LexieId lexieId) {
            this.code = code;
            this.lexieId = lexieId;
        }

        protected void addOccurrence(int lexieIndexInAlinea, CollationUnitPosition collationUnitPosition, AlineaRank currentAlineaRank, int alineaWholeLength) {
            if ((lastOccurrenceByAlinea == null) || (!lastOccurrenceByAlinea.alineaRank.equals(currentAlineaRank))) {
                lastOccurrenceByAlinea = new AlineaOccurrenceBuilder(currentAlineaRank, collationUnitPosition, alineaWholeLength);
                alineaOccurrenceBuilderList.add(lastOccurrenceByAlinea);
            }
            lastOccurrenceByAlinea.addLexieIndex(lexieIndexInAlinea);
        }

        public LexieOccurrences toLexieOccurences() {
            int length = alineaOccurrenceBuilderList.size();
            AlineaOccurrence[] alineaOccurrenceArray = new AlineaOccurrence[length];
            for (int i = 0; i < length; i++) {
                alineaOccurrenceArray[i] = alineaOccurrenceBuilderList.get(i).toAlineaOccurrence();
            }
            return new LexieOccurrences(code, alineaOccurrenceArray);
        }

    }


    private static class AlineaOccurrenceBuilder {

        private final AlineaRank alineaRank;
        private final CollationUnitPosition bestPositionInAlinea;
        private final int alineaWholeLength;
        private final List<Integer> lexieIndexList = new ArrayList<Integer>();

        private AlineaOccurrenceBuilder(AlineaRank alineaRank, CollationUnitPosition bestPositionInAlinea, int alineaWholeLength) {
            this.alineaRank = alineaRank;
            this.bestPositionInAlinea = bestPositionInAlinea;
            this.alineaWholeLength = alineaWholeLength;
        }

        private void addLexieIndex(int lexieIndex) {
            lexieIndexList.add(lexieIndex);
        }

        public AlineaOccurrence toAlineaOccurrence() {
            return new AlineaOccurrence(alineaRank, bestPositionInAlinea, alineaWholeLength, PrimUtils.toIntArray(lexieIndexList));
        }

    }


    private class InternalLexieParseHandler implements LexieParseHandler {

        private InternalLexieParseHandler() {
        }

        @Override
        public void flushLexie(String lexie, int lexieStartIndex) {
            if (currentLexieFilter.acceptLexie(lexie)) {
                lexificationEngine.addLexie(getCurrentLang(), lexie, lexieStartIndex);
            }
        }

        @Override
        public void checkSpecialChar(char carac) {
        }

    }

}
