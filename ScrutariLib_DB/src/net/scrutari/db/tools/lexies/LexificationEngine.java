/* ScrutariLib_DB - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.lexies;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.collation.CollationUnit;
import net.mapeadores.util.text.collation.CollationUnitPosition;
import net.mapeadores.util.text.collation.map.CollatedKeyMap;
import net.mapeadores.util.text.collation.map.SortedCollatedKeyMap;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheWriter;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieUtils;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public final class LexificationEngine {

    private final OccurrencesBuffer occurrencesBuffer;
    private final Map<Lang, CollatedKeyMap<LexieId>> lexieMapByLangMap = new HashMap<Lang, CollatedKeyMap<LexieId>>();
    private final Map<LexieId, ScrutariLexieUnit> lexieUnitMap = new LinkedHashMap<LexieId, ScrutariLexieUnit>();
    private final LexieCacheWriter lexieCacheWriter;
    private long newId = 1;

    private LexificationEngine(FieldRankManager fieldRankManager, LexieCacheWriter lexieCacheWriter) {
        this.lexieCacheWriter = lexieCacheWriter;
        occurrencesBuffer = new OccurrencesBuffer(fieldRankManager, this, lexieCacheWriter);
    }

    CollatedKeyMap<LexieId> getOrCreateLexieMap(Lang lang) {
        CollatedKeyMap<LexieId> collatedStringMap = lexieMapByLangMap.get(lang);
        if (collatedStringMap == null) {
            collatedStringMap = new SortedCollatedKeyMap<LexieId>(lang.toLocale());
            lexieMapByLangMap.put(lang, collatedStringMap);
        }
        return collatedStringMap;
    }

    void addLexie(Lang lang, String sourceLexie, int lexieStartIndex) {
        CollatedKeyMap<LexieId> map = getOrCreateLexieMap(lang);
        CollationUnit collationUnit = new CollationUnit(sourceLexie, map.getCollator());
        LexieId lexieId = map.getValueByCollatedKey(collationUnit.getCollatedString());
        ScrutariLexieUnit scrutariLexieUnit;
        if (lexieId == null) {
            lexieId = new LexieId(newId);
            newId++;
            scrutariLexieUnit = LexieUtils.toScrutariLexieUnit(lexieId, lang, collationUnit.getCollatedString(), collationUnit.getSourceString());
            lexieUnitMap.put(lexieId, scrutariLexieUnit);
            map.putValueByCollatedKey(scrutariLexieUnit.getCollatedLexie(), lexieId);
        } else {
            scrutariLexieUnit = lexieUnitMap.get(lexieId);
            if (CollationUnit.checkReplace(scrutariLexieUnit.getCanonicalLexie(), collationUnit.getSourceString())) {
                scrutariLexieUnit = LexieUtils.derive(scrutariLexieUnit, collationUnit.getSourceString());
                lexieUnitMap.put(lexieId, scrutariLexieUnit);
            }
        }
        CollationUnitPosition collationUnitPosition = new CollationUnitPosition(collationUnit, lexieStartIndex);
        occurrencesBuffer.addLexie(lexieId, collationUnitPosition);
    }

    private void newCorpus(Integer corpusCode) {
        occurrencesBuffer.setCurrentSubsetCode(corpusCode);
    }

    private void initFicheData(FicheLexification ficheLexification, FicheData ficheData) {
        ficheLexification.clear();
        occurrencesBuffer.initFicheData(ficheLexification, ficheData);
        lexieCacheWriter.cacheFicheLexification(ficheData.getFicheCode(), ficheLexification);
    }

    private void newThesaurus(Integer thesaurusCode) {
        occurrencesBuffer.setCurrentSubsetCode(thesaurusCode);
    }

    private void initMotcleData(MotcleLexification motcleLexification, MotcleData motcleData) {
        motcleLexification.clear();
        occurrencesBuffer.initMotcleData(motcleLexification, motcleData);
        lexieCacheWriter.cacheMotcleLexification(motcleData.getMotcleCode(), motcleLexification);
    }

    private Result end() {
        List<ScrutariLexieUnit> scrutariLexieUnitList = new ArrayList<ScrutariLexieUnit>(lexieUnitMap.values());
        return new Result(lexieCacheWriter.endCacheWrite(scrutariLexieUnitList), scrutariLexieUnitList);
    }


    public static Result cacheLexification(DataAccess dataAccess, FieldRankManager fieldRankManager, LexieCacheWriter lexieCacheWriter) {
        FicheLexification ficheLexification = new FicheLexification();
        MotcleLexification motcleLexification = new MotcleLexification();
        LexificationEngine engine = new LexificationEngine(fieldRankManager, lexieCacheWriter);
        for (CorpusData corpusData : dataAccess.getCorpusDataList()) {
            engine.newCorpus(corpusData.getCorpusCode());
            for (Integer ficheCode : corpusData.getFicheCodeList()) {
                FicheData ficheData = dataAccess.getFicheData(ficheCode);
                engine.initFicheData(ficheLexification, ficheData);
            }
        }
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            engine.newThesaurus(thesaurusData.getThesaurusCode());
            for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
                MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                engine.initMotcleData(motcleLexification, motcleData);
            }
        }
        return engine.end();
    }


    public static class Result {

        private final LexieCacheReaderFactory lexieCacheReaderFactory;
        private final Collection<ScrutariLexieUnit> scrutariLexieUnits;

        private Result(LexieCacheReaderFactory lexieCacheReaderFactory, Collection<ScrutariLexieUnit> scrutariLexieUnits) {
            this.lexieCacheReaderFactory = lexieCacheReaderFactory;
            this.scrutariLexieUnits = scrutariLexieUnits;
        }

        public LexieCacheReaderFactory getLexieCacheReaderFactory() {
            return lexieCacheReaderFactory;
        }

        public Collection<ScrutariLexieUnit> getScrutariLexieUnits() {
            return scrutariLexieUnits;
        }

    }

}
