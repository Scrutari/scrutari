/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.List;
import net.scrutari.data.BaseData;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.tools.util.DataUtils;


/**
 *
 * @author Vincent Calame
 */
class BaseInfo extends AbstractDataInfo {

    private final BaseData baseData;

    BaseInfo(Integer code, ScrutariDBName firstAdd, BaseData baseData) {
        super(code, firstAdd);
        this.baseData = baseData;
    }

    public BaseData getBaseData() {
        return baseData;
    }

    static List<BaseData> toBaseDataList(List<BaseInfo> infoList) {
        int size = infoList.size();
        int p = 0;
        BaseData[] array = new BaseData[size];
        for (BaseInfo info : infoList) {
            array[p] = info.getBaseData();
            p++;
        }
        return DataUtils.wrap(array);
    }

}
