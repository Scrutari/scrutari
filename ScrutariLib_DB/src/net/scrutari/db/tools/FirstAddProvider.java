/* ScrutariLib_DB - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.Map;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class FirstAddProvider {

    private final Map<ScrutariDataURI, ScrutariDBName> map;
    private final ScrutariDBName defaultDBName;

    public FirstAddProvider(Map<ScrutariDataURI, ScrutariDBName> map, ScrutariDBName defaultDBName) {
        this.map = map;
        this.defaultDBName = defaultDBName;
    }

    public ScrutariDBName getFirstAdd(ScrutariDataURI scrutariDataURI) {
        ScrutariDBName scrutariDBName = map.get(scrutariDataURI);
        if (scrutariDBName != null) {
            return scrutariDBName;
        } else {
            return defaultDBName;
        }
    }

    public boolean hasFirstAdd(ScrutariDataURI scrutariDataURI) {
        return map.containsKey(scrutariDataURI);
    }

}
