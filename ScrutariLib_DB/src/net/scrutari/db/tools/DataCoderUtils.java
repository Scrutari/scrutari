/* ScrutariLib_DB - Copyright (c) 2014-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */
package net.scrutari.db.tools;

import java.util.HashMap;
import java.util.Map;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataCoder;


/**
 *
 * @author Vincent Calame
 */
public final class DataCoderUtils {

    private DataCoderUtils() {

    }

    public static DataCoder encapsulate(URIHistory historyDataCoder) {
        return new InternalDataCoder(historyDataCoder);
    }


    private static class InternalDataCoder implements DataCoder {

        private final URIHistory uriHistory;
        private final Map<ScrutariDataURI, Integer> codeByScrutariDataURIMap = new HashMap<ScrutariDataURI, Integer>();

        private InternalDataCoder(URIHistory uriHistory) {
            this.uriHistory = uriHistory;
        }

        @Override
        public Integer getCode(ScrutariDataURI scrutariDataURI, boolean createIfNotExist) {
            Integer code = codeByScrutariDataURIMap.get(scrutariDataURI);
            if (code != null) {
                return code;
            } else if (!createIfNotExist) {
                return null;
            }
            code = uriHistory.getOrCreateCode(scrutariDataURI);
            codeByScrutariDataURIMap.put(scrutariDataURI, code);
            return code;
        }

    }

}
