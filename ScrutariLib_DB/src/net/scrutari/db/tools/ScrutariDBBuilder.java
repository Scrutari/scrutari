/* ScrutariLib_DB - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import net.mapeadores.util.logging.TimeLog;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.DataAccessProvider;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.LexieAccessProvider;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.api.caches.DataCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheReaderFactory;
import net.scrutari.db.api.caches.LexieCacheWriter;
import net.scrutari.db.api.stats.ScrutariDBStats;
import net.scrutari.db.tools.stats.ScrutariDBStatsBuilder;


/**
 *
 * @author Vincent Calame
 */
public final class ScrutariDBBuilder {

    private ScrutariDBBuilder() {

    }

    public static ScrutariDB build(ScrutariDBName scrutariDBName, FieldRankManager fieldRankManager, DataCacheReaderFactory dataCacheReaderFactory, LexieCacheWriter lexieCacheWriter, ScrutariDataURIChecker scrutariDataURIChecker, FirstAddProvider firstAddProvider, TimeLog timeLog) {
        timeLog.logAction("dataAccessProvider");
        DataAccessProvider dataAccessProvider = DataAccessProviderBuilder.build(dataCacheReaderFactory, scrutariDataURIChecker, firstAddProvider);
        try (DataAccess dataAccess = dataAccessProvider.openDataAccess()) {
            timeLog.logAction("lexieAccessProvider");
            LexieAccessProvider lexieAccessProvider = LexieAccessProviderBuilder.build(dataAccess, fieldRankManager, lexieCacheWriter);
            try (LexieAccess lexieAccess = lexieAccessProvider.openLexieAccess()) {
                timeLog.logAction("scrutariDBStats");
                ScrutariDBStats stats = ScrutariDBStatsBuilder.build(dataAccess, lexieAccess);
                return new InternalScrutariDB(scrutariDBName, dataAccessProvider, lexieAccessProvider, fieldRankManager, stats);
            }
        }
    }

    public static ScrutariDB build(ScrutariDBName scrutariDBName, FieldRankManager fieldRankManager, DataCacheReaderFactory dataCacheReaderFactory, LexieCacheReaderFactory lexieCacheReaderFactory, ScrutariDataURIChecker scrutariDataURIChecker, FirstAddProvider firstAddProvider, TimeLog timeLog) {
        timeLog.logAction("dataAccessProvider");
        DataAccessProvider dataAccessProvider = DataAccessProviderBuilder.build(dataCacheReaderFactory, scrutariDataURIChecker, firstAddProvider);
        timeLog.logAction("lexieAccessProvider");
        LexieAccessProvider lexieAccessProvider = LexieAccessProviderBuilder.build(lexieCacheReaderFactory);
        try (DataAccess dataAccess = dataAccessProvider.openDataAccess(); LexieAccess lexieAccess = lexieAccessProvider.openLexieAccess()) {
            timeLog.logAction("scrutariDBStats");
            ScrutariDBStats stats = ScrutariDBStatsBuilder.build(dataAccess, lexieAccess);
            return new InternalScrutariDB(scrutariDBName, dataAccessProvider, lexieAccessProvider, fieldRankManager, stats);
        }
    }


    private static class InternalScrutariDB implements ScrutariDB {

        private final ScrutariDBName scrutariDBName;
        private final DataAccessProvider dataAccessProvider;
        private final LexieAccessProvider lexieAccessProvider;
        private final FieldRankManager fieldRankManager;
        private final ScrutariDBStats stats;

        private InternalScrutariDB(ScrutariDBName scrutariDBName, DataAccessProvider dataAccessProvider, LexieAccessProvider lexieAccessProvider, FieldRankManager fieldRankManager, ScrutariDBStats stats) {
            this.scrutariDBName = scrutariDBName;
            this.dataAccessProvider = dataAccessProvider;
            this.lexieAccessProvider = lexieAccessProvider;
            this.fieldRankManager = fieldRankManager;
            this.stats = stats;
        }

        @Override
        public ScrutariDBName getScrutariDBName() {
            return scrutariDBName;
        }

        @Override
        public DataAccess openDataAccess() {
            return dataAccessProvider.openDataAccess();
        }

        @Override
        public LexieAccess openLexieAccess() {
            return lexieAccessProvider.openLexieAccess();
        }

        @Override
        public FieldRankManager getFieldRankManager() {
            return fieldRankManager;
        }

        @Override
        public ScrutariDBStats getStats() {
            return stats;
        }

    }

}
