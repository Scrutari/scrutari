/* ScrutariLib_DB - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.primitives.io.PrimitivesReader;
import net.mapeadores.util.primitives.io.PrimitivesWriter;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.db.api.FieldRankManager;
import net.scrutari.db.api.ScrutariConstants;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FieldRank;


/**
 *
 * @author Vincent Calame
 */
public class FieldRankManagerBuilder {

    private final static String[] DEFAULT_ORDER = {"titre", "soustitre", "primary", "complement", "secondary"};
    private final Set<AttributeKey> existingSet = new HashSet<AttributeKey>();
    private final List<AttributeKey> primaryList = new ArrayList<AttributeKey>();
    private final List<AttributeKey> secondaryList = new ArrayList<AttributeKey>();
    private final List< AttributeKey> technicalList = new ArrayList<AttributeKey>();
    private final Map<AttributeKey, FieldRank> attributeRankMap = new HashMap<AttributeKey, FieldRank>();
    private final List<String> orderList = new ArrayList<String>();
    private FieldRank titreRank = null;
    private FieldRank soustitreRank = null;
    private FieldRank motclesRank = null;
    private int orderLength = 0;
    private int complementOrder = -1;
    private int primaryOrder = -1;
    private int secondaryOrder = -1;

    public FieldRankManagerBuilder() {
    }

    public FieldRankManagerBuilder addOrder(String order) {
        orderList.add(order);
        return this;
    }

    public FieldRankManagerBuilder putSpecialFieldRank(String name, FieldRank fieldRank) {
        switch (name) {
            case "titre":
                this.titreRank = fieldRank;
                break;
            case "soustitre":
                this.soustitreRank = fieldRank;
                break;
            case "motcles":
                this.motclesRank = fieldRank;
                break;
        }
        return this;
    }

    public FieldRankManagerBuilder putAttributeFieldRank(AttributeKey attributeKey, FieldRank fieldRank) {
        attributeRankMap.put(attributeKey, fieldRank);
        return this;
    }

    public FieldRankManagerBuilder setComplementOrder(int complementOrder) {
        this.complementOrder = complementOrder;
        return this;
    }

    private void initAttributeLists(Map<String, AttributeDef[]> defArrayMap) {
        if (defArrayMap == null) {
            return;
        }
        addInList(defArrayMap.get(ScrutariConstants.PRIMARY_GROUP), primaryList);
        addInList(defArrayMap.get(ScrutariConstants.SECONDARY_GROUP), secondaryList);
        AttributeDef[] technicalArray = defArrayMap.get(ScrutariConstants.TECHNICAL_GROUP);
        if (technicalArray != null) {
            for (AttributeDef attributeDef : technicalArray) {
                AttributeKey attributeKey = attributeDef.getAttributeKey();
                if (!existingSet.contains(attributeKey)) {
                    technicalList.add(attributeKey);
                    existingSet.add(attributeKey);
                }
            }
        }
    }

    private void addInList(AttributeDef[] array, List<AttributeKey> list) {
        if (array == null) {
            return;
        }
        for (AttributeDef attributeDef : array) {
            AttributeKey attributeKey = attributeDef.getAttributeKey();
            if (!existingSet.contains(attributeKey)) {
                if (list.size() < 999) {
                    list.add(attributeKey);
                } else {
                    technicalList.add(attributeKey);
                }
                existingSet.add(attributeKey);
            }
        }
    }

    private void initPertinenceOrder(String[] pertinenceOrder) {
        for (AttributeKey technical : technicalList) {
            attributeRankMap.put(technical, FieldRank.TECHNICAL_ATTRIBUTE);
        }
        if (pertinenceOrder != null) {
            checkArray(pertinenceOrder);
        }
        checkArray(DEFAULT_ORDER);
        int primaryPosition = 1;
        for (AttributeKey primary : primaryList) {
            if (!attributeRankMap.containsKey(primary)) {
                attributeRankMap.put(primary, FieldRank.build(primaryOrder, primaryPosition, FieldRank.PRIMARY_TYPE));
                primaryPosition++;
            }
        }
        int secondaryPosition = 1;
        for (AttributeKey secondary : secondaryList) {
            if (!attributeRankMap.containsKey(secondary)) {
                attributeRankMap.put(secondary, FieldRank.build(secondaryOrder, secondaryPosition, FieldRank.SECONDARY_TYPE));
                secondaryPosition++;
            }
        }
    }

    private void checkArray(String[] tokens) {
        for (String token : tokens) {
            boolean done = false;
            switch (token) {
                case "titre":
                    done = addTitre();
                    break;
                case "soustitre":
                    done = addSoustitre();
                    break;
                case "complement":
                    if (complementOrder == -1) {
                        orderLength++;
                        complementOrder = orderLength;
                        done = true;
                    }
                    break;
                case "primary":
                    if (primaryOrder == -1) {
                        orderLength++;
                        primaryOrder = orderLength;
                        done = true;
                    }
                    break;
                case "secondary":
                    if (secondaryOrder == -1) {
                        orderLength++;
                        secondaryOrder = orderLength;
                        done = true;
                    }
                    break;
                default:
                    try {
                    AttributeKey attributeKey = AttributeKey.parse(token);
                    if ((existingSet.contains(attributeKey)) && (!attributeRankMap.containsKey(attributeKey))) {
                        orderLength++;
                        if (primaryList.contains(attributeKey)) {
                            attributeRankMap.put(attributeKey, FieldRank.build(orderLength, 1, FieldRank.PRIMARY_TYPE));
                        } else {
                            attributeRankMap.put(attributeKey, FieldRank.build(orderLength, 1, FieldRank.SECONDARY_TYPE));
                        }
                        done = true;
                    }
                } catch (ParseException pe) {

                }
            }
            if (done) {
                orderList.add(token);
            }
        }
    }

    private boolean addTitre() {
        if (titreRank != null) {
            return false;
        }
        orderLength++;
        titreRank = FieldRank.build(orderLength, 1, FieldRank.TITRE_TYPE);
        return true;
    }

    private boolean addSoustitre() {
        if (soustitreRank != null) {
            return false;
        }
        orderLength++;
        soustitreRank = FieldRank.build(orderLength, 1, FieldRank.SOUSTITRE_TYPE);
        return true;
    }

    public FieldRankManager toFieldRankManager() {
        if (motclesRank == null) {
            motclesRank = FieldRank.build(complementOrder, 0, FieldRank.MOTCLES_TYPE);
        }
        List<String> finalOrderList = StringUtils.toList(orderList);
        return new InternalFieldRankManager(finalOrderList, titreRank, soustitreRank, motclesRank, attributeRankMap, complementOrder);
    }

    public static FieldRankManager build(Map<String, AttributeDef[]> defArrayMap, String[] pertinenceOrder) {
        FieldRankManagerBuilder builder = new FieldRankManagerBuilder();
        builder.initAttributeLists(defArrayMap);
        builder.initPertinenceOrder(pertinenceOrder);
        return builder.toFieldRankManager();
    }

    public static void toPrimitives(FieldRankManager fieldRankManager, PrimitivesWriter primitivesWriter) throws IOException {
        List<String> orderList = fieldRankManager.getOrderList();
        primitivesWriter.writeInt(orderList.size());
        for (String order : orderList) {
            primitivesWriter.writeString(order);
        }
        primitivesWriter.writeInt(fieldRankManager.getComplementOrder());
        primitivesWriter.writeInt(fieldRankManager.getSpecialFieldRank("titre").getValue());
        primitivesWriter.writeInt(fieldRankManager.getSpecialFieldRank("soustitre").getValue());
        primitivesWriter.writeInt(fieldRankManager.getSpecialFieldRank("motcles").getValue());
        Set<AttributeKey> set = fieldRankManager.getAttributeKeySet();
        primitivesWriter.writeInt(set.size());
        for (AttributeKey attributeKey : fieldRankManager.getAttributeKeySet()) {
            FieldRank fieldRank = fieldRankManager.getAttributeFieldRank(attributeKey);
            primitivesWriter.writeString(attributeKey.toString());
            primitivesWriter.writeInt(fieldRank.getValue());
        }
    }

    public static FieldRankManager fromPrimitives(PrimitivesReader primitivesReader) throws IOException {
        FieldRankManagerBuilder builder = new FieldRankManagerBuilder();
        int orderSize = primitivesReader.readInt();
        for (int i = 0; i < orderSize; i++) {
            builder.addOrder(primitivesReader.readString());
        }
        builder
                .setComplementOrder(primitivesReader.readInt())
                .putSpecialFieldRank("titre", FieldRank.build(primitivesReader.readInt()))
                .putSpecialFieldRank("soustitre", FieldRank.build(primitivesReader.readInt()))
                .putSpecialFieldRank("motcles", FieldRank.build(primitivesReader.readInt()));
        int attributeSize = primitivesReader.readInt();
        for (int i = 0; i < attributeSize; i++) {
            String attributeString = primitivesReader.readString();
            int value = primitivesReader.readInt();
            try {
                AttributeKey attributeKey = AttributeKey.parse(attributeString);
                builder.putAttributeFieldRank(attributeKey, FieldRank.build(value));
            } catch (ParseException pe) {

            }
        }
        return builder.toFieldRankManager();
    }


    private static class InternalFieldRankManager implements FieldRankManager {

        private final Map<AttributeKey, FieldRank> attributeRankMap;
        private final List<String> orderList;
        private final FieldRank titreRank;
        private final FieldRank soustitreRank;
        private final FieldRank motclesRank;
        private final AlineaRank titreAlineaRank;
        private final AlineaRank soustitreAlineaRank;
        private final AlineaRank motclesAlineaRank;
        private final int complementOrder;

        private InternalFieldRankManager(List<String> orderList, FieldRank titreRank, FieldRank soustitreRank, FieldRank motclesRank, Map<AttributeKey, FieldRank> attributeRankMap, int complementOrder) {
            this.orderList = orderList;
            this.titreRank = titreRank;
            this.soustitreRank = soustitreRank;
            this.attributeRankMap = attributeRankMap;
            this.complementOrder = complementOrder;
            this.motclesRank = motclesRank;
            this.titreAlineaRank = AlineaRank.build(titreRank, 1);
            this.soustitreAlineaRank = AlineaRank.build(soustitreRank, 1);
            this.motclesAlineaRank = AlineaRank.build(motclesRank, 1);
        }

        @Override
        public List<String> getOrderList() {
            return orderList;
        }

        @Override
        public int getComplementOrder() {
            return complementOrder;
        }

        @Override
        public FieldRank getSpecialFieldRank(String name) {
            switch (name) {
                case "titre":
                    return titreRank;
                case "soustitre":
                    return soustitreRank;
                case "motcles":
                    return motclesRank;
                default:
                    return null;
            }
        }

        @Override
        public FieldRank getAttributeFieldRank(AttributeKey attributeKey) {
            FieldRank fieldRank = attributeRankMap.get(attributeKey);
            if (fieldRank == null) {
                return FieldRank.UNKNOWN_ATTRIBUTE;
            } else {
                return fieldRank;
            }
        }

        @Override
        public AlineaRank getSpecialAlineaRank(String name) {
            switch (name) {
                case "titre":
                    return titreAlineaRank;
                case "soustitre":
                    return soustitreAlineaRank;
                case "motcles":
                    return motclesAlineaRank;
                default:
                    return null;
            }
        }

        @Override
        public Set<AttributeKey> getAttributeKeySet() {
            return Collections.unmodifiableSet(attributeRankMap.keySet());
        }

    }

}
