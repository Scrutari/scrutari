/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.List;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.tools.util.DataUtils;


/**
 *
 * @author Vincent Calame
 */
public class ThesaurusInfo extends AbstractDataInfo {

    private final ThesaurusData thesaurusData;

    ThesaurusInfo(Integer code, ScrutariDBName firstAdd, ThesaurusData thesaurusData) {
        super(code, firstAdd);
        this.thesaurusData = thesaurusData;
    }

    public ThesaurusData getThesaurusData() {
        return thesaurusData;
    }

    static List<ThesaurusData> toThesaurusDataList(List<ThesaurusInfo> infoList) {
        int size = infoList.size();
        int p = 0;
        ThesaurusData[] array = new ThesaurusData[size];
        for (ThesaurusInfo info : infoList) {
            array[p] = info.getThesaurusData();
            p++;
        }
        return DataUtils.wrap(array);
    }

}
