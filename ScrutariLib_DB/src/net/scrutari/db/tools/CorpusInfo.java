/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.List;
import net.scrutari.data.CorpusData;
import net.scrutari.db.api.ScrutariDBName;
import net.scrutari.db.tools.util.DataUtils;


/**
 *
 * @author Vincent Calame
 */
class CorpusInfo extends AbstractDataInfo {

    private final CorpusData corpusData;

    CorpusInfo(Integer code, ScrutariDBName firstAdd, CorpusData corpusData) {
        super(code, firstAdd);
        this.corpusData = corpusData;
    }

    public CorpusData getCorpusData() {
        return corpusData;
    }

    static List<CorpusData> toCorpusDataList(List<CorpusInfo> infoList) {
        int size = infoList.size();
        int p = 0;
        CorpusData[] array = new CorpusData[size];
        for (CorpusInfo info : infoList) {
            array[p] = info.getCorpusData();
            p++;
        }
        return DataUtils.wrap(array);
    }

}
