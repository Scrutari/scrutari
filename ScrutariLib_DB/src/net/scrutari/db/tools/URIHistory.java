/* ScrutariLib_DB - Copyright (c) 2013-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.MotcleNode;
import net.scrutari.datauri.tree.ThesaurusNode;
import net.scrutari.datauri.tree.URINode;
import net.scrutari.datauri.tree.URITree;
import net.scrutari.datauri.tree.URITreeBuilder;
import net.scrutari.db.api.CodeMatch;


/**
 *
 * @author Vincent Calame
 */
public class URIHistory {

    private final Map<ScrutariDataURI, Integer> uriMap;
    private final Set<ScrutariDataURI> newUriSet = new LinkedHashSet<ScrutariDataURI>();
    private final FirstAddProvider firstAddProvider;
    private int maxCode;

    private URIHistory(Map<ScrutariDataURI, Integer> uriMap, int maxCode, FirstAddProvider firstAddProvider) {
        this.uriMap = uriMap;
        this.maxCode = maxCode;
        this.firstAddProvider = firstAddProvider;
    }

    public Integer getOrCreateCode(ScrutariDataURI scrutariDataURI) {
        Integer code = uriMap.get(scrutariDataURI);
        if (code == null) {
            code = maxCode;
            maxCode++;
            uriMap.put(scrutariDataURI, code);
        }
        if (!firstAddProvider.hasFirstAdd(scrutariDataURI)) {
            newUriSet.add(scrutariDataURI);
        }
        return code;
    }

    public boolean hasNewURI() {
        return (!newUriSet.isEmpty());
    }

    public URITree toURITree() {
        URITreeBuilder builder = new URITreeBuilder();
        for (Map.Entry<ScrutariDataURI, Integer> entry : uriMap.entrySet()) {
            builder.addScrutariDataURI(entry.getKey(), entry.getValue());
        }
        return builder.toURITree();
    }

    public URITree getNewlyAddedURITree() {
        if (newUriSet.isEmpty()) {
            return null;
        }
        URITreeBuilder builder = new URITreeBuilder();
        for (ScrutariDataURI uri : newUriSet) {
            builder.addScrutariDataURI(uri, null);
        }
        return builder.toURITree();
    }

    public static URIHistory build(CodeMatch[] initialCodeMatchArray, URITree initialURITree, FirstAddProvider firstAddProvider) {
        Builder builder = new Builder();
        if (initialCodeMatchArray != null) {
            for (CodeMatch codeMatch : initialCodeMatchArray) {
                builder.add(codeMatch.getScrutariDataURI(), codeMatch.getCode());
            }
        }
        builder.add(initialURITree);
        return new URIHistory(builder.uriMap, builder.maxInitialCode + 1, firstAddProvider);
    }


    private static class Builder {

        private final Set<Integer> usedCodeSet = new HashSet<Integer>();
        private final Map<ScrutariDataURI, Integer> uriMap = new LinkedHashMap<ScrutariDataURI, Integer>();
        private int maxInitialCode = 1000;

        private void add(URITree uriTree) {
            if (uriTree == null) {
                return;
            }
            for (BaseNode baseNode : uriTree.getBaseNodeList()) {
                add(baseNode.getBaseURI(), baseNode);
                for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
                    add(corpusNode.getCorpusURI(), corpusNode);
                    for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
                        add(ficheNode.getFicheURI(), ficheNode);
                    }
                }
                for (ThesaurusNode thesaurusNode : baseNode.getThesaurusNodeList()) {
                    add(thesaurusNode.getThesaurusURI(), thesaurusNode);
                    for (MotcleNode motcleNode : thesaurusNode.getMotcleNodeList()) {
                        add(motcleNode.getMotcleURI(), motcleNode);
                    }
                }
            }
        }

        private void add(ScrutariDataURI scrutariDataURI, URINode uriNode) {
            if (!uriNode.isActive()) {
                return;
            }
            Object obj = uriNode.getAssociatedObject();
            if (obj == null) {
                return;
            }
            int code = 0;
            if (obj instanceof Integer) {
                code = ((Integer) obj);
            } else if (obj instanceof String) {
                try {
                    code = Integer.parseInt((String) obj);
                } catch (NumberFormatException nfe) {
                }
            }
            if (code > 0) {
                add(scrutariDataURI, code);
            }
        }

        private void add(ScrutariDataURI scrutariDataURI, Integer code) {
            if ((!uriMap.containsKey(scrutariDataURI)) && (!usedCodeSet.contains(code))) {
                maxInitialCode = Math.max(maxInitialCode, code);
                uriMap.put(scrutariDataURI, code);
                usedCodeSet.add(code);
            }
        }

    }

}
