/* ScrutariLib_DB - Copyright (c) 2013-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.codes;

import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.logging.MessageHandler;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.URIParseException;
import net.scrutari.datauri.URIParser;
import net.scrutari.db.api.DataAccess;


/**
 *
 * @author Vincent Calame
 */
public final class CodeParser {

    private CodeParser() {
    }

    public static Integer toCode(DataAccess dataAccess, String s, MessageHandler messageHandler, short uriType) {
        int slashIndex = s.indexOf('/');
        if (slashIndex == -1) {
            return parseWithoutSlash(dataAccess, s, messageHandler, uriType);
        }
        if (slashIndex == 0) {
            return parseURI(dataAccess, ScrutariDataURI.typeToSchemeString(uriType) + ":" + s, messageHandler, uriType, s);
        }
        int colonIndex = s.indexOf(':');
        if ((colonIndex > 0) && (colonIndex < slashIndex)) {
            return parseURI(dataAccess, s, messageHandler, uriType, s);
        }
        if (uriType == ScrutariDataURI.BASEURI_TYPE) {
            addError(messageHandler, "_ error.code.malformed.uri", s);
            return null;
        }
        String firstPart = s.substring(0, slashIndex);
        String secondPart = s.substring(slashIndex);
        ScrutariDataURI startScrutariDataURI;
        try {
            Integer firstCode = Integer.parseInt(firstPart);
            startScrutariDataURI = getParentScrutariDataURI(dataAccess, firstCode);
            if (startScrutariDataURI == null) {
                addError(messageHandler, "_ error.code.unknown.uri", firstPart + " (" + s + ")");
                return null;
            }
        } catch (NumberFormatException nfe) {
            BaseData baseData = dataAccess.getBaseDataByScrutariSourceName(firstPart);
            if (baseData != null) {
                startScrutariDataURI = baseData.getBaseURI();
            } else {
                addError(messageHandler, "_ error.code.malformed.notinteger", firstPart + " (" + s + ")");
                return null;
            }
        }
        String uriString = ScrutariDataURI.typeToSchemeString(uriType) + ':' + startScrutariDataURI.getPath() + secondPart;
        return parseURI(dataAccess, uriString, messageHandler, uriType, secondPart + " (" + s + ")");
    }

    private static Integer parseWithoutSlash(DataAccess dataAccess, String s, MessageHandler messageHandler, short uriType) {
        try {
            Integer code = Integer.parseInt(s);
            if (!testCode(dataAccess, code, uriType)) {
                addError(messageHandler, "_ error.code.unknown.code", s);
                return null;
            }
            return code;
        } catch (NumberFormatException nfe) {
            if (uriType == ScrutariDataURI.BASEURI_TYPE) {
                BaseData baseData = dataAccess.getBaseDataByScrutariSourceName(s);
                if (baseData != null) {
                    return baseData.getBaseCode();
                }
            }
            addError(messageHandler, "_ error.code.malformed.notinteger", s);
            return null;
        }
    }

    private static Integer parseURI(DataAccess dataAccess, String uriString, MessageHandler messageHandler, short uriType, String originalString) {
        try {
            ScrutariDataURI scrutariDataURI = URIParser.parse(uriString);
            if (scrutariDataURI.getType() != uriType) {
                addError(messageHandler, "_ error.code.wrong.uritype", originalString);
                return null;
            }
            Integer code = dataAccess.getCode(scrutariDataURI);
            if (code == null) {
                addError(messageHandler, "_ error.code.unknown.uri", originalString);
            }
            return code;
        } catch (URIParseException uriParseException) {
            addError(messageHandler, "_ error.code.malformed.uri", uriString);
            return null;
        }
    }

    private static void addError(MessageHandler messageHandler, String messageKey, Object messageValue) {
        messageHandler.addMessage("severe.code", LocalisationUtils.toMessage(messageKey, messageValue));
    }

    private static boolean testCode(DataAccess dataAccess, Integer code, short uriType) {
        switch (uriType) {
            case ScrutariDataURI.BASEURI_TYPE:
                return (dataAccess.getBaseData(code) != null);
            case ScrutariDataURI.CORPUSURI_TYPE:
                return (dataAccess.getCorpusData(code) != null);
            case ScrutariDataURI.THESAURUSURI_TYPE:
                return (dataAccess.getThesaurusData(code) != null);
            case ScrutariDataURI.FICHEURI_TYPE:
                return (dataAccess.getFicheInfo(code) != null);
            case ScrutariDataURI.MOTCLEURI_TYPE:
                return (dataAccess.getMotcleInfo(code) != null);
            default:
                throw new SwitchException("uriType = " + uriType);
        }

    }

    private static ScrutariDataURI getParentScrutariDataURI(DataAccess dataAccess, Integer code) {
        BaseData baseData = dataAccess.getBaseData(code);
        if (baseData != null) {
            return baseData.getBaseURI();
        }
        CorpusData corpusData = dataAccess.getCorpusData(code);
        if (corpusData != null) {
            return corpusData.getCorpusURI();
        }
        ThesaurusData thesaurusData = dataAccess.getThesaurusData(code);
        if (thesaurusData != null) {
            return thesaurusData.getThesaurusURI();
        }
        return null;
    }

}
