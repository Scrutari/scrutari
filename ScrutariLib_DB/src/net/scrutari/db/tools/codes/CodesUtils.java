/* ScrutariLib_DB - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.tools.codes;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.RandomAccess;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.ScrutariDataURI;


/**
 *
 * @author Vincent Calame
 */
public final class CodesUtils {

    private CodesUtils() {

    }

    public static Collection<Integer> shuffle(Collection<Integer> codes, int finalSize) {
        int length = codes.size();
        if (length < 2) {
            return codes;
        }
        List<Integer> list = new ArrayList<Integer>();
        list.addAll(codes);
        Collections.shuffle(list);
        if ((finalSize == -1) || (length <= finalSize)) {
            return list;
        } else {
            return list.subList(0, finalSize);
        }
    }

    public static List<BaseURI> wrap(BaseURI[] array) {
        return new BaseURIList(array);
    }

    public static List<CorpusURI> wrap(CorpusURI[] array) {
        return new CorpusURIList(array);
    }

    public static List<ScrutariDataURI> wrap(ScrutariDataURI[] array) {
        return new ScrutariDataURIList(array);
    }


    private static class BaseURIList extends AbstractList<BaseURI> implements RandomAccess {

        private final BaseURI[] array;

        private BaseURIList(BaseURI[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public BaseURI get(int index) {
            return array[index];
        }

    }


    private static class ScrutariDataURIList extends AbstractList<ScrutariDataURI> implements RandomAccess {

        private final ScrutariDataURI[] array;

        private ScrutariDataURIList(ScrutariDataURI[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public ScrutariDataURI get(int index) {
            return array[index];
        }

    }


    private static class CorpusURIList extends AbstractList<CorpusURI> implements RandomAccess {

        private final CorpusURI[] array;

        private CorpusURIList(CorpusURI[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public CorpusURI get(int index) {
            return array[index];
        }

    }

}
