/* ScrutariLib_DB - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.util.List;
import java.util.Set;
import net.mapeadores.util.attr.AttributeKey;
import net.scrutari.lexie.AlineaRank;
import net.scrutari.lexie.FieldRank;


/**
 *
 * @author Vincent Calame
 */
public interface FieldRankManager {

    public List<String> getOrderList();

    public int getComplementOrder();

    public FieldRank getSpecialFieldRank(String name);

    public FieldRank getAttributeFieldRank(AttributeKey attributeKey);

    public Set<AttributeKey> getAttributeKeySet();

    public default FieldRank getComplementFieldRank(int complementNumber) {
        return FieldRank.build(getComplementOrder(), complementNumber, FieldRank.COMPLEMENT_TYPE);
    }

    public default AlineaRank getSpecialAlineaRank(String name) {
        return AlineaRank.build(getSpecialFieldRank(name), 1);
    }

    public default AlineaRank getComplementAlineaRank(int complementNumber) {
        return AlineaRank.build(getComplementFieldRank(complementNumber), 1);
    }

    public default int getOrderSize() {
        return getOrderList().size();
    }

}
