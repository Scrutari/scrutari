/* ScrutariLib_DB - Copyright (c) 2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import net.scrutari.datauri.ScrutariDataURI;


/**
 *
 * @author Vincent Calame
 */
public class CodeMatch {

    private final int code;
    private final ScrutariDataURI scrutariDataURI;

    public CodeMatch(int code, ScrutariDataURI scrutariDataURI) {
        this.code = code;
        this.scrutariDataURI = scrutariDataURI;
    }

    public int getCode() {
        return code;
    }

    public ScrutariDataURI getScrutariDataURI() {
        return scrutariDataURI;
    }

}
