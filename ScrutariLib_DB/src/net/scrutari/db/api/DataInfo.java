/* ScrutariLib_DB - Copyright (c) 2020-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.util.List;
import java.util.SortedSet;
import net.mapeadores.util.attr.AttributeKey;
import net.scrutari.data.RelationData;


/**
 *
 * @author Vincent Calame
 */
public interface DataInfo {

    public Integer getCode();

    public ScrutariDBName getFirstAdd();

    public boolean hasRelAttribute();

    public List<Integer> getRelAttributeCodeList(String relName);

    public int getIndexationCount();

    public List<Integer> getIndexationCodeList(Integer subsetCode);

    public boolean hasRelation();

    public SortedSet<RoleKey> getAvalaibleRoleKeySet();

    public List<RelationData> getRelationList(RoleKey roleKey);

    public default List<RelationData> getRelationList(String type, String role) {
        return getRelationList(RoleKey.build(type, role));
    }

    public default List<Integer> getRelAttributeCodeList(AttributeKey attributeKey) {
        return getRelAttributeCodeList(attributeKey.toString());
    }

    public default boolean hasIndexation() {
        return (getIndexationCount() != 0);
    }

}
