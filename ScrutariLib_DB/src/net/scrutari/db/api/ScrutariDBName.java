/* ScrutariLib_DB - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.SimpleTimeZone;


/**
 *
 * @author Vincent Calame
 */
public final class ScrutariDBName implements Comparable<ScrutariDBName> {

    private final static DateFormat ISO_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    private final static DateFormat NAME_FORMAT = new SimpleDateFormat("yyyy-MMdd-HHmm");
    private final static DateFormat DAY_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private final static Map<String, ScrutariDBName> internMap = new HashMap<String, ScrutariDBName>();

    static {
        ISO_FORMAT.setTimeZone(new SimpleTimeZone(0, "GMT"));
        NAME_FORMAT.setTimeZone(new SimpleTimeZone(0, "GMT"));
        DAY_FORMAT.setTimeZone(new SimpleTimeZone(0, "GMT"));
    }

    private final String name;
    private final Date date;

    private ScrutariDBName(String name, Date date) {
        this.name = name;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getYear() {
        return name.substring(0, 4);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        ScrutariDBName otherScrutariDBName = (ScrutariDBName) other;
        return this.name.equals(otherScrutariDBName.name);
    }

    @Override
    public int compareTo(ScrutariDBName otherScrutariDBName) {
        return this.name.compareTo(otherScrutariDBName.name);
    }

    public String toGmtIsoFormat() {
        synchronized (ISO_FORMAT) {
            return ISO_FORMAT.format(date);
        }
    }

    public String toDayFormat() {
        synchronized (DAY_FORMAT) {
            return DAY_FORMAT.format(date);
        }
    }

    public static boolean isCurrentName(ScrutariDBName scrutariDBName) {
        String current = NAME_FORMAT.format(new Date());
        return scrutariDBName.name.equals(current);
    }

    public synchronized static ScrutariDBName newName() {
        Date current = new Date();
        return newName(current);
    }

    public synchronized static ScrutariDBName newName(Date date) {
        String name = NAME_FORMAT.format(date);
        ScrutariDBName current = internMap.get(name);
        if (current != null) {
            return current;
        } else {
            current = new ScrutariDBName(name, date);
            internMap.put(name, current);
            return current;
        }
    }

    public synchronized static ScrutariDBName parse(String name) throws ParseException {
        int length = name.length();
        if (length > 14) {
            name = name.substring(0, 4) + "-" + name.substring(4, 8) + "-" + name.substring(9, 13);
        }
        ScrutariDBName current = internMap.get(name);
        if (current != null) {
            return current;
        } else {
            Date date = NAME_FORMAT.parse(name);
            current = new ScrutariDBName(name, date);
            internMap.put(name, current);
            return current;
        }
    }

}
