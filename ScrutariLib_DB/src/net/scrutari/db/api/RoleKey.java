/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author Vincent Calame
 */
public final class RoleKey implements Comparable<RoleKey> {

    private final static Map<String, Map<String, RoleKey>> internMap = new HashMap<String, Map<String, RoleKey>>();
    private final String type;
    private final String role;

    private RoleKey(String type, String role) {
        this.type = type;
        this.role = role;
        intern(this);
    }

    public String getType() {
        return type;
    }

    public String getRole() {
        return role;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + type.hashCode();
        result = prime * result + role.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other.getClass() != this.getClass()) {
            return false;
        }
        RoleKey otherKey = (RoleKey) other;
        if (!otherKey.type.equals(this.type)) {
            return false;
        }
        if (!otherKey.role.equals(this.role)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return type + "|" + role;
    }

    @Override
    public int compareTo(RoleKey otherKey) {
        if (otherKey == this) {
            return 0;
        }
        int comp = this.type.compareTo(otherKey.type);
        if (comp != 0) {
            return comp;
        } else {
            return this.role.compareTo(otherKey.role);
        }
    }

    public static RoleKey build(String type, String role) {
        if (type == null) {
            throw new IllegalArgumentException("type is null");
        }
        if (role == null) {
            throw new IllegalArgumentException("role is null");
        }
        RoleKey existingKey = getExistingKey(type, role);
        if (existingKey != null) {
            return existingKey;
        }
        return new RoleKey(type, role);
    }

    private static RoleKey getExistingKey(String type, String role) {
        Map<String, RoleKey> roleMap = internMap.get(type);
        if (roleMap == null) {
            return null;
        } else {
            return roleMap.get(role);
        }
    }

    private synchronized static void intern(RoleKey roleKey) {
        Map<String, RoleKey> roleMap = internMap.get(roleKey.type);
        if (roleMap == null) {
            roleMap = new HashMap<String, RoleKey>();
            internMap.put(roleKey.type, roleMap);
        }
        roleMap.put(roleKey.role, roleKey);
    }

}
