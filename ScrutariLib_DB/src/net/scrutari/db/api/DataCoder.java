/* ScrutariLib_DB - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import net.scrutari.datauri.ScrutariDataURI;


/**
 *
 * @author Vincent Calame
 */
public interface DataCoder {

    public Integer getCode(ScrutariDataURI scrutariDataURI, boolean createIfNotExist);

    public default boolean containsCode(ScrutariDataURI scrutariDataURI) {
        return (getCode(scrutariDataURI, false) != null);
    }

}
