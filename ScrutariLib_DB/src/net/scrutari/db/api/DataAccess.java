/* ScrutariLib_DB - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.util.List;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ScrutariDataURIChecker;


/**
 *
 * @author Vincent Calame
 */
public interface DataAccess extends AutoCloseable {

    public List<BaseData> getBaseDataList();

    public List<CorpusData> getCorpusDataList();

    public List<ThesaurusData> getThesaurusDataList();

    public Integer getCode(ScrutariDataURI scrutariDataURI);

    public ScrutariDataURIChecker getScrutariDataURIChecker();

    /**
     * La nature de l'objet dépend de la classe de scrutariDataURI BaseURi =
     * BaseData, CorpusURI = CorpusData, ThesaurusURI = thesaurusData, FicheURI
     * = ficheData, MotcleURI = MotcleData
     */
    public Object getScrutariData(ScrutariDataURI scrutariDataURI);

    public DataInfo getDataInfo(Integer code);

    public BaseData getBaseData(Integer baseCode);

    public BaseData getBaseDataByScrutariSourceName(String scrutariSourceName);

    public CorpusData getCorpusData(Integer corpusCode);

    public FicheData getFicheData(Integer ficheCode);

    public ThesaurusData getThesaurusData(Integer thesaurusCode);

    public MotcleData getMotcleData(Integer motcleCode);

    public FuzzyDate getFicheDate(Integer ficheCode);

    public boolean containsRelAttributeFor(String relName);

    public boolean hasIndexation(Integer corpusCode, Integer thesaurusCode);

    @Override
    public void close();

    public default FicheInfo getFicheInfo(Integer ficheCode) {
        try {
            return (FicheInfo) getDataInfo(ficheCode);
        } catch (ClassCastException cce) {
            return null;
        }
    }

    public default MotcleInfo getMotcleInfo(Integer motcleCode) {
        try {
            return (MotcleInfo) getDataInfo(motcleCode);
        } catch (ClassCastException cce) {
            return null;
        }
    }

    public default boolean isMotcle(Integer code) {
        DataInfo dataInfo = getDataInfo(code);
        if (dataInfo == null) {
            return false;
        }
        return (dataInfo instanceof MotcleInfo);
    }

    public default boolean isFiche(Integer code) {
        DataInfo dataInfo = getDataInfo(code);
        if (dataInfo == null) {
            return false;
        }
        return (dataInfo instanceof FicheInfo);
    }

    public default Lang getFicheLang(Integer ficheCode) {
        FicheInfo ficheInfo = getFicheInfo(ficheCode);
        if (ficheInfo == null) {
            return FicheData.UNDETERMINED_LANG;
        } else {
            return ficheInfo.getLang();
        }
    }

    public default BaseData[] getBaseDataArray() {
        List<BaseData> baseDataList = getBaseDataList();
        return baseDataList.toArray(new BaseData[baseDataList.size()]);
    }

    public default CorpusData[] getCorpusDataArray() {
        List<CorpusData> corpusDataList = getCorpusDataList();
        return corpusDataList.toArray(new CorpusData[corpusDataList.size()]);
    }

    public default ThesaurusData[] getThesaurusDataArray() {
        List<ThesaurusData> thesaurusDataList = getThesaurusDataList();
        return thesaurusDataList.toArray(new ThesaurusData[thesaurusDataList.size()]);
    }

    public default Integer[] getBaseCodeArray() {
        List<BaseData> baseDataList = getBaseDataList();
        int size = baseDataList.size();
        Integer[] baseCodeArray = new Integer[size];
        for (int i = 0; i < size; i++) {
            baseCodeArray[i] = baseDataList.get(i).getBaseCode();
        }
        return baseCodeArray;
    }

    public default Integer[] getCorpusCodeArray() {
        List<CorpusData> corpusDataList = getCorpusDataList();
        int size = corpusDataList.size();
        Integer[] corpusCodeArray = new Integer[size];
        for (int i = 0; i < size; i++) {
            corpusCodeArray[i] = corpusDataList.get(i).getCorpusCode();
        }
        return corpusCodeArray;
    }

    public default Integer[] getThesaurusCodeArray() {
        List<ThesaurusData> thesaurusDataList = getThesaurusDataList();
        int size = thesaurusDataList.size();
        Integer[] thesaurusCodeArray = new Integer[size];
        for (int i = 0; i < size; i++) {
            thesaurusCodeArray[i] = thesaurusDataList.get(i).getThesaurusCode();
        }
        return thesaurusCodeArray;
    }

    public default boolean containsRelAttributeFor(AttributeKey attributeKey) {
        return containsRelAttributeFor(attributeKey.toString());
    }

}
