/* ScrutariLib_DB - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariConstants {

    public final static String PRIMARY_GROUP = "primary";
    public final static String SECONDARY_GROUP = "secondary";
    public final static String TECHNICAL_GROUP = "technical";

    public static boolean isValidGroupName(String name) {
        switch (name) {
            case PRIMARY_GROUP:
            case SECONDARY_GROUP:
            case TECHNICAL_GROUP:
                return true;
            default:
                return false;

        }
    }

}
