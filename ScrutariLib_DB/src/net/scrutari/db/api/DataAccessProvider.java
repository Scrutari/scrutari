/* ScrutariLib_DB  - Copyright (c) 2012-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;


/**
 *
 * @author Vincent Calame
 */
public interface DataAccessProvider {

    public DataAccess openDataAccess();

}
