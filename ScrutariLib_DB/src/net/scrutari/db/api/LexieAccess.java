/* ScrutariLib_DB - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.collation.map.CollatedKeyMap;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public interface LexieAccess extends AutoCloseable {

    public int getLexieCount();

    public List<Lang> getLangList();

    public ScrutariLexieUnit getScrutariLexieUnit(LexieId lexieId);

    public CollatedKeyMap<ScrutariLexieUnit> getLexieUnitMapByLang(Lang lang);

    public void checkLexieOccurrences(LexieId lexieId, Integer subsetCode, IntPredicate codePredicate, Consumer<LexieOccurrences> occurrencesConsumer);

    public List<LexieOccurrences> getLexieOccurrencesList(LexieId lexieId, Integer subsetCode);

    public void populateFicheLexification(Integer ficheCode, FicheLexification ficheLexification);

    public void populateMotcleLexification(Integer motcleCode, MotcleLexification motcleLexification);

    @Override
    public void close();

}
