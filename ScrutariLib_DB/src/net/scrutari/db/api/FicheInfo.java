/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import net.mapeadores.util.localisation.Lang;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.datauri.FicheURI;


/**
 *
 * @author Vincent Calame
 */
public interface FicheInfo extends DataInfo {

    public FicheURI getFicheURI();

    public CorpusData getCorpusData();

    public Lang getLang();

    public default Integer getCorpusCode() {
        return getCorpusData().getCorpusCode();
    }

    public default FicheData getFicheData(DataAccess dataAccess) {
        return dataAccess.getFicheData(getCode());
    }

}
