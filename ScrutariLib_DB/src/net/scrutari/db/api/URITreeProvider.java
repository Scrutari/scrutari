/* ScrutariLib_DB - Copyright (c) 2013-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.tree.URITree;


/**
 *
 * @author Vincent Calame
 */
public interface URITreeProvider {

    public int getAddURITreeDateCount();

    public ScrutariDBName getScrutariDBName(int i);

    public URITree getAddURITree(int i, ScrutariDataURIChecker checker);

}
