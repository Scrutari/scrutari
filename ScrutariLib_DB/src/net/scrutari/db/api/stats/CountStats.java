/* ScrutariLib_DB - Copyright (c) 2005-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.stats;


/**
 *
 * @author Vincent Calame
 */
public interface CountStats {

    public int getCorpusCount();

    public int getThesaurusCount();

    public int getMotcleCount();

    public int getFicheCount();

    public int getBaseCount();

    public int getLexieCount();

    public int getIndexationCount();

}
