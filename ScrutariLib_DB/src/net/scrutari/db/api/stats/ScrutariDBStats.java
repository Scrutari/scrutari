/* ScrutariLib_DB - Copyright (c) 2014-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.stats;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariDBStats {

    public CountStats getEngineCountStats();

    public CountStats getCountStats(Integer code);

    public LangStats getFicheEngineLangStats();

    public LangStats getMotcleEngineLangStats();

    /**
     * Le code est celui d'une base, d'un corpus ou d'un thésaurus.
     */
    public LangStats getLangStats(Integer code);

}
