/* ScrutariLib_DB - Copyright (c) 2014-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.stats;

import java.util.List;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface LangStats extends List<LangStat>, RandomAccess {


    public default Lang[] getLangArray() {
        int size = size();
        Lang[] result = new Lang[size];
        for (int i = 0; i < size; i++) {
            result[i] = get(i).getLang();
        }
        return result;
    }

}
