/* ScrutariLib_DB - Copyright (c) 2005-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import net.scrutari.db.api.stats.ScrutariDBStats;


/**
 * Interface regroupant les différents élément d'une base.
 *
 * @author Vincent Calame
 */
public interface ScrutariDB extends DataAccessProvider, LexieAccessProvider {

    public ScrutariDBName getScrutariDBName();

    public FieldRankManager getFieldRankManager();

    public ScrutariDBStats getStats();

}
