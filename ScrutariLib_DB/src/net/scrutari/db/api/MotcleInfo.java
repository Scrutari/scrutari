/* ScrutariLib_DB - Copyright (c) 2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.MotcleURI;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleInfo extends DataInfo {

    public MotcleURI getMotcleURI();

    public ThesaurusData getThesaurusData();

    public default Integer getThesaurusCode() {
        return getThesaurusData().getThesaurusCode();
    }

    public default MotcleData getMotcleData(DataAccess dataAccess) {
        return dataAccess.getMotcleData(getCode());
    }

}
