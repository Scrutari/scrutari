/* ScrutariLib_DB - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.caches;

import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;


/**
 *
 * @author Vincent Calame
 */
public interface DataCacheReader extends AutoCloseable {

    public FicheData getFicheData(Integer ficheCode);

    public MotcleData getMotcleData(Integer motcleCode);

    public Lang getFicheLang(Integer ficheCode);

    public FuzzyDate getFicheDate(Integer ficheCode);

    @Override
    public void close();

}
