/* ScrutariLib_DB - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.caches;

import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.FicheData;
import net.scrutari.data.IndexationData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.RelationData;
import net.scrutari.data.ThesaurusData;


/**
 *
 * @author Vincent Calame
 */
public interface DataCacheWriter {

    public void cacheBaseData(BaseData baseData);

    public void cacheCorpusData(CorpusData corpusData);

    public void cacheThesaurusData(ThesaurusData thesaurusData);

    public void cacheFicheData(FicheData ficheData);

    public void cacheMotcleData(MotcleData motcleData);

    public void cacheIndexationData(IndexationData indexationData);

    public void cacheRelationData(RelationData relationData);

    public DataCacheReaderFactory endCacheWrite();

}
