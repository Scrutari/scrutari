/* ScrutariLib_DB - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.caches;

import java.util.List;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public interface LexieCacheWriter {

    public void cacheLexieOccurrences(LexieId lexieId, Integer subsetCode, LexieOccurrences lexieOccurencesList);

    public void cacheFicheLexification(Integer ficheCode, FicheLexification ficheLexification);

    public void cacheMotcleLexification(Integer motcleCode, MotcleLexification motcleLexification);

    public LexieCacheReaderFactory endCacheWrite(List<ScrutariLexieUnit> scrutariLexieUnitList);

}
