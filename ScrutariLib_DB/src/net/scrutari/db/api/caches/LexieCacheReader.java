/* ScrutariLib_DB - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api.caches;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import net.scrutari.lexie.FicheLexification;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.LexieOccurrences;
import net.scrutari.lexie.MotcleLexification;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public interface LexieCacheReader extends AutoCloseable {

    public List<ScrutariLexieUnit> getScrutariLexieUnitList();

    public void checkLexieOccurrences(LexieId lexieId, Integer subsetCode, IntPredicate codePredicate, Consumer<LexieOccurrences> occurrencesConsumer);

    public List<LexieOccurrences> getLexieOccurrencesList(LexieId lexieId, Integer subsetCode);

    public void readFicheLexification(Integer ficheCode, FicheLexification ficheLexification);

    public void readMotcleLexification(Integer motcleCode, MotcleLexification motcleLexification);

    @Override
    public void close();

}
