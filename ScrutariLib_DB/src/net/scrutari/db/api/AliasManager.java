/* ScrutariLib_DB - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.db.api;

import java.util.Set;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.ScrutariDataURIChecker;


/**
 *
 * @author Vincent Calame
 */
public interface AliasManager extends ScrutariDataURIChecker {

    public BaseURI getMainBaseURI(String sourceName);

    public String getSourceName(BaseURI baseURI);

    public Set<String> getSourceNameSet();

    public Set<BaseURI> getBaseURISet();

}
