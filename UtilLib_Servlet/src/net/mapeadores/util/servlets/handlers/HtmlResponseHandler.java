/* UtilLib_Servlet - Copyright (c) 2010-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets.handlers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HtmlProducer;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public class HtmlResponseHandler implements ResponseHandler {

    private final HtmlProducer htmlProducer;
    private long lastModified = ResponseHandler.LASTMODIFIED_NOCACHE;

    public HtmlResponseHandler(HtmlProducer htmlProducer) {
        this.htmlProducer = htmlProducer;
    }

    @Override
    public long getLastModified() {
        return lastModified;
    }

    public HtmlResponseHandler lastModified(long lastModified) {
        this.lastModified = lastModified;
        return this;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.HTML + ";charset=UTF-8");
        try (PrintWriter pw = response.getWriter()) {
            htmlProducer.writeHtml(pw);
        }
    }

    public HtmlResponseHandler init(HtmlProducer htmlProducer) {
        return new HtmlResponseHandler(htmlProducer);
    }

}
