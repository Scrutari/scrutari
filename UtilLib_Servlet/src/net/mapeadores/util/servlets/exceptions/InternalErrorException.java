/* UtilLib_Servlet - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.mapeadores.util.servlets.exceptions;

import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.servlets.HttpAccessException;


/**
 *
 * @author Vincent Calame
 */
public class InternalErrorException extends HttpAccessException {

    private final static int CODE = HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

    public InternalErrorException() {
        super(CODE);
    }

    public InternalErrorException(CommandMessage message) {
        super(CODE, message);
    }

    public InternalErrorException(String messageKey, Object... messageValues) {
        super(CODE, LogUtils.error(messageKey, messageValues));
    }

}
