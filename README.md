# Logiciel Scrutari

Ce dépôt contient le code source du logiciel Scrutari, moteur de recherche sur des méta-données. Scrutari est écrit en Java.


## Prérequis

Scrutari tourne avec le serveur d'application Tomcat (version 7 ou supérieure),  lui-même écrit en Java. La compilation de Scrutari se fait via un script Ant. Java, Tomcat et Ant sont des logiciels très utilisés qui devraient s'installer facilement avec Votre distribution GNU-Linux préférée. Tomcat s'installe également sans trop de difficultés à partir de la version compilée proposée sur son site : http://tomcat.apache.org

Scrutari n'a pas d'autres dépendances. En particulier, le logiciel ne nécessite pas de bases de données externes car il stocke les données qu'il traite sous forme de fichiers.

Pour que Scrutari fonctionne, il faut lui indiquer deux répertoires distincts :
- le répertoire de configuration (appelé « confDir ») qui doit être accessible en lecture, ce répertoire est important car toute la configuration se fait « manuellement » avec des fichiers contenus dans ce répertoire. Typiquement, ce répertoire pourrait être /etc/scrutari/
- le répertoire des données (appelé « varDir ») qui doit être accessible en lecture et écriture à Scrutari (autrement dit à l'utilisateur avec lequel tourne Tomcat), par exemple : /var/lib/scrutari ou /var/lib/tomcat/scrutari

L'indication de ces deux répertoires se fait dans le fichier context.xml sur lequel se fonde Tomcat pour le déploiement de l'application. Ce fichier ressemble au contenu ci-dessous, "confDir" et "varDir" sont les paramètres respectivement du répertoire de configuration et du répertoire des données : 


    <?xml version="1.0" encoding="UTF-8"?>
    <Context>
        <Parameter name="confDir" value="/path/to/scrutari/conf/"/>
        <Parameter name="varDir" value="/path/to/scrutari/var/"/>
    </Context>

Ce fichier est placé dans un des emplacements suivants :

    $CATALINA_BASE/conf/[enginename]/[hostname]/[webappname].xml
    $CATALINA_BASE/webapps/[webappname]/META-INF/context.xml
 
Voir la documentation de Tomcat pour plus de détails :

- Tomcat Web Application Deployment :https://tomcat.apache.org/tomcat-7.0-doc/deployer-howto.html
- Deployment : https://tomcat.apache.org/tomcat-7.0-doc/deployer-howto.html

Un moyen rapide proposé par Tomcat pour déployer une application consiste à créer un fichier .war contenant l'ensemble (code compilé de Scrutari et configuration de context.xml). Le script Ant de compilation (voir le fichier ant/Scrutari/build.xml) offre la possibilité de créer ce fichier .war. La démarche est expliquée dans le chapitre suivant.


## Exemple de compilation et d'installation sous Ubuntu

L'exemple ci-dessous donne un exemple d'installation complète d'un moteur Scrutari fonctionnel. La configuration du moteur sera celle contenu dans examples/context/conf qui propose des extraits de configuration de moteurs existants (par exemple, celui de la Coredem).

1) Installez le paquet de Tomcat (ici la version 8), Java est une dépendance de Tomcat, il devrait s'installer également si ce n'est pas déjà fait

    sudo apt-get install tomcat8

Dans votre navigateur, ouvrez l'adresse http://localhost:8080/, un message devrait s'afficher indiquant que Tomcat fonctionne. S'il ne fonctionne pas, lancer le service :

    sudo systemctl start tomcat8

Pour le stopper, la commande est :

    sudo systemctl stop tomcat8

 
2) Installez Ant si ce n'est déjà fait

    sudo apt-get install ant


3) Dans le répertoire de votre choix, clonez le dépôt

    git clone https://vcalame@framagit.org/Scrutari/scrutari.git

    
4) Rendez-vous dans le répertoire scrutari/ et lancer le script de compilation (pour que les chemins de l'exemple fonctionne, il doit être impérativement lancé à partir du répertoire qui le contient)

    cd scrutari
    ./ant-scrutari.sh
   
Quelques messages s'affichent pendant la compilation, l'avant-dernier devant être : BUILD SUCCESSFUL


5) Sous Ubuntu, Tomcat fonctionne avec l'utilisateur tomcat8. Comme indiqué dans les prérequis, celui doit avoir accès en écriture au répertoire de données. Par défaut dans notre exemple, celui-ci est dans examples/context/var est 

    sudo chown tomcat8:tomcat8 examples/context/var
    
Cette solution est peu satisfaisante sur le long terme, nous verrons plus loin comment le modifier mais pour l'instant cela nous suffit.


6) Déplacez le fichier .war créé par la compilation qui se trouve dans target/dist vers le répertoire webapps/ de Tomcat (/var/lib/tomcat8/webapps/ dans notre cas)

    sudo mv target/dist/scrutari.war /var/lib/tomcat8/webapps/
    

7) Ouvrez l'adresse http://localhost:8080/scrutari/, Tomcat devrait se déployer automatiquement (sinon arrêtez et relancez Tomcat)


8) La page http://localhost:8080/scrutari/ indique que Scrutari est installé et liste les cinq moteurs proposés dans l'exemple de configuration. En cliquant sur un, vous pouvez accéder à l'interface de visualisation de l'administration. Le premier chargement prend du temps, ce qui est normal, Scrutari télécharge les fichiers des données et les indexe.


## Configuration du script de compilation ant-scrutari.sh

Dans notre exemple, le script ant-scrutari.sh doit être lancé à partir de son répertoire pour que le fichier .war soit opérationnel. Ceci est nullement obligatoire et le script peut être placé n'importe où au prix de quelques changements dans les paramètres.

Le contenu du fichier est le suivant :

    #!/bin/bash
    Sources="$PWD"
    Target="$PWD/target"
    Version="local"
    Repository="local"
    WebappName="scrutari"
    ConfDir="$PWD/examples/context/conf/"
    VarDir="$PWD/examples/context/var/"
    AntTargets="webinf war"
    
    ant -Ddeploy.path=$Target -Dproject.version="$Version"  -Dproject.repository="$Repository" -Dproject.webapp_name="$WebappName" -Dproject.conf_dir="$ConfDir" -Dproject.var_dir="$VarDir" -f $Sources/ant/Scrutari/build.xml $AntTargets

$Sources est le chemin des source, $Target est le chemin de destination de la compilation.

$ConfDir et $VarDir sont les deux paramètres à modifier pour pointer vers des répertoires plus appropriés pour la configuration et les données de Scrutari

$Version et $Repository servent à indiquer la version et l'origine du code.

$WebappName est le nom du fichier .war, en l'appelant « scrutari2 » par exemple, le moteur sera accessible via http://localhost:8080/scrutari2

$AntTargets indique les actions à effectuer par Ant. "webinf" fait la compilation sans fichier .war, "webinf war" fait le fichier .war et "webinf zip" est plus complète que "webinf war" car elle crèe un fichier zip des sources et du binaire et indique un numéro de version pour le .war

## Aller plus loin

- http://www.scrutari.net : la documentation technique
- http://www.scrutari.net/dokuwiki/serveurscrutari:config : plus de détails sur la configuration




