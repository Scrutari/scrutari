/* ScrutariLib_Supermotcle - Copyright (c) 2013-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.supermotcle.tools;

import java.util.HashMap;
import java.util.Map;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.supermotcle.api.SupermotcleHolder;
import net.scrutari.supermotcle.api.SupermotcleProvider;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleProviderBuilder {

    private final Map<Lang, SupermotcleHolderBuilder> builderMap = new HashMap<Lang, SupermotcleHolderBuilder>();
    private final Map<Lang, SupermotcleHolder> supermotcleHolderMap = new HashMap<Lang, SupermotcleHolder>();

    private SupermotcleProviderBuilder() {
    }

    private SupermotcleProvider toSupermotcleProvider() {
        return new InternalSupermotcleProvider(supermotcleHolderMap);
    }

    private void build(DataAccess dataAccess) {
        for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
            for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
                MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                initMotcle(dataAccess, thesaurusData, motcleData);
            }
        }
        for (Map.Entry<Lang, SupermotcleHolderBuilder> entry : builderMap.entrySet()) {
            Lang lang = entry.getKey();
            SupermotcleHolderBuilder builder = entry.getValue();
            SupermotcleHolder supermotcleHolder = builder.toSupermotcleMap();
            supermotcleHolderMap.put(lang, supermotcleHolder);
        }
    }

    private void initMotcle(DataAccess dataAccess, ThesaurusData thesaurusData, MotcleData motcleData) {
        Integer motcleCode = motcleData.getMotcleCode();
        for (Label label : motcleData.getLabels()) {
            SupermotcleHolderBuilder supermotcleHolderBuilder = getOrCreateSupermotcleHolderBuilder(dataAccess, label.getLang());
            supermotcleHolderBuilder.addMotcle(thesaurusData.getBaseCode(), thesaurusData.getThesaurusCode(), motcleCode, label.getLabelString());
        }
    }

    private SupermotcleHolderBuilder getOrCreateSupermotcleHolderBuilder(DataAccess dataAccess, Lang lang) {
        SupermotcleHolderBuilder supermotcleHolderBuilder = builderMap.get(lang);
        if (supermotcleHolderBuilder == null) {
            supermotcleHolderBuilder = new SupermotcleHolderBuilder(dataAccess, lang);
            builderMap.put(lang, supermotcleHolderBuilder);
        }
        return supermotcleHolderBuilder;
    }

    public static SupermotcleProvider build(ScrutariDB scrutariDB) {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            SupermotcleProviderBuilder builder = new SupermotcleProviderBuilder();
            builder.build(dataAccess);
            return builder.toSupermotcleProvider();
        }
    }


    private static class InternalSupermotcleProvider implements SupermotcleProvider {

        private final Map<Lang, SupermotcleHolder> supermotcleHolderMap;

        private InternalSupermotcleProvider(Map<Lang, SupermotcleHolder> supermotcleHolderMap) {
            this.supermotcleHolderMap = supermotcleHolderMap;
        }

        @Override
        public SupermotcleHolder getSupermotcleHolder(Lang lang) {
            return supermotcleHolderMap.get(lang);
        }

    }

}
