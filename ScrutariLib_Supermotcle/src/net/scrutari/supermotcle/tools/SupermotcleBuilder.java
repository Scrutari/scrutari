/* ScrutariLib_Supermotcle - Copyright (c) 2010-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.supermotcle.tools;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.RandomAccess;
import java.util.Set;
import java.util.TreeSet;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.text.collation.CollationUnit;
import net.scrutari.data.BaseData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.supermotcle.api.Supermotcle;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleBuilder {

    private final static IndexationCounterComparator INDEXATION_COMPARATOR = new IndexationCounterComparator();
    private final Lang lang;
    private final Integer code;
    private final List<Integer> motcleCodeList = new ArrayList<Integer>();
    private final DataAccess dataAccess;
    private final List<IndexationCounter> indexationCounterList = new ArrayList<IndexationCounter>();
    private CollationUnit collationUnit;

    public SupermotcleBuilder(DataAccess dataAccess, Lang lang, Integer code, Integer thesaurusCode, Integer motcleCode, CollationUnit collationUnit) {
        this.dataAccess = dataAccess;
        this.lang = lang;
        this.code = code;
        this.collationUnit = collationUnit;
        motcleCodeList.add(motcleCode);
        for (BaseData baseData : dataAccess.getBaseDataList()) {
            Integer baseCode = baseData.getBaseCode();
            Integer[] corpusCodeArray = PrimUtils.toArray(baseData.getCorpusCodeList());
            IndexationCounter indexationCounter = new IndexationCounter(baseCode, corpusCodeArray);
            indexationCounterList.add(indexationCounter);
        }
        countIndexation(motcleCode);
    }

    public void addMotcle(Integer thesaurusCode, Integer motcleCode, CollationUnit newCollationUnit) { //On compare pour voir quelle est la meilleur « collation »
        boolean replace = CollationUnit.checkReplace(collationUnit.getSourceString(), newCollationUnit.getSourceString());
        if (replace) {
            collationUnit = newCollationUnit;
        }
        motcleCodeList.add(motcleCode);
        countIndexation(motcleCode);
    }

    public Supermotcle toSupermotcle() {
        List<Integer> finalMotcleCodeList = PrimUtils.wrap(PrimUtils.toArray(motcleCodeList));
        Set<IndexationCounter> resultSet = new TreeSet<IndexationCounter>(INDEXATION_COMPARATOR);
        int globalIndexationCount = 0;
        for (IndexationCounter indexationCounter : indexationCounterList) {
            if (indexationCounter.indexationCount > 0) {
                globalIndexationCount = globalIndexationCount + indexationCounter.indexationCount;
                resultSet.add(indexationCounter);
            }
        }
        List<Supermotcle.IndexationByBase> indexationByBaseList = new IndexationByBaseList(resultSet.toArray(new Supermotcle.IndexationByBase[resultSet.size()]));
        return new InternalSupermotcle(lang, code, collationUnit, finalMotcleCodeList, globalIndexationCount, indexationByBaseList);
    }

    private void countIndexation(Integer motcleCode) {
        int baseCount = indexationCounterList.size();
        for (int i = 0; i < baseCount; i++) {
            IndexationCounter indexationCounter = indexationCounterList.get(i);
            indexationCounter.checkMotcle(motcleCode);
        }
    }


    private class IndexationCounter implements Supermotcle.IndexationByBase {

        private final Integer baseCode;
        private final Integer[] corpusCodeArray;
        private int indexationCount;

        private IndexationCounter(Integer baseCode, Integer[] corpusCodeArray) {
            this.baseCode = baseCode;
            this.corpusCodeArray = corpusCodeArray;
            this.indexationCount = 0;
        }

        private void checkMotcle(Integer motcleCode) {
            MotcleInfo motcleInfo = dataAccess.getMotcleInfo(motcleCode);
            for (Integer corpusCode : corpusCodeArray) {
                List<Integer> indexationFicheCodeList = motcleInfo.getIndexationCodeList(corpusCode);
                indexationCount += indexationFicheCodeList.size();
            }
        }

        @Override
        public Integer getBaseCode() {
            return baseCode;
        }

        @Override
        public int getIndexationCount() {
            return indexationCount;
        }

    }


    private static class InternalSupermotcle implements Supermotcle {

        private final Lang lang;
        private final Integer code;
        private final CollationUnit collationUnit;
        private final List<Integer> motcleCodeList;
        private final int indexationCount;
        private final List<Supermotcle.IndexationByBase> indexationByBaseList;

        private InternalSupermotcle(Lang lang, Integer code, CollationUnit collationUnit, List<Integer> motcleCodeList, int indexationCount, List<Supermotcle.IndexationByBase> indexationByBaseList) {
            this.lang = lang;
            this.code = code;
            this.collationUnit = collationUnit;
            this.motcleCodeList = motcleCodeList;
            this.indexationCount = indexationCount;
            this.indexationByBaseList = indexationByBaseList;
        }

        @Override
        public Lang getLang() {
            return lang;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public CollationUnit getCollationUnit() {
            return collationUnit;
        }

        @Override
        public List<Integer> getMotcleCodeList() {
            return motcleCodeList;
        }

        @Override
        public int getIndexationCount() {
            return indexationCount;
        }

        @Override
        public List<IndexationByBase> getIndexationByBaseList() {
            return indexationByBaseList;
        }

    }


    private static class IndexationCounterComparator implements Comparator<IndexationCounter> {

        private IndexationCounterComparator() {
        }

        @Override
        public int compare(IndexationCounter s1, IndexationCounter s2) {
            int ic1 = s1.indexationCount;
            int ic2 = s2.indexationCount;
            if (ic1 > ic2) {
                return -1;
            }
            if (ic1 < ic2) {
                return 1;
            }
            int c1 = s1.baseCode;
            int c2 = s2.baseCode;
            if (c1 < c2) {
                return -1;
            }
            if (c2 > c1) {
                return 1;
            }
            return 0;
        }

    }


    private static class IndexationByBaseList extends AbstractList<Supermotcle.IndexationByBase> implements RandomAccess {

        private final Supermotcle.IndexationByBase[] array;

        private IndexationByBaseList(Supermotcle.IndexationByBase[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Supermotcle.IndexationByBase get(int index) {
            return array[index];
        }

    }

}
