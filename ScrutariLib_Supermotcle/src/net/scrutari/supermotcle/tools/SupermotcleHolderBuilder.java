/* ScrutariLib_Supermotcle - Copyright (c) 2010-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.supermotcle.tools;

import java.util.AbstractList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.RandomAccess;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.collation.CollationUnit;
import net.mapeadores.util.text.collation.map.CollatedKeyMap;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.mapeadores.util.text.collation.map.SortedCollatedKeyMap;
import net.mapeadores.util.text.collation.map.ValueFilter;
import net.scrutari.db.api.DataAccess;
import net.scrutari.supermotcle.api.Supermotcle;
import net.scrutari.supermotcle.api.SupermotcleHolder;


/**
 *
 * @author Vincent Calame
 */
public class SupermotcleHolderBuilder {

    private final DataAccess dataAccess;
    private final Map<String, SupermotcleBuilder> builderMap = new HashMap<String, SupermotcleBuilder>();
    private final CollatedKeyMap<SupermotcleBuilder> collatedKeyMap;
    private final Lang lang;
    private int currentCode = 1;

    public SupermotcleHolderBuilder(DataAccess dataAccess, Lang lang) {
        this.dataAccess = dataAccess;
        this.lang = lang;
        this.collatedKeyMap = new SortedCollatedKeyMap<SupermotcleBuilder>(lang.toLocale());
    }

    public void addMotcle(Integer baseCode, Integer thesaurusCode, Integer motcleCode, String libelleString) {
        CollationUnit collationUnit = new CollationUnit(libelleString, collatedKeyMap.getCollator());
        String collatedKey = collationUnit.getCollatedString();
        SupermotcleBuilder supermotcleBuilder = builderMap.get(collatedKey);
        if (supermotcleBuilder == null) {
            supermotcleBuilder = new SupermotcleBuilder(dataAccess, lang, currentCode, thesaurusCode, motcleCode, collationUnit);
            currentCode++;
            builderMap.put(collatedKey, supermotcleBuilder);
            collatedKeyMap.putValueByCollatedKey(collatedKey, supermotcleBuilder);
        } else {
            supermotcleBuilder.addMotcle(thesaurusCode, motcleCode, collationUnit);
        }
    }

    public SupermotcleHolder toSupermotcleMap() {
        CollatedKeyMap<Supermotcle> finalCollatedKeyMap = new SortedCollatedKeyMap<Supermotcle>(lang.toLocale());
        Map<Integer, Supermotcle> supermotcleMap = new HashMap<Integer, Supermotcle>();
        int size = builderMap.size();
        Supermotcle[] supermotcleArray = new Supermotcle[size];
        int p = 0;
        for (SupermotcleBuilder supermotcleBuilder : builderMap.values()) {
            Supermotcle supermotcle = supermotcleBuilder.toSupermotcle();
            supermotcleArray[p] = supermotcle;
            p++;
            supermotcleMap.put(supermotcle.getCode(), supermotcle);
            finalCollatedKeyMap.putValueByCollatedKey(supermotcle.getCollationUnit().getCollatedString(), supermotcle);
        }
        return new InternalSupermotcleHolder(new SupermotcleList(supermotcleArray), finalCollatedKeyMap, supermotcleMap);
    }


    private static class InternalSupermotcleHolder implements SupermotcleHolder {

        private final List<Supermotcle> supermotcleList;
        private final CollatedKeyMap<Supermotcle> collatedKeyMap;
        private final Map<Integer, Supermotcle> supermotcleMap;

        private InternalSupermotcleHolder(List<Supermotcle> supermotcleList, CollatedKeyMap<Supermotcle> collatedKeyMap, Map<Integer, Supermotcle> supermotcleMap) {
            this.supermotcleList = supermotcleList;
            this.collatedKeyMap = collatedKeyMap;
            this.supermotcleMap = supermotcleMap;
        }

        @Override
        public List<Supermotcle> getSupermotcleList() {
            return supermotcleList;
        }

        @Override
        public Supermotcle getSupermotcleByCode(int code) {
            return supermotcleMap.get(code);
        }

        @Override
        public List<SearchResultUnit<Supermotcle>> search(String s, int searchType, ValueFilter valueFilter) {
            return collatedKeyMap.search(s, searchType, valueFilter);
        }

    }


    private static class SupermotcleList extends AbstractList<Supermotcle> implements RandomAccess {

        private final Supermotcle[] array;

        private SupermotcleList(Supermotcle[] array) {
            this.array = array;
        }

        @Override
        public int size() {
            return array.length;
        }

        @Override
        public Supermotcle get(int index) {
            return array[index];
        }

    }

}
