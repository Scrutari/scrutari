/* ScrutariLib_Supermotcle - Copyright (c) 2009-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.supermotcle.api;

import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.collation.CollationUnit;


/**
 *
 * @author Vincent Calame
 */
public interface Supermotcle {

    public Lang getLang();

    public Integer getCode();

    public CollationUnit getCollationUnit();

    public List<Integer> getMotcleCodeList();

    public int getIndexationCount();

    public List<IndexationByBase> getIndexationByBaseList();


    public interface IndexationByBase {

        public Integer getBaseCode();

        public int getIndexationCount();

    }

}
