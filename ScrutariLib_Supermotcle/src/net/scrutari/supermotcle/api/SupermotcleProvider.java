/* ScrutariLib_Supermotcle - Copyright (c) 2009-2014 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.supermotcle.api;

import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public interface SupermotcleProvider {

    public SupermotcleHolder getSupermotcleHolder(Lang lang);

}
