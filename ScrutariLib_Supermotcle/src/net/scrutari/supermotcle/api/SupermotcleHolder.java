/* ScrutariLib_Supermotcle - Copyright (c) 2009-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.supermotcle.api;

import java.util.List;
import net.mapeadores.util.text.collation.map.SearchResultUnit;
import net.mapeadores.util.text.collation.map.ValueFilter;


/**
 *
 * @author Vincent Calame
 */
public interface SupermotcleHolder {

    public List<Supermotcle> getSupermotcleList();

    public Supermotcle getSupermotcleByCode(int code);

    public List<SearchResultUnit<Supermotcle>> search(String s, int searchType, ValueFilter valueFilter);

}
