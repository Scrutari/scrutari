/* global SCT,$$ */

var Feed = {};

Feed.ARGS = {
    clientId: "",
    lang: "",
    canonicalUrl: ""
};

Feed.init = function (args) {
    var $client = $$(args.clientId);
    var feedId = SCT.generateId();
    var feedRootUrl = args.canonicalUrl + "feed/";
    $client.html(SCT.render("feed:client", {
        feedId: feedId,
        lang: args.lang,
        feedRootUrl: feedRootUrl
    }));
    SCT.$($client, {_element: "input"}).on("input", _update);
    
    function _update() {
        let type = SCT.$($client, {_name: "type", _checked: true}).val();
        let options = "";
        SCT.$($client, {_type: "checkbox"}).each(function (index, element) {
           let $element = $(element);
           let usage = $element.data("usage");
           if ((usage === "all") || (usage.indexOf(type) !== -1)) {
               $element.parents("li").removeClass("admin-Unused");
               if (element.checked) {
               options = options + $element.val();
           }
           } else {
               $element.parents("li").addClass("admin-Unused");
           }
        });
        let lang = SCT.$($client, {_name: "lang"}).val();
        let url = feedRootUrl + type;
        if (options.length > 0) {
            url = url + "-" + options;
        }
        url = url + "_" + lang + ".atom";
        $$(feedId, "url").text(url).attr("href", url);
    }
};

$(function () {
    SCT.initTemplates();
    Feed.init(Feed.ARGS);
});