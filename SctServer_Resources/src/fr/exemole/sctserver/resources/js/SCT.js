var SCT = {};

SCT.genid = 0;

function $$(id) {
    if (arguments.length > 1) {
        id = Array.from(arguments).join("_");
    }
    return $(document.getElementById(id)); 
}

function ID(id) {
    if (arguments.length > 1) {
        id = Array.from(arguments).join("_");
    }
    return document.getElementById(id);
}

SCT.genidMap = {};

SCT.templateHelpers = {
    loc: function() {
            return SCT.Loc.get.apply(this, arguments);
        },
    genid: function (nameSpace, localKey) {
            return SCT.generateId(nameSpace, localKey);
        }
};
SCT.templateTags = {
    colon: function() {
                return SCT.escape(SCT.Loc.get("_ label.global.colon"));
        },
    clean: function () {
        return this.tagCtx.render().replace(/\s\s+/gi, "");
    }
};
SCT.templateConverters = {};
SCT.templateInitDone = false;

SCT.generateId = function (nameSpace, localKey) {
    var genIdKey = false;
    if ((nameSpace) && (localKey)) {
        genIdKey = nameSpace + ":" + localKey;
        if (SCT.genidMap.hasOwnProperty(genIdKey)) {
            return SCT.genidMap[genIdKey];
        }
    }
    var newId = SCT.genid + 1;
    SCT.genid = newId;
    var id = "js-";
    if (newId < 10) {
        id += "0";
    } 
    if (newId < 100) {
        id += "0";
    }
    if (newId < 1000) {
        id += "0";
    }
    id = id + newId; 
    if (genIdKey) {
        SCT.genidMap[genIdKey] = id;
    }
    return id;
};

SCT.log = function (msg) {
    if ((console) && (console.log)) {
        console.log(msg);
    }
};

SCT.to$elements = function (jqArgument) {
    if (jqArgument.jquery) {
        return jqArgument;
    } else {
        return $(jqArgument);
    }
};

SCT.escape = function (text) {
    var result = "";
    for(let i = 0, len = text.length; i < len; i++) {
        let carac = text.charAt(i);
        switch (carac) {
            case '&':
                result += "&amp;";
                break;
            case '"':
                result += "&quot;";
                break;
            case '<':
                result += "&lt;";
                break;
            case '>':
                result += "&gt;";
                break;
            case '\'':
                result += "&#x27;";
                break;
            default:
                result += carac;
        }
    }
    return result;
};

SCT.initTemplates = function () {
    if (!SCT.templateInitDone) {
        SCT.templateInitDone = true;
        if ($.templates) {
            $.views.helpers(SCT.templateHelpers);
            $.views.tags(SCT.templateTags);
            $.views.converters(SCT.templateConverters);
            $("script[type='text/x-jsrender']").each(function (index, element) {
                var $element = $(element);
                var name = $element.data("name");
                $.templates(name, $element.html());
            });
        }
    }
};

/**
 * See https://www.jsviews.com/#tmplrender
 * 
 * @param {String} name
 * @param {Object||Array} data
 * @param {Object} context
 * @param {Boolean} noIteration
 * @returns {String}
 */
SCT.render = function (name, data, context, noIteration) {
    if (!SCT.templateInitDone) {
        SCT.log("SCT.initTemplates not called");
        return "";
    }
    var template = $.templates[name];
    if (template) {
        return template.render(data, context, noIteration);
    } else {
        SCT.log("Unknown template: " + name);
        return "";
    }
};

/*******************************************************************************
* SCT.Loc localisation du texte (SCT.Loc.map est remplie par SCT.Loc.add)
*******************************************************************************/

SCT.Loc = {};

SCT.Loc.map = {};

SCT.Loc.add = function (locs) {
    for(let key in locs) {
        SCT.Loc.map[key] = locs[key];
    }
};

SCT.Loc.contains = function (messageKey) {
    return SCT.Loc.map.hasOwnProperty(messageKey);
};

SCT.Loc.get = function (messageKey) {
    var argLength = arguments.length;
    if (messageKey === "") {
        let text = "";
        if (argLength > 1) {
            for(let i = 1; i < argLength; i++) {
                let arg = arguments[i];
                if (!arg) {
                    text += arg;
                }
            }
        }
        return text;
    } else if (!SCT.Loc.map.hasOwnProperty(messageKey)) {
        return "?" + messageKey + "?";
    } else {
        let text = SCT.Loc.map[messageKey];
        if (argLength > 1) {
            for(let i = 1; i < argLength; i++) {
                let arg = arguments[i];
                if (!arg) {
                    arg = "";
                }
                let p = i-1;
                text = text.replace( "{" + p + "}", arg);
            }
        }
        return text;
    }
};

SCT.Loc.escape = function () {
    return SCT.escape(SCT.Loc.get.apply(this, arguments));
};


/*******************************************************************************
* Raccourci JQuery
*******************************************************************************/

SCT.$ = function (jqArgument, properties) {
    if (!properties) {
        properties = jqArgument;
        jqArgument = null;
    }
    var query = SCT.toCssQuery(properties);
    if (jqArgument) {
         return SCT.to$elements(jqArgument).find(query);
    } else {
         return $(query);
    }
};

SCT.$children = function (jqArgument, properties) {
    return SCT.to$elements(jqArgument).children(SCT.toCssQuery(properties));
};

SCT.$parents = function (jqArgument, properties) {
    return SCT.to$elements(jqArgument).parents(SCT.toCssQuery(properties));
};

SCT.toCssQuery = function (properties) {
    var query = "";
    var elementName = false;
    var suffix = "";
    for(let key in properties) {
        let value = properties[key];
        if (!key.startsWith("_")) {
            query += _selector(SCT.toDataAttribute(key), value);
        } else if (key === "_checked") {
            if (value) {
               suffix += ":checked";
           } else {
               suffix += ":not(:checked)";
           }
        } else if (key === "_element") {
            elementName = value;
        } else {
            query += _selector(key.substring(1), value);
        }
    }
    if (elementName) {
        query = elementName + query;
    }
    query += suffix;
    return query;
    
    
    function _selector(name, value) {
        if (value === true) {
            return "[" + name + "]";
        } else {
            value = value.toString().replace("'", "\\'");
            return "[" + name + "='" + value + "']";
        }
    }
    
};

SCT.toDataAttribute = function (camelCaseString) {
    return "data-" + camelCaseString.replace(/[A-Z]/g, function (upperLetter) {
        return "-" + upperLetter.toLowerCase();
    });
};