/* global SCT,$$,ID */

var QueryBuilder = {};

QueryBuilder.ARGS = {
    clientId: "",
    lang: "",
    canonicalUrl: "",
    withCategory: false,
    variantArray: ["empty", "query", "data", "geo"]
};

QueryBuilder.FICHEFIELDS = [
    {
        name: "-codefiche",
        loc: "_ field.exclude_codefiche"
    },
    {
        name: "codecorpus",
        loc: "_ field.codecorpus"
    },
    {
        name: "codebase",
        loc: "_ field.codebase"
    },
    {
        name: "basename",
        loc: "_ field.basename"
    },
    {
        name: "codebase",
        loc: "_ field.corpusname"
    },
    {
        name: "ficheid",
        loc: "_ field.ficheid"
    },
    {
        name: "titre",
        loc: "_ field.titre"
    },
    {
        name: "soustitre",
        loc: "_ field.soustitre"
    },
    {
        name: "href",
        loc: "_ field.href"
    },
    {
        name: "lang",
        loc: "_ field.lang"
    },
    {
        name: "year",
        loc: "_ field.year"
    },
    {
        name: "date",
        loc: "_ field.date"
    },
    {
        name: "dateiso",
        loc: "_ field.dateiso"
    },
    {
        name: "ficheicon",
        loc: "_ field.ficheicon"
    },
    {
        name: "icon",
        loc: "_ field.icon"
    },
    {
        name: "geo",
        loc: "_ field.geo"
    },
    {
        name: "complements",
        loc: "_ field.complements"
    },
    {
        name: "attrs",
        loc: "_ field.attrs"
    },
    {
        name: "attrs_primary",
        loc: "_ field.attrs_primary"
    },
    {
        name: "attrs_all",
        loc: "_ field.attrs_all"
    },
    {
        name: "mtitre",
        loc: "_ field.mtitre"
    },
    {
        name: "msoustitre",
        loc: "_ field.msoustitre"
    },
    {
        name: "mcomplements",
        loc: "_ field.mcomplements"
    },
    {
        name: "mcomplements_all",
        loc: "_ field.mcomplements_all"
    },
    {
        name: "mattrs",
        loc: "_ field.mattrs"
    },
    {
        name: "mattrs_primary",
        loc: "_ field.mattrs_primary"
    },
    {
        name: "mattrs_all",
        loc: "_ field.mattrs_all"
    },
    {
        name: "score",
        loc: "_ field.score"
    },
    {
        name: "codemotclearray",
        loc: "_ field.codemotclearray"
    },
    {
        name: "-codemotclearray",
        loc: "_ field.exclude_codemotclearray"
    },
    {
        name: "codemotclearray_all",
        loc: "_ field.codemotclearray_all"
    },
    {
        name: "bythesaurusmap",
        loc: "_ field.bythesaurusmap"
    },
    {
        name: "bythesaurusmap_all",
        loc: "_ field.bythesaurusmap_all"
    }
];

QueryBuilder.SCOPES = [
    {
        name: "langlist",
        loc: "_ label.querybuilder.scope_lang",
        size: 10
    },
    {
        name: "baselist",
        loc: "_ label.querybuilder.scope_base",
        size: 35
    },
    {
        name: "corpuslist",
        loc: "_ label.querybuilder.scope_corpus",
        size: 50
    },
    {
        name: "categorylist",
        loc: "_ label.querybuilder.scope_category",
        size: 30
    },
    {
        name: "thesauruslist",
        loc: "_ label.querybuilder.scope_thesaurus",
        size: 50
    },
];
 

QueryBuilder.init = function (args) {
    var $client = $$(args.clientId);
    var builderId = SCT.generateId();
    var jsonRootUrl = args.canonicalUrl + "json";
    var types = QueryBuilder.Type.init(QueryBuilder.TYPES, args.withCategory);
    var scopes = QueryBuilder.initScopes(args.withCategory);
    $client.html(SCT.render("querybuilder:client", {
        builderId: builderId,
        lang: args.lang,
        jsonRootUrl: jsonRootUrl,
        types: types,
        scopes: scopes,
        variants: args.variantArray,
        fichefields: QueryBuilder.FICHEFIELDS
    }));
    SCT.$($client, {_element: "input"}).on("input", _update);
    SCT.$($client, {_element: "select"}).on("input", _update);
    SCT.$($client, {deployTarget: true}).on("change", _deploy);
    $$(builderId, "type").on("change", function () {
        let type = _getType();
        SCT.$($client, {role: "fieldset"}).each(function (index, element) {
            let $element = $(element);
            let name = $element.data("name");
            if (type.useFieldset(name)) {
                $element.removeClass("querybuilder-Hidden");
            } else {
                $element.addClass("querybuilder-Hidden");
            }
        });
        SCT.$($client, {role: "scope"}).each(function (index, element) {
            let $element = $(element);
            let name = $element.data("name");
            if (type.useScope(name)) {
                $element.removeClass("querybuilder-Hidden");
            } else {
                $element.addClass("querybuilder-Hidden");
            }
        });
        SCT.$($client, {role: "fichefield"}).each(function (index, element) {
            let $element = $(element);
            let name = $element.data("name");
            if (type.useField(name)) {
                $element.removeClass("querybuilder-Hidden");
            } else {
                $element.addClass("querybuilder-Hidden");
            }
        });
    });
    
    function _update() {
        let type = _getType();
        let query = "?type=" + type.type + "&version=3";
        let litteralQuery = "?type=" + type.type + "&version=3";
        __addParam("lang", ID(builderId, "lang").value);
        if (type.useFieldset("fichelist")) {
            let input = ID(builderId, "fichelist");
            let value = QueryBuilder.normalize(input.value);
            __addParam("fichelist", value);
        }
        if (type.useFieldset("scope")) {
            for(let scope of scopes) {
                __addList("scope", scope.name);
            }
        }
        if (type.useFieldset("fichecontent")) {
            let checkedInput = SCT.$($client, {_element: "input", _name: "fichecontent", _checked: true});
            let value = checkedInput.val();
            if (value === "custom") {
                let values = "";
                SCT.$($client, {_element: "input", _name: "fichefield", _checked: true}).each(function (index,element) {
                    let fieldName = element.value;
                    if (type.useField(fieldName)) {
                        values += "," + element.value;
                    }
                });
                __addParam("fichefields", QueryBuilder.normalize(values));
            } else if (value === "variant") {
                let variantSelect = ID(builderId, "variant");
                let variantName = variantSelect.options[variantSelect.selectedIndex].value;
                __addParam("fieldvariant", variantName);
            }
        }
        $$(builderId, "url").text(jsonRootUrl + litteralQuery).attr("href", jsonRootUrl + query);
        
        
        function __addParam(name, value) {
            if (value) {
                value = value.trim();
            }
            if (!value) {
                return;
            }
            query += "&" + name + "="+ encodeURIComponent(value);
            litteralQuery += "&" + name + "="+ value;
        }
        
        function __addList(prefix, scopeName) {
            if (type.useScope(scopeName)) {
                let input = ID(builderId, prefix, scopeName);
                let value = QueryBuilder.normalize(input.value);
                __addParam(scopeName, value);
            }
        }
    }
    
    
    
    function _deploy() {
        let name = this.name;
        SCT.$($client, {_element: "input", _name: name}).each(function (index, element) {
            let target = $(this).data("deployTarget");
            if (target) {
                if (this.checked) {
                    $$(target).removeClass("querybuilder-Hidden");
                } else {
                    $$(target).addClass("querybuilder-Hidden");
                }
            }
        });
    }
    
    function _getType() {
       let select = ID(builderId, "type");
       let name = select.options[select.selectedIndex].value;
       for(let type of types) {
           if (type.name === name) {
               return type;
           }
       }
    }

};

QueryBuilder.checkUsage = function (scope, type) {
    if (scope.usage.indexOf(type.name) !== -1) {
        return true;
    } else {
        return false;
    }
};

QueryBuilder.initScopes = function (withCategory) {
    let array = new Array();
    for (let scope of QueryBuilder.SCOPES) {
        if ((!withCategory) && (scope.name === "categorylist")) {
            continue;
        }
        array.push(scope);
    }
    return array;
};

QueryBuilder.encode = function (val) {
    if (!val) {
        return "";
    }
    return encodeURIComponent(val.trim());
};

QueryBuilder.normalize = function (val) {
    if (!val) {
        return "";
    }
    val = val.trim();
    let array = val.split(/[\s,;]/);
    let result = "";
    
    for(let token of array) {
        if (token.length > 0) {
            if (result.length > 0) {
                result += ",";
            }
            result += token;
        }
    }
    return result;
};
    
 $(function () {
    SCT.initTemplates();
    QueryBuilder.init(QueryBuilder.ARGS);
});