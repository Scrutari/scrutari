/* global SCT,$$,ID,QueryBuilder */

QueryBuilder.Type = function (obj) {
    this.name = obj.name;
    this.type = obj.type;
    this.loc = obj.loc;
    this.fieldsets = obj.fieldsets;
    this.excludeScopes = obj.excludeScopes;
    this.excludeFields = obj.excludeFields;
};

QueryBuilder.Type.prototype.useFieldset = function (fieldsetName) {
    return ((this.fieldsets) && (this.fieldsets.indexOf(fieldsetName) !== -1));
};

QueryBuilder.Type.prototype.useScope = function (scopeName) {
    if (!this.excludeScopes) {
        return this.useFieldset("scope");
    }
    return (this.excludeScopes.indexOf(scopeName) === -1);
};

QueryBuilder.Type.prototype.useField = function (fieldName) {
    if (!this.excludeFields) {
        return this.useFieldset("fichecontent");
    }
    return (this.excludeFields.indexOf(fieldName) === -1);
};

QueryBuilder.Type.init = function (array, withCategory) {
    var typeArray = new Array();
    for(let obj of array) {
        if ((!withCategory) && (obj.name === "category")) {
            continue;
        }
        typeArray.push(new QueryBuilder.Type(obj));
    }
    return typeArray;
};


QueryBuilder.FICHEEXCLUDE = ["mtitre", "msoustitre", "mcomplements", "mcomplements_all", "mattrs", "mattrs_primary", "mattrs_all", "score", "-codemotclearray", "codemotclearray_all",  "bythesaurusmap_all"];

QueryBuilder.TYPES = [
    {
        name: "base",
        type: "base",
        loc: "_ label.querybuilder.type_base"
    },
    {
        name: "corpus",
        type: "corpus",
        loc: "_ label.querybuilder.type_corpus"
    },
    {
        name: "thesaurus",
        type: "thesaurus",
        loc: "_ label.querybuilder.type_thesaurus"
    },
    {
        name: "category",
        type: "category",
        loc: "_ label.querybuilder.type_category"
    },
    {
        name: "qfiche",
        type: "q-fiche",
        loc: "_ label.querybuilder.type_qfiche",
        fieldsets: ["scope", "filters", "fichecontent", "postscriptum"],
        excludeFields: QueryBuilder.FICHEEXCLUDE
    },
    {
        name: "fiche_list",
        type: "fiche",
        loc: "_ label.querybuilder.type_fiche_list",
        fieldsets: ["fichelist", "fichecontent", "postscriptum"],
        excludeScopes: ["thesauruslist"],
        excludeFields: QueryBuilder.FICHEEXCLUDE
    },
    {
        name: "fiche_scope",
        type: "fiche",
        loc: "_ label.querybuilder.type_fiche_scope",
        fieldsets: ["scope", "filters", "fichecontent", "postscriptum"],
        excludeScopes: ["thesauruslist"],
        excludeFields: ["codemotclearray"]
    },
   
];
