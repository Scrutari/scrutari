/* global Scrutari */

var Client = {};

Client.ARGS = {
   lang: "en", 
   engineName: "",
   clientId: ""
};

$(function () {
    var scrutariConfig = new Scrutari.Config(Client.ARGS.engineName, "../", Client.ARGS.lang, "admin");
    Scrutari.Client.init(scrutariConfig, Client.ARGS.clientId, {
        ignoreList: "area-title,result-poweredby"
    });
});