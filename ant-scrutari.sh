#!/bin/bash
Sources="$PWD"
Target="$PWD/target"
Version="local"
Repository="local"
WebappName="scrutari"
ConfDir="$PWD/examples/context/conf/"
VarDir="$PWD/examples/context/var/"
AntTargets="webinf war"

ant -Ddeploy.path=$Target -Dproject.version="$Version"  -Dproject.repository="$Repository" -Dproject.webapp_name="$WebappName" -Dproject.conf_dir="$ConfDir" -Dproject.var_dir="$VarDir" -f $Sources/ant/Scrutari/build.xml $AntTargets
