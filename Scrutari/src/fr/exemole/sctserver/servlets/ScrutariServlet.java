/* Scrutari - Copyright (c) 2009-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.context.EngineGroup;
import fr.exemole.sctserver.desmography.DesmographyResponseHandlerFactory;
import fr.exemole.sctserver.oai.OaiResponseHandlerFactory;
import fr.exemole.sctserver.request.json.JsonProducerFactory;
import fr.exemole.sctserver.servlets.admin.AdminResponseHandlerFactory;
import fr.exemole.sctserver.servlets.export.ExportResponseHandlerFactory;
import fr.exemole.sctserver.servlets.feed.FeedResponseHandlerFactory;
import fr.exemole.sctserver.servlets.logs.LogsResponseHandlerFactory;
import fr.exemole.sctserver.servlets.webapp.GroupsResponseHandlerFactory;
import fr.exemole.sctserver.servlets.webapp.WebappResponseHandlerFactory;
import java.io.IOException;
import java.text.ParseException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.json.JsonProducer;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.HttpServletRequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.servlets.handlers.JsonResponseHandler;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest requete, HttpServletResponse reponse) throws IOException, ServletException {
        process(requete, reponse);
    }

    @Override
    public void doPost(HttpServletRequest requete, HttpServletResponse reponse) throws IOException, ServletException {
        process(requete, reponse);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null) {
            pathInfo = "/";
        }
        int idx = pathInfo.indexOf('/', 2);
        if (idx != -1) {
            String engineName = pathInfo.substring(1, idx);
            String relativePath = pathInfo.substring(idx + 1);
            if (engineName.equals("_resources")) {
                processResourcesRequest(relativePath, request, response);
            } else if (engineName.equals("_groups")) {
                processGroupsRequest(relativePath, request, response);
            } else {
                processEngineRequest(engineName, relativePath, request, response);
            }
        } else {
            processWebappsRequest(pathInfo.substring(1), request, response);
        }
    }

    private void processWebappsRequest(String action, HttpServletRequest request, HttpServletResponse response) throws IOException {
        if (action.length() == 0) {
            StringBuffer requestURLString = request.getRequestURL();
            if (requestURLString.charAt(requestURLString.length() - 1) != '/') {
                response.sendRedirect(requestURLString.append('/').toString());
                return;
            }
        }
        ScrutariWebapp scrutariWebapp = ScrutariWebapp.getScrutariWebapp(getServletContext());
        RequestMap requestMap = new HttpServletRequestMap(request);
        ResponseHandler responseHandler = WebappResponseHandlerFactory.getHandler(action, scrutariWebapp, requestMap);
        if (responseHandler == null) {
            if (scrutariWebapp.getEngine(action) != null) {
                response.sendRedirect(request.getRequestURL().append('/').toString());
                return;
            }
        }
        handleResponse(request, response, responseHandler);
    }

    private void processResourcesRequest(String relativePath, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ScrutariWebapp scrutariWebapp = ScrutariWebapp.getScrutariWebapp(getServletContext());
        ResponseHandler responseHandler = getResourceHandler(scrutariWebapp.getResourceStorages(), relativePath);
        handleResponse(request, response, responseHandler);
    }

    private void processGroupsRequest(String relativePath, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ScrutariWebapp scrutariWebapp = ScrutariWebapp.getScrutariWebapp(getServletContext());
        ResponseHandler responseHandler = null;
        int idx = relativePath.indexOf('/');
        String groupName;
        String groupRelativePath;
        if (idx == -1) {
            groupName = relativePath;
            groupRelativePath = null;
        } else {
            groupName = relativePath.substring(0, idx);
            groupRelativePath = relativePath.substring(idx + 1);
        }
        EngineGroup engineGroup = scrutariWebapp.getEngineGroup(groupName);
        if (engineGroup != null) {
            if (groupRelativePath == null) {
                response.sendRedirect(request.getRequestURL().append('/').toString());
                return;
            }
            responseHandler = GroupsResponseHandlerFactory.getHandler(engineGroup, groupRelativePath, scrutariWebapp, new HttpServletRequestMap(request));
        }
        handleResponse(request, response, responseHandler);
    }

    private void processEngineRequest(String engineName, String engineRelativePath, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ScrutariWebapp scrutariWebapp = ScrutariWebapp.getScrutariWebapp(getServletContext());
        SctEngine engine = scrutariWebapp.getEngine(engineName);
        if (engine == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getPathInfo());
            return;
        }
        RequestMap requestMap = new HttpServletRequestMap(request);
        ResponseHandler responseHandler = null;
        int idx = engineRelativePath.indexOf('/');
        if (idx == -1) {
            if (engineRelativePath.length() == 0) {
                responseHandler = ServletUtils.wrap("AVAILABLE SCRUTARI ENGINE [" + engineName + "]");
            } else if (engineRelativePath.toLowerCase().equals("json")) {
                JsonProducer jsonProducer = JsonProducerFactory.getJsonProducer(engine.getScrutariSession(), requestMap);
                responseHandler = JsonResponseHandler.init(jsonProducer, requestMap.getParameter("callback"))
                        .accessControl(JsonResponseHandler.ALL_ORIGIN);
            } else {
                switch (engineRelativePath) {
                    case "admin":
                    case "feed":
                    case "resources":
                    case "logs":
                    case "export":
                        response.sendRedirect(request.getRequestURL().append('/').toString());
                        return;
                }
            }
        } else {
            String scope = engineRelativePath.substring(0, idx);
            String scopeRelativePath = engineRelativePath.substring(idx + 1);
            switch (scope) {
                case "admin":
                    responseHandler = AdminResponseHandlerFactory.getHandler(engine, scopeRelativePath, requestMap);
                    break;
                case "feed":
                    responseHandler = FeedResponseHandlerFactory.getHandler(engine, scopeRelativePath, requestMap, ServletUtils.getFullUrl(engine.getEngineContext().getWebappCanonicalUrl(), request));
                    break;
                case "resources":
                    responseHandler = getResourceHandler(engine.getResourceStorages(), scopeRelativePath);
                    break;
                case "logs":
                    responseHandler = LogsResponseHandlerFactory.getHandler(scopeRelativePath, engine.getEngineStorage().getLogStorage());
                    break;
                case "export":
                    responseHandler = ExportResponseHandlerFactory.getHandler(engine, scopeRelativePath, requestMap);
                    break;
                case "api":
                    responseHandler = getApiResponseHandler(engine, scopeRelativePath, requestMap);
                    break;
            }
        }
        handleResponse(request, response, responseHandler);
    }

    private void handleResponse(HttpServletRequest request, HttpServletResponse response, ResponseHandler responseHandler) throws IOException {
        if (responseHandler == null) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getPathInfo());
            return;
        }
        long lastModified = responseHandler.getLastModified();
        if (lastModified == ResponseHandler.LASTMODIFIED_NOCACHE) {
            response.setHeader("Cache-Control", "no-store");
        } else if (lastModified > 0) {
            response.setDateHeader("Last-Modified", lastModified);
            try {
                long since = request.getDateHeader("If-Modified-Since");
                if (since >= lastModified) {
                    response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                    return;
                }
            } catch (IllegalArgumentException iae) {
            }
        }
        responseHandler.handleResponse(response);
    }

    private ResponseHandler getApiResponseHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        int idx = relativePath.indexOf('/');
        String apiName;
        String apiRelativePath;
        if (idx == -1) {
            apiName = relativePath;
            apiRelativePath = "";
        } else {
            apiName = relativePath.substring(0, idx);
            apiRelativePath = relativePath.substring(idx + 1);
        }
        switch (apiName) {
            case "desmography":
                return DesmographyResponseHandlerFactory.getHandler(engine, apiRelativePath, requestMap);
            case "oai":
                return OaiResponseHandlerFactory.getHandler(engine, apiRelativePath, requestMap);
            default:
                return null;
        }
    }

    private ResponseHandler getResourceHandler(ResourceStorages resourceStorages, String path) {
        try {
            RelativePath relativePath = RelativePath.parse(path);
            DocStream docStream = resourceStorages.getResourceDocStream(relativePath);
            if (docStream != null) {
                return ServletUtils.wrap(docStream);
            } else {
                return null;
            }
        } catch (ParseException pe) {
            return null;
        }
    }

}
