/* Scrutari - Copyright (c) 2008-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import fr.exemole.sctserver.api.SctEngine;


/**
 *
 * @author Vincent Calame
 */
public class RestartCommand extends Command {

    public final static String COMMANDNAME = "Restart";
    public final static String COMMANDKEY = "_ SCT-02";
    public final static String FORCE_PARAMNAME = "force";

    public RestartCommand(SctEngine engine) {
        super(engine);
    }

    @Override
    protected void doCommand() {
        boolean forceReload = requestMap.isTrue(FORCE_PARAMNAME);
        boolean done = engine.reload(forceReload);
        if (done) {
            setDone("_ done.admin.restart");
        } else {
            setError("_ error.reloadprocess");
        }
    }

}
