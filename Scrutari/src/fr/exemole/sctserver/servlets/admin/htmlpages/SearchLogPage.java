/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.storage.LogStorage;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class SearchLogPage extends AdminHtmlPage {

    private final FuzzyDate date;

    public SearchLogPage(SctEngine engine, Lang workingLang, FuzzyDate date) {
        super(engine, workingLang);
        this.date = date;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        LogStorage logStorage = engine.getEngineStorage().getLogStorage();
        FuzzyDate[] jourArray = logStorage.getDateArray(FuzzyDate.DAY_TYPE, date);
        start();
        this
                .__(printTools())
                .__(printMessage())
                .H2()
                .__escape(date)
                ._H2();
        for (FuzzyDate dayDate : jourArray) {
            this
                    .__(printJour(dayDate, logStorage));
        }
        end();
    }

    private boolean printJour(FuzzyDate dayDate, LogStorage logStorage) {
        String content = logStorage.getQLog(dayDate);
        this
                .H3()
                .__append(dayDate.getDay())
                ._H3()
                .__(startPanel())
                .PRE()
                .__escape(content, true)
                ._PRE()
                .__(endPanel());
        return true;
    }

}
