/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.stats.CountStats;


/**
 *
 * @author Vincent Calame
 */
public class StatsPage extends AdminHtmlPage {

    private final ScrutariDB scrutariDB;

    public StatsPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
        scrutariDB = scrutariSession.getScrutariDB();
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            initResponse(response);
            CountStats countStats = scrutariDB.getStats().getEngineCountStats();
            start();
            this
                    .__(printTools())
                    .__(printMessage())
                    .__(startH2Panel("_ title.stats.stats"))
                    .TABLE()
                    .__(printRow("_ label.stats.corpus", countStats.getCorpusCount()))
                    .__(printRow("_ label.stats.thesaurus", countStats.getThesaurusCount()))
                    .__(printRow("_ label.stats.fiches", countStats.getFicheCount()))
                    .__(printRow("_ label.stats.motscles", countStats.getMotcleCount()))
                    .__(printRow("_ label.stats.lexie", countStats.getLexieCount()))
                    .__(printRow("_ label.stats.indexation", countStats.getIndexationCount()))
                    ._TABLE()
                    .__(endPanel())
                    .__(printCorpusURIList(dataAccess))
                    .__(printThesaurusURIList(dataAccess));
            end();
        }
    }

    private boolean printRow(String key, int count) {
        this
                .TR()
                .TD()
                .__localize(key)
                .__colon()
                .__nonBreakableSpace()
                ._TD()
                .TD()
                .__append(count)
                ._TD()
                ._TR();
        return true;
    }

    private boolean printCorpusURIList(DataAccess dataAccess) {
        this
                .__(startH2Panel("_ title.stats.list_corpus"))
                .UL();
        for (BaseData baseData : dataAccess.getBaseDataList()) {
            printCorpusByBase(baseData, dataAccess);
        }
        this
                ._UL()
                .__(endPanel());
        return true;
    }

    private boolean printThesaurusURIList(DataAccess dataAccess) {
        this
                .__(startH2Panel("_ title.stats.list_thesaurus"))
                .UL();
        for (BaseData baseData : dataAccess.getBaseDataList()) {
            printThesaurusByBase(baseData, dataAccess);
        }
        this
                ._UL()
                .__(endPanel());
        return true;
    }

    private boolean printCorpusByBase(BaseData baseData, DataAccess dataAccess) {
        this
                .LI()
                .P()
                .__escape(LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_LONGTITLE, workingLang, "?"))
                ._P()
                .UL();
        for (Integer corpusCode : baseData.getCorpusCodeList()) {
            CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
            printCorpusData(corpusData);
        }
        this
                ._UL()
                ._LI();
        return true;
    }

    private boolean printThesaurusByBase(BaseData baseData, DataAccess dataAccess) {
        this
                .LI()
                .P()
                .__escape(LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_LONGTITLE, workingLang, "?"))
                ._P()
                .UL();
        for (Integer thesaurusCode : baseData.getThesaurusCodeList()) {
            ThesaurusData thesaurusData = dataAccess.getThesaurusData(thesaurusCode);
            printThesaurusData(thesaurusData);
        }
        this
                ._UL()
                ._LI();
        return true;
    }

    private boolean printCorpusData(CorpusData corpusData) {
        this
                .LI()
                .P()
                .__escape(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_TITLE, workingLang, "?"))
                .__space()
                .CODE()
                .__escape('[')
                .__escape(corpusData.getCorpusURI())
                .__escape(']')
                .__escape(" (code=")
                .__escape(corpusData.getCorpusCode())
                .__escape(")")
                ._CODE()
                ._P()
                ._LI();
        return true;
    }

    private boolean printThesaurusData(ThesaurusData thesaurusData) {
        this
                .LI()
                .P()
                .__escape(LabelUtils.seekLabelString(thesaurusData.getPhrases(), DataConstants.THESAURUS_TITLE, workingLang, "?"))
                .__space()
                .CODE()
                .__escape('[')
                .__escape(thesaurusData.getThesaurusURI())
                .__escape(']')
                .__escape(" (code=")
                .__escape(thesaurusData.getThesaurusCode())
                .__escape(")")
                ._CODE()
                ._P()
                ._LI();
        return true;
    }

}
