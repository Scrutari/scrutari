/* Scrutari - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.servlets.admin.SctJsLibCatalog;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class QueryBuilderPage extends AdminHtmlPage {

    public QueryBuilderPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
        addJsLib(SctJsLibCatalog.QUERYBUILDER);
        addThemeCss("querybuilder.css");
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("lang", workingLang.toString())
                .put("clientId", clientId)
                .put("canonicalUrl", engine.getCanonicalUrl())
                .put("withCategory", engine.getScrutariSession().getGlobalSearchOptions().isWithCategory())
                .put("variantArray", getVariantArray());
        start();
        this
                .SCRIPT()
                .__jsAssignObject("QueryBuilder.ARGS", args)
                ._SCRIPT()
                .__(printTools())
                .__(printMessage())
                .__(startH2Panel("_ title.admin.querybuilder"))
                .DIV(HA.id(clientId))
                ._DIV()
                .__(endPanel());
        end();
    }

    private String[] getVariantArray() {
        List<FieldVariant> variantList = engine.getFieldVariantManager().getFieldVariantList(true);
        int length = variantList.size();
        String[] array = new String[length];
        for (int i = 0; i < length; i++) {
            array[i] = variantList.get(i).getName();
        }
        return array;
    }

}
