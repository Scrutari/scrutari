/* Scrutari - Copyright (c) 2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import java.util.List;


/**
 *
 * @author Vincent Calame
 */
public final class PertinenceOrderPrint {

    private PertinenceOrderPrint() {

    }

    public static boolean printPertinenceOrder(AdminHtmlPage hp) {
        List<String> orderList = hp.getEngine().getScrutariSession().getScrutariDB().getFieldRankManager().getOrderList();
        String[] confOrder = hp.getEngine().getEngineStorage().getEngineConf().getPertinenceOrderArray();
        boolean next = false;
        hp
                .P()
                .__localize("_ label.sctconf.pertinenceorder_current")
                .__colon()
                .__space()
                .CODE();
        for (String token : orderList) {
            if (next) {
                hp
                        .__escape(", ");
            } else {
                next = true;
            }
            hp
                    .__escape(token);
        }
        hp
                ._CODE()
                ._P();
        if (confOrder != null) {
            next = false;
            hp
                    .P()
                    .__localize("_ label.sctconf.pertinenceorder_conf")
                    .__colon()
                    .__space()
                    .CODE();
            for (String token : confOrder) {
                if (next) {
                    hp
                            .__escape(", ");
                } else {
                    next = true;
                }
                hp
                        .__escape(token);
            }
            hp
                    ._CODE()
                    ._P();
        }
        return true;
    }

}
