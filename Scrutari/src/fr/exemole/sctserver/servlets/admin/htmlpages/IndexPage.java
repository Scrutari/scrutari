/* Scrutari - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.AttributeDefManager;
import fr.exemole.sctserver.api.ConfState;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.conf.ConfConstants;
import fr.exemole.sctserver.api.log.SctLogConstants;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.servlets.admin.ConfUpdateCommand;
import fr.exemole.sctserver.servlets.admin.RestartCommand;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.AttributesPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.CategoriesPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.FieldsPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.MetadataPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.PertinenceOrderPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.SourcesPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.UriCodesPrint;
import fr.exemole.sctserver.servlets.admin.htmlpages.utils.ValidationPrint;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MessageLog;
import net.mapeadores.util.logging.MessageSource;
import net.scrutari.db.api.ScrutariConstants;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.options.PertinencePonderation;


/**
 *
 * @author Vincent Calame
 */
public class IndexPage extends AdminHtmlPage {

    private final ConfState confState;

    public IndexPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
        confState = engine.getConfState();
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        start();
        this
                .__(printTools())
                .__(printMessage())
                .__(printUpdateProcess())
                .__(printConfState());
        end();
    }

    private boolean printUpdateProcess() {
        this
                .__(startH2Panel("_ title.sctconf.updateprocess"))
                .__(printLog())
                .__(printRestartForm())
                .__(endPanel());
        return true;
    }

    private boolean printLog() {
        LogStorage logStorage = engine.getEngineStorage().getLogStorage();
        String dataLog = logStorage.getLog(SctLogConstants.DATA_DIR, "data.xml");
        if (dataLog == null) {
            return false;
        }
        MessageLog messageLog = LogUtils.readMessageLog(dataLog);
        if (messageLog.isEmpty()) {
            return false;
        }
        this
                .P()
                .SPAN("admin-ErrorMessage")
                .__localize("_ info.admin.updateprocesslog")
                ._SPAN()
                ._P();
        this
                .UL();
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            this
                    .LI();
            this
                    .P()
                    .CODE()
                    .__escape('[')
                    .__escape(messageSource.getName())
                    .__escape(']')
                    ._CODE()
                    ._P();
            List<MessageSource.Entry> messageList = messageSource.getEntryList();
            this
                    .UL();
            if (messageList.size() < 6) {
                for (MessageSource.Entry entry : messageList) {
                    printMessageEntry(entry);
                }
            } else {
                int remaining = 0;
                Map<String, Increment> incrementMap = new HashMap<String, Increment>();
                for (MessageSource.Entry entry : messageList) {
                    String category = entry.getCategory();
                    Increment increment = incrementMap.get(category);
                    if (increment == null) {
                        increment = new Increment(3);
                        incrementMap.put(category, increment);
                    }
                    if (increment.isReached()) {
                        remaining++;
                    } else {
                        printMessageEntry(entry);
                        increment.next();
                    }
                }
                if (remaining > 0) {
                    this
                            .LI()
                            .P()
                            .__localize("_ info.admin.moreerrors", remaining)
                            ._P()
                            ._LI();
                }
            }
            this
                    ._UL();
            this
                    ._LI();
        }
        this
                ._UL();
        return true;
    }

    private boolean printMessageEntry(MessageSource.Entry entry) {
        String category = entry.getCategory();
        this
                .LI()
                .P();
        if (category.length() > 0) {
            this
                    .CODE()
                    .__escape('[')
                    .__escape(category)
                    .__escape(']')
                    ._CODE()
                    .__space();
        }
        this
                .__localize(entry.getMessage())
                ._P()
                ._LI();
        return true;
    }


    private boolean printConfState() {
        this
                .__(startH2Panel("_ title.sctconf.files"));
        short state = confState.getGlobalState();
        boolean printConfDetails = false;
        switch (state) {
            case ConfConstants.GLOBAL_CONFIO_ERROR:
                this
                        .P()
                        .SPAN("admin-ErrorMessage")
                        .__localize("_ error.global.conf_io")
                        .__escape(" : ")
                        .__escape(confState.getConfIOMessage())
                        ._SPAN()
                        ._P();
                break;
            case ConfConstants.GLOBAL_UNDEFINED:
                this
                        .P()
                        .SPAN("admin-ErrorMessage")
                        .__localize("_ error.global.conf_null")
                        .__escape(" : ")
                        .__escape(confState.getConfIOMessage())
                        ._SPAN()
                        ._P();
                break;
            case ConfConstants.GLOBAL_OK:
                this
                        .UL()
                        .__(printConfFileLi(ConfConstants.SOURCES_NAME))
                        .__(printConfFileLi(ConfConstants.METADATA_NAME))
                        .__(printConfFileLi(ConfConstants.ATTRIBUTES_NAME))
                        .__(printConfFileLi(ConfConstants.CATEGORIES_NAME))
                        .__(printConfFileLi(ConfConstants.URI_CODES_NAME))
                        .__(printConfFileLi(ConfConstants.OPTIONS_NAME))
                        .__(printConfFileLi(ConfConstants.VALIDATION_NAME))
                        .__(printConfFileLi(ConfConstants.FIELDS_NAME))
                        ._UL();
                printConfDetails = true;
                break;
        }
        this
                .__(startCommandBox(ConfUpdateCommand.COMMANDNAME, ConfUpdateCommand.COMMANDKEY))
                .__(endCommandBox("_ submit.admin.confupdate"))
                .__(endPanel());
        if (printConfDetails) {
            this
                    .__(startH2Panel("_ title.sctconf.details"))
                    .__(printSourcesDetails())
                    .__(printMetadataDetails())
                    .__(printAttributesDetails())
                    .__(printCategoriesDetails())
                    .__(printUriCodesDetails())
                    .__(printOptionsDetails())
                    .__(printValidationDetails())
                    .__(printFieldsDetails())
                    .__(endPanel());
        }
        return true;
    }

    private boolean printConfFileLi(String name) {
        this
                .LI()
                .P()
                .__(printFileInfo(name, true))
                ._P()
                ._LI();
        return true;
    }

    private boolean printSourcesDetails() {
        this
                .H3(HA.id(ConfConstants.SOURCES_NAME))
                .__localize("_ title.sctconf.sources")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.SOURCES_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.SOURCES_NAME))
                .__(SourcesPrint.printScrutariSourceList(this))
                .__(endPanel());
        return true;
    }

    private boolean printMetadataDetails() {
        this
                .H3(HA.id(ConfConstants.METADATA_NAME))
                .__localize("_ title.sctconf.metadata")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.METADATA_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.METADATA_NAME))
                .__(MetadataPrint.printMetadata(this, engine.getEngineMetadata()))
                .__(endPanel());
        return true;
    }

    private boolean printAttributesDetails() {
        AttributeDefManager attributeDefManager = engine.getAttributeDefManager();
        this
                .H3(HA.id(ConfConstants.ATTRIBUTES_NAME))
                .__localize("_ title.sctconf.attributes")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.ATTRIBUTES_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.ATTRIBUTES_NAME));
        if (attributeDefManager.getGroupNameSet().isEmpty()) {
            this
                    .P()
                    .SPAN("admin-Info")
                    .__localize("_ info.sctconf.none_attributes")
                    ._SPAN()
                    ._P();
        } else {
            this
                    .__(printAttributesList(attributeDefManager.getAttributeDefList(ScrutariConstants.PRIMARY_GROUP), "_ title.sctconf.attributes_primary"))
                    .__(printAttributesList(attributeDefManager.getAttributeDefList(ScrutariConstants.SECONDARY_GROUP), "_ title.sctconf.attributes_secondary"))
                    .__(printAttributesList(attributeDefManager.getAttributeDefList(ScrutariConstants.TECHNICAL_GROUP), "_ title.sctconf.attributes_technical"));
        }
        this
                .__(endPanel());
        return true;
    }

    private boolean printAttributesList(List<AttributeDef> attributeDefList, String titleMessageKey) {
        if (attributeDefList.isEmpty()) {
            return false;
        }
        this
                .H4()
                .__localize(titleMessageKey)
                ._H4()
                .__(startPanel())
                .__(AttributesPrint.printAttributesList(this, attributeDefList))
                .__(endPanel());
        return true;
    }


    private boolean printCategoriesDetails() {
        this
                .H3(HA.id(ConfConstants.CATEGORIES_NAME))
                .__localize("_ title.sctconf.categories")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.CATEGORIES_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.CATEGORIES_NAME))
                .__(CategoriesPrint.printCategories(this))
                .__(endPanel());
        return true;
    }

    private boolean printUriCodesDetails() {
        this
                .H3(HA.id(ConfConstants.URI_CODES_NAME))
                .__localize("_ title.sctconf.uricodes")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.URI_CODES_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.URI_CODES_NAME))
                .__(UriCodesPrint.printUriCodes(this))
                .__(endPanel());
        return true;
    }


    private boolean printOptionsDetails() {
        GlobalSearchOptions globalSearchOptions = engine.getScrutariSession().getGlobalSearchOptions();
        PertinencePonderation pertinencePonderation = globalSearchOptions.getPertinencePonderation();
        this
                .H3(HA.id(ConfConstants.OPTIONS_NAME))
                .__localize("_ title.sctconf.options")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.OPTIONS_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.OPTIONS_NAME))
                .H4()
                .__localize("_ title.sctconf.options_ponderation")
                ._H4()
                .__(startPanel())
                .TABLE()
                .__(printPonderatioRow("_ label.sctconf.ponderation_occurrence", pertinencePonderation.getOccurrencePonderation()))
                .__(printPonderatioRow("_ label.sctconf.ponderation_date", pertinencePonderation.getDatePonderation()))
                .__(printPonderatioRow("_ label.sctconf.ponderation_origin", pertinencePonderation.getOriginPonderation()))
                .__(printPonderatioRow("_ label.sctconf.ponderation_lang", pertinencePonderation.getLangPonderation()))
                ._TABLE()
                .__(endPanel())
                .H4()
                .__localize("_ title.sctconf.options_pertinenceorder")
                ._H4()
                .__(startPanel())
                .__(PertinenceOrderPrint.printPertinenceOrder(this))
                .__(endPanel())
                .__(endPanel());
        return true;
    }

    private boolean printValidationDetails() {
        this
                .H3(HA.id(ConfConstants.VALIDATION_NAME))
                .__localize("_ title.sctconf.validation")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.VALIDATION_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.VALIDATION_NAME))
                .__(ValidationPrint.printValidator(this))
                .__(endPanel());
        return true;
    }

    private boolean printFieldsDetails() {
        this
                .H3(HA.id(ConfConstants.FIELDS_NAME))
                .__localize("_ title.sctconf.fields")
                .__escape(" = ")
                .__(printFileInfo(ConfConstants.FIELDS_NAME, false))
                ._H3()
                .__(startPanel())
                .__(printConfLog(ConfConstants.FIELDS_NAME))
                .__(FieldsPrint.printFieldVariantList(this))
                .__(endPanel());
        return true;
    }

    private boolean printPonderatioRow(String key, float f) {
        this
                .TR()
                .TD()
                .__localize(key)
                .__colon()
                .__nonBreakableSpace()
                ._TD()
                .TD()
                .__escape(String.valueOf(Math.round(f * 100)))
                .__escape('%')
                ._TD()
                ._TR();
        return true;
    }

    private boolean printRestartForm() {
        this
                .__(startCommandBox(RestartCommand.COMMANDNAME, RestartCommand.COMMANDKEY))
                .P()
                .__localize("_ label.admin.scrutaridbname")
                .__colon()
                .__space()
                .__escape(scrutariSession.getScrutariDB().getScrutariDBName())
                ._P()
                .P()
                .LABEL()
                .INPUT_checkbox(name(RestartCommand.FORCE_PARAMNAME).value("1"))
                .__space()
                .__localize("_ label.admin.forcerestart")
                ._LABEL()
                ._P()
                .__(endCommandBox("_ submit.admin.restart"));
        return true;
    }

    private boolean printConfLog(String name) {
        short fileState = confState.getFileState(name);
        switch (fileState) {
            case ConfConstants.FILE_STATE_UNKNOWN:
            case ConfConstants.FILE_MISSING:
            case ConfConstants.FILE_OK:
                return false;
        }
        String logContent = engine.getEngineStorage().getLogStorage().getLog(SctLogConstants.CONF_DIR, name + ".xml");
        if (logContent == null) {
            return false;
        }
        MessageSource messageSource = LogUtils.readMessageSource("log", logContent);
        if (!messageSource.isEmpty()) {
            this
                    .H4()
                    .__localize("_ title.admin.log")
                    ._H4();
            this
                    .UL();
            for (MessageSource.Entry entry : messageSource.getEntryList()) {
                String category = entry.getCategory();
                this
                        .LI()
                        .P();
                if (category.length() > 0) {
                    this
                            .CODE()
                            .__escape('[')
                            .__escape(category)
                            .__escape(']')
                            ._CODE()
                            .__space();
                }
                this
                        .__localize(entry.getMessage())
                        ._P()
                        ._LI();
            }
            this
                    ._UL();
        }
        return true;
    }

    private boolean printFileInfo(String name, boolean withLink) {
        short fileState = confState.getFileState(name);
        if (withLink) {
            this
                    .A(HA.href("#" + name));
        }
        this
                .__escape(name)
                .__escape(".xml");
        if (withLink) {
            this
                    ._A();
        }
        this
                .__space()
                .__escape('(')
                .SPAN(getSpanClass(fileState))
                .__localize(getFileStateMessageKey(fileState))
                ._SPAN()
                .__escape(')');
        return true;
    }

    private static String getSpanClass(short fileState) {
        switch (fileState) {
            case ConfConstants.FILE_STATE_UNKNOWN:
                return "admin-State_Neutral";
            case ConfConstants.FILE_MISSING:
                return "admin-State_Neutral";
            case ConfConstants.FILE_XML_MALFORMED:
                return "admin-State_Error";
            case ConfConstants.FILE_XML_ERROR:
                return "admin-State_Error";
            case ConfConstants.FILE_XML_WARNING:
                return "admin-State_Warning";
            case ConfConstants.FILE_OK:
                return "admin-State_Ok";
            default:
                throw new SwitchException("Unknown getFileState : " + fileState);
        }
    }

    private static String getFileStateMessageKey(short fileState) {
        switch (fileState) {
            case ConfConstants.FILE_STATE_UNKNOWN:
                return "_ state.file.unknownstate";
            case ConfConstants.FILE_MISSING:
                return "_ state.file.missing";
            case ConfConstants.FILE_XML_MALFORMED:
                return "_ state.file.xml_malformed";
            case ConfConstants.FILE_XML_ERROR:
                return "_ state.file.xml_error";
            case ConfConstants.FILE_XML_WARNING:
                return "_ state.file.xml_warning";
            case ConfConstants.FILE_OK:
                return "_ state.file.ok";
            default:
                throw new SwitchException("Unknown getFileState : " + fileState);
        }
    }


    private static class Increment {

        private final int max;
        private int value = 0;

        private Increment(int max) {
            this.max = max;
        }

        public boolean isReached() {
            return (value >= max);
        }

        public void next() {
            value++;
        }

    }

}
