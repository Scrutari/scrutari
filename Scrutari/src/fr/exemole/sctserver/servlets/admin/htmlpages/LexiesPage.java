/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.db.api.stats.LangStat;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public class LexiesPage extends AdminHtmlPage {

    private final Lang lang;
    private List<ThesaurusData> thesaurusDataList;

    public LexiesPage(SctEngine engine, Lang workingLang, Lang lang) {
        super(engine, workingLang);
        this.lang = lang;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
        try (DataAccess dataAccess = scrutariDB.openDataAccess(); LexieAccess lexieAccess = scrutariDB.openLexieAccess()) {
            initResponse(response);
            start();
            this
                    .__(printTools())
                    .__(printMessage());
            if (lang == null) {
                this
                        .__(printLexieIndex());
            } else {
                this
                        .__(printLangResponse(response, dataAccess, lexieAccess));
            }
            end();
        }
    }

    private boolean printLexieIndex() throws IOException {
        this
                .__(startH2Panel("_ title.sctconf.lexies"))
                .P()
                .SPAN("admin-Info")
                .__localize("_ info.sctconf.lexies")
                ._SPAN()
                ._P();
        for (LangStat availableLangStat : scrutariSession.getScrutariDB().getStats().getMotcleEngineLangStats()) {
            String langString = availableLangStat.getLang().toString();
            this
                    .P()
                    .A(HA.href("lexies_" + langString + ".html"))
                    .__escape(langString)
                    ._A()
                    .__space()
                    .__escape('(')
                    .__escape("ods : ")
                    .A(HA.href("lexies_" + langString + ".ods"))
                    .__escape("_all")
                    ._A()
                    .__escape(')')
                    ._P();
        }
        this
                .__(endPanel());
        return true;
    }

    private boolean printLangResponse(HttpServletResponse response, DataAccess dataAccess, LexieAccess lexieAccess) throws IOException {
        thesaurusDataList = dataAccess.getThesaurusDataList();
        this
                .H2()
                .__localize("_ title.sctconf.lexies_lang", lang.toString())
                ._H2()
                .__(startPanel())
                .UL();
        for (ScrutariLexieUnit scrutariLexieUnit : lexieAccess.getLexieUnitMapByLang(lang).values()) {
            List<MotcleData> list = getMotcleDataList(scrutariLexieUnit.getLexieId(), dataAccess, lexieAccess);
            if (!list.isEmpty()) {
                this
                        .__(printLexieUnit(scrutariLexieUnit, list));
            }
        }
        this
                ._UL()
                .__(endPanel());
        return true;
    }

    private boolean printLexieUnit(ScrutariLexieUnit lexieUnit, List<MotcleData> list) {
        this
                .LI()
                .P()
                .__escape(lexieUnit.getCanonicalLexie())
                ._P()
                .UL();
        for (MotcleData motcleData : list) {
            this
                    .LI();
            Label label = motcleData.getLabels().getLabel(lang);
            this
                    .__escape(label.getLabelString())
                    ._LI();
        }
        this
                ._UL()
                ._LI();
        return true;
    }

    private List<MotcleData> getMotcleDataList(LexieId lexieId, DataAccess dataAccess, LexieAccess lexieAccess) {
        final List<MotcleData> list = new ArrayList<MotcleData>();
        for (ThesaurusData thesaurusData : thesaurusDataList) {
            lexieAccess.checkLexieOccurrences(lexieId, thesaurusData.getThesaurusCode(), motcleCode -> {
                MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                list.add(motcleData);
                return false;
            }, null);
        }
        return list;
    }


}
