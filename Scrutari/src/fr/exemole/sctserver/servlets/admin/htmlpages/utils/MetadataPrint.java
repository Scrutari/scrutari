/* Scrutari - Copyright (c) 2016-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.api.EngineMetadata;
import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import java.net.URL;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Labels;
import net.mapeadores.util.text.Phrase;


/**
 *
 * @author Vincent Calame
 */
public final class MetadataPrint {

    private MetadataPrint() {

    }

    public static boolean printMetadata(AdminHtmlPage hp, EngineMetadata engineMetadata) {
        boolean done = false;
        if (MetadataPrint.printLabels(hp, "title", engineMetadata.getTitleLabels())) {
            done = true;
        }
        for (Phrase phrase : engineMetadata.getPhrases()) {
            if (MetadataPrint.printLabels(hp, "phrase=" + phrase.getName(), phrase)) {
                done = true;
            }
        }
        if (MetadataPrint.printUrl(hp, "icon", engineMetadata.getIcon())) {
            done = true;
        }
        if (MetadataPrint.printUrl(hp, "website", engineMetadata.getWebsite())) {
            done = true;
        }
        if (MetadataPrint.printAttributes(hp, engineMetadata.getAttributes())) {
            done = true;
        }
        if (!done) {
            hp
                    .P()
                    .SPAN("admin-Info")
                    .__localize("_ info.sctconf.none_metadata")
                    ._SPAN()
                    ._P();
        }
        return true;
    }

    public static boolean printLabels(AdminHtmlPage hp, String name, Labels labels) {
        if (labels.isEmpty()) {
            return false;
        }
        hp
                .P()
                .CODE()
                .__escape('[')
                .__escape(name)
                .__escape(']')
                ._CODE()
                ._P();
        hp
                .UL();
        for (Label label : labels) {
            hp
                    .LI()
                    .__escape(label.getLang())
                    .__escape(" = ")
                    .__escape(label.getLabelString())
                    ._LI();
        }
        hp
                ._UL();
        return true;
    }

    public static boolean printUrl(AdminHtmlPage hp, String name, URL url) {
        if (url == null) {
            return false;
        }
        hp
                .P()
                .CODE()
                .__escape(name)
                .__escape(" = ")
                .A(HA.href(url.toString()))
                .__escape(url.toString())
                ._A()
                ._CODE()
                ._P();
        return true;
    }

    public static boolean printAttributes(AdminHtmlPage hp, Attributes attributes) {
        if (attributes.isEmpty()) {
            return false;
        }
        for (Attribute attribute : attributes) {
            hp
                    .P()
                    .CODE()
                    .__escape(attribute.getAttributeKey())
                    .__escape(" = ");
            int valueLength = attribute.size();
            for (int j = 0; j < valueLength; j++) {
                if (j > 0) {
                    hp
                            .__escape(" ; ");
                }
                hp
                        .__escape(attribute.get(j));
            }
            hp
                    ._CODE()
                    ._P();
        }
        return true;
    }

}
