/* Scrutari - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.api.ScrutariSourceManager;
import fr.exemole.sctserver.api.collect.CollectResult;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import fr.exemole.sctserver.api.collect.ScrutariSourceDef;
import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import fr.exemole.sctserver.tools.collect.CollectUtils;
import java.util.List;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public final class SourcesPrint {

    private SourcesPrint() {
    }

    public static boolean printScrutariSourceList(AdminHtmlPage hp) {
        ScrutariSourceManager scrutariSourceManager = hp.getEngine().getScrutariSourceManager();
        List<ScrutariSource> scrutariSourceList = scrutariSourceManager.getScrutariSourceList();
        if (scrutariSourceList.isEmpty()) {
            hp
                    .P()
                    .SPAN("admin-Info")
                    .__localize("_ info.sctconf.none_sources")
                    ._SPAN()
                    ._P();
            return true;
        }
        hp.
                TABLE("admin-Sources");
        for (ScrutariSource scrutariSource : scrutariSourceList) {
            ScrutariSourceDef scrutariSourceDef = scrutariSource.getScrutariSourceDef();
            hp
                    .TR();
            hp
                    .TD()
                    .__escape(scrutariSourceDef.getName())
                    ._TD();
            hp
                    .TD();
            boolean next = false;
            for (ScrutariSourceDef.UrlEntry urlEntry : scrutariSourceDef.getUrlEntryList()) {
                if (next) {
                    hp
                            .BR();
                } else {
                    next = true;
                }
                String urlString = urlEntry.getUrl().toString();
                hp
                        .A(HA.href(urlString));
                int idx = urlString.indexOf("://");
                if (idx != -1) {
                    urlString = urlString.substring(idx + 3);
                }
                hp
                        .__escape(urlString)
                        ._A()
                        .__space()
                        .CODE()
                        .__escape('[');
                switch (urlEntry.getType()) {
                    case ScrutariSourceDef.INFO_TYPE:
                        hp
                                .__escape("info");
                        break;
                    case ScrutariSourceDef.SCRUTARIDATA_TYPE:
                        hp
                                .__escape("scrutaridata");
                        break;
                }
                hp
                        .__escape(']')
                        ._CODE();
            }
            hp
                    ._TD();
            String state = scrutariSource.getState();
            hp
                    .TD()
                    .SPAN(getSpanClasses(state))
                    .__escape("[")
                    .__escape(state)
                    .__escape("] ")
                    .__localize(CollectUtils.getStateMessageKey(state))
                    ._SPAN()
                    ._TD();
            hp
                    .TD();
            FuzzyDate lastUpdate = scrutariSource.getLastUpdate();
            if (lastUpdate != null) {
                hp
                        .__escape(lastUpdate.toISOString());
            }
            hp
                    ._TD();
            hp
                    ._TR();
        }
        hp
                ._TABLE();
        return true;
    }

    private static String getSpanClasses(String state) {
        switch (state) {
            case CollectResult.OK_STATE:
                return "admin-DoneMessage";
            default:
                return "admin-ErrorMessage";
        }
    }

}
