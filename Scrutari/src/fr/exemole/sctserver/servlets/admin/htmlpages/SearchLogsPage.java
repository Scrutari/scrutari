/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlAttributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;


/**
 *
 * @author Vincent Calame
 */
public class SearchLogsPage extends AdminHtmlPage {

    public SearchLogsPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        FuzzyDate[] monthArray = engine.getEngineStorage().getLogStorage().getDateArray(FuzzyDate.MONTH_TYPE, null);
        start();
        this
                .__(printTools())
                .__(printMessage())
                .__(startH2Panel("_ title.admin.searchlog_month"));
        HtmlAttributes aAttr = HA.href("");
        for (FuzzyDate month : monthArray) {
            String monthString = month.toString();
            String href = "searchlog_" + monthString + ".html";
            this
                    .P()
                    .A(aAttr.href(href))
                    .__escape(monthString)
                    ._A()
                    ._P();
        }
        this
                .__(endPanel());
        end();
    }

}
