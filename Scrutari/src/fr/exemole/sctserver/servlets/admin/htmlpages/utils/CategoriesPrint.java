/* Scrutari - Copyright (c) 2009-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import java.util.List;
import net.mapeadores.util.text.Label;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;


/**
 *
 * @author Vincent Calame
 */
public final class CategoriesPrint {

    private CategoriesPrint() {
    }

    public static boolean printCategories(AdminHtmlPage hp) {
        try (DataAccess dataAccess = hp.getEngine().getScrutariSession().getScrutariDB().openDataAccess()) {
            GlobalSearchOptions globalSearchOptions = hp.getEngine().getScrutariSession().getGlobalSearchOptions();
            List<Category> categoryList = globalSearchOptions.getCategoryList();
            if (categoryList.isEmpty()) {
                hp
                        .P()
                        .SPAN("admin-Info")
                        .__localize("_ info.sctconf.none_categories")
                        ._SPAN()
                        ._P();
            } else {
                hp
                        .UL();
                for (Category category : categoryList) {
                    hp
                            .__(printCategoryLi(hp, category.getCategoryDef(), dataAccess));
                }
                hp
                        ._UL();
            }
        }
        return true;
    }

    private static boolean printCategoryLi(AdminHtmlPage hp, CategoryDef categoryDef, DataAccess dataAccess) {
        List<CorpusURI> corpusURIList = categoryDef.getCorpusURIList();
        String name = categoryDef.getName();
        hp
                .LI();
        hp
                .P()
                .CODE()
                .__escape('[')
                .__append(categoryDef.getRank())
                .__escape(" / ")
                .__escape(name)
                .__escape(']')
                ._CODE()
                .__escape(" - ");
        boolean next = false;
        for (Label label : categoryDef.getTitleLabels()) {
            if (next) {
                hp
                        .__escape(" / ");
            } else {
                next = true;
            }
            hp
                    .__escape(label.getLabelString());
        }
        hp
                ._P();
        hp
                .UL();
        if (corpusURIList.isEmpty()) {
            hp
                    .LI()
                    .P()
                    .EM();
            if (name.equals("_default")) {
                hp
                        .__localize("_ info.sctconf.defaultcorpusuri");
            } else {
                hp
                        .__localize("_ info.sctconf.none_corpusuri");
            }
            hp
                    ._EM()
                    ._P()
                    ._LI();
        } else {
            for (CorpusURI corpusURI : corpusURIList) {
                Integer corpusCode = dataAccess.getCode(corpusURI);
                boolean isHere = (corpusCode != null);
                hp
                        .LI();
                hp
                        .P();
                if (!isHere) {
                    hp
                            .SPAN("admin-State_Error");
                }
                hp
                        .CODE()
                        .__escape('[')
                        .__escape(corpusURI)
                        .__escape(']')
                        ._CODE();
                if (!isHere) {
                    hp
                            ._SPAN();
                }
                hp
                        ._P()
                        ._LI();
            }
        }
        hp
                ._UL();
        hp
                ._LI();
        return true;
    }

}
