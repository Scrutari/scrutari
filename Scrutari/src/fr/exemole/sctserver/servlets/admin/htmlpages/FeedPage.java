/* Scrutari - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.servlets.admin.SctJsLibCatalog;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.JsObject;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class FeedPage extends AdminHtmlPage {

    public FeedPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
        addJsLib(SctJsLibCatalog.FEED);
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        String clientId = generateId();
        JsObject args = JsObject.init()
                .put("lang", workingLang.toString())
                .put("clientId", clientId)
                .put("canonicalUrl", engine.getCanonicalUrl());
        start();
        this
                .SCRIPT()
                .__jsAssignObject("Feed.ARGS", args)
                ._SCRIPT()
                .__(printTools())
                .__(printMessage())
                .__(startH2Panel("_ title.admin.feedbuilder"))
                .DIV(HA.id(clientId))
                ._DIV()
                .__(endPanel());
        end();
    }


}
