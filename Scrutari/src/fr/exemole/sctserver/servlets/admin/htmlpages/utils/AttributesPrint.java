/* Scrutari - Copyright (c) 2012-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import java.util.List;
import net.mapeadores.util.attr.AttributeDef;
import net.mapeadores.util.text.Label;


/**
 *
 * @author Vincent Calame
 */
public final class AttributesPrint {

    private AttributesPrint() {
    }

    public static boolean printAttributesList(AdminHtmlPage hp, List<AttributeDef> attributeDefList) {
        hp
                .UL();
        for (AttributeDef attributeDef : attributeDefList) {
            hp
                    .__(printAttributeDefLi(hp, attributeDef));
        }
        hp
                ._UL();
        return true;
    }

    private static boolean printAttributeDefLi(AdminHtmlPage hp, AttributeDef attributeDef) {
        hp
                .LI()
                .P()
                .CODE()
                .__escape('[')
                .__escape(attributeDef.getAttributeKey())
                .__escape(']')
                ._CODE()
                .__escape(" - ");
        boolean next = false;
        for (Label label : attributeDef.getTitleLabels()) {
            if (next) {
                hp
                        .__escape(" / ");
            } else {
                next = true;
            }
            hp
                    .__escape(label.getLabelString());
        }
        hp
                ._P()
                ._LI();
        return true;
    }

}
