/* Scrutari - Copyright (c) 2008-2018 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;


/**
 *
 * @author Vincent Calame
 */
public class FichesPage extends AdminHtmlPage {

    private final ScrutariDB scrutariDB;

    public FichesPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
        this.scrutariDB = engine.getScrutariSession().getScrutariDB();
        addThemeCss("list.css");
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            initResponse(response);
            start();
            this
                    .__(printTools())
                    .__(printMessage())
                    .__(startH2Panel("_ title.sctconf.fiches"));
            for (BaseData baseData : dataAccess.getBaseDataList()) {
                printBaseData(baseData, dataAccess);
            }
            this
                    .__(endPanel());
            end();
        }
    }

    private void printBaseData(BaseData baseData, DataAccess dataAccess) {
        this
                .DETAILS()
                .SUMMARY("list-Base")
                .__escape(LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_LONGTITLE, workingLang, "?"))
                .__space()
                .CODE()
                .__escape('[')
                .__append(baseData.getCorpusCodeList().size())
                .__escape(']')
                ._CODE()
                ._SUMMARY();
        this
                .UL("list-Corpus");
        for (Integer corpusCode : baseData.getCorpusCodeList()) {
            CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
            printCorpusData(corpusData, dataAccess);
        }
        this
                ._UL()
                ._DETAILS();
    }

    private void printCorpusData(CorpusData corpusData, DataAccess dataAccess) {
        this
                .LI()
                .DETAILS()
                .SUMMARY()
                .__escape(LabelUtils.seekLabelString(corpusData.getPhrases(), DataConstants.CORPUS_TITLE, workingLang, "?"))
                .__space()
                .CODE()
                .__escape('[')
                .__append(corpusData.getFicheCodeList().size())
                .__escape(']')
                ._CODE()
                ._SUMMARY();
        this
                .UL("list-Fiches");
        for (Integer ficheCode : corpusData.getFicheCodeList()) {
            FicheData ficheData = dataAccess.getFicheData(ficheCode);
            printFicheData(ficheData);
        }
        this
                ._UL()
                ._DETAILS();
    }

    private void printFicheData(FicheData ficheData) {
        this
                .LI("list-Fiche")
                .__escape(ficheData.getTitre())
                ._LI();
    }

}
