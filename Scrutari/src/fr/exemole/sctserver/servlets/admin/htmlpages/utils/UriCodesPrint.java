/* Scrutari - Copyright (c) 2011-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import net.scrutari.db.api.CodeMatch;


/**
 *
 * @author Vincent Calame
 */
public final class UriCodesPrint {

    private UriCodesPrint() {
    }

    public static boolean printUriCodes(AdminHtmlPage hp) {
        CodeMatch[] initialCodeMatchArray = hp.getEngine().getEngineStorage().getEngineConf().getInitialCodeMatchArray(null);
        if (initialCodeMatchArray == null) {
            hp
                    .P()
                    .SPAN("admin-Info")
                    .__localize("_ info.sctconf.none_uricodes")
                    ._SPAN()
                    ._P();
        } else {
            hp
                    .UL();
            for (CodeMatch codeMatch : initialCodeMatchArray) {
                hp
                        .LI()
                        .CODE()
                        .__append(codeMatch.getCode())
                        .__escape(" = ")
                        .__escape(codeMatch.getScrutariDataURI().toString())
                        ._CODE()
                        ._LI();
            }
            hp
                    ._UL();
        }
        return true;
    }

}
