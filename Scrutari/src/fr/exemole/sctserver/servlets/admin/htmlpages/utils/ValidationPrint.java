/* Scrutari - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import java.util.List;
import net.scrutari.data.DataFieldKey;
import net.scrutari.data.DataValidator;


/**
 *
 * @author Vincent Calame
 */
public final class ValidationPrint {

    private ValidationPrint() {

    }

    public static boolean printValidator(AdminHtmlPage hp) {
        DataValidator dataValidator = hp.getEngine().getEngineStorage().getEngineConf().getDataValidator();
        boolean done = false;
        if (dataValidator != null) {
            List<DataFieldKey> mandatoryList = dataValidator.getDataFieldKeyList(DataValidator.MANDATORY_VALIDATION);
            if (!mandatoryList.isEmpty()) {
                hp
                        .P()
                        .__localize("_ label.sctconf.validation_mandatory")
                        .__colon()
                        .__space();
                append(hp, mandatoryList);
                hp
                        ._P();
                done = true;
            }
            List<DataFieldKey> lengthList = dataValidator.getDataFieldKeyList(DataValidator.LENGTH_VALIDATION);
            if (!lengthList.isEmpty()) {
                hp
                        .P()
                        .__localize("_ label.sctconf.validation_length")
                        .__colon()
                        .__space();
                append(hp, lengthList);
                hp
                        ._P();
                done = true;
            }
            List<DataFieldKey> uniqueList = dataValidator.getDataFieldKeyList(DataValidator.UNIQUE_VALIDATION);
            if (!uniqueList.isEmpty()) {
                hp
                        .P()
                        .__localize("_ label.sctconf.validation_unique")
                        .__colon()
                        .__space();
                append(hp, uniqueList);
                hp
                        ._P();
                done = true;
            }
            List<DataFieldKey> formatList = dataValidator.getDataFieldKeyList(DataValidator.FORMAT_VALIDATION);
            if (!formatList.isEmpty()) {
                hp
                        .P()
                        .__localize("_ label.sctconf.validation_format")
                        .__colon()
                        .__space();
                append(hp, formatList);
                hp
                        ._P();
                done = true;
            }
            List<DataFieldKey> splitList = dataValidator.getDataFieldKeyList(DataValidator.SPLIT_VALIDATION);
            if (!splitList.isEmpty()) {
                hp
                        .P()
                        .__localize("_ label.sctconf.validation_split")
                        .__colon()
                        .__space();
                append(hp, splitList);
                hp
                        ._P();
                done = true;
            }
        }
        if (!done) {
            hp
                    .P()
                    .SPAN("admin-Info")
                    .__localize("_ info.sctconf.none_validation")
                    ._SPAN()
                    ._P();
        }
        return true;
    }

    private static void append(AdminHtmlPage hp, List<DataFieldKey> list) {
        boolean next = false;
        for (DataFieldKey dataFieldKey : list) {
            if (next) {
                hp.__escape(", ");
            } else {
                next = true;
            }
            String locKey = getLocKey(dataFieldKey);
            if (locKey != null) {
                hp.__localize(locKey);
            } else {
                hp.__escape(dataFieldKey.toString());
            }
        }
    }

    private static String getLocKey(DataFieldKey dataFieldKey) {
        if (dataFieldKey.equals(DataFieldKey.GEOLOC)) {
            return "_ label.field.geoloc";
        } else if (dataFieldKey.equals(DataFieldKey.SOUSTITRE)) {
            return "_ label.field.soustitre";
        } else {
            return null;
        }
    }

}
