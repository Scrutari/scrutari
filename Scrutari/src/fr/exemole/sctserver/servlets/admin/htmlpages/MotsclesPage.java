/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.ScrutariDB;


/**
 *
 * @author Vincent Calame
 */
public class MotsclesPage extends AdminHtmlPage {

    private final ScrutariDB scrutariDB;

    public MotsclesPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
        scrutariDB = scrutariSession.getScrutariDB();
        addThemeCss("list.css");
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        try (DataAccess dataAccess = scrutariDB.openDataAccess()) {
            initResponse(response);
            start();
            this
                    .__(printTools())
                    .__(printMessage())
                    .__(startH2Panel("_ title.sctconf.motscles"));
            this
                    .P()
                    .__escape("ods : ")
                    .A(HA.href("motscles.ods"))
                    .__escape("_all")
                    ._A()
                    ._P();
            for (BaseData baseData : dataAccess.getBaseDataList()) {
                printBaseData(baseData, dataAccess);
            }
            this
                    .__(endPanel());
            end();
        }
    }

    private void printBaseData(BaseData baseData, DataAccess dataAccess) {
        this
                .DETAILS()
                .SUMMARY("list-Base")
                .__escape(LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_LONGTITLE, workingLang, "?"))
                .__space()
                .CODE()
                .__escape('[')
                .__append(baseData.getCorpusCodeList().size())
                .__escape(']')
                ._CODE()
                ._SUMMARY();
        this
                .UL("list-Thesaurus");
        for (Integer thesaurusCode : baseData.getThesaurusCodeList()) {
            ThesaurusData thesaurusData = dataAccess.getThesaurusData(thesaurusCode);
            printThesaurusData(thesaurusData, dataAccess);
        }
        this
                ._UL()
                ._DETAILS();
    }

    private void printThesaurusData(ThesaurusData thesaurusData, DataAccess dataAccess) {
        List<Integer> codeList = thesaurusData.getMotcleCodeList();
        List<MotcleData> motcleDataList = new ArrayList<MotcleData>();
        SortedMap<String, Lang> langMap = new TreeMap<String, Lang>();
        for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
            MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
            motcleDataList.add(motcleData);
            for (Label label : motcleData.getLabels()) {
                Lang lang = label.getLang();
                langMap.put(lang.toString(), lang);
            }
        }
        Lang[] langArray = langMap.values().toArray(new Lang[langMap.size()]);
        this
                .LI()
                .DETAILS()
                .SUMMARY()
                .__escape(LabelUtils.seekLabelString(thesaurusData.getPhrases(), DataConstants.THESAURUS_TITLE, workingLang, "?"))
                .__space()
                .CODE()
                .__escape('[')
                .__append(codeList.size())
                .__escape(']')
                ._CODE()
                ._SUMMARY();
        this
                .TABLE("list-Table");
        this
                .THEAD();
        this
                .TR();
        for (Lang lang : langArray) {
            this
                    .TH()
                    .__escape(lang)
                    ._TH();
        }
        this
                ._TR();
        this
                ._THEAD();
        this
                .TBODY();
        for (MotcleData motcleData : motcleDataList) {
            printMotcleData(motcleData, langArray);
        }
        this
                ._TBODY()
                ._TABLE()
                ._DETAILS()
                ._LI();

    }

    private void printMotcleData(MotcleData motcleData, Lang[] langArray) {
        this
                .TR();
        for (Lang lang : langArray) {
            this
                    .TD();
            Label label = motcleData.getLabels().getLabel(lang);
            if (label != null) {
                this
                        .__escape(label.getLabelString());
            }
            this
                    ._TD();
        }
        this
                ._TR();
    }

}
