/* Scrutari - Copyright (c) 2017-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages.utils;

import fr.exemole.sctserver.api.FieldVariantManager;
import fr.exemole.sctserver.api.fieldvariant.FieldVariant;
import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import fr.exemole.sctserver.tools.fieldvariant.FieldVariantUtils;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


/**
 *
 * @author Vincent Calame
 */
public final class FieldsPrint {

    private FieldsPrint() {

    }

    public static boolean printFieldVariantList(AdminHtmlPage hp) {
        FieldVariantManager fieldVariantManager = hp.getEngine().getFieldVariantManager();
        Set<String> defaultSet = new LinkedHashSet<String>();
        defaultSet.add(FieldVariantManager.QUERY_VARIANT);
        defaultSet.add(FieldVariantManager.DATA_VARIANT);
        defaultSet.add(FieldVariantManager.GEO_VARIANT);
        defaultSet.add(FieldVariantManager.EMPTY_VARIANT);
        List<FieldVariant> fieldVariantList = fieldVariantManager.getFieldVariantList(false);
        if (fieldVariantList.isEmpty()) {
            hp
                    .P()
                    .SPAN("admin-Info")
                    .__localize("_ info.sctconf.none_fields")
                    ._SPAN()
                    ._P();
        } else {
            hp
                    .UL();
            for (FieldVariant fieldVariant : fieldVariantList) {
                defaultSet.remove(fieldVariant.getName());
                printFieldVariant(hp, fieldVariant);
            }
            hp
                    ._UL();
        }
        if (!defaultSet.isEmpty()) {
            hp
                    .H4()
                    .__localize("_ title.sctconf.defaultfieldvariants")
                    ._H4()
                    .UL();
            for (String name : defaultSet) {
                printFieldVariant(hp, fieldVariantManager.getFieldVariant(name));
            }
            hp
                    ._UL();
        }
        return true;
    }

    private static boolean printFieldVariant(AdminHtmlPage hp, FieldVariant fieldVariant) {
        hp
                .LI()
                .P()
                .CODE()
                .__escape('[')
                .__escape(fieldVariant.getName())
                .__escape(']')
                ._CODE()
                ._P()
                .UL()
                .__(printDetail(hp, "_ label.stats.fiches", FieldVariantUtils.ficheFieldsToString(fieldVariant), fieldVariant.getFicheAliasList()))
                .__(printDetail(hp, "_ label.stats.motscles", FieldVariantUtils.motcleFieldsToString(fieldVariant), FieldVariantUtils.EMPTY_ALIASLIST))
                ._UL()
                ._LI();
        return true;
    }

    private static boolean printDetail(AdminHtmlPage hp, String key, String content, List<FieldVariant.Alias> aliasList) {
        hp
                .LI()
                .DIV()
                .__localize(key)
                .__colon()
                .CODE()
                .__escape(content)
                ._CODE()
                ._DIV()
                .__(printAliases(hp, aliasList))
                ._LI();
        return true;
    }

    private static boolean printAliases(AdminHtmlPage hp, List<FieldVariant.Alias> aliasList) {
        if (aliasList.isEmpty()) {
            return false;
        }
        hp
                .DIV()
                .__localize("_ label.sctconf.alias")
                .__colon()
                ._DIV()
                .UL();
        for (FieldVariant.Alias alias : aliasList) {
            hp
                    .LI()
                    .CODE()
                    .__escape(alias.getName())
                    ._CODE()
                    .__escape(" = ")
                    .CODE()
                    .__escape(FieldVariantUtils.aliasFieldsToString(alias))
                    ._CODE()
                    .__space()
                    .__escape("(\"")
                    .__escape(alias.getSeparator())
                    .__escape("\")")
                    ._LI();
        }
        hp
                ._UL()
                ._LI();
        return true;
    }

}
