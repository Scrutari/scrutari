/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class ErrorPage extends AdminHtmlPage {

    public ErrorPage(SctEngine engine, Lang workingLang) {
        super(engine, workingLang);
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        start();
        this
                .__(printTools())
                .__(printMessage());
        end();
    }

}
