/* Scrutari - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.htmlpages;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.servlets.admin.AdminConstants;
import fr.exemole.sctserver.servlets.admin.AdminResponseHandlerFactory;
import fr.exemole.sctserver.servlets.admin.SctJsLibManager;
import fr.exemole.sctserver.tools.EngineUtils;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlConstants;
import net.mapeadores.util.html.HtmlPrinterUtils;
import net.mapeadores.util.html.PageHtmlPrinter;
import net.mapeadores.util.jslib.JsLib;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.mimetype.MimeTypeUtils;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public abstract class AdminHtmlPage extends PageHtmlPrinter implements ResponseHandler {

    protected final SctEngine engine;
    protected final ScrutariSession scrutariSession;
    protected final Lang workingLang;
    protected final String title;
    private final Set<String> themeCssPathSet = new LinkedHashSet<String>();
    private SctJsLibManager jsLibManager = null;
    private CommandMessage commandMessage;
    private short currentPage;

    public AdminHtmlPage(SctEngine engine, Lang workingLang) {
        this.engine = engine;
        this.scrutariSession = engine.getScrutariSession();
        this.workingLang = workingLang;
        addMessageLocalisation(EngineUtils.getMessageLocalisation(engine, workingLang));
        String customTitle = engine.getEngineMetadata().getTitleLabels().seekLabelString(workingLang, null);
        if (customTitle != null) {
            title = " – " + customTitle;
        } else {
            title = "";
        }
    }

    public final void addThemeCss(String... cssThemeFile) {
        for (String value : cssThemeFile) {
            themeCssPathSet.add("theme/css/" + value);
        }
    }

    public void setCurrentPage(short currentPage) {
        this.currentPage = currentPage;
    }

    public void setCommandMessage(CommandMessage commandMessage) {
        this.commandMessage = commandMessage;
    }

    public final void addJsLib(JsLib jsLib) {
        testJsLibManager();
        jsLibManager.addJsLib(jsLib);
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    protected void initResponse(HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        setIcons();
        resolveJavascript();
        resolveCss();
        initPrinter(response.getWriter());
    }

    protected void setIcons() {
        String resourcesPath = "../resources/";
        URL iconURL = engine.getEngineMetadata().getIcon();
        if (iconURL != null) {
            String path = iconURL.toString();
            addIcon(iconURL.toString(), MimeTypeUtils.getMimeType(MimeTypeUtils.DEFAULT_RESOLVER, path), null, "icon");
        } else {
            addIconPng(resourcesPath + "theme/images/icon16.png", "16");
            addIconPng(resourcesPath + "theme/images/icon32.png", "32");
            addIconPng(resourcesPath + "theme/images/icon48.png", "48");
            addIconPng(resourcesPath + "theme/images/icon64.png", "64");
            addIcon(resourcesPath + "theme/images/touchicon64.png", "64x64", "image/png", "apple-touch-icon");
        }
    }

    protected void resolveJavascript() {
        if (jsLibManager != null) {
            String suffix = "";
            jsLibManager.resolve(this, workingLang, "../resources/", suffix);
        }
    }

    protected void resolveCss() {
        String resourcesPath = "../resources/";
        addCssUrl(resourcesPath + "theme/css/_default.css");
        addCssUrl(resourcesPath + "theme/css/admin.css");
        for (String path : themeCssPathSet) {
            addCssUrl(resourcesPath + path);
        }
    }

    protected boolean printTools() {
        this
                .H1()
                .__escape("Scrutari – ")
                .CODE()
                .__escape("[")
                .__escape(engine.getEngineName())
                .__escape("]")
                ._CODE()
                .__escape(title)
                ._H1();
        this
                .DIV("admin-Tool")
                .__(printToolLink(AdminConstants.INDEX_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.STATS_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.FICHES_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.MOTSCLES_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.LEXIES_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.SEARCHLOGS_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.FEEDS_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.CLIENT_PAGE))
                .__dash()
                .__(printToolLink(AdminConstants.QUERYBUILDER_PAGE))
                ._DIV();
        return true;
    }

    protected boolean printMessage() {
        if (commandMessage != null) {
            this
                    .DIV("admin-MessageArea")
                    .__(HtmlPrinterUtils.printCommandMessage(this, commandMessage, "admin-DoneMessage", "admin-ErrorMessage"))
                    ._DIV();
            return true;
        } else {
            return false;
        }
    }

    protected void start() {
        start(workingLang, "Scrutari – [" + engine.getEngineName() + "]" + title);
        this
                .MAIN();
    }

    @Override
    public void end() {
        if (jsLibManager != null) {
            jsLibManager.end(this, workingLang);
        }
        this
                ._MAIN();
        super.end();
    }

    protected boolean startPanel() {
        this
                .DIV("admin-Panel");
        return true;
    }

    protected boolean startH2Panel(String titleKey) {
        this
                .H2()
                .__localize(titleKey)
                ._H2()
                .DIV("admin-Panel");
        return true;
    }

    protected boolean endPanel() {
        this
                ._DIV();
        return true;
    }

    public boolean startCommandBox(String commandName, String titleLocKey) {
        this
                .SECTION("admin-command-Area")
                .FORM_post(HA.action("."))
                .INPUT_hidden("cmd", commandName)
                .HEADER("admin-command-Title")
                .__localize(titleLocKey)
                ._HEADER()
                .DIV("admin-command-Fields");
        return true;
    }

    public boolean endCommandBox(String submitMessageKey) {
        this
                .DIV("admin-command-Submit")
                .BUTTON(HA.type(HtmlConstants.SUBMIT_TYPE))
                .__localize(submitMessageKey)
                ._BUTTON()
                ._DIV()
                ._DIV()
                ._FORM()
                ._SECTION();
        return true;
    }

    private void testJsLibManager() {
        if (jsLibManager == null) {
            jsLibManager = new SctJsLibManager(engine.getJsAnalyser(), engine.getResourceStorages());
        }
    }

    private boolean printToolLink(short page) {
        String key = AdminResponseHandlerFactory.pageToString(page);
        if (currentPage == page) {
            this
                    .SPAN("admin-Current");
        } else {
            this
                    .A(HA.href(key + ".html"));
        }
        this
                .__localize(AdminResponseHandlerFactory.getPageMessageKey(page));
        if (currentPage == page) {
            this
                    ._SPAN();
        } else {
            this
                    ._A();
        }
        return true;
    }

    public SctEngine getEngine() {
        return engine;
    }

}
