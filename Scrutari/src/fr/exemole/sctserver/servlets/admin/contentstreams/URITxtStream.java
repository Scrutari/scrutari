/* Scrutari - Copyright (c) 2008-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.contentstreams;

import fr.exemole.sctserver.api.ScrutariSession;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.servlets.ResponseHandler;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.db.api.DataAccess;


/**
 *
 * @author Vincent Calame
 */
public class URITxtStream implements ResponseHandler {

    private final ScrutariSession scrutariSession;

    public URITxtStream(ScrutariSession scrutariSession) {
        this.scrutariSession = scrutariSession;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType("text/plain;charset=UTF-8");
        PrintWriter pw = response.getWriter();
        try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            for (ThesaurusData thesaurusData : dataAccess.getThesaurusDataList()) {
                printURI(pw, thesaurusData.getThesaurusURI());
            }
        }
    }

    private void printURI(PrintWriter pw, ScrutariDataURI scrutariURI) {
        pw.print(scrutariURI.toString());
        pw.print("\n");
    }

}
