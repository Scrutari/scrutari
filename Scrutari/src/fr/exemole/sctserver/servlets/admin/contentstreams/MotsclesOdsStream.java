/* Scrutari - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.contentstreams;

import fr.exemole.sctserver.api.ScrutariSession;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.opendocument.io.OdSource;
import net.mapeadores.opendocument.io.OdXML;
import net.mapeadores.opendocument.io.OdZip;
import net.mapeadores.opendocument.io.OdZipEngine;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.MotcleInfo;


/**
 *
 * @author Vincent Calame
 */
public class MotsclesOdsStream implements ResponseHandler {

    private final ScrutariSession scrutariSession;
    private final Lang workingLang;

    public MotsclesOdsStream(ScrutariSession scrutariSession, Lang workingLang) {
        this.scrutariSession = scrutariSession;
        this.workingLang = workingLang;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.ODS);
        try (OutputStream os = response.getOutputStream()) {
            OdZipEngine.run(os, OdZip.spreadSheet()
                    .contentOdSource(new ContentOdSource())
            );
        }
    }


    private class ContentOdSource implements OdSource {

        private String currentBaseLibelle;
        private String currentThesaurusLibelle;

        private ContentOdSource() {
        }

        @Override
        public void writeStream(OutputStream outputStream) throws IOException {
            BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
                MotsclesXMLPart motsclesXMLPart = new MotsclesXMLPart(xmlWriter, dataAccess);
                motsclesXMLPart.start();
                motsclesXMLPart.addMotscles();
                motsclesXMLPart.end();
            }
            buf.flush();
        }


        private class MotsclesXMLPart extends XMLPart {

            private final DataAccess dataAccess;
            private final Lang[] motcleLangArray;

            private MotsclesXMLPart(XMLWriter xmlWriter, DataAccess dataAccess) {
                super(xmlWriter);
                this.dataAccess = dataAccess;
                motcleLangArray = scrutariSession.getScrutariDB().getStats().getMotcleEngineLangStats().getLangArray();
            }

            private void start() throws IOException {
                OdXML.openDocumentContent(this);
                OdXML.openBody(this);
                OdXML.openSpreadsheet(this);
                OdXML.openTable(this, "motscles");
                addLibRow();
            }

            private void addLibRow() throws IOException {
                OdXML.openTableRow(this);
                for (Lang lang : motcleLangArray) {
                    OdXML.addStringTableCell(this, lang.toString());
                }
                OdXML.addStringTableCell(this, "occurrence");
                OdXML.addStringTableCell(this, "thesaurus");
                OdXML.addStringTableCell(this, "base");
                OdXML.closeTableRow(this);
            }

            private void addMotscles() throws IOException {
                for (BaseData baseData : dataAccess.getBaseDataList()) {
                    addBaseData(baseData);
                }
            }

            private void addBaseData(BaseData baseData) throws IOException {
                currentBaseLibelle = LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_TITLE, workingLang, "?");
                for (Integer thesaurusCode : baseData.getThesaurusCodeList()) {
                    ThesaurusData thesaurusData = dataAccess.getThesaurusData(thesaurusCode);
                    addThesaurusData(thesaurusData);
                }
            }

            private void addThesaurusData(ThesaurusData thesaurusData) throws IOException {
                currentThesaurusLibelle = LabelUtils.seekLabelString(thesaurusData.getPhrases(), DataConstants.THESAURUS_TITLE, workingLang, "?");
                for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
                    addMotcleData((MotcleInfo) dataAccess.getMotcleInfo(motcleCode), dataAccess.getMotcleData(motcleCode));
                }
            }

            private void addMotcleData(MotcleInfo motcleInfo, MotcleData motcleData) throws IOException {
                OdXML.openTableRow(this);
                for (Lang lang : motcleLangArray) {
                    Label label = motcleData.getLabels().getLabel(lang);
                    if (label != null) {
                        OdXML.addStringTableCell(this, label.getLabelString());
                    } else {
                        OdXML.addEmptyTableCell(this);
                    }
                }
                OdXML.addNumberTableCell(this, motcleInfo.getIndexationCount());
                OdXML.addStringTableCell(this, currentThesaurusLibelle);
                OdXML.addStringTableCell(this, currentBaseLibelle);
                OdXML.closeTableRow(this);
            }

            private void end() throws IOException {
                OdXML.closeTable(this);
                OdXML.closeSpreadsheet(this);
                OdXML.closeBody(this);
                OdXML.closeDocumentContent(this);
            }

        }

    }

}
