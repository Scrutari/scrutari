/* Scrutari - Copyright (c) 2008-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin.contentstreams;

import fr.exemole.sctserver.api.ScrutariSession;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.opendocument.io.OdSource;
import net.mapeadores.opendocument.io.OdXML;
import net.mapeadores.opendocument.io.OdZip;
import net.mapeadores.opendocument.io.OdZipEngine;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.LabelUtils;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import net.scrutari.data.BaseData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.LexieAccess;
import net.scrutari.db.api.MotcleInfo;
import net.scrutari.db.api.ScrutariDB;
import net.scrutari.lexie.LexieId;
import net.scrutari.lexie.ScrutariLexieUnit;


/**
 *
 * @author Vincent Calame
 */
public class LexiesOdsStream implements ResponseHandler {

    private final ScrutariSession scrutariSession;
    private final Lang workingLang;
    private final Lang labelLang;


    public LexiesOdsStream(ScrutariSession scrutariSession, Lang workingLang, Lang labelLang) {
        this.scrutariSession = scrutariSession;
        this.workingLang = workingLang;
        this.labelLang = labelLang;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.ODS);
        try (OutputStream os = response.getOutputStream()) {
            OdZipEngine.run(os, OdZip.spreadSheet()
                    .contentOdSource(new ContentOdSource())
            );
        }
    }


    private class ContentOdSource implements OdSource {

        private ContentOdSource() {
        }

        @Override
        public void writeStream(OutputStream outputStream) throws IOException {
            BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            ScrutariDB scrutariDB = scrutariSession.getScrutariDB();
            try (DataAccess dataAccess = scrutariDB.openDataAccess(); LexieAccess lexieAccess = scrutariDB.openLexieAccess()) {
                LexieXMLPart lexieXMLPart = new LexieXMLPart(xmlWriter, dataAccess, lexieAccess);
                lexieXMLPart.start();
                lexieXMLPart.addTable();
                lexieXMLPart.end();
            }
            buf.flush();
        }


        private class LexieXMLPart extends XMLPart {

            private final ThesaurusData[] thesaurusDataArray;
            private final String[] baseLibelle;
            private final String[] thesaurusLibelle;
            private final Integer[] thesaurusCodeArray;
            private final String[] name;
            private final int thesaurusLength;
            private final DataAccess dataAccess;
            private final LexieAccess lexieAccess;
            private final Map<String, LexieBundle> bundleMap = new TreeMap<String, LexieBundle>();

            private LexieXMLPart(XMLWriter xmlWriter, DataAccess dataAccess, LexieAccess lexieAccess) {
                super(xmlWriter);
                this.dataAccess = dataAccess;
                this.lexieAccess = lexieAccess;
                List<ThesaurusData> thesaurusDataList = dataAccess.getThesaurusDataList();
                this.thesaurusLength = thesaurusDataList.size();
                this.thesaurusDataArray = new ThesaurusData[thesaurusLength];
                this.name = new String[thesaurusLength];
                this.thesaurusLibelle = new String[thesaurusLength];
                this.baseLibelle = new String[thesaurusLength];
                this.thesaurusCodeArray = new Integer[thesaurusLength];
                for (int i = 0; i < thesaurusLength; i++) {
                    ThesaurusData thData = thesaurusDataList.get(i);
                    ThesaurusURI thesaurusURI = thData.getThesaurusURI();
                    thesaurusDataArray[i] = thData;
                    thesaurusCodeArray[i] = thData.getThesaurusCode();
                    name[i] = thesaurusURI.getThesaurusName();
                    thesaurusLibelle[i] = LabelUtils.seekLabelString(thData.getPhrases(), DataConstants.THESAURUS_TITLE, workingLang, "?");
                    BaseData baseData = dataAccess.getBaseData(thData.getBaseCode());
                    baseLibelle[i] = LabelUtils.seekLabelString(baseData.getPhrases(), DataConstants.BASE_TITLE, workingLang, "?");
                }
                initBundleMap();
            }

            private void initBundleMap() {
                for (ScrutariLexieUnit scrutariLexieUnit : lexieAccess.getLexieUnitMapByLang(labelLang).values()) {
                    initLexieBundle(scrutariLexieUnit);
                }
            }

            private void initLexieBundle(ScrutariLexieUnit lexieUnit) {
                String canonicalLexie = lexieUnit.getCanonicalLexie();
                final LexieBundle lexieBundle = new LexieBundle(canonicalLexie);
                LexieId lexieId = lexieUnit.getLexieId();
                for (int i = 0; i < thesaurusLength; i++) {
                    Integer thesaurusCode = thesaurusCodeArray[i];
                    final int index = i;
                    lexieAccess.checkLexieOccurrences(lexieId, thesaurusCode, motcleCode -> {
                        MotcleInfo motcleInfo = (MotcleInfo) dataAccess.getMotcleInfo(motcleCode);
                        MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                        Label label = motcleData.getLabels().getLabel(labelLang);
                        lexieBundle.add(index, label.getLabelString(), motcleInfo.getIndexationCount());
                        return false;
                    }, null);
                }
                if (!lexieBundle.isEmpty()) {
                    bundleMap.put(lexieUnit.getCollatedLexie(), lexieBundle);
                }
            }

            private void start() throws IOException {
                OdXML.openDocumentContent(this);
                OdXML.openBody(this);
                OdXML.openSpreadsheet(this);
            }

            private void addTable() throws IOException {
                OdXML.openTable(this, "lexies_" + labelLang.toString());
                for (LexieBundle lexieBundle : bundleMap.values()) {
                    addLexieBundle(lexieBundle);
                }
                OdXML.closeTable(this);
            }

            private void addLexieBundle(LexieBundle lexieBundle) throws IOException {
                String canonicalLexie = lexieBundle.getCanonicalLexie();
                boolean premier = true;
                for (MotcleBundle motcleBundle : lexieBundle.getMotcleBundleList()) {
                    addMotcleBundle(canonicalLexie, premier, motcleBundle);
                    premier = false;
                }
            }

            private void addMotcleBundle(String canonicalLexie, boolean premier, MotcleBundle motcleBundle) throws IOException {
                int thesaurusIndex = motcleBundle.getThesaurusIndex();
                OdXML.openTableRow(this);
                if (premier) {
                    OdXML.addStringTableCell(this, canonicalLexie);
                } else {
                    OdXML.addEmptyTableCell(this);
                }
                OdXML.addStringTableCell(this, canonicalLexie);
                OdXML.addStringTableCell(this, motcleBundle.getLibelleString());
                OdXML.addStringTableCell(this, thesaurusLibelle[thesaurusIndex]);
                OdXML.addStringTableCell(this, baseLibelle[thesaurusIndex]);
                OdXML.addNumberTableCell(this, motcleBundle.getCount());
                OdXML.closeTableRow(this);
            }

            private void end() throws IOException {
                OdXML.closeSpreadsheet(this);
                OdXML.closeBody(this);
                OdXML.closeDocumentContent(this);
            }

        }

    }


    private static class LexieBundle {

        private final String canonicalLexie;
        private final List<MotcleBundle> motcleBundleList = new ArrayList<MotcleBundle>();

        LexieBundle(String canonicalLexie) {
            this.canonicalLexie = canonicalLexie;
        }

        public boolean isEmpty() {
            return motcleBundleList.isEmpty();
        }

        public void add(int thesaurusIndex, String libelleString, int count) {
            MotcleBundle motcleBundle = new MotcleBundle();
            motcleBundle.setThesaurusIndex(thesaurusIndex);
            motcleBundle.setLibelleString(libelleString);
            motcleBundle.setCount(count);
            motcleBundleList.add(motcleBundle);
        }

        public List<MotcleBundle> getMotcleBundleList() {
            return motcleBundleList;
        }

        public String getCanonicalLexie() {
            return canonicalLexie;
        }

    }


    private static class MotcleBundle {

        private int count;
        private int thesaurusIndex;
        private String libelleString;

        MotcleBundle() {
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public int getThesaurusIndex() {
            return thesaurusIndex;
        }

        public void setThesaurusIndex(int thesaurusIndex) {
            this.thesaurusIndex = thesaurusIndex;
        }

        public String getLibelleString() {
            return libelleString;
        }

        public void setLibelleString(String libelleString) {
            this.libelleString = libelleString;
        }

    }

}
