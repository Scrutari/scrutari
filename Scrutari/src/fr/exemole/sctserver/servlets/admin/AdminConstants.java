/* Scrutari - Copyright (c) 2011-2017 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;


/**
 *
 * @author Vincent Calame
 */
public interface AdminConstants {

    public final static short INDEX_PAGE = 1;
    public final static short ERROR_PAGE = 2;
    public final static short STATS_PAGE = 3;
    public final static short FICHES_PAGE = 4;
    public final static short MOTSCLES_PAGE = 5;
    public final static short LEXIES_PAGE = 6;
    public final static short TEST_XMLDATA = 8;
    public final static short SEARCHLOGS_PAGE = 9;
    public final static short SEARCHLOG_PAGE = 10;
    public final static short CLIENT_PAGE = 11;
    public final static short FEEDS_PAGE = 12;
    public final static short QUERYBUILDER_PAGE = 13;
    public final static short LEXIES_STREAM = 51;
    public final static short URI_STREAM = 52;
    public final static short MOTSCLES_STREAM = 53;

}
