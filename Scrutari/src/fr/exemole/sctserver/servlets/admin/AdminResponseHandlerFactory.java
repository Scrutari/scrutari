/* Scrutari - Copyright (c) 2008-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.request.ParameterErrorMessageKeys;
import fr.exemole.sctserver.servlets.admin.contentstreams.LexiesOdsStream;
import fr.exemole.sctserver.servlets.admin.contentstreams.MotsclesOdsStream;
import fr.exemole.sctserver.servlets.admin.contentstreams.URITxtStream;
import fr.exemole.sctserver.servlets.admin.htmlpages.AdminHtmlPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.ClientPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.ErrorPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.FeedPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.FichesPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.IndexPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.LexiesPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.MotsclesPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.QueryBuilderPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.SearchLogPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.SearchLogsPage;
import fr.exemole.sctserver.servlets.admin.htmlpages.StatsPage;
import fr.exemole.sctserver.tools.EngineUtils;
import java.text.ParseException;
import net.mapeadores.util.exceptions.SwitchException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.request.RequestConstants;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class AdminResponseHandlerFactory {

    private final ScrutariSession scrutariSession;
    private final SctEngine engine;
    private boolean notFound = false;
    private short responseHandlerType;
    private Parameters parameters;
    private final Lang workingLang;
    private CommandMessage commandMessage;
    private FuzzyDate journalDate;

    private AdminResponseHandlerFactory(SctEngine engine, String path, RequestMap requestMap) {
        this.engine = engine;
        this.workingLang = EngineUtils.getDefaultLang(engine);
        this.scrutariSession = engine.getScrutariSession();
        if (requestMap.isTrue("reload")) {
            engine.getJsAnalyser().clearCache();
        }
        if (path.equals("")) {
            initAndDoCommand(requestMap);
            return;
        }
        responseHandlerType = getResponseHandlerType(path);
        if (responseHandlerType != -1) {
            return;
        }
        if (path.startsWith("motscles")) {
            parameters = new Parameters();
            if (!parameters.init(path.substring("motscles".length()), false)) {
                setNotFound();
                return;
            }
            String extension = parameters.getExtension();
            if (extension.equals("html")) {
                responseHandlerType = AdminConstants.MOTSCLES_PAGE;
            } else if (extension.equals("ods")) {
                responseHandlerType = AdminConstants.MOTSCLES_STREAM;
            } else {
                setNotFound();
            }
        } else if (path.startsWith("lexies_")) {
            parameters = new Parameters();
            if (!parameters.init(path.substring("lexies_".length()), true)) {
                setNotFound();
                return;
            }
            String extension = parameters.getExtension();
            if (extension.equals("html")) {
                responseHandlerType = AdminConstants.LEXIES_PAGE;
            } else if (extension.equals("ods")) {
                responseHandlerType = AdminConstants.LEXIES_STREAM;
            } else {
                setNotFound();
            }
        } else if (path.startsWith("searchlog_")) {
            JournalParameters journalParameters = new JournalParameters();
            if (!journalParameters.init(path.substring("searchlog_".length()))) {
                setNotFound();
                return;
            }
            String extension = journalParameters.getExtension();
            if (extension.equals("html")) {
                responseHandlerType = AdminConstants.SEARCHLOG_PAGE;
                journalDate = journalParameters.getDate();
            } else {
                setNotFound();
            }
        } else {
            setNotFound();
        }
    }

    public static ResponseHandler getHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        AdminResponseHandlerFactory factory = new AdminResponseHandlerFactory(engine, relativePath, requestMap);
        if (factory.notFound) {
            return null;
        } else {
            return factory.getResponseHandler();
        }
    }

    private ResponseHandler getResponseHandler() {
        switch (responseHandlerType) {
            case AdminConstants.LEXIES_STREAM:
                return new LexiesOdsStream(scrutariSession, workingLang, parameters.getPageLang());
            case AdminConstants.URI_STREAM:
                return new URITxtStream(scrutariSession);
            case AdminConstants.MOTSCLES_STREAM:
                return new MotsclesOdsStream(scrutariSession, workingLang);
            default:
                AdminHtmlPage page = getAdminHtmlPage();
                page.setCurrentPage(responseHandlerType);
                page.setCommandMessage(commandMessage);
                return page;

        }
    }

    private AdminHtmlPage getAdminHtmlPage() {
        switch (responseHandlerType) {
            case AdminConstants.INDEX_PAGE:
                return new IndexPage(engine, workingLang);
            case AdminConstants.ERROR_PAGE:
                return new ErrorPage(engine, workingLang);
            case AdminConstants.STATS_PAGE:
                return new StatsPage(engine, workingLang);
            case AdminConstants.FICHES_PAGE:
                return new FichesPage(engine, workingLang);
            case AdminConstants.MOTSCLES_PAGE:
                return new MotsclesPage(engine, workingLang);
            case AdminConstants.LEXIES_PAGE:
                return new LexiesPage(engine, workingLang, (parameters != null) ? parameters.getPageLang() : null);
            case AdminConstants.SEARCHLOGS_PAGE:
                return new SearchLogsPage(engine, workingLang);
            case AdminConstants.SEARCHLOG_PAGE:
                return new SearchLogPage(engine, workingLang, journalDate);
            case AdminConstants.CLIENT_PAGE:
                return new ClientPage(engine, workingLang);
            case AdminConstants.FEEDS_PAGE:
                return new FeedPage(engine, workingLang);
            case AdminConstants.QUERYBUILDER_PAGE:
                return new QueryBuilderPage(engine, workingLang);
            default:
                throw new SwitchException("responseHandlerType=" + responseHandlerType);
        }
    }

    private void initAndDoCommand(RequestMap requestMap) {
        String cmd = requestMap.getParameter(RequestConstants.COMMAND_PARAMETER);
        if (cmd == null) {
            responseHandlerType = AdminConstants.INDEX_PAGE;
        } else {
            Command command = Command.getCommandInstance(cmd, engine);
            if (command == null) {
                responseHandlerType = AdminConstants.ERROR_PAGE;
                commandMessage = LogUtils.error(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, RequestConstants.COMMAND_PARAMETER, cmd);
            } else {
                responseHandlerType = AdminConstants.INDEX_PAGE;
                commandMessage = command.doCommand(requestMap);
            }
        }
    }

    public static String pageToString(short pageType) {
        switch (pageType) {
            case AdminConstants.INDEX_PAGE:
                return "index";
            case AdminConstants.STATS_PAGE:
                return "stats";
            case AdminConstants.FICHES_PAGE:
                return "fiches";
            case AdminConstants.MOTSCLES_PAGE:
                return "motscles";
            case AdminConstants.LEXIES_PAGE:
                return "lexies";
            case AdminConstants.SEARCHLOGS_PAGE:
                return "searchlogs";
            case AdminConstants.FEEDS_PAGE:
                return "feeds";
            case AdminConstants.CLIENT_PAGE:
                return "client";
            case AdminConstants.QUERYBUILDER_PAGE:
                return "querybuilder";
            default:
                return null;
        }
    }

    public static String getPageMessageKey(short pageType) {
        switch (pageType) {
            case AdminConstants.INDEX_PAGE:
                return "_ link.admin.index";
            case AdminConstants.STATS_PAGE:
                return "_ link.admin.stats";
            case AdminConstants.FICHES_PAGE:
                return "_ link.admin.fiches";
            case AdminConstants.MOTSCLES_PAGE:
                return "_ link.admin.motscles";
            case AdminConstants.LEXIES_PAGE:
                return "_ link.admin.lexies";
            case AdminConstants.FEEDS_PAGE:
                return "_ link.admin.feeds";
            case AdminConstants.SEARCHLOGS_PAGE:
                return "_ link.admin.searchlogs";
            case AdminConstants.CLIENT_PAGE:
                return "_ link.admin.client";
            case AdminConstants.QUERYBUILDER_PAGE:
                return "_ link.admin.querybuilder";
            default:
                return null;
        }
    }

    private static short getResponseHandlerType(String path) {
        switch (path) {
            case "index.html":
                return AdminConstants.INDEX_PAGE;
            case "stats.html":
                return AdminConstants.STATS_PAGE;
            case "fiches.html":
                return AdminConstants.FICHES_PAGE;
            case "lexies.html":
                return AdminConstants.LEXIES_PAGE;
            case "uri.txt":
                return AdminConstants.URI_STREAM;
            case "searchlogs.html":
                return AdminConstants.SEARCHLOGS_PAGE;
            case "client.html":
            case "scrutarijs.html":
                return AdminConstants.CLIENT_PAGE;
            case "feeds.html":
                return AdminConstants.FEEDS_PAGE;
            case "querybuilder.html":
                return AdminConstants.QUERYBUILDER_PAGE;
            default:
                return -1;
        }
    }

    private void setNotFound() {
        notFound = true;
    }


    private class JournalParameters {

        private String extension;
        private FuzzyDate date;

        JournalParameters() {
        }

        boolean init(String pathInfo) {
            int idx = pathInfo.lastIndexOf(".");
            if (idx == -1) {
                return false;
            }
            extension = pathInfo.substring(idx + 1);
            pathInfo = pathInfo.substring(0, idx);
            try {
                date = FuzzyDate.parse(pathInfo);
            } catch (ParseException pe) {
                return false;
            }
            return true;
        }

        public String getExtension() {
            return extension;
        }

        public FuzzyDate getDate() {
            return date;
        }

    }


    private class Parameters {

        private String extension;
        private Lang pageLang;

        Parameters() {
        }

        boolean init(String pathInfo, boolean testLangue) {
            int idx = pathInfo.lastIndexOf(".");
            if (idx == -1) {
                return false;
            }
            extension = pathInfo.substring(idx + 1);
            pathInfo = pathInfo.substring(0, idx);
            if (testLangue) {
                try {
                    pageLang = Lang.parse(pathInfo);
                } catch (ParseException pe) {
                    return false;
                }
            }
            return true;
        }

        public String getExtension() {
            return extension;
        }

        public Lang getPageLang() {
            return pageLang;
        }

    }

}
