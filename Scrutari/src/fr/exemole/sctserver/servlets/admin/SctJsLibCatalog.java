/* Scrutari - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import net.mapeadores.util.jslib.JsLib;
import net.mapeadores.util.jslib.JsLibBuilder;
import net.mapeadores.util.jslib.TemplateFamilyBuilder;
import net.mapeadores.util.jslib.ThirdLib;
import net.mapeadores.util.jslib.ThirdLibBuilder;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class SctJsLibCatalog {

    public final static String JQUERY_THIRDLIBNAME = "jquery";
    public final static String JSRENDER_THIRDLIBNAME = "jsrender";
    public final static String SCRUTARIJS_THIRDLIBNAME = "scrutarijs";
    public final static ThirdLib JQUERY_THIRDLIB = ThirdLibBuilder.init(JQUERY_THIRDLIBNAME).toThirdLib();
    public final static ThirdLib JSRENDER_THIRDLIB = ThirdLibBuilder.init(JSRENDER_THIRDLIBNAME).toThirdLib();
    public final static JsLib CLIENT = JsLibBuilder.init()
            .addThirdLib(JQUERY_THIRDLIB)
            .addThirdLib(JSRENDER_THIRDLIB)
            .addThirdLib(ThirdLibBuilder.init(SCRUTARIJS_THIRDLIBNAME).toThirdLib())
            .addJsScript(RelativePath.build("js/client/Client.js"))
            .toJsLib();
    public final static JsLib FEED = JsLibBuilder.init()
            .addThirdLib(JQUERY_THIRDLIB)
            .addThirdLib(JSRENDER_THIRDLIB)
            .addJsScript(RelativePath.build("js/SCT.js"))
            .addJsScript(RelativePath.build("js/feed/Feed.js"))
            .addTemplateFamily(TemplateFamilyBuilder.init("feed").toTemplateFamily())
            .toJsLib();
    public final static JsLib QUERYBUILDER = JsLibBuilder.init()
            .addThirdLib(JQUERY_THIRDLIB)
            .addThirdLib(JSRENDER_THIRDLIB)
            .addJsScript(RelativePath.build("js/SCT.js"))
            .addJsScript(RelativePath.build("js/querybuilder/QueryBuilder.js"))
            .addJsScript(RelativePath.build("js/querybuilder/QueryBuilder.Type.js"))
            .addTemplateFamily(TemplateFamilyBuilder.init("querybuilder").toTemplateFamily())
            .toJsLib();


}
