/* Scrutari - Copyright (c) 2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import fr.exemole.sctserver.api.SctEngine;


/**
 *
 * @author Vincent Calame
 */
public class SourceCollectCommand extends Command {

    public final static String COMMANDNAME = "SourceCollect";
    public final static String COMMANDKEY = "_ SCT-03";
    public final static String SOURCE_PARAMNAME = "source";

    public SourceCollectCommand(SctEngine engine) {
        super(engine);
    }

    @Override
    protected void doCommand() {
        String sourceName = requestMap.getParameter(SOURCE_PARAMNAME);
        if (sourceName != null) {
            engine.getScrutariSourceManager().collect(sourceName, true);
        }
    }

}
