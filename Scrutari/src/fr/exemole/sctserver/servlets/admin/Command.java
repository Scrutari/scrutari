/* Scrutari - Copyright (c) 2008-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import fr.exemole.sctserver.api.SctEngine;
import net.mapeadores.util.logging.CommandMessage;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.request.RequestMap;


/**
 *
 * @author Vincent Calame
 */
public abstract class Command {

    protected SctEngine engine;
    protected RequestMap requestMap;
    private CommandMessage commandMessage;

    protected Command(SctEngine engine) {
        this.engine = engine;
    }

    public CommandMessage doCommand(RequestMap requestMap) {
        this.requestMap = requestMap;
        doCommand();
        return commandMessage;
    }

    protected void setDone(String doneMessageKey, Object... messageValues) {
        commandMessage = LogUtils.done(doneMessageKey, messageValues);
    }

    protected void setError(String errorMessageKey, Object... messageValues) {
        commandMessage = LogUtils.error(errorMessageKey, messageValues);
    }

    protected abstract void doCommand();

    public static Command getCommandInstance(String commandName, SctEngine engine) {
        if (commandName.equals(RestartCommand.COMMANDNAME)) {
            return new RestartCommand(engine);
        } else if (commandName.equals(ConfUpdateCommand.COMMANDNAME)) {
            return new ConfUpdateCommand(engine);
        } else if (commandName.equals(SourceCollectCommand.COMMANDNAME)) {
            return new SourceCollectCommand(engine);
        } else {
            return null;
        }
    }

}
