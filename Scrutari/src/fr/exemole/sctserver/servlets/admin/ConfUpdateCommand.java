/* Scrutari - Copyright (c) 2009-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import fr.exemole.sctserver.api.SctEngine;


/**
 *
 * @author Vincent Calame
 */
public class ConfUpdateCommand extends Command {

    public final static String COMMANDNAME = "ConfUpdate";
    public final static String COMMANDKEY = "_ SCT-03";

    public ConfUpdateCommand(SctEngine engine) {
        super(engine);
    }

    @Override
    protected void doCommand() {
        boolean done = engine.reloadConf();
        if (done) {
            setDone("_ done.admin.confupdate");
        } else {
            setError("_ error.reloadprocess");
        }
    }

}
