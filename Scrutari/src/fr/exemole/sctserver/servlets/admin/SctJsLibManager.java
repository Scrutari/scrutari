/* Scrutari - Copyright (c) 2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.admin;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.HtmlPrinter;
import net.mapeadores.util.html.PageHtmlPrinter;
import net.mapeadores.util.html.TrustedHtmlFactory;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.io.ResourceUtils;
import net.mapeadores.util.jslib.JsAnalyser;
import net.mapeadores.util.jslib.JsLibManager;
import net.mapeadores.util.jslib.TemplateFamily;
import net.mapeadores.util.jslib.ThirdLib;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LocalisationUtils;
import net.mapeadores.util.text.RelativePath;


/**
 *
 * @author Vincent Calame
 */
public class SctJsLibManager extends JsLibManager {

    private final Set<String> messageKeySet = new LinkedHashSet();
    private final JsAnalyser jsAnalyser;
    private final ResourceStorages resourceStorages;

    public SctJsLibManager(JsAnalyser jsAnalyser, ResourceStorages resourceStorages) {
        this.jsAnalyser = jsAnalyser;
        this.resourceStorages = resourceStorages;
    }


    public void resolve(PageHtmlPrinter htmlPrinter, Lang lang, String resourcePath, String suffix) {
        ThirdLib jqueryThirdLib = getThirdLib(SctJsLibCatalog.JQUERY_THIRDLIBNAME);
        if (jqueryThirdLib != null) {
            htmlPrinter
                    .addJsUrl(resourcePath + "third-lib/jquery/jquery.min.js" + suffix);
        }
        ThirdLib jsrenderThirdLib = getThirdLib(SctJsLibCatalog.JSRENDER_THIRDLIBNAME);
        if (jsrenderThirdLib != null) {
            htmlPrinter
                    .addJsUrl(resourcePath + "third-lib/jsrender/jsrender.min.js" + suffix);
        }
        ThirdLib scrutarijsThirdLib = getThirdLib(SctJsLibCatalog.SCRUTARIJS_THIRDLIBNAME);
        if (scrutarijsThirdLib != null) {
            Lang checkedLang = checkScrutarijsLang(lang);
            htmlPrinter
                    .addJsUrl(resourcePath + "third-lib/scrutarijs/l10n/" + checkedLang.toString() + ".js")
                    .addJsUrl(resourcePath + "third-lib/scrutarijs/frameworks/none.js")
                    .addJsUrl(resourcePath + "third-lib/scrutarijs/scrutarijs.js")
                    .addCssUrl(resourcePath + "third-lib/scrutarijs/scrutarijs.css")
                    .addCssUrl(resourcePath + "third-lib/scrutarijs/frameworks/none.css");
        }
        for (RelativePath relativePath : getScriptPaths()) {
            htmlPrinter
                    .addJsUrl(resourcePath + relativePath.toString() + suffix);
        }
    }

    public void end(HtmlPrinter hp, Lang workingLang) {
        for (RelativePath relativePath : getScriptPaths()) {
            messageKeySet.addAll(jsAnalyser.getJsMessageKeySet(relativePath));
        }
        if (messageKeySet.isEmpty()) {
            return;
        }
        hp
                .__(insertTemplates(hp));
        hp
                .SCRIPT()
                .__escape("SCT.Loc.add({")
                .__newLine();
        boolean next = false;
        for (String messageKey : messageKeySet) {
            String loc = hp.getMessageLocalisation().toString(messageKey);
            if (loc != null) {
                insertLoc(hp, messageKey, loc, next);
                next = true;
            }
        }
        hp
                .__newLine()
                .__escape("});")
                ._SCRIPT();
    }

    private boolean insertLoc(HtmlPrinter hp, String name, String value, boolean next) {
        if (next) {
            hp
                    .__escape(',')
                    .__newLine();
        }
        hp
                .__scriptLiteral(name)
                .__escape(':')
                .__scriptLiteral(value);
        return true;
    }

    private boolean insertTemplates(HtmlPrinter hp) {
        for (TemplateFamily templateFamily : getTemplates()) {
            String familyName = templateFamily.getName();
            RelativePath familyPath = getRelativePath(templateFamily);
            SortedMap<String, RelativePath> map = ResourceUtils.listResources(resourceStorages, familyPath, true);
            insertTemplates(hp, familyName, map);
        }
        return true;
    }

    private void insertTemplates(HtmlPrinter hp, String familyName, SortedMap<String, RelativePath> map) {
        for (Map.Entry<String, RelativePath> entry : map.entrySet()) {
            String name = entry.getKey();
            if (name.endsWith(".html")) {
                name = name.substring(0, name.length() - 5);
                String content = "";
                DocStream docStream = resourceStorages.getResourceDocStream(entry.getValue());
                if (docStream != null) {
                    content = docStream.getContent();
                    messageKeySet.addAll(LocalisationUtils.scanMessageKeys(content));
                }
                String templateName;
                if (familyName.isEmpty()) {
                    templateName = name;
                } else {
                    templateName = familyName + ":" + name;
                }
                hp
                        .SCRIPT(HA.attr("data-name", templateName).type("text/x-jsrender"))
                        .__append(TrustedHtmlFactory.UNCHECK.build(content))
                        ._SCRIPT();
            }
        }
    }

    private static Lang checkScrutarijsLang(Lang lang) {
        Lang rootLang = lang.getRootLang();
        switch (rootLang.toString()) {
            case "de":
            case "en":
            case "es":
            case "fr":
            case "it":
                return rootLang;
            default:
                return Lang.build("fr");
        }
    }

    private static RelativePath getRelativePath(TemplateFamily templateFamily) {
        String familyName = templateFamily.getName();
        return RelativePath.build("templates/" + familyName);
    }

}
