/* Scrutari - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.webapp.htmlpages;

import fr.exemole.sctserver.api.conf.ConfConstants;
import fr.exemole.sctserver.servlets.ScrutariWebapp;
import fr.exemole.sctserver.tools.EngineUtils;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.PageHtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public abstract class WebappHtmlPage extends PageHtmlPrinter implements ResponseHandler {

    protected final ScrutariWebapp scrutariWebapp;
    protected final Lang workingLang;

    public WebappHtmlPage(ScrutariWebapp scrutariWebapp, Lang workingLang) {
        this.scrutariWebapp = scrutariWebapp;
        this.workingLang = workingLang;
        addMessageLocalisation(EngineUtils.getMessageLocalisation(scrutariWebapp, workingLang));
        String resourcesPath = "_resources/";
        addIconPng(resourcesPath + "theme/images/icon16.png", "16");
        addIconPng(resourcesPath + "theme/images/icon32.png", "32");
        addIconPng(resourcesPath + "theme/images/icon48.png", "48");
        addIconPng(resourcesPath + "theme/images/icon64.png", "64");
        addIcon(resourcesPath + "theme/images/iconwhite64.png", "64x64", "image/png", "apple-touch-icon");
        addCssUrl(resourcesPath + "theme/css/_default.css");
        addCssUrl(resourcesPath + "theme/css/webapp.css");
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    protected void initResponse(HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        initPrinter(response.getWriter());

    }

    public void start() {
        start(workingLang, "Scrutari");
    }

    public static String getStateClass(short state) {
        switch (state) {
            case ConfConstants.GLOBAL_OK:
                return "webapp-State_ok";
            case ConfConstants.GLOBAL_CONFIO_ERROR:
                return "webapp-State_error";
            case ConfConstants.GLOBAL_UNDEFINED:
                return "webapp-State_undefined";
            default:
                return "webapp-State_inactive";

        }
    }

    public static String getStateL10nKey(short state) {
        switch (state) {
            case ConfConstants.GLOBAL_OK:
                return "_ state.global.ok";
            case ConfConstants.GLOBAL_CONFIO_ERROR:
                return "_ error.global.conf_io";
            case ConfConstants.GLOBAL_UNDEFINED:
                return "_ error.global.conf_null";
            default:
                return "_ state.global.inactive";

        }
    }

}
