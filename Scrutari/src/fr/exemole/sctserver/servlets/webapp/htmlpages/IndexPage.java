/* Scrutari - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.webapp.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.servlets.ScrutariWebapp;
import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.buildinfo.BuildInfo;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageLog;
import net.mapeadores.util.logging.MessageSource;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.text.DateFormatBundle;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class IndexPage extends WebappHtmlPage {

    private final String messageKey;

    public IndexPage(ScrutariWebapp scrutariWebapp, Lang workingLang, String messageKey) {
        super(scrutariWebapp, workingLang);
        this.messageKey = messageKey;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        start();
        this
                .MAIN();
        printLogo();
        printCanonicalUrl();
        if (!scrutariWebapp.isInit()) {
            printInitErrors();
        } else {
            printInfo();
            printConfLog();
            printEngineList();
        }
        printBuildInfo();
        printMemoryInfo();
        this
                ._MAIN();
        end();
    }

    private void printLogo() {
        this
                .IMG(HA.src("_resources/theme/images/logo.png").classes("webapp-Logo"));
    }

    private void printCanonicalUrl() {
        String canonicalUrl = scrutariWebapp.getWebappCanonicalUrl();
        if (canonicalUrl.length() > 0) {
            this
                    .H1("webapp-Title")
                    .__escape(canonicalUrl)
                    ._H1();
        }
    }

    private void printInfo() {
        String locKey = null;
        if (messageKey != null) {
            locKey = messageKey;
        } else if (areAllInactive()) {
            locKey = "_ info.webapp.congratulations";
        }
        if (locKey != null) {
            this
                    .DIV("webapp-Panel")
                    .P("webapp-Title")
                    .__localize(locKey)
                    ._P()
                    ._DIV();
        }
    }

    private void printInitErrors() {
        List<MessageSource> messageSourceList = scrutariWebapp.getInitMessageLog().getMessageSourceList();
        this
                .DIV("webapp-Panel")
                .P("webapp-ErrorTitle")
                .__localize("_ info.webapp.initerrors")
                ._P()
                .UL();
        for (MessageSource messageSource : messageSourceList) {
            this
                    .LI();
            this
                    .P()
                    .CODE()
                    .__escape('[')
                    .__escape(messageSource.getName())
                    .__escape(']')
                    ._CODE()
                    ._P();
            this
                    .UL();
            for (MessageSource.Entry entry : messageSource.getEntryList()) {
                this
                        .LI()
                        .P()
                        .__localize(entry.getMessage())
                        ._P()
                        ._LI();
            }
            this
                    ._UL();
            this
                    ._LI();
        }
        this
                ._UL()
                ._DIV();
    }

    private void printConfLog() {
        MessageLog messageLog = scrutariWebapp.getConfMessageLog();
        if (messageLog.isEmpty()) {
            return;
        }
        this
                .P("webapp-ErrorTitle")
                .__localize("_ info.webapp.conferrors")
                ._P();
        this
                .UL();
        for (MessageSource messageSource : messageLog.getMessageSourceList()) {
            this
                    .LI();
            this
                    .P()
                    .CODE()
                    .__escape('[')
                    .__escape(messageSource.getName())
                    .__escape(']')
                    ._CODE()
                    ._P();
            this
                    .UL();
            for (MessageSource.Entry entry : messageSource.getEntryList()) {
                String category = entry.getCategory();
                this
                        .LI();
                if (category.startsWith("critical.")) {
                    this
                            .P("webapp-Error");
                } else {
                    this
                            .P();
                }
                if (category.length() > 0) {
                    this
                            .CODE()
                            .__escape('[')
                            .__escape(category)
                            .__escape(']')
                            ._CODE()
                            .__space();
                }
                this
                        .__localize(entry.getMessage())
                        ._P()
                        ._LI();
            }
            this
                    ._UL();
            this
                    ._LI();
        }
        this
                ._UL();
    }

    private void printBuildInfo() {
        BuildInfo buildInfo = scrutariWebapp.getBuildInfo();
        if (buildInfo == null) {
            return;
        }
        String dateString = "";
        FuzzyDate date = buildInfo.getDate();
        if (date != null) {
            DateFormatBundle dateFormatBundle = DateFormatBundle.getDateFormatBundle(workingLang.toLocale());
            dateString = date.getDateLitteral(dateFormatBundle);
        }
        this
                .H2()
                .__localize("_ title.webapp.buildinfo")
                ._H2()
                .DIV("webapp-Panel")
                .TABLE()
                .__(printRow("_ label.webapp.version", buildInfo.getVersion()))
                .__(printRow("_ label.webapp.repository", buildInfo.getRepository()))
                .__(printRow("_ label.webapp.builddate", dateString))
                ._TABLE()
                ._DIV();

    }

    private void printMemoryInfo() {
        Runtime run = Runtime.getRuntime();
        this
                .H2()
                .__localize("_ title.webapp.memory")
                ._H2()
                .DIV("webapp-Panel")
                .TABLE()
                .__(printRow("_ label.webapp.memory_free", run.freeMemory() / (1024 * 1024)))
                .__(printRow("_ label.webapp.memory_total", run.totalMemory() / (1024 * 1024)))
                .__(printRow("_ label.webapp.memory_max", run.maxMemory() / (1024 * 1024)))
                ._TABLE()
                ._DIV();
    }

    private boolean printRow(String key, long count) {
        this
                .TR()
                .TD()
                .__localize(key)
                .__colon()
                .__nonBreakableSpace()
                ._TD()
                .TD()
                .__append(count)
                ._TD()
                ._TR();
        return true;
    }

    private boolean printRow(String key, String value) {
        this
                .TR()
                .TD()
                .__localize(key)
                .__colon()
                .__nonBreakableSpace()
                ._TD()
                .TD()
                .__escape(value)
                ._TD()
                ._TR();
        return true;
    }

    private void printEngineList() {
        List<String> nameList = scrutariWebapp.getEngineNameList();
        int size = nameList.size();
        this
                .H2();
        switch (size) {
            case 0:
                this
                        .__localize("_ info.webapp.engine_none");
                break;
            case 1:
                this
                        .__localize("_ info.webapp.engine_one");
                break;
            default:
                this
                        .__localize("_ info.webapp.engine_many", StringUtils.toString(size, workingLang.toLocale()));
        }
        this
                ._H2();
        this
                .DIV("webapp-Panel");
        if (size > 0) {
            this
                    .UL();
            for (String name : nameList) {
                String title = "";
                short state = -999;
                if (scrutariWebapp.isEngineInit(name)) {
                    SctEngine engine = scrutariWebapp.getEngine(name);
                    String engineTitle = engine.getEngineMetadata().getTitleLabels().seekLabelString(workingLang, null);
                    if (engineTitle != null) {
                        title = " – " + engineTitle;
                    }
                    state = engine.getConfState().getGlobalState();
                }
                this
                        .LI()
                        .P()
                        .A(HA.href(name + "/admin/"))
                        .CODE()
                        .__escape("[")
                        .__escape(name)
                        .__escape("]")
                        ._CODE()
                        .__escape(title)
                        ._A()
                        .__space()
                        .SMALL()
                        .SPAN(getStateClass(state))
                        .__escape("(")
                        .__localize(getStateL10nKey(state))
                        .__escape(")")
                        ._SPAN()
                        ._SMALL()
                        ._P()
                        ._LI();
            }
            this
                    ._UL();
        }
        this
                .DIV("webapp-Tool")
                .A(HA.href("_reload"))
                .__localize("_ link.global.reload")
                ._A()
                .__dash()
                .A(HA.href("_activeall"))
                .__localize("_ link.global.activeall")
                ._A()
                .__dash()
                .A(HA.href("_logs"))
                .__localize("_ link.global.logs")
                ._A()
                ._DIV();
        this
                ._DIV();
    }

    private boolean areAllInactive() {
        for (String name : scrutariWebapp.getEngineNameList()) {
            if (scrutariWebapp.isEngineInit(name)) {
                return false;
            }
        }
        return true;
    }


}
