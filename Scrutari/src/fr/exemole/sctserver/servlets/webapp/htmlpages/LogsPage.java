/* Scrutari - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.webapp.htmlpages;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.log.SctLogConstants;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.servlets.ScrutariWebapp;
import java.io.IOException;
import java.util.SortedSet;
import java.util.TreeSet;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.localisation.Lang;


/**
 *
 * @author Vincent Calame
 */
public class LogsPage extends WebappHtmlPage {

    public LogsPage(ScrutariWebapp scrutariWebapp, Lang workingLang) {
        super(scrutariWebapp, workingLang);
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        initResponse(response);
        start();
        this
                .TABLE("webapp-Table")
                .TR()
                .__(headerLoc("_ label.webapp.header_name"))
                .__(headerLoc("_ label.webapp.header_title"))
                .__(headerLoc("_ label.webapp.header_state"))
                .__(headerLoc("_ label.webapp.header_inittype"))
                .__(headerLoc("_ label.webapp.header_init"))
                .__(headerLoc("_ label.webapp.header_update"))
                .__(headerLoc("_ label.webapp.header_errors"))
                ._TR();
        for (String name : scrutariWebapp.getEngineNameList()) {
            this
                    .__(printEngineLog(name));
        }
        this
                ._TABLE();
        end();
    }

    private boolean printEngineLog(String name) {
        short state = -999;
        String title = "";
        if (scrutariWebapp.isEngineInit(name)) {
            SctEngine engine = scrutariWebapp.getEngine(name);
            title = engine.getEngineMetadata().getTitleLabels().seekLabelString(workingLang, "");
            state = engine.getConfState().getGlobalState();
        }
        this
                .TR()
                .TD()
                .A(HA.href(name + "/admin/"))
                .__escape(name)
                ._A()
                ._TD()
                .TD()
                .__escape(title)
                ._TD()
                .TD()
                .SPAN(getStateClass(state))
                .__localize(getStateL10nKey(state))
                ._SPAN()
                ._TD()
                .__(printLog(name))
                ._TR();
        return true;
    }

    private boolean headerLoc(String locKey) {
        this
                .TH()
                .__localize(locKey)
                ._TH();
        return true;
    }

    private boolean printLog(String engineName) {
        LogStorage logStorage = scrutariWebapp.getLogStorage(engineName);
        if (scrutariWebapp.isEngineInit(engineName)) {
            this
                    .__(printInitType(engineName, logStorage))
                    .__(printDateCell(engineName, logStorage, "init.txt"))
                    .__(printDateCell(engineName, logStorage, "update.txt"))
                    .__(printLogs(engineName, logStorage));
        } else {
            this
                    .TD();
            String running = logStorage.getLog(SctLogConstants.LOAD_DIR, "_running.txt");
            if (running != null) {
                this
                        .__localize("_ label.webapp.init_running");
            }
            this
                    ._TD();
            for (int i = 0; i < 3; i++) {
                this
                        .TD()
                        ._TD();
            }
        }
        return true;
    }

    private boolean printInitType(String engineName, LogStorage logStorage) {
        boolean done = false;
        String text = logStorage.getLog(SctLogConstants.LOAD_DIR, "init.txt");
        if (text != null) {
            int idx = text.indexOf('\n');
            if (idx > 0) {
                String firstLine = text.substring(0, idx);
                int tabIdx = firstLine.indexOf("\t");
                if (tabIdx > 0) {
                    String title = firstLine.substring(0, tabIdx);
                    int colonIdx = firstLine.indexOf("@");
                    if (colonIdx > 0) {
                        this
                                .TD()
                                .__escape(title.substring(0, colonIdx))
                                ._TD();
                        done = true;
                    }
                }
            }
        }
        if (!done) {
            this
                    .TD()
                    ._TD();
        }
        return true;
    }

    private boolean printDateCell(String engineName, LogStorage logStorage, String fileName) {
        String dirName = SctLogConstants.LOAD_DIR;
        String text = logStorage.getLog(dirName, fileName);
        boolean done = false;
        if (text != null) {
            int idx = text.indexOf('\n');
            if (idx > 0) {
                String firstLine = text.substring(0, idx);
                int tabIdx = firstLine.lastIndexOf("\t");
                if (tabIdx > 0) {
                    String date = firstLine.substring(tabIdx + 1);
                    this
                            .TD()
                            .A(HA.href(engineName + "/logs/" + dirName + "/" + fileName + ".html"))
                            .__escape(date)
                            ._A()
                            ._TD();
                    done = true;
                }
            }
        }
        if (!done) {
            this
                    .TD()
                    ._TD();
        }
        return true;
    }

    private boolean printLogs(String engineName, LogStorage logStorage) {
        boolean next = false;
        this
                .TD();
        String running = logStorage.getLog(SctLogConstants.LOAD_DIR, "_running.txt");
        if (running != null) {
            next = true;
            this
                    .A(HA.href(engineName + "/logs/" + SctLogConstants.LOAD_DIR + "/_running.txt"))
                    .__escape("_running.txt")
                    ._A();
        }
        next = printLogs(engineName, logStorage, SctLogConstants.EXCEPTION_DIR, next);
        next = printLogs(engineName, logStorage, SctLogConstants.DATA_DIR, next);
        this
                ._TD();
        return true;
    }

    private boolean printLogs(String engineName, LogStorage logStorage, String dirName, boolean next) {
        SortedSet<String> set = new TreeSet<String>(logStorage.getAvailableLogs(dirName));
        for (String log : set) {
            if (next) {
                __escape(", ");
            } else {
                next = true;
            }
            A(HA.href(engineName + "/logs/" + dirName + "/" + log));
            __escape(log);
            _A();
        }
        return next;
    }

}
