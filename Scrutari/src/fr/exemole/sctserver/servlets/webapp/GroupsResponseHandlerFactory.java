/* Scrutari - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.webapp;

import fr.exemole.sctserver.api.context.EngineGroup;
import fr.exemole.sctserver.request.ParameterErrorMessageKeys;
import fr.exemole.sctserver.request.Parameters;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.request.WarningHandler;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import fr.exemole.sctserver.request.json.EngineGroupJsonProducer;
import fr.exemole.sctserver.request.json.ErrorJsonProducer;
import fr.exemole.sctserver.servlets.ScrutariWebapp;
import fr.exemole.sctserver.tools.EngineUtils;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.servlets.handlers.JsonResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class GroupsResponseHandlerFactory {

    private GroupsResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(EngineGroup engineGroup, String relativePath, ScrutariWebapp scrutariWebapp, RequestMap requestMap) {
        if (relativePath.length() == 0) {
            return ServletUtils.wrap("AVAILABLE SCRUTARI GROUP [" + engineGroup.getName() + "]");
        } else if (relativePath.toLowerCase().equals("json")) {
            WarningHandler warningHandler = new WarningHandler();
            Lang workingLang = RequestMapUtils.getLang(requestMap, warningHandler);
            if (workingLang == null) {
                workingLang = scrutariWebapp.getWebappDefaultLang();
            }
            return getJsonHandler(engineGroup, scrutariWebapp, requestMap, workingLang);
        } else {
            return null;
        }
    }

    private static JsonResponseHandler getJsonHandler(EngineGroup engineGroup, ScrutariWebapp scrutariWebapp, RequestMap requestMap, Lang lang) {
        AbstractJsonProducer abstractJsonProducer;
        try {
            abstractJsonProducer = getJsonProducer(engineGroup, scrutariWebapp, requestMap, lang);
        } catch (ErrorMessageException eme) {
            abstractJsonProducer = new ErrorJsonProducer(eme.getErrorMessage(), EngineUtils.getMessageLocalisation(scrutariWebapp, lang));
        }
        return JsonResponseHandler.init(abstractJsonProducer, requestMap.getParameter("callback"))
                .accessControl(JsonResponseHandler.ALL_ORIGIN);
    }

    private static AbstractJsonProducer getJsonProducer(EngineGroup engineGroup, ScrutariWebapp scrutariWebapp, RequestMap requestMap, Lang lang) throws ErrorMessageException {
        String type = RequestMapUtils.getMandatoryParam(Parameters.TYPE, requestMap);
        String versionString = RequestMapUtils.getMandatoryParam(Parameters.VERSION, requestMap);
        int version;
        try {
            version = Integer.parseInt(versionString);
        } catch (NumberFormatException nfe) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.WRONG_PARAMETER_VALUE, Parameters.VERSION, versionString);
        }
        if (version < 0) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.VERSION, versionString);
        }
        switch (type) {
            case "enginegroup":
                return new EngineGroupJsonProducer(version, scrutariWebapp, engineGroup, lang, false);
            default:
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.TYPE, type);
        }
    }

}
