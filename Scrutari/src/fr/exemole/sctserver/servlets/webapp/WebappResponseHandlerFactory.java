/* Scrutari - Copyright (c) 2009-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.webapp;

import fr.exemole.sctserver.request.ParameterErrorMessageKeys;
import fr.exemole.sctserver.request.Parameters;
import fr.exemole.sctserver.request.RequestMapUtils;
import fr.exemole.sctserver.request.WarningHandler;
import fr.exemole.sctserver.request.json.AbstractJsonProducer;
import fr.exemole.sctserver.request.json.ErrorJsonProducer;
import fr.exemole.sctserver.request.json.JsonProducerFactory;
import fr.exemole.sctserver.servlets.ScrutariWebapp;
import fr.exemole.sctserver.servlets.webapp.htmlpages.IndexPage;
import fr.exemole.sctserver.servlets.webapp.htmlpages.LogsPage;
import fr.exemole.sctserver.tools.EngineUtils;
import java.util.List;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.ErrorMessageException;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.handlers.JsonResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class WebappResponseHandlerFactory {

    private WebappResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(String action, ScrutariWebapp scrutariWebapp, RequestMap requestMap) {
        WarningHandler warningHandler = new WarningHandler();
        Lang workingLang = RequestMapUtils.getLang(requestMap, warningHandler);
        if (workingLang == null) {
            workingLang = scrutariWebapp.getWebappDefaultLang();
        }
        action = action.toLowerCase();
        switch (action) {
            case "":
                return new IndexPage(scrutariWebapp, workingLang, null);
            case "_reload":
                scrutariWebapp.reload();
                return new IndexPage(scrutariWebapp, workingLang, "_ done.webapp.reload");
            case "_activeall":
                activeAll(scrutariWebapp);
                return new IndexPage(scrutariWebapp, workingLang, "_ done.webapp.activeall");
            case "_json":
                return getJsonHandler(scrutariWebapp, requestMap, workingLang, warningHandler);
            case "_logs":
                return new LogsPage(scrutariWebapp, workingLang);
            default:
                return null;
        }
    }

    private static void activeAll(ScrutariWebapp scrutariWebapp) {
        List<String> nameList = scrutariWebapp.getEngineNameList();
        for (String name : nameList) {
            if (!scrutariWebapp.isEngineInit(name)) {
                scrutariWebapp.getEngine(name);
            }
        }
    }

    private static JsonResponseHandler getJsonHandler(ScrutariWebapp scrutariWebapp, RequestMap requestMap, Lang lang, WarningHandler warningHandler) {
        AbstractJsonProducer abstractJsonProducer;
        try {
            abstractJsonProducer = getJsonProducer(scrutariWebapp, requestMap, lang, warningHandler);
        } catch (ErrorMessageException eme) {
            abstractJsonProducer = new ErrorJsonProducer(eme.getErrorMessage(), EngineUtils.getMessageLocalisation(scrutariWebapp, lang));
        }
        return JsonResponseHandler.init(abstractJsonProducer, requestMap.getParameter("callback"))
                .accessControl(JsonResponseHandler.ALL_ORIGIN);
    }

    private static AbstractJsonProducer getJsonProducer(ScrutariWebapp scrutariWebapp, RequestMap requestMap, Lang lang, WarningHandler warningHandler) throws ErrorMessageException {
        String type = RequestMapUtils.getMandatoryParam(Parameters.TYPE, requestMap);
        String versionString = RequestMapUtils.getMandatoryParam(Parameters.VERSION, requestMap);
        int version;
        try {
            version = Integer.parseInt(versionString);
        } catch (NumberFormatException nfe) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.WRONG_PARAMETER_VALUE, Parameters.VERSION, versionString);
        }
        if (version < 0) {
            throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.VERSION, versionString);
        }
        switch (type) {
            case "enginegroup":
                return JsonProducerFactory.getEngineGroupJsonProducer(scrutariWebapp, requestMap, version, lang, warningHandler);
            default:
                throw new ErrorMessageException(ParameterErrorMessageKeys.UNKNOWN_PARAMETER_VALUE, Parameters.TYPE, type);
        }
    }

}
