/* Scrutari - Copyright (c) 2013-2016 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 *
 * @author Vincent Calame
 */
public class ServletContextListenerImpl implements ServletContextListener {

    public ServletContextListenerImpl() {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ScrutariWebapp.init(sce.getServletContext());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ScrutariWebapp scrutariWebapp = ScrutariWebapp.getScrutariWebapp(sce.getServletContext());
        scrutariWebapp.contextDestroyed();
    }

}
