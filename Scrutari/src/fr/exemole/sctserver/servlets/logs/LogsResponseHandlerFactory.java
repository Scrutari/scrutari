/* Scrutari - Copyright (c) 2017-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.logs;

import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.tools.log.SctLogUtils;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.servlets.ServletUtils;


/**
 *
 * @author Vincent Calame
 */
public final class LogsResponseHandlerFactory {

    private LogsResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(String relativePath, LogStorage logStorage) {
        int idx = relativePath.indexOf('/');
        if (idx == -1) {
            return null;
        }
        String dir = relativePath.substring(0, idx);
        if (!SctLogUtils.isValidLogDir(dir)) {
            return null;
        }
        String name = relativePath.substring(idx + 1);
        int idx2 = name.indexOf('/');
        if (idx2 != -1) {
            return null;
        }
        if (name.endsWith(".txt.html")) {
            String contentName = name.substring(0, name.length() - 5);
            String content = logStorage.getLog(dir, contentName);
            if (content == null) {
                return null;
            }
            return new HtmlVersionHandler(contentName, content);
        } else {
            String content = logStorage.getLog(dir, name);
            if (content == null) {
                return null;
            }
            return ServletUtils.wrap(content);
        }
    }

}
