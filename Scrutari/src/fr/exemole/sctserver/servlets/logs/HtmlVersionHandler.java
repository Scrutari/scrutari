/* Scrutari - Copyright (c) 2019-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.logs;

import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.PageHtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.text.StringUtils;


/**
 *
 * @author Vincent Calame
 */
public class HtmlVersionHandler extends PageHtmlPrinter implements ResponseHandler {

    private final String title;
    private final String content;

    public HtmlVersionHandler(String title, String content) {
        this.title = title;
        this.content = content;
        String resourcesPath = "../../resources/";
        addCssUrl(resourcesPath + "theme/css/logs.css");
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        initPrinter(response.getWriter());
        start(Lang.build("en"), title);
        this
                .TABLE("logs-Table")
                .__(printContent())
                ._TABLE();
        end();
    }

    private boolean printContent() {
        for (String line : StringUtils.getLineTokens(content, StringUtils.NOTCLEAN)) {
            line = line.trim();
            if (line.length() > 0) {
                this
                        .TR();
                for (String token : StringUtils.getTokens(line, '\t', StringUtils.NOTCLEAN)) {
                    this
                            .TD()
                            .__(printToken(token))
                            ._TD();
                }
                this
                        ._TR();
            }
        }
        return true;
    }

    private boolean printToken(String token) {
        int idx = token.indexOf('@');
        if (idx > 0) {
            this
                    .__escape(token.substring(0, idx))
                    .SPAN("logs-Name")
                    .__escape(token.substring(idx))
                    ._SPAN();
            return true;
        }
        idx = token.indexOf(',');
        if (idx > 0) {
            this
                    .__escape(token.substring(0, idx))
                    .SPAN("logs-Milliseconds")
                    .__escape(token.substring(idx))
                    ._SPAN();
            return true;
        }
        this
                .__escape(token);
        return true;
    }


}
