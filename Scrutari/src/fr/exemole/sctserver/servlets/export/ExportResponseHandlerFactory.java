/* Scrutari - Copyright (c) 2012-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.export;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import fr.exemole.sctserver.request.Parameters;
import fr.exemole.sctserver.servlets.export.contentstreams.ResultCsvStream;
import fr.exemole.sctserver.servlets.export.contentstreams.ResultOdsStream;
import fr.exemole.sctserver.servlets.export.contentstreams.ScrutariDataXmlStream;
import fr.exemole.sctserver.servlets.export.contentstreams.SourceListHtmlStream;
import java.text.ParseException;
import java.util.Date;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;
import net.scrutari.data.BaseData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.QId;
import net.scrutari.searchengine.api.result.FicheSearchResult;


/**
 *
 * @author Vincent Calame
 */
public final class ExportResponseHandlerFactory {

    private final static String RESULT_PREFIX = "result_";
    private final static String DATA_PREFIX = "data-";
    private final static String ODS_EXTENSION = ".ods";
    private final static String XML_EXTENSION = ".xml";
    private final static String CSV_EXTENSION = ".csv";

    private ExportResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        if (relativePath.equals("source-list.html")) {
            return new SourceListHtmlStream(engine);
        } else if (relativePath.startsWith(RESULT_PREFIX)) {
            return getResultHandler(engine, relativePath, requestMap);
        } else if (relativePath.startsWith(DATA_PREFIX)) {
            return getDataHandler(engine, relativePath, requestMap);
        } else {
            return null;
        }
    }

    private static ResponseHandler getResultHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        ScrutariSession scrutariSession = engine.getScrutariSession();
        if (relativePath.endsWith(ODS_EXTENSION)) {
            ResultParameters resultParameters = initResultParameters(scrutariSession, relativePath.substring(RESULT_PREFIX.length(), relativePath.length() - (ODS_EXTENSION.length())));
            if (resultParameters != null) {
                return new ResultOdsStream(scrutariSession, resultParameters.ficheSearchResult, resultParameters.lang);
            }
        } else if (relativePath.endsWith(CSV_EXTENSION)) {
            ResultParameters resultParameters = initResultParameters(scrutariSession, relativePath.substring(RESULT_PREFIX.length(), relativePath.length() - (CSV_EXTENSION.length())));
            if (resultParameters != null) {
                return new ResultCsvStream(scrutariSession, resultParameters.ficheSearchResult, resultParameters.lang);
            }
        }
        return null;
    }

    private static ResponseHandler getDataHandler(SctEngine engine, String relativePath, RequestMap requestMap) {
        if (relativePath.endsWith(XML_EXTENSION)) {
            String scrutariSourceName = relativePath.substring(DATA_PREFIX.length(), relativePath.length() - (XML_EXTENSION.length()));
            ScrutariSource scrutariSource = engine.getScrutariSourceManager().getScrutariSouce(scrutariSourceName);
            if (scrutariSource == null) {
                return null;
            }
            Date date = null;
            FuzzyDate lastUpdate = scrutariSource.getLastUpdate();
            if (lastUpdate != null) {
                date = lastUpdate.toDate();
            }
            Lang defaultLang = null;
            String langParam = requestMap.getParameter(Parameters.LANG);
            if (langParam != null) {
                try {
                    defaultLang = Lang.parse(langParam);
                } catch (ParseException pe) {

                }
            }
            try (DataAccess dataAccess = engine.getScrutariSession().getScrutariDB().openDataAccess()) {
                BaseData baseData = dataAccess.getBaseDataByScrutariSourceName(scrutariSourceName);
                if (baseData == null) {
                    return null;
                }
                return new ScrutariDataXmlStream(engine, dataAccess, baseData, date, defaultLang);
            }
        } else {
            return null;
        }
    }

    private static ResultParameters initResultParameters(ScrutariSession scrutariSession, String param) {
        int idx2 = param.lastIndexOf('_');
        Lang lang;
        try {
            lang = Lang.parse(param.substring(idx2 + 1));
        } catch (ParseException pe) {
            return null;
        }
        FicheSearchResult ficheSearchResult = null;
        try {
            QId qId = QId.parse(param.substring(0, idx2));
            ficheSearchResult = scrutariSession.getSearch(qId);
        } catch (ParseException pe) {
        }
        if (ficheSearchResult == null) {
            return null;
        }
        return new ResultParameters(ficheSearchResult, lang);
    }


    private static class ResultParameters {

        private final FicheSearchResult ficheSearchResult;
        private final Lang lang;

        private ResultParameters(FicheSearchResult ficheSearchResult, Lang lang) {
            this.ficheSearchResult = ficheSearchResult;
            this.lang = lang;
        }

    }

}
