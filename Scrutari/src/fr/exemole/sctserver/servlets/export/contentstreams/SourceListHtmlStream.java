/* Scrutari - Copyright (c) 2018-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.export.contentstreams;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.collect.ScrutariSource;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.html.HA;
import net.mapeadores.util.html.PageHtmlPrinter;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public class SourceListHtmlStream extends PageHtmlPrinter implements ResponseHandler {

    private final SctEngine engine;

    public SourceListHtmlStream(SctEngine engine) {
        this.engine = engine;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        initPrinter(response.getWriter());
        start(Lang.build("en"), "Source List");
        this
                .UL();
        for (ScrutariSource scrutariSource : engine.getScrutariSourceManager().getScrutariSourceList()) {
            String sourceName = scrutariSource.getScrutariSourceDef().getName();
            this
                    .LI()
                    .A(HA.href("data-" + sourceName + ".xml"))
                    .__escape(sourceName)
                    ._A()
                    ._LI();
        }
        this
                ._UL();
        end();
    }

}
