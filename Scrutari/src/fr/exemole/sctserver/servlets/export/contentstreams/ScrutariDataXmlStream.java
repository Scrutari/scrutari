/* Scrutari - Copyright (c) 2018-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.export.contentstreams;

import fr.exemole.sctserver.api.SctEngine;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.annotation.Nullable;
import net.mapeadores.util.attr.Attribute;
import net.mapeadores.util.attr.AttributeKey;
import net.mapeadores.util.attr.Attributes;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.primitives.PrimUtils;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.text.Label;
import net.mapeadores.util.text.Phrase;
import net.mapeadores.util.text.Phrases;
import net.scrutari.data.BaseData;
import net.scrutari.data.CorpusData;
import net.scrutari.data.DataConstants;
import net.scrutari.data.FicheData;
import net.scrutari.data.MotcleData;
import net.scrutari.data.ThesaurusData;
import net.scrutari.dataexport.ScrutariDataExportFactory;
import net.scrutari.dataexport.api.AttributeExport;
import net.scrutari.dataexport.api.BaseMetadataExport;
import net.scrutari.dataexport.api.CorpusMetadataExport;
import net.scrutari.dataexport.api.FicheExport;
import net.scrutari.dataexport.api.MetadataExport;
import net.scrutari.dataexport.api.MotcleExport;
import net.scrutari.dataexport.api.ScrutariDataExport;
import net.scrutari.dataexport.api.ThesaurusMetadataExport;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.db.api.DataAccess;
import net.scrutari.db.api.FicheInfo;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariDataXmlStream implements ResponseHandler {

    private final SctEngine engine;
    private final DataAccess dataAccess;
    private final BaseData baseData;
    private final Date extractionDate;
    private final Lang defaultLang;

    public ScrutariDataXmlStream(SctEngine engine, DataAccess dataAccess, BaseData baseData, Date extractionDate, @Nullable Lang defaultLang) {
        this.engine = engine;
        this.dataAccess = dataAccess;
        this.baseData = baseData;
        this.extractionDate = extractionDate;
        if (defaultLang != null) {
            defaultLang = baseData.checkAvailableLang(defaultLang);
        }
        this.defaultLang = defaultLang;
    }

    @Override
    public long getLastModified() {
        if (extractionDate != null) {
            return extractionDate.getTime();
        } else {
            return engine.getScrutariSession().getScrutariDB().getScrutariDBName().getDate().getTime();
        }
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.XML + ";charset=UTF-8");
        try (PrintWriter pw = response.getWriter()) {
            writeExport(pw);
        }
    }

    private void writeExport(Appendable appendable) throws IOException {
        Integer[] thesaurusCodeArray = PrimUtils.toArray(baseData.getThesaurusCodeList());
        ScrutariDataExport scrutariDataExport = ScrutariDataExportFactory.newInstance(appendable, 0, true, extractionDate);
        BaseMetadataExport baseMetadataExport = scrutariDataExport.startExport();
        writeBaseMetadata(baseMetadataExport);
        for (Integer corpusCode : baseData.getCorpusCodeList()) {
            CorpusData corpusData = dataAccess.getCorpusData(corpusCode);
            writeCorpus(corpusData, scrutariDataExport, thesaurusCodeArray);
        }
        for (Integer thesaurusCode : thesaurusCodeArray) {
            ThesaurusData thesaurusData = dataAccess.getThesaurusData(thesaurusCode);
            writeThesaurus(thesaurusData, scrutariDataExport);
        }
        scrutariDataExport.endExport();
    }

    private void writeBaseMetadata(BaseMetadataExport baseMetadataExport) {
        BaseURI baseURI = baseData.getBaseURI();
        baseMetadataExport.setAuthority(baseURI.getAuthority());
        baseMetadataExport.setBaseName(baseURI.getBaseName());
        baseMetadataExport.setBaseIcon(baseData.getBaseIcon(defaultLang));
        for (Lang lang : baseData.getAvailableLangList()) {
            baseMetadataExport.addLangUI(lang.toString());
        }
        Phrases phrases = baseData.getPhrases();
        writeIntitule(baseMetadataExport, phrases.getPhrase(DataConstants.BASE_TITLE), BaseMetadataExport.INTITULE_SHORT);
        writeIntitule(baseMetadataExport, phrases.getPhrase(DataConstants.BASE_LONGTITLE), BaseMetadataExport.INTITULE_LONG);
        writeAttributes(baseMetadataExport, baseData.getAttributes());
    }


    private void writeCorpus(CorpusData corpusData, ScrutariDataExport scrutariDataExport, Integer[] thesaurusCodeArray) throws IOException {
        CorpusMetadataExport corpusMetadataExport = scrutariDataExport.newCorpus(corpusData.getCorpusURI().getCorpusName());
        Phrases phrases = corpusData.getPhrases();
        writeIntitule(corpusMetadataExport, phrases.getPhrase(DataConstants.CORPUS_TITLE), CorpusMetadataExport.INTITULE_CORPUS);
        writeIntitule(corpusMetadataExport, phrases.getPhrase(DataConstants.CORPUS_FICHE), CorpusMetadataExport.INTITULE_FICHE);
        int complementMaxNumber = corpusData.getComplementMaxNumber();
        for (int num = 1; num <= complementMaxNumber; num++) {
            corpusMetadataExport.addComplement();
            Phrase phrase = phrases.getPhrase(DataConstants.CORPUS_COMPLEMENTPREFIX + num);
            if (phrase != null) {
                for (Label label : phrase) {
                    corpusMetadataExport.setComplementIntitule(num, label.getLang().toString(), label.getLabelString());
                }
            }
        }
        writeAttributes(corpusMetadataExport, corpusData.getAttributes());
        for (Integer ficheCode : corpusData.getFicheCodeList()) {
            FicheData ficheData = dataAccess.getFicheData(ficheCode);
            FicheURI ficheURI = ficheData.getFicheURI();
            FicheExport ficheExport = scrutariDataExport.newFiche(ficheURI.getFicheId());
            ficheExport.setTitre(ficheData.getTitre());
            ficheExport.setSoustitre(ficheData.getSoustitre());
            FuzzyDate date = ficheData.getDate();
            if (date != null) {
                ficheExport.setDate(date.toString());
            }
            Lang lang = ficheData.getLang();
            if (!lang.equals(FicheData.UNDETERMINED_LANG)) {
                ficheExport.setLang(lang.toString());
            }
            String href = ficheData.getHref(defaultLang);
            if (href != null) {
                ficheExport.setHref(href);
            }
            String ficheIcon = ficheData.getFicheIcon();
            if (ficheIcon != null) {
                ficheExport.setFicheIcon(ficheIcon);
            }
            if (ficheData.isWithGeo()) {
                ficheExport.setGeoloc(ficheData.getLatitude().toString(), ficheData.getLongitude().toString());
            }
            for (int num = 1; num <= complementMaxNumber; num++) {
                String comp = ficheData.getComplementByNumber(num);
                if (comp == null) {
                    continue;
                }
                ficheExport.addComplement(num, comp);
            }
            writeAttributes(ficheExport, ficheData.getAttributes());
            FicheInfo ficheInfo = dataAccess.getFicheInfo(ficheCode);
            for (Integer thesaurusCode : thesaurusCodeArray) {
                List<Integer> indexationMotcleCodeList = ficheInfo.getIndexationCodeList(thesaurusCode);
                for (Integer motcleCode : indexationMotcleCodeList) {
                    MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
                    MotcleURI motcleURI = motcleData.getMotcleURI();
                    scrutariDataExport.addIndexation(ficheURI.getCorpusURI().getCorpusName(), ficheURI.getFicheId(), motcleURI.getThesaurusURI().getThesaurusName(), motcleURI.getMotcleId(), 1);
                }
            }
        }
    }

    private void writeThesaurus(ThesaurusData thesaurusData, ScrutariDataExport scrutariDataExport) throws IOException {
        ThesaurusMetadataExport thesaurusMetadataExport = scrutariDataExport.newThesaurus(thesaurusData.getThesaurusURI().getThesaurusName());
        Phrases phrases = thesaurusData.getPhrases();
        writeIntitule(thesaurusMetadataExport, phrases.getPhrase(DataConstants.THESAURUS_TITLE), ThesaurusMetadataExport.INTITULE_THESAURUS);
        writeAttributes(thesaurusMetadataExport, thesaurusData.getAttributes());
        for (Integer motcleCode : thesaurusData.getMotcleCodeList()) {
            MotcleData motcleData = dataAccess.getMotcleData(motcleCode);
            MotcleExport motcleExport = scrutariDataExport.newMotcle(motcleData.getMotcleURI().getMotcleId());
            for (Label label : motcleData.getLabels()) {
                motcleExport.setLibelle(label.getLang().toString(), label.getLabelString());
            }
            writeAttributes(motcleExport, motcleData.getAttributes());
        }
    }

    private void writeIntitule(MetadataExport metadataExport, Phrase phrase, int type) {
        if (phrase == null) {
            return;
        }
        for (Label label : phrase) {
            metadataExport.setIntitule(type, label.getLang().toString(), label.getLabelString());
        }
    }

    private void writeAttributes(AttributeExport attributeExport, Attributes attributes) {
        for (Attribute attribute : attributes) {
            AttributeKey attributeKey = attribute.getAttributeKey();
            for (String value : attribute) {
                attributeExport.addAttributeValue(attributeKey.getNameSpace(), attributeKey.getLocalKey(), value);
            }
        }
    }

}
