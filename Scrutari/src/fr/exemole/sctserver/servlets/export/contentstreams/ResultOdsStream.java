/* Scrutari - Copyright (c) 2012-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.export.contentstreams;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.tools.ExportCache;
import fr.exemole.sctserver.tools.feed.BaseWrapper;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.opendocument.io.OdSource;
import net.mapeadores.opendocument.io.OdXML;
import net.mapeadores.opendocument.io.OdZip;
import net.mapeadores.opendocument.io.OdZipEngine;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLPart;
import net.mapeadores.util.xml.XMLUtils;
import net.mapeadores.util.xml.XMLWriter;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public class ResultOdsStream implements ResponseHandler {

    private final ScrutariSession scrutariSession;
    private final FicheSearchResult ficheSearchResult;
    private final Lang lang;

    public ResultOdsStream(ScrutariSession scrutariSession, FicheSearchResult ficheSearchResult, Lang lang) {
        this.scrutariSession = scrutariSession;
        this.ficheSearchResult = ficheSearchResult;
        this.lang = lang;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.ODS);
        try (OutputStream os = response.getOutputStream()) {
            OdZipEngine.run(os, OdZip.spreadSheet()
                    .contentOdSource(new ContentOdSource())
            );
        }
    }


    private class ContentOdSource implements OdSource {

        private ContentOdSource() {
        }

        @Override
        public void writeStream(OutputStream outputStream) throws IOException {
            BufferedWriter buf = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf);
            xmlWriter.appendXMLDeclaration();
            try (DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
                ResultXMLPart resultXMLPart = new ResultXMLPart(xmlWriter, dataAccess);
                resultXMLPart.start();
                resultXMLPart.addResult();
                resultXMLPart.end();
            }
            buf.flush();
        }


        private class ResultXMLPart extends XMLPart {

            private final DataAccess dataAccess;
            private final GlobalSearchOptions globalSearchOptions;
            private final ExportCache exportCache;


            private ResultXMLPart(XMLWriter xmlWriter, DataAccess dataAccess) {
                super(xmlWriter);
                this.dataAccess = dataAccess;
                this.globalSearchOptions = scrutariSession.getGlobalSearchOptions();
                this.exportCache = new ExportCache(dataAccess, lang);
            }

            private void start() throws IOException {
                OdXML.openDocumentContent(this);
                OdXML.openBody(this);
                OdXML.openSpreadsheet(this);
            }

            private void end() throws IOException {
                OdXML.closeSpreadsheet(this);
                OdXML.closeBody(this);
                OdXML.closeDocumentContent(this);
            }

            private void addResult() throws IOException {
                List<FicheSearchResultGroup> groupList = ficheSearchResult.getFicheSearchResultGroupList();
                int groupLength = groupList.size();
                int p = 1;
                for (FicheSearchResultGroup ficheSearchResultGroup : groupList) {
                    String tableName;
                    Category category = globalSearchOptions.getCategoryByRank(ficheSearchResultGroup.getCategoryRank());
                    if (category != null) {
                        CategoryDef categoryDef = category.getCategoryDef();
                        tableName = categoryDef.getTitle(lang);
                    } else {
                        if (groupLength == 1) {
                            tableName = "Result";
                        } else {
                            tableName = "Result " + p;
                        }
                    }
                    OdXML.openTable(this, tableName);
                    for (FicheSearchResultInfo ficheSearchResultInfo : ficheSearchResultGroup.getFicheSearchResultInfoList()) {
                        addFicheData(dataAccess.getFicheData(ficheSearchResultInfo.getCode()));
                    }
                    OdXML.closeTable(this);
                    p++;
                }
            }

            private void addFicheData(FicheData ficheData) throws IOException {
                BaseWrapper baseWrapper = exportCache.getBaseWrapper(ficheData.getBaseCode());
                OdXML.openTableRow(this);
                OdXML.addStringTableCell(this, ficheData.getTitre());
                OdXML.addStringTableCell(this, ficheData.getSoustitre());
                FuzzyDate date = ficheData.getDate();
                if (date != null) {
                    OdXML.addNumberTableCell(this, date.getYear());
                } else {
                    OdXML.addEmptyTableCell(this);
                }
                OdXML.addStringTableCell(this, baseWrapper.getShortLabelString());
                OdXML.addStringTableCell(this, ficheData.getLang().toString());
                OdXML.addLinkStringTableCell(this, ficheData.getHref(baseWrapper.getBaseCheck().getCheckedLang()));
                OdXML.addStringTableCell(this, exportCache.getIndexationString(ficheData.getFicheCode()));
                int max = ficheData.getComplementMaxNumber();
                for (int i = 1; i <= max; i++) {
                    OdXML.addStringTableCell(this, ficheData.getComplementByNumber(i));
                }
                OdXML.closeTableRow(this);
            }

        }

    }

}
