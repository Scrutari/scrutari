/* Scrutari - Copyright (c) 2019-2023 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.export.contentstreams;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.tools.ExportCache;
import fr.exemole.sctserver.tools.feed.BaseWrapper;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.mimetype.MimeTypeConstants;
import net.mapeadores.util.primitives.FuzzyDate;
import net.mapeadores.util.servlets.ResponseHandler;
import net.scrutari.data.FicheData;
import net.scrutari.db.api.DataAccess;
import net.scrutari.searchengine.api.options.Category;
import net.scrutari.searchengine.api.options.CategoryDef;
import net.scrutari.searchengine.api.options.GlobalSearchOptions;
import net.scrutari.searchengine.api.result.FicheSearchResult;
import net.scrutari.searchengine.api.result.FicheSearchResultGroup;
import net.scrutari.searchengine.api.result.FicheSearchResultInfo;


/**
 *
 * @author Vincent Calame
 */
public class ResultCsvStream implements ResponseHandler {

    private final ScrutariSession scrutariSession;
    private final FicheSearchResult ficheSearchResult;
    private final Lang lang;

    public ResultCsvStream(ScrutariSession scrutariSession, FicheSearchResult ficheSearchResult, Lang lang) {
        this.scrutariSession = scrutariSession;
        this.ficheSearchResult = ficheSearchResult;
        this.lang = lang;
    }

    @Override
    public long getLastModified() {
        return ResponseHandler.LASTMODIFIED_NOCACHE;
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType(MimeTypeConstants.CSV + ";charset=UTF-8");
        try (OutputStreamWriter writer = new OutputStreamWriter(response.getOutputStream()); DataAccess dataAccess = scrutariSession.getScrutariDB().openDataAccess()) {
            CSVWriter csvWriter = new CSVWriter(writer, dataAccess);
            csvWriter.writeResult();
        }
    }


    private class CSVWriter {

        private final Appendable appendable;
        private final DataAccess dataAccess;
        private final GlobalSearchOptions globalSearchOptions;
        private final ExportCache exportCache;

        private CSVWriter(Appendable appendable, DataAccess dataAccess) {
            this.appendable = appendable;
            this.dataAccess = dataAccess;
            this.globalSearchOptions = scrutariSession.getGlobalSearchOptions();
            this.exportCache = new ExportCache(dataAccess, lang);
        }

        private void writeResult() throws IOException {
            List<FicheSearchResultGroup> groupList = ficheSearchResult.getFicheSearchResultGroupList();
            boolean unique = (!globalSearchOptions.isWithCategory());
            int p = 1;
            for (FicheSearchResultGroup ficheSearchResultGroup : groupList) {
                String groupTitle = null;
                if (!unique) {
                    Category category = globalSearchOptions.getCategoryByRank(ficheSearchResultGroup.getCategoryRank());
                    if (category != null) {
                        CategoryDef categoryDef = category.getCategoryDef();
                        groupTitle = categoryDef.getTitle(lang);
                    } else {
                        groupTitle = "Group " + p;
                    }
                }
                for (FicheSearchResultInfo ficheSearchResultInfo : ficheSearchResultGroup.getFicheSearchResultInfoList()) {
                    writeLine(groupTitle, ficheSearchResultInfo);
                }
                p++;
            }
        }

        private void writeLine(String groupTitle, FicheSearchResultInfo ficheSearchResultInfo) throws IOException {
            if (groupTitle != null) {
                escape(groupTitle);
                separator();
            }
            FicheData ficheData = dataAccess.getFicheData(ficheSearchResultInfo.getCode());
            BaseWrapper baseWrapper = exportCache.getBaseWrapper(ficheData.getBaseCode());
            escape(ficheData.getTitre());
            separator();
            escape(ficheData.getSoustitre());
            separator();
            FuzzyDate date = ficheData.getDate();
            if (date != null) {
                number(date.getYear());
            }
            separator();
            escape(baseWrapper.getShortLabelString());
            separator();
            escape(ficheData.getLang().toString());
            separator();
            escape(ficheData.getHref(baseWrapper.getBaseCheck().getCheckedLang()));
            separator();
            escape(exportCache.getIndexationString(ficheData.getFicheCode()));
            int max = ficheData.getComplementMaxNumber();
            for (int i = 1; i <= max; i++) {
                separator();
                escape(ficheData.getComplementByNumber(i));
            }
            endLine();
        }

        private void number(int number) throws IOException {
            appendable.append(String.valueOf(number));
        }

        private void escape(String text) throws IOException {
            if ((text == null) || (text.length() == 0)) {
                return;
            }
            appendable.append("\"");
            int length = text.length();
            for (int j = 0; j < length; j++) {
                char carac = text.charAt(j);
                switch (carac) {
                    case '\"':
                        appendable.append("\"\"");
                        break;
                    default:
                        appendable.append(carac);
                }
            }
            appendable.append("\"");
        }

        private void separator() throws IOException {
            appendable.append(",");
        }

        private void endLine() throws IOException {
            appendable.append("\r\n");
        }

    }

}
