/* SctServer - Copyright (c) 2009-2022 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets;

import fr.exemole.sctserver.api.EngineContext;
import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.context.EngineGroup;
import fr.exemole.sctserver.api.storage.EngineStorage;
import fr.exemole.sctserver.api.storage.LogStorage;
import fr.exemole.sctserver.directory.DirectoryEngineStorage;
import fr.exemole.sctserver.directory.persistent.DirectoryLogStorage;
import fr.exemole.sctserver.impl.SctEngineImpl;
import fr.exemole.sctserver.tools.DistResourceStorage;
import fr.exemole.sctserver.tools.ErrorUtils;
import fr.exemole.sctserver.tools.SctMessageLocalisationFactory;
import fr.exemole.sctserver.tools.context.EngineGroupDirectory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.TreeSet;
import javax.servlet.ServletContext;
import net.mapeadores.util.buildinfo.BuildInfo;
import net.mapeadores.util.buildinfo.BuildInfoParser;
import net.mapeadores.util.exceptions.InternalResourceException;
import net.mapeadores.util.io.DirectoryResourceStorage;
import net.mapeadores.util.io.DocStream;
import net.mapeadores.util.io.IOUtils;
import net.mapeadores.util.io.ResourceStorage;
import net.mapeadores.util.io.ResourceStorages;
import net.mapeadores.util.io.ResourceUtils;
import net.mapeadores.util.io.docstream.FileDocStream;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.localisation.LangPreference;
import net.mapeadores.util.localisation.MessageLocalisation;
import net.mapeadores.util.localisation.MessageLocalisationFactory;
import net.mapeadores.util.logging.LogUtils;
import net.mapeadores.util.logging.MessageLog;
import net.mapeadores.util.logging.MessageLogBuilder;
import net.mapeadores.util.mimetype.MimeTypeUtils;
import net.mapeadores.util.servlets.ServletUtils;
import net.mapeadores.util.text.RelativePath;
import net.mapeadores.util.text.StringUtils;
import net.scrutari.db.api.ScrutariDBName;


/**
 *
 * @author Vincent Calame
 */
public class ScrutariWebapp implements EngineContext {

    public final static String SCRUTARIWEBAPP_KEY = "fr.exemole.ScrutariWebapp";
    public final static Lang DEFAULT_LANG = Lang.build("fr");
    private final BuildInfo buildInfo;
    private final String scrutariConfFileString;
    private final Timer timer;
    private final MessageLocalisationFactory messageLocalisationFactory;
    private final Set<String> engineNameSet = new TreeSet<String>();
    private final Map<String, SctEngineImpl> engineMap = new TreeMap<String, SctEngineImpl>();
    private final Map<String, EngineGroup> engineGroupMap = new TreeMap<String, EngineGroup>();
    private final boolean isInit;
    private final File confDir;
    private final File varDir;
    private final MessageLog initMessageLog;
    private final ResourceStorages resourceStorages;
    private String webappUrl = "";
    private Lang webappDefaultLang = DEFAULT_LANG;
    private MessageLog confMessageLog = LogUtils.EMPTY_MESSAGELOG;


    ScrutariWebapp(BuildInfo buildInfo, boolean isInit, File confDir, File varDir, MessageLog initMessageLog, String scrutariConfFileString) {
        this.buildInfo = buildInfo;
        this.isInit = isInit;
        this.confDir = confDir;
        this.varDir = varDir;
        this.initMessageLog = initMessageLog;
        this.scrutariConfFileString = scrutariConfFileString;
        this.messageLocalisationFactory = SctMessageLocalisationFactory.build();
        this.resourceStorages = initResourceStorages(confDir);
        if (isInit) {
            reload();
            migrate();
        }
        this.timer = new Timer(true);
        timer.scheduleAtFixedRate(new UpdateTask(), getFirstTime(), getPeriod());
    }

    public List<String> getEngineNameList() {
        ArrayList<String> list = new ArrayList<String>();
        list.addAll(engineNameSet);
        return list;
    }

    public MessageLog getInitMessageLog() {
        return initMessageLog;
    }

    public MessageLog getConfMessageLog() {
        return confMessageLog;
    }

    public boolean isEngineInit(String engineName) {
        return engineMap.containsKey(engineName);
    }

    public LogStorage getLogStorage(String engineName) {
        File engineVarDir = new File(varDir, engineName);
        File logsDir = new File(engineVarDir, "logs");
        return new DirectoryLogStorage(logsDir);
    }

    @Override
    public BuildInfo getBuildInfo() {
        return buildInfo;
    }

    @Override
    public SctEngine getEngine(String engineName) {
        SctEngineImpl engine = engineMap.get(engineName);
        if (engine == null) {
            if (!isInit) {
                return null;
            }
            engine = createEngine(engineName);
        }
        return engine;
    }

    @Override
    public Lang getWebappDefaultLang() {
        return webappDefaultLang;
    }

    @Override
    public String getWebappCanonicalUrl() {
        return webappUrl;
    }

    @Override
    public EngineGroup getEngineGroup(String groupName) {
        return engineGroupMap.get(groupName);
    }

    @Override
    public MessageLocalisation getMessageLocalisation(LangPreference langPreference) {
        return messageLocalisationFactory.newInstance(langPreference, langPreference.getFirstLang().toLocale());
    }

    @Override
    public DocStream getResourceDocStream(RelativePath path, SctEngine sctEngine) {
        if (confDir == null) {
            return null;
        }
        File dir;
        if (sctEngine == null) {
            dir = new File(confDir, "_resources");
        } else {
            dir = new File(confDir, sctEngine.getEngineName() + File.separatorChar + "resources");
        }
        File file = new File(dir, path.toString());
        if ((file.exists()) && (!file.isDirectory())) {
            FileDocStream fileDocStream = new FileDocStream(file);
            String mimeType = MimeTypeUtils.getMimeType(MimeTypeUtils.DEFAULT_RESOLVER, file.getName());
            fileDocStream.setMimeType(mimeType);
            return fileDocStream;
        } else {
            return null;
        }
    }

    public boolean isInit() {
        return isInit;
    }

    public void contextDestroyed() {
        timer.cancel();
    }

    public final synchronized void reload() {
        if (!isInit) {
            return;
        }
        MessageLogBuilder messageLogBuilder = new MessageLogBuilder();
        readConf(messageLogBuilder);
        readGroups(messageLogBuilder);
        readEngines();
        this.confMessageLog = messageLogBuilder.toMessageLog();
    }

    @Override
    public ResourceStorages getResourceStorages() {
        return resourceStorages;
    }

    private SctEngineImpl createEngine(String engineName) {
        if (!engineNameSet.contains(engineName)) {
            return null;
        }
        engineName = engineName.intern();
        SctEngineImpl engine;
        synchronized (engineName) {
            engine = engineMap.get(engineName);
            if (engine == null) {
                engine = buildEngine(engineName);
                synchronized (engineMap) {
                    engineMap.put(engineName, engine);
                }
            }
        }
        return engine;
    }

    private void readConf(MessageLogBuilder messageLogBuilder) {
        File scrutariConfFile;
        if (scrutariConfFileString == null) {
            scrutariConfFile = new File(confDir, "conf.xml");
            if (!scrutariConfFile.exists()) {
                return;
            }
        } else {
            scrutariConfFile = new File(scrutariConfFileString);
        }
        messageLogBuilder.setCurrentSource(scrutariConfFile.getPath());
        if (scrutariConfFile.isDirectory()) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.conffile_dir", scrutariConfFileString);
            return;
        }
        ConfDOMReader.Conf conf = ConfDOMReader.readConf(scrutariConfFile, messageLogBuilder);
        if (conf != null) {
            webappUrl = conf.getWebappCanonicalUrl();
            webappDefaultLang = conf.getWebappDefaultLang();
        }
    }

    private void readGroups(MessageLogBuilder messageLogBuilder) {
        engineGroupMap.clear();
        File groupsDir = new File(confDir, "_groups");
        if ((groupsDir.exists()) && (groupsDir.isDirectory())) {
            Map<String, EngineGroup> newEngineGroupMap = EngineGroupDirectory.read(groupsDir, messageLogBuilder);
            engineGroupMap.putAll(newEngineGroupMap);
        }
    }

    private void readEngines() {
        Set<String> hereSet = new HashSet<String>();
        for (File dir : confDir.listFiles()) {
            if (!dir.isDirectory()) {
                continue;
            }
            String name = dir.getName();
            if ((!name.startsWith("_")) && (StringUtils.isTechnicalName(name, true))) {
                hereSet.add(name);
            }
        }
        engineNameSet.clear();
        engineNameSet.addAll(hereSet);
        synchronized (engineMap) {
            Map<String, SctEngineImpl> newMap = new HashMap<String, SctEngineImpl>();
            for (String engineName : hereSet) {
                SctEngineImpl engine = engineMap.get(engineName);
                if (engine != null) {
                    newMap.put(engineName, engine);
                }
            }
            engineMap.clear();
            engineMap.putAll(newMap);
        }
    }

    private Date getFirstTime() {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, day + 1);
        calendar.set(Calendar.HOUR_OF_DAY, 1);
        calendar.set(Calendar.MINUTE, 11);
        return calendar.getTime();
    }

    private long getPeriod() {
        return 24 * 60 * 60 * 1000;
    }

    private SctEngineImpl buildEngine(String engineName) {
        File engineConfDir = new File(confDir, engineName);
        File engineVarDir = new File(varDir, engineName);
        if (!engineVarDir.exists()) {
            engineVarDir.mkdirs();
        }
        EngineStorage engineStorage = new DirectoryEngineStorage(engineConfDir, engineVarDir);
        return new SctEngineImpl(engineName, engineStorage, this);
    }

    private void migrate() {
        //Révision 3109 (2017-01-17) : simplification du nom d'une session
        if (varDir.exists()) {
            for (File child : varDir.listFiles()) {
                migrateR3109(child);
            }
        }
    }

    private void migrateR3109(File file) {
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                migrateR3109(child);
            }
        }
        String name = file.getName();
        String extension = null;
        int idx = name.lastIndexOf('.');
        if (idx > 1) {
            extension = name.substring(idx);
            name = name.substring(0, idx);
        }
        if (name.length() > 14) {
            try {
                ScrutariDBName scrutariDBName = ScrutariDBName.parse(name);
                String newName = scrutariDBName.getName();
                if (extension != null) {
                    newName = newName + extension;
                }
                file.renameTo(new File(file.getParentFile(), newName));
            } catch (ParseException pe) {

            }
        }
    }

    public static ScrutariWebapp getScrutariWebapp(ServletContext servletContext) {
        return (ScrutariWebapp) servletContext.getAttribute(SCRUTARIWEBAPP_KEY);
    }

    static ScrutariWebapp init(ServletContext servletContext) {
        BuildInfo buildInfo = null;
        if (ScrutariWebapp.class.getResource("VERSION") != null) {
            try (InputStream is = ScrutariWebapp.class.getResourceAsStream("VERSION")) {
                buildInfo = BuildInfoParser.parse(is);
            } catch (IOException ioe) {
                throw new InternalResourceException(ioe);
            }
        }
        MessageLogBuilder messageLogBuilder = new MessageLogBuilder();
        String confDirString = null;
        String varDirString = null;
        boolean done = false;
        String scrutariConfFileString = ServletUtils.getInitParameter(servletContext, "scrutariConfFile");
        if (scrutariConfFileString != null) {
            messageLogBuilder.setCurrentSource(scrutariConfFileString);
            ConfDOMReader.Dirs dirs = ConfDOMReader.readDirs(scrutariConfFileString, messageLogBuilder);
            if (dirs != null) {
                confDirString = dirs.getConfDirString();
                varDirString = dirs.getVarDirString();
                done = true;
            }
        } else {
            messageLogBuilder.setCurrentSource("CATALINA:context.xml");
            done = true;
            confDirString = ServletUtils.getInitParameter(servletContext, "confDir");
            if (confDirString == null) {
                ErrorUtils.configCritical(messageLogBuilder, "_ error.config.initparameter", "confDir");
                done = false;
            }
            varDirString = ServletUtils.getInitParameter(servletContext, "varDir");
            if (varDirString == null) {
                ErrorUtils.configCritical(messageLogBuilder, "_ error.config.initparameter", "varDir");
                done = false;
            }
        }
        File confDir = null;
        File varDir = null;
        if (done) {
            confDir = new File(confDirString);
            try {
                if (!confDir.exists()) {
                    ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_notexist", "confDir", confDirString);
                    done = false;
                } else if (!confDir.isDirectory()) {
                    ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_notdir", "confDir", confDirString);
                    done = false;
                } else if (!confDir.canRead()) {
                    ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_cannotread", "confDir", confDirString);
                    done = false;
                }
            } catch (SecurityException se) {
                ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_cannotread", "confDir", confDirString);
                done = false;
            }
            varDir = new File(varDirString);
            if (!varDir.isAbsolute()) {
                varDir = new File(ServletUtils.getRealFile(servletContext, "/"), varDirString);
            }
            try {
                if (!varDir.exists()) {
                    boolean isCreated;
                    try {
                        isCreated = varDir.mkdirs();
                    } catch (SecurityException se2) {
                        isCreated = false;
                    }
                    if (!isCreated) {
                        ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_cannotcreate", "varDir", varDirString);
                        done = false;
                    }
                } else if (!varDir.isDirectory()) {
                    ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_notdir", "varDir", varDirString);
                    done = false;
                } else if (!varDir.canRead()) {
                    ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_cannotread", "varDir", varDirString);
                    done = false;
                } else if (!varDir.canWrite()) {
                    ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_cannotwrite", "varDir", varDirString);
                    done = false;
                }
            } catch (SecurityException se) {
                ErrorUtils.configCritical(messageLogBuilder, "_ error.config.path_unreadable", "varDir", varDirString);
                done = false;
            }

        }
        ScrutariWebapp scrutariWebapp = new ScrutariWebapp(buildInfo, done, confDir, varDir, messageLogBuilder.toMessageLog(), scrutariConfFileString);
        servletContext.setAttribute(ScrutariWebapp.SCRUTARIWEBAPP_KEY, scrutariWebapp);
        return scrutariWebapp;
    }

    private static ResourceStorages initResourceStorages(File confDir) {
        ResourceStorage[] array = new ResourceStorage[2];
        array[0] = new DirectoryResourceStorage("webapp", new File(confDir, "_resources"));
        array[1] = DistResourceStorage.newInstance();
        return ResourceUtils.toResourceStorages(array);
    }


    private class UpdateTask extends TimerTask {

        private UpdateTask() {
        }

        @Override
        public void run() {
            Set<String> suspendSet = checkSuspend();
            if (suspendSet.contains("_all")) {
                return;
            }
            SctEngineImpl[] engineArray;
            synchronized (engineMap) {
                engineArray = engineMap.values().toArray(new SctEngineImpl[engineMap.size()]);
            }
            for (SctEngineImpl engine : engineArray) {
                if (!suspendSet.contains(engine.getEngineName())) {
                    engine.update();
                }
            }
        }

        private Set<String> checkSuspend() {
            File suspendFile = new File(confDir, "suspend-update.txt");
            Set<String> result = new HashSet<String>();
            if (suspendFile.exists()) {
                try (InputStream is = new FileInputStream(suspendFile)) {
                    List<String> lines = IOUtils.readLines(is, "UTF-8");
                    for (String line : lines) {
                        result.add(line.trim());
                    }
                } catch (IOException ioe) {
                    result.add("_all");
                }
            }
            return result;
        }

    }

}
