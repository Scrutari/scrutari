/* Scrutari - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.feed;

import fr.exemole.sctserver.api.SctEngine;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.tools.feed.SctFeedOptionsBuilder;
import java.text.ParseException;
import java.util.HashSet;
import java.util.Set;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.request.RequestMap;
import net.mapeadores.util.servlets.ResponseHandler;


/**
 *
 * @author Vincent Calame
 */
public final class FeedResponseHandlerFactory {

    private final static String ATOM_EXTENSION = ".atom";

    private FeedResponseHandlerFactory() {

    }

    public static ResponseHandler getHandler(SctEngine engine, String relativePath, RequestMap requestMap, String selfUrl) {
        if (!relativePath.endsWith(ATOM_EXTENSION)) {
            return null;
        }
        String base = relativePath.substring(0, relativePath.length() - ATOM_EXTENSION.length());
        int idx = base.lastIndexOf('_');
        if (idx == -1) {
            return null;
        }
        Lang lang;
        try {
            lang = Lang.parse(base.substring(idx + 1));
        } catch (ParseException lie) {
            return null;
        }
        String options;
        short type;
        if (base.startsWith("tree")) {
            options = base.substring("tree".length(), idx);
            type = SctFeedConstants.TREE_SCTFEED_TYPE;
        } else if (base.startsWith("classes")) {
            options = base.substring("classes".length(), idx);
            type = SctFeedConstants.CATEGORIES_SCTFEED_TYPE;
        } else if (base.startsWith("categories")) {
            options = base.substring("categories".length(), idx);
            type = SctFeedConstants.CATEGORIES_SCTFEED_TYPE;
        } else if (base.startsWith("fiches")) {
            options = base.substring("fiches".length(), idx);
            type = SctFeedConstants.FICHES_SCTFEED_TYPE;
        } else if (base.startsWith("errors")) {
            options = base.substring("errors".length(), idx);
            type = SctFeedConstants.ERRORS_SCTFEED_TYPE;
        } else {
            return null;
        }
        SctFeedOptionsBuilder optionsBuilder = new SctFeedOptionsBuilder(type, lang, requestMap, selfUrl);
        if (options.length() > 0) {
            if (!options.startsWith("-")) {
                return null;
            }
            parse(options, optionsBuilder);
        }
        return new AtomResponseHandler(engine.getScrutariSession(), optionsBuilder.toSctFeedOptions());
    }

    public static void parse(String optionsString, SctFeedOptionsBuilder sctFeedOptionsBuilder) {
        Set<Character> caracSet = new HashSet<Character>();
        int length = optionsString.length();
        for (int i = 0; i < length; i++) {
            char carac = optionsString.charAt(i);
            if (carac == '-') {
            } else {
                caracSet.add(carac);
            }
        }
        if (caracSet.contains('f')) {
            sctFeedOptionsBuilder.setOption(SctFeedConstants.FICHEONLY_OPTION, true);
        }
        if (caracSet.contains('s')) {
            sctFeedOptionsBuilder.setOption(SctFeedConstants.SOUSTITRE_OPTION, true);
        }
        if (caracSet.contains('i')) {
            sctFeedOptionsBuilder.setOption(SctFeedConstants.ICON_OPTION, true);
        }
        if (caracSet.contains('h')) {
            sctFeedOptionsBuilder.setOption(SctFeedConstants.INDENT_OPTION, true);
        }
        if (caracSet.contains('v')) {
            sctFeedOptionsBuilder.setOption(SctFeedConstants.TECHNICALTITLE_OPTION, true);
        }
    }

}
