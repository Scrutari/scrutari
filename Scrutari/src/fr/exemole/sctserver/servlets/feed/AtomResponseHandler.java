/* Scrutari - Copyright (c) 2011-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets.feed;

import fr.exemole.sctserver.api.ScrutariSession;
import fr.exemole.sctserver.api.feed.SctFeedCache;
import fr.exemole.sctserver.api.feed.SctFeedConstants;
import fr.exemole.sctserver.api.feed.SctFeedOptions;
import fr.exemole.sctserver.xml.feed.AtomFeedXMLPart;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletResponse;
import net.mapeadores.util.servlets.ResponseHandler;
import net.mapeadores.util.xml.AppendableXMLWriter;
import net.mapeadores.util.xml.XMLUtils;


/**
 *
 * @author Vincent Calame
 */
public class AtomResponseHandler implements ResponseHandler {

    private final ScrutariSession scrutariSession;
    private final SctFeedOptions feedOptions;

    public AtomResponseHandler(ScrutariSession scrutariSession, SctFeedOptions feedOptions) {
        this.scrutariSession = scrutariSession;
        this.feedOptions = feedOptions;
    }

    @Override
    public long getLastModified() {
        return scrutariSession.getSctFeedInfo().getLastScrutariDBNameList().get(0).getDate().getTime();
    }

    @Override
    public void handleResponse(HttpServletResponse response) throws IOException {
        response.setContentType("application/atom+xml;charset=UTF-8");
        try (PrintWriter pw = response.getWriter()) {
            SctFeedCache sctFeedCache = scrutariSession.getSctFeedCache();
            String feedString = sctFeedCache.getFeedString(feedOptions);
            if (feedString == null) {
                StringBuilder buf = new StringBuilder();
                int indent = 0;
                if ((feedOptions.getOptionMask() & SctFeedConstants.INDENT_OPTION) == 0) {
                    indent = -99999;
                }
                AppendableXMLWriter xmlWriter = XMLUtils.toXMLWriter(buf, indent);
                xmlWriter.appendXMLDeclaration();
                AtomFeedXMLPart feedXMLPart = new AtomFeedXMLPart(xmlWriter, scrutariSession, feedOptions);
                feedXMLPart.appendFeed();
                feedString = buf.toString();
                sctFeedCache.saveFeedString(feedOptions, feedString);
            }
            pw.print(feedString);
        }
    }

}
