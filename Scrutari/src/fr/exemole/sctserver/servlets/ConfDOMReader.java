/* Scrutari - Copyright (c) 2017-2019 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package fr.exemole.sctserver.servlets;

import fr.exemole.sctserver.tools.ErrorUtils;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import net.mapeadores.util.localisation.Lang;
import net.mapeadores.util.logging.MessageLogBuilder;
import net.mapeadores.util.xml.DomMessages;
import net.mapeadores.util.xml.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


/**
 *
 * @author Vincent Calame
 */
class ConfDOMReader {


    static Conf readConf(File f, MessageLogBuilder messageLogBuilder) {
        Document document = toDocument(f, messageLogBuilder);
        if (document == null) {
            return null;
        }
        Conf conf = new Conf();
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nd = nodeList.item(i);
            if (nd instanceof Element) {
                Element el = (Element) nd;
                String tagName = el.getTagName();
                String data = XMLUtils.getData(el);
                if ((tagName.equals("canonical-url")) || (tagName.equals("url"))) {
                    if (data.length() > 0) {
                        if (!data.endsWith("/")) {
                            data = data + "/";
                        }
                    }
                    conf.webappCanonicalUrl = data;
                } else if ((tagName.equals("default-lang")) || (tagName.equals("default-lang-ui"))) {
                    if (data.length() > 0) {
                        try {
                            conf.webappDefaultLang = Lang.parse(data);
                        } catch (ParseException pe) {
                            DomMessages.wrongElementValue(messageLogBuilder, "/conf/" + tagName, data);
                        }
                    }
                }
            }
        }
        return conf;
    }

    static Dirs readDirs(String scrutariConfFileString, MessageLogBuilder messageLogBuilder) {
        Document document = toDocument(scrutariConfFileString, messageLogBuilder);
        if (document == null) {
            return null;
        }
        String confDirString = null;
        String varDirString = null;
        NodeList nodeList = document.getDocumentElement().getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node nd = nodeList.item(i);
            if (nd instanceof Element) {
                Element el = (Element) nd;
                String tagName = el.getTagName();
                String data = XMLUtils.getData(el);
                if (tagName.equals("conf-dir")) {
                    confDirString = data;
                } else if (tagName.equals("var-dir")) {
                    varDirString = data;
                }
            }
        }
        boolean critical = false;
        if ((confDirString == null) || (confDirString.length() == 0)) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.xml_missingtag", "/scrutari-conf/conf-dir");
            critical = true;
        }
        if ((varDirString == null) || (varDirString.length() == 0)) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.xml_missingtag", "/scrutari-conf/var-dir");
            critical = true;
        }
        if (critical) {
            return null;
        }
        return new Dirs(confDirString, varDirString);
    }


    private static Document toDocument(String scrutariConfFileString, MessageLogBuilder messageLogBuilder) {
        messageLogBuilder.setCurrentSource(scrutariConfFileString);
        File scrutariConfFile = new File(scrutariConfFileString);
        if (!scrutariConfFile.exists()) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.conffile_missing", scrutariConfFileString);
            return null;
        }
        if (scrutariConfFile.isDirectory()) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.conffile_dir", scrutariConfFileString);
            return null;
        }
        return toDocument(scrutariConfFile, messageLogBuilder);
    }

    static Document toDocument(File f, MessageLogBuilder messageLogBuilder) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(f);
            return doc;
        } catch (ParserConfigurationException pce) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.parseconfig", pce.getMessage());
            return null;
        } catch (IOException ioe) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.conffile_io", f.getPath(), ioe.getMessage());
            return null;
        } catch (SAXException sax) {
            ErrorUtils.configCritical(messageLogBuilder, "_ error.config.conffile_xml", f.getPath(), sax.getMessage());
            return null;
        }
    }


    static class Dirs {

        private final String confDirString;
        private final String varDirString;

        private Dirs(String confDirString, String varDirString) {
            this.confDirString = confDirString;
            this.varDirString = varDirString;
        }

        String getConfDirString() {
            return confDirString;
        }

        String getVarDirString() {
            return varDirString;
        }


    }


    static class Conf {

        private String webappCanonicalUrl = "";
        private Lang webappDefaultLang = ScrutariWebapp.DEFAULT_LANG;

        public String getWebappCanonicalUrl() {
            return webappCanonicalUrl;
        }

        public Lang getWebappDefaultLang() {
            return webappDefaultLang;
        }

    }

}
