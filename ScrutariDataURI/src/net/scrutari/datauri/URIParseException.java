/* ScrutariDataURI - Copyright (c) 2005-2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;


/**
 *
 * @author Vincent Calame
 */
public class URIParseException extends Exception {

    public final static short URISYNTAX_EXCEPTION = 0;
    public final static short MISSINGSCHEME_EXCEPTION = 1;
    public final static short UNKNOWNSCHEME_EXCEPTION = 2;
    public final static short OPAQUEURI_EXCEPTION = 3;
    public final static short MALFORMEDPATH_EXCEPTION = 4;
    short type;

    public URIParseException(short type) {
        this.type = type;
    }

    public URIParseException(short type, String message) {
        super(message);
        this.type = type;
    }

    public short getType() {
        return type;
    }

}
