/* ScrutariDataURI - Copyright (c) 2005-2010 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;

import java.io.Serializable;


/**
 *
 * @author Vincent Calame
 */
public final class IndexationKey implements Serializable, Comparable {

    private MotcleURI motcleURI;
    private FicheURI ficheURI;

    public IndexationKey(FicheURI ficheURI, MotcleURI motcleURI) {
        this.ficheURI = ficheURI;
        this.motcleURI = motcleURI;
    }

    public MotcleURI getMotcleURI() {
        return motcleURI;
    }

    public FicheURI getFicheURI() {
        return ficheURI;
    }

    @Override
    public int hashCode() {
        return ficheURI.hashCode() + motcleURI.hashCode();
    }

    public int compareTo(Object o) {
        return compareTo((IndexationKey) o);
    }

    public int compareTo(IndexationKey indexationKey) {
        int resultat = ficheURI.compareTo(indexationKey.ficheURI);
        if (resultat == 0) {
            resultat = motcleURI.compareTo(indexationKey.motcleURI);
        }
        return resultat;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof IndexationKey)) {
            return false;
        }
        IndexationKey uid = (IndexationKey) o;
        return (compareTo(uid) == 0);
    }

}
