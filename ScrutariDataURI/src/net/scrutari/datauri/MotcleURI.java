/* ScrutariDataURI - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;


/**
 *
 * @author Vincent Calame
 */
public final class MotcleURI extends ScrutariDataURI implements Comparable<MotcleURI> {

    private final ThesaurusURI thesaurusURI;
    private final String motcleId;

    public MotcleURI(ThesaurusURI thesaurusURI, String motcleId) {
        if (motcleId == null) {
            throw new IllegalArgumentException("corpusName is null");
        }
        if (thesaurusURI == null) {
            throw new IllegalArgumentException("thesaurusURI is null");
        }
        this.thesaurusURI = thesaurusURI;
        this.motcleId = motcleId;
    }

    public String getMotcleId() {
        return motcleId;
    }

    @Override
    public BaseURI getBaseURI() {
        return thesaurusURI.getBaseURI();
    }

    public ThesaurusURI getThesaurusURI() {
        return thesaurusURI;
    }

    @Override
    public int hashCode() {
        return motcleId.hashCode() + thesaurusURI.hashCode();
    }

    @Override
    public int compareTo(MotcleURI motcleURI) {
        int result = this.motcleId.compareTo(motcleURI.motcleId);
        if (result == 0) {
            result = this.thesaurusURI.compareTo(motcleURI.thesaurusURI);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof MotcleURI)) {
            return false;
        }
        return (compareTo((MotcleURI) o) == 0);
    }

    @Override
    public short getType() {
        return MOTCLEURI_TYPE;
    }

    public MotcleURI derive(ThesaurusURI thesaurusURI) {
        return new MotcleURI(thesaurusURI, motcleId);
    }

    @Override
    protected void appendPath(StringBuilder buf) {
        BaseURI baseURI = thesaurusURI.getBaseURI();
        buf.append('/');
        buf.append(baseURI.getAuthority());
        buf.append('/');
        buf.append(baseURI.getBaseName());
        buf.append('/');
        buf.append(thesaurusURI.getThesaurusName());
        buf.append('/');
        buf.append(motcleId);
    }

    public static MotcleURI check(ThesaurusURI thesaurusURI, String motcleId) throws URIParseException {
        motcleId = URIParser.checkPathPart(motcleId);
        return new MotcleURI(thesaurusURI, motcleId);
    }

}
