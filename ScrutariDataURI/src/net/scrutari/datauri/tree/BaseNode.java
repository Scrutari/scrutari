/* ScrutariDataUR - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri.tree;

import java.util.List;
import net.scrutari.datauri.BaseURI;


/**
 *
 * @author Vincent Calame
 */
public interface BaseNode extends URINode {

    public BaseURI getBaseURI();

    public List<CorpusNode> getCorpusNodeList();

    public List<ThesaurusNode> getThesaurusNodeList();

}
