/* ScrutariDataURI - Copyright (c) 2011 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri.tree;

import net.scrutari.datauri.MotcleURI;


/**
 *
 * @author Vincent Calame
 */
public interface MotcleNode extends URINode {

    public MotcleURI getMotcleURI();

}
