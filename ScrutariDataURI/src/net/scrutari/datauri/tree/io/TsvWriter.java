/* ScrutariDataURI - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri.tree.io;

import java.io.IOException;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.MotcleNode;
import net.scrutari.datauri.tree.ThesaurusNode;
import net.scrutari.datauri.tree.URINode;
import net.scrutari.datauri.tree.URITree;


/**
 *
 * @author Vincent Calame
 */
public class TsvWriter {

    public static void write(Appendable appendable, URITree uriTree) throws IOException {
        for (BaseNode baseNode : uriTree.getBaseNodeList()) {
            BaseURI baseURI = baseNode.getBaseURI();
            appendable.append('b');
            appendActive(appendable, baseNode);
            appendable.append(baseURI.getAuthority());
            appendable.append('/');
            appendable.append(baseURI.getBaseName());
            appendAssociatedObject(appendable, baseNode);
            appendable.append('\n');
            for (CorpusNode corpusNode : baseNode.getCorpusNodeList()) {
                appendable.append('c');
                appendActive(appendable, corpusNode);
                appendable.append(corpusNode.getCorpusURI().getCorpusName());
                appendAssociatedObject(appendable, corpusNode);
                appendable.append('\n');
                for (FicheNode ficheNode : corpusNode.getFicheNodeList()) {
                    appendable.append('f');
                    appendActive(appendable, ficheNode);
                    appendable.append(ficheNode.getFicheURI().getFicheId());
                    appendAssociatedObject(appendable, ficheNode);
                    appendable.append('\n');
                }
            }
            for (ThesaurusNode thesaurusNode : baseNode.getThesaurusNodeList()) {
                appendable.append('t');
                appendActive(appendable, thesaurusNode);
                appendable.append(thesaurusNode.getThesaurusURI().getThesaurusName());
                appendAssociatedObject(appendable, thesaurusNode);
                appendable.append('\n');
                for (MotcleNode motcleNode : thesaurusNode.getMotcleNodeList()) {
                    appendable.append('m');
                    appendActive(appendable, motcleNode);
                    appendable.append(motcleNode.getMotcleURI().getMotcleId());
                    appendAssociatedObject(appendable, motcleNode);
                    appendable.append('\n');
                }
            }
        }
    }

    private static void appendActive(Appendable appendable, URINode uriNode) throws IOException {
        if (uriNode.isActive()) {
            appendable.append('=');
        } else {
            appendable.append(':');
        }
    }

    private static void appendAssociatedObject(Appendable appendable, URINode uriNode) throws IOException {
        Object obj = uriNode.getAssociatedObject();
        if (obj != null) {
            appendable.append('\t');
            appendable.append(obj.toString());
        }
    }

}
