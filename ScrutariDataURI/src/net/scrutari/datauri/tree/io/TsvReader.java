/* ScrutariDataURI - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri.tree.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ScrutariDataURIChecker;
import net.scrutari.datauri.ThesaurusURI;
import net.scrutari.datauri.tree.BaseNode;
import net.scrutari.datauri.tree.CorpusNode;
import net.scrutari.datauri.tree.FicheNode;
import net.scrutari.datauri.tree.MotcleNode;
import net.scrutari.datauri.tree.ThesaurusNode;
import net.scrutari.datauri.tree.URINode;
import net.scrutari.datauri.tree.URITree;


/**
 *
 * @author Vincent Calame
 */
public class TsvReader {

    private final InternalURITree uriTree = new InternalURITree();
    private InternalBaseNode currentBaseNode;

    private TsvReader() {
    }

    private URITree toURITree() {
        return uriTree;
    }

    public static URITree readURITree(Reader reader, ScrutariDataURIChecker checker, AssociatedObjectParser parser) throws IOException {
        BufferedReader bufreader = new BufferedReader(reader);
        TsvReader tsvReader = new TsvReader();
        String ligne;
        int lineNumber = 0;
        while ((ligne = bufreader.readLine()) != null) {
            lineNumber++;
            tsvReader.readLine(ligne, checker, lineNumber, parser);
        }
        return tsvReader.toURITree();
    }

    private void readLine(String line, ScrutariDataURIChecker checker, int lineNumber, AssociatedObjectParser parser) {
        if (line.length() < 2) {
            return;
        }
        char caracInitial = line.charAt(0);
        char caracActive = line.charAt(1);
        boolean active = true;
        if (caracActive == ':') {
            active = false;
        }
        int idx = line.indexOf('\t');
        String associatedString = null;
        String uriString;
        if (idx != -1) {
            associatedString = line.substring(idx + 1);
            uriString = line.substring(2, idx);
        } else {
            uriString = line.substring(2);
        }
        AbstractURINode uriNode;
        switch (caracInitial) {
            case 'b':
                uriNode = readBase(uriString, checker);
                break;
            case 'c':
                uriNode = readCorpus(uriString, checker);
                break;
            case 't':
                uriNode = readThesaurus(uriString, checker);
                break;
            case 'f':
                uriNode = readFiche(uriString, checker);
                break;
            case 'm':
                uriNode = readMotcle(uriString, checker);
                break;
            default:
                return;
        }
        if (uriNode != null) {
            uriNode.setActive(active);
            if (parser != null) {
                uriNode.setAssociatedObject(parser.parse(lineNumber, associatedString));
            }
        }
    }

    private InternalBaseNode readBase(String uriString, ScrutariDataURIChecker checker) {
        int idx = uriString.indexOf('/');
        if (idx == -1) {
            return null;
        }
        String authority = uriString.substring(0, idx);
        String baseName = uriString.substring(idx + 1);
        BaseURI baseURI = new BaseURI(authority, baseName);
        if (checker != null) {
            baseURI = (BaseURI) checker.checkURI(baseURI);
        }
        InternalBaseNode baseNode = new InternalBaseNode(baseURI);
        uriTree.addBaseNode(baseNode);
        currentBaseNode = baseNode;
        return baseNode;
    }

    private InternalCorpusNode readCorpus(String uriString, ScrutariDataURIChecker checker) {
        BaseURI baseURI = currentBaseNode.getBaseURI();
        CorpusURI corpusURI = new CorpusURI(baseURI, uriString);
        if (checker != null) {
            corpusURI = (CorpusURI) checker.checkURI(corpusURI);
        }
        InternalCorpusNode corpusNode = new InternalCorpusNode(corpusURI);
        currentBaseNode.addCorpusNode(corpusNode);
        return corpusNode;
    }

    private InternalThesaurusNode readThesaurus(String uriString, ScrutariDataURIChecker checker) {
        BaseURI baseURI = currentBaseNode.getBaseURI();
        ThesaurusURI thesaurusURI = new ThesaurusURI(baseURI, uriString);
        if (checker != null) {
            thesaurusURI = (ThesaurusURI) checker.checkURI(thesaurusURI);
        }
        InternalThesaurusNode thesaurusNode = new InternalThesaurusNode(thesaurusURI);
        currentBaseNode.addThesaurusNode(thesaurusNode);
        return thesaurusNode;
    }

    private InternalFicheNode readFiche(String uriString, ScrutariDataURIChecker checker) {
        InternalCorpusNode corpusNode = currentBaseNode.currentCorpusNode;
        CorpusURI corpusURI = corpusNode.getCorpusURI();
        FicheURI ficheURI = new FicheURI(corpusURI, uriString);
        if (checker != null) {
            ficheURI = (FicheURI) checker.checkURI(ficheURI);
        }
        InternalFicheNode ficheNode = new InternalFicheNode(ficheURI);
        corpusNode.addFicheNode(ficheNode);
        return ficheNode;
    }

    private InternalMotcleNode readMotcle(String uriString, ScrutariDataURIChecker checker) {
        InternalThesaurusNode thesaurusNode = currentBaseNode.currentThesaurusNode;
        ThesaurusURI thesaurusURI = thesaurusNode.getThesaurusURI();
        MotcleURI motcleURI = new MotcleURI(thesaurusURI, uriString);
        if (checker != null) {
            motcleURI = (MotcleURI) checker.checkURI(motcleURI);
        }
        InternalMotcleNode ficheNode = new InternalMotcleNode(motcleURI);
        thesaurusNode.addMotcleNode(ficheNode);
        return ficheNode;
    }


    public static abstract class AssociatedObjectParser {

        public abstract Object parse(int lineNumber, String associatedString);

    }


    private static class InternalURITree implements URITree {

        private final List<BaseNode> baseNodeList = new ArrayList<BaseNode>();

        private InternalURITree() {
        }

        @Override
        public List<BaseNode> getBaseNodeList() {
            return baseNodeList;
        }

        private void addBaseNode(InternalBaseNode baseNode) {
            baseNodeList.add(baseNode);
        }

    }


    private static abstract class AbstractURINode implements URINode {

        private boolean active = false;
        private Object associatedObject = null;

        protected AbstractURINode() {

        }

        @Override
        public boolean isActive() {
            return active;
        }

        protected void setActive(boolean active) {
            this.active = active;
        }

        @Override
        public Object getAssociatedObject() {
            return associatedObject;
        }

        protected void setAssociatedObject(Object associatedObject) {
            this.associatedObject = associatedObject;
        }

    }


    private static class InternalBaseNode extends AbstractURINode implements BaseNode {

        private final BaseURI baseURI;
        private InternalCorpusNode currentCorpusNode;
        private InternalThesaurusNode currentThesaurusNode;
        private final List<CorpusNode> corpusNodeList = new ArrayList<CorpusNode>();
        private final List<ThesaurusNode> thesaurusNodeList = new ArrayList<ThesaurusNode>();

        private InternalBaseNode(BaseURI baseURI) {
            this.baseURI = baseURI;
        }

        @Override
        public BaseURI getBaseURI() {
            return baseURI;
        }

        @Override
        public List<CorpusNode> getCorpusNodeList() {
            return corpusNodeList;
        }

        @Override
        public List<ThesaurusNode> getThesaurusNodeList() {
            return thesaurusNodeList;
        }

        private void addCorpusNode(InternalCorpusNode corpusNode) {
            corpusNodeList.add(corpusNode);
            currentCorpusNode = corpusNode;
        }

        private void addThesaurusNode(InternalThesaurusNode thesaurusNode) {
            thesaurusNodeList.add(thesaurusNode);
            currentThesaurusNode = thesaurusNode;
        }

    }


    private static class InternalCorpusNode extends AbstractURINode implements CorpusNode {

        private final CorpusURI corpusURI;
        private final List<FicheNode> ficheNodeList = new ArrayList<FicheNode>();

        private InternalCorpusNode(CorpusURI corpusURI) {
            this.corpusURI = corpusURI;
        }

        @Override
        public CorpusURI getCorpusURI() {
            return corpusURI;
        }

        @Override
        public List<FicheNode> getFicheNodeList() {
            return ficheNodeList;
        }

        private void addFicheNode(InternalFicheNode ficheNode) {
            ficheNodeList.add(ficheNode);
        }

    }


    private static class InternalThesaurusNode extends AbstractURINode implements ThesaurusNode {

        private final ThesaurusURI thesaurusURI;
        private final List<MotcleNode> motcleNodeList = new ArrayList<MotcleNode>();

        private InternalThesaurusNode(ThesaurusURI thesaurusURI) {
            this.thesaurusURI = thesaurusURI;
        }

        @Override
        public ThesaurusURI getThesaurusURI() {
            return thesaurusURI;
        }

        @Override
        public List<MotcleNode> getMotcleNodeList() {
            return motcleNodeList;
        }

        private void addMotcleNode(InternalMotcleNode motcleNode) {
            motcleNodeList.add(motcleNode);
        }

    }


    private static class InternalFicheNode extends AbstractURINode implements FicheNode {

        private final FicheURI ficheURI;

        private InternalFicheNode(FicheURI ficheURI) {
            this.ficheURI = ficheURI;
        }

        @Override
        public FicheURI getFicheURI() {
            return ficheURI;
        }

    }


    private static class InternalMotcleNode extends AbstractURINode implements MotcleNode {

        private final MotcleURI motcleURI;

        private InternalMotcleNode(MotcleURI motcleURI) {
            this.motcleURI = motcleURI;
        }

        @Override
        public MotcleURI getMotcleURI() {
            return motcleURI;
        }

    }

}
