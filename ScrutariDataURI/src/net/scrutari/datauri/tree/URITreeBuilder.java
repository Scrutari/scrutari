/* ScrutariDataURI - Copyright (c) 2011-2021 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri.tree;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.scrutari.datauri.BaseURI;
import net.scrutari.datauri.CorpusURI;
import net.scrutari.datauri.FicheURI;
import net.scrutari.datauri.MotcleURI;
import net.scrutari.datauri.ScrutariDataURI;
import net.scrutari.datauri.ThesaurusURI;


/**
 *
 * @author Vincent Calame
 */
public class URITreeBuilder {

    private Map<BaseURI, InternalBaseNode> baseNodeMap = new HashMap<BaseURI, InternalBaseNode>();
    private Map<CorpusURI, InternalCorpusNode> corpusNodeMap = new HashMap<CorpusURI, InternalCorpusNode>();
    private Map<ThesaurusURI, InternalThesaurusNode> thesaurusNodeMap = new HashMap<ThesaurusURI, InternalThesaurusNode>();
    private Map<FicheURI, InternalFicheNode> ficheNodeMap = new HashMap<FicheURI, InternalFicheNode>();
    private Map<MotcleURI, InternalMotcleNode> motcleNodeMap = new HashMap<MotcleURI, InternalMotcleNode>();
    private InternalURITree uriTree = new InternalURITree();

    public URITreeBuilder() {
    }

    public void addScrutariDataURI(ScrutariDataURI scrutariDataURI, Object associatedObject) {
        switch (scrutariDataURI.getType()) {
            case ScrutariDataURI.BASEURI_TYPE:
                add((BaseURI) scrutariDataURI, associatedObject);
                return;
            case ScrutariDataURI.CORPUSURI_TYPE:
                add((CorpusURI) scrutariDataURI, associatedObject);
                return;
            case ScrutariDataURI.THESAURUSURI_TYPE:
                add((ThesaurusURI) scrutariDataURI, associatedObject);
                return;
            case ScrutariDataURI.FICHEURI_TYPE:
                add((FicheURI) scrutariDataURI, associatedObject);
                return;
            case ScrutariDataURI.MOTCLEURI_TYPE:
                add((MotcleURI) scrutariDataURI, associatedObject);
                return;
            default:
                throw new IllegalArgumentException("Unknown type: " + scrutariDataURI.getType());
        }
    }

    public void add(BaseURI baseURI, Object associatedObject) {
        InternalBaseNode baseNode = getBaseNode(baseURI);
        baseNode.setActive(true);
        baseNode.setAssociatedObject(associatedObject);
    }

    public void add(CorpusURI corpusURI, Object associatedObject) {
        InternalCorpusNode corpusNode = getCorpusNode(corpusURI);
        corpusNode.setActive(true);
        corpusNode.setAssociatedObject(associatedObject);
    }

    public void add(ThesaurusURI thesaurusURI, Object associatedObject) {
        InternalThesaurusNode thesaurusNode = getThesaurusNode(thesaurusURI);
        thesaurusNode.setActive(true);
        thesaurusNode.setAssociatedObject(associatedObject);
    }

    public void add(FicheURI ficheURI, Object associatedObject) {
        InternalFicheNode ficheNode = getFicheNode(ficheURI);
        ficheNode.setActive(true);
        ficheNode.setAssociatedObject(associatedObject);
    }

    public void add(MotcleURI motcleURI, Object associatedObject) {
        InternalMotcleNode motcleNode = getMotcleNode(motcleURI);
        motcleNode.setActive(true);
        motcleNode.setAssociatedObject(associatedObject);
    }

    public URITree toURITree() {
        return uriTree;
    }

    private InternalBaseNode getBaseNode(BaseURI baseURI) {
        InternalBaseNode baseNode = baseNodeMap.get(baseURI);
        if (baseNode == null) {
            baseNode = new InternalBaseNode(baseURI);
            baseNodeMap.put(baseURI, baseNode);
            uriTree.addBaseNode(baseNode);
        }
        return baseNode;
    }

    private InternalCorpusNode getCorpusNode(CorpusURI corpusURI) {
        InternalCorpusNode corpusNode = corpusNodeMap.get(corpusURI);
        if (corpusNode == null) {
            corpusNode = new InternalCorpusNode(corpusURI);
            corpusNodeMap.put(corpusURI, corpusNode);
            InternalBaseNode baseNode = getBaseNode(corpusURI.getBaseURI());
            baseNode.addCorpusNode(corpusNode);
        }
        return corpusNode;
    }

    private InternalThesaurusNode getThesaurusNode(ThesaurusURI thesaurusURI) {
        InternalThesaurusNode thesaurusNode = thesaurusNodeMap.get(thesaurusURI);
        if (thesaurusNode == null) {
            thesaurusNode = new InternalThesaurusNode(thesaurusURI);
            thesaurusNodeMap.put(thesaurusURI, thesaurusNode);
            InternalBaseNode baseNode = getBaseNode(thesaurusURI.getBaseURI());
            baseNode.addThesaurusNode(thesaurusNode);
        }
        return thesaurusNode;
    }

    private InternalFicheNode getFicheNode(FicheURI ficheURI) {
        InternalFicheNode ficheNode = ficheNodeMap.get(ficheURI);
        if (ficheNode == null) {
            ficheNode = new InternalFicheNode(ficheURI);
            ficheNodeMap.put(ficheURI, ficheNode);
            InternalCorpusNode corpusNode = getCorpusNode(ficheURI.getCorpusURI());
            corpusNode.addFicheNode(ficheNode);
        }
        return ficheNode;
    }

    private InternalMotcleNode getMotcleNode(MotcleURI motcleURI) {
        InternalMotcleNode motcleNode = motcleNodeMap.get(motcleURI);
        if (motcleNode == null) {
            motcleNode = new InternalMotcleNode(motcleURI);
            motcleNodeMap.put(motcleURI, motcleNode);
            InternalThesaurusNode thesaurusNode = getThesaurusNode(motcleURI.getThesaurusURI());
            thesaurusNode.addMotcleNode(motcleNode);
        }
        return motcleNode;
    }


    private static class InternalURITree implements URITree {

        private final List<BaseNode> baseNodeList = new ArrayList<BaseNode>();

        private InternalURITree() {
        }

        @Override
        public List<BaseNode> getBaseNodeList() {
            return baseNodeList;
        }

        private void addBaseNode(InternalBaseNode baseNode) {
            baseNodeList.add(baseNode);
        }

    }


    private static abstract class AbstractURINode implements URINode {

        private boolean active = false;
        private Object associatedObject = null;

        @Override
        public boolean isActive() {
            return active;
        }

        protected void setActive(boolean active) {
            this.active = active;
        }

        @Override
        public Object getAssociatedObject() {
            return associatedObject;
        }

        protected void setAssociatedObject(Object associatedObject) {
            this.associatedObject = associatedObject;
        }

    }


    private static class InternalBaseNode extends AbstractURINode implements BaseNode {

        private final BaseURI baseURI;
        private final List<CorpusNode> corpusNodeList = new ArrayList<CorpusNode>();
        private final List<ThesaurusNode> thesaurusNodeList = new ArrayList<ThesaurusNode>();

        private InternalBaseNode(BaseURI baseURI) {
            this.baseURI = baseURI;
        }

        @Override
        public BaseURI getBaseURI() {
            return baseURI;
        }

        @Override
        public List<CorpusNode> getCorpusNodeList() {
            return corpusNodeList;
        }

        @Override
        public List<ThesaurusNode> getThesaurusNodeList() {
            return thesaurusNodeList;
        }

        private void addCorpusNode(InternalCorpusNode corpusNode) {
            corpusNodeList.add(corpusNode);
        }

        private void addThesaurusNode(InternalThesaurusNode thesaurusNode) {
            thesaurusNodeList.add(thesaurusNode);
        }

    }


    private static class InternalCorpusNode extends AbstractURINode implements CorpusNode {

        private final CorpusURI corpusURI;
        private final List<FicheNode> ficheNodeList = new ArrayList<FicheNode>();

        private InternalCorpusNode(CorpusURI corpusURI) {
            this.corpusURI = corpusURI;
        }

        @Override
        public CorpusURI getCorpusURI() {
            return corpusURI;
        }

        @Override
        public List<FicheNode> getFicheNodeList() {
            return ficheNodeList;
        }

        private void addFicheNode(InternalFicheNode ficheNode) {
            ficheNodeList.add(ficheNode);
        }

    }


    private static class InternalThesaurusNode extends AbstractURINode implements ThesaurusNode {

        private final ThesaurusURI thesaurusURI;
        private final List<MotcleNode> motcleNodeList = new ArrayList<MotcleNode>();

        private InternalThesaurusNode(ThesaurusURI thesaurusURI) {
            this.thesaurusURI = thesaurusURI;
        }

        @Override
        public ThesaurusURI getThesaurusURI() {
            return thesaurusURI;
        }

        @Override
        public List<MotcleNode> getMotcleNodeList() {
            return motcleNodeList;
        }

        private void addMotcleNode(InternalMotcleNode motcleNode) {
            motcleNodeList.add(motcleNode);
        }

    }


    private static class InternalFicheNode extends AbstractURINode implements FicheNode {

        private final FicheURI ficheURI;

        private InternalFicheNode(FicheURI ficheURI) {
            this.ficheURI = ficheURI;
        }

        @Override
        public FicheURI getFicheURI() {
            return ficheURI;
        }

    }


    private static class InternalMotcleNode extends AbstractURINode implements MotcleNode {

        private final MotcleURI motcleURI;

        private InternalMotcleNode(MotcleURI motcleURI) {
            this.motcleURI = motcleURI;
        }

        @Override
        public MotcleURI getMotcleURI() {
            return motcleURI;
        }

    }

}
