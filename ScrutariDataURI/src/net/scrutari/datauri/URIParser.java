/* ScrutariDataURI - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Pattern;


/**
 *
 * @author Vincent Calame
 */
public class URIParser {

    private final static Pattern CHECK_PATTERN = Pattern.compile("^[-_\\.0-9a-zA-Z]+$");

    private URIParser() {
    }

    public static ScrutariDataURI parse(String s) throws URIParseException {
        try {
            URI uri = new URI(s);
            String scheme = uri.getScheme();
            if (scheme == null) {
                throw new URIParseException(URIParseException.MISSINGSCHEME_EXCEPTION, "missing scheme");
            }
            if (uri.isOpaque()) {
                throw new URIParseException(URIParseException.OPAQUEURI_EXCEPTION, "opaque uri");
            }
            String tokens[] = uri.toASCIIString().split("/");
            int size = tokens.length - 1;
            if (size < 2) {
                throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "missing parts");
            }
            if (size > 4) {
                throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "too many parts");
            }
            BaseURI baseURI = new BaseURI(tokens[1], tokens[2]);
            if (scheme.equals("fiche")) {
                if (size != 4) {
                    throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "need 4 parts");
                }
                CorpusURI corpusURI = new CorpusURI(baseURI, tokens[3]);
                return new FicheURI(corpusURI, tokens[4]);
            } else if (scheme.equals("corpus")) {
                if (size != 3) {
                    throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "need 3 parts");
                }
                return new CorpusURI(baseURI, tokens[3]);
            } else if (scheme.equals("thesaurus")) {
                if (size != 3) {
                    throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "need 3 parts");
                }
                return new ThesaurusURI(baseURI, tokens[3]);
            } else if (scheme.equals("motcle")) {
                if (size != 4) {
                    throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "need 4 parts");
                }
                ThesaurusURI thesaurusURI = new ThesaurusURI(baseURI, tokens[3]);
                return new MotcleURI(thesaurusURI, tokens[4]);
            } else if (scheme.equals("base")) {
                if (size != 2) {
                    throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "need 2 parts");
                }
                return baseURI;
            } else {
                throw new URIParseException(URIParseException.UNKNOWNSCHEME_EXCEPTION, "unknown scheme: " + scheme);
            }
        } catch (URISyntaxException urise) {
            throw new URIParseException(URIParseException.URISYNTAX_EXCEPTION, urise.getMessage());
        }
    }

    public static String checkPathPart(String part) throws URIParseException {
        if ((part == null) || (part.length() == 0)) {
            throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "empty part");
        }
        if (CHECK_PATTERN.matcher(part).matches()) {
            return part;
        }
        if (part.indexOf('/') != -1) {
            throw new URIParseException(URIParseException.MALFORMEDPATH_EXCEPTION, "contains / character");
        }
        try {
            URI uri = new URI(null, "check", part);
            String ascii = uri.toASCIIString();
            int idx = ascii.indexOf('#');
            return ascii.substring(idx + 1);
        } catch (URISyntaxException urise) {
            throw new URIParseException(URIParseException.URISYNTAX_EXCEPTION, urise.getMessage());
        }
    }

}
