/* ScrutariDataURI - Copyright (c) 2005-2013 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;


/**
 *
 * @author Vincent Calame
 */
public abstract class ScrutariDataURI implements Serializable {

    public final static short BASEURI_TYPE = 1;
    public final static short CORPUSURI_TYPE = 2;
    public final static short THESAURUSURI_TYPE = 3;
    public final static short FICHEURI_TYPE = 4;
    public final static short MOTCLEURI_TYPE = 5;

    public URI toURI() {
        try {
            return new URI(toString());
        } catch (URISyntaxException urise) {
            return null;
        }
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        buf.append(typeToSchemeString(getType()));
        buf.append(':');
        appendPath(buf);
        return buf.toString();
    }

    /**
     * Retourne la partie « chemin » de l'URI (URI sans le schème)
     */
    public String getPath() {
        StringBuilder buf = new StringBuilder();
        appendPath(buf);
        return buf.toString();
    }

    public abstract short getType();

    public abstract BaseURI getBaseURI();

    protected abstract void appendPath(StringBuilder buf);

    public static String typeToSchemeString(short type) {
        switch (type) {
            case BASEURI_TYPE:
                return "base";
            case CORPUSURI_TYPE:
                return "corpus";
            case THESAURUSURI_TYPE:
                return "thesaurus";
            case FICHEURI_TYPE:
                return "fiche";
            case MOTCLEURI_TYPE:
                return "motcle";
            default:
                throw new IllegalArgumentException("Unknown type = " + type);
        }
    }

}
