/* ScrutariDataURI - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;


/**
 *
 * @author Vincent Calame
 */
public final class BaseURI extends ScrutariDataURI implements Comparable<BaseURI> {

    private final String authority;
    private final String baseName;

    public BaseURI(String authority, String baseName) {
        if (authority == null) {
            throw new IllegalArgumentException("authorityUUID is null");
        }
        if (baseName == null) {
            throw new IllegalArgumentException("baseName is null");
        }
        this.authority = authority;
        this.baseName = baseName;
    }

    public String getAuthority() {
        return authority;
    }

    public String getBaseName() {
        return baseName;
    }

    @Override
    public BaseURI getBaseURI() {
        return this;
    }

    @Override
    public int hashCode() {
        return authority.hashCode() + baseName.hashCode();
    }

    @Override
    public int compareTo(BaseURI baseURI) {
        int result = this.baseName.compareTo(baseURI.baseName);
        if (result == 0) {
            result = this.authority.compareTo(baseURI.authority);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof BaseURI)) {
            return false;
        }
        return (compareTo((BaseURI) o) == 0);
    }

    @Override
    public short getType() {
        return BASEURI_TYPE;
    }

    @Override
    protected void appendPath(StringBuilder buf) {
        buf.append('/');
        buf.append(authority);
        buf.append('/');
        buf.append(baseName);
    }

    public static BaseURI check(String authority, String baseName) throws URIParseException {
        authority = URIParser.checkPathPart(authority);
        baseName = URIParser.checkPathPart(baseName);
        return new BaseURI(authority, baseName);

    }

}
