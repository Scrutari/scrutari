/* ScrutariDataURI - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;


/**
 *
 * @author Vincent Calame
 */
public final class CorpusURI extends ScrutariDataURI implements Comparable<CorpusURI> {

    private final BaseURI baseURI;
    private final String corpusName;

    public CorpusURI(BaseURI baseURI, String corpusName) {
        if (corpusName == null) {
            throw new IllegalArgumentException("corpusName is null");
        }
        if (baseURI == null) {
            throw new IllegalArgumentException("baseURI is null");
        }
        this.corpusName = corpusName;
        this.baseURI = baseURI;
    }

    @Override
    public BaseURI getBaseURI() {
        return baseURI;
    }

    public String getCorpusName() {
        return corpusName;
    }

    @Override
    public int hashCode() {
        return corpusName.hashCode() + baseURI.hashCode();
    }

    @Override
    public int compareTo(CorpusURI corpusURI) {
        int result = this.corpusName.compareTo(corpusURI.corpusName);
        if (result == 0) {
            result = this.baseURI.compareTo(corpusURI.baseURI);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CorpusURI)) {
            return false;
        }
        return (compareTo((CorpusURI) o) == 0);
    }

    @Override
    public short getType() {
        return CORPUSURI_TYPE;
    }

    public CorpusURI derive(BaseURI newBaseURI) {
        return new CorpusURI(newBaseURI, corpusName);
    }

    @Override
    protected void appendPath(StringBuilder buf) {
        buf.append('/');
        buf.append(baseURI.getAuthority());
        buf.append('/');
        buf.append(baseURI.getBaseName());
        buf.append('/');
        buf.append(corpusName);
    }

    public static CorpusURI check(BaseURI baseURI, String corpusName) throws URIParseException {
        corpusName = URIParser.checkPathPart(corpusName);
        return new CorpusURI(baseURI, corpusName);
    }

}
