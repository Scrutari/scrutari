/* ScrutariDataURI - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;


/**
 *
 * @author Vincent Calame
 */
public final class FicheURI extends ScrutariDataURI implements Comparable<FicheURI> {

    private final CorpusURI corpusURI;
    private final String ficheId;

    public FicheURI(CorpusURI corpusURI, String ficheId) {
        if (corpusURI == null) {
            throw new IllegalArgumentException("corpusURI is null");
        }
        if (ficheId == null) {
            throw new IllegalArgumentException("ficheId is null");
        }
        this.corpusURI = corpusURI;
        this.ficheId = ficheId;
    }

    public String getFicheId() {
        return ficheId;
    }

    @Override
    public BaseURI getBaseURI() {
        return corpusURI.getBaseURI();
    }

    public CorpusURI getCorpusURI() {
        return corpusURI;
    }

    @Override
    public int hashCode() {
        return ficheId.hashCode() + corpusURI.hashCode();
    }

    @Override
    public int compareTo(FicheURI ficheURI) {
        int result = this.ficheId.compareTo(ficheURI.ficheId);
        if (result == 0) {
            result = this.corpusURI.compareTo(ficheURI.corpusURI);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof FicheURI)) {
            return false;
        }
        return (compareTo((FicheURI) o) == 0);
    }

    @Override
    public short getType() {
        return FICHEURI_TYPE;
    }

    public FicheURI derive(CorpusURI newCorpusURI) {
        return new FicheURI(newCorpusURI, ficheId);
    }

    @Override
    protected void appendPath(StringBuilder buf) {
        BaseURI baseURI = corpusURI.getBaseURI();
        buf.append('/');
        buf.append(baseURI.getAuthority());
        buf.append('/');
        buf.append(baseURI.getBaseName());
        buf.append('/');
        buf.append(corpusURI.getCorpusName());
        buf.append('/');
        buf.append(ficheId);
    }

    public static FicheURI check(CorpusURI corpusURI, String ficheId) throws URIParseException {
        ficheId = URIParser.checkPathPart(ficheId);
        return new FicheURI(corpusURI, ficheId);
    }

}
