/* ScrutariDataURI - Copyright (c) 2005-2020 Vincent Calame - Exemole
 * Logiciel libre donné sous triple licence :
 * 1) selon les termes de la CeCILL V2
 * 2) selon les termes de l’EUPL V.1.1
 * 3) selon les termes de la GNU GPLv3
 * Voir le fichier licences.txt
 */


package net.scrutari.datauri;


/**
 *
 * @author Vincent Calame
 */
public final class ThesaurusURI extends ScrutariDataURI implements Comparable<ThesaurusURI> {

    private final BaseURI baseURI;
    private final String thesaurusName;

    public ThesaurusURI(BaseURI baseURI, String thesaurusName) {
        if (thesaurusName == null) {
            throw new IllegalArgumentException("corpusName is null");
        }
        if (baseURI == null) {
            throw new IllegalArgumentException("baseURI is null");
        }
        this.thesaurusName = thesaurusName;
        this.baseURI = baseURI;
    }

    @Override
    public BaseURI getBaseURI() {
        return baseURI;
    }

    public String getThesaurusName() {
        return thesaurusName;
    }

    @Override
    public int hashCode() {
        return thesaurusName.hashCode() + baseURI.hashCode();
    }

    @Override
    public int compareTo(ThesaurusURI thesaurusURI) {
        int result = this.thesaurusName.compareTo(thesaurusURI.thesaurusName);
        if (result == 0) {
            result = this.baseURI.compareTo(thesaurusURI.baseURI);
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ThesaurusURI)) {
            return false;
        }
        return (compareTo((ThesaurusURI) o) == 0);
    }

    @Override
    public short getType() {
        return THESAURUSURI_TYPE;
    }

    public ThesaurusURI derive(BaseURI baseURI) {
        return new ThesaurusURI(baseURI, thesaurusName);
    }

    @Override
    protected void appendPath(StringBuilder buf) {
        buf.append('/');
        buf.append(baseURI.getAuthority());
        buf.append('/');
        buf.append(baseURI.getBaseName());
        buf.append('/');
        buf.append(thesaurusName);
    }

    public static ThesaurusURI check(BaseURI baseURI, String thesaurusName) throws URIParseException {
        thesaurusName = URIParser.checkPathPart(thesaurusName);
        return new ThesaurusURI(baseURI, thesaurusName);
    }

}
